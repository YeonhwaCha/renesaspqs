/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_format_yuv.c
 * @brief         Source file for YUV image format convert using CVe. See @ref PQS_Format_Yuv for more details
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @page       PQS_Format_Yuv_Algorithm  PQS Format YUV Algorithm
 * @ingroup    PQS_Format_Yuv
 * # Algorithm Description:
 *             This uses CVe to do image formatting. Input is a Interleaved UYVY image. Output is a UYVA and a fully
 *             planar YUV444 image. And a 16bit grey scale image. No color values are changed, bytes are just reordered.
 * # Block Diagram:
 *             @image html format_yuv.png
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include "r_common.h"
#include "r_object_image.h"

#include "r_format_rgb.h"
#include "r_uyvy_rgb_format.cv.h"

#include "generic_api_memory.h"
#include "r_atmlib_prot.h"
#include "r_extatm_ocv.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define  FORMAT_PRV_CHANNEL_UYVY_IN              (0u) /*!< Channel for UYVY input */
#define  FORMAT_PRV_CHANNEL_UYVA_OUT             (1u) /*!< Channel for UYVA output */
#define  FORMAT_PRV_CHANNEL_RGB_PLANAR_OUT       (2u) /*!< Channel for YUV444 planar output */
#define  FORMAT_PRV_CHANNEL_Y16_OUT              (3u) /*!< Channel for the 16 bit greyscale output */

#define  FORMAT_PRV_UYVY_DATA_PLANE              (0u) /*!< CVe program plane index for uyvy image */
#define  FORMAT_PRV_UYVA_DATA_PLANE              (1u) /*!< CVe program plane index for uyva image */
#define  FORMAT_PRV_PLANAR_R_DATA_PLANE          (2u) /*!< CVe program plane index for planar Y data plane*/
#define  FORMAT_PRV_PLANAR_G_DATA_PLANE          (3u) /*!< CVe program plane index for planar U data plane */
#define  FORMAT_PRV_PLANAR_B_DATA_PLANE          (4u) /*!< CVe program plane index for planar V data plane */
#define  FORMAT_PRV_Y16_DATA_PLANE               (5u) /*!< CVe program plane index for 16bit Y data plane */
#define  FORMAT_PRV_NUM_DATA_PLANES              (6u) /*!< Total number of CVe program data planes */

#define  FORMAT_PRV_MAX_CL_SIZE                  (256u * 4u) /*!< Max size of the command list */

#define  FORMAT_PRV_DEFAULT_IMAGE_HEIGHT         (800u)  /*!< Default height of the image */
#define  FORMAT_PRV_DEFAULT_IMAGE_WIDTH          (1280u) /*!< Default width of the image */

#define  FORMAT_PRV_DEFAULT_START_CORE           (0u) /*!< Default core to start with */
#define  FORMAT_PRV_DEFAULT_END_CORE             (4u) /*!< Default core to end with */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Data needed for CVe program execution */
typedef struct {
    R_ATMLIB_CLData          cl_data;                                 /*!< This is command list data */
    R_ATMLIB_OCVPlaneParam   plane_param[FORMAT_PRV_NUM_DATA_PLANES]; /*!< Plane param array. One param for each data plane */
    R_ATMLIB_OCVUniformParam uniform_param;                           /*!< Uniform param */
    uint32_t *               p_prog_mem;                              /*!< Pointer to program memory */
} st_cve_data_t;

/*! Configuration structure for format_yuv module */
typedef struct {
    uint32_t   height;           /*!< Height of image */
    uint32_t   width;            /*!< Width of image */
    uint32_t   start_core;       /*!< Which core to start with */
    uint32_t   end_core;         /*!< Which core to end with */
} st_config_t;

/*! Private data structure for format_yuv module */
typedef struct st_priv_tag {
    st_config_t     config;               /*!< Configuration structure */
    st_cve_data_t   cve[R_MAX_CVE_CORES]; /*!< CVe program data */
} st_priv_t; /*lint !e9109 !e761 */

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_instance);
static r_result_t R_Deinit(st_module_instance_t * const p_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_instance);

/*! Module interface definition for format_yuv */
const st_module_interface_t g_module_interface_format_rgb = {
    .module_name   = "format_rgb",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = R_Deinit,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/***********************************************************************************************************************
 Local Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   register_plane
 * @brief           Adds the data plane to the command list data.
 * @param [in,out]  p_cl_data - pointer to the command list
 * @param [in]      p_plane - Pointer to the image plane
 * @param [in]      format - Format of the image
 * @param [in]      stride - Stride of the image
 * @param [in]      plane_index - Plane index of the image
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t register_plane(R_ATMLIB_CLData *p_cl_data, R_ATMLIB_OCVPlaneParam * p_plane, R_ATMLIB_IMAGE_FORMAT format, uint32_t stride, uint8_t plane_index)
{
    r_result_t  result = R_RESULT_SUCCESS;

    p_plane->img_type   = format;
    p_plane->img_addr   = 0; //To be filled out later
    p_plane->img_stride = stride;

    if (r_extatm_OCV_RegisterPlane(p_cl_data, p_plane, plane_index) != 0)
    {
        R_PRINT_ERROR("Failed to register plane %u", plane_index);
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   init_cl
 * @brief           Initializes the command list
 * @param [in]      p_data - pointer to the private data
 * @param [in]      core_index - Index of the CVe core to initialize
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_cl(st_priv_t * p_data, uint32_t core_index)
{
    r_result_t result   = R_RESULT_SUCCESS;
    uint32_t y_loops    = 0;
    uint32_t y_offset   = 0;
    uint32_t * p_cl_mem = NULL;

    /* CVe program memory regions */
    uint8_t program[CVE_size_of_r_uyvy_rgb_format_cv_text]    = {CVE_r_uyvy_rgb_format_cv_text};
	uint8_t uniform[CVE_size_of_r_uyvy_rgb_format_cv_uniform] = {CVE_r_uyvy_rgb_format_cv_uniform};

    /* CVe parameter structs */
    R_EXTATM_OCVMasterSlaveParam master_slave_param = {R_EXTATM_OCV_MASTER_SLAVE_ERROR, 0};
    R_EXTATM_OCVProgram          program_param      = {{0}};
    R_EXTATM_OCVFunctionParam    function_param     = {0};
    R_ATMLIB_OCVRectParam        rect_param         = {0};

    if (NULL == p_data)
    {
        R_PRINT_ERROR("NULL p_data");
        result = R_RESULT_FAILED;
    }

    /* Update some variables */
    if (R_RESULT_SUCCESS == result)
    {
        /* Calculate y loops and offset for each core */
        y_loops  = p_data->config.height / ((p_data->config.end_core - p_data->config.start_core) + 1u);
        y_offset = y_loops * (core_index - p_data->config.start_core);

        /* Update y_loops if lines don't divide evenly into cores */
        if (p_data->config.end_core == core_index)
        {
            y_loops += (p_data->config.height % ((p_data->config.end_core - p_data->config.start_core) + 1u));
        }
    }

    /* Get program and command list memory */
    if (R_RESULT_SUCCESS == result)
    {
        p_data->cve[core_index].p_prog_mem = gf_Memalign(64, CVE_size_of_r_uyvy_rgb_format_cv_text);
        p_cl_mem = gf_Memalign(64, FORMAT_PRV_MAX_CL_SIZE);

        /* Copy program to allocated memory for the C code: array of instructions */
        if ((NULL != p_data->cve[core_index].p_prog_mem) || (NULL != p_cl_mem))
        {
            R_COMMON_Memcpy(p_data->cve[core_index].p_prog_mem, program, sizeof(program));
            R_COMMON_Memset(p_cl_mem, 0, FORMAT_PRV_MAX_CL_SIZE);
        }
        else
        {
            R_PRINT_ERROR("Failed to get memory");
            result = R_RESULT_FAILED;
        }
    }

    /* Initialize command list */
    if (R_RESULT_SUCCESS == result)
    {
        if (r_atmlib_InitializeOCVCL(&p_data->cve[core_index].cl_data, p_cl_mem, FORMAT_PRV_MAX_CL_SIZE >> 2) != R_ATMLIB_E_OK)
        {
            R_PRINT_ERROR("Failed to initialize command list");
            result = R_RESULT_FAILED;
        }
    }

    /* Program physical address */
    if (R_RESULT_SUCCESS == result)
    {
        program_param.phy_addr.master = (uint32_t)gf_GetPhysAddr(p_data->cve[core_index].p_prog_mem);
        if (r_extatm_OCV_SetProgramAddress(&p_data->cve[core_index].cl_data, &program_param) != 0)
        {
            R_PRINT_ERROR("Failed to set program address");
            result = R_RESULT_FAILED;
        }
    }

	/* Uniform configuration*/
    if (R_RESULT_SUCCESS == result)
    {
        p_data->cve[core_index].uniform_param.size = (uint32_t)CVE_size_of_r_uyvy_rgb_format_cv_uniform >> 2;
        p_data->cve[core_index].uniform_param.data = ((uint32_t*)uniform); /*lint !e2445 */
        if (r_extatm_OCV_SetUniform(&p_data->cve[core_index].cl_data, &p_data->cve[core_index].uniform_param) != 0)
        {
            R_PRINT_ERROR("Failed to set uniform address");
            result = R_RESULT_FAILED;
        }
    }

    /* Set master status */
    if (R_RESULT_SUCCESS == result)
    {
        master_slave_param.mode = R_EXTATM_OCV_ALL_MASTERS;
        if (r_extatm_OCV_SetMasterSlaveMode(&p_data->cve[core_index].cl_data, &master_slave_param) != 0)
        {
            R_PRINT_ERROR("Failed to set master status");
            result = R_RESULT_FAILED;
        }
    }

    /* Register planes */
    if (R_RESULT_SUCCESS == result)
    {
        result |= register_plane(&p_data->cve[core_index].cl_data, &p_data->cve[core_index].plane_param[FORMAT_PRV_UYVY_DATA_PLANE],
                R_ATMLIB_IMG_8U, p_data->config.width *  R_IMAGE_BYTES_PP_UYVY_INTERLEAVED,    FORMAT_PRV_UYVY_DATA_PLANE);
        result |= register_plane(&p_data->cve[core_index].cl_data, &p_data->cve[core_index].plane_param[FORMAT_PRV_UYVA_DATA_PLANE],
                R_ATMLIB_IMG_8U, p_data->config.width *  R_IMAGE_BYTES_PP_UYVA_INTERLEAVED,    FORMAT_PRV_UYVA_DATA_PLANE);
        result |= register_plane(&p_data->cve[core_index].cl_data, &p_data->cve[core_index].plane_param[FORMAT_PRV_PLANAR_R_DATA_PLANE],
                R_ATMLIB_IMG_8U, p_data->config.width *  R_IMAGE_BYTES_PP_YUV444_FULLY_PLANAR, FORMAT_PRV_PLANAR_R_DATA_PLANE);
        result |= register_plane(&p_data->cve[core_index].cl_data, &p_data->cve[core_index].plane_param[FORMAT_PRV_PLANAR_G_DATA_PLANE],
                R_ATMLIB_IMG_8U, p_data->config.width *  R_IMAGE_BYTES_PP_YUV444_FULLY_PLANAR, FORMAT_PRV_PLANAR_G_DATA_PLANE);
        result |= register_plane(&p_data->cve[core_index].cl_data, &p_data->cve[core_index].plane_param[FORMAT_PRV_PLANAR_B_DATA_PLANE],
                R_ATMLIB_IMG_8U, p_data->config.width *  R_IMAGE_BYTES_PP_YUV444_FULLY_PLANAR, FORMAT_PRV_PLANAR_B_DATA_PLANE);
        result |= register_plane(&p_data->cve[core_index].cl_data, &p_data->cve[core_index].plane_param[FORMAT_PRV_Y16_DATA_PLANE],
                R_ATMLIB_IMG_16S, p_data->config.width * R_IMAGE_BYTES_PP_GRAYSCALE_16BIT,     FORMAT_PRV_Y16_DATA_PLANE);
    }

    /* Function offset set*/
    if (R_RESULT_SUCCESS == result)
    {
        function_param.pc_master = CVE_entry_master_r_uyvy_rgb_format_cv__main;
        function_param.pc_slave  = 0;
        if (r_extatm_OCV_SetFunctionOffsets(&p_data->cve[core_index].cl_data, &function_param) != 0)
        {
            R_PRINT_ERROR("Failed to set function offsets");
            result = R_RESULT_FAILED;
        }
    }

    /* Add rect command */
    if (R_RESULT_SUCCESS == result)
    {
        rect_param.dx1  = 4;                                   // Increase x by 4 each loop (because we consume 4 bytes of input image each loop)
        rect_param.dy1  = 1;                                   // Increase y by 1 each loop
        rect_param.dx2  = 0;                                   // Usually you leave this at 0...
        rect_param.dy2  = 0;                                   // Usually you leave this at 0...
        rect_param.xlen = (uint16_t)p_data->config.width / 2u; // Divide width by 2 because we consume 2 pixels at a time
        rect_param.ylen = (uint16_t)y_loops;                   // This is how many y channels we do per core
        rect_param.xs   = (int16_t)0;                          // Start x at 0
        rect_param.ys   = (int16_t)y_offset;                   // Start y at offset based on core

        if (r_atmlib_OCV_RECT(&p_data->cve[core_index].cl_data, R_ATMLIB_DISABLE, 0, 0, &rect_param) != R_ATMLIB_E_OK)
        {
            R_PRINT_ERROR("Failed to add rect command");
            result = R_RESULT_FAILED;
        }
    }

    /* Add sync */
    if (R_RESULT_SUCCESS == result)
    {
        if (r_atmlib_OCV_SYNCS(&p_data->cve[core_index].cl_data, R_ATMLIB_OCVSYNCS_OCVCORE) != R_ATMLIB_E_OK)
        {
            R_PRINT_ERROR("Failed to sync command list");
            result = R_RESULT_FAILED;
        }
    }

    /* Finalize */
    if (R_RESULT_SUCCESS == result)
    {
        if (r_atmlib_FinalizeOCVCL(&p_data->cve[core_index].cl_data) != R_ATMLIB_E_OK)
        {
            R_PRINT_ERROR("Failed to finalize command list");
            result = R_RESULT_FAILED;
        }
    }

    /* Flush Cache of command list and program */
    if (R_RESULT_SUCCESS == result)
    {
        gf_DCacheFlushRange((void*)p_data->cve[core_index].cl_data.top_addr, FORMAT_PRV_MAX_CL_SIZE);
        gf_DCacheFlushRange((void*)p_data->cve[core_index].p_prog_mem,       CVE_size_of_r_uyvy_rgb_format_cv_text);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   deinit_cl
 * @brief           Deinitializes the command list
 * @param [in]      p_data - pointer to the private data
 * @param [in]      core_index - Index of the CVe core to initialize
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t deinit_cl(st_priv_t * p_data, uint32_t core_index)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NULL != p_data)
    {
        gf_Free((void *)p_data->cve[core_index].cl_data.top_addr);
        gf_Free((void *)p_data->cve[core_index].p_prog_mem);
    }
    else
    {
        R_PRINT_ERROR("NULL p_data");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   process_cve
 * @brief           Updates the CVe command list with the new data planes, then executes the command list and waits
 *                  for data processing to be completed.
 * @param [in]      p_instance - pointer to the module instance
 * @param [in]      p_object_uyvy - Pointer to the uyvy input object
 * @param [in]      p_object_uyva - Pointer to the uyva output object
 * @param [in]      p_object_rgb_planar - Pointer to the planar yuv output object
 * @param [in]      p_object_y16 - Pointer to the 16 bit y output object
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t process_cve(st_module_instance_t * const p_instance, st_object_t * p_object_uyvy, st_object_t * p_object_uyva,
                                st_object_t * p_object_rgb_planar, st_object_t * p_object_y16)
{
    r_result_t        result     = R_RESULT_SUCCESS;
    st_priv_t *       p_data     = R_MODULE_GetPrivDataPtr(p_instance);
    uint32_t          core_index = 0;
    RCvIMPDRVCOREINFO core_info  = {0};

    /* Image planes */
    void * p_uyvy     = NULL;
    void * p_uyva     = NULL;
    void * p_r_planar = NULL;
    void * p_g_planar = NULL;
    void * p_b_planar = NULL;
    void * p_y16      = NULL;

    /* Check private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("No private data available");
        result = R_RESULT_FAILED;
    }

    /* Get data planes  */
    if (R_RESULT_SUCCESS == result)
    {
        p_uyvy      = R_OBJECT_GetImagePlaneCPU(p_object_uyvy);
        p_uyva      = R_OBJECT_GetImagePlaneCPU(p_object_uyva);
        p_r_planar  = R_OBJECT_GetIndexedImagePlaneCPU(p_object_rgb_planar, R_IMAGE_YUV444_PLANE_INDEX_Y);
        p_g_planar  = R_OBJECT_GetIndexedImagePlaneCPU(p_object_rgb_planar, R_IMAGE_YUV444_PLANE_INDEX_U);
        p_b_planar  = R_OBJECT_GetIndexedImagePlaneCPU(p_object_rgb_planar, R_IMAGE_YUV444_PLANE_INDEX_V);
        p_y16       = R_OBJECT_GetImagePlaneCPU(p_object_y16);

        if ((NULL == p_uyvy) || (NULL == p_uyva) || (NULL == p_r_planar) || (NULL == p_g_planar) ||
            (NULL == p_b_planar) || (NULL == p_y16))
        {
            R_PRINT_ERROR("Error getting image planes");
            result = R_RESULT_FAILED;
        }
    }

    /* Start CVe program */
    if (R_RESULT_SUCCESS == result)
    {
        /* Flush cache of all data planes */
        R_TIMER_LogMessage(p_instance, "Flushing cache of data planes");
        gf_DCacheFlushRange(p_uyvy,     (size_t)(R_IMAGE_BYTES_PP_UYVY_INTERLEAVED    * p_data->config.width * p_data->config.height));
        gf_DCacheFlushRange(p_uyva,     (size_t)(R_IMAGE_BYTES_PP_UYVA_INTERLEAVED    * p_data->config.width * p_data->config.height));
        gf_DCacheFlushRange(p_r_planar, (size_t)(R_IMAGE_BYTES_PP_YUV444_FULLY_PLANAR * p_data->config.width * p_data->config.height));
        gf_DCacheFlushRange(p_g_planar, (size_t)(R_IMAGE_BYTES_PP_YUV444_FULLY_PLANAR * p_data->config.width * p_data->config.height));
        gf_DCacheFlushRange(p_b_planar, (size_t)(R_IMAGE_BYTES_PP_YUV444_FULLY_PLANAR * p_data->config.width * p_data->config.height));
        gf_DCacheFlushRange(p_y16,      (size_t)(R_IMAGE_BYTES_PP_GRAYSCALE_16BIT     * p_data->config.width * p_data->config.height));

        /* Update image plane address in command lists */
        for (core_index = p_data->config.start_core; core_index <= p_data->config.end_core; core_index++)
        {
            p_data->cve[core_index].cl_data.top_addr[p_data->cve[core_index].plane_param[FORMAT_PRV_UYVY_DATA_PLANE].clofs_img_addr]     = (uint32_t)gf_GetPhysAddr(p_uyvy);
            p_data->cve[core_index].cl_data.top_addr[p_data->cve[core_index].plane_param[FORMAT_PRV_UYVA_DATA_PLANE].clofs_img_addr]     = (uint32_t)gf_GetPhysAddr(p_uyva);
            p_data->cve[core_index].cl_data.top_addr[p_data->cve[core_index].plane_param[FORMAT_PRV_PLANAR_R_DATA_PLANE].clofs_img_addr] = (uint32_t)gf_GetPhysAddr(p_r_planar);
            p_data->cve[core_index].cl_data.top_addr[p_data->cve[core_index].plane_param[FORMAT_PRV_PLANAR_G_DATA_PLANE].clofs_img_addr] = (uint32_t)gf_GetPhysAddr(p_g_planar);
            p_data->cve[core_index].cl_data.top_addr[p_data->cve[core_index].plane_param[FORMAT_PRV_PLANAR_B_DATA_PLANE].clofs_img_addr] = (uint32_t)gf_GetPhysAddr(p_b_planar);
            p_data->cve[core_index].cl_data.top_addr[p_data->cve[core_index].plane_param[FORMAT_PRV_Y16_DATA_PLANE].clofs_img_addr]      = (uint32_t)gf_GetPhysAddr(p_y16);

            /* Flush cache of command list */
            gf_DCacheFlushRange((void *)(p_data->cve[core_index].cl_data.top_addr), FORMAT_PRV_MAX_CL_SIZE);
        }

        /* Execute CVe program */
        R_TIMER_LogMessage(p_instance, "Executing CVe program");
        for (core_index = p_data->config.start_core; core_index <= p_data->config.end_core; core_index++)
        {
            core_info.CoreNum  = core_index;
            core_info.CoreType = RCVDRV_CORE_TYPE_OCV;

            if (R_COMMON_HW_ExecuteIMP((uint32_t)gf_GetPhysAddr((void *)p_data->cve[core_index].cl_data.top_addr), &core_info) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Error executing command list for core %u", core_info.CoreNum);
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Wait for CVe program to finish operation */
    if (R_RESULT_SUCCESS == result)
    {
        /* Wait for all cores */
        R_TIMER_LogMessage(p_instance, "Waiting for result");
        for (core_index = p_data->config.start_core; core_index <= p_data->config.end_core; core_index++)
        {
            core_info.CoreNum  = core_index;
            core_info.CoreType = RCVDRV_CORE_TYPE_OCV;

            if (R_COMMON_HW_WaitIMP(&core_info) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Error waiting for core %u", core_info.CoreNum);
                result = R_RESULT_FAILED;
            }
        }

        /* Invalidate cache of output planes */
        R_TIMER_LogMessage(p_instance, "Invalidating cache of output planes");
        gf_DCacheInvalidateRange(p_uyva,     (size_t)(R_IMAGE_BYTES_PP_UYVA_INTERLEAVED    * p_data->config.width * p_data->config.height));
        gf_DCacheInvalidateRange(p_r_planar, (size_t)(R_IMAGE_BYTES_PP_YUV444_FULLY_PLANAR * p_data->config.width * p_data->config.height));
        gf_DCacheInvalidateRange(p_g_planar, (size_t)(R_IMAGE_BYTES_PP_YUV444_FULLY_PLANAR * p_data->config.width * p_data->config.height));
        gf_DCacheInvalidateRange(p_b_planar, (size_t)(R_IMAGE_BYTES_PP_YUV444_FULLY_PLANAR * p_data->config.width * p_data->config.height));
        gf_DCacheInvalidateRange(p_y16,      (size_t)(R_IMAGE_BYTES_PP_GRAYSCALE_16BIT     * p_data->config.width * p_data->config.height));
    }

    return result;
}

/******************************************************************************************************************//**
 * @page    PQS_Format_Yuv_Config  PQS Format YUV Config
 * @ingroup PQS_Format_Yuv
 * # Configuration Table:
 *
 *   Config Name    | Config Type      | Default | Range                              | Description
 *   -------------- | ---------------- | ------- | ---------------------------------- | ------------------
 *   height         | uint32_t         | 800     | 1 - @ref R_OBJECT_IMAGE_MAX_HEIGHT | Height of the input image
 *   width          | uint32_t         | 1280    | 2 - @ref R_OBJECT_IMAGE_MAX_WIDTH  | Width of the input image
 *   start_core     | uint32_t         | 0       | 0 - (@ref R_MAX_CVE_CORES - 1)     | Starting core to use for CVe program
 *   end_core       | uint32_t         | 4       | 0 - (@ref R_MAX_CVE_CORES - 1)     | Ending core to use for CVe program. Cannot be less than start_core.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_config
 * @brief           Initializes the configuration structure with either the default values or with values from a config
 *                  file if available.
 * @param [in,out]  p_config - pointer to the configuration structure
 * @param [in]      p_instance_name - String of the module instance name
 * @param [in]      p_module_name - String of the module name
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_config(st_config_t * p_config, char const * const p_instance_name, char const * const p_module_name)
{
    r_result_t result = R_RESULT_SUCCESS;

    (void)p_module_name; //Not currently used

    /* Init config */
    p_config->height     = R_COMMON_CONFIG_GetUint32(p_instance_name, "height",     FORMAT_PRV_DEFAULT_IMAGE_HEIGHT);
    p_config->width      = R_COMMON_CONFIG_GetUint32(p_instance_name, "width",      FORMAT_PRV_DEFAULT_IMAGE_WIDTH);
    p_config->start_core = R_COMMON_CONFIG_GetUint32(p_instance_name, "start_core", FORMAT_PRV_DEFAULT_START_CORE);
    p_config->end_core   = R_COMMON_CONFIG_GetUint32(p_instance_name, "end_core",   FORMAT_PRV_DEFAULT_END_CORE);

    /* Check config */
    if (p_config->start_core > p_config->end_core)
    {
        R_PRINT_ERROR("Staring CVE core cannot be greater than ending CVe core.");
        result = R_RESULT_FAILED;
    }

    if (R_MAX_CVE_CORES <= p_config->end_core)
    {
        R_PRINT_ERROR("End CVE core cannot be greater than R_MAX_CVE_CORES");
        result = R_RESULT_FAILED;
    }

    if (p_config->height > R_OBJECT_IMAGE_MAX_HEIGHT)
    {
        R_PRINT_ERROR("Height must be less than R_OBJECT_IMAGE_MAX_HEIGHT");
        result = R_RESULT_FAILED;
    }

    if (0u == p_config->height )
    {
        R_PRINT_ERROR("Height cannot be zero!");
        result = R_RESULT_FAILED;
    }

    if (p_config->width > R_OBJECT_IMAGE_MAX_WIDTH)
    {
        R_PRINT_ERROR("Width must be less than R_OBJECT_IMAGE_MAX_WIDTH");
        result = R_RESULT_FAILED;
    }

    if (2u > p_config->width )
    {
        R_PRINT_ERROR("Width cannot be less than two!");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   init_priv
 * @brief           Initializes the private data structure
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_priv(st_module_instance_t * const p_instance)
{
    r_result_t result                  = R_RESULT_SUCCESS;
    char const * const p_instance_name = R_MODULE_GetInstanceName(p_instance);
    char const * const p_module_name   = R_MODULE_GetModuleName(p_instance);
    st_priv_t * p_data                 = R_MODULE_GetPrivDataPtr(p_instance);

    /* Check private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("No private data available");
        result = R_RESULT_FAILED;
    }

    /* Init configuration */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_config(&p_data->config, p_instance_name, p_module_name) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Configuration init has failed");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/***********************************************************************************************************************
 Module Interface Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    r_result_t result  = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Init private data */
    if (R_RESULT_SUCCESS == result)
    {
        result = init_priv(p_instance);
    }

    /* Init command lists for all cores */
    if (R_RESULT_SUCCESS == result)
    {
        for (uint32_t core_index = p_data->config.start_core; core_index <= p_data->config.end_core; core_index++)
        {
            result |= init_cl(p_data, core_index);
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Format_Yuv_Channels PQS Format YUV Channels
 * @ingroup    PQS_Format_Yuv
 * # Channel Table:
 *
 *  Channel name   | Channel Type | Object Type  | Description
 *  -------------- | ------------ | ------------ | ----------------------------------
 *  uyvy_in        | input        | image        | @ref PQS_Format_Yuv_Channel_uyvy_in
 *  uyva_out       | output       | image        | @ref PQS_Format_Yuv_Channel_uyva_out
 *  rgb_planar_out | output       | image        | @ref PQS_Format_Yuv_Channel_rgb_planar_out
 *  y16_out        | output       | image        | @ref PQS_Format_Yuv_Channel_y16_out
 *
 * @anchor PQS_Format_Yuv_Channel_uyvy_in
 * ## PQS Format YUV Channel UYVY In
 *             UYVY image of the configured height and width. Format is R_IMAGE_FORMAT_UYVY_INTERLEAVED. Bytes
 *             per pixel is 2 and image plane depth is 1. This type of image comes from a camera.
 *
 * @anchor PQS_Format_Yuv_Channel_uyva_out
 * ## PQS Format YUV Channel UYVA Out
 *             UYVA image of the configured height and width. Format is R_IMAGE_FORMAT_UYVA_INTERLEAVED. Bytes
 *             per pixel is 4 and image plane depth is 1. This type of image can be viewed on an HDMI display.
 *
 * @anchor PQS_Format_Yuv_Channel_rgb_planar_out
 * ## PQS Format YUV Channel YUV Fully Planar Out
 *             YUV444 planar image of the configured height and width. Format is R_IMAGE_FORMAT_YUV444_FULLY_PLANAR. Bytes
 *             per pixel is 1 per plane and image plane depth is 3. This type of image can be the input for a CNN.
 *
 * @anchor PQS_Format_Yuv_Channel_y16_out
 * ## PQS Format YUV Channel Y 16 out
 *             16 bit Greyscale image of the configured height and width. Format is R_IMAGE_FORMAT_GRAYSCALE_16BIT. Bytes
 *             per pixel is 2 per plane and image plane depth is 1. This type of image can be used by the vision IP.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_BROKER_InitChannelInfo(p_instance, FORMAT_PRV_CHANNEL_UYVY_IN,        "uyvy_in",        R_OBJECT_TYPE_IMAGE, R_BROKER_DATA_DIRECTION_IN);
    result |= R_BROKER_InitChannelInfo(p_instance, FORMAT_PRV_CHANNEL_UYVA_OUT,       "uyva_out",       R_OBJECT_TYPE_IMAGE, R_BROKER_DATA_DIRECTION_OUT);
    result |= R_BROKER_InitChannelInfo(p_instance, FORMAT_PRV_CHANNEL_RGB_PLANAR_OUT, "rgb_planar_out", R_OBJECT_TYPE_IMAGE, R_BROKER_DATA_DIRECTION_OUT);
    result |= R_BROKER_InitChannelInfo(p_instance, FORMAT_PRV_CHANNEL_Y16_OUT,        "y16_out",        R_OBJECT_TYPE_IMAGE, R_BROKER_DATA_DIRECTION_OUT);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_Deinit
 * @brief           Deinitializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Deinit(st_module_instance_t * const p_instance)
{
    r_result_t result  = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Init command lists for all cores */
    if (R_RESULT_SUCCESS == result)
    {
        for (uint32_t core_index = p_data->config.start_core; core_index <= p_data->config.end_core; core_index++)
        {
            result |= deinit_cl(p_data, core_index);
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    r_result_t result                     = R_RESULT_SUCCESS;
    st_priv_t * p_data                    = R_MODULE_GetPrivDataPtr(p_instance);
    st_object_t * p_object_uyvy_in        = R_BROKER_GetObjectPtr(p_instance, FORMAT_PRV_CHANNEL_UYVY_IN);
    st_object_t * p_object_uyva_out       = R_BROKER_GetObjectPtr(p_instance, FORMAT_PRV_CHANNEL_UYVA_OUT);
    st_object_t * p_object_rgb_planar_out = R_BROKER_GetObjectPtr(p_instance, FORMAT_PRV_CHANNEL_RGB_PLANAR_OUT);
    st_object_t * p_object_y16_out        = R_BROKER_GetObjectPtr(p_instance, FORMAT_PRV_CHANNEL_Y16_OUT);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Check object types */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CheckObjectType(p_object_uyvy_in,        R_OBJECT_TYPE_IMAGE);
        result |= R_OBJECT_CheckObjectType(p_object_uyva_out,       R_OBJECT_TYPE_IMAGE);
        result |= R_OBJECT_CheckObjectType(p_object_rgb_planar_out, R_OBJECT_TYPE_IMAGE);
        result |= R_OBJECT_CheckObjectType(p_object_y16_out,        R_OBJECT_TYPE_IMAGE);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Invalid object input");
            result = R_RESULT_FAILED;
        }
    }

    /* Check input */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CheckImageWidth(        p_object_uyvy_in, p_data->config.width);
        result |= R_OBJECT_CheckImageHeight(       p_object_uyvy_in, p_data->config.height);
        result |= R_OBJECT_CheckImageDepth(        p_object_uyvy_in, 1);
        result |= R_OBJECT_CheckImageBytesPerPixel(p_object_uyvy_in, R_IMAGE_BYTES_PP_UYVY_INTERLEAVED);
        result |= R_OBJECT_CheckImageFormat(       p_object_uyvy_in, R_IMAGE_FORMAT_UYVY_INTERLEAVED);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Invalid image: Expected %u x %u depth=%d, bpp=%d, format=%d. Got %d x %d depth=%d, bpp=%d, format=%d",
                    p_data->config.width,
                    p_data->config.height,
                    1,
                    R_IMAGE_BYTES_PP_UYVY_INTERLEAVED,
                    R_IMAGE_FORMAT_UYVY_INTERLEAVED,
                    R_OBJECT_GetImageWidth(        p_object_uyvy_in),
                    R_OBJECT_GetImageHeight(       p_object_uyvy_in),
                    R_OBJECT_GetImageDepth(        p_object_uyvy_in),
                    R_OBJECT_GetImageBytesPerPixel(p_object_uyvy_in),
                    R_OBJECT_GetImageFormat(       p_object_uyvy_in));
        }
    }

    /* Fill uyva object */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CopyHeader(           p_object_uyva_out, p_object_uyvy_in);
        result |= R_OBJECT_SetImageHeight(       p_object_uyva_out, R_OBJECT_GetImageHeight(p_object_uyvy_in));
        result |= R_OBJECT_SetImageWidth(        p_object_uyva_out, R_OBJECT_GetImageWidth(p_object_uyvy_in));
        result |= R_OBJECT_SetImageDepth(        p_object_uyva_out, 1);
        result |= R_OBJECT_SetImageBytesPerPixel(p_object_uyva_out, R_IMAGE_BYTES_PP_UYVA_INTERLEAVED);
        result |= R_OBJECT_SetImageFormat(       p_object_uyva_out, R_IMAGE_FORMAT_UYVA_INTERLEAVED);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Couldn't set uyva output params");
        }
    }

    /* Fill yuv fully planar object */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CopyHeader(           p_object_rgb_planar_out, p_object_uyvy_in);
        result |= R_OBJECT_SetImageHeight(       p_object_rgb_planar_out, R_OBJECT_GetImageHeight(p_object_uyvy_in));
        result |= R_OBJECT_SetImageWidth(        p_object_rgb_planar_out, R_OBJECT_GetImageWidth(p_object_uyvy_in));
        result |= R_OBJECT_SetImageDepth(        p_object_rgb_planar_out, 3);
        result |= R_OBJECT_SetImageBytesPerPixel(p_object_rgb_planar_out, R_IMAGE_BYTES_PP_YUV444_FULLY_PLANAR);
        result |= R_OBJECT_SetImageFormat(       p_object_rgb_planar_out, R_IMAGE_FORMAT_YUV444_FULLY_PLANAR);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Couldn't set yuv planar output params");
        }
    }

    /* Fill 16bit grayscale object */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CopyHeader(           p_object_y16_out, p_object_uyvy_in);
        result |= R_OBJECT_SetImageHeight(       p_object_y16_out, R_OBJECT_GetImageHeight(p_object_uyvy_in));
        result |= R_OBJECT_SetImageWidth(        p_object_y16_out, R_OBJECT_GetImageWidth(p_object_uyvy_in));
        result |= R_OBJECT_SetImageDepth(        p_object_y16_out, 1);
        result |= R_OBJECT_SetImageBytesPerPixel(p_object_y16_out, R_IMAGE_BYTES_PP_GRAYSCALE_16BIT);
        result |= R_OBJECT_SetImageFormat(       p_object_y16_out, R_IMAGE_FORMAT_GRAYSCALE_16BIT);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Couldn't set y16 output params");
        }
    }

    /* Do image processing */
    if (R_RESULT_SUCCESS == result)
    {
        result = process_cve(p_instance, p_object_uyvy_in, p_object_uyva_out, p_object_rgb_planar_out, p_object_y16_out);
    }

    return result;
}
