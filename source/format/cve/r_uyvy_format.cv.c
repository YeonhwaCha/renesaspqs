/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

/*
 *
 * Input format    (data plane 0): (u, y1, v, y2)
 * Output format 1 (data plane 1): (u, y1, v, a), (u, y2, v, a)
 * Output format 2 (data plane 2): y1, y2
 * Output format 3 (data plane 3): u, u
 * Output format 4 (data plane 4): v, v
 * Output format 5 (data plane 5): y1, y2
 */

#pragma entry main master
void main (void)
{
    unsigned char y1, y2, u, v;

    /* Read in UYVY interleaved camera data from data plane 0 (2 pixels of data) */
    __setSrcImageID(0);
    u  = __getSrc(0, 0);
    y1 = __getSrc(1, 0);
    v  = __getSrc(2, 0);
    y2 = __getSrc(3, 0);

    /* Prepare to write uyva data.  Double the x index because uyva is 2x more data compared to uyvy */
    __setX(__getX()*2);

    /* Select data plane 1 to write UYVA data (used for display purposes) */
    __setDstImageID(1);

    /* Write first uyva pixel */
    __setDst(0, 0, u);
    __setDst(1, 0, y1);   
    __setDst(2, 0, v);   
    __setDst(3, 0, 0);   

    /* Write second uyva pixel */
    __setDst(4, 0, u);
    __setDst(5, 0, y2);
    __setDst(6, 0, v);
    __setDst(7, 0, 0);

    /* Prepare to write planar data.  Divide x index by 4 to convert between UYVA interleaved indexe (as currently set)
     * and Y-U-V Planar index */
    __setX(__getX()/4);

    /* Set output data plane to 2 for Y plane (8-bit for CNNs) */
    __setDstImageID(2);

    /* Write 2 Y pixels */
    __setDst(0, 0, y1);
    __setDst(1, 0, y2);

    /* Set output data plane to 3 for U plane (8-bit for CNNs) */
    __setDstImageID(3);

    /* Write 2 U pixels */
    __setDst(0, 0, u);
    __setDst(1, 0, u);

    /* Set output data plane to 4 for V plane (8-bit for CNNs) */
    __setDstImageID(4);

    /* Write 2 V pixels */
    __setDst(0, 0, v);
    __setDst(1, 0, v);

    /* Set output data plane to 5 for 16-bit Y plane (for DOF/ACF/STV) */
    /* Note: No need to change X index, command list already set up data plane to be 16-bit */
    __setDstImageID(5);

    /* Write 2 Y pixels */
    __setDst(0, 0, y1);
    __setDst(1, 0, y2);

    __trap();
}
