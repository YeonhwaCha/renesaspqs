/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

#define Y_OFFSET            16
#define UV_OFFSET           128

#define CONST_RGB_Y_CONV    1.164
#define CONST_R_V_CONV      1.596
#define CONST_G_U_CONV      -0.813
#define CONST_G_V_CONV      -0.392
#define CONST_B_U_CONV      2.018

/*
 *
 * Image ID 0 : UYVA Channel
 * Image ID 1 : BGRA Channel
 *
 */


#pragma entry main master
void main (void)
{
    unsigned char y, u, v;
    short r, g, b;

    /* Set input plane to data plane 0 for UYVA image */
    __setSrcImageID(0);

    /* Read in y, u, v data */
    u = __getSrc(0, 0);
    y = __getSrc(1, 0);
    v = __getSrc(2, 0);

    /* Calculate Red Channel */
    r = ((y - Y_OFFSET) * CONST_RGB_Y_CONV) + ((v - UV_OFFSET) * CONST_R_V_CONV);
    r = __max(r, 0);
    r = __min(r, 255);

    /* Calculate Green Channel */
    g = ((y - Y_OFFSET) * CONST_RGB_Y_CONV) + ((u - UV_OFFSET) * CONST_G_U_CONV) + ((v - UV_OFFSET) * CONST_G_V_CONV);
    g = __max(g, 0);
    g = __min(g, 255);

    /* Calculate Blue Channel */
    b = ((y - Y_OFFSET) * CONST_RGB_Y_CONV) + ((u - UV_OFFSET) * CONST_B_U_CONV);
    b = __max(b, 0);
    b = __min(b, 255);

    /* Set output plane to data plane 1 for BGRA output to display */
    __setDstImageID(1);

    /* Write colored output */
    __setDst(0, 0, b); //blue
    __setDst(1, 0, g); //green
    __setDst(2, 0, r); //red
    __setDst(3, 0, 0); //alpha

    __trap();
}
