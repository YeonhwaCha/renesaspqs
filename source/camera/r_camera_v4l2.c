/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_camera_v4l2.c
 * @brief         Source file for camera input using V4L2 (Video For Linux 2). See @ref PQS_Camera_V4L2 for more details
 *********************************************************************************************************************/
 /******************************************************************************************************************//**
 * @page       PQS_Camera_V4L2_Algorithm  PQS Camera Algorithm
 * @ingroup    PQS_Camera_V4L2
 * # Algorithm Description:
 *             This module uses the Video For Linux 2 Interface to get a camera image. V4L2 uses the VIN hardware
 *             block to stream a camerafrom over MIPI-CSI2. The true size of the image is fixed by the camera,
 *             but this module can be configured to automatically crop the true image size to a smaller image size.
 * # Block Diagram:
 *             @image html camera_v4l2.png
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include <errno.h>  /* For errno */
#include <fcntl.h>  /* For open */
#include <unistd.h> /* For close */

#include <sys/mman.h>  /* For mmap */
#include <sys/stat.h>  /* For stat */
#include <sys/ioctl.h> /* For ioctl */

#include <linux/videodev2.h> /* For v4l2 */

#include "r_common.h"
#include "r_object_image.h"

#include "r_camera_v4l2.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define CAMERA_PRV_CHANNEL_UYVY_OUT    (0)   /*!< Channel for image output */
#define CAMERA_PRV_CHANNEL_TRIGGER     (1)   /*!< Channel for trigger input */

#define CAMERA_PRV_MAX_FILE_NAME       (128) /*!< max length for camera device name */

#define CAMERA_PRV_NUM_V4L2_BUFFERS    (4)   /*!< Number of V4L2 buffers in pool */

#define CAMERA_PRV_DEFAULT_NAME        "/dev/video0" /*!< Device name */

#define CAMERA_PRV_DEFAULT_FORMAT      (R_IMAGE_FORMAT_UYVY_INTERLEAVED) /*!< Desired output format */

#define CAMERA_PRV_DEFAULT_TRUE_HEIGHT (1080) /*!< True height of the camera */
#define CAMERA_PRV_DEFAULT_TRUE_WIDTH  (1280) /*!< True width of the camera */

#define CAMERA_PRV_DEFAULT_HEIGHT      (800)  /*!< Default height of output image */
#define CAMERA_PRV_DEFAULT_WIDTH       (1280) /*!< Default width of output image */

#define CAMERA_PRV_DEFAULT_CROP_LEFT    (0)   /*!< Default crop parameter. Number of pixels from the left of image to crop. */
#define CAMERA_PRV_DEFAULT_CROP_TOP     (0)   /*!< Default crop parameter. NUmber of pixels from the top of the image to crop*/


#define CAMERA_PRV_DEFAULT_KEEP_FRAME   (1)   /*!< This is a workaround since we cannot control the camera framerate directly.
                                               * 1 means keep every frame, 2 means keep every other frame,
                                               * 3 means keep every third frame and so on */

#define CAMERA_PRV_DEFAULT_TIMEOUT_SEC  (1)   /*!< Desired timeout when waiting for new frame. Total timeout = TIMEOUT_SEC + TIMEOUT_USEC */
#define CAMERA_PRV_DEFAULT_TIMEOUT_USEC (0)   /*!< Desired timeout when waiting for new frame. Total timeout = TIMEOUT_SEC + TIMEOUT_USEC */


#define CAMERA_PRV_DEFAULT_SENSOR_ID    (0)   /*!< Sensor ID for the camera */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Configuration Struct for camera_v4l2 module */
typedef struct {
    char              camera_name[CAMERA_PRV_MAX_FILE_NAME]; /*!< Name of camera device */
    e_image_format_t  format;           /*!< Output format */
    uint32_t          height;           /*!< Output height of image */
    uint32_t          width;            /*!< Output width of image */
    uint32_t          true_height;      /*!< True height of the camera  */
    uint32_t          true_width;       /*!< True width of the camera */
    uint32_t          crop_left;        /*!< Offset from left of image */
    uint32_t          crop_top;         /*!< Offset from top of image */
    uint32_t          keep_frame;       /*!< How many frames we skip */
    uint32_t          timeout_sec;      /*!< Timeout in seconds */
    uint32_t          timeout_usec;     /*!< Timeout in microseconds */
    uint32_t          sensor_id;        /*!< Id of the sensor */
} st_camera_cfg_t;

/*! Private data structure for camera_v4l2 module */
typedef struct st_priv_tag {
    st_camera_cfg_t config;             /*!< Config info for this instance */
    uint32_t        frame_count;        /*!< What number frame is this?  Resets after it hits config.keep_frame_number */
    int             fd;                 /*!< Camera file descriptor */
    st_object_t *   p_obj_buffer[CAMERA_PRV_NUM_V4L2_BUFFERS]; /*!< Buffer for objects */
} st_priv_t;

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_instance);
static r_result_t R_Deinit(st_module_instance_t * const p_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_instance);

/*! Module interface definition for camera_v4l2 */
const st_module_interface_t g_module_interface_camera_v4l2 = {
    .module_name   = "camera_v4l2",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = R_Deinit,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/***********************************************************************************************************************
 Local Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   get_v4l2_format
 * @brief           Returns the v4l2 pixel format of the given e_image_format_t format
 * @param [in]      format - Format of the image
 * @retval          The converted v4l2 format
 *********************************************************************************************************************/
static uint32_t get_v4l2_format(e_image_format_t format)
{
    uint32_t v4l2_format = 0;

    switch (format)
    {
        case R_IMAGE_FORMAT_UYVY_INTERLEAVED:
        {
            v4l2_format = V4L2_PIX_FMT_UYVY;
            break;
        }

        case R_IMAGE_FORMAT_RGB32_INTERLEAVED:
        {
            v4l2_format = V4L2_PIX_FMT_XBGR32;
            break;
        }

        default:
        {
            R_PRINT_ERROR("Fell through switch. Format not supported.");
        }
    }

    return v4l2_format;
}

/******************************************************************************************************************//**
 * Function Name:   get_bytes_per_pixel
 * @brief           Returns the number of bytes per pixel for the given e_image_format_t format
 * @param [in]      format - Format of the image
 * @retval          bytes per pixel of format
 *********************************************************************************************************************/
static uint32_t get_bytes_per_pixel(e_image_format_t format)
{
    uint8_t bytes_per_pixel = 0;

    switch (format)
    {
        case R_IMAGE_FORMAT_UYVY_INTERLEAVED:
        {
            bytes_per_pixel = R_IMAGE_BYTES_PP_UYVY_INTERLEAVED;
            break;
        }

        case R_IMAGE_FORMAT_RGB32_INTERLEAVED:
        {
            bytes_per_pixel = R_IMAGE_BYTES_PP_UYVY_INTERLEAVED;
            break;
        }

        default:
        {
            R_PRINT_ERROR("Fell through switch. Format not supported.");
        }
    }

    return bytes_per_pixel;
}

/******************************************************************************************************************//**
 * Function Name:   r_ioctl
 * @brief           Wrapper for ioctl. Loops.
 * @param [in]      fd - file descriptor
 * @param [in]      request - request code
 * @param [in]      arg - pointer to argument
 * @retval          result of operation
 *********************************************************************************************************************/
static int r_ioctl(int fd, unsigned long request, void *arg)
{
    /* Initialized  locals */
    int return_code = -1;

    do
    {
        return_code = ioctl(fd, request, arg);
    }
    while (((-1) == return_code) && (EINTR == errno));

    return return_code;
}

/******************************************************************************************************************//**
 * @page    PQS_Camera_V4L2_Config  PQS Camera V4L2 Config
 * @ingroup PQS_Camera_V4L2
 * # Configuration Table:
 *
 *   Config Name  | Config Type      | Default                          | Range                                                               | Description
 *   ------------ | ---------------- | -------------------------------- | ------------------------------------------------------------------- | ------------------
 *   camera_name  | string           | "dev/video0"                     | Less than CAMERA_PRV_MAX_FILE_NAME characters                       | Name of the Linux video device
 *   format       | e_image_format_t | R_IMAGE_FORMAT_UYVY_INTERLEAVED  | R_IMAGE_FORMAT_UYVY_INTERLEAVED or R_IMAGE_FORMAT_RGB32_INTERLEAVED | Image format
 *   height       | uint32_t         | 800                              | 1 - @ref R_OBJECT_IMAGE_MAX_HEIGHT                                  | Height of output image.
 *   width        | uint32_t         | 1280                             | 1 - @ref R_OBJECT_IMAGE_MAX_WIDTH                                   | Width of output image.
 *   crop_left    | uint32_t         | 0                                | 0 - (True image width minus configured image width)                 | Number of pixels to crop from the left of the image
 *   crop_top     | uint32_t         | 0                                | 0 - (True image height minus configured image height)               | Number of pixels to crop from the top of the image
 *   true_height  | uint32_t         | 1080                             | 1 - @ref R_OBJECT_IMAGE_MAX_HEIGHT                                  | Actual height of the camera image
 *   true_width   | uint32_t         | 1280                             | 1 - @ref R_OBJECT_IMAGE_MAX_WIDTH                                   | Actual width of the camera image
 *   keep_frame   | uint32_t         | 1                                | Greater than zero                                                   | Workaround since we cannot directly control framerate. 1 means output every frame, 2 means output every other frame, 3 means output every third frame and so on
 *   timeout_sec  | uint32_t         | 1                                | Greater than or equal to zero                                       | Timeout in seconds for waiting for image. Total timeout is timeout_sec + timeout_usec
 *   timeout_usec | uint32_t         | 0                                | Greater than or equal to zero                                       | Timeout in microseconds for waiting for image. Total timeout is timeout_sec + timeout_usec
 *   sensor_id    | uint32_t         | 0                                | Any                                                                 | ID of the sensor to set in object
 *
 * # Camera Cropping:
 *            This module can atomically crop the true image size from the camera to a smaller portion of the original image.
 *            The crop left config plus the width config must be less than the true image width. The crop top config plus
 *            the height config must be less than the true image height. See the below diagram for more details.
 *            @image html camera_v4l2_config.png
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_config
 * @brief           Initializes the configuration structure with either the default values or with values from a config
 *                  file if available.
 * @param [in,out]  p_config - pointer to the configuration structure
 * @param [in]      p_instance_name - String of the module instance name
 * @param [in]      p_module_name - String of the module name
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_config(st_camera_cfg_t * p_config, char const * const p_instance_name, char const * const p_module_name)
{
    r_result_t result = R_RESULT_SUCCESS;
    const char * format_names[] = E_IMAGE_FORMAT_AS_STRING;

    /* Init Config */
    result |= R_COMMON_STRING_Copy(p_config->camera_name, R_COMMON_CONFIG_GetString(p_instance_name, "camera_name", CAMERA_PRV_DEFAULT_NAME), CAMERA_PRV_MAX_FILE_NAME);
    p_config->format       = R_COMMON_CONFIG_GET_ENUM( p_instance_name, "format",       CAMERA_PRV_DEFAULT_FORMAT, format_names, (sizeof(format_names))/(sizeof(format_names[0])), e_image_format_t);
    p_config->height       = R_COMMON_CONFIG_GetUint32(p_instance_name, "height",       CAMERA_PRV_DEFAULT_HEIGHT);
    p_config->width        = R_COMMON_CONFIG_GetUint32(p_instance_name, "width",        CAMERA_PRV_DEFAULT_WIDTH);
    p_config->crop_left    = R_COMMON_CONFIG_GetUint32(p_instance_name, "crop_left",    CAMERA_PRV_DEFAULT_CROP_LEFT);
    p_config->crop_top     = R_COMMON_CONFIG_GetUint32(p_instance_name, "crop_top",     CAMERA_PRV_DEFAULT_CROP_TOP);
    p_config->true_height  = R_COMMON_CONFIG_GetUint32(p_instance_name, "true_height",  CAMERA_PRV_DEFAULT_TRUE_HEIGHT);
    p_config->true_width   = R_COMMON_CONFIG_GetUint32(p_instance_name, "true_width",   CAMERA_PRV_DEFAULT_TRUE_WIDTH);
    p_config->keep_frame   = R_COMMON_CONFIG_GetUint32(p_instance_name, "keep_frame",   CAMERA_PRV_DEFAULT_KEEP_FRAME);
    p_config->timeout_sec  = R_COMMON_CONFIG_GetUint32(p_instance_name, "timeout_sec",  CAMERA_PRV_DEFAULT_TIMEOUT_SEC);
    p_config->timeout_usec = R_COMMON_CONFIG_GetUint32(p_instance_name, "timeout_usec", CAMERA_PRV_DEFAULT_TIMEOUT_USEC);
    p_config->sensor_id    = R_COMMON_CONFIG_GetUint32(p_instance_name, "sensor_id",    CAMERA_PRV_DEFAULT_SENSOR_ID);

    /* Check config */
    if ((R_IMAGE_FORMAT_UYVY_INTERLEAVED != p_config->format) && (R_IMAGE_FORMAT_RGB32_INTERLEAVED != p_config->format))
    {
        R_PRINT_ERROR("Format not supported.");
        result = R_RESULT_FAILED;
    }

    if (p_config->height > R_OBJECT_IMAGE_MAX_HEIGHT)
    {
        R_PRINT_ERROR("Height must be less than R_OBJECT_IMAGE_MAX_HEIGHT");
        result = R_RESULT_FAILED;
    }

    if (p_config->width > R_OBJECT_IMAGE_MAX_WIDTH)
    {
        R_PRINT_ERROR("Width must be less than R_OBJECT_IMAGE_MAX_WIDTH");
        result = R_RESULT_FAILED;
    }

    if (p_config->true_height > R_OBJECT_IMAGE_MAX_HEIGHT)
    {
        R_PRINT_ERROR("Height must be less than R_OBJECT_IMAGE_MAX_HEIGHT");
        result = R_RESULT_FAILED;
    }

    if (p_config->true_width > R_OBJECT_IMAGE_MAX_WIDTH)
    {
        R_PRINT_ERROR("Width must be less than R_OBJECT_IMAGE_MAX_WIDTH");
        result = R_RESULT_FAILED;
    }

    if (0 == p_config->height )
    {
        R_PRINT_ERROR("Height cannot be zero!");
        result = R_RESULT_FAILED;
    }

    if (0 == p_config->true_height )
    {
        R_PRINT_ERROR("Height cannot be zero!");
        result = R_RESULT_FAILED;
    }

    if ((p_config->height + p_config->crop_top) > p_config->true_height)
    {
        R_PRINT_ERROR("Height + top cropping cannot be larger than the true image height");
        result = R_RESULT_FAILED;
    }

    if ((p_config->width + p_config->crop_left) > p_config->true_width)
    {
        R_PRINT_ERROR("Width + left cropping cannot be larger than the true image width");
        result = R_RESULT_FAILED;
    }

    if (0 == p_config->keep_frame )
    {
        R_PRINT_ERROR("keep frame cannot be zero!");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   init_priv
 * @brief           Initializes the private data structure
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_priv(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    char const * const p_instance_name = R_MODULE_GetInstanceName(p_instance);
    char const * const p_module_name   = R_MODULE_GetModuleName(p_instance);
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);
    uint32_t i = 0;

    /* Check private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("No private data available");
        result = R_RESULT_FAILED;
    }

    /* Init configuration */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_config(&p_data->config, p_instance_name, p_module_name) != R_RESULT_SUCCESS)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Configuration init has failed");
        }
    }

    /* Init the rest of the private structure */
    if (R_RESULT_SUCCESS == result)
    {
        p_data->frame_count = 0;
        p_data->fd          = -1;

        for (i = 0; CAMERA_PRV_NUM_V4L2_BUFFERS > i; i++)
        {
            p_data->p_obj_buffer[i] = NULL;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   open_camera
 * @brief           Opens a camera for use
 * @param [in]      p_data - Pointer to the private data
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t open_camera(st_priv_t * const p_data)
{
    /* Initialized  locals */
    r_result_t result = R_RESULT_SUCCESS;
    struct stat status = {0};

    /* Check status of dev/video */
    if (R_RESULT_SUCCESS == result)
    {
        if ((-1) == stat(p_data->config.camera_name, &status))
        {
            R_PRINT_ERROR("Cannot identify device %s", p_data->config.camera_name);
            result = R_RESULT_FAILED;
        }
    }

    /* Check file type */
    if (R_RESULT_SUCCESS == result)
    {
        if (!S_ISCHR(status.st_mode))
        {
            R_PRINT_ERROR("No device %s", p_data->config.camera_name);
            result = R_RESULT_FAILED;
        }
    }

    /* Get and check file descriptor */
    if (R_RESULT_SUCCESS == result)
    {
        p_data->fd = open(p_data->config.camera_name, O_RDWR, 0);

        if ((-1) == p_data->fd)
        {
            R_PRINT_ERROR("Cannot open device %s", p_data->config.camera_name);
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   config_camera
 * @brief           Configures camera per the supplied configuration structure
 * @param [in]      p_data - Pointer to the private data
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t config_camera(st_priv_t * const p_data)
{
    /* Initialized  locals */
    r_result_t result = R_RESULT_SUCCESS;
    struct v4l2_capability cap   = {0};
    struct v4l2_cropcap cropcap  = {0};
    struct v4l2_crop crop        = {0};
    struct v4l2_format format    = {0};

    /* Query device capabilities */
    if (R_RESULT_SUCCESS == result)
    {
        if ((-1) == r_ioctl(p_data->fd, VIDIOC_QUERYCAP, &cap))
        {
            R_PRINT_ERROR("ioctl VIDIOC_QUERYCAP failure using device %s. Error number %d", p_data->config.camera_name, errno);
            result = R_RESULT_FAILED;
        }
    }

    /* Check device capabilities */
    if (R_RESULT_SUCCESS == result)
    {
        if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE))
        {
            R_PRINT_ERROR("Device %s is not a video capture device", p_data->config.camera_name);
            result = R_RESULT_FAILED;
        }

        if (!(cap.capabilities & V4L2_CAP_STREAMING))
        {
            R_PRINT_ERROR("Device %s does not support streaming i/o", p_data->config.camera_name);
            result = R_RESULT_FAILED;
        }
    }

    /* Set cropping */
    if (R_RESULT_SUCCESS == result)
    {
        /* Select video input, video standard and tune here. */
        CLEAR(cropcap);
        cropcap.type = (int)V4L2_BUF_TYPE_VIDEO_CAPTURE;

        if (0 == r_ioctl(p_data->fd, VIDIOC_CROPCAP, &cropcap))
        {
            crop.type     = (int)V4L2_BUF_TYPE_VIDEO_CAPTURE;
            crop.c        = cropcap.defrect; /* reset to default */
            crop.c.left   = p_data->config.crop_left;
            crop.c.top    = p_data->config.crop_top;
            crop.c.width  = p_data->config.width;
            crop.c.height = p_data->config.height;

            if ((-1) == r_ioctl(p_data->fd, VIDIOC_S_CROP, &crop))
            {
                R_PRINT_ERROR("ioctl VIDIOC_S_CROP failure using device %s. Error number %d", p_data->config.camera_name, errno);
                result = R_RESULT_FAILED;
            }
        }
        else
        {
            R_PRINT_ERROR("ioctl VIDIOC_CROPCAP failure using device %s. Error number %d", p_data->config.camera_name, errno);
            result = R_RESULT_FAILED;
        }
    }

#if 0
    /* Set frame rate */
    if (R_RESULT_SUCCESS == result)
    {
        struct v4l2_streamparm stream = {0};

        stream.type = (int)V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if ((-1) == r_ioctl(p_data->fd, VIDIOC_G_PARM, &stream))
        {
            R_PRINT_ERROR("ioctl VIDIOC_G_PARM failure using device %s. Error number %d", p_data->config.camera_name, errno);
            result = R_RESULT_FAILED;
        }

        stream.parm.capture.timeperframe.numerator = 1;
        stream.parm.capture.timeperframe.denominator = CAMERA_V4L2_CFG_FRAMERATE;

        if ((-1) == r_ioctl(p_data->fd, VIDIOC_S_PARM, &stream))
        {
            R_PRINT_ERROR("ioctl VIDIOC_S_PARM failure using device %s. Error number %d", p_data->config.camera_name, errno));
            result = R_RESULT_FAILED;
        }
    }
#endif

    /* Set format */
    if (R_RESULT_SUCCESS == result)
    {
        CLEAR(format);

        format.type                = (int)V4L2_BUF_TYPE_VIDEO_CAPTURE;
        format.fmt.pix.width       = p_data->config.width;
        format.fmt.pix.height      = p_data->config.height;
        format.fmt.pix.field       = (int)V4L2_FIELD_NONE;
        format.fmt.pix.pixelformat = get_v4l2_format(p_data->config.format);

        if ((-1) == r_ioctl(p_data->fd, VIDIOC_S_FMT, &format))
        {
            R_PRINT_ERROR("ioctl VIDIOC_S_FMT failure using device %s. Error number %d", p_data->config.camera_name, errno);
            result = R_RESULT_FAILED;
        }
    }
    return result;
}

/******************************************************************************************************************//**
 * Function Name:   config_v4l2_buffers
 * @brief           Configures v4l2 buffers per the supplied configuration structure
 * @param [in]      p_data - Pointer to the private data
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t config_v4l2_buffers(st_priv_t * const p_data)
{
    /* Initialized  locals */
    r_result_t result = R_RESULT_SUCCESS;
    struct v4l2_requestbuffers request = {0};

    CLEAR(request);

    request.count  = CAMERA_PRV_NUM_V4L2_BUFFERS;
    request.type   = (int)V4L2_BUF_TYPE_VIDEO_CAPTURE;
    request.memory = (int)V4L2_MEMORY_USERPTR;

    if ((-1) == r_ioctl(p_data->fd, VIDIOC_REQBUFS, &request))
    {
        if (EINVAL == errno)
        {
            R_PRINT_ERROR("Device does not support user pointers %s. Error number %d", p_data->config.camera_name, errno);
            result = R_RESULT_FAILED;
        }
        else
        {
            R_PRINT_ERROR("ioctl VIDIOC_REQBUFS failure using device %s. Error number %d", p_data->config.camera_name, errno);
            result = R_RESULT_FAILED;
        }
     }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   queue_buffer
 * @brief           Queue a buffer in the V4L2 array. V4L2 will pull from this queue and write an image into an
 *                  availiable buffer when a new image is ready
 * @param [in]      p_data - Pointer to the private data
 * @param [in]      buffer_index - Index of the buffer to queue
 * @param [in]      p_object - Pointer object who's data plane should be filled in with an image.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t queue_buffer(st_priv_t * const p_data, const uint32_t buffer_index, st_object_t * p_object)
{
    r_result_t result = R_RESULT_SUCCESS;
    struct v4l2_buffer local_v4l2_buffer = {0};

    if (NULL == p_object)
    {
        R_PRINT_ERROR("NULL input");
        result = R_RESULT_FAILED;
    }

    /* Set object */
    if (R_RESULT_SUCCESS == result)
    {
        /* Save in buffer */
        p_data->p_obj_buffer[buffer_index] = p_object;

        /* Set up v4l2 buffer */
        CLEAR(local_v4l2_buffer);
        local_v4l2_buffer.type      = (int)V4L2_BUF_TYPE_VIDEO_CAPTURE;
        local_v4l2_buffer.memory    = (int)V4L2_MEMORY_USERPTR;
        local_v4l2_buffer.index     = buffer_index;
        local_v4l2_buffer.m.userptr = (unsigned long)(R_OBJECT_GetImagePlaneCPU(p_object));
        local_v4l2_buffer.length    = R_OBJECT_IMAGE_MAX_IMAGE_SIZE;

        /* Check to make sure we actually got the pointer */
        if (0 == local_v4l2_buffer.m.userptr)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Didn't get the user pointer");
        }
    }

    /* Queue v4l2 buffer */
    if (R_RESULT_SUCCESS == result)
    {
        if ((-1) == r_ioctl(p_data->fd, VIDIOC_QBUF, &local_v4l2_buffer))
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("ioctl VIDIOC_QBUF failure using device %s. Error number %d", p_data->config.camera_name, errno);
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   init_buffer
 * @brief           Initializes the v4l2 buffers and queue them
 * @param [in]      p_data - Pointer to the private data
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_buffer(st_priv_t * p_data)
{
    r_result_t result = R_RESULT_SUCCESS;
    int32_t i = 0;

    for (i = 0; CAMERA_PRV_NUM_V4L2_BUFFERS > i; i++)
    {
        p_data->p_obj_buffer[i] = NULL;
        result |= queue_buffer(p_data, i, R_OBJECT_GetFreeObject(R_OBJECT_TYPE_IMAGE));
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   wait_for_frame
 * @brief           Blocks until a V4L2 frame is ready
 * @param [in]      p_data - Pointer to the private data
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t wait_for_frame(st_priv_t * p_data)
{
    r_result_t result = R_RESULT_SUCCESS;
    struct timeval time_out = {0};
    fd_set set = {0};
    int select_result = 0;

    FD_ZERO(&set);
    FD_SET(p_data->fd, &set);
    time_out.tv_sec = p_data->config.timeout_sec;
    time_out.tv_usec = p_data->config.timeout_usec;

    select_result = select(p_data->fd+1, &set, NULL, NULL, &time_out);


    if (0 >= select_result)
    {
        switch(select_result)
        {
            case (-1):
            {
                R_PRINT_ERROR("Select error");
                break;
            }

            case 0:
            {
                R_PRINT_ERROR("Select timeout");
                break;
            }

            default:
            {
                R_PRINT_ERROR("Select unknown error");
                break;
            }
        }

        result = R_RESULT_FAILED;
    }
    else
    {
        if (!FD_ISSET(p_data->fd, &set))
        {
            R_PRINT_ERROR("FD_ISSET failed");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   get_frame_buffer_id_and_timestamp
 * @brief           Retrieves data about the incoming V4L2 image
 * @param [in]      p_data - Pointer to the private data
 * @param [out]     p_buffer_index - Buffer index that was filled in by V4L2
 * @param [out]     p_time_stamp_usec - Time stamp of when the image was captured by V4L2
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t get_frame_buffer_id_and_timestamp(st_priv_t * const p_data, uint32_t * const p_buffer_index, uint64_t * const p_time_stamp_usec)
{
    /* Initialized  locals */
    r_result_t result = R_RESULT_SUCCESS;
    struct v4l2_buffer local_v4l2_buffer = {0};

    CLEAR(local_v4l2_buffer);
    local_v4l2_buffer.type   = (int)V4L2_BUF_TYPE_VIDEO_CAPTURE;
    local_v4l2_buffer.memory = (int)V4L2_MEMORY_USERPTR;

    /* Deque buffer */
    if (R_RESULT_SUCCESS == result)
    {
        if ((-1) == r_ioctl(p_data->fd, VIDIOC_DQBUF, &local_v4l2_buffer))
        {
            R_PRINT_ERROR("ioctl VIDIOC_DQBUF failure using device %s. Error number %d", p_data->config.camera_name, errno);
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* Make sure buffer index is valid */
        if (local_v4l2_buffer.index >= CAMERA_PRV_NUM_V4L2_BUFFERS)
        {
            R_PRINT_ERROR("V4L2 index is too high");
            result = R_RESULT_FAILED;
        }
        /* Make sure pointers match */
        else if ((unsigned long)R_OBJECT_GetImagePlaneCPU(p_data->p_obj_buffer[local_v4l2_buffer.index]) != local_v4l2_buffer.m.userptr)
        {
            R_PRINT_ERROR("V4L2 and image object buffer mismatch", p_data->config.camera_name);
            result = R_RESULT_FAILED;
        }
        /* Copy info back to user */
        else
        {
            *p_buffer_index    = local_v4l2_buffer.index;
            *p_time_stamp_usec = (local_v4l2_buffer.timestamp.tv_sec * 1000000) + local_v4l2_buffer.timestamp.tv_usec;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   fill_header
 * @brief           Fills in the object header info with the supplied data.
 * @param [in]      p_data - Pointer to the private data
 * @param [out]     p_object - Object who's header should be set
 * @param [out]     time_stamp_usec_boot_time - Time stamp of when the image was captured
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t fill_header(st_priv_t * const p_data, st_object_t * const p_object, uint64_t time_stamp_usec_boot_time)
{
    r_result_t result = R_RESULT_SUCCESS;
    uint64_t time_stamp_usec = 0;

    /* Convert timestamp to real time */
    /* Note:
     * The time stamp from V4L2 is using the boot timer. We must convert this to the real time from Epoch
     */
    time_stamp_usec = R_COMMON_TIME_ConvertBootToReal(time_stamp_usec_boot_time);

    /* Check return value */
    if (0 == time_stamp_usec)
    {
        R_PRINT_ERROR("V4L2 Time stamp issue using device %s", p_data->config.camera_name);
        result = R_RESULT_FAILED;
    }

    /* Set time stamp and sensor of image object */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_SetSensorId(           p_object, p_data->config.sensor_id);
        result |= R_OBJECT_SetTimeStamp(          p_object, time_stamp_usec);
        result |= R_OBJECT_SetImageHeight(        p_object, p_data->config.height);
        result |= R_OBJECT_SetImageWidth(         p_object, p_data->config.width);
        result |= R_OBJECT_SetImageDepth(         p_object, 1);
        result |= R_OBJECT_SetImageBytesPerPixel( p_object, get_bytes_per_pixel(p_data->config.format));
        result |= R_OBJECT_SetImageFormat(        p_object, p_data->config.format);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Error setting header info");
        }
    }

    return result;
}

/***********************************************************************************************************************
 Module Interface Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = init_priv(p_instance);
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = open_camera(p_data);
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = config_camera(p_data);
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = config_v4l2_buffers(p_data);
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = init_buffer(p_data);
    }

    /* Now start the video stream */
    if (R_RESULT_SUCCESS == result)
    {
        enum v4l2_buf_type local_v4l2_buffer_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if ((-1) == r_ioctl(p_data->fd, VIDIOC_STREAMON, &local_v4l2_buffer_type))
        {
            R_PRINT_ERROR("ioctl VIDIOC_STREAMON failure using device %s. Error number %d", p_data->config.camera_name, errno);
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_Deinit
 * @brief           Deinitializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Deinit(st_module_instance_t * const p_instance)
{
    r_result_t result  = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        if ((0 > p_data->fd) && (0 != close(p_data->fd)))
        {
            /* Error closing file */
            result = R_RESULT_FAILED;
        }
    }

    /* Stop capturing */
    if (R_RESULT_SUCCESS == result)
    {
        enum v4l2_buf_type local_v4l2_buffer_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if ((-1) == r_ioctl(p_data->fd, VIDIOC_STREAMOFF, &local_v4l2_buffer_type))
        {
            R_PRINT_ERROR("ioctl VIDIOC_STREAMON failure using device %s. Error number %d", p_data->config.camera_name, errno);
            result = R_RESULT_FAILED;
        }
    }

    /* Release all buffers */
    if (R_RESULT_SUCCESS == result)
    {
        for (int i = 0; CAMERA_PRV_NUM_V4L2_BUFFERS > i; i++)
        {
            if (NULL != p_data->p_obj_buffer[i])
            {
                result |= R_OBJECT_Lock(p_data->p_obj_buffer[i]);
                result |= R_OBJECT_DecrementRefCount(p_data->p_obj_buffer[i]);
                p_data->p_obj_buffer[i] = NULL;
            }
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Camera_V4L2_Channels PQS Camera V4L2 Channels
 * @ingroup    PQS_Camera_V4L2
 * # Channel Table:
 *
 *  Channel name | Channel Type | Object Type  | Description
 *  ------------ | ------------ | ------------ | ----------------------------------
 *  uyvy_out     | output       | image        | @ref PQS_Camera_V4L2_Channel_uyvy_out
 *  trigger      | trigger      | trigger      | @ref PQS_Camera_V4L2_Channel_trigger
 *
 * @anchor PQS_Camera_V4L2_Channel_uyvy_out
 * ## PQS Camera V4L2 Channel uyvy_out
 *             This is a UYVY image AKA interleaved YUV422. The size of the image is configurable.
 *
 * @anchor PQS_Camera_V4L2_Channel_trigger
 * ## PQS Camera V4L2 Channel trigger
 *             This is the trigger for the camera module. Any object can be routed to this channel to trigger
 *             the camera module to collect a single frame. The uyvy_out of the camera module can be routed
 *             to this channel to keep a continuous stream of images going. Another option is to route the
 *             final output object of a given processing pipeline into this channel to assure the pipeline is only
 *             processing one image at a time. This channel must be kicked off by an external trigger in the graph
 *             file.
 *
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_BROKER_InitChannelInfo(p_instance, CAMERA_PRV_CHANNEL_UYVY_OUT, "uyvy_out", R_OBJECT_TYPE_IMAGE,   R_BROKER_DATA_DIRECTION_OUT);
    result |= R_BROKER_InitChannelInfo(p_instance, CAMERA_PRV_CHANNEL_TRIGGER,  "trigger",  R_OBJECT_TYPE_TRIGGER, R_BROKER_DATA_DIRECTION_IN);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    st_priv_t * p_data     = R_MODULE_GetPrivDataPtr(p_instance);
    r_result_t result      = R_RESULT_SUCCESS;
    st_object_t * p_empty_object = R_BROKER_GetObjectPtr(p_instance, CAMERA_PRV_CHANNEL_UYVY_OUT);
    st_object_t * p_captured_object = NULL;
    uint32_t buffer_index   = 0;
    uint64_t time_stamp_usec_boot_time = 0;

    /*
     * Note: The camera module needs to keep a v4l2 buffer containing different image object planes.
     * The v4l2 driver will fill one of these data planes with the camera image. Instead of copying
     * the image to the provided output object, we replace the output object with the already
     * populated object. When then add the provided empty object to the buffer queue to get populated
     * at a later time.
     */

    if (R_OBJECT_CheckObjectType(p_empty_object, R_OBJECT_TYPE_IMAGE) != R_RESULT_SUCCESS)
    {
        R_PRINT_ERROR("Invalid object input");
        result = R_RESULT_FAILED;
    }

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    while ((R_RESULT_SUCCESS == result) && (NULL == p_captured_object))
    {
        if (wait_for_frame(p_data) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Couldn't get frame");
            result = R_RESULT_FAILED;
        }

        if (R_RESULT_SUCCESS == result)
        {
            if (get_frame_buffer_id_and_timestamp(p_data, &buffer_index, &time_stamp_usec_boot_time) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Couldn't read frame");
                result = R_RESULT_FAILED;
            }
        }

        if (R_RESULT_SUCCESS == result)
        {
            p_data->frame_count++;
            p_captured_object = p_data->p_obj_buffer[buffer_index];

            /* Check if we want to send the image to the broker */
            if (p_data->config.keep_frame <= p_data->frame_count)
            {
                p_data->frame_count = 0;

                if (fill_header(p_data, p_captured_object, time_stamp_usec_boot_time) == R_RESULT_SUCCESS)
                {
                    /* Queue with new object */
                    result |= queue_buffer(p_data, buffer_index, p_empty_object);
                }
                else
                {
                    R_PRINT_ERROR("Couldn't fill header");
                    result = R_RESULT_FAILED;
                }
            }
            else
            {
                /* Queue with old object */
                result |= queue_buffer(p_data, buffer_index, p_captured_object);
                p_captured_object = NULL;
            }
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = R_BROKER_ReplaceOutputObject(p_instance, CAMERA_PRV_CHANNEL_UYVY_OUT, p_captured_object);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Couldn't add output to broker");
        }
    }

    return result;
}
