/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_text.h
 * @brief         Header file for common API to interact with a collection of bit arrays used to represent letters,
 *                numbers and other characters.
 * @defgroup      PQS_Common_Text PQS Common Text
 * @section       PQS_Common_Text_Summary Module Summary
 *                Collection of bit arrays used to represent letters, numbers and other characters. Used primarily to
 *                write text onto an image for HDMI display. User can request a a_letter_t pointer for a given character
 *                then query if a specific pixel at a desired height and width should be shaded or not to draw that
 *                character on an image.
 *
 *                All font bitmaps created using https://littlevgl.com/ttf-font-to-c-array
 *                using the 8 px hight pixel font Unscii available for download on the website.
 *                Height equal 14 and bpp equals 1. Arrays we manually converted from uint8_t to uint16_t.
 * @ingroup       PQS_Common
 * @{
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

#ifndef R_COMMON_TEXT_H_
    #define R_COMMON_TEXT_H_

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define R_COMMON_LETTER_HEIGHT  (16u) /*!< Height of the letter in pixels */
#define R_COMMON_LETTER_WIDTH   (14u) /*!< Width of the letter in pixels. */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

typedef uint16_t a_letter_t[R_COMMON_LETTER_HEIGHT]; /*!< Array of bitmasks representing various characters */

/***********************************************************************************************************************
 Exported global variables
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 **********************************************************************************************************************/

const a_letter_t * R_COMMON_TEXT_GetLetterArray(char letter);
bool R_COMMON_TEXT_GetLetterValue(const a_letter_t * p_letter_array, uint8_t height, uint8_t width);

#endif /* R_COMMON_TEXT_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Common>
 *********************************************************************************************************************/
