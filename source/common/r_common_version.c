/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_version.c
 * @brief         Source file for common API for versioning info
 *********************************************************************************************************************/
/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include "r_common.h"
#include "r_common_version.h"
#include "rcar_environment.h"

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_VERSION_Print
 * @brief           Prints the version information of PQS
 *********************************************************************************************************************/
void R_COMMON_VERSION_Print( void )
{
    /*lint --e{123} --e{40} */

    /*
     * Note: PQS_MAJOR_VERSION, PQS_MINOR_VERSION, PQS_BUGFIX_VERSION,
     * GIT_COMMIT, GIT_UNCOMITTED, and BUILD_DATE are defined in Makefile.
     */

    R_PRINT("################################################################################\n"
            "# \n"
            "# \t Renesas PQS Version: v%s.%s.%s \n"
            "# \t RCAR Environment Version %s \n"
            "# \t Git Commit Hash: %s \n"
            "# \t Uncommitted Files: %s \n"
            "# \t Build Date: %s \n"
            "# \n"
            "################################################################################\n"
            "\n",
            PQS_MAJOR_VERSION, PQS_MINOR_VERSION, PQS_BUGFIX_VERSION,
            RCAR_ENV_GIT_BRANCH,
            GIT_COMMIT,
            GIT_UNCOMITTED,
            BUILD_DATE);

}
