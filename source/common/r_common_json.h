/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_json.h
 * @brief         Header file for common API to populate and save JSON files.
 * @defgroup      PQS_Common_Json PQS Common JSON
 * @section       PQS_Common_Json_Summary Module Summary
 *                Various functions are available to populate and save JSON files.
 *                JSON files are formed by blocks (separated by pairs of brackets) and entries (key-value pairs).
 *                To create a JSON file, a st_json_block_t struct can be used as a container. It must be initialized with R_COMMON_JSON_InitUnnamedBlock() first (specifying a pointer to a char array
 *                that will store its content) and then more sub-blocks can be added to it by initializing them first and copying their content to the parent container with R_COMMON_JSON_AddBlockContent().
 *                Once the main container has been populated, it can be saved to file with R_COMMON_JSON_SaveToFile().
 * @ingroup       PQS_Common
 * @{
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

#ifndef R_COMMON_JSON_H_
    #define R_COMMON_JSON_H_

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define R_JSON_BLOCK_HEADER_LENGTH    (128u)       /*!< Size of the char array used to store the name and the opening brackets of a JSON block. */
#define R_JSON_BLOCK_FOOTER_LENGTH    (4u)         /*!< Size of the char array used to store the closing bracket of a JSON block. */
#define R_JSON_DATA_BUFFER_SIZE       (131072u)    /*!< Size of the char array used to store the complete content of a JSON file before writing it to file. */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Struct for storing a JSON block. */
typedef struct
{
    char        header[R_JSON_BLOCK_HEADER_LENGTH];   /*!< Char array for storing the name and the opening brackets of a JSON block. */
    char *      p_content_buffer;                     /*!< Pointer to a char array for storing the body of a JSON block. */
    size_t      content_buffer_size;                  /*!< Size of the char array used to store the body of a JSON block. */
    char        footer[R_JSON_BLOCK_FOOTER_LENGTH];   /*!< Char array for storing closing bracket of a JSON block. */
    bool        is_empty;                             /*!< Boolean value specifying if the body of the JSON block already contains data or not. */
} st_json_block_t;

/***********************************************************************************************************************
 Exported global variables
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 **********************************************************************************************************************/

r_result_t R_COMMON_JSON_InitUnnamedBlock(st_json_block_t * p_json_block, char * p_content_buffer, size_t content_buffer_size);
r_result_t R_COMMON_JSON_InitNamedBlock(st_json_block_t * p_json_block, const char * const p_block_name, char * p_content_buffer, size_t content_buffer_size);
r_result_t R_COMMON_JSON_InitArrayBlock(st_json_block_t * p_json_block, const char * const p_array_name, char * p_content_buffer, size_t content_buffer_size);
r_result_t R_COMMON_JSON_AddBlockContent(st_json_block_t * p_dest_block, st_json_block_t * p_source_block);
r_result_t R_COMMON_JSON_AddStrEntry(st_json_block_t * p_dest_block, const char * const p_key, const char * const p_value);
r_result_t R_COMMON_JSON_AddIntEntry(st_json_block_t * p_dest_block, const char * const p_key, int32_t value);
r_result_t R_COMMON_JSON_AddFloatEntry(st_json_block_t * p_dest_block, const char * const p_key, float value);
r_result_t R_COMMON_JSON_AddBoolEntry(st_json_block_t * p_dest_block, const char * const p_key, bool value);
r_result_t R_COMMON_JSON_SaveToFile(st_json_block_t * p_json_block, const char * const p_file_path);

#endif /* R_COMMON_JSON_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Common>
 *********************************************************************************************************************/
