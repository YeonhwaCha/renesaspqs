/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_log.h
 * @brief         Header file for common API to print statements to stream with finer control than printf()
 * @defgroup      PQS_Common_Log PQS Common Log
 * @section       PQS_Common_Log_Summary Module Summary
 *                Allows to print statements to stdout with finer control than printf()
 * @ingroup       PQS_Common
 * @{
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

#ifndef R_COMMON_LOG_H_
#define R_COMMON_LOG_H_

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

/*lint -esym(9022, R_LOG, R_PRINT, R_PRINT_DEBUG, R_PRINT_INFO, R_PRINT_NOTICE, R_PRINT_WARNING, R_PRINT_ERROR) */

#define R_LOG(l, ...)          R_COMMON_LOG_Log((l), __FILE__, __LINE__, __func__, __VA_ARGS__) /*!< Print level to log with filename, line number and function */
#define R_PRINT(...)           R_COMMON_LOG_Print(__VA_ARGS__)     /*!< Print to stream without altering message */

#define R_PRINT_DEBUG(...)     R_LOG(R_LOG_DEBUG,   __VA_ARGS__) /*!< Print DEBUG to log */
#define R_PRINT_INFO(...)      R_LOG(R_LOG_INFO,    __VA_ARGS__) /*!< Print INFO to log */
#define R_PRINT_NOTICE(...)    R_LOG(R_LOG_NOTICE,  __VA_ARGS__) /*!< Print NOTICE to log */
#define R_PRINT_WARNING(...)   R_LOG(R_LOG_WARNING, __VA_ARGS__) /*!< Print WARNING to log */
#define R_PRINT_ERROR(...)     R_LOG(R_LOG_ERROR,   __VA_ARGS__) /*!< Print ERROR to log */

/*! Enumerated list of log levels as a string array */
#define E_LOG_LEVEL_AS_STRING    {                        \
                                    "R_LOG_DEBUG",        \
                                    "R_LOG_INFO",         \
                                    "R_LOG_NOTICE",       \
                                    "R_LOG_WARNING",      \
                                    "R_LOG_ERROR",        \
                                    "R_LOG_DISABLED",     \
                                  }

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Enumerated list of log levels */
typedef enum {
    R_LOG_DEBUG = 0,     /*!< Chatter, can safely be ignored unless debugging  */
    R_LOG_INFO,          /*!< General status, usually on key events (like infrequent state changes) */
    R_LOG_NOTICE,        /*!< Possible error, system will continue to operate  */
    R_LOG_WARNING,       /*!< Expected error, but system will continue to operate  */
    R_LOG_ERROR,         /*!< Less serious failure, system will still try to run */
    R_LOG_DISABLED,      /*!< No log will be printed */
} e_log_level_t;

/***********************************************************************************************************************
 Exported global variables
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 **********************************************************************************************************************/
extern r_result_t R_COMMON_LOG_Preinit(void);
extern r_result_t R_COMMON_LOG_Init(void);
extern r_result_t R_COMMON_LOG_Deinit(void);
extern r_result_t R_COMMON_LOG_SetLevel(const e_log_level_t level);
extern r_result_t R_COMMON_LOG_SetLogFile(void);
extern void       R_COMMON_LOG_Log(const e_log_level_t level, char const * const file_name, const int32_t file_line, char const * const function_name, char const * const message, ...); //lint !e1916
extern void       R_COMMON_LOG_Print(char const * const message, ...); //lint !e1916
#endif /* R_COMMON_LOG_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Common>
 *********************************************************************************************************************/

