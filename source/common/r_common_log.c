/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_log.c
 * @brief         Source file for common API to print statements to stream with finer control than printf()
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/*lint --e{586} */

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include <stdarg.h> /* For va_list */

#include "r_common.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define LOG_PRV_MAX_MESSAGE_LENGTH     (2048u)  /*!< Maximum length of a message */
#define LOG_PRV_MAX_LOG_FILENAME       (128u)   /*!< Maximum length of a log filename */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Structure containing the info for a particular log level */
typedef struct {
    char const * const  name;         /*!< level as string */
    char const * const  vt100_format; /*!< vt100 format of level to change print statement's color */
    char const * const  vt100_clear;  /*!< vt100 clearing after message is printed */
} st_log_level_info_t;

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

static e_log_level_t  s_log_level;  /*!< Current log level */
static st_word_list_t s_unmute;     /*!< Word list containing the file names of all files to unmute */
static st_word_list_t s_mute;       /*!< Word list containing the file names of all files to mute */
static FILE *         sp_file;      /*!< File pointer for logging */

/*! @cond DOX_IGNORE */
static const st_log_level_info_t s_log_level_info_error   = { .name = "ERROR",   .vt100_format = "\033[0;31m", /* Red */         .vt100_clear = "\033[0m" }; /*lint !e9039 !e9104 */
static const st_log_level_info_t s_log_level_info_warning = { .name = "WARNING", .vt100_format = "\033[1;33m", /* Yellow */      .vt100_clear = "\033[0m" }; /*lint !e9039 !e9104 */
static const st_log_level_info_t s_log_level_info_notice  = { .name = "NOTICE",  .vt100_format = "\033[1;33m", /* Yellow */      .vt100_clear = "\033[0m" }; /*lint !e9039 !e9104 */
static const st_log_level_info_t s_log_level_info_info    = { .name = "INFO",    .vt100_format = "\033[1;32m", /* Light Green */ .vt100_clear = "\033[0m" }; /*lint !e9039 !e9104 */
static const st_log_level_info_t s_log_level_info_debug   = { .name = "DEBUG",   .vt100_format = "\033[0;37m", /* Light Gray */  .vt100_clear = "\033[0m" }; /*lint !e9039 !e9104 */
/*! @endcond */

/******************************************************************************************************************//**
 * Function Name:   get_log_level_info
 * @brief           Returns the pointer to the st_log_level_info_t struct for a given log level
 * @param [in]      level - requested level
 * @retval          Pointer to the corresponding st_log_level_info_t struct
 *********************************************************************************************************************/
static st_log_level_info_t const * get_log_level_info(const e_log_level_t level)
{
    st_log_level_info_t const * p_log_level_info;

    switch (level)
    {
        case R_LOG_ERROR:
        {
            p_log_level_info = &s_log_level_info_error;
            break;
        }

        case R_LOG_WARNING:
        {
            p_log_level_info = &s_log_level_info_warning;
            break;
        }

        case R_LOG_NOTICE:
        {
            p_log_level_info = &s_log_level_info_notice;
            break;
        }

        case R_LOG_INFO:
        {
            p_log_level_info = &s_log_level_info_info;
            break;
        }

        case R_LOG_DISABLED:
        case R_LOG_DEBUG:
        default:
        {
            p_log_level_info = &s_log_level_info_debug;
            break;
        }

    }

    return p_log_level_info;
}

/******************************************************************************************************************//**
 * Function Name:   is_in_list
 * @brief           Returns true if a word is in the word list
 * @param [in]      word - String containing a word
 * @param [in]      list - Word list containing a list of words
 * @retval          true - if word is in list. false - otherwise
 *********************************************************************************************************************/
static bool is_in_list(char const * const word, st_word_list_t * list)
{
    bool result = false;

    /*
     * Note we want to start at the ends of both strings and work out way to the beginning.
     * That way we can disregard the folder locations of the file names
     */

    /* loop through all the names in the list */
    for (uint32_t list_index = 0; list_index < list->num_words; list_index++)
    {
        /* Init every loop */
        result = true;

        /* Start at the end of both strings */
        uint32_t char_index_list = (uint32_t)R_COMMON_STRING_Length(list->word_list[list_index]);
        uint32_t char_index_word = (uint32_t)R_COMMON_STRING_Length(word);

        /* Account for null termination */
        char_index_list -= 1u;
        char_index_word -= 1u;

        if (char_index_list < R_STRING_MAX_WORD_LENGTH)
        {
            /* loop through all the characters in the string */
            while ((0u != char_index_list) && (0u != char_index_word))
            {
                if (list->word_list[list_index][char_index_list] != word[char_index_word])
                {
                    result = false;
                    break;
                }

                char_index_list--;
                char_index_word--;
            }
        }
        else
        {
            R_PRINT_ERROR("Char array out of bounds");
        }

        /* Check if a match was found, we can exit the for loop */
        if (true == result)
        {
            break;
        }
    }

    return result;
}


/******************************************************************************************************************//**
 * Function Name:   R_COMMON_LOG_Preinit
 * @brief           Preinit of module. Must be called before R_COMMON_LOG_Init(). Configuration files have not been
 *                  read at this point.
 * @retval          R_RESULT_SUCCESS - on success, R_RESULT_FAILED otherwise
 *********************************************************************************************************************/
r_result_t R_COMMON_LOG_Preinit(void)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Default log level for log messages before init function */
    s_log_level = R_LOG_NOTICE;

    /* Default log file to standard out */
    sp_file = stdout;

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_LOG_Init
 * @brief           Initialization of module. Inits the log mute list, and log unmute list based off of
 *                  configuration files.
 * @retval          R_RESULT_SUCCESS - on success, R_RESULT_FAILED otherwise
 *********************************************************************************************************************/
r_result_t R_COMMON_LOG_Init(void)
{
    r_result_t result = R_RESULT_SUCCESS;
    char unmute_string[R_STRING_MAX_NUM_WORDS * R_STRING_MAX_WORD_LENGTH] = {'\0'};
    char mute_string[R_STRING_MAX_NUM_WORDS * R_STRING_MAX_WORD_LENGTH] = {'\0'};

    /* Get all file names to unmute */
    result |= R_COMMON_STRING_Copy(unmute_string, R_COMMON_CONFIG_GetString("common", "log_unmute", ""), R_STRING_MAX_NUM_WORDS * R_STRING_MAX_WORD_LENGTH);
    result |= R_COMMON_STRING_Split(unmute_string, &s_unmute, ' ');

    /* Get all file names to mute */
    result |= R_COMMON_STRING_Copy(mute_string, R_COMMON_CONFIG_GetString("common", "log_mute", ""), R_STRING_MAX_NUM_WORDS * R_STRING_MAX_WORD_LENGTH);
    result |= R_COMMON_STRING_Split(mute_string, &s_mute, ' ');

    /* Check configuration */
    if (R_RESULT_SUCCESS == result)
    {
        for (uint32_t i = 0; i < s_unmute.num_words; i++)
        {
            for (uint32_t j = 0; j < s_mute.num_words; j++)
            {
                if (R_COMMON_STRING_Compare(&s_unmute.word_list[i][0], &s_mute.word_list[j][0]) == R_RESULT_SUCCESS)
                {
                    R_PRINT_ERROR("File name %s is configured for both log mute and log unmute. This is not allowed.", &s_unmute.word_list[i][0]);
                    result = R_RESULT_FAILED;
                }
            }
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_LOG_Deinit
 * @brief           Deinitialization of module.
 * @retval          R_RESULT_SUCCESS - on success, R_RESULT_FAILED otherwise
 *********************************************************************************************************************/
r_result_t R_COMMON_LOG_Deinit(void)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (stdout != sp_file)
    {
        if (fclose(sp_file) != 0) /*lint !e482 */
        {
            R_PRINT_ERROR("Error closing log file");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_LOG_SetLevel
 * @brief           Sets the current log level to the specified log level.
 * @param [in]      level - new log level
 * @retval          R_RESULT_SUCCESS on success, R_RESULT_FAILED otherwise
 *********************************************************************************************************************/
r_result_t R_COMMON_LOG_SetLevel(const e_log_level_t level)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (level <= R_LOG_DISABLED)
    {
        s_log_level = level;
        R_PRINT_NOTICE("Set log level to %s (%d)", get_log_level_info(level)->name, level);
    }
    else
    {
        R_PRINT_ERROR("Log level %d out of bounds", level);
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_LOG_SetLogFile
 * @brief           Opens a file to write log events to. Filename is based on the current time. After this function
 *                  is called log message will no longer be displayed to stdout, rather they will be written to the
 *                  log file.
 * @retval          R_RESULT_SUCCESS on success, R_RESULT_FAILED otherwise
 *********************************************************************************************************************/
r_result_t R_COMMON_LOG_SetLogFile(void)
{
    r_result_t result = R_RESULT_SUCCESS;
    char filename[LOG_PRV_MAX_LOG_FILENAME] = {'\0'};

    /* Get filename for log based off the current time */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        int32_t print_return = snprintf(filename, LOG_PRV_MAX_LOG_FILENAME, "log_%lu.txt", R_COMMON_TIME_GetRealUsec());

        if ((print_return < 0) || (print_return >= (int32_t)LOG_PRV_MAX_LOG_FILENAME))
        {
            R_PRINT_ERROR("Error getting file name");
            result = R_RESULT_FAILED;
        }
    }

    /* Open file  */
    if (R_RESULT_SUCCESS == result)
    {
        FILE * p_log_file = fopen(filename, "w");

        if (NULL != p_log_file)
        {
            /* Set file pointer */
            sp_file = p_log_file;
        }
        else
        {
            R_PRINT_ERROR("Error opening log file");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_LOG_Log
 * @brief           Print a message to stream along with the file name, line number and function name of the calling
 *                  function. If the level parameter does not meet or exceed the current log the message will not be
 *                  printed. This function wraps printf() in order to more finely control what gets printed to
 *                  stdout or to a file.
 * @param [in]      level - log level of the message
 * @param [in]      file_name - name of the file that the message originated from
 * @param [in]      file_line - line number in the file that the message originated from
 * @param [in]      function_name - name of the function that the message orginated from
 * @param [in]      message - string containing the message. Must be lees than LOG_PRV_MAX_MESSAGE_LENGTH
 * @param [in]      ... - variable argument list for message syntaxes
 *********************************************************************************************************************/
void R_COMMON_LOG_Log(const e_log_level_t level, char const * const file_name, const int32_t file_line, char const * const function_name, char const * const message, ...) /*lint !e9165 !e1916 */
{
    bool unmute = is_in_list(file_name, &s_unmute);
    bool mute   = is_in_list(file_name, &s_mute);

    /* Check if this message should be printed based on file configuration and log level */
    if (((unmute) || (level >= s_log_level)) && (!mute))
    {
        /* Init full_message inside this block so we save time if the message will not be printed */
        r_result_t result = R_RESULT_SUCCESS;
        char full_message[LOG_PRV_MAX_MESSAGE_LENGTH] = {'\0'};
        st_log_level_info_t const * p_log_level_info = get_log_level_info(level);
        uint32_t char_count = 0;
        int32_t  print_return = 0;
        va_list args; //Intentionally left uninitialized.

        /* Add message prefix */
        if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
        {
            if (stdout == sp_file)
            {
                /* Use vt 100 formatting */
                print_return = snprintf(full_message, LOG_PRV_MAX_MESSAGE_LENGTH, "%s%s%s:%s%d%s: %s%s%s in function %s%s()%s\n",
                        p_log_level_info->vt100_format, file_name,              p_log_level_info->vt100_clear,
                        p_log_level_info->vt100_format, file_line,              p_log_level_info->vt100_clear,
                        p_log_level_info->vt100_format, p_log_level_info->name, p_log_level_info->vt100_clear,
                        p_log_level_info->vt100_format, function_name,          p_log_level_info->vt100_clear);
            }
            else
            {
                /* Do not use vt 100 formatting */
                print_return = snprintf(full_message, LOG_PRV_MAX_MESSAGE_LENGTH, "%s:%d: %s in function %s()\n",
                                    file_name,
                                    file_line,
                                    p_log_level_info->name,
                                    function_name);
            }

            char_count += (uint32_t)print_return;

            if ((print_return < 0) || (print_return >= (int32_t)LOG_PRV_MAX_MESSAGE_LENGTH))
            {
                (void)printf("Error formatting message");
                result = R_RESULT_FAILED;
            }
        }

        /* Add rest of message */
        if (R_RESULT_SUCCESS == result)
        {
            va_start(args, message);
            print_return = vsnprintf(&full_message[char_count], (size_t)(LOG_PRV_MAX_MESSAGE_LENGTH - char_count), message, args);
            va_end(args);

            char_count += (uint32_t)print_return;

            if ((print_return < 0) || (print_return >= (int32_t)LOG_PRV_MAX_MESSAGE_LENGTH))
            {
                (void)printf("Error formatting message");
                result = R_RESULT_FAILED;
            }
        }

        /* Add suffix */
        if (R_RESULT_SUCCESS == result)
        {
            print_return = snprintf(&full_message[char_count], (size_t)(LOG_PRV_MAX_MESSAGE_LENGTH - char_count), "\n\n");

            char_count += (uint32_t)print_return;

            if ((print_return < 0) || (print_return >= (int32_t)LOG_PRV_MAX_MESSAGE_LENGTH))
            {
                (void)printf("Error formatting message");
                result = R_RESULT_FAILED;
            }

        }

        /* Print message */
        if (R_RESULT_SUCCESS == result)
        {
            if (char_count < LOG_PRV_MAX_MESSAGE_LENGTH)
            {
                (void)fprintf(sp_file, "%s", full_message);
                (void)fflush(sp_file);
            }
            else
            {
                (void)fprintf(sp_file, "Error: Message is longer than LOG_PRV_MAX_MESSAGE_LENGTH");
                (void)fflush(sp_file);
            }
        }
    }
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_LOG_Print
 * @brief           Print a message to stream exactly how it is formatted by the caller.
 * @param [in]      message - string containing the message.
 * @param [in]      ... - variable argument list for message syntaxes
 *********************************************************************************************************************/
void R_COMMON_LOG_Print(char const * const message, ...) /*lint !e1916 !e9165 */
{
    va_list args; //Intentionally left uninitialized.

    /* Check if logging is disabled */
    if (s_log_level < R_LOG_DISABLED)
    {
        /* Print message */
        va_start(args, message);
        (void)vfprintf(sp_file, message, args);
        (void)fflush(sp_file);
        va_end(args);
    }
}

