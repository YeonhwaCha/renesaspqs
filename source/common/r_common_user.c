/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_user.c
 * @brief         Source file for common API to allow modules to register a keystroke the user can press to interact
 *                with a particular module.  Modules can then query whenever a registered key is pressed by the user.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include <pthread.h> /* For mutex */
#include <unistd.h>  /* For usleep */

#include "r_common.h"
#include "r_common_os.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define USER_PRV_MAX_INPUTS         (64u)    /*!< Maximum number of different user inputs allowed */
#define USER_PRV_DESCRIPTION_LENGTH (64u)    /*!< Maximum length of user inputs descriptions */
#define USER_PRV_USLEEP_WAIT_TIME   (10000u) /*!< Time in microseconds to wait before trying to get the next input from user */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Struct for containing user inputs */
typedef struct {
    st_module_instance_t * p_instance;                                /*!< Instance the input is assigned to */
    char                   user_input;                                /*!< Specific character of the input */
    char                   description[USER_PRV_DESCRIPTION_LENGTH];  /*!< Description describing what happens when the input is pressed */
    uint32_t               count;                                     /*!< Number of times the user has pressed the input */
} st_user_input_t;

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static st_user_input_t   s_inputs[USER_PRV_MAX_INPUTS];  /*!< Array containing all user inputs */
static uint32_t          s_num_inputs;                   /*!< Number of user inputs in the s_inputs array */
static pthread_mutex_t   s_inputs_lock;                  /*!< Lock to prevent threads from accessing the user inputs at the same time */ /*lint !e9018 */

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   print_inputs
 * @brief           Prints all available user inputs to stdout
 *********************************************************************************************************************/
static void print_inputs(void)
{
    /*lint --e{9127} */

    R_PRINT("################################################################################\n"
            "# \n"
            "# \t Available user inputs.... \n"
            "# \n"
            "# \t Press 'q' to quit \n"
            "# \t Press 'm' to print module information\n"
            "# \t Press 'h' for help (this screen)\n"
            "# \n");

    (void)pthread_mutex_lock(&s_inputs_lock);

    for (uint32_t i = 0; i < s_num_inputs; i++)
    {
        switch (s_inputs[i].user_input)
        {
            /* Special handling for some characters */
            case SPACE_BAR:
            {
                R_PRINT("# \t Instance %s : Character 'SPACE_BAR' : %s \n", R_MODULE_GetInstanceName(s_inputs[i].p_instance), s_inputs[i].description);
                break;
            }
            case UP_ARROW:
            {
                R_PRINT("# \t Instance %s : Character 'UP_ARROW' : %s \n", R_MODULE_GetInstanceName(s_inputs[i].p_instance), s_inputs[i].description);
                break;
            }
            case DOWN_ARROW:
            {
                R_PRINT("# \t Instance %s : Character 'DOWN_ARROW' : %s \n", R_MODULE_GetInstanceName(s_inputs[i].p_instance), s_inputs[i].description);
                break;
            }
            case RIGHT_ARROW:
            {
                R_PRINT("# \t Instance %s : Character 'RIGHT_ARROW' : %s \n", R_MODULE_GetInstanceName(s_inputs[i].p_instance), s_inputs[i].description);
                break;
            }
            case LEFT_ARROW:
            {
                R_PRINT("# \t Instance %s : Character 'LEFT_ARROW' : %s \n", R_MODULE_GetInstanceName(s_inputs[i].p_instance), s_inputs[i].description);
                break;
            }

            default:
            {
                R_PRINT("# \t Instance %s : Character '%c' : %s \n", R_MODULE_GetInstanceName(s_inputs[i].p_instance), s_inputs[i].user_input, s_inputs[i].description);
                break;
            }
        }
    }

    (void)pthread_mutex_unlock(&s_inputs_lock);

    R_PRINT("# \n"
            "################################################################################\n");

}

/******************************************************************************************************************//**
 * Function Name:   process_input
 * @brief           This function should be called when a user has entered an input. The input will be stored and
 *                  later the module can query for any inputs.
 * @param [in]      user_input - Character the user has entered
 * @retval          false if the user has requested to exit the program, true otherwise
 *********************************************************************************************************************/
static bool process_input(const char user_input)
{
    bool running = true;
    bool input_is_in_list = false;

    switch (user_input)
    {
        case 'Q':
        case 'q':
        {
            running = false;
            break;
        }

        case 'H':
        case 'h':
        {
            print_inputs();
            break;
        }

        case 'M':
        case 'm':
        {
            R_BROKER_PrintChannels();
            break;
        }

        default:
        {
            (void)pthread_mutex_lock(&s_inputs_lock);

            /* For every entry in the list that matches the user input increment the counter */
            for (uint32_t i = 0; i < s_num_inputs; i++)
            {
                if (user_input == s_inputs[i].user_input)
                {
                    s_inputs[i].count++;
                    input_is_in_list = true;
                }
            }

            (void)pthread_mutex_unlock(&s_inputs_lock);

            /* Check if entry didn't match any registered elements */
            if (false == input_is_in_list)
            {
                R_PRINT_NOTICE("Input %c ASCII value %d is not registered by any module instances as an input.", user_input, user_input);
            }

            break;
        }
    }

    return running;
}


/******************************************************************************************************************//**
 * Function Name:   R_COMMON_USER_Init
 * @brief           Initializes the R_COMMON_USER file
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_USER_Init(void)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Init registered input list */
    for (uint32_t i = 0; i < USER_PRV_MAX_INPUTS; i++)
    {
        s_inputs[i].p_instance = NULL;
        s_inputs[i].user_input = '\0';
        s_inputs[i].count      = 0;
        R_COMMON_Memset(s_inputs[i].description, 0, USER_PRV_DESCRIPTION_LENGTH);
    }

    s_num_inputs = 0;

    if (0 != pthread_mutex_init(&s_inputs_lock, NULL))
    {
        R_PRINT_ERROR("Lock creation failed");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_USER_Deinit
 * @brief           Deinitializes the R_COMMON_USER file
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_USER_Deinit(void)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (0 != pthread_mutex_destroy(&s_inputs_lock))
    {
        R_PRINT_ERROR("Lock destruction failed");
        result = R_RESULT_FAILED;
    }

    s_num_inputs = 0;

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_USER_LoopOnInput
 * @brief           This is a BLOCKING function. It will loop endlessly until the user has quit the program by entering
 *                  either a 'q' or a 'Q'
 * @param [in]      p_preinput - Array of characters to preprocess before waiting for user input
 * @param [in]      preinput_size - Size of p_preinput array
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
void R_COMMON_USER_LoopOnInput(const char * p_preinput, size_t preinput_size)
{
    /*lint --e{586} */

    int input = 0;
    bool running = true;

    /* Remove the need for the user to have to press return after entering a input */
    R_COMMON_OS_SetTerminalRaw();

    /* Print available inputs user can enter */
    print_inputs();

    /* Handle the preprocess keystrokes */
    for (uint32_t pre_index = 0; pre_index < preinput_size; pre_index++)
    {
        running = process_input(p_preinput[pre_index]);
    }

    /* Loop until user quits the program */
    while (running)
    {
        (void)usleep(USER_PRV_USLEEP_WAIT_TIME);
        input = getchar();

        /* Check for escape key */
        if ((int)ESCAPE_KEY == input)
        {
            input = getchar();

            /* Check for prearrow key */
            if ((int)PRE_ARROW == input)
            {
                /* Now get the arrow key */
                input = getchar();

                /* Add an offset to put it into the special character ASCII section
                 * Otherwise the arrow keys map to 'A', 'B', 'C', and 'D' */
                input += ARROW_OFFSET;
            }
        }

        if (input > 0)
        {
            running = process_input((char)(input));
        }
    }

    /* Set terminal back to the way it was */
    R_COMMON_OS_ClearTerminalRaw();
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_USER_RegisterInput
 * @brief           Modules should call this function during initialization to register a specific character as a user
 *                  input
 * @param [in]      p_instance - Instance the user input should be register to
 * @param [in]      user_input - Character that will be used as user input
 * @param [in]      description - String description of what happens in the module when the user input is entered
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_USER_RegisterInput(st_module_instance_t * const p_instance, const char user_input, const char * const description)
{
    /*lint --e{9127} */

    r_result_t result = R_RESULT_SUCCESS;

    /* Check for reserved characters */
    switch (user_input)
    {
        case USER_INPUT_NOT_AVAILABLE:
        case (char)ESCAPE_KEY:
        case (char)PRE_ARROW:
        case 'Q':
        case 'q':
        case 'H':
        case 'h':
        case 'M':
        case 'm':
        {
            R_PRINT_ERROR("Character %c ASCII value %d is reserved. Please choose another", user_input, user_input);
            result = R_RESULT_FAILED;
            break;
        }

        default:
        {
            /* Everything is fine */
            break;
        }
    }

    /* Check if the list is full */
    if (R_RESULT_SUCCESS == result)
    {
        if ((s_num_inputs + 1u) == USER_PRV_MAX_INPUTS)
        {
            R_PRINT_ERROR("Too many inputs have already been registered");
            result = R_RESULT_FAILED;
        }
    }

    /* Check description length */
    if (R_RESULT_SUCCESS == result)
    {
        if (R_COMMON_STRING_Length(description) > USER_PRV_DESCRIPTION_LENGTH)
        {
            R_PRINT_ERROR("Description is too long must be less than USER_PRV_DESCRIPTION_LENGTH");
            result = R_RESULT_FAILED;
        }
    }

    /* See if there is already another instance using this character as input */
    if (R_RESULT_SUCCESS == result)
    {
        (void)pthread_mutex_lock(&s_inputs_lock);
        for (uint32_t i = 0u; i < s_num_inputs; i++)
        {
            if (user_input == s_inputs[i].user_input)
            {
                /* This isn't an error but we should inform the user */
                R_PRINT_NOTICE("Another module has already registered character %c ASCII value %d", user_input, user_input);
            }
        }
        (void)pthread_mutex_unlock(&s_inputs_lock);
    }

    /* Save the user input into the input list */
    if (R_RESULT_SUCCESS == result)
    {
        (void)pthread_mutex_lock(&s_inputs_lock);
        s_inputs[s_num_inputs].p_instance = p_instance;
        s_inputs[s_num_inputs].user_input = user_input;
        s_inputs[s_num_inputs].count      = 0;
        (void)R_COMMON_STRING_Copy(s_inputs[s_num_inputs].description, description, USER_PRV_DESCRIPTION_LENGTH);
        s_num_inputs++;
        (void)pthread_mutex_unlock(&s_inputs_lock);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_USER_GetInput
 * @brief           Modules should call this function during processing to see if the user has entered any inputs
 * @param [in]      p_instance - Instance the user input should be register to
 * @retval          USER_INPUT_NOT_AVAILABLE if no input is available, otherwise returs the character that the user
 *                  has pressed
 *********************************************************************************************************************/
char R_COMMON_USER_GetInput(st_module_instance_t * const p_instance)
{
    char result = USER_INPUT_NOT_AVAILABLE;

    (void)pthread_mutex_lock(&s_inputs_lock);

    /* See if there is a pending user input */
    for (uint32_t i = 0u; i < s_num_inputs; i++)
    {
        if ((p_instance == s_inputs[i].p_instance) && (s_inputs[i].count > 0u))
        {
            result = s_inputs[i].user_input;
            s_inputs[i].count--;
        }
    }

    (void)pthread_mutex_unlock(&s_inputs_lock);

    return result;

}
