/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_config.c
 * @brief         Source file for common API to configure modules and other source code.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include "r_common.h"
#include "iniparser.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define CONFIG_PRV_MAX_CONFIG_FILES           (16u)           /*!< Maximum number of config files that can be added */
#define CONFIG_PRV_SECTION_KEY_MAX_LENGTH     (128u)          /*!< Max length of section key combination */
#define CONFIG_PRV_UINT32_NOT_FOUND           (-1)            /*!< Value ini library should return if section:key is note found in config file */
#define CONFIG_PRV_INT32_NOT_FOUND            (-0x7FFFFFFF)   /*!< Value ini library should return if section:key is note found in config file */
#define CONFIG_PRV_DOUBLE_NOT_FOUND           (-2.22507e-308) /*!< Value ini library should return if section:key is note found in config file */
#define CONFIG_PRV_BOOL_NOT_FOUND             (-1)            /*!< Value ini library should return if section:key is note found in config file */
#define CONFIG_PRV_STRING_NOT_FOUND           (NULL)          /*!< Value ini library should return if section:key is note found in config file */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

static dictionary * sp_config_files[CONFIG_PRV_MAX_CONFIG_FILES]; /*!< array containing all of the configuration dictionaries */
static uint8_t      s_config_file_count;              /*!< Number of configuration dictionaries in the sp_config_files array */

/******************************************************************************************************************//**
 * Function Name:   combine_section_key
 * @brief           Combines the section string and key string into one string with a ":" in the middle
 * @param [in]      section - String of the section name
 * @param [in]      key - String of the key name
 * @param [out]     section_key - Combined setction key name with a ":" separating the section from the key
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t combine_section_key(const char * const section, const char * const key, char * section_key)
{
    r_result_t return_code = R_RESULT_SUCCESS;

    /* Check section and key length */
    if (R_RESULT_SUCCESS == return_code) /*lint !e774 !e948 */
    {
        return_code |= R_COMMON_STRING_Copy(   section_key,  section, CONFIG_PRV_SECTION_KEY_MAX_LENGTH);
        return_code |= R_COMMON_STRING_Combine(section_key,  ":",     CONFIG_PRV_SECTION_KEY_MAX_LENGTH);
        return_code |= R_COMMON_STRING_Combine(section_key,  key,     CONFIG_PRV_SECTION_KEY_MAX_LENGTH);

        if (R_RESULT_SUCCESS != return_code)
        {
            R_PRINT_ERROR("Error creating section key. section = %s, key = %s", section, key);
            return_code = R_RESULT_FAILED;
        }
    }

    return return_code;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_CONFIG_Init
 * @brief           Initializes the R_COMMON_CONFIG file
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_CONFIG_Init(void)
{
    s_config_file_count = 0;

    for (uint8_t config_index = 0; config_index < CONFIG_PRV_MAX_CONFIG_FILES; config_index++)
    {
        sp_config_files[config_index] = NULL;
    }

    return R_RESULT_SUCCESS;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_CONFIG_Deinit
 * @brief           Initializes the R_COMMON_CONFIG file
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_CONFIG_Deinit(void)
{
    /* Release config file */
    for (uint8_t config_index = 0; config_index < CONFIG_PRV_MAX_CONFIG_FILES; config_index++)
    {
        if (NULL != sp_config_files[config_index])
        {
            iniparser_freedict(sp_config_files[config_index]);
        }
    }

    return R_RESULT_SUCCESS;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_CONFIG_AddFile
 * @brief           Add a file to the list of config dictionaries
 * @param [in]      file_name - Name of the file to add to the config dictionaries
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_CONFIG_AddFile(char const * const file_name)
{
    r_result_t return_code = R_RESULT_SUCCESS;

    if (R_RESULT_SUCCESS == return_code) /*lint !e774 !e948 */
    {
        if (s_config_file_count >= CONFIG_PRV_MAX_CONFIG_FILES)
        {
            R_PRINT_ERROR("Cannot add config file \"%s\", already at maximum config file count (%d)", file_name, CONFIG_PRV_MAX_CONFIG_FILES);
            return_code = R_RESULT_FAILED;
        }
    }

    /* Check if config file exists */
    if (R_RESULT_SUCCESS == return_code)
    {
        if (access(file_name, F_OK) == (-1))
        {
            R_PRINT_ERROR("Config file \"%s\" does not exist.", file_name);
            return_code = R_RESULT_FAILED;
        }
    }

    /* Load config file */
    if (R_RESULT_SUCCESS == return_code)
    {
        sp_config_files[s_config_file_count] = iniparser_load(file_name);

        if (NULL != sp_config_files[s_config_file_count])
        {
            R_PRINT_NOTICE("Added config file %s", file_name);
            s_config_file_count++;
        }
        else
        {
            R_PRINT_ERROR("Error loading config file");
            return_code = R_RESULT_FAILED;
        }
    }

    return return_code;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_CONFIG_GetUint32
 * @brief           Search the config for the section:key if it exists return the value in the config file
 *                  if it does not return the default_value
 * @param [in]      section - Name of the section in the config file
 * @param [in]      key - Name of the key in the config file
 * @param [in]      default_value - Value to return if the section:key does not exist
 * @retval          value to initialize configuration to
 *********************************************************************************************************************/
uint32_t R_COMMON_CONFIG_GetUint32(const char * const section, const char * const key, uint32_t default_value)
{
    r_result_t result = R_RESULT_SUCCESS;
    char section_key[CONFIG_PRV_SECTION_KEY_MAX_LENGTH] = {'\0'};
    long int temp_value = 0;
    uint32_t return_value = default_value;

    /* Combine section and key into one string */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        if (combine_section_key(section, key, section_key) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error combining section and key");
            result = R_RESULT_FAILED;
        }
    }

    /* Loop through all config files to find section-key */
    if (R_RESULT_SUCCESS == result)
    {
        for (uint8_t config_index = 0; config_index < s_config_file_count; config_index++)
        {
            /* Init every loop */
            result = R_RESULT_SUCCESS;

            /* Check if entry exists */
            if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
            {
                if (iniparser_find_entry(sp_config_files[config_index], section_key) != 1)
                {
                    /* Note: This is not an error to report */
                    R_PRINT_INFO("Entry %s not found in config", section_key);
                    result = R_RESULT_FAILED;
                }
            }

            /* Get value */
            if (R_RESULT_SUCCESS == result)
            {
                temp_value = iniparser_getlongint(sp_config_files[config_index], section_key, CONFIG_PRV_UINT32_NOT_FOUND);

                if (CONFIG_PRV_UINT32_NOT_FOUND == temp_value)
                {
                    R_PRINT_ERROR("Error getting value.");
                    result = R_RESULT_FAILED;
                }
            }

            /* Check value */
            if (R_RESULT_SUCCESS == result)
            {
                if (temp_value < 0)
                {
                    R_PRINT_ERROR("Error value is out of range");
                    result = R_RESULT_FAILED;
                }
            }

            /* Copy value */
            if (R_RESULT_SUCCESS == result)
            {
                return_value = (uint32_t)temp_value;
                R_PRINT_INFO("Config param %s set to %u", section_key, return_value);
            }
        }
    }

    return return_value;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_CONFIG_GetInt32
 * @brief           Search the config for the section:key if it exists return the value in the config file
 *                  if it does not return the default_value
 * @param [in]      section - Name of the section in the config file
 * @param [in]      key - Name of the key in the config file
 * @param [in]      default_value - Value to return if the section:key does not exist
 * @retval          value to initialize configuration to
 *********************************************************************************************************************/
int32_t R_COMMON_CONFIG_GetInt32(const char * const section, const char * const key, int32_t default_value)
{
    r_result_t result = R_RESULT_SUCCESS;
    char section_key[CONFIG_PRV_SECTION_KEY_MAX_LENGTH] = {'\0'};
    long int temp_value = 0;
    int32_t return_value = default_value;

    /* Combine section and key into one string */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        if (combine_section_key(section, key, section_key) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error combining section and key");
            result = R_RESULT_FAILED;
        }
    }

    /* Loop through all config files to find section-key */
    if (R_RESULT_SUCCESS == result)
    {
        for (uint8_t config_index = 0; config_index < s_config_file_count; config_index++)
        {
            /* Init every loop */
            result = R_RESULT_SUCCESS;

            /* Check if entry exists */
            if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
            {
                if (iniparser_find_entry(sp_config_files[config_index], section_key) != 1)
                {
                    /* Note: This is not an error to report */
                    R_PRINT_INFO("Entry %s not found in config", section_key);
                    result = R_RESULT_FAILED;
                }
            }

            /* Get value */
            if (R_RESULT_SUCCESS == result)
            {
                temp_value = iniparser_getlongint(sp_config_files[config_index], section_key, CONFIG_PRV_INT32_NOT_FOUND);

                if (CONFIG_PRV_INT32_NOT_FOUND == temp_value)
                {
                    R_PRINT_ERROR("Error getting value. Please check if your configured value conflicts with the definition of CONFIG_PRV_INT32_NOT_FOUND");
                    result = R_RESULT_FAILED;
                }
            }

            /* Copy value */
            if (R_RESULT_SUCCESS == result)
            {
                return_value = (int32_t)temp_value;
                R_PRINT_INFO("Config param %s set to %i", section_key, return_value);
            }
        }
    }

    return return_value;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_CONFIG_GetDouble
 * @brief           Search the config for the section:key if it exists return the value in the config file
 *                  if it does not return the default_value
 * @param [in]      section - Name of the section in the config file
 * @param [in]      key - Name of the key in the config file
 * @param [in]      default_value - Value to return if the section:key does not exist
 * @retval          value to initialize configuration to
 *********************************************************************************************************************/
double R_COMMON_CONFIG_GetDouble(const char * const section, const char * const key, double default_value)
{
    r_result_t result = R_RESULT_SUCCESS;
    char section_key[CONFIG_PRV_SECTION_KEY_MAX_LENGTH] = {'\0'};
    double temp_value = 0.0;
    double return_value = default_value;

    /* Combine section and key into one string */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        if (combine_section_key(section, key, section_key) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error combining section and key");
            result = R_RESULT_FAILED;
        }
    }

    /* Loop through all config files to find section-key */
    if (R_RESULT_SUCCESS == result)
    {
        for (uint8_t config_index = 0; config_index < s_config_file_count; config_index++)
        {
            /* Init every loop */
            result = R_RESULT_SUCCESS;

            /* Check if entry exists */
            if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
            {
                if (iniparser_find_entry(sp_config_files[config_index], section_key) != 1)
                {
                    /* Note: This is not an error to report */
                    R_PRINT_INFO("Entry %s not found in config", section_key);
                    result = R_RESULT_FAILED;
                }
            }

            /* Get value */
            if (R_RESULT_SUCCESS == result)
            {
                temp_value = iniparser_getdouble(sp_config_files[config_index], section_key, CONFIG_PRV_DOUBLE_NOT_FOUND);

                if (CONFIG_PRV_DOUBLE_NOT_FOUND == temp_value) /*lint !e777 !e9137 */
                {
                    R_PRINT_ERROR("Error getting value. Please check if your configured value conflicts with the definition of CONFIG_PRV_DOUBLE_NOT_FOUND");
                    result = R_RESULT_FAILED;
                }
            }

            /* Copy value to pointer */
            if (R_RESULT_SUCCESS == result)
            {
                return_value = temp_value;
                R_PRINT_INFO("Config param %s set to %f", section_key, return_value);
            }
        }
    }

    return return_value;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_CONFIG_GetBool
 * @brief           Search the config for the section:key if it exists return the value in the config file
 *                  if it does not return the default_value
 * @param [in]      section - Name of the section in the config file
 * @param [in]      key - Name of the key in the config file
 * @param [in]      default_value - Value to return if the section:key does not exist
 * @retval          value to initialize configuration to
 *********************************************************************************************************************/
bool R_COMMON_CONFIG_GetBool(const char * const section, const char * const key, bool default_value)
{
    r_result_t result = R_RESULT_SUCCESS;
    char section_key[CONFIG_PRV_SECTION_KEY_MAX_LENGTH] = {'\0'};
    int temp_value = 0;
    bool return_value = default_value;

    /* Combine section and key into one string */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        if (combine_section_key(section, key, section_key) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error combining section and key");
            result = R_RESULT_FAILED;
        }
    }

    /* Loop through all config files to find section-key */
    if (R_RESULT_SUCCESS == result)
    {
        for (uint8_t config_index = 0; config_index < s_config_file_count; config_index++)
        {
            /* Init every loop */
            result = R_RESULT_SUCCESS;

            /* Check if entry exists */
            if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
            {
                if (iniparser_find_entry(sp_config_files[config_index], section_key) != 1)
                {
                    /* Note: This is not an error to report */
                    R_PRINT_INFO("Entry %s not found in config", section_key);
                    result = R_RESULT_FAILED;
                }
            }

            /* Get value */
            if (R_RESULT_SUCCESS == result)
            {
                temp_value = iniparser_getboolean(sp_config_files[config_index], section_key, CONFIG_PRV_BOOL_NOT_FOUND);

                if (CONFIG_PRV_BOOL_NOT_FOUND == temp_value)
                {
                    R_PRINT_ERROR("Error getting value.");
                    result = R_RESULT_FAILED;
                }
            }

            /* Check value */
            if (R_RESULT_SUCCESS == result)
            {
                if (0 == temp_value)
                {
                    return_value = false;
                }
                else if (1 == temp_value)
                {
                    return_value = true;
                }
                else
                {
                    R_PRINT_ERROR("Error value is out of range");
                    result = R_RESULT_FAILED;
                }
            }
        }
    }

    return return_value;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_CONFIG_GetString
 * @brief           Search the config for the section:key if it exists return the value in the config file
 *                  if it does not return the default_value
 * @param [in]      section - Name of the section in the config file
 * @param [in]      key - Name of the key in the config file
 * @param [in]      default_string - String to return if the section:key does not exist
 * @retval          value to initialize configuration to
 *********************************************************************************************************************/
const char * R_COMMON_CONFIG_GetString(const char * const section, const char * const key, const char * const default_string)
{
    r_result_t result = R_RESULT_SUCCESS;
    char section_key[CONFIG_PRV_SECTION_KEY_MAX_LENGTH] = {'\0'};
    const char * temp_value = NULL;
    const char * return_value = default_string;

    /* Combine section and key into one string */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        if (combine_section_key(section, key, section_key) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error combining section and key");
            result = R_RESULT_FAILED;
        }
    }

    /* Loop through all config files to find section-key */
    if (R_RESULT_SUCCESS == result)
    {
        for (uint8_t config_index = 0; config_index < s_config_file_count; config_index++)
        {
            /* Init every loop */
            result = R_RESULT_SUCCESS;

            /* Check if entry exists */
            if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
            {
                if (iniparser_find_entry(sp_config_files[config_index], section_key) != 1)
                {
                    /* Note: This is not an error to report */
                    R_PRINT_INFO("Entry %s not found in config", section_key);
                    result = R_RESULT_FAILED;
                }
            }

            /* Get value */
            if (R_RESULT_SUCCESS == result)
            {
                temp_value = iniparser_getstring(sp_config_files[config_index], section_key, CONFIG_PRV_STRING_NOT_FOUND);

                if (CONFIG_PRV_STRING_NOT_FOUND == temp_value)
                {
                    R_PRINT_ERROR("Error getting value.");
                    result = R_RESULT_FAILED;
                }
            }

            /* Check if the string is only the null character */
            if (R_RESULT_SUCCESS == result)
            {
                if (R_COMMON_STRING_Length(temp_value) == 1u)
                {
                    /* Note this is not an error to report */
                    R_PRINT_INFO("String is only NULL character");
                    result = R_RESULT_FAILED;
                }
            }

            /* Copy value */
            if (R_RESULT_SUCCESS == result)
            {
                return_value = temp_value;
                R_PRINT_INFO("Config param %s set to %s", section_key, return_value);
            }
        }
    }

    return return_value;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_CONFIG_GetEnum
 * @brief           Search the config for the section:key if it exists return the value in the config file
 *                  if it does not return the default_value. This function requires that the enum definition starts
 *                  at zero and increments by one. (i.e. there is no jumps between enum values)
 * @param [in]      section - Name of the section in the config file
 * @param [in]      key - Name of the key in the config file
 * @param [in]      default_value - Value to return if the section:key does not exist
 * @param [in]      p_enum_names - Array containing the enums as strings
 * @param [in]      enum_name_size - size of the p_enum_names array
 * @retval          value to initialize configuration to
 *********************************************************************************************************************/
int R_COMMON_CONFIG_GetEnum(const char * const section, const char * const key, int default_value, const char * p_enum_names[], uint32_t enum_name_size)
{
    r_result_t result = R_RESULT_SUCCESS;
    char section_key[CONFIG_PRV_SECTION_KEY_MAX_LENGTH] = {'\0'};
    const char * temp_string = NULL;
    int return_value = default_value;
    bool enum_found = false;

    /* Combine section and key into one string */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        if (combine_section_key(section, key, section_key) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error combining section and key");
            result = R_RESULT_FAILED;
        }
    }

    /* Loop through all config files to find section-key */
    if (R_RESULT_SUCCESS == result)
    {
        for (uint8_t config_index = 0; config_index < s_config_file_count; config_index++)
        {
            /* Init every loop */
            result = R_RESULT_SUCCESS;

            /* Check if entry exists */
            if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
            {
                if (iniparser_find_entry(sp_config_files[config_index], section_key) != 1)
                {
                    /* Note: This is not an error to report */
                    R_PRINT_INFO("Entry not found");
                    result = R_RESULT_FAILED;
                }
            }

            /* Get value */
            if (R_RESULT_SUCCESS == result)
            {
                temp_string = iniparser_getstring(sp_config_files[config_index], section_key, CONFIG_PRV_STRING_NOT_FOUND);

                if (CONFIG_PRV_STRING_NOT_FOUND == temp_string)
                {
                    R_PRINT_ERROR("Error getting value.");
                    result = R_RESULT_FAILED;
                }
            }

            /* Check if the string is only null character */
            if (R_RESULT_SUCCESS == result)
            {
                if (R_COMMON_STRING_Length(temp_string) == 1u)
                {
                    /* Note this is not an error to report */
                    R_PRINT_INFO("String is only NULL character");
                    result = R_RESULT_FAILED;
                }
            }

            /* Loop through enum and set  */
            if (R_RESULT_SUCCESS == result)
            {
                uint32_t enum_index;

                /* Loop though all enum string */
                for (enum_index = 0; enum_index < enum_name_size; enum_index++)
                {
                    if (R_COMMON_STRING_Compare(temp_string, p_enum_names[enum_index]) == R_RESULT_SUCCESS)
                    {
                        return_value = (int)enum_index;
                        enum_found = true;
                        R_PRINT_INFO("Config param %s set to %s", section_key, temp_string);
                        break;
                    }
                }

                /* Check if the string matched an enum name */
                if (false == enum_found)
                {
                    R_PRINT_ERROR("Enum name %s did not match known enumerations. Possible values are ... ", temp_string );

                    for (enum_index = 0; enum_index < enum_name_size; enum_index++)
                    {
                        R_PRINT("\t %s \n " , p_enum_names[enum_index]);
                    }

                    result = R_RESULT_FAILED;
                }
            }
        }
    }

    return return_value;
}
