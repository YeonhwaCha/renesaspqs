/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_rvx.c
 * @brief         Source file for common API for abstraction of RVX interactions.
 *********************************************************************************************************************/
/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include "r_common.h"

#include "generic_api_memory.h"
#include "r_common_rvx.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define RVX_PRV_VERBOSITY   (false)   /*!< True if we want verbose messages from RVX functions, false otherwise */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   read_file_to_mem
 * @brief           Reads in the binary file, allocating memory for the contents
 * @param [in]      filename - Name of the binary file to read in
 * @param [out]     p_mem - Pointer to the memory struct to fill in with the allocated memory addresses and size info
 * @retval          R_RESULT_SUCCESS - upon success
 *********************************************************************************************************************/
static r_result_t read_file_to_mem(const char* filename, st_rvx_mem_t * p_mem)
{
    /*lint --e{586} --e{593} --e{480} --e{481} --e{482} */

    r_result_t result = R_RESULT_SUCCESS;
    size_t filesize   = 0;
    FILE* p_file      = NULL;

    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        if (NULL == p_mem)
        {
            R_PRINT_ERROR("NULL input");
            result = R_RESULT_FAILED;
        }
    }

    /* Open file */
    if (R_RESULT_SUCCESS == result)
    {
        p_file = fopen(filename, "rb");

        if (NULL == p_file)
        {
            R_PRINT_ERROR("Error opening file %s", filename);
            result = R_RESULT_FAILED;
        }
    }

    /* Get size of file */
    if (R_RESULT_SUCCESS == result)
    {
        /* Go to the end of the file. */
        if (fseek(p_file, 0, SEEK_END) == 0) /*lint !e668 */
        {
            /* Get the size of the file. */
            int64_t temp_filesize = ftell(p_file); /*lint !e668 */

            if (temp_filesize > 0)
            {
                filesize = (size_t)temp_filesize;
            }
            else
            {
                R_PRINT_ERROR("Could not open file");
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Allocate our buffer for file */
    if (R_RESULT_SUCCESS == result)
    {
        p_mem->virt = gf_Malloc(filesize);

        if (NULL == p_mem->virt)
        {
            R_PRINT_ERROR("Could not allocate buffer for file");
            result = R_RESULT_FAILED;
        }
    }

    /* Read file into memory */
    if (R_RESULT_SUCCESS == result)
    {
        /* Go back to the start of the file. */
        if (fseek(p_file, 0, SEEK_SET) == 0) /*lint !e668 */
        {
            /* Read the entire file into memory. */
            if (fread(p_mem->virt, sizeof(char), filesize, p_file) != filesize) /*lint !e668 */
            {
                R_PRINT_ERROR("Did not read whole file to memory");
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Close file */
    if (R_RESULT_SUCCESS == result)
    {
        p_mem->size = filesize;
        p_mem->phys = gf_GetPhysAddr(p_mem->virt);

        if (0 != fclose(p_file)) /*lint !e668 */
        {
            R_PRINT_ERROR("Error closing file %s", filename);
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   patch_bcl_addresses
 * @brief           Patches the binary command list. Memory regions will be allocated and added to the memory list
 * @param [in,out]  p_bcl - Pointer to the command list
 * @param [in,out]  p_memlist - Pointer to the memory list address info
 * @retval          R_RESULT_SUCCESS - upon success
 *********************************************************************************************************************/
static r_result_t patch_bcl_addresses(st_rvx_mem_t * p_bcl, MemoryList* p_memlist)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Allocate memory for BCL regions */
    if (rvxt_bcl_alloc(p_bcl->virt, 0, p_memlist) == false)
    {
        R_PRINT_ERROR("Error patching BCL file");
        result = R_RESULT_FAILED;
    }

    if (rvxt_bcl_print(p_bcl->virt, 0) == false)
    {
        R_PRINT_ERROR("Error printing BCL file");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   patch_cnn_addresses
 * @brief           Patches the CNN command list addresses with the address in the memory list
 * @param [in,out]  p_cl - Pointer to the command list
 * @param [in]      p_memlist - Pointer to the memory list address info
 * @retval          R_RESULT_SUCCESS - upon success
 *********************************************************************************************************************/
static r_result_t patch_cnn_addresses(st_rvx_mem_t * p_cl, MemoryList* p_memlist)
{
    r_result_t result = R_RESULT_SUCCESS;
    MemoryListIterator* p_iterator = memoryListIteratorGet(p_memlist);

    while (NULL != p_iterator)
    {
        MemoryListEntry* p_entry = memoryListIteratorGetValue(p_memlist, p_iterator);

        if (r_cl_patch_CNN(p_cl->virt, p_entry->addressOld, p_entry->addressNew, p_entry->size, RVX_PRV_VERBOSITY) == false)
        {
            R_PRINT_ERROR("Error patching command list");
            result = R_RESULT_FAILED;
        }

        if (r_cl_patch_CNN_patchGOSUB(p_cl->virt, p_entry->addressOld, p_entry->addressNew, p_entry->size, RVX_PRV_VERBOSITY) == false)
        {
            R_PRINT_ERROR("Error patching command list");
            result = R_RESULT_FAILED;
        }

        p_iterator = p_iterator->next;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   patch_dma_addresses
 * @brief           Patches the DMA command list addresses with the address in the memory list
 * @param [in,out]  p_cl - Pointer to the command list
 * @param [in]      p_memlist - Pointer to the memory list address info
 * @retval          R_RESULT_SUCCESS - upon success
 *********************************************************************************************************************/
static r_result_t patch_dma_addresses(st_rvx_mem_t * p_cl, MemoryList* p_memlist)
{
    r_result_t result = R_RESULT_SUCCESS;
    MemoryListIterator* p_iterator = memoryListIteratorGet(p_memlist);

    while (NULL != p_iterator)
    {
        MemoryListEntry* p_entry = memoryListIteratorGetValue(p_memlist, p_iterator);

        if (r_cl_patch_DMA(p_cl->virt, p_entry->addressOld, p_entry->addressNew, p_entry->size, RVX_PRV_VERBOSITY) == false)
        {
            R_PRINT_ERROR("Error patching command list");
            result = R_RESULT_FAILED;
        }

        p_iterator = p_iterator->next;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   free_memory
 * @brief           Frees the memory contained in the memory structure and zeros out the structure elements
 * @param [in,out]  p_mem - Pointer to the memory structure
 *********************************************************************************************************************/
static void free_memory(st_rvx_mem_t * p_mem)
{
    gf_Free(p_mem->virt);
    p_mem->virt = NULL;
    p_mem->phys = 0;
    p_mem->size = 0;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_RVX_Init
 * @brief           Initializes the contents of the binary command list, CNN command list, and DMA command list.
 *                  These memory structs will be completely overwritten. Do not allocate memory to these structs
 *                  before calling this function.
 * @param [out]     p_bcl - Pointer to the binary command list memory structure
 * @param [out]     p_cnn_cl - Pointer to the CNN command list memory structure
 * @param [out]     p_dma_cl - Pointer to the DMA command list memory structure
 * @param [in]      blc_name - Filename of the binary command list file
 * @param [in]      cnn_bin_name - Filename of the CNN command list file
 * @param [in]      dma_bin_name - Filename of the DMA command list file
 * @retval          R_RESULT_SUCCESS - upon success
 *********************************************************************************************************************/
r_result_t R_COMMON_RVX_Init(st_rvx_mem_t * p_bcl, st_rvx_mem_t * p_cnn_cl, st_rvx_mem_t * p_dma_cl,
        const char * blc_name, const char * cnn_bin_name, const char * dma_bin_name )
{
    r_result_t result = R_RESULT_SUCCESS;
    MemoryList * p_mem_list = NULL;

    /* Read BCL File */
    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        if ((NULL != blc_name) && (NULL != p_bcl))
        {
            if (read_file_to_mem(blc_name, p_bcl) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Error reading BCL file");
                result = R_RESULT_FAILED;
            }
        }
        else
        {
            R_PRINT_ERROR("NULL input");
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
    }

    /* Read CNN File */
    if (R_RESULT_SUCCESS == result)
    {
        if ((NULL != cnn_bin_name) && (NULL != p_cnn_cl))
        {
            if (read_file_to_mem(cnn_bin_name, p_cnn_cl) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Error reading CNN file");
                result = R_RESULT_FAILED;
            }
        }
        else
        {
            R_PRINT_ERROR("NULL INPUT");
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
    }

    /* Read DMA File */
    if (R_RESULT_SUCCESS == result)
    {
        if ((NULL != dma_bin_name) && (NULL != p_dma_cl))
        {
            if (read_file_to_mem(dma_bin_name, p_dma_cl) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Error reading DMA file");
                result = R_RESULT_FAILED;
            }
        }
        else
        {
            R_PRINT_ERROR("NULL Input");
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
    }

    /* Create memory list */
    if (R_RESULT_SUCCESS == result)
    {
        p_mem_list = memoryListCreate();

        if (NULL == p_mem_list)
        {
            R_PRINT_ERROR("Error allocating memory list");
            result = R_RESULT_FAILED;
        }
    }

    /* Patch command lists */
    if (R_RESULT_SUCCESS == result)
    {
        /* Update bcl file */
        result |= patch_bcl_addresses(p_bcl, p_mem_list);

        /* Patch command lists with updated addresses in BCL files */
        result |= patch_dma_addresses(p_dma_cl, p_mem_list);
        result |= patch_cnn_addresses(p_cnn_cl, p_mem_list);

        /* Flush command lists after updating */
        gf_DCacheFlushRange(p_dma_cl->virt, p_dma_cl->size);
        gf_DCacheFlushRange(p_cnn_cl->virt, p_cnn_cl->size);

        /* Free memory list, this does not free the data planes in the memory list */
        memoryListFree(p_mem_list);
    }

    return result;
}

#if 0
/* Function for on the fly patching of command lists */
r_result_t R_COMMON_RVX_Patch(st_rvx_mem_t * p_cnn_cl, st_rvx_mem_t * p_dma_cl, uintptr_t old_address, uintptr_t new_address, size_t size)
{
    r_cl_patch_CNN(           p_cnn_cl->virt, old_address, new_address, size, RVX_PRV_VERBOSITY);
    r_cl_patch_CNN_patchGOSUB(p_cnn_cl->virt, old_address, new_address, size, RVX_PRV_VERBOSITY);
    r_cl_patch_DMA(           p_dma_cl->virt, old_address, new_address, size, RVX_PRV_VERBOSITY);

    gf_DCacheFlushRange((void*)p_dma_cl->virt, p_dma_cl->size);
    gf_DCacheFlushRange((void*)p_cnn_cl->virt, p_cnn_cl->size);
}
#endif

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_RVX_Execute
 * @brief           Executes the command lists and waits for their completion
 * @param [in]      p_bcl - Pointer to the binary command list memory structure
 * @param [in]      p_cnn_cl - Pointer to the CNN command list memory structure
 * @param [in]      p_dma_cl - Pointer to the DMA command list memory structure
 * @retval          R_RESULT_SUCCESS - upon success
 *********************************************************************************************************************/
r_result_t R_COMMON_RVX_Execute(st_rvx_mem_t * p_bcl, st_rvx_mem_t * p_cnn_cl, st_rvx_mem_t * p_dma_cl)
{
    r_result_t result = R_RESULT_SUCCESS;

    RCvIMPDRVCOREINFO core_info_cnn = {.CoreNum = 0, .CoreType = RCVDRV_CORE_TYPE_CNN};
    RCvIMPDRVCOREINFO core_info_dma = {.CoreNum = 0, .CoreType = RCVDRV_CORE_TYPE_DMAC};

    (void)p_bcl; // Not currently used

    /*
     * Note: we could use the BCL file to flush the cache before CNN execution then Invalidate the cache
     * after CNN execution. This would not work however if the command lists are being patched with updated
     * address of input/output planes, unless we also patched the bcl as well. Also it is probably beneficial
     * to have the cache handling in every CNN module to keep the handling the same as our other IMR/CVe/IMP modules.
     */

    /*
     * Note: If we do only the fly command list patching we should flush the command lists before executing.
     */

    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        if ((NULL == p_cnn_cl) ||
            (NULL == p_dma_cl))
        {
            R_PRINT_ERROR("NULL input");
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        result |= R_COMMON_HW_ExecuteIMP((uint32_t)p_cnn_cl->phys, &core_info_cnn);
        result |= R_COMMON_HW_ExecuteIMP((uint32_t)p_dma_cl->phys, &core_info_dma);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Error executing command lists");
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        result |= R_COMMON_HW_WaitIMP(&core_info_cnn);
        result |= R_COMMON_HW_WaitIMP(&core_info_dma);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Error waiting for command lists");
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_RVX_Deinit
 * @brief           Deinits the command lists. Freeing the memory contained in them and zeroing the address and size
 *                  info
 * @param [in,out]  p_bcl - Pointer to the binary command list memory structure
 * @param [in,out]  p_cnn_cl - Pointer to the CNN command list memory structure
 * @param [in,out]  p_dma_cl - Pointer to the DMA command list memory structure
 * @retval          R_RESULT_SUCCESS - upon success
 *********************************************************************************************************************/
r_result_t R_COMMON_RVX_Deinit(st_rvx_mem_t * p_bcl, st_rvx_mem_t * p_cnn_cl, st_rvx_mem_t * p_dma_cl)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        if ((NULL == p_bcl)    ||
            (NULL == p_cnn_cl) ||
            (NULL == p_dma_cl))
        {
            R_PRINT_ERROR("NULL input");
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* TODO need to free the memory planes inside the BCL */
        free_memory(p_bcl);
        free_memory(p_cnn_cl);
        free_memory(p_dma_cl);
    }

    return R_RESULT_SUCCESS;
}


