/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_string.c
 * @brief         Source file for common API to wrap all <string.h> functions to assure safe string usage.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/*lint --e{586} */

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include <string.h> /* For string functions */

#include "r_common.h"

/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Typedef definitions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Private global variables and functions
 *********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_STRING_Length
 * @brief           Returns the size of the string including the null termination. Note this is different than strlen()
 * @param [in]      string - String to be analyzed
 * @retval          size of string in bytes. Includes the NULL termination
 *********************************************************************************************************************/
size_t R_COMMON_STRING_Length(const char * const string)
{
    size_t length = 0u;

    if (NULL == string)
    {
        R_PRINT_ERROR("NULL input");
    }
    else
    {
        /* Note: strlen() returns length of string up to but not including null byte */
        length = strlen(string);
    }

    /* Account for the NULL termination */
    return length + 1u;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_STRING_Combine
 * @brief           Appends the source string to the end of the destination string. Destination string can be empty or not.
 *                  Note this is not the same API as strncat( ). dest_length is the total size of the destination string,
 *                  NOT how many bytes to copy
 * @param [in,out]  dest - Destination string Can be empty or already contain characters
 * @param [in]      source - Source string to append to the end of dest
 * @param [in]      dest_length - Maximum length the destination string can hold including the NULL termination.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_STRING_Combine(char * const dest, const char * const source, size_t dest_length)
{
    r_result_t result = R_RESULT_SUCCESS;
    size_t source_length       = 0;
    size_t current_dest_length = 0;

    if (0u == dest_length)
    {
        R_PRINT_ERROR("Cannot copy to length 0 string");
        result = R_RESULT_FAILED;
    }

    if ((NULL == source) || (NULL == dest)) /*lint --e{668} */
    {
        R_PRINT_ERROR("NULL input");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result )
    {
        /* Note: strlen() returns length of string up to but not including null byte */
        source_length = strlen(source);
        current_dest_length = strlen(dest);

        /* Check length of string, use less than because we need one byte for null termination */
        if ((current_dest_length + source_length) < dest_length)
        {
            /* Note: strncat() always add a null byte to dest */
            (void)strncat(dest, source, source_length);
        }
        else
        {
            /* Note: strncat() always add a null byte to dest */
            /* Cat the strings but leave room for the null termination */
            (void)strncat(dest, source, dest_length - (current_dest_length + 1u));
            R_PRINT_ERROR("Destination is not large enough for source \n"
                    "\t source = %s \n"
                    "\t current dest string = %s \n "
                    "\t total dest length = %lu",
                    source, dest, dest_length );
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_STRING_Copy
 * @brief           Copies the source string to the end of the destination string. If the destination already contains
 *                  data it will be overwritten. Note this is not the same API as strncpy(). dest_length is the total
 *                  size of the destination string, NOT how many bytes to copy. This function always assure that the
 *                  destination string is NULL terminated.
 * @param [out]     dest - Destination string. Can be empty or already contain characters
 * @param [in]      source - Source string to copy into dest
 * @param [in]      dest_length - Maximum size of the destination string can hold including the NULL termination.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_STRING_Copy(char * const dest, const char * const source, size_t dest_length)
{
    r_result_t result = R_RESULT_SUCCESS;
    size_t source_length = 0;

    if (0u == dest_length)
    {
        R_PRINT_ERROR("Cannot copy to length 0 string");
        result = R_RESULT_FAILED;
    }

    if ((NULL == source) || (NULL == dest)) /*lint --e{668} */
    {
        R_PRINT_ERROR("NULL input");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result )
    {
        /* Note: strlen() returns length of string up to but not including null byte */
        source_length = strlen(source);

        /* Note: strncpy() does not add a null byte to dest if source is longer than dest_length*/
        (void)strncpy(dest, source, dest_length);

        /* Check length of string, use source + 1 because we need room for null termination */
        if ((source_length + 1u) > dest_length)
        {
            /* Copy what we can but make sure destination is always null terminated */
            dest[dest_length - 1u] = '\0';

            R_PRINT_ERROR("Source is greater than destination length. \n"
                    "\t source = %s \n"
                    "\t dest length = %lu",
                    source, dest_length);
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_STRING_Compare
 * @brief           Compares string1 to sting2.
 * @param [in]      string1 - First string to compare
 * @param [in]      string2 - Second string to compare
 * @retval          R_RESULT_SUCCESS if both strings are the same, R_RESULT_FAILED otherwise.
 *********************************************************************************************************************/
r_result_t R_COMMON_STRING_Compare(const char * const string1, const char * const string2)
{
    r_result_t result = R_RESULT_FAILED;
    size_t string1_length = 0;
    size_t string2_length = 0;

    if ((NULL == string1) || (NULL == string2))
    {
        R_PRINT_ERROR("NULL input");
    }
    else
    {
        /* Note: strlen() returns length of string up to but not including null byte */
        string1_length = strlen(string1);
        string2_length = strlen(string2);

        /* Check if lengths are equal, quick way to break out */
        if (string1_length == string2_length)
        {
            /* Check if strings are equal */
            if (strncmp(string1, string2, string1_length) == 0)
            {
                result = R_RESULT_SUCCESS;
            }
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_STRING_Split
 * @brief           Splits a string into words that are separated by 'separator'
 * @param [in]      string - Input string
 * @param [out]     p_list - Pointer to the output word struct to be populated.
 * @param [in]      separator - Character that separated words in 'string'
 * @retval          R_RESULT_SUCCESS on success.
 *********************************************************************************************************************/
r_result_t R_COMMON_STRING_Split(const char * const string, st_word_list_t * p_list, char separator)
{
    r_result_t result = R_RESULT_SUCCESS;
    const char * current_word = string;
    const char * next_word = NULL;
    uint32_t current_word_length = 0;

    if ((NULL == string) || (NULL == p_list))
    {
        R_PRINT_ERROR("NULL input");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result )
    {
        /* Init the number of names in list */
        p_list->num_words = 0;

        /* Loop until we are done processing the list */
        while (true) /*lint !e716 */
        {
            /* Find the next word */
            next_word = strchr(current_word, (int)separator);

            if (NULL != next_word)
            {
                /* Get the length of the current word */
                current_word_length = (uint32_t)((uintptr_t)next_word - (uintptr_t)current_word); /*lint !e9078 !e923 !e9091 */ //TODO refactor pointer math

                if (0u == current_word_length)
                {
                    /* Multiple separator values in a row disregard */
                }
                else if (R_STRING_MAX_WORD_LENGTH > current_word_length)
                {
                    /* Copy string to word list */
                    (void)strncpy(&p_list->word_list[p_list->num_words][0], current_word, R_STRING_MAX_WORD_LENGTH);

                    /* Word needs to be null terminated */
                    p_list->word_list[p_list->num_words][current_word_length] = '\0';

                    /* Increment word counter */
                    p_list->num_words++;
                }
                else
                {
                    /* Copy string to word list */
                    (void)strncpy(&p_list->word_list[p_list->num_words][0], current_word, R_STRING_MAX_WORD_LENGTH);

                    /* Word needs to be null terminated */
                    p_list->word_list[p_list->num_words][R_STRING_MAX_WORD_LENGTH - 1u] = '\0';

                    /* Increment word counter */
                    p_list->num_words++;

                    R_PRINT_ERROR("Word is too long");
                    result = R_RESULT_FAILED;
                }

                /* Set the current word to the next word starting after the separator */
                current_word = &next_word[1];

                /* Check for overruns */
                if (R_STRING_MAX_NUM_WORDS == p_list->num_words)
                {
                    R_PRINT_ERROR("Too many words in string");
                    result = R_RESULT_FAILED;
                    break;
                }
            }
            else
            {
                /* Check if the current word is the separator value or null termination. We can disregard these */
                if ((current_word[0] != separator) && ('\0' != current_word[0]))
                {
                    /* Current word is the last word */
                    result |= R_COMMON_STRING_Copy(&p_list->word_list[p_list->num_words][0], current_word, R_STRING_MAX_WORD_LENGTH);

                    /* Word needs to be null terminated */
                    p_list->word_list[p_list->num_words][R_STRING_MAX_WORD_LENGTH - 1u] = '\0';

                    /* Increment word counter */
                    p_list->num_words++;
                }

                /* We are finished processing the word list */
                break; /*lint !e9011 */
            }
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_STRING_WordInList
 * @brief           Checks if the given word is contained in the given word list
 * @param [in]      word - Word to check for
 * @param [in]      list - LIst to check
 * @retval          R_RESULT_SUCCESS if word is in list, R_RESULT_FAILED otherwise
 *********************************************************************************************************************/
r_result_t R_COMMON_STRING_WordInList(const char * const word, const st_word_list_t * list)
{
    r_result_t result = R_RESULT_SUCCESS;

    if ((NULL == word) || (NULL == list))
    {
        R_PRINT_ERROR("NULL input");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result )
    {
        result = R_RESULT_FAILED;

        for (uint32_t word_index = 0; word_index < list->num_words; word_index++)
        {
            if (R_COMMON_STRING_Compare(&list->word_list[word_index][0], word) == R_RESULT_SUCCESS)
            {
                result = R_RESULT_SUCCESS;
                break;
            }
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_STRING_AppendIfNeeded
 * @brief           If newChar is not already the last char in dest, it will be appended.
 * @param [out]     dest - String where to append the new char. Can be empty or already containing characters.
 * @param [in]      newChar - Char to be appended if not already present
 * @param [in]      dest_length - Maximum size of the destination string can hold including the NULL termination.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_STRING_AppendIfNeeded(char * const dest, const char newChar, size_t dest_length)
{
    r_result_t result = R_RESULT_SUCCESS;
    size_t current_dest_length = 0;
    char lastChar = '\0';
    bool notPresent = true;

    /* Check for NULL pointer */
    if (NULL == dest)
    {
        R_PRINT_ERROR("NULL pointer to destination string.");
        result = R_RESULT_FAILED;
    }

    /* Check destination length */
    if (0u == dest_length)
    {
        R_PRINT_ERROR("Cannot copy to length 0 string.");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* Check the current length of the destination string */
        /* Note: strlen() returns length of string up to but not including null byte */
        current_dest_length = strlen(dest);

        /* If the destination string is empty, newChar must be appended. If it's not empty, maybe it's already the last char */
        if (current_dest_length > 0u)
        {
            /* Extract last char from string */
            lastChar = dest[current_dest_length - 1u];

            /* If newChar doesn't match lastChar, it must be appended */
            if (lastChar == newChar)
            {
                notPresent = false;
            }
        }

        if (notPresent)
        {
            /* Check length of string, use less than because we need one byte for null termination */
            if ((current_dest_length + 1u) < dest_length)
            {
                /* Append newChar to the end of the destination string */
                dest[current_dest_length] = newChar;

                /* Add null char at the end */
                dest[current_dest_length + 1u] = '\0';
            }
            else
            {
                R_PRINT_ERROR("Destination is not large enough.");
                result = R_RESULT_FAILED;
            }
        }
    }

    return result;
}
