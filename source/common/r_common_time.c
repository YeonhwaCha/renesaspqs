/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_time.c
 * @brief         Source file for common API of various time and timing related functions.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include <time.h>     /* For time functions */
#include <sys/time.h> /* For time functions */

#include "r_common.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   get_time
 * @brief           Returns the current time
 * @param [in]      clock_id - Which clock to use to get time
 * @retval          current time in microseconds
 *********************************************************************************************************************/
static uint64_t get_time(clockid_t clock_id)
{
    struct timespec current_time = {0};
    uint64_t current_time_usec = 0;

    if ((-1) == clock_gettime(clock_id, &current_time))
    {
        R_PRINT_ERROR("Error getting time");
        current_time_usec = 0;
    }
    else
    {
        current_time_usec = ((uint64_t)current_time.tv_sec * 1000000ul) + ((uint64_t)current_time.tv_nsec / 1000ul);
    }

    return current_time_usec;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_TIME_GetRealUsec
 * @brief           Returns the current real time
 * @retval          Current real time in microseconds
 *********************************************************************************************************************/
uint64_t R_COMMON_TIME_GetRealUsec(void)
{
    return get_time(CLOCK_REALTIME);
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_TIME_GetBootUsec
 * @brief           Returns the current boot time
 * @retval          Current boot time in microseconds
 *********************************************************************************************************************/
uint64_t R_COMMON_TIME_GetBootUsec(void)
{
    return get_time(CLOCK_MONOTONIC);
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_TIME_ConvertBootToReal
 * @brief           Converts boot time to real time
 * @param [in]      boot_time_stamp - time stamp in micro seconds
 * @retval          boot time converted to real time in microseconds
 *********************************************************************************************************************/
uint64_t R_COMMON_TIME_ConvertBootToReal(uint64_t boot_time_stamp)
{
    uint64_t real_time_stamp = 0;
    uint64_t current_real_time_usec = R_COMMON_TIME_GetRealUsec();
    uint64_t current_boot_time_usec = R_COMMON_TIME_GetBootUsec();

    if ((0u == current_real_time_usec) || (0u == current_boot_time_usec))
    {
        R_PRINT_ERROR("Error getting boot and real times");
    }
    else
    {
        if (current_boot_time_usec > current_real_time_usec)
        {
            R_PRINT_ERROR("Error times are not valid");
        }
        else
        {
            real_time_stamp = boot_time_stamp + (current_real_time_usec - current_boot_time_usec);
        }
    }

    return real_time_stamp;
}


/******************************************************************************************************************//**
 * Function Name:   R_COMMON_TIME_ConvertRealToBoot
 * @brief           Converts real time to boot time
 * @param [in]      real_time_stamp - time stamp in micro seconds
 * @retval          real time converted to boot time in microseconds
 *********************************************************************************************************************/
uint64_t R_COMMON_TIME_ConvertRealToBoot(uint64_t real_time_stamp)
{
    uint64_t boot_time_stamp = 0;
    uint64_t current_real_time_usec = R_COMMON_TIME_GetRealUsec();
    uint64_t current_boot_time_usec = R_COMMON_TIME_GetBootUsec();

    if ((0u == current_real_time_usec) || (0u == current_boot_time_usec))
    {
        R_PRINT_ERROR("Error getting boot and real times");
    }
    else
    {
        if ((real_time_stamp < (current_real_time_usec - current_boot_time_usec)) || (current_boot_time_usec > current_real_time_usec))
        {
            R_PRINT_ERROR("Error times are not valid");
        }
        else
        {
            boot_time_stamp = real_time_stamp - (current_real_time_usec - current_boot_time_usec);
        }
    }

    return boot_time_stamp;
}
