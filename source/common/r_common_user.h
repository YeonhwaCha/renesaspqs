/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_user.h
 * @brief         Header file for common API to allow modules to register a keystroke the user can press to interact
 *                with a particular module.  Modules can then query whenever a registered key is pressed by the user.
 * @defgroup      PQS_Common_User PQS Common User
 * @section       PQS_Common_User_Summary Module Summary
 *                This allows modules to register a keystroke the user can press to interact with a particular module.
 *                Modules can then query whenever a registered key is pressed by the user.
 * @ingroup       PQS_Common
 * @{
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#ifndef R_COMMON_USER_H_
    #define R_COMMON_USER_H_

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define USER_INPUT_NOT_AVAILABLE    ('\0') /*!< This is what is return if no user input is available */

#define ESCAPE_KEY     (27)  /*!< Escape character that comes before arrow keys */
#define PRE_ARROW      (91)  /*!< Another character that comes before arrow keys */
#define ARROW_OFFSET   (128) /*!< Add an offset to map arrow keys to the into the special character ASCII section
                              * Otherwise the arrow keys map to 'A', 'B', 'C', and 'D' */

#define SPACE_BAR    (' ')                        /*!< Define space bar user input */
#define UP_ARROW     ((char)(65 + ARROW_OFFSET))  /*!< Define up arrow user input */
#define DOWN_ARROW   ((char)(66 + ARROW_OFFSET))  /*!< Define down arrow user input */
#define RIGHT_ARROW  ((char)(67 + ARROW_OFFSET))  /*!< Define right arrow user input */
#define LEFT_ARROW   ((char)(68 + ARROW_OFFSET))  /*!< Define left arrow user input */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global variables
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 **********************************************************************************************************************/

r_result_t R_COMMON_USER_Init(void);
r_result_t R_COMMON_USER_Deinit(void);
void       R_COMMON_USER_LoopOnInput(const char * p_preinput, size_t preinput_size);
r_result_t R_COMMON_USER_RegisterInput(st_module_instance_t * const p_instance, const char user_input, const char * const description);
char       R_COMMON_USER_GetInput(st_module_instance_t * const p_instance);

#endif /* R_COMMON_USER_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Common>
 *********************************************************************************************************************/

