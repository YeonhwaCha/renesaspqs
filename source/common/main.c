/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          main.c
 * @brief         main program loop
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include <unistd.h> /* For getopt */

#include "r_common.h"
#include "r_common_version.h"
#include "r_module_list.h"
#include "r_object_list.h"
#include "r_network_parser.h"

/**********************************************************************************************************************
 Local Typedef definitions
 *********************************************************************************************************************/

/*! Structure defining the command line options definition */
typedef struct {
    const char  option[3];  /*!< Option character */
    const char  help[1024]; /*!< Help description of what the option does */
    const char  use[1024];  /*!< Example usage of the option */
} st_argv_definition_t;

/**********************************************************************************************************************
 Local variables
 *********************************************************************************************************************/

static char s_pre_keypress[32];       /*!< Array containing all of the user inputs entered via command line options */
static uint32_t s_pre_keypress_count; /*!< Size of entries in the s_pre_keypress array */

/*! List defining all of the the command line options used by the program */
static const st_argv_definition_t s_argv_definition_list[] = {
    {"-k", "Simulates immediate user keypresses",    "-k 'c' "},
    {"-g", "Specify graph files",                    "-g my_graph.rgf"},
    {"-c", "Specify config files",                   "-c my_config.ini"},
    {"-r", "Specify an output channel to record",    "-r camera.uyvy_out"},
    {"-h", "Prints this help and exits",             "-h"},
    {"-v", "Verbosity level 0 (all) - 5 (disabled)", "-v0"},
    {"-l", "Log to file instead of stdout",          "-l"},
};

/**********************************************************************************************************************
 Functions
 *********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   init_preprocess_keys
 * @brief           Init preprocess key storage variables
 *********************************************************************************************************************/
static void init_preprocess_keys(void)
{
    for (uint32_t i = 0; i < (sizeof(s_pre_keypress)); i++)
    {
        s_pre_keypress[i] = '\0';
    }

    s_pre_keypress_count = 0;
}

/******************************************************************************************************************//**
 * Function Name:   save_keypress
 * @brief           Saves a list of keypresses for later processing
 * @param [in]      p_keys - Array containing keypress characters
 * @retval          R_RESULT_SUCCESS on success, R_RESULT_FAILED otherwise
 *********************************************************************************************************************/
static r_result_t save_keypress(char * p_keys)
{
    r_result_t result = R_RESULT_SUCCESS;

    for (uint32_t i = 0; '\0' != p_keys[i]; i++)
    {
        if ((sizeof(s_pre_keypress)) > s_pre_keypress_count)
        {
            s_pre_keypress[s_pre_keypress_count] = p_keys[i];
            s_pre_keypress_count++;
        }
        else
        {
            R_PRINT_ERROR("Too many keypress options");
            result = R_RESULT_FAILED;
            break;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   print_help
 * @brief           Prints help info for command line argument usage
 *********************************************************************************************************************/
static void print_help(void)
{
    uint32_t num_options = (uint32_t)((sizeof(s_argv_definition_list))/(sizeof(s_argv_definition_list[0])));

    for (uint32_t i = 0; i < num_options; i++)
    {
        R_PRINT("Option: %s \n",  s_argv_definition_list[i].option);
        R_PRINT("\t Help: %s\n",  s_argv_definition_list[i].help);
        R_PRINT("\t Usage: %s\n", s_argv_definition_list[i].use);
    }
}

/******************************************************************************************************************//**
 * Function Name:   process_argv
 * @brief           Processes all of the command line arguments
 * @param [in]      argc - Size of argument vector
 * @param [in]      argv - Argument vector containing command line arguments
 * @retval          R_RESULT_SUCCESS on success, R_RESULT_FAILED otherwise
 *********************************************************************************************************************/
static r_result_t process_argv(int argc, char ** argv)
{
    r_result_t result = R_RESULT_SUCCESS;
    bool exit_loop = false;

    while (!exit_loop)
    {
        int opt = getopt(argc, argv, ":c:g:r:k:v:lh");

        switch (opt)
        {
            /* Finished processing command line arguments */
            case -1:
            {
                exit_loop = true;
                break;
            }

            /* Configuration files */
            case (int)'c':
            {
                result |= R_COMMON_CONFIG_AddFile(optarg);
                break;
            }

            /* Graph files */
            case (int)'g':
            {
                result |= R_NETWORK_PARSER_AddFile(optarg);
                break;
            }

            /* Record channel */
            case (int)'r':
            {
                result |= R_NETWORK_PARSER_RecordChannel(optarg);
                break;
            }

            /* Command line key presses */
            case (int)'k':
            {
                result |= save_keypress(optarg);
                break;
            }

            /* Set verbosity level */
            case (int)'v':
            {
                result |= R_COMMON_LOG_SetLevel((e_log_level_t)atoi(optarg)); /*lint !e586 !e9030 !e9034 */
                break;
            }

            /* Log to file */
            case (int)'l':
            {
                result |= R_COMMON_LOG_SetLogFile();
                break;
            }

            /* Help */
            case (int)'h':
            {
                /* print_help(); */
                /* We don't actually have to print help, it will be done later */
                result = R_RESULT_FAILED;
                exit_loop = true;
                break;
            }

            /* Option without argument */
            case (int)':':
            {
                R_PRINT_ERROR("Option -%c requires an operand", optopt);
                result = R_RESULT_FAILED;
                exit_loop = true;
                break;
            }

            /* Unknown option */
            case (int)'?':
            {
                R_PRINT_ERROR("Unknown option -%c", optopt);
                result = R_RESULT_FAILED;
                exit_loop = true;
                break;
            }

            default:
            {
                R_PRINT_ERROR("Fell through switch");
                result = R_RESULT_FAILED;
                exit_loop = true;
                break;
            }
        }
    }

    /* optind is for the extra arguments that are not parsed */
    for (; optind < argc; optind++)
    {
        R_PRINT_ERROR("Extra program arguments: %s", argv[optind]);
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_FAILED == result)
    {
        print_help();
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   main
 * @brief           Entry point of program. Reads program arguments. Does initialization, loops until user aborts,
 *                  then does deinitialization.
 * @param [in]      argc - Size of argument vector
 * @param [in]      argv - Argument vector containing command line arguments
 * @retval          0 on success
 *********************************************************************************************************************/
int main(int argc, char* argv[])
{ /*lint !e9075 */
    r_result_t result = R_RESULT_SUCCESS;

    init_preprocess_keys();

    /* Pre-initialization */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        result |= R_COMMON_LOG_Preinit();

        R_PRINT("\n"
                "################################################################################\n"
                "# \n"
                "# \t Renesas PQS Hello... \n"
                "# \n"
                "################################################################################\n"
                "\n");

        R_COMMON_VERSION_Print();

        result |= R_COMMON_CONFIG_Init();
        result |= R_NETWORK_PARSER_Init();
    }

    /* Process program arguments */
    if (R_RESULT_SUCCESS == result)
    {
        result |= process_argv(argc, argv);
    }

    /* Main initialization */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_COMMON_Init();
        result |= R_OBJECT_Init(gp_object_list, g_object_list_size);
        result |= R_MODULE_Init(gp_module_list, g_module_list_size);
        result |= R_BROKER_Init();
        result |= R_TIMER_Init();
        result |= R_NETWORK_PARSER_Start();
        result |= R_MODULE_Start();
    }

    /* Start the pipeline */
    if (R_RESULT_SUCCESS == result)
    {
        R_PRINT("\n"
                "################################################################################\n"
                "# \n"
                "# \t Renesas PQS entering main loop... \n"
                "# \n"
                "################################################################################\n"
                "\n");

        result |= R_NETWORK_PARSER_Trigger();
    }

    /* Main loop */
    if (R_RESULT_SUCCESS == result)
    {
        /*
         * Note: This function blocks until user quits process.
         */

        R_COMMON_USER_LoopOnInput(s_pre_keypress, s_pre_keypress_count);

        R_PRINT("\n"
                "################################################################################\n"
                "# \n"
                "# \t Renesas PQS exiting main loop... \n"
                "# \n"
                "################################################################################\n"
                "\n");

        /* Do deinitialization */
        result |= R_MODULE_Stop();
        result |= R_MODULE_Deinit();
        result |= R_BROKER_Deinit();
        result |= R_TIMER_Deinit();
        result |= R_OBJECT_Deinit();
        result |= R_COMMON_CONFIG_Deinit();
        result |= R_COMMON_Deinit();
    }

    R_PRINT("\n"
            "################################################################################\n"
            "# \n"
            "# \t Renesas PQS Goodbye... \n"
            "# \n"
            "################################################################################\n"
            "\n");

    return (int)result;
}
