/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_network_parser.h
 * @brief         Header file for text file parsing to determine the PQS graph on startup.
 * @defgroup      PQS_Framework_Network_Parser PQS Framework Network Parser
 * @section       PQS_Framework_Network_Parser_Summary Module Summary
 *                Parses a text file to determine the PQS graph on startup.
 * @ingroup       PQS_Framework
 * @{
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/
#ifndef R_NETWORK_PARSER_H_
#define R_NETWORK_PARSER_H_

/**********************************************************************************************************************
 External global functions
 *********************************************************************************************************************/

extern r_result_t R_NETWORK_PARSER_Init(void);
extern r_result_t R_NETWORK_PARSER_AddFile(char const * const fname);
extern r_result_t R_NETWORK_PARSER_RecordChannel(char const * const channel_name);
extern r_result_t R_NETWORK_PARSER_Start(void);
extern r_result_t R_NETWORK_PARSER_Trigger(void);

#endif /* R_NETWORK_PARSER_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Framework>
 *********************************************************************************************************************/
