/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/

/******************************************************************************************************************//**
 * @file          r_broker.h
 * @brief         Header file for data broker in the PQS Framework
 * @defgroup      PQS_Framework PQS Framework
 * @brief         Framework for handling module and object interfaces
 * @defgroup      PQS_Framework_Broker PQS Framework Broker
 * @section       PQS_Framework_Broker_Summary Module Summary
 *                The PQS Frame work data broker is responsible for routing objects from one module to the next
 *                as defined by the graph.
 * @ingroup       PQS_Framework
 * @{
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

#ifndef R_BROKER_H_
#define R_BROKER_H_

/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

#define R_BROKER_DATA_DIRECTION_UNUSED  (0x00u) /*!< Value for unused objects */
#define R_BROKER_DATA_DIRECTION_IN      (0x01u) /*!< Value for pure input objects */
#define R_BROKER_DATA_DIRECTION_OUT     (0x02u) /*!< Value for pure output objects*/
#define R_BROKER_DATA_DIRECTION_INOUT   (0x04u) /*!< Value for in/out objects. These are objects that are received as input but are also altered by the processing module */

/*! Combination of all input object directions */
#define R_BROKER_DATA_DIRECTION_BITMASK_ANY_INPUTS  (R_BROKER_DATA_DIRECTION_IN | R_BROKER_DATA_DIRECTION_INOUT)

/*! Combination of all output object directions */
#define R_BROKER_DATA_DIRECTION_BITMASK_ANY_OUTPUTS (R_BROKER_DATA_DIRECTION_OUT | R_BROKER_DATA_DIRECTION_INOUT)

/******************************************************************************/
/*                 These calls are needed by main                             */
/******************************************************************************/

extern r_result_t R_BROKER_Init(void);
extern r_result_t R_BROKER_Deinit(void);

/******************************************************************************/
/*                 These calls are needed by module instances                 */
/******************************************************************************/

/* Lets broker know what channels to expect. Called in each modules init function */
extern r_result_t R_BROKER_InitChannelInfo(st_module_instance_t * const p_instance, uint32_t channel, char const * const name, e_object_type_t type, uint32_t direction);

/* Returns whatever is queued in object for given mode */
extern st_object_t * R_BROKER_GetObjectPtr(st_module_instance_t * const p_instance, uint32_t channel);

/* Replaces the current output object with the given the object */
extern r_result_t R_BROKER_ReplaceOutputObject(st_module_instance_t * const p_instance, uint32_t channel, st_object_t * const p_object);

/******************************************************************************/
/*                These calls are needed to build graph                       */
/******************************************************************************/

/* Returns the ID of the specified channel name.  Normally should not need to use this  */
extern r_result_t R_BROKER_GetChannelByName(st_module_instance_t * const p_instance, char const * const channel_name, uint32_t * p_channel);

/* Link a source channel to a sink channel */
extern r_result_t R_BROKER_Link(st_module_instance_t * const p_source, uint32_t source_channel, st_module_instance_t * const p_sink, uint32_t sink_channel);
extern r_result_t R_BROKER_LinkByName(char const * const source_name, char const * const source_channel_name, char const * const sink_name, char const * const sink_channel_name);

/* Sends a trigger object to specified instance channel.  Sometimes needed to "jump start" processing. */
extern r_result_t R_BROKER_Trigger(st_module_instance_t * const p_sink, uint32_t sink_channel);
extern r_result_t R_BROKER_TriggerByName(char const * const sink_name, char const * const sink_channel_name);

/* Prints all the channels for all instance  */
extern void R_BROKER_PrintChannels(void);
#endif /* R_BROKER_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Framework>
 *********************************************************************************************************************/
