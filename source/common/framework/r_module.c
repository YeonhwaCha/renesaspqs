/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_module.c
 * @brief         Source file for PQS Framework module handling
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#define _GNU_SOURCE /*!< Needed for thread naming */
#include <pthread.h> /* For thread creation, mutexes and signals */

#include "r_common.h"

/*! Define framework core, otherwise r_module_broker.h will throw an error */
#define _R_FRAMEWORK_CORE_
#include "r_module_broker.h"

/**********************************************************************************************************************
 Local Typedef definitions
 *********************************************************************************************************************/

/*! All the global data for the module framework */
typedef struct {
    e_module_state_t                      state;                             /*!< State of this module */
    st_module_interface_t const * const * pp_module_interfaces;              /*!< Registered modules */
    uint32_t                              module_count;                      /*!< How big is pp_module_interfaces */
    st_module_instance_t                  instances[R_MODULE_MAX_INSTANCES]; /*!< Active instances */
    uint32_t                              instance_count;                    /*!< How many instances? */
} st_module_t;

/**********************************************************************************************************************
 Local Variables
 *********************************************************************************************************************/

static st_module_t s_mod; /*!< All of the module framework's global data */

/**********************************************************************************************************************
 Functions
 *********************************************************************************************************************/

/* TODO document static functions */
/*! @cond STATIC */

static r_result_t check_string_length(char const * const str, const size_t max_length)
{
    r_result_t result = R_RESULT_SUCCESS;
    size_t length = R_COMMON_STRING_Length(str);

    if (0u == length)
    {
        /* String too short */
        result = R_RESULT_FAILED;
    }
    else if (max_length < length)
    {
        /* String too large */
        result = R_RESULT_FAILED;
    }
    else
    {
        /* String just right */
        result = R_RESULT_SUCCESS;
    }

    return result;
}


static r_result_t check_name(st_module_instance_t * const p_instance)
{
    r_result_t result = check_string_length(p_instance->name, R_MODULE_INSTANCE_NAME_SIZE);

    if (R_RESULT_SUCCESS != result)
    {
        R_PRINT_ERROR("Invalid length");
    }

    return result;
}

static r_result_t check_interface(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NULL == p_instance)
    {
        result = R_RESULT_FAILED;
        R_PRINT_ERROR("Invalid null pointer");
    }
    else if (NULL == p_instance->p_module_interface)
    {
        /* Null pointer, interface was not defined */
        result = R_RESULT_FAILED;
        R_PRINT_ERROR("Invalid null pointer");
    }
    else
    {
        /* MISRA */
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = check_string_length(p_instance->p_module_interface->module_name, R_MODULE_NAME_SIZE);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Invalid length");
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if ((NULL == p_instance->p_module_interface->init_channels) ||
            (NULL == p_instance->p_module_interface->process_data))
        {
            result = R_RESULT_ILLEGAL_NULL_POINTER;
            R_PRINT_ERROR("Didn't define proper callbacks for module %s", p_instance->name);
        }
    }

    /* TODO: Do we need to check channel_info struct? */

    return result;
}

static r_result_t check_module(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* TODO: Should we check anything here? */
    (void)p_instance;

    return result;
}

static r_result_t check_broker(st_module_instance_t * p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* TODO: Should we check anything here? */
    (void)p_instance;

    return result;
}

static st_module_instance_t * add_instance_to_list(char const * const instance_name, st_module_interface_t const * const p_module_interface)
{
    st_module_instance_t * p_instance = NULL;

    /* Is there already an instance with this name? */
    if (R_MODULE_GetInstanceByName(instance_name) != NULL)
    {
        /* Instance with given name already exists! */
        R_PRINT_ERROR("Could not add instance \"%s.%s\" to list, an existimng module already exists!", p_module_interface->module_name, instance_name);
    }

    /* Do we have room for a new module instance?  Fail if we don't */
    else if (R_MODULE_MAX_INSTANCES <= s_mod.instance_count)
    {
        /* Ran out of room in the instances list */
        R_PRINT_ERROR("Ran out of space for instance count.  Max allowable count: %d.  Could not add instance \"%s\"", instance_name, R_MODULE_MAX_INSTANCES);
    }

    /* Add if successful */
    else
    {
        /* Copy instance pointer */
        p_instance = &(s_mod.instances[s_mod.instance_count]);

        /* Increment instance count */
        s_mod.instance_count++;
    }

    return p_instance;
}

static void * module_thread(void * const p_pthread_data)
{
    st_module_instance_t * p_instance = (st_module_instance_t *)(p_pthread_data);
    r_result_t result = R_RESULT_FAILED;
    R_PRINT_INFO("Instance %s thread attached", p_instance->name);


    if (MODULE_STATE_STARTING == p_instance->module.state)
    {
        p_instance->module.state = MODULE_STATE_STARTED;
        result = R_RESULT_SUCCESS;
    }

    R_PRINT_INFO("Instance %s STARTED", p_instance->name);

    while (MODULE_STATE_STARTED == p_instance->module.state)
    {
        result = R_RESULT_SUCCESS;

        (void)pthread_mutex_lock(&(p_instance->algo.lock));

        /* Wait for data */
        while ((MODULE_STATE_STARTED == p_instance->module.state) && (ALGORITHM_HAS_DATA != p_instance->algo.state))
        {
            /* Wait until i/o becomes available */
            (void)pthread_cond_wait(&(p_instance->algo.signal), &(p_instance->algo.lock));
        }

        (void)pthread_mutex_unlock(&(p_instance->algo.lock));

        if (MODULE_STATE_STARTED == p_instance->module.state)
        {
            /* About to process new data. Update state */
            (void)pthread_mutex_lock(&(p_instance->algo.lock));
            p_instance->algo.state = ALGORITHM_BUSY;
            (void)pthread_mutex_unlock(&(p_instance->algo.lock));

            /* Start time analysis */
            (void)R_TIMER_StartAnalysis(p_instance);

            /* Call processing function */
            result = p_instance->p_module_interface->process_data(p_instance);

            /* Stop and print time analysis */
            (void)R_TIMER_StopAnalysis(p_instance);
            (void)R_TIMER_PrintAnalysis(p_instance);

            /* Processing done. Update state */
            (void)pthread_mutex_lock(&(p_instance->algo.lock));
            p_instance->algo.state = ALGORITHM_OUTPUT_READY;
            (void)pthread_mutex_unlock(&(p_instance->algo.lock));

            /* Call callback for broker to pass the new data along */
            R_BROKER_CallBack(p_instance, result);
        }
    }

    if (MODULE_STATE_STOPPING == p_instance->module.state)
    {
        p_instance->module.state = MODULE_STATE_THREAD_STOPPED;
    }

    R_PRINT_INFO("%s thread exiting", p_instance->name);
    pthread_exit(NULL);

    return NULL;
}

static r_result_t instance_start(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Verify proper state for stop */
    switch (p_instance->module.state) /*lint !e788 */
    {
        case MODULE_STATE_INITIALIZED:
        case MODULE_STATE_STOPPED:
        case MODULE_STATE_START_ERROR:
        {
            p_instance->module.state  = MODULE_STATE_STARTING;
            result = R_RESULT_SUCCESS;
            break;
        }

        default:
        {
            R_PRINT_ERROR("Start failed, %s is in wrong state", p_instance->name);
            result = R_RESULT_FAILED;
            break;
        }
    }

    if (MODULE_STATE_STARTING == p_instance->module.state)
    {
        R_PRINT_INFO("Starting %s...", p_instance->name);
        p_instance->module.state = MODULE_STATE_STARTING;

        if (R_MODULE_IsValidInstance(p_instance) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Instance %s not valid!", p_instance->name);
        }

        /* Create Thread */
        if (R_RESULT_SUCCESS == result)
        {
            if (0 == pthread_create(&(p_instance->module.thread), NULL, module_thread, p_instance))
            {
                /* Name thread -- doesn't really matter if it fails */
                (void)(pthread_setname_np(p_instance->module.thread, p_instance->name));
            }
            else
            {
                R_PRINT_ERROR("pthread create failure for %s", p_instance->name);
                result = R_RESULT_FAILED;
            }
        }

        /* Note: Thread is responsible for setting state to "Started" */
        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Error starting module %s", p_instance->name);
            p_instance->module.state = MODULE_STATE_START_ERROR;
        }
    }

    return result;
}

static r_result_t instance_stop(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    struct timespec timeout = {0};

    /* Verify proper state for stop */
    switch (p_instance->module.state) /*lint !e788 */
    {
        case MODULE_STATE_STARTED:
        case MODULE_STATE_STOPPING_ERROR:
        {
            p_instance->module.state  = MODULE_STATE_STOPPING;
            result = R_RESULT_SUCCESS;
            break;
        }

        default:
        {
            R_PRINT_ERROR("Stop failed, %s is in wrong state", p_instance->name);
            result = R_RESULT_FAILED;
            break;
        }
    }

    /* Check algorithm state */
    if (MODULE_STATE_STOPPING == p_instance->module.state)
    {
        R_PRINT_INFO("Stopping %s...\n", p_instance->name);

        /* Stop the thread gracefully */
        (void)pthread_mutex_lock(&(p_instance->algo.lock));

        p_instance->module.state = MODULE_STATE_STOPPING;

        if (pthread_cond_signal(&(p_instance->algo.signal)) != 0)
        {
            R_PRINT_ERROR("Error signaling algo");
            result = R_RESULT_FAILED;
        }

        (void)pthread_mutex_unlock(&(p_instance->algo.lock));

        /* Get the current time */
        (void)clock_gettime(CLOCK_REALTIME, &timeout);

        /* Add one second to the waiting period */
        timeout.tv_sec += 1;

        /* Wait for thread to return */
        if (pthread_timedjoin_np(p_instance->module.thread, NULL, &timeout) != 0)
        {
            R_PRINT_ERROR("Error joining %s thread. Module is stuck...", p_instance->name);
        }

        if (R_RESULT_SUCCESS == result)
        {
            /* Throw away all available data */
            result = R_BROKER_InstanceStop(p_instance);
        }

        /* Set state*/
        if (R_RESULT_SUCCESS == result)
        {
            R_PRINT_INFO("Instance %s STOPPED", p_instance->name);
            p_instance->module.state = MODULE_STATE_STOPPED;

        }
        else
        {
            R_PRINT_ERROR("Error stopping module %s", p_instance->name);
            p_instance->module.state = MODULE_STATE_STOPPING_ERROR;
        }
    }

    return result;
}

static r_result_t init_instance_module_struct(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* First just clear everything */
    p_instance->module.p_priv = NULL;

    /* Don't do anything to thread yet */
    if (0u < p_instance->p_module_interface->priv_size)
    {
        p_instance->module.p_priv = (st_priv_t *)malloc(p_instance->p_module_interface->priv_size); /*lint !e586  !e9076*/

        if (NULL == p_instance->module.p_priv)
        {
            R_PRINT_ERROR("Could not allocate memory for private data ");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

static r_result_t init_instance_algo_struct(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    p_instance->algo.state = ALGORITHM_IDLE;

    if (0 != pthread_mutex_init(&(p_instance->algo.lock), NULL))
    {
        R_PRINT_ERROR("Mutex creation failure");
        result = R_RESULT_FAILED;
    }

    if (0 != pthread_cond_init(&(p_instance->algo.signal), NULL))
    {
        R_PRINT_ERROR("Condition creation failure");
        result = R_RESULT_FAILED;
    }

    return result;
}

static r_result_t init_instance(st_module_instance_t * const p_instance, st_module_interface_t const * const p_module_interface, char const * const instance_name)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Verify proper state for init */
    switch (p_instance->module.state) /*lint !e788 */
    {
        case MODULE_STATE_NOT_INITIALIZED:
        case MODULE_STATE_DEINITIALIZED:
        case MODULE_STATE_INITIALIZATION_ERROR:
        {
            p_instance->module.state  = MODULE_STATE_INITIALIZING;
            result = R_RESULT_SUCCESS;
            break;
        }

        default:
        {
            R_PRINT_ERROR("Initialization failed, %s is in wrong state", instance_name);
            result = R_RESULT_FAILED;
            break;
        }
    }

    if (MODULE_STATE_INITIALIZING == p_instance->module.state)
    {
        /* Start initialization */
        R_PRINT_INFO("Initializing module instance %s...", instance_name);

        /* Copy name */
        result |= R_COMMON_STRING_Copy(p_instance->name, instance_name, R_MODULE_INSTANCE_NAME_SIZE);

        /* Copy module interface */
        p_instance->p_module_interface = p_module_interface;

        /* Init module structure */
        if (R_RESULT_SUCCESS == result)
        {
            result = init_instance_module_struct(p_instance);

            if (R_RESULT_SUCCESS != result)
            {
                R_PRINT_ERROR("Failed at init_instance_module for %s", instance_name);
            }
        }

        /* Init algorithm structure */
        if (R_RESULT_SUCCESS == result)
        {
            result = init_instance_algo_struct(p_instance);

            if (R_RESULT_SUCCESS != result)
            {
                R_PRINT_ERROR("Failed at init_instance_algo for %s", instance_name);
            }
        }

        /* Call the instance callback if it exists */
        if ((R_RESULT_SUCCESS == result) && (NULL != p_instance->p_module_interface->init))
        {
            result = p_instance->p_module_interface->init(p_instance);

            if (R_RESULT_SUCCESS != result)
            {
                R_PRINT_ERROR("Failed at interface init callback for %s", instance_name);
            }
        }

        /* Init broker for instance */
        if (R_RESULT_SUCCESS == result)
        {
            result = R_BROKER_InitInstance(p_instance);

            if (R_RESULT_SUCCESS != result)
            {
                R_PRINT_ERROR("Failed at broker init for %s", instance_name);
            }
        }

        /* Init timer structure */
        if (R_RESULT_SUCCESS == result)
        {
            result = R_TIMER_InitAnalysis(p_instance);
        }

        /* Update state based on init result */
        if (R_RESULT_SUCCESS == result)
        {
            R_PRINT_INFO("Instance %s INITIALIZED", instance_name);
            p_instance->module.state = MODULE_STATE_INITIALIZED;
        }
        else
        {
            R_PRINT_ERROR("Initialization failed for %s", instance_name);
            p_instance->module.state = MODULE_STATE_INITIALIZATION_ERROR;
        }
    }

    return result;
}

static r_result_t deinit_instance(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Verify proper state for deinit */
    switch (p_instance->module.state) /*lint !e788 */
    {
        case MODULE_STATE_INITIALIZED:
        case MODULE_STATE_STOPPED:
        case MODULE_STATE_DEINITIALIZATION_ERROR:
        {
            p_instance->module.state = MODULE_STATE_DEINITIALIZING;
            result = R_RESULT_SUCCESS;
            break;
        }

        default:
        {
            R_PRINT_ERROR("Deinitialization failed, %s is in wrong state", p_instance->name);
            result = R_RESULT_FAILED;
            break;
        }
    }

    if (MODULE_STATE_DEINITIALIZING == p_instance->module.state)
    {
        R_PRINT_INFO("Deinitializing instance %s...\n", p_instance->name);

        if (NULL != p_instance->p_module_interface->deinit)
        {
            result = p_instance->p_module_interface->deinit(p_instance);
        }

        if (0 != pthread_mutex_destroy(&(p_instance->algo.lock)))
        {
            R_PRINT_ERROR("Mutex deinit has failed");
            result = R_RESULT_FAILED;
        }

        if (0 != pthread_cond_destroy(&(p_instance->algo.signal)))
        {
            R_PRINT_ERROR("Condition deinit has failed");
            result = R_RESULT_FAILED;
        }

        if (R_RESULT_SUCCESS == result)
        {
            R_PRINT_INFO("Instance %s DEINITIALIZED", p_instance->name);
            p_instance->module.state = MODULE_STATE_DEINITIALIZED;
        }
        else
        {
            R_PRINT_ERROR("Deinitialization failed for %s", p_instance->name);
            p_instance->module.state = MODULE_STATE_DEINITIALIZATION_ERROR;
        }
    }

    return result;
}

/* TODO document static functions */
/*! @endcond */

/******************************************************************************/
/*                                                                            */
/*                           Semi-Public Interface                            */
/*                                                                            */
/******************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_MODULE_GetInstanceList
 * @brief           Framework functions call this to get a pointer to the list of module instances
 * @param [out]     p_num_instances - Pointer to the number of instance to overwrite
 * @retval          Pointer to the instance list.
 *********************************************************************************************************************/
st_module_instance_t * R_MODULE_GetInstanceList(uint32_t * p_num_instances)
{
    *p_num_instances = s_mod.instance_count;
    return s_mod.instances;
}

/******************************************************************************/
/*                                                                            */
/*                              Public Interface                              */
/*                                                                            */
/******************************************************************************/


/******************************************************************************************************************//**
 * Function Name:   R_MODULE_Init
 * @brief           Initializes the module framework
 *                  On entry: Module framework state must be MODULE_STATE_NOT_INITIALIZED or MODULE_STATE_DEINITIALIZED
 *                  On exit: Module framework state will be set to MODULE_STATE_INITIALIZED or MODULE_STATE_INITIALIZATION_ERROR
 * @param [in]      pp_module_interfaces - Pointer to the module interface list
 * @param [in]      module_count - Length of the module interface list
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_MODULE_Init(st_module_interface_t const * const * const pp_module_interfaces, uint32_t module_count)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Init s_mod */
    s_mod.state                = MODULE_STATE_INITIALIZING;
    s_mod.pp_module_interfaces = pp_module_interfaces;
    s_mod.module_count         = module_count;
    s_mod.instance_count       = 0;

    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        s_mod.state = MODULE_STATE_INITIALIZED;
    }
    else
    {
        s_mod.state = MODULE_STATE_INITIALIZATION_ERROR;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_MODULE_Deinit
 * @brief           Deinitializes the module framework
 *                  On entry: Module framework state must be MODULE_STATE_INITIALIZED or MODULE_STATE_STOPPED
 *                  On exit: Module framework state will be set to MODULE_STATE_DEINITIALIZED or MODULE_STATE_DEINITIALIZATION_ERROR
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_MODULE_Deinit(void)
{
    r_result_t result = R_RESULT_FAILED;

    if ((MODULE_STATE_INITIALIZED == s_mod.state) ||
        (MODULE_STATE_STOPPED     == s_mod.state))
    {
        s_mod.state = MODULE_STATE_DEINITIALIZING;
        r_result_t step_result = R_RESULT_SUCCESS;

        /* Do instance deinit calls */
        for (uint32_t i = 0; i < s_mod.instance_count; i++)
        {
            step_result = deinit_instance(&(s_mod.instances[i]));

            if (R_RESULT_SUCCESS != step_result)
            {
                result = step_result;
            }
        }

        /* Set state */
        if (R_RESULT_SUCCESS == result)
        {
            s_mod.state = MODULE_STATE_DEINITIALIZED;
        }
        else
        {
            s_mod.state = MODULE_STATE_DEINITIALIZATION_ERROR;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_MODULE_IsValidInstance
 * @brief           Checks to see if the give module instance is valid or not
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_MODULE_IsValidInstance(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NULL == p_instance)
    {
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Instance not valid: invalid null pointer");
    }
    else
    {
        result |= check_name(p_instance);
        result |= check_interface(p_instance);
        result |= check_module(p_instance);
        result |= check_broker(p_instance);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_MODULE_AddInstance
 * @brief           Adds a new instance to the instances list
 * @param [in]      instance_name - Name of the module instance
 * @param [in]      p_interface - pointer to the interface of the module
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_MODULE_AddInstance(char const * const instance_name, st_module_interface_t const * const p_interface)
{
    r_result_t result = R_RESULT_FAILED;
    R_PRINT_INFO("Trying to add module instance %s", instance_name);

    if (NULL == p_interface)
    {
        R_PRINT_ERROR("Couldn't add instance '%s' -- no corresponding module interface", instance_name);
    }
    else if (MODULE_STATE_INITIALIZED != s_mod.state)
    {
        R_PRINT_ERROR("Invalid module state");
    }
    else
    {
        st_module_instance_t * p_instance = add_instance_to_list(instance_name, p_interface);
        if (NULL != p_instance)
        {
            result = init_instance(p_instance, p_interface, instance_name);

            if (R_RESULT_SUCCESS != result)
            {
                R_PRINT_ERROR("Error initializing instance %s", instance_name);
            }
        }
        else
        {
            R_PRINT_ERROR("Could not add instance %s to list", instance_name);
        }
    }
    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_MODULE_AddInstanceByName
 * @brief           Adds a new instance to the instances list
 * @param [in]      instance_name - Name of the module instance
 * @param [in]      module_name - Name of the module
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_MODULE_AddInstanceByName(char const * const instance_name, char const * const module_name)
{
    return R_MODULE_AddInstance(instance_name, R_MODULE_GetModuleByName(module_name));
}

/******************************************************************************************************************//**
 * Function Name:   R_MODULE_GetModuleByName
 * @brief           Returns a pointer to the module interface for a given module name
 * @param [in]      module_name - Name of the module
 * @retval          NULL if module is not found, Otherwise the pointer to the module interface
 *********************************************************************************************************************/
st_module_interface_t const * R_MODULE_GetModuleByName(char const * const module_name)
{
    st_module_interface_t const * p_module_interface = NULL;

    for (uint32_t i = 0; i < s_mod.module_count; i++)
    {
        if (NULL != s_mod.pp_module_interfaces[i])
        {
            if (R_COMMON_STRING_Compare(module_name, s_mod.pp_module_interfaces[i]->module_name) == R_RESULT_SUCCESS)
            {
                p_module_interface = s_mod.pp_module_interfaces[i];
                break;
            }
        }
    }

    /* Not finding a module might not actually be an error, so purposly do not
     * print an error message if we don't find it */
    return p_module_interface;
}

/******************************************************************************************************************//**
 * Function Name:   R_MODULE_GetInstanceByName
 * @brief           Returns a pointer to the module instance for a given instance name
 * @param [in]      instance_name - Name of the module instance
 * @retval          NULL if module is not found, Otherwise the pointer to the module instance
 *********************************************************************************************************************/
st_module_instance_t * R_MODULE_GetInstanceByName(char const * const instance_name)
{
    st_module_instance_t * p_instance = NULL;

    for(uint32_t i = 0; i < s_mod.instance_count; i++)
    {
        if (R_COMMON_STRING_Compare(instance_name, s_mod.instances[i].name) == R_RESULT_SUCCESS)
        {
            p_instance = &(s_mod.instances[i]);
            break;
        }
    }

    /* Not finding an instance might not actually be an error, so purposly do not
     * print an error message if we don't find it */
    return p_instance;
}

/******************************************************************************************************************//**
 * Function Name:   R_MODULE_GetModuleName
 * @brief           Returns the name of the module for a given module instance
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          NULL on failure, Otherwise the string name of the module
 *********************************************************************************************************************/
char const * const R_MODULE_GetModuleName(st_module_instance_t * const p_instance)
{
    char const * name = NULL;

    if ((NULL != p_instance) && (NULL != p_instance->p_module_interface))
    {
        name = p_instance->p_module_interface->module_name;
    }

    return name;
}

/******************************************************************************************************************//**
 * Function Name:   R_MODULE_GetInstanceName
 * @brief           Returns the name of the module instance for a given module instance
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          NULL on failure, Otherwise the string name of the module instance
 *********************************************************************************************************************/
char const * const R_MODULE_GetInstanceName(st_module_instance_t * const p_instance)
{
    char const * name = NULL;

    if (NULL != p_instance)
    {
        name = p_instance->name;
    }

    return name;
}

/******************************************************************************************************************//**
 * Function Name:   R_MODULE_Start
 * @brief           Call this to start the module framework
 *                  On entry: Module framework state must be MODULE_STATE_INITIALIZED
 *                  On exit: Module framework state will be set to MODULE_STATE_STARTED or MODULE_STATE_START_ERROR
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_MODULE_Start(void)
{
    r_result_t result = R_RESULT_FAILED;

    if (MODULE_STATE_INITIALIZED == s_mod.state)
    {
        s_mod.state = MODULE_STATE_STARTING;

        result = R_BROKER_PreInstanceStart();

        if (R_RESULT_SUCCESS != result)
        {
            /* TODO: Error starting broker */
            R_PRINT_ERROR("Failed to pre-start broker");
        }

        for (uint32_t i = 0; i < s_mod.instance_count; i++)
        {
            r_result_t step_result = instance_start(&(s_mod.instances[i]));

            if (R_RESULT_SUCCESS != step_result)
            {
                R_PRINT_ERROR("Failed to start module instance %s", s_mod.instances[i].name);
                result = step_result;
            }
        }

        if (R_RESULT_SUCCESS == result)
        {
            result = R_BROKER_PostInstanceStart();

            if (R_RESULT_SUCCESS != result)
            {
                R_PRINT_ERROR("Failed post-instance start of broker");
            }
        }

        /* Set state */
        if (R_RESULT_SUCCESS == result)
        {
            s_mod.state = MODULE_STATE_STARTED;
        }
        else
        {
            s_mod.state = MODULE_STATE_START_ERROR;
        }
    }
    else
    {
        R_PRINT_ERROR("Wrong state");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_MODULE_Stop
 * @brief           Call this to stop the module framework
 *                  On entry: Module framework state must be MODULE_STATE_STARTED
 *                  On exit: Module framework state will be set to MODULE_STATE_STOPPED or MODULE_STATE_STOPPING_ERROR
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_MODULE_Stop(void)
{
    r_result_t result = R_RESULT_FAILED;

    if (MODULE_STATE_STARTED == s_mod.state)
    {
        s_mod.state = MODULE_STATE_STOPPING;

        result = R_BROKER_PreInstanceStop();

        for(uint32_t i = 0; ((R_MODULE_MAX_INSTANCES > i) && (NULL != s_mod.instances[i].p_module_interface)); i++)
        {
            st_module_instance_t * p_instance = &(s_mod.instances[i]);
            r_result_t step_result = instance_stop(p_instance);

            if (R_RESULT_SUCCESS != step_result)
            {
                R_PRINT_ERROR("Failed to stop module instance %s", s_mod.instances[i].name);
                result = step_result;
            }
        }

        /* Set state */
        if (R_RESULT_SUCCESS == result)
        {
            s_mod.state = MODULE_STATE_STOPPED;
        }
        else
        {
            s_mod.state = MODULE_STATE_STOPPING_ERROR;
        }
    }
    else
    {
        R_PRINT_ERROR("Wrong state");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_MODULE_InstanceStart
 * @brief           Call this to start a specific module instance
 * @param [in]      p_instance - Pointer to the module instance to start
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_MODULE_InstanceStart(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NULL != p_instance)
    {
        result = instance_start(p_instance);
    }
    else
    {
        R_PRINT_ERROR("NULL pointer");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_MODULE_InstanceStop
 * @brief           Call this to stop a specific module instance
 * @param [in]      p_instance - Pointer to the module instance to stop
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_MODULE_InstanceStop(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NULL != p_instance)
    {
        result = instance_stop(p_instance);
    }
    else
    {
        R_PRINT_ERROR("NULL pointer");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_MODULE_GetPrivDataPtr
 * @brief           Module instance should call this to get a pointer to their private data
 * @param [in]      p_instance - Pointer to the module instance to stop
 * @retval          NULL on failure, otherwise a pointer to the private data for that module instance
 *********************************************************************************************************************/
st_priv_t * R_MODULE_GetPrivDataPtr(st_module_instance_t * const p_instance)
{
    st_priv_t * p_priv = NULL;
    if (NULL != p_instance)
    {
        p_priv = p_instance->module.p_priv;
    }
    return p_priv;
}
