/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_module_list.c
 * @brief         Source file for list of all modules
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include "r_common.h"
#include "r_module_list.h"

/* Sensors */
#include "r_camera_v4l2.h"

/* Display */
#include "r_display_fb.h"

/* IMR modules */
#include "r_correction_imr.h"
#include "r_resize_imr.h"
#include "r_birdeye_imr.h"
#include "r_crop_imr.h"

/* PSC modules */
#include "r_resize_psc.h"

/* CVe modules */
#include "r_format_yuv.h"
#include "r_format_rgb.h"
#include "r_convert_yuv2rgb.h"

/* Drawing modules */
#include "r_draw_2d_box.h"
#include "r_draw_3d_box.h"
#include "r_draw_lane.h"
#include "r_draw_freespace.h"
#include "r_draw_fixed.h"

/* Recording modules */
#include "r_record_rof.h"
#include "r_record_raw.h"

/* Playback modules */
#include "r_playback_rof.h"
#include "r_playback_raw.h"
#include "r_playback_vraw.h"

/* Semantic segmentation modules */
#include "r_semseg_rvx.h"
#include "r_semseg_color_cve.h"

/* Phantom modules */
#include "r_phantom_rvx.h"
#include "r_phantom_color_cve.h"

/**********************************************************************************************************************
 Exported global variables
 *********************************************************************************************************************/

/*! List of all module interfaces */
st_module_interface_t const * const gp_module_list[] =
{
    /* Sensors */
    &g_module_interface_camera_v4l2,

    /* Display */
    &g_module_interface_display_fb,

    /* IMR modules */
    &g_module_interface_correction_imr,
    &g_module_interface_resize_imr,
    &g_module_interface_birdeye_imr,
    &g_module_interface_crop_imr,

    /* PSC modules */
    &g_module_interface_resize_psc,

    /* CVe modules */
    &g_module_interface_format_yuv,
	&g_module_interface_format_rgb,
    &g_module_interface_convert_yuv2rgb,

    /* Drawing modules */
    &g_module_interface_draw_2d_box,
    &g_module_interface_draw_3d_box,
    &g_module_interface_draw_lane,
    &g_module_interface_draw_fixed,
    &g_module_interface_draw_freespace,

    /* Recording modules */
    &g_module_interface_record_rof,
    &g_module_interface_record_raw,

    /* Playback modules */
    &g_module_interface_playback_rof,
    &g_module_interface_playback_raw,
    &g_module_interface_playback_vraw,

    /* Semantic segmentation modules */
    &g_module_interface_semseg_rvx,
    &g_module_interface_semseg_color_cve,

    /* Phantom modules */
    &g_module_interface_phantom_rvx,
    &g_module_interface_phantom_color_cve,
};


/*! Number of modules in the module list */
const uint32_t g_module_list_size = (uint32_t)(sizeof(gp_module_list)) / (sizeof(gp_module_list[0]));
