/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/

/******************************************************************************************************************//**
 * @file          r_module_list.h
 * @brief         Header file for list of all modules
 * @defgroup      PQS_Modules PQS Modules
 * @brief         Modules that perform data processing on objects
 * @defgroup      PQS_Modules_Publish PQS Modules Publish
 * @brief         Modules that publish data over Ethernet.
 * @ingroup       PQS_Modules
 * @defgroup      PQS_Modules_Playback PQS Modules Playback
 * @brief         Modules that playback data from disk.
 * @ingroup       PQS_Modules
 * @defgroup      PQS_Modules_Record PQS Modules Record
 * @brief         Modules that record data to disk.
 * @ingroup       PQS_Modules
 * @defgroup      PQS_Modules_Drawing PQS Modules Drawing
 * @brief         Modules that draw over images.
 * @ingroup       PQS_Modules
 * @defgroup      PQS_Modules_Semseg PQS Modules Semseg
 * @ingroup       PQS_Modules
 * @brief         Modules involved with semantic segmentation.
 * @defgroup      PQS_Modules_Camera_Obstacle_Detection PQS Modules Camera Obstacle Detection
 * @brief         Modules for Camera obstacles detector
 * @ingroup       PQS_Modules
 * @defgroup      PQS_Modules_Dof PQS Modules DOF
 * @brief         Modules involved with Dense Optical Flow
 * @ingroup       PQS_Modules
 * @defgroup      PQS_Modules_Resize PQS Modules Resize
 * @brief         Modules involved with image resizing
 * @ingroup       PQS_Modules
 * @defgroup      PQS_Framework_Modules_List PQS Framework Module List
 * @section       PQS_Module_List_Summary Module Summary
 *                List of all PQS Modules
 * @ingroup       PQS_Framework
 * @{
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/
#ifndef R_MODULE_LIST_H_
#define R_MODULE_LIST_H_

/**********************************************************************************************************************
 External global variables
 *********************************************************************************************************************/

extern st_module_interface_t const * const gp_module_list[];
extern const uint32_t g_module_list_size;

#endif /* R_MODULE_LIST_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Framework>
 *********************************************************************************************************************/
