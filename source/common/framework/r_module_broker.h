/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/

/******************************************************************************************************************//**
 * @file          r_module_broker.h
 * @brief         Header file for types shared between the Framework data broker and Framework module wrapper.
 *                Processing modules should not have to include this file!
 * @defgroup      PQS_Framework_Module_Broker PQS Framework Module Broker
 * @section       PQS_Framework_Module_Broker_Summary Module Summary
 *                Definition for data and functions used by the PQS framework. Processing modules should not
 *                have to include this file!
 * @ingroup       PQS_Framework
 * @{
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/
#ifndef R_MODULE_BROKER_H_
#define R_MODULE_BROKER_H_

/******************************************************************************/
/*                              !!! WARNING !!!                               */
/******************************************************************************/
/* This is to only be included by r_module.c and r_broker.c.                  */
/* If you need something from this file then re-evaluate why you need it.     */
/******************************************************************************/

#ifndef _R_FRAMEWORK_CORE_
#error This file must only be included in core framework functions
#endif

#define R_BROKER_MAX_ROUTES (16u) /*!< Maximum number of different routing paths a output channel can be routed */

#define R_TIME_ANALYSIS_MAX_MESSAGE_SIZE   (64u) /*!< Max length in characters for timing messages */
#define R_TIME_ANALYSIS_MAX_LOGS           (16u) /*!< Max number of timing logs */
#define R_TIME_ANALYSIS_MAX_TIMESTAMPS     (8u)  /*!< Max number of timing timestamps  */

/*! Algorithm processing state of modules @image html algo_state_machine.png */
typedef enum
{
    ALGORITHM_NOT_INITIALIZED, /*!< Algorithm has not been initialized */
    ALGORITHM_IDLE,            /*!< Algorithm is idle */
    ALGORITHM_HAS_DATA,        /*!< Algorithm has input data */
    ALGORITHM_BUSY,            /*!< Algorithm is processing input data */
    ALGORITHM_OUTPUT_READY     /*!< Algorithm is finished processing input data and output data is available */
} e_algorithm_state_t;

/*! State of modules and data broker */ /* TODO this needs a state picture */
typedef enum
{
    MODULE_STATE_NOT_INITIALIZED,
    MODULE_STATE_INITIALIZING,
    MODULE_STATE_INITIALIZATION_ERROR,
    MODULE_STATE_INITIALIZED,
    MODULE_STATE_STARTING,
    MODULE_STATE_START_ERROR,
    MODULE_STATE_STARTED,
    MODULE_STATE_STOPPING,
    MODULE_STATE_THREAD_STOPPED,
    MODULE_STATE_STOPPING_ERROR,
    MODULE_STATE_STOPPED,
    MODULE_STATE_DEINITIALIZING,
    MODULE_STATE_DEINITIALIZATION_ERROR,
    MODULE_STATE_DEINITIALIZED,
    MODULE_STATE_MAX,
} e_module_state_t;

/*! State machine of timer */ /* TODO this needs a state picture */
typedef enum
{
    R_TIMER_PRE_INIT, /*!< Pre initialized state */
    R_TIMER_INIT,     /*!< Timer is initialized */
    R_TIMER_STARTED,  /*!< Timer is started */
    R_TIMER_STOPPED,  /*!< Timer is stopped */
} e_timer_state_t;

/*! Specific route for an output object */
typedef struct {
    st_module_instance_t * p_instance; /*!< What's the module that gets the data? */
    uint32_t               channel;    /*!< What channel to copy to? */
} st_broker_link_list_t;

/*! List of all routes for an output object, one per channel */
typedef struct {
    uint32_t              count;                     /*!< How many routes */
    st_broker_link_list_t list[R_BROKER_MAX_ROUTES]; /*!< List of routes */
} st_broker_link_t;

/*! Channel definition, one per channel */
typedef struct {
    char                      name[R_MODULE_CHANNEL_NAME_SIZE]; /*!< Name of channel */
    e_object_type_t           type;                             /*!< Type of the datafield */
    uint32_t                  direction;                        /*!< Direction of channel */
} st_channel_info_t;

/*! Algorithm state data, one per instance */
typedef struct {
    e_algorithm_state_t state;    /*!< State of algorithm */
    pthread_mutex_t     lock;     /*!< Locks state */
    pthread_cond_t      signal;   /*!< Signals changes to state */
} st_algo_data_t;

/*! Broker routing data, one per instance */
typedef struct {
    st_object_t *       active_object_list[R_MODULE_MAX_CHANNELS];  /*!< Storage for active data planes */
    st_object_t *       pending_object_list[R_MODULE_MAX_CHANNELS]; /*!< Storage for pending data planes */
    st_broker_link_t    link[R_MODULE_MAX_CHANNELS];                /*!< List of where to route the data */
    st_channel_info_t   channel_info[R_MODULE_MAX_CHANNELS];        /*!< name/type/direction of channel */
} st_broker_data_t;

/*! Module data, one per instance */
typedef struct {
    e_module_state_t state;   /*!< State of module, init in AddInstance */
    st_priv_t *      p_priv;  /*!< Private data for module, init  in AddInstance */
    pthread_t        thread;  /*!< Main thread, init in start */
} st_module_data_t;

/*! Time stamp log*/
typedef struct
{
    char message[R_TIME_ANALYSIS_MAX_MESSAGE_SIZE];  /*!< Timer log message */
    uint64_t timestamp_usec;                         /*!< Timestamp of the message */
} st_time_log_t;

/*! Timer data, one per instance */
typedef struct
{
    e_timer_state_t timer_state;        /*!< State of the timer */
    bool timer_analysis;                /*!< True if timer analysis should be printed, false otherwise */
    bool timer_log;                     /*!< True if timer log should be printed, false otherwise */
    double num_measurements;            /*!< Number of timer measurements */
    double start_time_usec;             /*!< Current timer starting time */
    double finish_time_usec;            /*!< Current timer finish time */
    double processing_time_msec;        /*!< Current time from start to finish */
    double processing_time_msec_avg;    /*!< Running average of processing time */
    double start_period_hz;             /*!< Current frequency that timer gets started */
    double start_period_hz_avg;         /*!< Average Frequency that timer gets started */
    uint32_t message_log_count;                                   /*!< Current number of logs */
    st_time_log_t message_logs[R_TIME_ANALYSIS_MAX_LOGS];         /*!< Log data */
    uint32_t timestamp_log_count;                                 /*!< Current number of timestamps */
    st_time_log_t timestamp_logs[R_TIME_ANALYSIS_MAX_TIMESTAMPS]; /*!< Log data */
} st_timer_data_t;

/*! Definition of module instance */
typedef struct st_module_instance_tag {
    char                name[R_MODULE_INSTANCE_NAME_SIZE]; /*!< Unique name of instance */
    st_module_interface_t const * p_module_interface;      /*!< Pointer to callbacks implemented in module */
    st_module_data_t             module;                   /*!< Instance specific module information */
    st_algo_data_t               algo;                     /*!< Instance specific algorithm data */
    st_broker_data_t             broker;                   /*!< Data broker for module instance */
    st_timer_data_t              timer;                    /*!< Instance specific execution time information */
} st_module_instance_t; /*lint !e9109 !e761 */

extern st_module_instance_t * R_MODULE_GetInstanceList(uint32_t * p_num_instances);

extern r_result_t R_BROKER_InitInstance(st_module_instance_t * const p_instance);
extern r_result_t R_BROKER_PreInstanceStart(void);
extern r_result_t R_BROKER_PostInstanceStart(void);
extern r_result_t R_BROKER_PreInstanceStop(void);
extern r_result_t R_BROKER_InstanceStop(st_module_instance_t * const p_instance);

extern void R_BROKER_CallBack(st_module_instance_t * const p_instance, r_result_t result);

#endif /* R_MODULE_BROKER_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Framework>
 *********************************************************************************************************************/

