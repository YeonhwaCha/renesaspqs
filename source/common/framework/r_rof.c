/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_rof.c
 * @brief         Source file for Renesas Object Format read/write handling.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include "r_common.h"
#include "r_rof.h"

/* Future improvements:
 * 1) Allow data reading / writing to happen in multiple calls rather than
 *    all at once.  This will be helpful for things like planar data or if
 *    multiple structs are in a single data block
 */

/**********************************************************************************************************************
 Local variables
 *********************************************************************************************************************/

/*! Renesas Object Format 32 bit */
static const uint32_t ROF3 = ('R' << 0) | ('O' << 8) | ('F' << 16) | ('3' << 24);

/**********************************************************************************************************************
 Functions
 *********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   ready_for_block_io
 * @brief           Checks if the rof file is in the expected state
 * @param [in]      p_rof - Pointer to the rof file to check
 * @param [in]      expected_state - state to check
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t ready_for_block_io(st_rof_t * const p_rof, const e_rof_state_t expected_state)
{
    r_result_t result;

    if (NULL == p_rof)
    {
        result = R_RESULT_ILLEGAL_NULL_POINTER;
    }
    else if (expected_state != p_rof->state)
    {
        result = R_RESULT_INVALID_STATE;
    }
    else
    {
        result = R_RESULT_SUCCESS;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   get_pad
 * @brief           Returns the number of bytes to pad at the end of a block
 * @param [in]      ftype - Type of file
 * @param [in]      size - size of the block to write in bytes
 * @retval          Number of bytes to pad at the end of the block for the given file type
 *********************************************************************************************************************/
static uint32_t get_pad(const uint32_t ftype, const uint32_t size)
{
    uint32_t block_size_bytes;
    if (ftype == ROF3)
    {
        block_size_bytes = 4;
    }
    else
    {
        block_size_bytes = 1;
    }
    return (block_size_bytes - 1) & (block_size_bytes - size);
}

/******************************************************************************************************************//**
 * Function Name:   read_block_data
 * @brief           Reads a data block to buffer
 * @param [in]      p_rof - Pointer to the ROF descriptor
 * @param [in]      buff_size - Size of data to read in
 * @param [out]     p_buff - Buffer to read data into
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t read_block_data(st_rof_t * const p_rof, const uint32_t buff_size, uint8_t * const p_buff)
{
    r_result_t result = ready_for_block_io(p_rof, R_ROF_STATE_READ_AT_DATA);

    long int seek_size = 0;

    /* Read in data */
    if (R_RESULT_SUCCESS == result)
    {
        uint32_t read_size = (NULL == p_buff) ? 0 : buff_size;

        seek_size = (long int)(get_pad(p_rof->ftype, p_rof->section.size));

        if (read_size >= p_rof->section.size)
        {
            /* Buffer larger than (or equal to) file data field */
            /* Trim read size down to actual file size */
            read_size = p_rof->section.size;
        }
        else
        {
            /* Buffer smaller than file data field -- add on to seek size */
            seek_size += (long int)(p_rof->section.size - read_size);
        }

        /* Shouldn't really have to check p_buff for null pointer, but it just feels safer this way */
        if ((NULL != p_buff) && (0 < read_size))
        {
            if (1 != fread(p_buff, read_size, 1, p_rof->p_file))
            {
                result       = R_RESULT_FAILED;
                p_rof->state = R_ROF_STATE_ERROR_OPEN;
            }
        }
    }

    /* Skip unneeded data */

    if ((R_RESULT_SUCCESS == result) && (0 != seek_size))
    {
        if (0 != fseek(p_rof->p_file, seek_size, SEEK_CUR))
        {
            result       = R_RESULT_FAILED;
            p_rof->state = R_ROF_STATE_ERROR_OPEN;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        p_rof->state = R_ROF_STATE_READ_AT_HEADER;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   read_block_header
 * @brief           Reads a data block header
 * @param [in]      p_rof - Pointer to the ROF descriptor
 * @param [out]     p_header - Pointer to header info of block
 * @param [out]     p_size - Pointer to size of data of the block
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t read_block_header(st_rof_t * const p_rof, uint32_t * const p_header, uint32_t * const p_size)
{
    r_result_t result = ready_for_block_io(p_rof, R_ROF_STATE_READ_AT_HEADER);

    /* TODO why would this help? What is causing this error? */
    /* If we get an invalid state error, then see if skipping the data section helps */
    if (R_RESULT_INVALID_STATE == result)
    {
        result = read_block_data(p_rof, p_rof->section.size, NULL);
    }

    if (R_RESULT_SUCCESS == result)
    {
        int blocks_read = fread(&(p_rof->section), 4, 2, p_rof->p_file);
        if (2 == blocks_read)
        {
            /* Successful header read */
            p_rof->state      = R_ROF_STATE_READ_AT_DATA;
            p_rof->section.at = 0;

            if (NULL != p_header)
            {
                *p_header = p_rof->section.name;
            }

            if (NULL != p_size)
            {
                *p_size = p_rof->section.size;
            }
        }
        else if ((0 == blocks_read) && (feof(p_rof->p_file)))
        {
            result       = R_RESULT_FAILED;
            p_rof->state = R_ROF_STATE_READ_AT_FILE_END;
        }
        else
        {
            result       = R_RESULT_FAILED;
            p_rof->state = R_ROF_STATE_ERROR_OPEN;
        }
    }

    return result;
}


/******************************************************************************************************************//**
 * Function Name:   R_ROF_WriteOpen
 * @brief           Open file for writing
 * @param [in]      p_rof - Pointer to the ROF descriptor
 * @param [in]      file_name - name of the file to open
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_ROF_WriteOpen(st_rof_t * const p_rof, char const * const file_name)
{
    r_result_t result = R_RESULT_SUCCESS;
    FILE * p_file = NULL;

    if (NULL == p_rof)
    {
        result = R_RESULT_FAILED;
    }
    else
    {
        p_rof->state = R_ROF_STATE_INITIALIZATION;
    }

    if (R_RESULT_SUCCESS == result)
    {
        p_file = fopen(file_name, "wb");

        if (NULL == p_file)
        {
            result = R_RESULT_FAILED;
            p_rof->state = R_ROF_STATE_ERROR_CLOSED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (1 != fwrite(&ROF3, 4, 1, p_file))
        {
            result = R_RESULT_FAILED;
            fclose(p_file);
            p_rof->state = R_ROF_STATE_ERROR_CLOSED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        p_rof->p_file           = p_file;
        p_rof->ftype        = ROF3;
        p_rof->state        = R_ROF_STATE_WRITE_BLOCK_READY;
        p_rof->section.name = 0;
        p_rof->section.size = 0;
        p_rof->section.at   = 0;
    }
    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ROF_WriteBlock
 * @brief           Writes a data block to file
 * @param [in]      p_rof - Pointer to the ROF descriptor
 * @param [in]      header - Header info
 * @param [in]      size - Size of data
 * @param [in]      p_data - Pointer to data
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_ROF_WriteBlock(st_rof_t * const p_rof, const uint32_t header, const uint32_t size, uint8_t const * const p_data)
{
    r_result_t result = ready_for_block_io(p_rof, R_ROF_STATE_WRITE_BLOCK_READY);

    /* Write header block, title field */
    if (R_RESULT_SUCCESS == result)
    {
        if (1 != fwrite(&header, 4, 1, p_rof->p_file))
        {
            result       = R_RESULT_FAILED;
            p_rof->state = R_ROF_STATE_ERROR_OPEN;
        }
    }

    /* Write header block, size field */
    if (R_RESULT_SUCCESS == result)
    {
        if (1 != fwrite(&size, 4, 1, p_rof->p_file))
        {
            result       = R_RESULT_FAILED;
            p_rof->state = R_ROF_STATE_ERROR_OPEN;
        }
    }

    /* Write data */
    if ((size > 0) && (R_RESULT_SUCCESS == result))
    {
        if (1 != fwrite(p_data, size, 1, p_rof->p_file))
        {
            result      = R_RESULT_FAILED;
            p_rof->state = R_ROF_STATE_ERROR_OPEN;
        }
    }

    /* Pad if necessary */
    if (R_RESULT_SUCCESS == result)
    {
        uint32_t pad = get_pad(p_rof->ftype, size);
        if (pad)
        {
            uint32_t padding = 0x00000000;
            if (1 != fwrite(&padding, pad, 1, p_rof->p_file))
            {
                result       = R_RESULT_FAILED;
                p_rof->state = R_ROF_STATE_ERROR_OPEN;
            }
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ROF_ReadOpen
 * @brief           Open file for reading
 * @param [in]      p_rof - Pointer to the ROF descriptor
 * @param [in]      file_name - name of the file to open
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_ROF_ReadOpen(st_rof_t * const p_rof, char const * const file_name)
{
    r_result_t result = R_RESULT_SUCCESS;
    FILE * p_file = NULL;
    uint32_t rof_format = 0;

    if (NULL == p_rof)
    {
        result = R_RESULT_FAILED;
    }
    else
    {
        p_rof->state = R_ROF_STATE_INITIALIZATION;
    }

    if (R_RESULT_SUCCESS == result)
    {
        p_file = fopen(file_name, "rb");
        if (NULL == p_file)
        {
            result = R_RESULT_FAILED;
            p_rof->state = R_ROF_STATE_ERROR_CLOSED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (1 != fread(&rof_format, 4, 1, p_file))
        {
            /* Failed to read file */
            result = R_RESULT_FAILED;
        }
        else if (rof_format != ROF3)
        {
            /* ROF format not supported */
            result = R_RESULT_FAILED;
        }
        else
        {
            /* ROF format supported */
            result = R_RESULT_SUCCESS;
        }

        if (R_RESULT_SUCCESS != result)
        {
            fclose(p_file);
            p_rof->state = R_ROF_STATE_ERROR_CLOSED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        p_rof->p_file       = p_file;
        p_rof->ftype        = rof_format;
        p_rof->state        = R_ROF_STATE_READ_AT_HEADER;
        p_rof->section.name = 0;
        p_rof->section.size = 0;
        p_rof->section.at   = 0;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ROF_ReadBlock
 * @brief           Reads a header and data block
 * @param [in]      p_rof - Pointer to the ROF descriptor
 * @param [in]      header - Header info of data to read
 * @param [in]      size - Size of data
 * @param [in]      p_data - Pointer to data
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_ROF_ReadBlock(st_rof_t * const p_rof, const uint32_t header, const uint32_t size, uint8_t * const p_data)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Find the correct header */
    while (R_RESULT_SUCCESS == result)
    {
        uint32_t act_header = 0;
        result = read_block_header(p_rof, &act_header, NULL);
        if (act_header == header)
        {
            break;
        }

        /* TODO: Do we care if size is ok?  How could we even check it? */
        /* TODO: Loop back to beginning of file? */
    }

    /* Read in the data */
    if (R_RESULT_SUCCESS == result)
    {
        result = read_block_data(p_rof, size, p_data);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ROF_Close
 * @brief           Closes a file
 * @param [in]      p_rof - Pointer to the ROF descriptor
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_ROF_Close(st_rof_t * const p_rof)
{
    r_result_t result = R_RESULT_FAILED;

    if (NULL != p_rof)
    {
        switch(p_rof->state)
        {
            case R_ROF_STATE_WRITE_BLOCK_READY:
            case R_ROF_STATE_READ_AT_HEADER:
            case R_ROF_STATE_READ_AT_DATA:
            case R_ROF_STATE_READ_AT_FILE_END:
            case R_ROF_STATE_ERROR_OPEN:
            {
                result        = fclose(p_rof->p_file) ? R_RESULT_FAILED : R_RESULT_SUCCESS;
                p_rof->p_file = NULL;
                p_rof->state  = R_ROF_STATE_FILE_CLOSED;
                break;
            }

            default:
            {
                result = R_RESULT_FAILED;
                break;
            }
        }
    }

    return result;
}
