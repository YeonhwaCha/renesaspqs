
/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_module.h
 * @brief         Header file for PQS Framework module handling
 * @defgroup      PQS_Framework_Module PQS Framework Module
 * @section       PQS_Framework_Module_Summary Module Summary
 *                Framework Module is responsible for all modules handling in the PQS Framework.
 * @ingroup       PQS_Framework
 * @{
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/
#ifndef R_MODULE_H_
#define R_MODULE_H_

/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

/* Note: Thread names can only be 16 characters long, including null */
#define R_MODULE_NAME_SIZE          (32u)   /*!< Max size of a module name */
#define R_MODULE_INSTANCE_NAME_SIZE (32u)   /*!< Max size of an instance name */
#define R_MODULE_CHANNEL_NAME_SIZE  (32u)   /*!< Max size of a channel name */
#define R_MODULE_MAX_MODULES        (64u)   /*!< Max number of modules supported*/
#define R_MODULE_MAX_INSTANCES      (128u)  /*!< Max number of module instances supported */
#define R_MODULE_MAX_CHANNELS       (9u)    /*!< Max number of channel each module can implement */

/**********************************************************************************************************************
 Typedef definitions
 *********************************************************************************************************************/

/*! Module instance opaque data type. Modules never need to dereference or interact with
 * st_module_instance_t. It is just passed to the framework. */
typedef struct st_module_instance_tag st_module_instance_t;

/*! Each module must define its own strut st_priv_s if it needs private data.
 * The definition should only be available inside the C file */
typedef struct st_priv_tag st_priv_t;

/*! This is the interface that each module must implement */
typedef struct {
    /*! Required: Name of module */
    char       module_name[R_MODULE_NAME_SIZE];
    
    /*! Required: How large private data is (set it to sizeof(st_priv_t)) */
    uint32_t   priv_size;

    /*! Optional: Init callback for instance */
    r_result_t (*init)(st_module_instance_t * const p_module_instance);

    /*! Optional: Deinit callback for instance */
    r_result_t (*deinit)(st_module_instance_t * const p_module_instance);

    /*! Required: Sets up data broker map */
    r_result_t (*init_channels)(st_module_instance_t * const p_module_instance);

    /*! Required: Process data callback */
    r_result_t (*process_data)(st_module_instance_t * const p_module_instance);
} st_module_interface_t;

/**********************************************************************************************************************
 Functions
 *********************************************************************************************************************/

/* Init/deinit functions, auto-calls init for all modules/instances. */
/* Note: Each specific module is initiated as it is added to the system */
extern r_result_t R_MODULE_Init(st_module_interface_t const * const * const pp_module_interfaces, uint32_t module_count);
extern r_result_t R_MODULE_Deinit(void);

/* Checks if instance is valid for given mode */
extern r_result_t R_MODULE_IsValidInstance(st_module_instance_t * const p_instance);

/* Adds a named instance of a given module */
extern r_result_t R_MODULE_AddInstance(char const * const instance_name, st_module_interface_t const * const p_interface);
extern r_result_t R_MODULE_AddInstanceByName(char const * const instance_name, char const * const module_name);

/* Gets the pointer to a module interface */
extern st_module_interface_t const * R_MODULE_GetModuleByName(char const * const module_name);

/* Gets the pointer to an instance of a module */
extern st_module_instance_t * R_MODULE_GetInstanceByName(char const * const instance_name);

/* Start and stop instances */
extern r_result_t R_MODULE_InstanceStart(st_module_instance_t * const p_instance);
extern r_result_t R_MODULE_InstanceStop(st_module_instance_t * const p_instance);

/* Returns pointer to module/instance name string */
extern char const * const R_MODULE_GetModuleName(st_module_instance_t * const p_instance);
extern char const * const R_MODULE_GetInstanceName(st_module_instance_t * const p_instance);

/* Start/stop functions, auto-calls all module instance start/stop */
extern r_result_t R_MODULE_Start(void);
extern r_result_t R_MODULE_Stop(void);

/* Call this whenever you need priv data.  Returns null if unavailable */
extern st_priv_t * R_MODULE_GetPrivDataPtr(st_module_instance_t * const p_instance);

#endif /* R_MODULE_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Framework>
 *********************************************************************************************************************/
