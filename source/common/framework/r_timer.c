/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_timer.c
 * @brief         Source file for module timing API.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include "r_common.h"

/*! Define framework core, otherwise r_module_broker.h will throw an error */
#define _R_FRAMEWORK_CORE_
#include "r_module_broker.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

static st_word_list_t s_timer_analysis; /*!< Contains a list of all the module instances whose timer analysis should be printed to output */
static st_word_list_t s_timer_log;      /*!< Contains a list of all the module instances whose timer log messages should be printed to output */

/******************************************************************************************************************//**
 * Function Name:   R_TIMER_Init
 * @brief           Initializes the timing module. Gets configuration to see which timing modules should be printed.
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
r_result_t R_TIMER_Init(void)
{
    r_result_t result = R_RESULT_SUCCESS;
    char enable_analysis_string[R_STRING_MAX_NUM_WORDS * R_STRING_MAX_WORD_LENGTH] = {'\0'};
    char enable_log_string[R_STRING_MAX_NUM_WORDS * R_STRING_MAX_WORD_LENGTH] = {'\0'};

    /* Get all instance names to enable */
    result |= R_COMMON_STRING_Copy(enable_analysis_string, R_COMMON_CONFIG_GetString("common", "timer_analysis", ""), R_STRING_MAX_NUM_WORDS * R_STRING_MAX_WORD_LENGTH);
    result |= R_COMMON_STRING_Copy(enable_log_string,      R_COMMON_CONFIG_GetString("common", "timer_log",      ""), R_STRING_MAX_NUM_WORDS * R_STRING_MAX_WORD_LENGTH);
    result |= R_COMMON_STRING_Split(enable_analysis_string, &s_timer_analysis, ' ');
    result |= R_COMMON_STRING_Split(enable_log_string,      &s_timer_log, ' ');

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_TIMER_Deinit
 * @brief           Deinitializes the timing module.
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
r_result_t R_TIMER_Deinit(void)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Nothing to do now */

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_TIMER_InitAnalysis
 * @brief           Initializes the timing analysis for a particular module instance. This function should only be
 *                  called by the PQS Framework
 * @param [in]      p_instance - pointer to module instance
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
r_result_t R_TIMER_InitAnalysis(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Check input */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        if (NULL == p_instance)
        {
            R_PRINT_ERROR("Input parameter is NULL");
            result = R_RESULT_FAILED;
        }
    }

    /* Reset the time measurement structure */
    if (R_RESULT_SUCCESS == result)
    {
        p_instance->timer.timer_state              = R_TIMER_INIT;
        p_instance->timer.num_measurements         = 0.0f;
        p_instance->timer.start_time_usec          = 0.0f;
        p_instance->timer.finish_time_usec         = 0.0f;
        p_instance->timer.processing_time_msec     = 0.0f;
        p_instance->timer.processing_time_msec_avg = 0.0f;
        p_instance->timer.start_period_hz          = 0.0f;
        p_instance->timer.start_period_hz_avg      = 0.0f;

        /* Check if the module name is in the timer analysis word list */
        if (R_COMMON_STRING_WordInList(p_instance->name, &s_timer_analysis) == R_RESULT_SUCCESS)
        {
            p_instance->timer.timer_analysis = true;
        }
        else
        {
            p_instance->timer.timer_analysis = false;
        }

        /* Check if the module name is in the timer log word list */
        if (R_COMMON_STRING_WordInList(p_instance->name, &s_timer_log) == R_RESULT_SUCCESS)
        {
            p_instance->timer.timer_log = true;
        }
        else
        {
            p_instance->timer.timer_log = false;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_TIMER_StartAnalysis
 * @brief           Starts the timing analysis for a particular module instance before that modules processing function
 *                  is called. This function should only be called by the PQS Framework.
 * @param [in]      p_instance - pointer to module instance
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
r_result_t R_TIMER_StartAnalysis(st_module_instance_t * const p_instance)
{
    r_result_t result  = R_RESULT_SUCCESS;
    st_timer_data_t * p_timer = NULL;

    /* Check input */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        if (NULL != p_instance)
        {
            p_timer = &p_instance->timer;
        }
        else
        {
            R_PRINT_ERROR("Input parameter is NULL");
            result = R_RESULT_FAILED;
        }
    }

    /* Check state */
    if (R_RESULT_SUCCESS == result)
    {
        if ((R_TIMER_INIT == p_instance->timer.timer_state) || (R_TIMER_STOPPED == p_timer->timer_state))
        {
            p_timer->timer_state = R_TIMER_STARTED;
        }
        else
        {
            R_PRINT_ERROR("Timer is in wrong state");
            result = R_RESULT_FAILED;
        }
    }

    /* Start the time measurement structure */
    if (R_RESULT_SUCCESS == result)
    {
        double current_time = (double)R_COMMON_TIME_GetRealUsec();

        /* Have to ignore the first time for average calculations */
        if (0.0 != p_timer->num_measurements) /*lint !e9137 */
        {
            /* Calculate the new calling period */
            double new_start_period = 1000000.0f / (current_time - p_timer->start_time_usec);
            p_timer->start_period_hz = new_start_period;

            /* Update the running average with the current period */
            double new_start_period_avg = ((p_timer->start_period_hz_avg * (p_timer->num_measurements - 1.0)) + new_start_period) / (p_timer->num_measurements);
            p_timer->start_period_hz_avg = new_start_period_avg;

            /* Note: do not update the number of measurements here. That is done in the stop function */
        }

        /* Update starting time stamps */
        p_timer->start_time_usec = current_time;

        /* Set log counter to zero */
        p_timer->message_log_count = 0;
        p_timer->timestamp_log_count = 0;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_TIMER_StopAnalysis
 * @brief           Stops the timing analysis for a particular module instance after that modules processing function
 *                  is complete. This function should only be called by the PQS Framework.
 * @param [in]      p_instance - pointer to module instance
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
r_result_t R_TIMER_StopAnalysis(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_timer_data_t * p_timer = NULL;

    /* Check input */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        if (NULL != p_instance)
        {
            p_timer = &p_instance->timer;
        }
        else
        {
            R_PRINT_ERROR("Input parameter is NULL");
            result = R_RESULT_FAILED;
        }
    }

    /* Check state */
    if (R_RESULT_SUCCESS == result)
    {
        if (R_TIMER_STARTED == p_timer->timer_state)
        {
            p_timer->timer_state = R_TIMER_STOPPED;
        }
        else
        {
            R_PRINT_ERROR("Timer is in wrong state");
            result = R_RESULT_FAILED;
        }
    }

    /* Stop the time measurement structure */
    if (R_RESULT_SUCCESS == result)
    {
        double current_time = (double)R_COMMON_TIME_GetRealUsec();

        /* Get the finish time */
        p_timer->finish_time_usec = current_time;

        /* Calculate total processing time */
        double new_processing_time = (p_timer->finish_time_usec - p_timer->start_time_usec) / 1000.0;
        p_timer->processing_time_msec = new_processing_time;

        /* Update average information */
        double new_processing_avg = ((p_timer->processing_time_msec_avg * p_timer->num_measurements) + new_processing_time) / (p_timer->num_measurements + 1.0);
        p_timer->processing_time_msec_avg = new_processing_avg;

        /* Update number of measurements */
        p_timer->num_measurements++;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_TIMER_LogMessage
 * @brief           Modules can call this function to save messages. The time stamp in real time of when this function
 *                  was called will also be stored. When time logging is enabled for that module the time stamp
 *                  difference between messages will be printed after the module is finished with processing.
 * @param [in]      p_instance - pointer to module instance
 * @param [in]      p_message - string of message that should be logged
 *********************************************************************************************************************/
void R_TIMER_LogMessage(st_module_instance_t * const p_instance, const char * const p_message)
{
    /* Initialized  locals */
    r_result_t result = R_RESULT_SUCCESS;
    st_timer_data_t * p_timer = NULL;

    /* Check input */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        if ((NULL == p_instance) || (NULL == p_message))
        {
            R_PRINT_ERROR("Input parameter is NULL");
            result = R_RESULT_FAILED;
        }

        if (R_RESULT_SUCCESS == result)
        {
            p_timer = &p_instance->timer;
        }
    }

    /* Check state */
    if (R_RESULT_SUCCESS == result)
    {
        if (R_TIMER_STARTED != p_timer->timer_state)
        {
            R_PRINT_ERROR("Timer is in wrong state");
            result = R_RESULT_FAILED;
        }
    }

    /* Save log message */
    if (R_RESULT_SUCCESS == result)
    {
        /* Save time stamp */
        p_timer->message_logs[p_timer->message_log_count].timestamp_usec = R_COMMON_TIME_GetRealUsec();

        /* Save message */
        result = R_COMMON_STRING_Copy(p_timer->message_logs[p_timer->message_log_count].message, p_message, R_TIME_ANALYSIS_MAX_MESSAGE_SIZE);
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* Update log counter */
        if ((p_timer->message_log_count + 1u) < R_TIME_ANALYSIS_MAX_LOGS)
        {
            p_timer->message_log_count++;
        }
        else
        {
            R_PRINT_ERROR("Exceed maximum number of log messages");
            result = R_RESULT_FAILED;
        }
    }

    (void)result; //Not used
}

/******************************************************************************************************************//**
 * Function Name:   R_TIMER_LogTimestamp
 * @brief           Modules can call this function to save messages and timestamps. When time logging is enabled for
 *                  that module the messages and timestamps will be printed after the module is finished with processing.
 * @param [in]      p_instance - pointer to module instance
 * @param [in]      timestamp - timestamp that should be logged
 * @param [in]      p_message - string of message that should be logged
 *********************************************************************************************************************/
void R_TIMER_LogTimestamp(st_module_instance_t * const p_instance, uint64_t timestamp, const char * const p_message)
{
    /* Initialized  locals */
    r_result_t result = R_RESULT_SUCCESS;
    st_timer_data_t * p_timer = NULL;

    /* Check input */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        if ((NULL == p_instance) || (NULL == p_message))
        {
            R_PRINT_ERROR("Input parameter is NULL");
            result = R_RESULT_FAILED;
        }

        if (0u == timestamp)
        {
            R_PRINT_ERROR("Invalid timestamp");
            result = R_RESULT_FAILED;
        }

        if (R_RESULT_SUCCESS == result)
        {
            p_timer = &p_instance->timer;
        }
    }

    /* Check state */
    if (R_RESULT_SUCCESS == result)
    {
        if (R_TIMER_STARTED != p_timer->timer_state)
        {
            R_PRINT_ERROR("Timer is in wrong state");
            result = R_RESULT_FAILED;
        }
    }

    /* Save log message */
    if (R_RESULT_SUCCESS == result)
    {
        /* Save time stamp */
        p_timer->timestamp_logs[p_timer->timestamp_log_count].timestamp_usec = timestamp;

        /* Save message */
        result = R_COMMON_STRING_Copy(p_timer->timestamp_logs[p_timer->timestamp_log_count].message, p_message, R_TIME_ANALYSIS_MAX_MESSAGE_SIZE);
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* Update log counter */
        if ((p_timer->timestamp_log_count + 1u) < R_TIME_ANALYSIS_MAX_TIMESTAMPS)
        {
            p_timer->timestamp_log_count++;
        }
        else
        {
            R_PRINT_ERROR("Exceed maximum number of log messages");
            result = R_RESULT_FAILED;
        }
    }

    (void)result;
}

/******************************************************************************************************************//**
 * Function Name:   R_TIMER_PrintAnalysis
 * @brief           Prints the timing analysis for a particular module instance after that modules processing function
 *                  is complete. This function should only be called by the PQS Framework.
 * @param [in]      p_instance - pointer to module instance
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
r_result_t R_TIMER_PrintAnalysis(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_timer_data_t * p_timer = NULL;

    /* Check input */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        if (NULL != p_instance)
        {
            p_timer = &p_instance->timer;
        }
        else
        {
            R_PRINT_ERROR("Input parameter is NULL");
            result = R_RESULT_FAILED;
        }
    }

    /* Check state */
    if (R_RESULT_SUCCESS == result)
    {
        if (R_TIMER_STOPPED != p_timer->timer_state)
        {
            R_PRINT_ERROR("Timer is in wrong state");
            result = R_RESULT_FAILED;
        }
    }

    /* Print timing analysis  */
    if (R_RESULT_SUCCESS == result)
    {
        if (true == p_timer->timer_analysis)
        {
            R_PRINT("TIMER ANALYSIS: for module %s\n"
                        "\t Current processing time: %.3f ms\n"
                        "\t Average processing time over %g iterations: %.3f ms\n"
                        "\t Current calling period: %.3f Hz\n"
                        "\t Average calling period over %g iterations: %.3f Hz\n",
                        p_instance->name,
                        p_timer->processing_time_msec,
                        p_timer->num_measurements,
                        p_timer->processing_time_msec_avg,
                        p_timer->start_period_hz,
                        p_timer->num_measurements,
                        p_timer->start_period_hz_avg);
        }
    }

    /* Print message logs */
    if (R_RESULT_SUCCESS == result)
    {
        if (true == p_timer->timer_log)
        {
            /* Loop though all the message logs */
            for (uint32_t log_index = 0u; log_index < p_timer->message_log_count; log_index++)
            {
                /* Check if it is the first log */
                if (0u == log_index)
                {
                    R_PRINT("TIMER MESSAGE LOGS: for module %s\n", p_instance->name);
                    R_PRINT("\t Starting Measurement to %s: %.3f ms\n",
                            p_timer->message_logs[log_index].message,
                            ((double)p_timer->message_logs[log_index].timestamp_usec - p_timer->start_time_usec)/1000.0 );
                }
                else
                {
                    R_PRINT("\t %s to %s: %.3f ms\n",
                            p_timer->message_logs[log_index - 1u].message,
                            p_timer->message_logs[log_index].message,
                            ((double)p_timer->message_logs[log_index].timestamp_usec - (double)p_timer->message_logs[log_index - 1u].timestamp_usec)/1000.0 );
                }

                /* Check for last log */
                if ((log_index + 1u) == p_timer->message_log_count)
                {
                    R_PRINT("\t %s to Stopping Measurement: %.3f ms\n\n",
                            p_timer->message_logs[log_index].message,
                            (p_timer->finish_time_usec - (double)p_timer->message_logs[log_index].timestamp_usec)/1000.0 );
                }
            }
        }
    }

    /* Print timestamp logs */
    if (R_RESULT_SUCCESS == result)
    {
        if (true == p_timer->timer_log)
        {
            /* Loop though all the timestamp logs */
            for (uint32_t log_index = 0u; log_index < p_timer->timestamp_log_count; log_index++)
            {
                /* Check if it is the first log */
                if (0u == log_index)
                {
                    R_PRINT("TIMER TIMESTAMP LOGS: for module %s\n", p_instance->name);
                }

                R_PRINT("\t %s %.3f ms \n",
                        p_timer->timestamp_logs[log_index].message,
                        (double)(p_timer->timestamp_logs[log_index].timestamp_usec)/1000.0);
            }

            /* Print final return */
            R_PRINT("\n");
        }
    }

    return result;
}
