/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_rof.h
 * @brief         Header file for Renesas Object Format read/write handling.
 * @defgroup      PQS_Framework_ROF PQS Framework Renesas Object Format
 * @section       PQS_Framework_ROF_Summary Module Summary
 *                Renesas Object Format data read/write handling.
 * @ingroup       PQS_Framework
 * @{
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/
#ifndef R_ROF_H_
#define R_ROF_H_

/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

/* These are some section headers used for ROF */
#define R_SECTION_META ((uint32_t)(((uint32_t)'M' << 0) | ((uint32_t)'E' << 8) | ((uint32_t)'T' << 16) | ((uint32_t)'A' << 24))) /*!< Meta data section tag */
#define R_SECTION_PRIV ((uint32_t)(((uint32_t)'P' << 0) | ((uint32_t)'R' << 8) | ((uint32_t)'I' << 16) | ((uint32_t)'V' << 24))) /*!< Private section tag */
#define R_SECTION_DATA ((uint32_t)(((uint32_t)'D' << 0) | ((uint32_t)'A' << 8) | ((uint32_t)'T' << 16) | ((uint32_t)'A' << 24))) /*!< Data section tag */

/**********************************************************************************************************************
 Local Typedef definitions
 *********************************************************************************************************************/

/*! State of the ROF handler */ /* TODO need some state diagram */
typedef enum {
    R_ROF_STATE_UNDEFINED = 0,     /*!< Unsure what state it is in */
    R_ROF_STATE_INITIALIZATION,    /*!< Opening file */
    R_ROF_STATE_WRITE_BLOCK_READY, /*!< Ready to write [another] block */
    R_ROF_STATE_READ_AT_HEADER,    /*!< Header info cleared */
    R_ROF_STATE_READ_AT_DATA,      /*!< Header info populated */
    R_ROF_STATE_READ_AT_FILE_END,  /*!< No more data */
    R_ROF_STATE_FILE_CLOSED,       /*!< File was closed */
    R_ROF_STATE_ERROR_OPEN,        /*!< Error while file is open */
    R_ROF_STATE_ERROR_CLOSED,      /*!< Error while file is closed */
} e_rof_state_t;

/*! ROF section header definition */
typedef struct {
    uint32_t name; /*!< What the name is for the section */
    uint32_t size; /*!< How large the section is */
    uint32_t at;   /*!< Where we have read/written to */
} st_rof_section_header_t;

/*! ROF file definition */
typedef struct st_rof_tag {
    FILE *                  p_file;  /*!< File pointer to open file */
    uint32_t                ftype;   /*!< File type, should normally be "ROF3" */
    e_rof_state_t           state;   /*!< State of file */
    st_rof_section_header_t section; /*!< Active section information, only when reading */
} st_rof_t;

/**********************************************************************************************************************
 Functions
 *********************************************************************************************************************/

extern r_result_t R_ROF_WriteOpen(st_rof_t * const p_rof, char const * const file_name);
extern r_result_t R_ROF_WriteBlock(st_rof_t * const p_rof, const uint32_t header, const uint32_t size, uint8_t const * const p_data);

extern r_result_t R_ROF_ReadOpen(st_rof_t * const p_rof, char const * const file_name);
extern r_result_t R_ROF_ReadBlock(st_rof_t * const p_rof, const uint32_t header, const uint32_t size, uint8_t * const p_data);

extern r_result_t R_ROF_Close(st_rof_t * const p_rof);

#endif /* R_ROF_H_ */

/******************************************************************************************************************//**
 * @} end group <My_Example_Module>
 *********************************************************************************************************************/
