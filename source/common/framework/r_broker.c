/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_broker.c
 * @brief         Source file for data broker in the PQS Framework
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

/*! Needed for thread naming */
#define _GNU_SOURCE
#include <pthread.h> /* For thread creation, mutexes and signals */

#include "r_common.h"

/*! Define framework core, otherwise r_module_broker.h will throw an error */
#define _R_FRAMEWORK_CORE_
#include "r_module_broker.h"

/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

#define BROKER_PRV_MAX_PENDING_TRIGGER (16u) /*!< Maximum number of external triggers */

/**********************************************************************************************************************
 Local Typedef definitions
 *********************************************************************************************************************/

/*! Info about external triggers. External triggers are created from outside of the normal processing flow. E.g. from the network parser
 * when reading a graph file */
typedef struct {
    st_module_instance_t * p_instance; /*!< Instance to trigger */
    uint32_t               channel;    /*!< Channel to trigger */
} st_trigger_info_t;

/*! List of external triggers */
typedef struct {
    st_trigger_info_t trigger_list[BROKER_PRV_MAX_PENDING_TRIGGER]; /*!< Array of pending external triggers.  */
    uint32_t          num_triggers;                               /*!< Number of external triggers in list */
    pthread_mutex_t   lock;                                       /*!< Mutex for external trigger list */
} st_triggers_t;

/*! Signal for data broker to process new data */
typedef struct {
    pthread_mutex_t       lock;     /*!< Locks new_data */
    pthread_cond_t        signal;   /*!< Signals changes to new_data */
    bool                  new_data; /*!< If new data is available for broker to process */
} st_broker_signal_t;

/*! Struct of all the global data of the data broker */
typedef struct {
    pthread_t             thread;             /*!< Thread of the data broker */
    e_module_state_t      state;              /*!< State of the data broker */
    st_broker_signal_t    wakeup;             /*!< Wakeup signal of the data broker */
    st_triggers_t         external_triggers;  /*!< List of external triggers to route */
} st_broker_t;

/**********************************************************************************************************************
 Local Variables
 *********************************************************************************************************************/

static st_broker_t s_broker; /*!< All of the data brokers global data */
static const char * s_object_type_name[] = E_OBJECT_TYPE_AS_STRING; /*!< String array of object types */

/**********************************************************************************************************************
 Functions
 *********************************************************************************************************************/

//TODO document static functions
/*! @cond STATIC */

static void clear_object_list(st_object_t * p_object_list[], uint32_t list_length)
{
    for (uint32_t index = 0; index < list_length; index++)
    {
        p_object_list[index] = NULL;
    }
}

static void clear_broker_links(st_module_instance_t * const p_instance)
{
    for (uint32_t channel = 0; channel < R_MODULE_MAX_CHANNELS; channel++)
    {
        p_instance->broker.link[channel].count = 0;

        for (uint32_t route = 0; route < R_BROKER_MAX_ROUTES; route++)
        {
            p_instance->broker.link[channel].list[route].p_instance = NULL;
            p_instance->broker.link[channel].list[route].channel    = R_MODULE_MAX_CHANNELS;
        }
    }
}

static void clear_broker_channel_info(st_module_instance_t * const p_instance)
{
    for (uint32_t channel = 0; channel < R_MODULE_MAX_CHANNELS; channel++)
    {
        R_COMMON_Memset(p_instance->broker.channel_info[channel].name, 0, R_MODULE_CHANNEL_NAME_SIZE);
        p_instance->broker.channel_info[channel].type      = R_OBJECT_TYPE_INVALID;
        p_instance->broker.channel_info[channel].direction = R_BROKER_DATA_DIRECTION_UNUSED;
    }
}

static r_result_t valid_channel(uint32_t channel, st_module_instance_t * p_instance, uint32_t direction_check_bitmask)
{
    r_result_t result = R_RESULT_FAILED;

    if (NULL == p_instance)
    {
        R_PRINT_ERROR("Null pointer");
        result = R_RESULT_FAILED;
    }
    else
    {
        /* Check for out of bounds channel id */
        if (channel >= R_MODULE_MAX_CHANNELS)
        {
            R_PRINT_ERROR("Channel index for instance \"%s\" out of bounds (was %d)", p_instance->name, channel);
            result = R_RESULT_FAILED;
        }

        /* Check for bitmask */
        else if (0u == (direction_check_bitmask & p_instance->broker.channel_info[channel].direction))
        {
            R_PRINT_ERROR("Channel bitmask wrong for channel \"%s.%s\".", p_instance->name, p_instance->broker.channel_info[channel].name);
            result = R_RESULT_FAILED;
        }

        /* nothing left to check! */
        else
        {
            result = R_RESULT_SUCCESS;
        }
    }

    return result;
}

static r_result_t add_object(st_module_instance_t * p_sink, uint32_t sink_channel, st_object_t * p_new_object)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Validate inputs */
    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        if (NULL == p_sink)
        {
            R_PRINT_ERROR("NULL pointer input");
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
    }

    /* Validate inputs */
    if (R_RESULT_SUCCESS == result)
    {
        if (sink_channel >= R_MODULE_MAX_CHANNELS)
        {
            R_PRINT_ERROR("Channel index out of bounds");
            result = R_RESULT_BAD_INPUT_ARGUMENTS;
        }
    }

    /* Validate inputs */
    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == p_new_object)
        {
            R_PRINT_ERROR("Passed an illegal object null pointer to module %s", p_sink->name);
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
        else
        {
            if (valid_channel(sink_channel, p_sink, R_BROKER_DATA_DIRECTION_BITMASK_ANY_INPUTS) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Invalid channel route input to module %s", p_sink->name);
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Increment counter */
    if (R_RESULT_SUCCESS == result)
    {
        if (R_OBJECT_IncrementRefCount(p_new_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Couldn't increment ref counter for input object to instance %s", p_sink->name);
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* Save off old pointer so we can delete it later */
        st_object_t * p_old_object = p_sink->broker.pending_object_list[sink_channel]; /*lint !e661 */

        /* Add new data */
        p_sink->broker.pending_object_list[sink_channel] = p_new_object; /*lint !e661 */

        /* Decrement old data */
        if (NULL != p_old_object)
        {
            R_PRINT_WARNING("Module %s dropped an input object for channel %s", p_sink->name, p_sink->broker.channel_info[sink_channel].name); /*lint !e662 */
            (void)R_OBJECT_DecrementRefCount(p_old_object);
        }
    }

    return result;
}

/* TODO: We have a few functions here that decrement counters, need to consider how to combine them all */

/* Assumes valid p_instance is passed */
static void route_instance_data(st_module_instance_t * const p_instance)
{
    /* Route data for this instance */
    for (uint32_t channel = 0; channel < R_MODULE_MAX_CHANNELS; channel++)
    {
        /* Route data */
        for (uint32_t route = 0; route < p_instance->broker.link[channel].count; route++)
        {
            R_PRINT_INFO("Routing data: %s.%s --> %s.%s",
                /* Source */
                /* name    */ p_instance->name,
                /* channel */ p_instance->broker.channel_info[channel].name,
                /* Sink */
                /* name    */ p_instance->broker.link[channel].list[route].p_instance->name,
                /* channel */ p_instance->broker.link[channel].list[route].p_instance->broker.channel_info[p_instance->broker.link[channel].list[route].channel].name);

            (void)add_object(   p_instance->broker.link[channel].list[route].p_instance,
                                p_instance->broker.link[channel].list[route].channel,
                                p_instance->broker.active_object_list[channel]);
        }

        /* Decrement count of object being this instance does not need it anymore */
        (void)R_OBJECT_DecrementRefCount(p_instance->broker.active_object_list[channel]);

        /* Remove from list */
        p_instance->broker.active_object_list[channel] = NULL;
    }
}

static r_result_t verify_object_list(st_object_t * object_list[], st_channel_info_t channel_info[], uint32_t direction_bitmask)
{
    r_result_t result = R_RESULT_SUCCESS;
    uint32_t ref_count = 0;

    /* Note:
     * It is not an error if an object doesn't exist. We could just be waiting for it.
     * Therefore do not print any errors in this function.
     */

    /* Loop through all objects */
    for (uint32_t channel_idx = 0; channel_idx < R_MODULE_MAX_CHANNELS; channel_idx++)
    {
        if (0u != (direction_bitmask & channel_info[channel_idx].direction))
        {
            /* Check if object exists */
            if (NULL != object_list[channel_idx])
            {
                /* Special handling for inout objects */
                if (R_BROKER_DATA_DIRECTION_INOUT == channel_info[channel_idx].direction)
                {
                    /* TODO this is a problem if the in out object gets routed to multiple modules */
                    if (R_OBJECT_GetRefCount(object_list[channel_idx], &ref_count) == R_RESULT_SUCCESS)
                    {
                        if (1u != ref_count)
                        {
                            result = R_RESULT_FAILED;
                        }
                    }
                    else
                    {
                        result = R_RESULT_FAILED;
                    }
                }
            }
            else
            {
                result = R_RESULT_FAILED;
            }
        }
    }
    return result;
}

static r_result_t route_all_data(void)
{
    r_result_t result = R_RESULT_SUCCESS;
    uint32_t num_instances = 0;
    st_module_instance_t *  p_instance = NULL;
    st_module_instance_t * p_instance_list = R_MODULE_GetInstanceList(&num_instances);

    for (uint32_t instance_index = 0; instance_index < num_instances; instance_index++)
    {
        /* Init every loop */
        result = R_RESULT_SUCCESS;
        p_instance = &(p_instance_list[instance_index]);

        /* Check algo state */
        if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
        {
            (void)pthread_mutex_lock(&(p_instance->algo.lock));

            if (ALGORITHM_OUTPUT_READY != p_instance->algo.state)
            {
                result = R_RESULT_FAILED;
            }

            (void)pthread_mutex_unlock(&(p_instance->algo.lock));
        }

        /* Check if output data exists */
        if (R_RESULT_SUCCESS == result)
        {
            if (verify_object_list(p_instance->broker.active_object_list, p_instance->broker.channel_info, R_BROKER_DATA_DIRECTION_BITMASK_ANY_OUTPUTS) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Active objects list from %s wasn't correct!", p_instance->name);
                result = R_RESULT_FAILED;
            }
        }

        /* Lock output objects */
        if (R_RESULT_SUCCESS == result)
        {
            for (uint32_t channel_index = 0; channel_index < R_MODULE_MAX_CHANNELS; channel_index++)
            {
                if (R_BROKER_DATA_DIRECTION_OUT == p_instance->broker.channel_info[channel_index].direction)
                {
                    if (R_OBJECT_Lock(p_instance->broker.active_object_list[channel_index]) != R_RESULT_SUCCESS)
                    {
                        R_PRINT_ERROR("Error locking output objects for %s", p_instance->name);
                        result = R_RESULT_FAILED;
                    }
                }
            }
        }

        if (R_RESULT_SUCCESS == result)
        {
            route_instance_data(p_instance);

            /* Set state to idle */
            (void)pthread_mutex_lock(&(p_instance->algo.lock));
            p_instance->algo.state = ALGORITHM_IDLE;
            (void)pthread_mutex_unlock(&(p_instance->algo.lock));
        }
    }

    return result;
}

static r_result_t route_external_triggers(void)
{
    r_result_t result = R_RESULT_SUCCESS;

    (void)pthread_mutex_lock(&s_broker.external_triggers.lock);

    for (uint32_t i = 0; i < s_broker.external_triggers.num_triggers; i++)
    {
        /* Get trigger object */
        st_object_t * p_trigger_object = R_OBJECT_GetFreeObject(R_OBJECT_TYPE_TRIGGER);

        if (NULL == p_trigger_object)
        {
            R_PRINT_ERROR("Error getting trigger");
            result = R_RESULT_FAILED;
        }

        /* Lock object */
        if (R_RESULT_SUCCESS == result)
        {
            if (R_OBJECT_Lock(p_trigger_object) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Error locking trigger");
                result = R_RESULT_FAILED;
            }
        }

        /* Add object to pending object list */
        if (R_RESULT_SUCCESS == result)
        {
            result = add_object(s_broker.external_triggers.trigger_list[i].p_instance, s_broker.external_triggers.trigger_list[i].channel, p_trigger_object);
        }

        /* Clear trigger */
        s_broker.external_triggers.trigger_list[i].p_instance = NULL;
        s_broker.external_triggers.trigger_list[i].channel    = R_MODULE_MAX_CHANNELS;

    }

    /* All triggers routed, reset counter */
    s_broker.external_triggers.num_triggers = 0;

    (void)pthread_mutex_unlock(&s_broker.external_triggers.lock);

    return result;
}

static r_result_t process_data(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_object_t * p_output_object = NULL;

    (void)pthread_mutex_lock(&p_instance->algo.lock);

    /* Can only update active objects when algo is idle */
    if (ALGORITHM_IDLE != p_instance->algo.state)
    {
        R_PRINT_ERROR("Thread %d is not idle", p_instance->name);
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* Copy pending input objects to active objects. Clear pending objects */
        for (uint32_t index = 0; index < R_MODULE_MAX_CHANNELS; index++)
        {
            p_instance->broker.active_object_list[index] = p_instance->broker.pending_object_list[index];
            p_instance->broker.pending_object_list[index] = NULL;
        }

        /* Get output objects */
        for (uint32_t index = 0; index < R_MODULE_MAX_CHANNELS; index++)
        {
            if (R_BROKER_DATA_DIRECTION_OUT == p_instance->broker.channel_info[index].direction)
            {
                p_output_object = R_OBJECT_GetFreeObject(p_instance->broker.channel_info[index].type);

                if (R_OBJECT_CheckObjectType(p_output_object, p_instance->broker.channel_info[index].type) == R_RESULT_SUCCESS)
                {
                    p_instance->broker.active_object_list[index] = p_output_object;
                }
                else
                {
                    R_PRINT_ERROR("Error validating output object for %s", p_instance->name);
                    result = R_RESULT_FAILED;
                }
            }
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* Update the algorithm state */
        p_instance->algo.state = ALGORITHM_HAS_DATA;

        /* Signal new input to waiting main_thread */
        if (0 == pthread_cond_signal(&(p_instance->algo.signal)))
        {
            R_PRINT_INFO("Kicking off thread %s", p_instance->name);
        }
        else
        {
            R_PRINT_ERROR("Thread %d error", p_instance->name);
            result = R_RESULT_FAILED;
        }
    }

    (void)pthread_mutex_unlock(&p_instance->algo.lock);

    return result;
}

/* Starts processing data for every module that is ready to run.  Used by
 * broker_thread() */
static void process_all_data(void)
{
    r_result_t result = R_RESULT_SUCCESS;
    uint32_t num_instances = 0;
    st_module_instance_t * p_instance_list = R_MODULE_GetInstanceList(&num_instances);
    st_module_instance_t * p_instance = NULL;

    for (uint32_t i = 0; i < num_instances; i++)
    {
        /* Init every loop */
        result = R_RESULT_SUCCESS;
        p_instance = &(p_instance_list[i]);

        /* Check algorithm is in the correct state */
        if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
        {
            (void)pthread_mutex_lock(&p_instance->algo.lock);

            if (ALGORITHM_IDLE != p_instance->algo.state)
            {
                /* This is not an error, this happens under normal conditions */
                result = R_RESULT_FAILED;
            }

            (void)pthread_mutex_unlock(&p_instance->algo.lock);
        }

        /* Verify object list */
        if (R_RESULT_SUCCESS == result)
        {
            result = verify_object_list(p_instance->broker.pending_object_list, p_instance->broker.channel_info, R_BROKER_DATA_DIRECTION_BITMASK_ANY_INPUTS);
        }

        /* Kick off data processing */
        if (R_RESULT_SUCCESS == result)
        {
            result = process_data(p_instance);
        }
    }
}

/* This is the execution thread for the broker */
static void * broker_thread(void * p_pthread_data)
{
    (void) p_pthread_data; //Not used

    R_PRINT_INFO("Broker thread attached");

    /* Wait for start procedure to complete */
    (void)pthread_mutex_lock(&s_broker.wakeup.lock);
    while (MODULE_STATE_STARTED > s_broker.state)
    {
        (void)pthread_cond_wait(&s_broker.wakeup.signal, &s_broker.wakeup.lock);
    }
    (void)pthread_mutex_unlock(&(s_broker.wakeup.lock));

    R_PRINT_INFO("Broker thread started");

    /* Act as broker */
    while (MODULE_STATE_STARTED == s_broker.state)
    {
        /* Step 1: Route new data */
        R_PRINT_INFO("Broker is routing data");
        (void)route_all_data();

        /* Step 2: Route pending external triggers */
        R_PRINT_INFO("Broker is routing external triggers");
        (void)route_external_triggers();

        /* Step 2: Process existing data */
        R_PRINT_INFO("Broker is processing data");
        process_all_data();

        /* Step 3: Wait for new data */
        (void)pthread_mutex_lock(&s_broker.wakeup.lock);

        /* Check if there is an already pending signal */
        while ((MODULE_STATE_STARTED == s_broker.state) && (false == s_broker.wakeup.new_data))
        {
            (void)pthread_cond_wait(&s_broker.wakeup.signal, &s_broker.wakeup.lock);
        }

        /* Reset variable */
        s_broker.wakeup.new_data = false;

        (void)pthread_mutex_unlock(&(s_broker.wakeup.lock));
    }

    if (MODULE_STATE_STOPPING == s_broker.state)
    {
        s_broker.state = MODULE_STATE_THREAD_STOPPED;
    }

    R_PRINT_INFO("Broker thread exiting");

    return NULL;
} /*lint !e715 */

static r_result_t instance_and_channel_check(st_module_instance_t * const p_instance, uint32_t channel)
{
    r_result_t result = R_RESULT_FAILED;

    if (NULL == p_instance)
    {
        /* Invalid p_instance */
        R_PRINT_ERROR("Illegal null pointer for p_instance");
        result = R_RESULT_FAILED;
    }
    else if (R_MODULE_MAX_CHANNELS <= channel)
    {
        /* Invalid channel # */
        R_PRINT_ERROR("Invalid channel number: %d", channel);
        result = R_RESULT_FAILED;
    }
    else
    {
        /* Everything worked! */
        result = R_RESULT_SUCCESS;
    }

    return result;
}

/* TODO document static functions */
/*! @endcond */

/******************************************************************************/
/*                                                                            */
/*                           Semi-Public Interface                            */
/*                                                                            */
/******************************************************************************/

/* Note: These functions shall only be called by r_module.c */

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_Init
 * @brief           Initializes the data broker
 *                  On entry: Broker state must be MODULE_STATE_NOT_INITIALIZED or MODULE_STATE_DEINITIALIZED
 *                  On exit: Broker state will be set to MODULE_STATE_DEINITIALIZED or MODULE_STATE_DEINITIALIZATION_ERROR
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_BROKER_Init(void)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Init state */
    if ((MODULE_STATE_NOT_INITIALIZED == s_broker.state) || \
        (MODULE_STATE_DEINITIALIZED == s_broker.state))
    {
        s_broker.state = MODULE_STATE_INITIALIZING;

        /* Init wakeup struct */
        s_broker.wakeup.new_data = false;

        if (0 != pthread_mutex_init(&s_broker.wakeup.lock, NULL))
        {
            R_PRINT_ERROR("Mutex init error");
            result = R_RESULT_FAILED;
        }

        if (0 != pthread_cond_init(&s_broker.wakeup.signal, NULL))
        {
            R_PRINT_ERROR("Condition init error");
            result = R_RESULT_FAILED;
        }

        /* Init external trigger struct */
        s_broker.external_triggers.num_triggers = 0;

        if (0 != pthread_mutex_init(&s_broker.external_triggers.lock, NULL))
        {
            R_PRINT_ERROR("Mutex init error");
            result = R_RESULT_FAILED;
        }

        for (uint32_t i = 0; i < BROKER_PRV_MAX_PENDING_TRIGGER; i++)
        {
            s_broker.external_triggers.trigger_list[i].p_instance = NULL;
            s_broker.external_triggers.trigger_list[i].channel    = R_MODULE_MAX_CHANNELS;
        }

        /* Get return code */
        if (R_RESULT_SUCCESS == result)
        {
            s_broker.state = MODULE_STATE_INITIALIZED;
        }
        else
        {
            s_broker.state = MODULE_STATE_INITIALIZATION_ERROR;
        }
    }
    else
    {
        R_PRINT_ERROR("Invalid state");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_Deinit
 * @brief           Deinitializes the data broker. R_BROKER_Init must be called first.
 *                  On entry: Broker state must be MODULE_STATE_STOPPED
 *                  On exit: Broker state will be set to MODULE_STATE_DEINITIALIZED or MODULE_STATE_DEINITIALIZATION_ERROR
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_BROKER_Deinit(void)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (MODULE_STATE_STOPPED == s_broker.state)
    {
        s_broker.state = MODULE_STATE_DEINITIALIZING;

        if (0 != pthread_mutex_destroy(&s_broker.wakeup.lock))
        {
            R_PRINT_ERROR("Mutex destruction error");
            result = R_RESULT_FAILED;
        }

        if (0 != pthread_cond_destroy(&s_broker.wakeup.signal))
        {
            R_PRINT_ERROR("Condition destruction error");
            result = R_RESULT_FAILED;
        }

        if (0 != pthread_mutex_destroy(&s_broker.external_triggers.lock))
        {
            R_PRINT_ERROR("Mutex destruction error");
            result = R_RESULT_FAILED;
        }

        if (R_RESULT_SUCCESS == result)
        {
            s_broker.state = MODULE_STATE_DEINITIALIZED;
        }
        else
        {
            s_broker.state = MODULE_STATE_DEINITIALIZATION_ERROR;
        }
    }
    else
    {
        R_PRINT_ERROR("Invalid state");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_InitInstance
 * @brief           Initializes the data used by the data broker for routing the data from a given module instance.
 * @param [in]      p_instance - Pointer to the module instance who's broker data should be initialized
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_BROKER_InitInstance(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NULL != p_instance)
    {
        /* Init object lists */
        clear_object_list(p_instance->broker.active_object_list, R_MODULE_MAX_CHANNELS);
        clear_object_list(p_instance->broker.pending_object_list, R_MODULE_MAX_CHANNELS);

        /* Init broker */
        clear_broker_links(p_instance);
        clear_broker_channel_info(p_instance);
    }
    else
    {
        R_PRINT_ERROR("Illegal NULL pointer input");
        result = R_RESULT_FAILED;
    }

    /* Check function pointer exists */
    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == p_instance->p_module_interface->init_channels)
        {
            R_PRINT_ERROR("Module %s is missing init_channels callback", p_instance->p_module_interface->module_name);
            result = R_RESULT_FAILED;
        }
    }

    /* Call function pointer */
    if (R_RESULT_SUCCESS == result)
    {
        if (p_instance->p_module_interface->init_channels(p_instance) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error with init_channels callback for instance %s", p_instance->name);
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_PreInstanceStart
 * @brief           Call this before starting any module instances. By this time, you should have all routes defined
 *                  On entry: Broker state must be MODULE_STATE_INITIALIZED
 *                  On exit: Broker state will be set to MODULE_STATE_STARTING or MODULE_STATE_START_ERROR
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_BROKER_PreInstanceStart(void)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Check state */
    if (MODULE_STATE_INITIALIZED == s_broker.state)
    {
        /* Create thread */
        if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
        {
            if (pthread_create(&s_broker.thread, NULL, broker_thread, NULL) != 0)
            {
                R_PRINT_ERROR("pthread creation error");
                result = R_RESULT_FAILED;
            }
        }

        /* Name thread */
        if (R_RESULT_SUCCESS == result)
        {
            /* Name thread */
            if((pthread_setname_np(s_broker.thread, "broker")) != 0)
            {
                R_PRINT_ERROR("pthread naming error");
                result = R_RESULT_FAILED;
            }
        }

        /* Set state */
        if (R_RESULT_SUCCESS == result)
        {
            s_broker.state = MODULE_STATE_STARTING;
        }
        else
        {
            s_broker.state = MODULE_STATE_START_ERROR;
        }
    }
    else
    {
        R_PRINT_ERROR("Broker is in wrong state");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_PostInstanceStart
 * @brief           Call this after starting all module instances
 *                  On entry: Broker state must be MODULE_STATE_STARTING
 *                  On exit: Broker state will be set to MODULE_STATE_STARTED or MODULE_STATE_START_ERROR
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_BROKER_PostInstanceStart(void)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (MODULE_STATE_STARTING == s_broker.state)
    {
        /* Wake thread up */
        (void)pthread_mutex_lock(&s_broker.wakeup.lock);

        if (pthread_cond_signal(&s_broker.wakeup.signal) != 0)
        {
            R_PRINT_ERROR("Error signaling broker");
            result = R_RESULT_FAILED;
        }

        (void)pthread_mutex_unlock(&s_broker.wakeup.lock);

        /* Set state */
        if (R_RESULT_SUCCESS == result)
        {
            s_broker.state = MODULE_STATE_STARTED;
        }
        else
        {
            s_broker.state = MODULE_STATE_START_ERROR;
        }
    }
    else
    {
        R_PRINT_ERROR("Broker is in wrong state");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_PreInstanceStop
 * @brief           Call this to stop the broker
 *                  On entry: Broker state must be MODULE_STATE_STARTING or MODULE_STATE_STARTED
 *                  On exit: Broker state will be set to MODULE_STATE_STOPPED or MODULE_STATE_STOPPING_ERROR
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_BROKER_PreInstanceStop(void)
{
    r_result_t result = R_RESULT_SUCCESS;

    if ((MODULE_STATE_STARTING == s_broker.state)
    ||  (MODULE_STATE_STARTED  == s_broker.state))
    {
        /* Set state to stopping */
        s_broker.state = MODULE_STATE_STOPPING;

        /* Wake thread up */
        (void)pthread_mutex_lock(&s_broker.wakeup.lock);

        if (pthread_cond_signal(&s_broker.wakeup.signal) != 0)
        {
            R_PRINT_ERROR("Error signaling broker");
            result = R_RESULT_FAILED;
        }

        (void)pthread_mutex_unlock(&s_broker.wakeup.lock);

        if (pthread_join(s_broker.thread, NULL) != 0)
        {
            /* Thread not joined, set state to error */
            R_PRINT_ERROR("Error joinging thread");
            result = R_RESULT_FAILED;
        }

        /* Set state */
        if (R_RESULT_SUCCESS == result)
        {
            s_broker.state = MODULE_STATE_STOPPED;
        }
        else
        {
            s_broker.state = MODULE_STATE_STOPPING_ERROR;
        }

    }
    else
    {
        R_PRINT_ERROR("Broker is in wrong state");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_InstanceStop
 * @brief           Deinitializes the data used by the data broker for routing the data from a given module instance.
 * @param [in]      p_instance - Pointer to the module instance who's broker data should be initialized
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_BROKER_InstanceStop(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    for (uint32_t channel = 0; channel < R_MODULE_MAX_CHANNELS; channel++)
    {
        /* Lock pending outputs  */
        if (0u != (R_BROKER_DATA_DIRECTION_OUT & p_instance->broker.channel_info[channel].direction))
        {
            if (NULL != p_instance->broker.active_object_list[channel])
            {
                result |= R_OBJECT_Lock(p_instance->broker.active_object_list[channel]);
            }
        }

        /* Decrement Reference counters */
        if (NULL != p_instance->broker.active_object_list[channel])
        {
            result |= R_OBJECT_DecrementRefCount(p_instance->broker.active_object_list[channel]);
            p_instance->broker.active_object_list[channel] = NULL;
        }

        /* Decrement Reference counters */
        if (NULL != p_instance->broker.pending_object_list[channel])
        {
            result |= R_OBJECT_DecrementRefCount(p_instance->broker.pending_object_list[channel]);
            p_instance->broker.pending_object_list[channel] = NULL;
        }
    }

    /* We have removed the inputs so set algo state to idle */
    p_instance->algo.state = ALGORITHM_IDLE;

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_CallBack
 * @brief           Call this function to signal there is new data to be processed by the data broker
 * @param [in]      p_instance - Pointer to the module instance that has new data to be routed
 * @param [in]      result - Result of the module's processing
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
void R_BROKER_CallBack(st_module_instance_t * const p_instance, r_result_t result)
{
    if (NULL == p_instance)
    {
        /* Major error */
        R_PRINT_ERROR("Null pointer given!");
    }
    else
    {
        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("CallBack for instance '%s' had returned an error", p_instance->name);
        }
        else
        {
            R_PRINT_INFO("CallBack for instance '%s' had return %d", p_instance->name, result);
        }

        if (MODULE_STATE_STARTED == s_broker.state)
        {
            R_PRINT_INFO("Adding result from %s to broker for routing", p_instance->name);

            /* Signal broker thread of new data to be routed */
            (void)pthread_mutex_lock(&s_broker.wakeup.lock);

            s_broker.wakeup.new_data = true;

            if (pthread_cond_signal(&s_broker.wakeup.signal) != 0)
            {
                R_PRINT_ERROR("Error signaling broker");
            }

            (void)pthread_mutex_unlock(&s_broker.wakeup.lock);
        }
        else
        {
            /* Incorrect state to route data */
            if ((MODULE_STATE_STOPPING       == s_broker.state) ||
                (MODULE_STATE_THREAD_STOPPED == s_broker.state) ||
                (MODULE_STATE_STOPPED        == s_broker.state))
            {
                R_PRINT_INFO("Broker is stopped and no longer routing data. Output from %s will not be routed", p_instance->name);
            }
            else
            {
                R_PRINT_ERROR("Incorrect broker state for callback from module %s!", p_instance->name);
            }
        }
    }
}

/******************************************************************************/
/*                                                                            */
/*                              Public Interface                              */
/*                                                                            */
/******************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_InitChannelInfo
 * @brief           Modules call this function during initialization to initilize every one of thier input and
 *                  output channels
 * @param [in]      p_instance - Pointer to the module instance
 * @param [in]      channel - Channel number to initialize
 * @param [in]      name - String name of channel
 * @param [in]      type - Type of object that channel holds
 * @param [in]      direction - Direction of the channel
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_BROKER_InitChannelInfo(st_module_instance_t * const p_instance, uint32_t channel, char const * const name, e_object_type_t type, uint32_t direction)
{
    r_result_t result = instance_and_channel_check(p_instance, channel);

    if (R_RESULT_SUCCESS == result)
    {
        /* Check if the module info is cleared */
        if ((R_COMMON_STRING_Length(p_instance->broker.channel_info[channel].name) != 1u)
        ||  (R_OBJECT_TYPE_INVALID          != p_instance->broker.channel_info[channel].type)
        ||  (R_BROKER_DATA_DIRECTION_UNUSED != p_instance->broker.channel_info[channel].direction))
        {
            R_PRINT_ERROR("Trying to init a channel twice");
            result = R_RESULT_FAILED;
        }
        else
        {
            result |= R_COMMON_STRING_Copy(p_instance->broker.channel_info[channel].name, name, R_MODULE_CHANNEL_NAME_SIZE);
            p_instance->broker.channel_info[channel].type      = type;
            p_instance->broker.channel_info[channel].direction = direction;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_GetObjectPtr
 * @brief           Modules call this function during data processing to get the object pointer for a given channel
 * @param [in]      p_instance - Pointer to the module instance
 * @param [in]      channel - Channel number to get the object from
 * @retval          Pointer to the object for a given channel.
 *********************************************************************************************************************/
st_object_t * R_BROKER_GetObjectPtr(st_module_instance_t * const p_instance, uint32_t channel)
{
    st_object_t * p_object = NULL;
    r_result_t result = instance_and_channel_check(p_instance, channel);

    /* Check algorithm is in the correct state */
    if (R_RESULT_SUCCESS == result)
    {
        (void)pthread_mutex_lock(&p_instance->algo.lock);

        if (ALGORITHM_BUSY != p_instance->algo.state)
        {
            R_PRINT_ERROR("Module %s in in the wrong state", p_instance->name);
            result = R_RESULT_FAILED;
        }

        (void)pthread_mutex_unlock(&p_instance->algo.lock);
    }

    if (R_RESULT_SUCCESS == result)
    {
        if ((R_OBJECT_TYPE_INVALID          == p_instance->broker.channel_info[channel].type)
        ||  (R_BROKER_DATA_DIRECTION_UNUSED == p_instance->broker.channel_info[channel].direction))
        {
            /* Not set up */
            result = R_RESULT_FAILED;
        }
        else
        {
            p_object = p_instance->broker.active_object_list[channel];

            if (NULL == p_object)
            {
                /* Data doesn't exist */
                result = R_RESULT_FAILED;
            }
        }
    }

    if (R_RESULT_SUCCESS != result)
    {
        p_object = NULL;
    }
    return p_object;
}

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_ReplaceOutputObject
 * @brief           This is a specialized function that only needs to be used if the module keeps an internal queue of
 *                  objects. .This function will replace the object at the given channel number with the supplied
 *                  object pointer. In most cases modules should not have to do this. Take care if using this function.
 *                  Once the output object is replaced the data broker has "lost track" of it, meaning the module itself.
 *                  must keep track of this object
 * @param [in]      p_instance - Pointer to the module instance
 * @param [in]      channel - Channel number to get the object from
 * @param [in]      p_object - Pointer to the object to replace the current object with
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_BROKER_ReplaceOutputObject(st_module_instance_t * const p_instance, uint32_t channel, st_object_t * const p_object)
{
    r_result_t result = instance_and_channel_check(p_instance, channel);

    /* Check algorithm is in the correct state */
    if (R_RESULT_SUCCESS == result)
    {
        (void)pthread_mutex_lock(&p_instance->algo.lock);

        if (ALGORITHM_BUSY != p_instance->algo.state)
        {
            R_PRINT_ERROR("Module %s in in the wrong state", p_instance->name);
            result = R_RESULT_FAILED;
        }

        (void)pthread_mutex_unlock(&p_instance->algo.lock);
    }

    /* Check if object is NULL */
    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == p_object)
        {
            R_PRINT_ERROR("NULL input");
            result = R_RESULT_FAILED;
        }
    }

    /* Check object type */
    if (R_RESULT_SUCCESS == result)
    {
        e_object_type_t obj_type = R_OBJECT_TYPE_INVALID;

        if (R_OBJECT_GetObjectType(p_object, &obj_type) == R_RESULT_SUCCESS)
        {
            if (obj_type != p_instance->broker.channel_info[channel].type)
            {
                /* Object type does not match */
                R_PRINT_ERROR("Couldn't add output from %s to broker: Type mismatch for Channel %d.  Expected type %s, got %s",
                        p_instance->name, channel, s_object_type_name[p_instance->broker.channel_info[channel].type], s_object_type_name[obj_type]);
                result = R_RESULT_FAILED;
            }
        }
        else
        {
            R_PRINT_ERROR("Failed to get object type");
            result = R_RESULT_FAILED;
        }
    }

    /* Check object direction */
    if (R_RESULT_SUCCESS == result)
    {
        if (R_BROKER_DATA_DIRECTION_OUT != p_instance->broker.channel_info[channel].direction)
        {
            /* Data direction isn't a pure output */
            R_PRINT_ERROR("Couldn't add output from %s to broker: Channel %d is not a pure output", p_instance->name, channel);
            result = R_RESULT_FAILED;
        }
    }

    /* Replace output object with provided object */
    if (R_RESULT_SUCCESS == result)
    {
        p_instance->broker.active_object_list[channel] = p_object;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_GetChannelByName
 * @brief           Returns the channel number for a given channel name
 * @param [in]      p_instance - Pointer to the module instance
 * @param [in]      channel_name - String name of a channel
 * @param[out]      p_channel - Pointer to the channel to overwrite with channel info
 * @retval          R_RESULT_SUCCESS on success
 *********************************************************************************************************************/
r_result_t R_BROKER_GetChannelByName(st_module_instance_t * const p_instance, char const * const channel_name, uint32_t * p_channel)
{
    r_result_t result = R_RESULT_FAILED;

    if (NULL != p_instance)
    {
        for (uint32_t index = 0; index < R_MODULE_MAX_CHANNELS; index++)
        {
            char const * index_name = p_instance->broker.channel_info[index].name;

            if (R_COMMON_STRING_Compare(channel_name, index_name) == R_RESULT_SUCCESS)
            {
                *p_channel = index;
                result = R_RESULT_SUCCESS;
                break;
            }
        }

        if (R_RESULT_FAILED == result)
        {
            R_PRINT_ERROR("Could not find channel %s.%s", p_instance->name, channel_name);
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_Link
 * @brief           Links the sink input channel to the source output channel
 * @param [in]      p_source - Pointer to the module instance of the source channel
 * @param [in]      source_channel - Channel number of the source channel to link
 * @param [in]      p_sink - Pointer to the module instance of the sink channel
 * @param [in]      sink_channel - Channel number of the sink channel to link
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_BROKER_Link(st_module_instance_t * const p_source, uint32_t source_channel, st_module_instance_t * const p_sink, uint32_t sink_channel)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Only allow linking after broker has been initialized but before it has been started */
    if (MODULE_STATE_INITIALIZED != s_broker.state)
    {
        R_PRINT_ERROR("Wrong state for broker link.  Expected %d, got %d", MODULE_STATE_INITIALIZED, s_broker.state);
        result = R_RESULT_FAILED;
    }

    /* Make sure channels are valid */
    if (R_RESULT_SUCCESS == result)
    {
        result |= valid_channel(source_channel, p_source, R_BROKER_DATA_DIRECTION_BITMASK_ANY_OUTPUTS);
        result |= valid_channel(sink_channel,   p_sink,   R_BROKER_DATA_DIRECTION_BITMASK_ANY_INPUTS);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("A channel was invalid");
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        st_broker_link_t * p_link = &(p_source->broker.link[source_channel]);

        if (R_BROKER_MAX_ROUTES <= p_link->count)
        {
            /* Count is too large, exceeded linking capability */
            R_PRINT_ERROR("Maximum routes for this channel (%d/%d)", p_link->count, R_BROKER_MAX_ROUTES);
            result = R_RESULT_FAILED;
        }
        else
        {
            /* Add link to list */
            p_link->list[p_link->count].p_instance = p_sink;
            p_link->list[p_link->count].channel    = sink_channel;
            (p_link->count)++;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_LinkByName
 * @brief           Links the sink input channel to the source output channel
 * @param [in]      source_name - String name of the source module
 * @param [in]      source_channel_name - Sting name of the source channel
 * @param [in]      sink_name - String name of the sink channel
 * @param [in]      sink_channel_name - String name of the sink channel
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_BROKER_LinkByName(char const * const source_name, char const * const source_channel_name, char const * const sink_name, char const * const sink_channel_name)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_module_instance_t * p_source = NULL;
    st_module_instance_t * p_sink   = NULL;
    uint32_t source_channel = 0;
    uint32_t sink_channel   = 0;

    /* Get the module instance */
    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        p_source = R_MODULE_GetInstanceByName(source_name);
        p_sink   = R_MODULE_GetInstanceByName(sink_name);

        if (NULL == p_source)
        {
            R_PRINT_ERROR("Error module instance for %s,", source_name);
            result = R_RESULT_FAILED;
        }

        if (NULL == p_sink)
        {
            R_PRINT_ERROR("Error module instance for %s,", sink_name);
            result = R_RESULT_FAILED;
        }
    }

    /* Get the channel number */
    if (R_RESULT_SUCCESS == result)
    {
        if (R_BROKER_GetChannelByName(p_source, source_channel_name, &source_channel) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error getting channel for module %s channel %s,", source_name, source_channel_name);
            result = R_RESULT_FAILED;
        }

        if (R_BROKER_GetChannelByName(p_sink, sink_channel_name, &sink_channel) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error getting channel for module %s channel %s,", sink_name, sink_channel_name);
            result = R_RESULT_FAILED;
        }
    }

    /* Link in data broker */
    if (R_RESULT_SUCCESS == result)
    {
        R_PRINT_INFO("Linking %s.%s (%d) to %s.%s (%d)", source_name, source_channel_name, source_channel, sink_name, sink_channel_name, sink_channel);

        result = R_BROKER_Link(p_source, (uint32_t)source_channel, p_sink, (uint32_t)sink_channel);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_Trigger
 * @brief           Sends a trigger signal to the specified module instance.  Really, this is just a wrapper for
 *                  R_BROKER_AddObject()
 * @param [in]      p_sink - Pointer to module instance to trigger
 * @param [in]      sink_channel - Channel number to send trigger event to
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_BROKER_Trigger(st_module_instance_t * const p_sink, uint32_t sink_channel)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Only allow triggering after broker has started */
    if (MODULE_STATE_STARTED == s_broker.state)
    {
        /* Save off trigger to be routed */
        (void)pthread_mutex_lock(&s_broker.external_triggers.lock);
        s_broker.external_triggers.trigger_list[s_broker.external_triggers.num_triggers].p_instance = p_sink;
        s_broker.external_triggers.trigger_list[s_broker.external_triggers.num_triggers].channel    = sink_channel;
        s_broker.external_triggers.num_triggers++;
        (void)pthread_mutex_unlock(&s_broker.external_triggers.lock);

        /* Signal broker thread of new data to be routed */
        (void)pthread_mutex_lock(&s_broker.wakeup.lock);

        s_broker.wakeup.new_data = true;

        if (pthread_cond_signal(&s_broker.wakeup.signal) != 0)
        {
            R_PRINT_ERROR("Error signaling broker");
            result = R_RESULT_FAILED;
        }

        (void)pthread_mutex_unlock(&s_broker.wakeup.lock);
    }
    else
    {
        R_PRINT_ERROR("Broker is not yet initialized");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_TriggerByName
 * @brief           Sends a trigger signal to the specified module instance.  Uses a name rather than module pointer.
 * @param [in]      sink_name - Name of sink instance
 * @param [in]      sink_channel_name - Name of channel to trigger
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_BROKER_TriggerByName(char const * const sink_name, char const * const sink_channel_name)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_module_instance_t * p_sink = NULL;
    uint32_t sink_channel = 0;

    /* Get the module instance */
    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        p_sink   = R_MODULE_GetInstanceByName(sink_name);

        if (NULL == p_sink)
        {
            R_PRINT_ERROR("Error module instance for %s,", sink_name);
            result = R_RESULT_FAILED;
        }
    }

    /* Get the channel number */
    if (R_RESULT_SUCCESS == result)
    {
        if (R_BROKER_GetChannelByName(p_sink, sink_channel_name, &sink_channel) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error getting channel for module %s channel %s,", sink_name, sink_channel_name);
            result = R_RESULT_FAILED;
        }
    }

    /* Trigger */
    if (R_RESULT_SUCCESS == result)
    {
        result = R_BROKER_Trigger(p_sink, (uint32_t)sink_channel);
    }

    return result;
}
/***********************************************************************************************************************
 End of function R_BROKER_TriggerByName
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_BROKER_PrintChannels
 * @brief           Prints all of the channel info for all module instances.
 *********************************************************************************************************************/
void R_BROKER_PrintChannels(void)
{
    uint32_t num_instance = 0;
    st_module_instance_t * p_instance_list = R_MODULE_GetInstanceList(&num_instance);

    char channel_name_in[]      = "[in]     ";
    char channel_name_out[]     = "[out]    ";
    char channel_name_inout[]   = "[inout]  ";
    char channel_name_default[] = "[ ]      ";
    char * p_channel_name       = channel_name_default;

    R_PRINT("################################################################################\n"
            "# \n"
            "# \t Printing channel data for all active modules... \n"
            "# \n");

    for (uint32_t instance_index = 0; instance_index < num_instance; instance_index++)
    {
        R_PRINT("# \t Instance: %s \n",  p_instance_list[instance_index].name);
        for (uint32_t channel_index = 0; channel_index < R_MODULE_MAX_CHANNELS; channel_index++)
        {
            if (R_BROKER_DATA_DIRECTION_UNUSED != p_instance_list[instance_index].broker.channel_info[channel_index].direction)
            {
                switch (p_instance_list[instance_index].broker.channel_info[channel_index].direction)
                {
                    case R_BROKER_DATA_DIRECTION_IN:
                    {
                        p_channel_name = channel_name_in;
                        break;
                    }

                    case R_BROKER_DATA_DIRECTION_OUT:
                    {
                        p_channel_name = channel_name_out;
                        break;
                    }

                    case R_BROKER_DATA_DIRECTION_INOUT:
                    {
                        p_channel_name = channel_name_inout;
                        break;
                    }

                    default:
                    {
                        p_channel_name = channel_name_default;
                        R_PRINT_ERROR("Channel direction not supported");
                        break;
                    }
                }

                R_PRINT("# \t \t Channel %s: %s \n",
                        p_channel_name,
                        p_instance_list[instance_index].broker.channel_info[channel_index].name);
            }
        }
    }

    R_PRINT( "# \n"
            "################################################################################\n");
}
