/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_network_parser.c
 * @brief         Source file for text file parsing to determine the PQS graph on startup.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include "r_common.h"

/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

/* TODO document static macros */
/*! @cond STATIC */
#define COMMAND(n, c, sn, si) {     \
    .command         = #n,          \
    .word_count      = c,           \
    .secondary_word  = sn,          \
    .secondary_index = si,          \
    .parser          = parser_ ## n \
}
/*! @endcond */

/**********************************************************************************************************************
 Local Typedef definitions
 *********************************************************************************************************************/

/* TODO document static types */
/*! @cond STATIC */
typedef enum {
    NP_MODE_BUILD_ALIAS_MAP = 1,
    NP_MODE_INSTANTIATE_MODULE_INSTANCES,
    NP_MODE_LINK_CHANNELS,
    NP_MODE_SETUP_COMPLETE,
    NP_MODE_TRIGGER_MODULES,
    NP_MODE_RUNNING,
} e_np_mode_t;

typedef enum {
    INPUT_TYPE_UNKNOWN = 0,
    INPUT_TYPE_INPUT,
    INPUT_TYPE_OUTPUT,
    INPUT_TYPE_INSTANCE,
    INPUT_TYPE_LINK,
    INPUT_TYPE_TRIGGER,
    INPUT_TYPE_RECORD,
} e_input_type;

typedef enum {
    LINE_PARSER_MODE_SEARCHING_FOR_WORD,
    LINE_PARSER_MODE_INSIDE_WORD,
    LINE_PARSER_MODE_INSIDE_COMMENT,
} e_line_parser_mode_t;

typedef struct {
    char * str;
    int32_t size;
} st_word_t;

typedef struct {
    char alias[256];
    char real[256];
} st_alias_t;

typedef struct {
    char instance[256];
    char channel[256];
} st_names_t;

typedef struct {
    char * command;
    uint32_t word_count;
    char * secondary_word;
    int32_t secondary_index;
    r_result_t (*parser)(st_word_t * const p_word_list, const uint32_t word_count, const e_np_mode_t mode);
} st_commands_t;
/*! @endcond */

/**********************************************************************************************************************
 Function Prototypes
 *********************************************************************************************************************/

/* TODO document static functions */
/*! @cond STATIC */
static r_result_t import_from_file(char const * const fname, const e_np_mode_t mode);
static r_result_t parse_line(st_word_t * const p_word_list, const uint32_t word_count, const e_np_mode_t mode);
static r_result_t get_names(char const * const str, st_names_t * const p_names);
static r_result_t add_recorder(char const * const channel_name);
static char * const find_alias(char const * const str);
static inline bool word_matches(char const * const p_key, st_word_t const * const p_word);
static int32_t get_null_terminated_words(char * const line, const uint32_t max_line_size, st_word_t * const p_word_list, const uint32_t word_list_size);
static bool is_eol_character(const char c);
static bool is_valid_str_character(const char c);

static r_result_t parser_alias(   st_word_t * const p_word_list, const uint32_t word_count, const e_np_mode_t mode);
static r_result_t parser_instance(st_word_t * const p_word_list, const uint32_t word_count, const e_np_mode_t mode);
static r_result_t parser_link(    st_word_t * const p_word_list, const uint32_t word_count, const e_np_mode_t mode);
static r_result_t parser_trigger( st_word_t * const p_word_list, const uint32_t word_count, const e_np_mode_t mode);
static r_result_t parser_record(  st_word_t * const p_word_list, const uint32_t word_count, const e_np_mode_t mode);
static r_result_t parser_include( st_word_t * const p_word_list, const uint32_t word_count, const e_np_mode_t mode);
/*! @endcond */

/**********************************************************************************************************************
 Local Variables
 *********************************************************************************************************************/

/* TODO document static variables */
/*! @cond STATIC */

static char const * gs_file_list[64];
static uint32_t     gs_file_list_size;

static char const * gs_recorder_list[64];
static uint32_t     gs_recorder_list_size;

static st_alias_t gs_alias_list[64];
static uint32_t   gs_alias_list_size = 0;

static const st_commands_t COMMAND_LIST[] = {
    COMMAND(alias,    4, "is", 2),
    COMMAND(instance, 4, "is", 2),
    COMMAND(link,     4, "to", 2),
    COMMAND(trigger,  2, NULL, -1),
    COMMAND(record,   2, NULL, -1),
    COMMAND(include,  2, NULL, -1),
};

//TODO document static variables
/*! @endcond */

//TODO document static functions
/*! @cond STATIC */

/******************************************************************************
 *                                                                            *
 *                                  Parsers                                   *
 *                                                                            *
 ******************************************************************************/

static r_result_t parser_alias(st_word_t * const p_word_list, const uint32_t word_count, const e_np_mode_t mode)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NP_MODE_BUILD_ALIAS_MAP == mode)
    {
        if (gs_alias_list_size < (sizeof(gs_alias_list)/sizeof(gs_alias_list[0])))
        {
            st_alias_t * p_alias = &(gs_alias_list[gs_alias_list_size]);
            result = R_RESULT_SUCCESS;

            result |= R_COMMON_STRING_Copy(p_alias->alias, p_word_list[1].str, sizeof(p_alias->alias));
            result |= R_COMMON_STRING_Copy(p_alias->real,  p_word_list[3].str, sizeof(p_alias->real));
            gs_alias_list_size++;

        }
        else
        {
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

static r_result_t parser_instance(st_word_t * const p_word_list, const uint32_t word_count, const e_np_mode_t mode)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NP_MODE_INSTANTIATE_MODULE_INSTANCES == mode)
    {
        result = R_MODULE_AddInstanceByName(p_word_list[1].str, p_word_list[3].str);
    }

    return result;
}

static r_result_t parser_link(st_word_t * const p_word_list, const uint32_t word_count, const e_np_mode_t mode)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_names_t source = {0};
    st_names_t sink   = {0};

    if (NP_MODE_LINK_CHANNELS == mode)
    {
        result |= get_names(p_word_list[1].str, &source);
        result |= get_names(p_word_list[3].str, &sink);

        if (R_RESULT_SUCCESS == result)
        {
            result = R_BROKER_LinkByName(source.instance, source.channel, sink.instance, sink.channel);
        }
    }
    return result;
}

static r_result_t parser_trigger(st_word_t * const p_word_list, const uint32_t word_count, const e_np_mode_t mode)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NP_MODE_TRIGGER_MODULES == mode)
    {
        st_names_t sink;
        result = get_names(p_word_list[1].str, &sink);
        if (R_RESULT_SUCCESS == result)
        {
            result = R_BROKER_TriggerByName(sink.instance, sink.channel);
        }
    }

    return result;
}

static r_result_t parser_record(st_word_t * const p_word_list, const uint32_t word_count, const e_np_mode_t mode)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NP_MODE_LINK_CHANNELS == mode)
    {
        result = add_recorder(p_word_list[1].str);
    }

    return result;
}

static r_result_t parser_include(st_word_t * const p_word_list, const uint32_t word_count, const e_np_mode_t mode)
{
    (void)(mode); /* Don't need to know mode in this case, we always include */
    return import_from_file(p_word_list[1].str, mode);
}


/******************************************************************************
 *                                                                            *
 *                              Static Functions                              *
 *                                                                            *
 ******************************************************************************/

static bool is_valid_str_character(const char c)
{
    return (c >= '!') && (c <= '~') && (c != '#');
}

static bool is_eol_character(const char c)
{
    return (c == 0) || (c == '#') || (c == '\n');
}

static int32_t get_null_terminated_words(char * const line, const uint32_t max_line_size, st_word_t * const p_word_list, const uint32_t word_list_size)
{
    uint32_t word_list_index  = 0;
    uint32_t line_index       = 0;
    uint32_t string_start     = 0;
    bool     eol              = false;
    e_line_parser_mode_t mode = LINE_PARSER_MODE_SEARCHING_FOR_WORD;

    for(; (max_line_size > line_index) && (false == eol); line_index++)
    {
        if (is_valid_str_character(line[line_index]))
        {
            if (LINE_PARSER_MODE_SEARCHING_FOR_WORD == mode)
            {
                p_word_list[word_list_index].str = &(line[line_index]);
                string_start = line_index;
                mode = LINE_PARSER_MODE_INSIDE_WORD;
            }
        }
        else
        {
            eol = is_eol_character(line[line_index]);
            if (LINE_PARSER_MODE_INSIDE_WORD == mode)
            {
                /* Null-terminate word */
                line[line_index] = 0;

                /* Save size of string */
                /* Note: +1 is for null termination */
                p_word_list[word_list_index].size = 1 + (line_index - string_start);

                /* Increase found words */
                word_list_index++;

                /* Update state */
                mode = LINE_PARSER_MODE_SEARCHING_FOR_WORD;

                /* Exit if we ran out of room for words */
                if (word_list_size <= word_list_index)
                {
                    break;
                }
            }
        }
    }
    return word_list_index;
}

static inline bool word_matches(char const * const p_key, st_word_t const * const p_word)
{
    return (R_COMMON_STRING_Compare(p_key, p_word->str) == R_RESULT_SUCCESS);
}

static char * const find_alias(char const * const str)
{
    char * alias = NULL;
    uint32_t i = 0;

    for (i = 0; gs_alias_list_size > i; i++)
    {
        if (R_COMMON_STRING_Compare(str, &(gs_alias_list[i].alias[0])) == R_RESULT_SUCCESS)
        {
            alias = &(gs_alias_list[i].real[0]);
            break;
        }
    }
    return alias;
}

static r_result_t get_names(char const * const str, st_names_t * const p_names)
{
    r_result_t result = R_RESULT_FAILED;
    st_word_list_t words = {0};

    if (NULL != str)
    {
        (void)R_COMMON_STRING_Split(str, &words, '.');

        if (1 == words.num_words)
        {
            /* Search for alias.  Justification for recursion:
             *  1) This is only performed at initialization
             *  2) This is an optional module which isn't needed to build the project
             */
            result = get_names(find_alias(str), p_names);
        }
        else if (2 == words.num_words)
        {
            /* Store pointers */
            if ((R_COMMON_STRING_Copy(p_names->instance, words.word_list[0], sizeof(p_names->instance)) == R_RESULT_SUCCESS) &&
                (R_COMMON_STRING_Copy(p_names->channel, words.word_list[1], sizeof(p_names->channel))   == R_RESULT_SUCCESS))
            {
                result = R_RESULT_SUCCESS;
            }
            else
            {
                R_PRINT_ERROR("Error copying name");
            }
        }
        else
        {
            R_PRINT_ERROR("Error splitting name");
        }
    }
    return result;
}

static r_result_t add_recorder(char const * const channel_name)
{
    r_result_t result       = R_RESULT_SUCCESS;
    st_names_t source       = {0};
    char source_names[256]  = {0};
    char recorder_name[256] = {0};

    result = R_COMMON_STRING_Copy(source_names, channel_name, sizeof(source_names));

    if (R_RESULT_SUCCESS == result)
    {
        result = get_names(source_names, &source);
    }

    if (R_RESULT_SUCCESS == result)
    {
        int written = snprintf(recorder_name, sizeof(recorder_name), "%s/%s", source.instance, source.channel);
        if (3 >= written)
        {
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* Technically we should do this in NP_MODE_INSTANTIATE_MODULE_INSTANCES
         * but it is safe to do it in the link case in this instance
         */
        result = R_MODULE_AddInstanceByName(recorder_name, "record");
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = R_BROKER_LinkByName(source.instance, source.channel, recorder_name, "data");
    }

    return result;
}

static r_result_t parse_line(st_word_t * const p_word_list, const uint32_t word_count, const e_np_mode_t mode)
{
    r_result_t result = R_RESULT_FAILED;
    uint32_t i = 0;

    if ((1 > word_count) || (NULL == p_word_list))
    {
        /* Too few words, it's an empty line */
        result = R_RESULT_SUCCESS;
    }
    else
    {
        for (i = 0; i < (sizeof(COMMAND_LIST) / sizeof(COMMAND_LIST[0])); i++)
        {
            st_commands_t const * const p_cmd = &(COMMAND_LIST[i]);
            if (word_matches(p_cmd->command, &(p_word_list[0])))
            {
                if (word_count != p_cmd->word_count)
                {
                    R_PRINT_ERROR("Command '%s' expects %d words, got %d", p_cmd->command, p_cmd->word_count, word_count);
                    result = R_RESULT_FAILED;
                }
                else if ( (NULL != p_cmd->secondary_word)
                     &&   (0 <= p_cmd->secondary_index)
                     &&   (!word_matches(p_cmd->secondary_word, &(p_word_list[p_cmd->secondary_index]))))
                {
                    R_PRINT_ERROR("Command '%s' expects word %d to be '%s', got '%s'", p_cmd->command, p_cmd->secondary_index, p_cmd->secondary_word, p_word_list[word_count].str);
                    result = R_RESULT_FAILED;
                }
                else
                {
                    result = (p_cmd->parser)(p_word_list, word_count, mode);
                }
                break;
            }
        }
    }

    return result;
}

static r_result_t import_from_file(char const * const fname, const e_np_mode_t mode)
{
    r_result_t result = R_RESULT_SUCCESS;
    FILE * fp = fopen(fname, "r");
    char line[1024] = {0};
    st_word_t words[8] = {0};
    int32_t line_count = 1;

    if (NULL == fp)
    {
        result = R_RESULT_FAILED;
        R_PRINT_ERROR("Couldn't open file '%s'", fname);
    }

    /* Parse line by line */
    while ((R_RESULT_SUCCESS == result) && fgets(line, sizeof(line) - 1, fp))
    {
        int32_t word_count = get_null_terminated_words(line, sizeof(line), words, sizeof(words)/sizeof(words[0]));
        result = parse_line(words, word_count, mode);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Failed at %s:%d", fname, line_count);
        }

        line_count++;
    }

    if (NULL != fp)
    {
        fclose(fp);
    }

    return result;
}

// TODO document static functions
/*! @endcond */

/******************************************************************************
 *                                                                            *
 *                                 Public API                                 *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_NETWORK_PARSER_Init
 * @brief           Initialize the network parser
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_NETWORK_PARSER_Init(void)
{
    r_result_t result = R_RESULT_SUCCESS;

    gs_file_list_size     = 0;
    gs_recorder_list_size = 0;
    gs_alias_list_size    = 0;

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_NETWORK_PARSER_AddFile
 * @brief           Add a file to the list of files to parse
 * @param [in]      fname - String name of the file to add
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_NETWORK_PARSER_AddFile(char const * const fname)
{
    r_result_t result = R_RESULT_FAILED;

    if (((sizeof(gs_file_list)) / (sizeof(gs_file_list[0]))) > gs_file_list_size)
    {
        gs_file_list[gs_file_list_size] = fname;
        gs_file_list_size++;

        R_PRINT_NOTICE("Added network file %s", fname);
        result = R_RESULT_SUCCESS;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_NETWORK_PARSER_RecordChannel
 * @brief           Add a channel to the list of channels to record
 * @param [in]      channel_name - String name channel to record
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_NETWORK_PARSER_RecordChannel(char const * const channel_name)
{
    r_result_t result = R_RESULT_FAILED;

    if (((sizeof(gs_recorder_list)) / (sizeof(gs_recorder_list[0]))) > gs_recorder_list_size)
    {
        gs_recorder_list[gs_recorder_list_size] = channel_name;
        gs_recorder_list_size++;
        result = R_RESULT_SUCCESS;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_NETWORK_PARSER_Start
 * @brief           Parse the files previously added through R_NETWORK_PARSER_AddFile( ). This will parse the
 *                  graph file and interact with the data broker and module framework to set the object routing
 *                  accordingly.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_NETWORK_PARSER_Start(void)
{
    r_result_t result = R_RESULT_SUCCESS;
    e_np_mode_t mode = NP_MODE_BUILD_ALIAS_MAP;
    uint32_t i = 0;

    /* Init everything */
    for (mode = NP_MODE_BUILD_ALIAS_MAP; (R_RESULT_SUCCESS == result) && (NP_MODE_SETUP_COMPLETE > mode); mode++)
    {
        for(i=0; (R_RESULT_SUCCESS == result) && (gs_file_list_size > i); i++)
        {
            result = import_from_file(gs_file_list[i], mode);
        }
    }

    /* Add any extra recorders */
    for (i=0; (R_RESULT_SUCCESS == result) && (gs_recorder_list_size > i); i++)
    {
        result = add_recorder(gs_recorder_list[i]);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Couldn't add a recorder for channel '%s'", gs_recorder_list[i]);
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_NETWORK_PARSER_Trigger
 * @brief           Parse the files previously added through R_NETWORK_PARSER_AddFile( ). This will parse the
 *                  graph file trigger and modules that need to be kicked off explicitly from teh graph file.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_NETWORK_PARSER_Trigger(void)
{
    r_result_t result = R_RESULT_SUCCESS;
    uint32_t i = 0;

    for (i = 0; (R_RESULT_SUCCESS == result) && (gs_file_list_size > i); i++)
    {
        result = import_from_file(gs_file_list[i], NP_MODE_TRIGGER_MODULES);
    }

    return result;
}
