/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_rvx.h
 * @brief         Header file for common API for abstraction of RVX functions.
 * @defgroup      PQS_Common_Rvx PQS Common RVX
 * @section       PQS_Common_Rvx_Summary Module Summary
 *                Abstraction for RVX interactions.
 * @ingroup       PQS_Common
 * @{
 *********************************************************************************************************************/
/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/
#ifndef R_COMMON_RVX_H_
#define R_COMMON_RVX_H_

#include "bcl.h"
#include "rvxt_bcl.h"

#include "r_cl_patch.h"
#include "r_cl_patch_cnn.h"
#include "r_cl_patch_cve.h"
#include "r_cl_patch_dma.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Memory information for RVX command lists */
typedef struct
{
    void *    virt; /*!< Virtual address of command list */
    uintptr_t phys; /*!< Physical address of command list */
    size_t    size; /*!< Size of command list */
} st_rvx_mem_t;

/***********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 **********************************************************************************************************************/

r_result_t R_COMMON_RVX_Init(st_rvx_mem_t * p_bcl, st_rvx_mem_t * p_cnn_cl, st_rvx_mem_t * p_dma_cl,
        const char * blc_name, const char * cnn_bin_name, const char * dma_bin_name);
r_result_t R_COMMON_RVX_Execute(st_rvx_mem_t * p_bcl, st_rvx_mem_t * p_cnn_cl, st_rvx_mem_t * p_dma_cl);
r_result_t R_COMMON_RVX_Deinit(st_rvx_mem_t * p_bcl, st_rvx_mem_t * p_cnn_cl, st_rvx_mem_t * p_dma_cl);

#endif  /* R_COMMON_RVX_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Common>
 *********************************************************************************************************************/
