/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_hw.c
 * @brief         Source file for common API for abstraction of hardware interactions.
 *********************************************************************************************************************/
/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include <pthread.h> /* For mutexes and signals */

#include "r_common.h"
#include "generic_api_memory.h"
#include "r_atmlib_types.h"
#include "r_imr_api.h"
#include "drv.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Enumeration of all of the active core in IMP-X5+ System */
typedef enum
{
    R_COREMAP_IDX_CNN0 = 0,
    R_COREMAP_IDX_DMA0,
    R_COREMAP_IDX_IMP0,
    R_COREMAP_IDX_IMP1,
    R_COREMAP_IDX_IMP2,
    R_COREMAP_IDX_IMP3,
    R_COREMAP_IDX_IMP4,
    R_COREMAP_IDX_CVE0,
    R_COREMAP_IDX_CVE1,
    R_COREMAP_IDX_CVE2,
    R_COREMAP_IDX_CVE3,
    R_COREMAP_IDX_CVE4,
    R_COREMAP_IDX_PSC0,
    R_COREMAP_IDX_MAX,
} e_coremap_idx_t;

/*! Typedef for keeping track if a IMP core is active */
typedef struct
{
    volatile bool   variable; /*!< True if the core is active, false otherwise */
    pthread_mutex_t lock;     /*!< Mutex for variable */
    pthread_cond_t  signal;   /*!< Signal to inform waiting thread that variable has changed */
} st_core_active_t;

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

/*! @cond NO_DOXYGEN */
gd_Result gf_init(void); /* TODO We shouldn't have to do this but it is not declared in the rcar_environment */ /*lint !e2701 */
/*! @endcond */

static RCvIMPDRVCTL      s_rcvdrvctl;                          /*!< IMP driver control struct */
static st_core_active_t  s_core_active[R_COREMAP_IDX_MAX];     /*!< Active CVe core array */
static RCVIMPDRV_CBFUNC  s_core_callback[R_COREMAP_IDX_MAX];   /*!< Array of callback functions for CVe cores */
static RCvIMPDRVCOREINFO s_coremap[RCVDRV_COREMAP_MAXID];      /*!< Core map for IMP initialization/deinitialization.
                                                                    Must use RCVDRV_COREMAP_MAXID and not R_COREMAP_IDX_MAX here */
/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   v3h_sample_fix
 * @brief           This function checks if hardware version is V3H 1.0 and applies a patch.
 *********************************************************************************************************************/
static void v3h_sample_fix(void)
{
#if 0
    /*
     * TODO: In the current CNN Toolchain v1.10.3 on 1.1 silicon we need to set the bus priority to overcome the CNN
     * overwriting parts of the display image. Setting these bus priorities solves the issue, but should not be
     * needed in later version of the CNN Toolchain.
     */

    /* Check if hardware version*/
    uint32_t rev = drvCommon_ReadReg32(0xfff00044U);

    /* Check if hardware is V3H sample version 1.0 */
    if ((rev & 0xFFFFU) == 0x5600U)
#endif
    {
        /* Apply fix */
        drvCommon_WriteReg32(0xffa00000U + 0x0510U, 0xFFFFFFFFU);
        drvCommon_WriteReg32(0xffa00000U + 0x0514U, 0xFFFFFFFFU);
    }
}

/******************************************************************************************************************//**
 * Function Name:   get_core_index
 * @brief           Populates the core map index for a given core information
 * @param [in]      p_core_info - Pointer to the core info struct to get the core map index for
 * @param [out]     p_core_index - Pointer to the core index to fill in
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
static r_result_t get_core_index(RCvIMPDRVCOREINFO * p_core_info, uint32_t * p_core_index)
{
    r_result_t result = R_RESULT_SUCCESS;
    uint32_t core_index = 0;

    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        if (NULL == p_core_info)
        {
            R_PRINT_ERROR("Illegal NULL input");
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == p_core_index)
        {
            R_PRINT_ERROR("Illegal NULL input");
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
    }

    /* Find the core map index that matches the provided core info */
    if (R_RESULT_SUCCESS == result)
    {
        result = R_RESULT_FAILED;

        for (core_index = 0; core_index < (uint32_t)R_COREMAP_IDX_MAX; core_index++)
        {
            /* Check if core number and core type of the core map match the provided core info */
            if ((s_coremap[core_index].CoreNum  == p_core_info->CoreNum) && \
                (s_coremap[core_index].CoreType == p_core_info->CoreType))
            {
                result = R_RESULT_SUCCESS;
                break;
            }
        }

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Error finding the core map index for CoreNum = %u, CoreType = %u",
                    p_core_info->CoreNum, p_core_info->CoreType);
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        *p_core_index = core_index;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   get_core_info
 * @brief           Populates the core information structure for a given core map index
 * @param [in]      core_index - Core index to get info for
 * @param [out]     p_core_info - Pointer to the core info struct to fill in
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
static r_result_t get_core_info(uint32_t core_index, RCvIMPDRVCOREINFO * p_core_info)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        if ((uint32_t)R_COREMAP_IDX_MAX <= core_index)
        {
            R_PRINT_ERROR("Core index out of bounds");
            result = R_RESULT_BAD_INPUT_ARGUMENTS;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == p_core_info)
        {
            R_PRINT_ERROR("Illegal NULL input");
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        p_core_info->CoreNum  = s_coremap[core_index].CoreNum;
        p_core_info->CoreType = s_coremap[core_index].CoreType;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   callback_core
 * @param [in]      ercd - Error code
 * @param [in]      code - Error code
 * @param [in]      core_index - IMP-X5+ core index
 * @brief           IMP-X5+ callback handling for all IMPX. Both ercd and code provide info on program execution
 *********************************************************************************************************************/
static void callback_core(RCVDRV_CB_RET ercd, int32_t code, uint32_t core_index)
{
    (void)code; //Not used
    RCvIMPDRVCOREINFO core_info = {0};

    if (RCVDRV_CB_RET_BEFORE_EXEC != ercd)
    {
        /* Check for negative return */
        if (RCVDRV_CB_RET_OK != ercd)
        {
            if (get_core_info(core_index, &core_info) == R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Core type %d, Core number %d, returned status %d",
                        core_info.CoreType, core_info.CoreNum, ercd);
            }
            else
            {
                R_PRINT_ERROR("Error getting core info for core index %d", core_index);
            }
        }

        /* Signal waiting thread */
        (void)pthread_mutex_lock(&(s_core_active[core_index].lock));
        s_core_active[core_index].variable = false;
        (void)pthread_cond_signal(&(s_core_active[core_index].signal));
        (void)pthread_mutex_unlock(&(s_core_active[core_index].lock));
    }
}

/******************************************************************************************************************//**
 * Function Name:   callback_core0
 * @param [in]      ercd - Error code
 * @param [in]      code - Error code
 * @brief           This is a callback function for an IMP-X5+ core. Both ercd and code provide info on program execution
 *********************************************************************************************************************/
static void callback_core0(RCVDRV_CB_RET ercd, int32_t code)
{
    callback_core(ercd, code, 0);
}

/******************************************************************************************************************//**
 * Function Name:   callback_core1
 * @param [in]      ercd - Error code
 * @param [in]      code - Error code
 * @brief           This is a callback function for an IMP-X5+ core. Both ercd and code provide info on program execution
 *********************************************************************************************************************/
static void callback_core1(RCVDRV_CB_RET ercd, int32_t code)
{
    callback_core(ercd, code, 1);
}

/******************************************************************************************************************//**
 * Function Name:   callback_core2
 * @param [in]      ercd - Error code
 * @param [in]      code - Error code
 * @brief           This is a callback function for an IMP-X5+ core. Both ercd and code provide info on program execution
 *********************************************************************************************************************/
static void callback_core2(RCVDRV_CB_RET ercd, int32_t code)
{
    callback_core(ercd, code, 2);
}

/******************************************************************************************************************//**
 * Function Name:   callback_core3
 * @param [in]      ercd - Error code
 * @param [in]      code - Error code
 * @brief           This is a callback function for an IMP-X5+ core. Both ercd and code provide info on program execution
 *********************************************************************************************************************/
static void callback_core3(RCVDRV_CB_RET ercd, int32_t code)
{
    callback_core(ercd, code, 3);
}

/******************************************************************************************************************//**
 * Function Name:   callback_core4
 * @param [in]      ercd - Error code
 * @param [in]      code - Error code
 * @brief           This is a callback function for an IMP-X5+ core. Both ercd and code provide info on program execution
 *********************************************************************************************************************/
static void callback_core4(RCVDRV_CB_RET ercd, int32_t code)
{
    callback_core(ercd, code, 4);
}

/******************************************************************************************************************//**
 * Function Name:   callback_core5
 * @param [in]      ercd - Error code
 * @param [in]      code - Error code
 * @brief           This is a callback function for an IMP-X5+ core. Both ercd and code provide info on program execution
 *********************************************************************************************************************/
static void callback_core5(RCVDRV_CB_RET ercd, int32_t code)
{
    callback_core(ercd, code, 5);
}

/******************************************************************************************************************//**
 * Function Name:   callback_core6
 * @param [in]      ercd - Error code
 * @param [in]      code - Error code
 * @brief           This is a callback function for an IMP-X5+ core. Both ercd and code provide info on program execution
 *********************************************************************************************************************/
static void callback_core6(RCVDRV_CB_RET ercd, int32_t code)
{
    callback_core(ercd, code, 6);
}

/******************************************************************************************************************//**
 * Function Name:   callback_core7
 * @param [in]      ercd - Error code
 * @param [in]      code - Error code
 * @brief           This is a callback function for an IMP-X5+ core. Both ercd and code provide info on program execution
 *********************************************************************************************************************/
static void callback_core7(RCVDRV_CB_RET ercd, int32_t code)
{
    callback_core(ercd, code, 7);
}

/******************************************************************************************************************//**
 * Function Name:   callback_core8
 * @param [in]      ercd - Error code
 * @param [in]      code - Error code
 * @brief           This is a callback function for an IMP-X5+ core. Both ercd and code provide info on program execution
 *********************************************************************************************************************/
static void callback_core8(RCVDRV_CB_RET ercd, int32_t code)
{
    callback_core(ercd, code, 8);
}

/******************************************************************************************************************//**
 * Function Name:   callback_core9
 * @param [in]      ercd - Error code
 * @param [in]      code - Error code
 * @brief           This is a callback function for an IMP-X5+ core. Both ercd and code provide info on program execution
 *********************************************************************************************************************/
static void callback_core9(RCVDRV_CB_RET ercd, int32_t code)
{
    callback_core(ercd, code, 9);
}

/******************************************************************************************************************//**
 * Function Name:   callback_core10
 * @param [in]      ercd - Error code
 * @param [in]      code - Error code
 * @brief           This is a callback function for an IMP-X5+ core. Both ercd and code provide info on program execution
 *********************************************************************************************************************/
static void callback_core10(RCVDRV_CB_RET ercd, int32_t code)
{
    callback_core(ercd, code, 10);
}

/******************************************************************************************************************//**
 * Function Name:   callback_core11
 * @param [in]      ercd - Error code
 * @param [in]      code - Error code
 * @brief           This is a callback function for an IMP-X5+ core. Both ercd and code provide info on program execution
 *********************************************************************************************************************/
static void callback_core11(RCVDRV_CB_RET ercd, int32_t code)
{
    callback_core(ercd, code, 11);
}

/******************************************************************************************************************//**
 * Function Name:   callback_core12
 * @param [in]      ercd - Error code
 * @param [in]      code - Error code
 * @brief           This is a callback function for an IMP-X5+ core. Both ercd and code provide info on program execution
 *********************************************************************************************************************/
static void callback_core12(RCVDRV_CB_RET ercd, int32_t code)
{
    callback_core(ercd, code, 12);
}

/******************************************************************************************************************//**
 * Function Name:   init_coremap
 * @brief           Initializes the core map array per the defined e_coremap_idx_t enum
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
static r_result_t init_coremap(void)
{
    r_result_t result = R_RESULT_SUCCESS;

    for (uint32_t core_index = 0; core_index < (uint32_t)R_COREMAP_IDX_MAX; core_index++)
    {
        /* Get the core number */
        switch ((e_coremap_idx_t)core_index) /*lint !e9030 */
        {
            case R_COREMAP_IDX_CNN0:
            case R_COREMAP_IDX_DMA0:
            case R_COREMAP_IDX_IMP0:
            case R_COREMAP_IDX_CVE0:
            case R_COREMAP_IDX_PSC0:
            {
                s_coremap[core_index].CoreNum = 0;
                break;
            }

            case R_COREMAP_IDX_IMP1:
            case R_COREMAP_IDX_CVE1:
            {
                s_coremap[core_index].CoreNum = 1;
                break;
            }

            case R_COREMAP_IDX_IMP2:
            case R_COREMAP_IDX_CVE2:
            {
                s_coremap[core_index].CoreNum = 2;
                break;
            }

            case R_COREMAP_IDX_IMP3:
            case R_COREMAP_IDX_CVE3:
            {
                s_coremap[core_index].CoreNum = 3;
                break;
            }

            case R_COREMAP_IDX_IMP4:
            case R_COREMAP_IDX_CVE4:
            {
                s_coremap[core_index].CoreNum = 4;
                break;
            }

            case R_COREMAP_IDX_MAX:
            default:
            {
                R_PRINT_ERROR("Core not supported");
                result = R_RESULT_FAILED;
                break;
            }
        }

        /* Get the core type */
        switch ((e_coremap_idx_t)core_index) /*lint !e9030 */
        {
            case R_COREMAP_IDX_CNN0:
            {
                s_coremap[core_index].CoreType = RCVDRV_CORE_TYPE_CNN;
                break;
            }

            case R_COREMAP_IDX_DMA0:
            {
                s_coremap[core_index].CoreType = RCVDRV_CORE_TYPE_DMAC;
                break;
            }

            case R_COREMAP_IDX_PSC0:
            {
                s_coremap[core_index].CoreType = RCVDRV_CORE_TYPE_PSCEXE;
                break;
            }

            case R_COREMAP_IDX_IMP0:
            case R_COREMAP_IDX_IMP1:
            case R_COREMAP_IDX_IMP2:
            case R_COREMAP_IDX_IMP3:
            case R_COREMAP_IDX_IMP4:
            {
                s_coremap[core_index].CoreType = RCVDRV_CORE_TYPE_IMP;
                break;
            }

            case R_COREMAP_IDX_CVE0:
            case R_COREMAP_IDX_CVE1:
            case R_COREMAP_IDX_CVE2:
            case R_COREMAP_IDX_CVE3:
            case R_COREMAP_IDX_CVE4:
            {
                s_coremap[core_index].CoreType = RCVDRV_CORE_TYPE_OCV;
                break;
            }

            case R_COREMAP_IDX_MAX:
            default:
            {
                R_PRINT_ERROR("Core not supported");
                result = R_RESULT_FAILED;
                break;
            }
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   imp_init
 * @brief           Initializes the whole of IMP-X5+. Core map must be set before this function is called.
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
static r_result_t imp_init(void)
{
    r_result_t      result      = R_RESULT_SUCCESS;
    RCvDrvErrorCode impdrv_ret  = RCV_EC_OK_DRV;

    /* Init core map array */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        if (init_coremap() != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error initializing core map");
            result = R_RESULT_FAILED;
        }
    }

    /* Initialize IMP Device */
    if (R_RESULT_SUCCESS == result)
    {
        impdrv_ret = rcv_impdrv_IMP_InitDevice(&s_rcvdrvctl);
        if (RCV_EC_OK_DRV != impdrv_ret)
        {
            R_PRINT_ERROR("[ERROR] rcv_impdrv_IMP_InitDevice  : code %d", impdrv_ret);
            result = R_RESULT_FAILED;
        }
    }

    /* Initialize IMP Driver */
    if (R_RESULT_SUCCESS == result)
    {
        impdrv_ret = rcv_impdrv_Init(&s_rcvdrvctl);
        if (RCV_EC_OK_DRV != impdrv_ret)
        {
            R_PRINT_ERROR("[ERROR] rcv_impdrv_Init  : code %d", impdrv_ret);
            result = R_RESULT_FAILED;
        }
    }

    /* Start IMP cores */
    if (R_RESULT_SUCCESS == result)
    {
        for (uint32_t core_index = 0; core_index < (uint32_t)R_COREMAP_IDX_MAX; core_index++)
        {
            impdrv_ret = rcv_impdrv_Start(&s_rcvdrvctl, &s_coremap[core_index], s_coremap);
            if (RCV_EC_OK_DRV != impdrv_ret)
            {
                 R_PRINT_ERROR("[ERROR] rcv_impdrv_Start : code %d, core type = %d core num = %d",
                         impdrv_ret, s_coremap[core_index].CoreType, s_coremap[core_index].CoreNum);
                 result = R_RESULT_FAILED;
            }
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   imp_deinit
 * @brief           Deinitializes the whole of IMP-X5+. IMP must be initialized before this function is called
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
static r_result_t imp_deinit(void)
{
    r_result_t      result      = R_RESULT_SUCCESS;
    RCvDrvErrorCode impdrv_ret  = RCV_EC_OK_DRV;

    /* Quit IMP cores */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        for (uint32_t core_index = 0; core_index < (uint32_t)R_COREMAP_IDX_MAX; core_index++)
        {
            impdrv_ret = rcv_impdrv_Stop(&s_rcvdrvctl, &s_coremap[core_index]);
            if (RCV_EC_OK_DRV != impdrv_ret)
            {
                R_PRINT_ERROR("[ERROR] rcv_impdrv_Stop : code %d, core type = %d core num = %d",
                        impdrv_ret, s_coremap[core_index].CoreType, s_coremap[core_index].CoreNum);
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Denitialize IMP Driver */
    if (R_RESULT_SUCCESS == result)
    {
        impdrv_ret = rcv_impdrv_Quit(&s_rcvdrvctl);
        if (RCV_EC_OK_DRV != impdrv_ret)
        {
            R_PRINT_ERROR("[ERROR] rcv_impdrv_Quit  : code %d", impdrv_ret);
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   imr_init
 * @brief           Initializes IMR
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
static r_result_t imr_init(void)
{
    r_result_t result = R_RESULT_SUCCESS;
    int32_t channel_index = 0;
    int32_t imr_ret = 0;

    /* Initialize IMR Device */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        imr_ret = R_IMR_Init();
        if (E_OK != imr_ret)
        {
            R_PRINT_ERROR("[ERROR] R_IMR_Init : code %d", imr_ret);
            result = R_RESULT_FAILED;
        }
    }

    /* Start IMR Driver */
    if (R_RESULT_SUCCESS == result)
    {
        for (channel_index = 0; channel_index < (int32_t)R_MAX_IMR_CHANNELS; channel_index++)
        {
            imr_ret = R_IMR_Start(channel_index);
            if (E_OK != imr_ret)
            {
                R_PRINT_ERROR("[ERROR] R_IMR_Start for channel %d : code %d", channel_index, imr_ret);
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Open IMR Driver */
    if (R_RESULT_SUCCESS == result)
    {
        for (channel_index = 0; channel_index < (int32_t)R_MAX_IMR_CHANNELS; channel_index++)
        {
            imr_ret = R_IMR_Open(channel_index);
            if (E_OK != imr_ret)
            {
                R_PRINT_ERROR("[ERROR] R_IMR_Open for channel %d : code %d", channel_index, imr_ret);
                result = R_RESULT_FAILED;
            }
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   imr_deinit
 * @brief           Denitializes IMR
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
static r_result_t imr_deinit(void)
{
    r_result_t result = R_RESULT_SUCCESS;
    int32_t channel_index = 0;
    int32_t imr_ret = 0;


    /* Close IMR Driver */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        for (channel_index = 0; channel_index < (int32_t)R_MAX_IMR_CHANNELS; channel_index++)
        {
            imr_ret = R_IMR_Close(channel_index);
            if (E_OK != imr_ret)
            {
                R_PRINT_ERROR("[ERROR] R_IMR_Close for channel %d : code %d", channel_index, imr_ret);
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Stop IMR Driver */
    if (R_RESULT_SUCCESS == result)
    {
        for (channel_index = 0; channel_index < (int32_t)R_MAX_IMR_CHANNELS; channel_index++)
        {
            imr_ret = R_IMR_Stop(channel_index);
            if (E_OK != imr_ret)
            {
                R_PRINT_ERROR("[ERROR] R_IMR_Stop for channel %d : code %d", channel_index, imr_ret);
                result = R_RESULT_FAILED;
            }
        }
    }


    /* Deinitialize IMR Device */
    if (R_RESULT_SUCCESS == result)
    {
        imr_ret = R_IMR_Deinit();
        if (E_OK != imr_ret)
        {
            R_PRINT_ERROR("[ERROR] R_IMR_Deinit : code %d", imr_ret);
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_HW_Init
 * @brief           Initializes Hardware drivers
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
r_result_t R_COMMON_HW_Init(void)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Init generic rtos environment */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        if (gf_init() != gd_OK)
        {
            R_PRINT_ERROR("gf_init could not be initialized");
            result = R_RESULT_FAILED;
        }

        if (gf_MemInit() == false)
        {
            R_PRINT_ERROR("gf_Memory API could not be initialized");
            result = R_RESULT_FAILED;
        }
    }

    /* Init Hardware */
    if (R_RESULT_SUCCESS == result)
    {
        result |= imp_init();
        result |= imr_init();
    }

    /* Init IMP-X5+ active cores array */
    if (R_RESULT_SUCCESS == result)
    {
        for (uint32_t core_index = 0; core_index < (uint32_t)R_COREMAP_IDX_MAX; core_index++)
        {
            s_core_active[core_index].variable = false;

            if (0 != pthread_mutex_init(&(s_core_active[core_index].lock), NULL))
            {
                result = R_RESULT_FAILED;
            }

            if (0 != pthread_cond_init(&(s_core_active[core_index].signal), NULL))
            {
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Init IMP-X5+ callback array */
    if (R_RESULT_SUCCESS == result)
    {
        s_core_callback[0]  = callback_core0;
        s_core_callback[1]  = callback_core1;
        s_core_callback[2]  = callback_core2;
        s_core_callback[3]  = callback_core3;
        s_core_callback[4]  = callback_core4;
        s_core_callback[5]  = callback_core5;
        s_core_callback[6]  = callback_core6;
        s_core_callback[7]  = callback_core7;
        s_core_callback[8]  = callback_core8;
        s_core_callback[9]  = callback_core9;
        s_core_callback[10] = callback_core10;
        s_core_callback[11] = callback_core11;
        s_core_callback[12] = callback_core12;
    }

    v3h_sample_fix();

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_HW_Deinit
 * @brief           Deinitializes Hardware drivers
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
r_result_t R_COMMON_HW_Deinit(void)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Deinit Hardware */
    if (R_RESULT_SUCCESS == result) /*lint !e774 !e948 */
    {
        result |= imp_deinit();
        result |= imr_deinit();
    }

    /* Deinit IMP-X5+ active core array */
    if (R_RESULT_SUCCESS == result)
    {
        for (uint32_t core_index = 0; core_index < (uint32_t)R_COREMAP_IDX_MAX; core_index++)
        {
            if (0 != pthread_mutex_destroy(&(s_core_active[core_index].lock)))
            {
                R_PRINT_ERROR("Mutex deinit has failed");
                result = R_RESULT_FAILED;
            }

            if (0 != pthread_cond_destroy(&(s_core_active[core_index].signal)))
            {
                R_PRINT_ERROR("Signal deinit has failed");
                result = R_RESULT_FAILED;
            }

            s_core_active[core_index].variable = false;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_HW_ExecuteIMP
 * @brief           Executes an IMP-X5+ command list on the given core
 * @param [in]      command_list_addr - Address of the command list
 * @param [in]      p_core_info - Pointer to core info
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
r_result_t R_COMMON_HW_ExecuteIMP(uint32_t command_list_addr, RCvIMPDRVCOREINFO * p_core_info)
{
    r_result_t result = R_RESULT_SUCCESS;
    RCvDrvErrorCode impdrv_ret = RCV_EC_OK_DRV;
    uint32_t core_index = 0;

    if (NULL == p_core_info)
    {
        R_PRINT_ERROR("Error NULL input");
        result = R_RESULT_ILLEGAL_NULL_POINTER;
    }

    if (0u == command_list_addr)
    {
        R_PRINT_ERROR("Error illegal input");
        result = R_RESULT_BAD_INPUT_ARGUMENTS;
    }

    /* Get the core map index for the given core info */
    if (R_RESULT_SUCCESS == result)
    {
        if (get_core_index(p_core_info, &core_index) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Cannot get core map index");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        (void)pthread_mutex_lock(&(s_core_active[core_index].lock));

        /* Check if core is already active */
        if (true == s_core_active[core_index].variable)
        {
            R_PRINT_ERROR("Error core is already active. Core type = %u, Core number = %u",
                    p_core_info->CoreType, p_core_info->CoreNum);
            result = R_RESULT_FAILED;
        }

        /* Execute command list */
        if (R_RESULT_SUCCESS == result)
        {
            s_core_active[core_index].variable = true;

            impdrv_ret = rcv_impdrv_ExecuteCB(&s_rcvdrvctl,
                    command_list_addr,
                    p_core_info,
                    s_core_callback[core_index]);

            if (RCV_EC_OK_DRV != impdrv_ret)
            {
                R_PRINT_ERROR("Error executing command list. Core type = %u, Core num = %u, error = %d",
                        p_core_info->CoreType, p_core_info->CoreNum, impdrv_ret);
                result = R_RESULT_FAILED;
            }
        }

        (void)pthread_mutex_unlock(&(s_core_active[core_index].lock));
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_HW_WaitIMP
 * @brief           Waits until a given core has returned from processing. This function blocks!
 * @param [in]      p_core_info - Pointer to core info
 * @retval          R_RESULT_SUCCESS- upon success
 *********************************************************************************************************************/
r_result_t R_COMMON_HW_WaitIMP(RCvIMPDRVCOREINFO * p_core_info)
{
    r_result_t result = R_RESULT_SUCCESS;
    uint32_t core_index = 0;

    if (NULL == p_core_info)
    {
        R_PRINT_ERROR("Error NULL input");
        result = R_RESULT_ILLEGAL_NULL_POINTER;
    }

    /* Get the core map index for the given core info */
    if (R_RESULT_SUCCESS == result)
    {
        if (get_core_index(p_core_info, &core_index) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Cannot get core map index");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        (void)pthread_mutex_lock(&(s_core_active[core_index].lock));

        R_PRINT_DEBUG("Waiting for Core Type = %u, Core Num = %u", p_core_info->CoreType, p_core_info->CoreNum);

        /* Wait while the core is active */
        while (true == s_core_active[core_index].variable)
        {
            (void)pthread_cond_wait(&(s_core_active[core_index].signal), &(s_core_active[core_index].lock));
        }

        R_PRINT_DEBUG("Core Type = %u, Core Num = %u has returned", p_core_info->CoreType, p_core_info->CoreNum);

        (void)pthread_mutex_unlock(&(s_core_active[core_index].lock));
    }

    return result;
}


