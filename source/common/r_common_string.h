/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_string.h
 * @brief         Header file for common API to wrap all <string.h> functions to assure safe string usage.
 * @defgroup      PQS_Common_String PQS Common String
 * @section       PQS_Common_String_Summary Module Summary
 *                Wrapper for all <string.h> functions to assure safe string usage.
 * @ingroup       PQS_Common
 * @{
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

#ifndef R_COMMON_STRING_H_
    #define R_COMMON_STRING_H_

/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

#define R_STRING_MAX_NUM_WORDS      (16u) /*!< Max number of words that can be stored in st_word_list_t */
#define R_STRING_MAX_WORD_LENGTH    (64u) /*!< Max length of each word that can be stored in st_word_list_t */

/**********************************************************************************************************************
 Typedef definitions
 *********************************************************************************************************************/

/*! Struct for storing a list of words */
typedef struct {
    char     word_list[R_STRING_MAX_NUM_WORDS][R_STRING_MAX_WORD_LENGTH]; /*!< Char array for storing words */
    uint32_t num_words;                                                   /*!< Number of words in word list */
} st_word_list_t;

/**********************************************************************************************************************
 Exported global variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 *********************************************************************************************************************/

extern size_t     R_COMMON_STRING_Length(const char * const string);
extern r_result_t R_COMMON_STRING_Combine(char * const dest, const char * const source, size_t dest_length);
extern r_result_t R_COMMON_STRING_Copy(char * const dest, const char * const source, size_t dest_length);
extern r_result_t R_COMMON_STRING_Compare(const char * const string1, const char * const string2);
extern r_result_t R_COMMON_STRING_Split(const char * const string, st_word_list_t * p_list, char separator);
extern r_result_t R_COMMON_STRING_WordInList(const char * const word, const st_word_list_t * list);
extern r_result_t R_COMMON_STRING_AppendIfNeeded(char * const dest, const char newChar, size_t dest_length);

#endif /* R_COMMON_STRING_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Common>
 *********************************************************************************************************************/
