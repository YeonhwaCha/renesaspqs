/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_os_linux.c
 * @brief         Source file for common API to abstract operating system for Linux
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/*lint --e{9001} --e{9130} */

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include <sys/stat.h> /* For stat and mkdir */
#include <termios.h>  /* For termios */

#include "r_common.h"
#include "r_common_os.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define OS_PRV_FOLDER_OFFSET         (2u)   /*!< Folder offset */
#define OS_PRV_MAX_FOLDER_LENGTH     (128u) /*!< Maximum folder length in characters  */

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

static struct termios s_term_attribs; /*!< Terminal attributes */

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_OS_SetTerminalRaw
 * @brief           Allows to process keystrokes as they happen. Removes the need for user to press enter
 *********************************************************************************************************************/
void R_COMMON_OS_SetTerminalRaw(void)
{
    struct termios new_term_attribs;

    (void)tcgetattr(fileno(stdin), &s_term_attribs);
    R_COMMON_Memcpy(&new_term_attribs, &s_term_attribs, sizeof(s_term_attribs));
    new_term_attribs.c_lflag &= (~ICANON); /*lint !e9029 */
    new_term_attribs.c_cc[VTIME] = 0;
    new_term_attribs.c_cc[VMIN] = 0;
    (void)tcsetattr(fileno(stdin), TCSANOW, &new_term_attribs);
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_OS_ClearTerminalRaw
 * @brief           Reverts the changes of R_COMMON_OS_SetTerminalRaw() and makes user have to press enter before
 *                  keystroke can be processed. R_COMMON_OS_SetTerminalRaw() must be called first!
 *********************************************************************************************************************/
void R_COMMON_OS_ClearTerminalRaw(void)
{
    (void)tcsetattr(fileno(stdin), TCSANOW, &s_term_attribs);
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_OS_MakeDir
 * @brief           Creates a directory
 * @param [in]      full_path - String containing the name of the directory to be created
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_OS_MakeDir(char const * const full_path)
{
    r_result_t result = R_RESULT_SUCCESS;
    char folder_name[OS_PRV_FOLDER_OFFSET + OS_PRV_MAX_FOLDER_LENGTH] = {'\0'};

    /* The purpose for the folder offset is so that we can ensure the "don't
     * make folder" rules will always have an i that can handle the subtraction. */
    uint32_t i = OS_PRV_FOLDER_OFFSET + 1u;

    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        result = R_COMMON_STRING_Copy(&(folder_name[OS_PRV_FOLDER_OFFSET]), full_path, OS_PRV_MAX_FOLDER_LENGTH);
    }

    /* Adjust starting point based on special chars like ~ and . */
    if (R_RESULT_SUCCESS == result)
    {
        if ((('.' == folder_name[OS_PRV_FOLDER_OFFSET]) || ('~' == folder_name[OS_PRV_FOLDER_OFFSET])) && ('/' == folder_name[OS_PRV_FOLDER_OFFSET + 1u]))
        {
            i = OS_PRV_FOLDER_OFFSET + 2u;
        }
    }

    while (R_RESULT_SUCCESS == result)
    {
        if (('/' == folder_name[i]) || ('\0' == folder_name[i]))
        {
            if (('.' == folder_name[i - 1u]) && ('.' == folder_name[i - 2u]) && ('/' == folder_name[i - 3u]))
            {
                /* Don't make a folder if it is /../
                 * Intentionally empty */
            }
            else if (('.' == folder_name[i - 1u]) && ('/' == folder_name[i - 2u]))
            {
                /* Don't make a folder if it is /./
                 * Intentionally empty */
            }
            else if (('/' == folder_name[i - 1u]))
            {
                /* Don't make a folder if it is //
                 * Intentionally empty */
            }
            else
            {
                struct stat sb = {0};
                char back = folder_name[i];
                folder_name[i] = '\0';

                if ((0 != stat(&(folder_name[OS_PRV_FOLDER_OFFSET]), &sb)) || (!S_ISDIR(sb.st_mode))) /*lint !e9029 */
                {
                    /* Folder does not exist */
                    R_PRINT_INFO("Creating folder %s", &(folder_name[OS_PRV_FOLDER_OFFSET]));

                    if (0 != mkdir(&(folder_name[OS_PRV_FOLDER_OFFSET]), S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH))
                    {
                        result = R_RESULT_FAILED;
                    }
                }

                folder_name[i] = back;

                if ('\0' == back)
                {
                    break;
                }
            }
        }

        i++;
    }

    return result;
}
