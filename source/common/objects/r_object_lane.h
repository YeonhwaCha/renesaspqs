/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_lane.h
 * @brief         Header file for object definition and related API of lane type.
 * @defgroup      PQS_Object_Lane PQS Object Lane
 * @section       PQS_Object_Lane_Summary Module Summary
 *                Object definition for lane type and API for accessing data in lane objects.
 * @ingroup       PQS_Objects
 * @{
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

#ifndef R_OBJECT_LANE_H_
#define R_OBJECT_LANE_H_

/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

#define R_OBJECT_LANE_MAX_LANE_COUNT (32u) /*!< How many lanes each object can hold */
#define R_OBJECT_LANE_MAX_OBJECTS    (8u)  /*!< Number of lane objects to allocate on startup */

/**********************************************************************************************************************
 Typedef definitions
 *********************************************************************************************************************/

/*! Enumeration of lane colors */
typedef enum
{
    R_LANE_COLOR_UNKNOWN = 0, /*!< Unknown lane color */
    R_LANE_COLOR_WHITE,       /*!< White lane */
    R_LANE_COLOR_YELLOW,      /*!< Yellow lane*/
    R_LANE_COLOR_MAX          /*!< Max lane color... Keep this last*/
} e_lane_color_t;

/*! Enumeration of lane types */
typedef enum
{
    R_LANE_TYPE_UNKNOWN = 0, /*!< Unknown lane type */
    R_LANE_TYPE_DASHED,      /*!< Dashed lane  */
    R_LANE_TYPE_SOLID,       /*!< Solid lane */
    R_LANE_TYPE_BOT_DOT,     /*!< Bot dots */
    R_LANE_TYPE_MAX          /*!< Max lane type... Keep this last. */
} e_lane_type_t;

/*! Definition of lane structure */
typedef struct st_lane_s {
    float          a;            /*!< In the form x = a*y^2 + b*y + c */
    float          b;            /*!< In the form x = a*y^2 + b*y + c */
    float          c;            /*!< In the form x = a*y^2 + b*y + c */
    uint32_t       ymax;         /*!< Maximum Y in the frame where lane paint is visible */
    uint32_t       ymin;         /*!< Minimum Y in the frame where lane paint is visible */
    uint32_t       len_pix;      /*!< Euclidean length of the lane between ymin and ymax in pixels */
    float          slope_deg;    /*!< Slope of the lane as a straight line as birds-eye view
                                      perspective in degrees; vertical -> 0 degrees;
                                      clockwise -> positive; anti-clockwise ->negative */
    e_lane_color_t color;        /*!< Lane color */
    e_lane_type_t  type;         /*!< Lane type */
    bool           extrapolated; /*!< Flag to identify if the lane was extrapolated or detected */
    float          confidence;   /*!< Percentage of confidence; 0 to 1 corresponding to 0 to 100% */
    float          offset;       /*!< Offset between vehicle center and bottom of the lane in meters;
                                       - offset if lane is to the left of vehicle center;
                                       + offset if lane is to the right of vehicle center */
} st_lane_t;

/**********************************************************************************************************************
 Exported global variables
 *********************************************************************************************************************/

extern const st_object_config_t g_object_config_lane;

/**********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 *********************************************************************************************************************/

extern r_result_t R_OBJECT_AddLane(st_object_t * const p_object, const st_lane_t * const p_item);
extern st_lane_t * R_OBJECT_GetLane(st_object_t * const p_object, uint32_t item_index);
extern uint32_t R_OBJECT_GetLaneCount(st_object_t * const p_object);

#endif /* R_OBJECT_LANE_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Objects>
 *********************************************************************************************************************/
