/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object.c
 * @brief         Source file for PQS Object handling
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include <pthread.h> /* For mutexes */

#include "generic_api_memory.h"
#include "r_common.h"

/*! Define object core, otherwise r_object_private.h will throw an error */
#define _R_OBJECT_CORE_
#include "r_object_private.h"
#include "r_rof.h"

/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

/*! Function macro to copy object element */
#define OBJECT_PRV_COPY_FROM_OBJECT(pd, ps, f)        R_COMMON_Memcpy(&(pd->f), &(ps->f), sizeof(pd->f))

/*! Function macro to copy object strings */
#define OBJECT_PRV_COPY_STRING_FROM_OBJECT(pd, ps, f) R_COMMON_Memcpy((pd->f),   (ps->f), sizeof(pd->f)/sizeof(pd->f[0]))

/**********************************************************************************************************************
 Local Typedef definitions
 *********************************************************************************************************************/

/*! Enum for different copy operations for copying from one object to another */
typedef enum {
    COPY_OBJECT_HEADER = 0,          /*!< Copy object header (sensor ID + timestamp) */
    COPY_OBJECT_HEADER_SENSOR_ID,    /*!< Copy object sensor ID */
    COPY_OBJECT_HEADER_TIME_STAMP,   /*!< Copy object timestamp  */
    COPY_OBJECT_HEADER_DESCRIPTION,  /*!< Copy object description  */
    COPY_OBJECT_PROPERTIES,          /*!< Copy object all object properties */
} e_copy_object_t;

/*! Object storage structure, Each object type will have one of these structs describing it. */
typedef struct {
    st_object_t *   p_object_list; /*!< Pointer to object storage */
    e_object_type_t type;          /*!< Type of object */
    uint32_t        object_count;  /*!< How many objects exist? */
    uint32_t        object_size;   /*!< How many bytes does each object use? */
    uint32_t        max_items;     /*!< How many items can be stored? */
    uint32_t        item_size;     /*!< How many bytes is each item? */
    pthread_mutex_t lock;          /*!< Mutex to protect storage list */
} st_object_storage_t;

/**********************************************************************************************************************
 Local Variables
 *********************************************************************************************************************/

static st_object_storage_t s_object_storage[R_OBJECT_TYPE_MAX]; /*!< List based on type for storing objects */
static uint32_t            s_next_unique_id;                    /*!< Next available unique id */

static const char * sp_object_type_name[]  = E_OBJECT_TYPE_AS_STRING;  /*!< Object types as a string array */
static const char * sp_object_state_name[] = E_OBJECT_STATE_AS_STRING; /*!< Object states as a string array */

/******************************************************************************/
/* Initialization code                                                        */
/******************************************************************************/

/* TODO document static functions */
/*! @cond STATIC */

static void clear_object_storage(st_object_storage_t * const p_object_storage)
{
    p_object_storage->p_object_list = NULL;
    p_object_storage->type          = R_OBJECT_TYPE_INVALID;
    p_object_storage->object_count  = 0;
    p_object_storage->object_size   = 0;
    p_object_storage->max_items     = 0;
    p_object_storage->item_size     = 0;
}

static r_result_t verify_config_list_item(st_object_config_t const * const p_config)
{
    r_result_t result = R_RESULT_FAILED;

    if ((R_OBJECT_TYPE_INVALID >= p_config->type) || (R_OBJECT_TYPE_MAX <= p_config->type))
    {
        result = R_RESULT_FAILED;
    }
    else if (0u == p_config->object_count)
    {
        result = R_RESULT_FAILED;
    }
    else
    {
        result = R_RESULT_SUCCESS;
    }

    return result;
}

static r_result_t init_object(st_object_storage_t * const p_object_storage, const uint32_t index)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_object_t * const p_object = &(p_object_storage->p_object_list[index]);

    /* Zero out everything, as zero is usually default */
    R_COMMON_Memset(p_object, 0, sizeof(*p_object));

    /* Init mutex */
    if (0 != pthread_mutex_init(&(p_object->control.lock), NULL))
    {
        R_PRINT_ERROR("Couldn't init mutex");
        result = R_RESULT_FAILED;
    }
    else
    {
        /* Init data */
        p_object->data.type  = p_object_storage->type;
        p_object->data.size  = p_object_storage->object_size;
        p_object->data.imp   = 0;
        p_object->data.p_cpu = NULL;

        if (p_object_storage->object_size > 0u)
        {
            p_object->data.p_cpu = gf_Memalign(256, p_object_storage->object_size);

            if (NULL == p_object->data.p_cpu)
            {
                R_PRINT_ERROR("Malloc failed in object init");
                result = R_RESULT_FAILED;
            }
            else
            {
                /* Set imp address */
                p_object->data.imp = (uint32_t)gf_GetPhysAddr(p_object->data.p_cpu);
            }
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        p_object->control.state = R_OBJECT_STATE_FREE;
    }
    else
    {
        p_object->control.state = R_OBJECT_STATE_ERROR;
    }

    return result;
}

static void free_object_mem(st_object_t * const p_object)
{
    if (NULL != p_object->data.p_cpu)
    {
        gf_Free(p_object->data.p_cpu);
        p_object->data.p_cpu = NULL;
        p_object->data.imp   = 0;
    }
}

static r_result_t deinit_object(st_object_t * const p_object)
{
    r_result_t result = R_RESULT_SUCCESS;

    switch(p_object->control.state)
    {
        case R_OBJECT_STATE_UNINITIALIZED:
        {
            /* Nothing to do */
            break;
        }

        case R_OBJECT_STATE_ERROR:
        {
            p_object->control.state = R_OBJECT_STATE_UNINITIALIZED;
            free_object_mem(p_object);
            break;
        }

        case R_OBJECT_STATE_FREE:
        {
            (void)pthread_mutex_lock(&(p_object->control.lock));
            p_object->control.state = R_OBJECT_STATE_UNINITIALIZED;
            free_object_mem(p_object);
            (void)pthread_mutex_unlock(&(p_object->control.lock));
            (void)pthread_mutex_destroy(&(p_object->control.lock));
            break;
        }

        case R_OBJECT_STATE_PROCESSING:
        case R_OBJECT_STATE_LOCKED:
        default:
        {
            /* Error cases */
            result = R_RESULT_FAILED;
            break;
        }
    }

    return result;
}

static r_result_t init_object_storage(st_object_storage_t * const p_object_storage, st_object_config_t const * const p_object_config)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NULL != p_object_storage->p_object_list)
    {
        /* Object type already exists */
        R_PRINT_ERROR("Object id %s already existed", sp_object_type_name[p_object_config->type]);
        result = R_RESULT_FAILED;
    }

    /* Get storage for array of objects */
    if (R_RESULT_SUCCESS == result)
    {
        R_PRINT_INFO("Requesting 0x%04X bytes for objects", p_object_config->object_count * sizeof(st_object_t));
        p_object_storage->p_object_list = malloc(p_object_config->object_count * sizeof(st_object_t)); /*lint !e586 */

        if (NULL == p_object_storage->p_object_list)
        {
            R_PRINT_ERROR("Malloc failed for object id %s", sp_object_type_name[p_object_config->type]);
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* Init basic stuffs */
        p_object_storage->type         = p_object_config->type;
        p_object_storage->object_count = p_object_config->object_count;
        p_object_storage->object_size  = p_object_config->max_items * p_object_config->item_size;
        p_object_storage->max_items    = p_object_config->max_items;
        p_object_storage->item_size    = p_object_config->item_size;

        /* Init lock */
        if (pthread_mutex_init(&p_object_storage->lock, NULL) != 0)
        {
            R_PRINT_ERROR("Cannot create mutex");
            result = R_RESULT_FAILED;
        }

        /* Init and reserve memory for each object */
        for (uint32_t object_index = 0; object_index < p_object_config->object_count; object_index++)
        {
            result = init_object(p_object_storage, object_index);

            if (R_RESULT_SUCCESS != result)
            {
                R_PRINT_ERROR("Error initializing object type %s.  Needed %d objects, only got %d",
                        sp_object_type_name[p_object_storage->type], p_object_storage->object_count, object_index);

                p_object_storage->object_count = object_index;
                break;
            }
        }

        R_PRINT_INFO("Object type %s:\n\tObject Count: %d\n\tObject Size:  0x%04X\n\tItem Count:   %d\n\tItem Size:    0x%04X",
                sp_object_type_name[p_object_storage->type], p_object_storage->object_count, p_object_storage->object_size, p_object_storage->max_items, p_object_storage->item_size);
    }

    return result;
}

static r_result_t deinit_object_storage(st_object_storage_t * const p_object_storage)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NULL != p_object_storage->p_object_list)
    {
        for (uint32_t object_index = 0; object_index < p_object_storage->object_count; object_index++)
        {
            result |= deinit_object(&(p_object_storage->p_object_list[object_index]));
        }

        free(p_object_storage->p_object_list); /*lint !e586 */
    }

    if (pthread_mutex_destroy(&p_object_storage->lock) != 0)
    {
        R_PRINT_ERROR("Error destroying mutex");
        result = R_RESULT_FAILED;
    }

    clear_object_storage(p_object_storage);

    return result;
}

/******************************************************************************/
/*                                                             * Runtime code */
/******************************************************************************/
static r_result_t object_lock(st_object_t * const p_object)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NULL == p_object)
    {
        result = R_RESULT_ILLEGAL_NULL_POINTER;
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (0 != pthread_mutex_lock(&(p_object->control.lock)))
        {
            R_PRINT_ERROR("Failed to lock mutex!");
            result = R_RESULT_FAILED;
        }
    }

    return result; /*lint !e456 !e454 */
}

static void object_unlock(st_object_t * const p_object)
{
    r_result_t  result = R_RESULT_SUCCESS;

    if (NULL == p_object)
    {
        result = R_RESULT_ILLEGAL_NULL_POINTER;
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (0 != pthread_mutex_unlock(&(p_object->control.lock))) /*lint !e455 */
        {
            R_PRINT_ERROR("Failed to unlock mutex!");
            result = R_RESULT_FAILED;
        }
    }

    (void)result; //Not currently used
}

static r_result_t is_in_state(st_object_t const * const p_object, const e_object_state_t expected_state)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (expected_state != p_object->control.state)
    {
        R_PRINT_ERROR("Expected state %s, Actual state %s", sp_object_state_name[expected_state], sp_object_state_name[p_object->control.state]);
        result = R_RESULT_FAILED;
    }

    return result;
}

static r_result_t is_type(st_object_t const * const p_object, const e_object_type_t expected_type)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (expected_type != p_object->data.type)
    {
        R_PRINT_ERROR("Expected object type %s, Actual object type %s", sp_object_type_name[expected_type], sp_object_type_name[p_object->data.type]);
        result = R_RESULT_FAILED;
    }

    return result;
}

static r_result_t can_read_info(st_object_t * const p_object)
{
    r_result_t result;

    switch (p_object->control.state)
    {
        case R_OBJECT_STATE_PROCESSING:
        case R_OBJECT_STATE_LOCKED:
        {
            result = R_RESULT_SUCCESS;
            break;
        }

        case R_OBJECT_STATE_FREE:
        case R_OBJECT_STATE_UNINITIALIZED:
        case R_OBJECT_STATE_ERROR:
        default:
        {
            R_PRINT_ERROR("Expected state R_OBJECT_STATE_PROCESSING or R_OBJECT_STATE_LOCKED, Actual state %s", sp_object_state_name[p_object->control.state]);
            result = R_RESULT_FAILED;
            break;
        }
    }

    return result;
}

static r_result_t can_write_info(st_object_t * const p_object)
{
    r_result_t result;

    if (R_OBJECT_STATE_PROCESSING == p_object->control.state)
    {
        result = R_RESULT_SUCCESS;
    }
    else
    {
        R_PRINT_ERROR("Expected state R_OBJECT_STATE_PROCESSING, Actual state %s", sp_object_state_name[p_object->control.state]);
        result = R_RESULT_FAILED;
    }

    return result;
}

static r_result_t get_item_address(st_object_t * const p_object, const uint32_t item_index, uint32_t * const p_imp_data, void ** const pp_cpu_data)
{
    r_result_t result = R_RESULT_SUCCESS;
    uint32_t offset = 0;

    if (p_object->meta.properties.item.count >= p_object->meta.properties.item.max_items)
    {
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (0u < p_object->meta.properties.item.item_size)
        {
            offset = p_object->meta.properties.item.item_size * item_index;
        }

        if (p_object->data.size < (offset + p_object->meta.properties.item.item_size))
        {
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (NULL != p_imp_data)
        {
            if (0u == p_object->data.imp)
            {
                result = R_RESULT_FAILED;
            }
            else
            {
                *p_imp_data = p_object->data.imp + offset;
            }
        }

        if (NULL != pp_cpu_data)
        {
            if (NULL == p_object->data.p_cpu)
            {
                result = R_RESULT_FAILED;
            }
            else
            {
                *pp_cpu_data = &(((uint8_t *)(p_object->data.p_cpu))[offset]);
            }
        }
    }
    return result;
}

static uint32_t get_used_data_size(st_object_t const * const p_object)
{
    uint32_t size = 0;

    if (R_OBJECT_TYPE_IMAGE == p_object->data.type)
    {
        size = p_object->meta.properties.image.width
                * p_object->meta.properties.image.height
                * p_object->meta.properties.image.depth
                * p_object->meta.properties.image.byte_pp;
    }
    else if (R_OBJECT_TYPE_FEATURE_MAP == p_object->data.type)
    {
        size = p_object->meta.properties.map.height
                * p_object->meta.properties.map.width
                * p_object->meta.properties.map.data_size
                * p_object->meta.properties.map.num_features;
    }
    else
    {
        size = p_object->meta.properties.item.count
                * p_object->meta.properties.item.item_size;
    }

    return size;
}

/* Assumes data is locked and is safe to delete */
static r_result_t reset_object(st_object_t * const p_object)
{
    r_result_t result = R_RESULT_SUCCESS;

    p_object->control.state     = R_OBJECT_STATE_FREE;
    p_object->control.ref_count = 0;

    R_COMMON_Memset(&(p_object->meta.header),     0, sizeof(p_object->meta.header));
    R_COMMON_Memset(&(p_object->meta.properties), 0, sizeof(p_object->meta.properties));

    return result;
}

static st_object_storage_t * get_valid_object_storage(const e_object_type_t type)
{
    st_object_storage_t * p_object_storage = NULL;

    if ((R_OBJECT_TYPE_INVALID < type)
    &&  (R_OBJECT_TYPE_MAX > type)
    &&  (type == s_object_storage[type].type)
    &&  (0u < s_object_storage[type].object_count))
    {
        p_object_storage = &(s_object_storage[type]);
    }

    return p_object_storage;
}

static void reset_properties_image(st_object_t * const p_object)
{
    p_object->meta.properties.image.format  = 0;
    p_object->meta.properties.image.width   = 0;
    p_object->meta.properties.image.height  = 0;
    p_object->meta.properties.image.depth   = 0;
    p_object->meta.properties.image.byte_pp = 0;
}

static void reset_properties_item(st_object_t * const p_object)
{
    st_object_storage_t * p_object_storage = get_valid_object_storage(p_object->data.type);

    p_object->meta.properties.item.count     = 0;
    p_object->meta.properties.item.max_items = 0;
    p_object->meta.properties.item.item_size = 0;

    if (NULL != p_object_storage)
    {
        p_object->meta.properties.item.max_items  = p_object_storage->max_items;
        p_object->meta.properties.item.item_size  = p_object_storage->item_size;
    }
}

static void reset_properties_map(st_object_t * const p_object)
{
    p_object->meta.properties.map.format       = 0;
    p_object->meta.properties.map.width        = 0;
    p_object->meta.properties.map.height       = 0;
    p_object->meta.properties.map.num_features = 0;
    p_object->meta.properties.map.data_size    = 0;
}

static r_result_t reserve_object(st_object_t * const p_object)
{
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (R_OBJECT_STATE_FREE == p_object->control.state)
        {
            p_object->control.state     = R_OBJECT_STATE_PROCESSING;
            p_object->control.ref_count = 1;
            p_object->meta.id           = R_OBJECT_ID_INVALID; /* Give it an invalid id */

            /* Init header */
            R_COMMON_Memset(&(p_object->meta.header), 0, sizeof(p_object->meta.header));

            /* Init properties */
            if (R_OBJECT_TYPE_IMAGE == p_object->data.type)
            {
                reset_properties_image(p_object);
            }
            else if (R_OBJECT_TYPE_FEATURE_MAP == p_object->data.type)
            {
                reset_properties_map(p_object);
            }
            else
            {
                reset_properties_item(p_object);
            }
        }
        else
        {
            result = R_RESULT_FAILED;
        }
    }

    object_unlock(p_object);

    return result;
}

static r_result_t copy_info_from_object(st_object_t * const p_object_dest, st_object_t * const p_object_source, const e_copy_object_t copy)
{
    r_result_t result = object_lock(p_object_dest);
    result |= object_lock(p_object_source);

    if (R_RESULT_SUCCESS == result)
    {
        result |= can_write_info(p_object_dest);
        result |= can_read_info(p_object_source);
    }

    if (R_RESULT_SUCCESS == result)
    {
        switch (copy)
        {
            case COPY_OBJECT_HEADER:
            {
                OBJECT_PRV_COPY_FROM_OBJECT(p_object_dest, p_object_source, meta.header);
                break;
            }

            case COPY_OBJECT_HEADER_SENSOR_ID:
            {
                OBJECT_PRV_COPY_FROM_OBJECT(p_object_dest, p_object_source, meta.header.sensor_id);
                break;
            }

            case COPY_OBJECT_HEADER_TIME_STAMP:
            {
                OBJECT_PRV_COPY_FROM_OBJECT(p_object_dest, p_object_source, meta.header.time_stamp);
                break;
            }

            case COPY_OBJECT_HEADER_DESCRIPTION:
            {
                OBJECT_PRV_COPY_STRING_FROM_OBJECT(p_object_dest, p_object_source, meta.header.description);
                break;
            }

            case COPY_OBJECT_PROPERTIES:
            {
                /* If copying properties, objects must have the same type */
                if (p_object_dest->data.type == p_object_source->data.type)
                {
                    OBJECT_PRV_COPY_FROM_OBJECT(p_object_dest, p_object_source, meta.properties);
                }
                else
                {
                    result = R_RESULT_FAILED;
                }
                break;
            }

            default:
            {
                result = R_RESULT_FAILED;
                break;
            }
        }
    }

    object_unlock(p_object_source);
    object_unlock(p_object_dest);

    return result;
}

/* TODO document static functions */
/*! @endcond */

/******************************************************************************/
/*                                                           Public interface */
/******************************************************************************/


/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_PrintDiagInfo
 * @brief           Print out object properties for a givem object
 * @param [in]      p_object - Pointer to object
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_PrintDiagInfo(st_object_t * const p_object)
{
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        R_PRINT_INFO(
            "Object info:\n"
            "    Type:      %d\n"
            "    State:     %s\n"
            "    Ref Count: %s\n",
            sp_object_type_name[p_object->data.type],
            sp_object_state_name[p_object->control.state],
            p_object->control.ref_count);

        if (R_OBJECT_TYPE_IMAGE == p_object->data.type)
        {
            R_PRINT_INFO("Image Info: \n"
                    "    Format:    %d\n"
                    "    Width:     %d\n"
                    "    Height:    %d\n"
                    "    Depth:     %d\n"
                    "    BPP:       %d\n",
                    p_object->meta.properties.image.format,
                    p_object->meta.properties.image.width,
                    p_object->meta.properties.image.height,
                    p_object->meta.properties.image.depth,
                    p_object->meta.properties.image.byte_pp);
        }
        else if (R_OBJECT_TYPE_FEATURE_MAP == p_object->data.type)
        {
            R_PRINT_INFO("Map Info:        \n"
                    "    Width:          %d\n"
                    "    Height:         %d\n"
                    "    Data Size:      %d\n",
                    "    # of channels:  %d\n",
                    p_object->meta.properties.map.width,
                    p_object->meta.properties.map.height,
                    p_object->meta.properties.map.data_size,
                    p_object->meta.properties.map.num_features);
        }
        else
        {
            R_PRINT_INFO("Item Info:  \n"
                    "    Count:     %d\n"
                    "    Max Items: %d\n"
                    "    Item Size: %d\n",
                    p_object->meta.properties.item.count,
                    p_object->meta.properties.item.max_items,
                    p_object->meta.properties.item.item_size);
        }
    }

    object_unlock(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_ReadROF
 * @brief           Read object properties and data from a ROF file and store in the supplied object
 * @param [in,out]  p_object - Pointer to object
 * @param [in]      filename - Name of the file to read from
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_ReadROF(st_object_t * const p_object, const char * const filename)
{
    r_result_t result = object_lock(p_object);
    st_rof_t rof; //Can leave this uninitialized

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }
    }

    /* Open file */
    if (R_RESULT_SUCCESS == result)
    {
        result = R_ROF_ReadOpen(&rof, filename);
    }

    /* Read data in */
    if (R_RESULT_SUCCESS == result)
    {
        /* Read standard metadata */
        result |= R_ROF_ReadBlock(&rof, R_SECTION_META, (uint32_t)sizeof(st_object_meta_t), (uint8_t *)(&(p_object->meta))); /*lint !e928 !e9176  */

        /* Read data */
        result |= R_ROF_ReadBlock(&rof, R_SECTION_DATA, get_used_data_size(p_object), p_object->data.p_cpu);
    }

    /* Close */
    if (R_RESULT_SUCCESS == result)
    {
        result = R_ROF_Close(&rof);
    }

    object_unlock(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_WriteROF
 * @brief           Write object properties and data to a ROF file
 * @param [in]      p_object - Pointer to object
 * @param [in]      filename - Name of the file to write to
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_WriteROF(st_object_t * const p_object, const char * const filename)
{
    r_result_t result = object_lock(p_object);
    st_rof_t rof; //Can leave this uninitialized

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }
    }

    /* Open file */
    if (R_RESULT_SUCCESS == result)
    {
        result = R_ROF_WriteOpen(&rof, filename);
    }

    /* Read data in */
    if (R_RESULT_SUCCESS == result)
    {
        /* Write standard metadata */
        result |= R_ROF_WriteBlock(&rof, R_SECTION_META, (uint32_t)sizeof(st_object_meta_t), (uint8_t *)(&(p_object->meta))); /*lint !e928 !e9176 */

        /* Write object-specific metadata */
        result |= R_ROF_WriteBlock(&rof, R_SECTION_PRIV, 0, NULL);

        /* Write data */
        result |= R_ROF_WriteBlock(&rof, R_SECTION_DATA, get_used_data_size(p_object), p_object->data.p_cpu);
    }

    /* Close */
    if (R_RESULT_SUCCESS == result)
    {
        result = R_ROF_Close(&rof);
    }

    object_unlock(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_ReadRaw
 * @brief           Read object properties and data from a raw file and store in object
 * @param [in]      p_object - Pointer to object
 * @param [in]      filename - Name of the file to read from
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_ReadRaw(st_object_t * const p_object, const char * const filename)
{
    /*lint --e{586} --e{593} --e{480} --e{481} */

    r_result_t result  = object_lock(p_object);
    FILE * p_file      = NULL;

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }
    }

    /* Open plane file */
    if (R_RESULT_SUCCESS == result)
    {
        p_file = fopen(filename, "rb");

        if (NULL == p_file)
        {
            R_PRINT_ERROR("Error opening file %s", filename);
            result = R_RESULT_FAILED;
        }
    }

    /* Read file plane and close */
    if (R_RESULT_SUCCESS == result)
    {
        if (fread(p_object->data.p_cpu, get_used_data_size(p_object), 1, p_file) != 1u) /*lint !e668 */
        {
            R_PRINT_ERROR("Error reading file %s", filename);
            result = R_RESULT_FAILED;
        }

        if (0 != fclose(p_file)) /*lint !e668 */
        {
            R_PRINT_ERROR("Error closing file %s", filename);
            result = R_RESULT_FAILED;
        }
    }

    object_unlock(p_object);

    return result;
}


/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_WriteRaw
 * @brief           Write object properties and data to a raw file
 * @param [in]      p_object - Pointer to object
 * @param [in]      filename - Name of the file to write to
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_WriteRaw(st_object_t * const p_object, const char * const filename)
{
    /*lint --e{586} --e{593} --e{480} --e{481} */

    r_result_t result = object_lock(p_object);
    FILE * p_file      = NULL;

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }
    }

    /* Open plane file */
    if (R_RESULT_SUCCESS == result)
    {
        p_file = fopen(filename, "wb");

        if (NULL == p_file)
        {
            R_PRINT_ERROR("Error opening file %s", filename);
            result = R_RESULT_FAILED;
        }
    }

    /* Write image plane */
    if (R_RESULT_SUCCESS == result)
    {
        if (fwrite(p_object->data.p_cpu, get_used_data_size(p_object), 1, p_file) != 1u) /*lint !e668 */
        {
            R_PRINT_ERROR("Error writing file %s", filename);
            result = R_RESULT_FAILED;
        }

        if (0 != fclose(p_file)) /*lint !e668 */
        {
            R_PRINT_ERROR("Error closing file %s", filename);
            result = R_RESULT_FAILED;
        }
    }

    object_unlock(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_Init
 * @brief           Initializes all objects for use. Call this before any other R_OBJECT function
 * @param [in]      pp_config_list - List of all object types to initialize
 * @param [in]      config_list_length - Length of the list of object types
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_Init(const st_object_config_t * const * const pp_config_list, uint32_t config_list_length)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Init unique Id */
    s_next_unique_id = R_OBJECT_ID_INVALID + 1u;

    /* Reset to default state */
    for (uint32_t i = 0; i < (uint32_t)R_OBJECT_TYPE_MAX; i++)
    {
        clear_object_storage(&(s_object_storage[i]));
    }

    /* Init objects based on config list */
    for (uint32_t i = 0; config_list_length > i; i++)
    {
        if (verify_config_list_item(pp_config_list[i]) == R_RESULT_SUCCESS)
        {
            R_PRINT_INFO("Initalizing object id %s", sp_object_type_name[pp_config_list[i]->type]);
            result |= init_object_storage(&(s_object_storage[pp_config_list[i]->type]), pp_config_list[i]);
        }
        else
        {
            R_PRINT_ERROR("Invalid object found at pp_config_list[%d]", i);
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_Deinit
 * @brief           Deinitializes all objects. Call this function during shutdown
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_Deinit(void)
{
    r_result_t result = R_RESULT_SUCCESS;

    for (uint32_t object_type_index = 0; object_type_index < (uint32_t)R_OBJECT_TYPE_MAX; object_type_index++)
    {
        result |= deinit_object_storage(&s_object_storage[object_type_index]);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetFreeObject
 * @brief           PQS Framework will call this function to get free objects for modules to write output data to
 * @param [in]      type - Type of the object to get
 * @retval          NULL - if no object of that type is available. Pointer to the object otherwise
 *********************************************************************************************************************/
st_object_t * R_OBJECT_GetFreeObject(const e_object_type_t type)
{
    st_object_t * p_object = NULL;
    st_object_storage_t * p_object_storage = get_valid_object_storage(type);

    if (NULL == p_object_storage)
    {
        R_PRINT_ERROR("Could not find object storage for class type %s", sp_object_type_name[type]);
    }
    else
    {
        (void)pthread_mutex_lock(&p_object_storage->lock);

        for (uint32_t i = 0; p_object_storage->object_count > i; i++)
        {
            st_object_t * p_object_check = &(p_object_storage->p_object_list[i]);

            if (reserve_object(p_object_check) == R_RESULT_SUCCESS)
            {
                p_object = p_object_check;
                break;
            }
        }

        if (NULL != p_object)
        {
            p_object->meta.id = s_next_unique_id;
            s_next_unique_id++;

            if (R_OBJECT_ID_INVALID == s_next_unique_id)
            {
                s_next_unique_id++;
            }
        }
        else
        {
            R_PRINT_ERROR("No free objects available. Object state dump for type %s:", sp_object_type_name[type]);

            for (uint32_t i = 0; p_object_storage->object_count > i; i++)
            {
                R_PRINT("   %d: %s\n", i, sp_object_state_name[p_object_storage->p_object_list[i].control.state]);
            }
        }

        (void)pthread_mutex_unlock(&p_object_storage->lock);
    }



    return p_object;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetObjectId
 * @brief           Modules and PQS Framework can call this function to get a unique ID for an object. Every time an
 *                  object is reserved for processing it will get a new ID.
 * @param [in]      p_object - Pointer to the object
 * @param [out]     p_get_id - Pointer to the id to update
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_GetObjectId(st_object_t * const p_object, uint32_t * const p_get_id)
{
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == p_get_id)
        {
            R_PRINT_ERROR("Illegal NULL input");
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        *p_get_id = p_object->meta.id;
    }

    object_unlock(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetSensorId
 * @brief           Modules call this function to get a unique sensor ID corresponding to the sensor used to capture
 *                  the data stored in the object.
 * @param [in]      p_object - Pointer to the object
 * @param [out]     p_get_sensor_id - Pointer to the id to update
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_GetSensorId(st_object_t * const p_object, uint32_t * const p_get_sensor_id)
{
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == p_get_sensor_id)
        {
            R_PRINT_ERROR("Illegal NULL input");
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        *p_get_sensor_id = p_object->meta.header.sensor_id;
    }

    object_unlock(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_SetSensorId
 * @brief           Modules call this function to set a unique sensor ID corresponding to the sensor used to capture
 *                  the data stored in the object. This function can only be used on output objects.
 * @param [in,out]  p_object - Pointer to the object
 * @param [in]      set_sensor_id - Sensor ID to set
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_SetSensorId(st_object_t * const p_object, uint32_t set_sensor_id)
{
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (can_write_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        p_object->meta.header.sensor_id = set_sensor_id;
    }

    object_unlock(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_SetSensorIdFromObject
 * @brief           Modules call this function to copy a unique sensor ID from one object to another. Destination
 *                  must be a module output object.
 * @param [in,out]  p_object_dest - Pointer to the destination object
 * @param [in]      p_object_source - Pointer to the source object
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_SetSensorIdFromObject(st_object_t * const p_object_dest, st_object_t * const p_object_source)
{
    return copy_info_from_object(p_object_dest, p_object_source, COPY_OBJECT_HEADER_SENSOR_ID);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetTimeStamp
 * @brief           Modules call this function to get the timestamp when the data contained in the object was captured.
 *                  Note, this time reflects when the sensor captured the data, not the time when data processing
 *                  was performed on the object
 * @param [in]      p_object - Pointer to the object
 * @param [out]     p_get_time - Pointer to the timestamp to update
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_GetTimeStamp(st_object_t * const p_object, uint64_t * const p_get_time)
{
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == p_get_time)
        {
            R_PRINT_ERROR("Illegal NULL input");
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        *p_get_time = p_object->meta.header.time_stamp;
    }

    object_unlock(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_SetTimeStamp
 * @brief           Modules call this function to set the timestamp when the data contained in the object was captured.
 *                  Note, this time reflects when the sensor captured the data, not the time when data processing
 *                  was performed on the object. This function can only be used on output objects.
 * @param [in, out] p_object - Pointer to the object
 * @param [in]      set_time - ID to set
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_SetTimeStamp(st_object_t * const p_object, const uint64_t set_time)
{
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (can_write_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        p_object->meta.header.time_stamp = set_time;
    }

    object_unlock(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_SetTimeStampFromObject
 * @brief           Modules call this function to copy a timestamp from one object to another. Destination
 *                  must be a module output object.
 * @param [in,out]  p_object_dest - Pointer to the destination object
 * @param [in]      p_object_source - Pointer to the source object
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_SetTimeStampFromObject(st_object_t * const p_object_dest, st_object_t * const p_object_source)
{
    return copy_info_from_object(p_object_dest, p_object_source, COPY_OBJECT_HEADER_TIME_STAMP);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetDescription
 * @brief           Modules call this function to get the optional string description of an object.
 * @param [in]      p_object - Pointer to the object
 * @param [out]     p_get_description - Pointer to the description to update. Must be at least
 *                  R_OBJECT_MAX_DESCRIPTION_LENGTH long.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_GetDescription(st_object_t * const p_object, char * const p_get_description)
{
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == p_get_description)
        {
            R_PRINT_ERROR("Illegal NULL input");
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = R_COMMON_STRING_Copy(p_get_description, p_object->meta.header.description, R_OBJECT_MAX_DESCRIPTION_LENGTH);
    }

    object_unlock(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_SetDescription
 * @brief           Modules call this function to set the optional string description of an object. This function
 *                  can only be used on output objects.
 * @param [in, out] p_object - Pointer to the object
 * @param [in]      p_set_description - Description to set
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_SetDescription(st_object_t * const p_object, const char * const p_set_description)
{
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == p_set_description)
        {
            R_PRINT_ERROR("Illegal NULL input");
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (can_write_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = R_COMMON_STRING_Copy(p_object->meta.header.description, p_set_description, R_OBJECT_MAX_DESCRIPTION_LENGTH);
    }

    object_unlock(p_object);

    return result;
}


/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_CopyDescription
 * @brief           Modules call this function to copy a description from one object to another. Destination
 *                  must be a module output object.
 * @param [in,out]  p_object_dest - Pointer to the destination object
 * @param [in]      p_object_source - Pointer to the source object
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_CopyDescription(st_object_t * const p_object_dest, st_object_t * const p_object_source)
{
    return copy_info_from_object(p_object_dest, p_object_source, COPY_OBJECT_HEADER_DESCRIPTION);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_CopyHeader
 * @brief           Modules call this function to copy the entire object header from one object to another. Header
 *                  include both sensor ID and timstamp. Destination must be a module output object.
 * @param [in,out]  p_object_dest - Pointer to the destination object
 * @param [in]      p_object_source - Pointer to the source object
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_CopyHeader(st_object_t * const p_object_dest, st_object_t * const p_object_source)
{
    return copy_info_from_object(p_object_dest, p_object_source, COPY_OBJECT_HEADER);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_CopyProperties
 * @brief           Modules call this function to copy object type specific properties (e.g. image height and width)
 *                  from one object to another. Destination must be a module output object of the same type as source.
 * @param [in,out]  p_object_dest - Pointer to the destination object
 * @param [in]      p_object_source - Pointer to the source object
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_CopyProperties(st_object_t * const p_object_dest, st_object_t * const p_object_source)
{
    return copy_info_from_object(p_object_dest, p_object_source, COPY_OBJECT_PROPERTIES);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetObjectType
 * @brief           Modules and framework call this function to get the object type of a given object
 * @param [in]      p_object - Pointer to the destination object
 * @param [out]     p_get_object_type - Pointer to the object type to update
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_GetObjectType(st_object_t * const p_object, e_object_type_t * p_get_object_type)
{
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == p_get_object_type)
        {
            R_PRINT_ERROR("Illegal NULL input");
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        *p_get_object_type = p_object->data.type;
    }

    object_unlock(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_Lock
 * @brief           PQS Framework will call this function to lock an object after a module has finished processing.
 *                  Once locked, object header (timestamp, sensor) and properties (image height/width) cannot be updated.
 * @param [in,out]  p_object - Pointer to the object
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_Lock(st_object_t * const p_object)
{
    r_result_t  result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (is_in_state(p_object, R_OBJECT_STATE_PROCESSING) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error locking object");
            result = R_RESULT_FAILED;
        }
        else
        {
            p_object->control.state = R_OBJECT_STATE_LOCKED;
        }
    }

    object_unlock(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_CheckObjectType
 * @brief           Modules and PQS Framework will call this function to check if an object is the expected type
 *                  and if the object is in the correct state to read data from.
 * @param [in,out]  p_object - Pointer to the object
 * @param [in]      expected_type - type to compare object's type to
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_CheckObjectType(st_object_t * const p_object, const e_object_type_t expected_type)
{
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }

        if (is_type(p_object, expected_type) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not the correct type");
            result = R_RESULT_FAILED;
        }
    }

    object_unlock(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_IncrementRefCount
 * @brief           PQS Framework calls this to increase the reference counter to the given object by 1. Framework will
 *                  increment reference counter to all objects a module uses (both input and output) before kicking
 *                  off the module.
 * @param [in,out]  p_object - Pointer to the object
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_IncrementRefCount(st_object_t * const p_object)
{
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (is_in_state(p_object, R_OBJECT_STATE_LOCKED) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Couldn't increment ref count: object was not locked!");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        p_object->control.ref_count++;
    }

    object_unlock(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_DecrementRefCount
 * @brief           PQS Framework calls this to decrease the reference counter to the given object by 1. Framework will
 *                  decrement reference counter to all objects a module uses (both input and output) after a module
 *                  has completed processing. When reference count for an object reaches zero the object is freed.
 * @param [in,out]  p_object - Pointer to the object
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_DecrementRefCount(st_object_t * const p_object)
{
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (is_in_state(p_object, R_OBJECT_STATE_LOCKED) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Couldn't decrement ref count: object was not locked!");
            result = R_RESULT_FAILED;
        }

        if (R_RESULT_SUCCESS == result)
        {
            if (1u >= p_object->control.ref_count)
            {
                /* Delete object */
                R_PRINT_INFO("Object ID %u ref count deleted", p_object->meta.id);
                result = reset_object(p_object);
            }
            else
            {
                p_object->control.ref_count--;
            }
        }
    }

    object_unlock(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetRefCount
 * @brief           PQS Framework calls this to get the reference counter of a given object.
 * @param [in,out]  p_object - Pointer to the object
 * @param [out]     p_ref_count - Pointer to the reference count to update
 * @retval          number of references to that object
 *********************************************************************************************************************/
r_result_t R_OBJECT_GetRefCount(st_object_t * const p_object, uint32_t * p_ref_count)
{
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == p_ref_count)
        {
            R_PRINT_ERROR("Illegal NULL input");
            result = R_RESULT_ILLEGAL_NULL_POINTER;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        *p_ref_count = p_object->control.ref_count;
    }

    object_unlock(p_object);

    return result;
}

/******************************************************************************/
/*                                    Private interface for other object files */
/******************************************************************************/

/*! @cond INTERNAL */
r_result_t R_OBJECT_PRV_LockMutex(st_object_t * const p_object)
{
    return object_lock(p_object);
}

void R_OBJECT_PRV_UnlockMutex(st_object_t * const p_object)
{
    object_unlock(p_object);
}

r_result_t R_OBJECT_PRV_IsInState(st_object_t * const p_object, e_object_state_t expected_state)
{
    return is_in_state(p_object, expected_state);
}

r_result_t R_OBJECT_PRV_IsType(st_object_t * const p_object, e_object_type_t expected_type)
{
    return is_type(p_object, expected_type);
}

r_result_t R_OBJECT_PRV_CanReadInfo(st_object_t * const p_object)
{
    return can_read_info(p_object);
}

r_result_t R_OBJECT_PRV_CanWriteInfo(st_object_t * const p_object)
{
    return can_write_info(p_object);
}

uint32_t R_OBJECT_PRV_GetMaxItemCount(st_object_t * const p_object, e_object_type_t type)
{
    r_result_t result = object_lock(p_object);
    uint32_t count = 0;

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }

        if (is_type(p_object, type) != R_RESULT_SUCCESS )
        {
            R_PRINT_ERROR("Object is not a valid type");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        count = p_object->meta.properties.item.max_items;
    }

    object_unlock(p_object);

    return count;
}

uint32_t R_OBJECT_PRV_GetItemCount(st_object_t * const p_object, e_object_type_t type)
{
    r_result_t result = object_lock(p_object);
    uint32_t count = 0;

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }

        if (is_type(p_object, type) != R_RESULT_SUCCESS )
        {
            R_PRINT_ERROR("Object is not a valid type");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        count = p_object->meta.properties.item.count;
    }

    object_unlock(p_object);

    return count;
}

r_result_t R_OBJECT_PRV_SetItemCount(st_object_t * const p_object, e_object_type_t type, uint32_t item_count)
{
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (can_write_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }

        if (is_type(p_object, type) != R_RESULT_SUCCESS )
        {
            R_PRINT_ERROR("Object is not a valid type");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (item_count > p_object->meta.properties.item.max_items)
        {
            R_PRINT_ERROR("Item count is larger than max item size");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        p_object->meta.properties.item.count = item_count;
    }

    object_unlock(p_object);

    return result;
}

uint32_t R_OBJECT_PRV_GetItemIMP(st_object_t * const p_object, e_object_type_t type, uint32_t item_index)
{
    r_result_t result = object_lock(p_object);
    uint32_t imp_data = 0;

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }

        if (is_type(p_object, type) != R_RESULT_SUCCESS )
        {
            R_PRINT_ERROR("Object is not a valid type");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (get_item_address(p_object, item_index, &imp_data, NULL) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Failed to get item address");
            result = R_RESULT_FAILED;
            imp_data = 0;
        }
    }

    object_unlock(p_object);

    (void)result; //Not returned
    return imp_data;
}

void * R_OBJECT_PRV_GetItemCPU(st_object_t * const p_object, e_object_type_t type, uint32_t item_index)
{
    r_result_t result = object_lock(p_object);
    void * p_cpu_data = NULL;

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }

        if (is_type(p_object, type) != R_RESULT_SUCCESS )
        {
            R_PRINT_ERROR("Object is not a valid type");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (get_item_address(p_object, item_index, NULL, &p_cpu_data) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Failed to get item address");
            result = R_RESULT_FAILED;
            p_cpu_data = NULL;
        }
    }

    object_unlock(p_object);

    (void)result; //Not returned
    return (void *)p_cpu_data;
}

r_result_t R_OBJECT_PRV_AddItem(st_object_t * const p_object, e_object_type_t type, void const * const p_item)
{
    r_result_t result = object_lock(p_object);
    void * p_cpu_data = NULL;

    if (R_RESULT_SUCCESS == result)
    {
        if (can_write_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not in a valid state");
            result = R_RESULT_FAILED;
        }

        if (is_type(p_object, type) != R_RESULT_SUCCESS )
        {
            R_PRINT_ERROR("Object is not a valid type");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (get_item_address(p_object, p_object->meta.properties.item.count, NULL, &p_cpu_data) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Failed to get item address");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        R_COMMON_Memcpy(p_cpu_data, p_item, p_object->meta.properties.item.item_size);
        (p_object->meta.properties.item.count)++;
    }

    object_unlock(p_object);

    return result;
}

uint32_t R_OBJECT_PRV_GetDataPlaneIMP(st_object_t * const p_object, e_object_type_t type)
{
    uint32_t imp_data = 0;
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Invalid object state");
            result = R_RESULT_FAILED;
        }

        if (is_type(p_object, type ) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not a valid type");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        imp_data = p_object->data.imp;
    }

    object_unlock(p_object);

    return imp_data;
}

void * R_OBJECT_PRV_GetDataPlaneCPU(st_object_t * const p_object, e_object_type_t type)
{
    void * p_cpu_data = NULL;
    r_result_t result = object_lock(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (can_read_info(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Invalid object state");
            result = R_RESULT_FAILED;
        }

        if (is_type(p_object, type) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not a valid type");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        p_cpu_data = p_object->data.p_cpu;
    }

    object_unlock(p_object);

    return p_cpu_data;
}

/*! @endcond */
