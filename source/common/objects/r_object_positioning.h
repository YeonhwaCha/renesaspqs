/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_positioning.h
 * @brief         Header file for object definition and related API of positioning of the car.
 * @defgroup      PQS_Object_Positioning PQS Object Positioning
 * @section       PQS_Object_Positioning_Summary Module Summary
 *                Object definition for positioning and API for accessing data in positioning objects.
 * @ingroup       PQS_Objects
 * @{
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

#ifndef R_OBJECT_POSITIONING_H_
#define R_OBJECT_POSITIONING_H_

/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

#define R_OBJECT_POSITION_MAX_COUNT     (1u)   /*!< How many positioning data each object can hold */
#define R_OBJECT_POSITION_MAX_OBJECTS   (1u)   /*!< Number of positioning objects to allocate on startup */

/*! Image formats as a string array */
#define E_POSITIONING_DEVICE_AS_STRING    {                                                 \
                                                "R_POSITIONING_DEVICE_NOVATEL_PROPAK6",     \
                                                "R_POSITIONING_DEVICE_NOVATEL_ATP7500",     \
                                                "R_POSITIONING_DEVICE_MAX"                  \
                                            }

#define R_POSITION_DELAYED_FLAG_NONE             (0)        /*!< No delay */
#define R_POSITION_DELAYED_FLAG_LINEAR_POSITION  (1 << 0)   /*!< Linear position delayed flag */
#define R_POSITION_DELAYED_FLAG_LINEAR_VELOCITY  (1 << 1)   /*!< Linear velocity delayed flag */
#define R_POSITION_DELAYED_FLAG_ANGULAR_POSITION (1 << 2)   /*!< Angular position delayed flag */
#define R_POSITION_DELAYED_FLAG_ANGULAR_VELOCITY (1 << 3)   /*!< Angular velocity delayed flag */

/**********************************************************************************************************************
 Typedef definitions
 *********************************************************************************************************************/

/*! Enumerated positioning devices */
typedef enum
{
    R_POSITIONING_DEVICE_NOVATEL_PROPAK6 = 0, /*!< NovAtel ProPak6 */
    R_POSITIONING_DEVICE_NOVATEL_ATP7500,     /*!< NovAtel ATP7500 */
    R_POSITIONING_DEVICE_MAX                  /*!< Maximum positioning device value */
} e_positioning_device_t;

/*! Enumeration of solution type */
typedef enum
{
    R_POSITION_USABLE=0,                /*!< Usable solution */
    R_POSITION_DELAYED,                 /*!< solution is delayed */
    R_POSITION_NOT_USABLE,              /*!< Non usable solution */
} e_positioning_sol_type_t;

/*! vehicle position */
typedef struct {
    uint64_t timestamp;                 /*!< unix timestamp */
    int32_t age_ms;                     /*!< age of the data in milliseconds */
    int32_t delta_t;                    /*!< delta_t in milliseconds between current and previous update */
    double latitude;                    /*!< latitude in degrees */
    double longitude;                   /*!< longitude in degrees */
    double elevation;                   /*!< elevation in meters, WGS84 geodetic */
    double lat_std;                     /*!< Latitude standard deviation (m) */
    double long_std;                    /*!< Longitude standard deviation (m) */
    double elev_std;                    /*!< Height standard deviation (m) */
} st_ins_position_t;

/*! vehicle linear velocity */
typedef struct {
    uint64_t timestamp;                 /*!< unix timestamp */
    int32_t age_ms;                     /*!< age of the data in milliseconds */
    int32_t delta_t;                    /*!< delta_t in milliseconds between current and previous update */
    double north_vel;                   /*!< North velocity (m/s) */
    double east_vel;                    /*!< East velocity (m/s) */
    double up_vel;                      /*!< Up velocity (m/s) */
    double north_vel_std;               /*!< North velocity standard deviation (m/s) */
    double east_vel_std;                /*!< East velocity standard deviation (m/s) */
    double up_vel_std;                  /*!< Up velocity standard deviation (m/s) */
} st_linear_velocity_t;

/*! vehicle angular position */
typedef struct {
    uint64_t timestamp;                 /*!< unix timestamp */
    int32_t age_ms;                     /*!< age of the data in milliseconds */
    int32_t delta_t;                    /*!< delta_t in milliseconds between current and previous update */
    double pitch;                       /*!< Pitch in Local Level (degrees) - rotation about x axis*/
    double roll;                        /*!< Roll in Local Level (degrees) - rotation about y axis*/
    double azimuth;                     /*!< Pitch in Local Level (degrees) - rotation about z axis*/
    double pitch_std;                   /*!< Pitch standard deviation (degrees) */
    double roll_std;                    /*!< Roll standard deviation (degrees) */
    double azimuth_std;                 /*!< Azimuth standard deviation (degrees) */
} st_angular_position_t;

/*! vehicle angular velocity */
typedef struct {
    uint64_t timestamp;                 /*!< unix timestamp */
    int32_t age_ms;                     /*!< age of the data in milliseconds */
    int32_t delta_t;                    /*!< delta_t in milliseconds between current and previous update */
    double pitch_rate;                  /*!< About x axis rotation (right-handed) (rad/sample) */
    double roll_rate;                   /*!< About y axis rotation (right-handed) (rad/sample) */
    double yaw_rate;                    /*!< About z axis rotation (right-handed) (rad/sample) */
    double lateral_acc;                 /*!< INS Lateral Acceleration (along x axis) (m/s/sample) */
    double longitudinal_acc;            /*!< INS Longitudinal Acceleration (along y axis) (m/s/sample) */
    double vertical_acc;                /*!< INS Vertical Acceleration (along z axis) (m/s/sample) */
} st_angular_velocity_t;

/*! vehicle positioning data */
typedef struct {
    e_positioning_sol_type_t solution_type; /*!< solution type */
    uint8_t delayed_flag;               /*!< delayed flags */
    st_ins_position_t position;         /*!< position of the vehicle */
    double speed;                       /*!< absolute speed in m/s */
    st_linear_velocity_t linear_vel;    /*!< linear velocity */
    st_angular_position_t angular_pos;  /*!< angular position */
    st_angular_velocity_t angular_vel;  /*!< angular velocity */
} st_positioning_data_t;

/**********************************************************************************************************************
 Exported global variables
 *********************************************************************************************************************/

extern const st_object_config_t g_object_config_positioning;

/**********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 *********************************************************************************************************************/

extern r_result_t R_OBJECT_FillPositioningData(st_object_t * const p_object, const st_positioning_data_t * const p_item);
extern st_positioning_data_t * R_OBJECT_GetPosition(st_object_t * const p_object);

#endif /* R_OBJECT_POSITIONING_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Objects>
 *********************************************************************************************************************/
