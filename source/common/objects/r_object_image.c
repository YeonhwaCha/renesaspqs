/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_image.c
 * @brief         Source file for object definition and related API of image type
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include "r_common.h"
#include "r_object_image.h"

/*! Define object core, otherwise r_object_private.h will throw an error */
#define _R_OBJECT_CORE_
#include "r_object_private.h"

/**********************************************************************************************************************
 Typedef definitions
 *********************************************************************************************************************/

/*! Properties contained in the image properties */
typedef enum
{
    IMAGE_PROPERTY_FORMAT = 0,
    IMAGE_PROPERTY_WIDTH,
    IMAGE_PROPERTY_HEIGHT,
    IMAGE_PROPERTY_DEPTH,
    IMAGE_PROPERTY_BPP,
} e_image_property_t;

/**********************************************************************************************************************
 Exported global variables
 *********************************************************************************************************************/

/*! Config for image object */
const st_object_config_t g_object_config_image = {
    .type         = R_OBJECT_TYPE_IMAGE,
    .object_count = R_OBJECT_IMAGE_MAX_OBJECTS,
    .max_items    = 1,
    .item_size    = R_OBJECT_IMAGE_MAX_IMAGE_SIZE,
};

/**********************************************************************************************************************
 Functions
 *********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   set_image_property
 * @brief           Sets the property of an object to a new value
 * @param [in,out]  p_object - pointer to object
 * @param [in]      new_val - value to set property to
 * @param [in]      property - type of property to set
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t set_image_property(st_object_t * const p_object, uint32_t new_val, e_image_property_t property)
{
    r_result_t result = R_OBJECT_PRV_LockMutex(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (R_OBJECT_PRV_CanWriteInfo(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Invalid object state");
            result = R_RESULT_FAILED;
        }

        if (R_OBJECT_PRV_IsType(p_object, R_OBJECT_TYPE_IMAGE ) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not an image");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        switch(property)
        {
            case IMAGE_PROPERTY_FORMAT:
                p_object->meta.properties.image.format = new_val;
                break;
            case IMAGE_PROPERTY_WIDTH:
                p_object->meta.properties.image.width = new_val;
                break;
            case IMAGE_PROPERTY_HEIGHT:
                p_object->meta.properties.image.height = new_val;
                break;
            case IMAGE_PROPERTY_DEPTH:
                p_object->meta.properties.image.depth = new_val;
                break;
            case IMAGE_PROPERTY_BPP:
                p_object->meta.properties.image.byte_pp = new_val;
                break;
            default:
                result = R_RESULT_FAILED;
                break;
        }

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Error trying to set property -- invalid property");
        }
    }

    R_OBJECT_PRV_UnlockMutex(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   get_image_property
 * @brief           Gets the value of the specified property of an object
 * @param [in,out]  p_object - pointer to object
 * @param [in]      property - type of property to get
 * @retval          value of property
 *********************************************************************************************************************/
static uint32_t get_image_property(st_object_t * const p_object, e_image_property_t property)
{
    uint32_t property_value = 0;
    r_result_t result = R_OBJECT_PRV_LockMutex(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (R_OBJECT_PRV_CanReadInfo(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Invalid object state");
            result = R_RESULT_FAILED;
        }

        if (R_OBJECT_PRV_IsType(p_object, R_OBJECT_TYPE_IMAGE ) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not an image");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        switch(property)
        {
            case IMAGE_PROPERTY_FORMAT:
                property_value = p_object->meta.properties.image.format;
                break;
            case IMAGE_PROPERTY_WIDTH:
                property_value = p_object->meta.properties.image.width;
                break;
            case IMAGE_PROPERTY_HEIGHT:
                property_value = p_object->meta.properties.image.height;
                break;
            case IMAGE_PROPERTY_DEPTH:
                property_value = p_object->meta.properties.image.depth;
                break;
            case IMAGE_PROPERTY_BPP:
                property_value = p_object->meta.properties.image.byte_pp;
                break;
            default:
                result = R_RESULT_FAILED;
                break;
        }

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Error trying to get property -- invalid property");
        }
    }

    R_OBJECT_PRV_UnlockMutex(p_object);

    return property_value;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_SetImageHeight
 * @brief           Sets the image height property to the specified value
 * @param [in,out]  p_object - pointer to object
 * @param [in]      height - value to set the image height to
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_SetImageHeight(st_object_t * const p_object, uint32_t height)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (R_OBJECT_IMAGE_MAX_HEIGHT < height)
    {
        R_PRINT_ERROR("Height %d is larger than R_OBJECT_IMAGE_MAX_HEIGHT %d", height, R_OBJECT_IMAGE_MAX_HEIGHT);
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = set_image_property(p_object, height, IMAGE_PROPERTY_HEIGHT);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_SetImageWidth
 * @brief           Sets the image width property to the specified value
 * @param [in,out]  p_object - pointer to object
 * @param [in]      width - value to set the image width to
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_SetImageWidth(st_object_t * const p_object, uint32_t width)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (R_OBJECT_IMAGE_MAX_WIDTH < width)
    {
        R_PRINT_ERROR("Width %d is larger than R_OBJECT_IMAGE_MAX_WIDTH %d", width, R_OBJECT_IMAGE_MAX_WIDTH);
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = set_image_property(p_object, width, IMAGE_PROPERTY_WIDTH);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_SetImageDepth
 * @brief           Sets the image depth property (number of planes) to the specified value
 * @param [in,out]  p_object - pointer to object
 * @param [in]      depth - value to set the image depth to
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_SetImageDepth(st_object_t * const p_object, uint32_t depth)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (R_OBJECT_IMAGE_MAX_DEPTH < depth)
    {
        R_PRINT_ERROR("Depth %d is larger than R_OBJECT_IMAGE_MAX_WIDTH %d", depth, R_OBJECT_IMAGE_MAX_DEPTH);
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = set_image_property(p_object, depth, IMAGE_PROPERTY_DEPTH);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_SetImageBytesPerPixel
 * @brief           Sets the image bpp property to the specified value. Note this function set bytes per pixel not
 *                  bits per pixel
 * @param [in,out]  p_object - pointer to object
 * @param [in]      bpp - value to set the image bpp to
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_SetImageBytesPerPixel(st_object_t * const p_object, uint32_t bpp)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (R_OBJECT_IMAGE_MAX_BPP < bpp)
    {
        R_PRINT_ERROR("Bytes per pixel %d is larger than R_OBJECT_IMAGE_MAX_BPP %d", bpp, R_OBJECT_IMAGE_MAX_BPP);
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = set_image_property(p_object, bpp, IMAGE_PROPERTY_BPP);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_SetImageFormat
 * @brief           Sets the image format property to the specified value
 * @param [in,out]  p_object - pointer to object
 * @param [in]      format - value to set the image format to
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_SetImageFormat(st_object_t * const p_object, e_image_format_t format)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (R_IMAGE_FORMAT_MAX <= format)
    {
        R_PRINT_ERROR("Format %d is larger than R_IMAGE_FORMAT_MAX %d", format);
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = set_image_property(p_object, (uint32_t)format, IMAGE_PROPERTY_FORMAT);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetImageHeight
 * @brief           Returns the image height. Object must be locked in order to use this function.
 * @param [in]      p_object - pointer to object
 * @retval          0 - on error. Otherwise the height of the image
 *********************************************************************************************************************/
uint32_t R_OBJECT_GetImageHeight(st_object_t * const p_object)
{
    return get_image_property(p_object, IMAGE_PROPERTY_HEIGHT);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetImageWidth
 * @brief           Returns the image width. Object must be locked in order to use this function.
 * @param [in]      p_object - pointer to object
 * @retval          0 - on error. Otherwise the width of the image
 *********************************************************************************************************************/
uint32_t R_OBJECT_GetImageWidth(st_object_t * const p_object)
{
    return get_image_property(p_object, IMAGE_PROPERTY_WIDTH);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetImageDepth
 * @brief           Returns the image depth (number of planes). Object must be locked in order to use this function.
 * @param [in]      p_object - pointer to object
 * @retval          0 - on error. Otherwise the depth of the image
 *********************************************************************************************************************/
uint32_t R_OBJECT_GetImageDepth(st_object_t * const p_object)
{
    return get_image_property(p_object, IMAGE_PROPERTY_DEPTH);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetImageBytesPerPixel
 * @brief           Returns the image bytes per pixel. Object must be locked in order to use this function.
 * @param [in]      p_object - pointer to object
 * @retval          0 - on error. Otherwise the bpp of the image
 *********************************************************************************************************************/
uint32_t R_OBJECT_GetImageBytesPerPixel(st_object_t * const p_object)
{
    return get_image_property(p_object, IMAGE_PROPERTY_BPP);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetImageFormat
 * @brief           Returns the image format. Object must be locked in order to use this function.
 * @param [in]      p_object - pointer to object
 * @retval          R_IMAGE_FORMAT_INVALID - on error. Otherwise the format of the image
 *********************************************************************************************************************/
e_image_format_t R_OBJECT_GetImageFormat(st_object_t * const p_object)
{
    return (e_image_format_t)get_image_property(p_object, IMAGE_PROPERTY_FORMAT); /*lint !e9030 !e9034 */
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_CheckImageHeight
 * @brief           Checks if the image height is equal to the specified value
 * @param [in]      p_object - pointer to object
 * @param [in]      height - value to compare image height to
 * @retval          R_RESULT_SUCCESS - if image property matches specified value
 *********************************************************************************************************************/
r_result_t R_OBJECT_CheckImageHeight(st_object_t * const p_object, uint32_t height)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (get_image_property(p_object, IMAGE_PROPERTY_HEIGHT) != height)
    {
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_CheckImageWidth
 * @brief           Checks if the image width is equal to the specified value
 * @param [in]      p_object - pointer to object
 * @param [in]      width - value to compare image width to
 * @retval          R_RESULT_SUCCESS - if image property matches specified value
 *********************************************************************************************************************/
r_result_t R_OBJECT_CheckImageWidth(st_object_t * const p_object, uint32_t width)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (get_image_property(p_object, IMAGE_PROPERTY_WIDTH) != width)
    {
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_CheckImageDepth
 * @brief           Checks if the image depth (number of planes) is equal to the specified value
 * @param [in]      p_object - pointer to object
 * @param [in]      depth - value to compare image depth to
 * @retval          R_RESULT_SUCCESS - if image property matches specified value
 *********************************************************************************************************************/
r_result_t R_OBJECT_CheckImageDepth(st_object_t * const p_object, uint32_t depth)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (get_image_property(p_object, IMAGE_PROPERTY_DEPTH) != depth)
    {
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_CheckImageBytesPerPixel
 * @brief           Checks if the image bytes per pixel is equal to the specified value
 * @param [in]      p_object - pointer to object
 * @param [in]      bpp - value to compare image bpp to
 * @retval          R_RESULT_SUCCESS - if image property matches specified value
 *********************************************************************************************************************/
r_result_t R_OBJECT_CheckImageBytesPerPixel(st_object_t * const p_object, uint32_t bpp)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (get_image_property(p_object, IMAGE_PROPERTY_BPP) != bpp)
    {
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_CheckImageFormat
 * @brief           Checks if the image format is equal to the specified value
 * @param [in]      p_object - pointer to object
 * @param [in]      format - value to compare image format to
 * @retval          R_RESULT_SUCCESS - if image property matches specified value
 *********************************************************************************************************************/
r_result_t R_OBJECT_CheckImageFormat(st_object_t * const p_object, e_image_format_t format)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (get_image_property(p_object, IMAGE_PROPERTY_FORMAT) != (uint32_t)format)
    {
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetImagePlaneIMP
 * @brief           Returns the physical address of the image plane contained in the image object
 * @param [in]      p_object - pointer to object
 * @retval          0 - on error, Otherwise the address of the image plane
 *********************************************************************************************************************/
uint32_t R_OBJECT_GetImagePlaneIMP(st_object_t * const p_object)
{
    return R_OBJECT_PRV_GetDataPlaneIMP(p_object, R_OBJECT_TYPE_IMAGE);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetIndexedImagePlaneIMP
 * @brief           Returns the physical address of the image plane contained in the image object for the supplied
 *                  plane index. This is useful for images stored in multiple data planes.
 * @param [in]      p_object - pointer to object
 * @param [in]      plane_index - Index of the image plane
 * @retval          0 - on error, Otherwise the address of the image plane
 *********************************************************************************************************************/
uint32_t R_OBJECT_GetIndexedImagePlaneIMP(st_object_t * const p_object, uint32_t plane_index)
{
    uint32_t imp_data = 0;
    r_result_t result = R_OBJECT_PRV_LockMutex(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (R_OBJECT_PRV_CanReadInfo(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Invalid object state");
            result = R_RESULT_FAILED;
        }

        if (R_OBJECT_PRV_IsType(p_object, R_OBJECT_TYPE_IMAGE ) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not an image");
            result = R_RESULT_FAILED;
        }
    }

    /* Check depth, height, width and size */
    if (R_RESULT_SUCCESS == result)
    {
        if ((0u == p_object->meta.properties.image.depth)  ||
            (0u == p_object->meta.properties.image.width)  ||
            (0u == p_object->meta.properties.image.height) ||
            (0u == p_object->meta.properties.image.byte_pp))
        {
            R_PRINT_ERROR("Height, width and bpp are not all set.");
            result = R_RESULT_FAILED;
        }
    }

    /* Check plane_index */
    if (R_RESULT_SUCCESS == result)
    {
        if (plane_index >= p_object->meta.properties.image.depth)
        {
            R_PRINT_ERROR("Plane index is invalid");
            result = R_RESULT_FAILED;
        }
    }

    /* Get the data plane for the requested index */
    if (R_RESULT_SUCCESS == result)
    {
        imp_data = p_object->data.imp;

        imp_data += (plane_index * p_object->meta.properties.image.width *
                p_object->meta.properties.image.height * p_object->meta.properties.image.byte_pp);
    }

    R_OBJECT_PRV_UnlockMutex(p_object);

    return imp_data;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetImagePlaneCPU
 * @brief           Returns the virtual address of the image plane contained in the image object
 * @param [in]      p_object - pointer to object
 * @retval          NULL - on error, Otherwise the pointer to the image plane
 *********************************************************************************************************************/
void * R_OBJECT_GetImagePlaneCPU(st_object_t * const p_object)
{
    return R_OBJECT_PRV_GetDataPlaneCPU(p_object, R_OBJECT_TYPE_IMAGE);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetIndexedImagePlaneCPU
 * @brief           Returns the virtual address of the image plane contained in the image object for the supplied
 *                  plane index. This is useful for images stored in multiple data planes.
 * @param [in]      p_object - pointer to object
 * @param [in]      plane_index - Index of the image plane
 * @retval          NULL - on error, Otherwise the pointer to the image plane
 *********************************************************************************************************************/
void * R_OBJECT_GetIndexedImagePlaneCPU(st_object_t * const p_object, uint32_t plane_index)
{
    uint8_t * p_cpu_data = NULL;
    r_result_t result = R_OBJECT_PRV_LockMutex(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (R_OBJECT_PRV_CanReadInfo(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Invalid object state");
            result = R_RESULT_FAILED;
        }

        if (R_OBJECT_PRV_IsType(p_object, R_OBJECT_TYPE_IMAGE ) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not an image");
            result = R_RESULT_FAILED;
        }
    }

    /* Check depth, height, width and size */
    if (R_RESULT_SUCCESS == result)
    {
        if ((0u == p_object->meta.properties.image.depth)  ||
            (0u == p_object->meta.properties.image.width)  ||
            (0u == p_object->meta.properties.image.height) ||
            (0u == p_object->meta.properties.image.byte_pp))
        {
            R_PRINT_ERROR("Height, width and bpp are not all set.");
            result = R_RESULT_FAILED;
        }
    }

    /* Check plane_index */
    if (R_RESULT_SUCCESS == result)
    {
        if (plane_index >= p_object->meta.properties.image.depth)
        {
            R_PRINT_ERROR("Plane index is invalid");
            result = R_RESULT_FAILED;
        }
    }

    /* Get the data plane for the requested index */
    if (R_RESULT_SUCCESS == result)
    {
        p_cpu_data = p_object->data.p_cpu;

        p_cpu_data = &p_cpu_data[(plane_index * p_object->meta.properties.image.width *
                p_object->meta.properties.image.height * p_object->meta.properties.image.byte_pp)];
    }

    R_OBJECT_PRV_UnlockMutex(p_object);

    return p_cpu_data;
}
