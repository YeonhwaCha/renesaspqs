/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_2d_box.c
 * @brief         Source file for object definition and related API of two dimensional box type.
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include "r_common.h"
#include "r_object_2d_box.h"

/*! Define object core, otherwise r_object_private.h will throw an error */
#define _R_OBJECT_CORE_
#include "r_object_private.h"

/**********************************************************************************************************************
 Exported global variables
 *********************************************************************************************************************/

/*! Config for 2d Box object */
const st_object_config_t g_object_config_2d_box = {
    .type         = R_OBJECT_TYPE_2D_BOX,
    .object_count = R_OBJECT_2D_BOX_MAX_OBJECTS,
    .max_items    = R_OBJECT_2D_BOX_MAX_BOX_COUNT,
    .item_size    = sizeof(st_2d_box_t),
};

/**********************************************************************************************************************
 Functions
 *********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_Get2DBox
 * @brief           Get the pointer to the 2D box item at a specific index
 * @param [in]      p_object - pointer to the object
 * @param [in]      item_index - index of the item contained in the object
 * @retval          NULL - on error, otherwise the pointer to the item at the specified index
 *********************************************************************************************************************/
st_2d_box_t * R_OBJECT_Get2DBox(st_object_t * const p_object, uint32_t item_index)
{
    return (st_2d_box_t *)R_OBJECT_PRV_GetItemCPU(p_object, R_OBJECT_TYPE_2D_BOX, item_index);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_Add2DBox
 * @brief           Add a 2D box to the list of box items contained in the object
 * @param [in, out] p_object - pointer to the object
 * @param [in]      p_item - pointer to the box to add
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_Add2DBox(st_object_t * const p_object, const st_2d_box_t * const p_item)
{
    return R_OBJECT_PRV_AddItem(p_object, R_OBJECT_TYPE_2D_BOX, (const void * const)p_item);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_Get2DBoxCount
 * @brief           Returns the number of 2D box items currently contained in the object
 * @param [in]      p_object - pointer to the object
 * @retval          Number of boxes currently in the object, returns zero if there is an error
 *********************************************************************************************************************/
uint32_t R_OBJECT_Get2DBoxCount(st_object_t * const p_object)
{
    return R_OBJECT_PRV_GetItemCount(p_object, R_OBJECT_TYPE_2D_BOX);
}
