/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_3d_box.h
 * @brief         Header file for object definition and related API of three dimensional box type.
 * @defgroup      PQS_Object_3D_Box PQS Object 3D Box
 * @section       PQS_Object_3D_Box_Summary Module Summary
 *                Object definition for three dimensional box type and API for accessing boxes within object.
 * @ingroup       PQS_Objects
 * @{
 *********************************************************************************************************************/

/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#ifndef R_OBJECT_3D_BOX_H_
#define R_OBJECT_3D_BOX_H_

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define R_OBJECT_3D_BOX_MAX_OBJECTS    (4u)   /*!< Number of 3D box objects to allocate on startup */
#define R_OBJECT_3D_BOX_MAX_BOX_COUNT  (256u) /*!< How many 3D boxes each box object can hold */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Enumerated list of the 3D box classes */
typedef enum
{
    R_3D_BOX_CLASS_UNKNOWN = 0,         /*!< Unknown Box class */
    R_3D_BOX_CLASS_VEHICLE,             /*!< Vehicle class */
    R_3D_BOX_CLASS_CAR,                 /*!< Car class */
    R_3D_BOX_CLASS_LARGE_CAR,           /*!< Large car class (van, small truck, SUV)*/
    R_3D_BOX_CLASS_LONG_VEHICLE,        /*!< Long vehicle class (Bus, semi truck)*/
    R_3D_BOX_CLASS_GROUP_OF_VEHICLES,   /*!< Group of packed vehicles class */
    R_3D_BOX_CLASS_PEDESTRIAN,          /*!< Pedestrian class */
    R_3D_BOX_CLASS_MAX                  /*!< Max class... Keep this element last */
} e_3d_box_class_t;

/*! Definition of a point in terms of rows and columns of the input feature map */
typedef struct
{
    uint32_t r;  /*!< Row */
    uint32_t c;  /*!< Column */
} st_2d_rc_point_t;

/*! Definition of a point in terms of x,y,z coordinates */
typedef struct
{
    float x; /*!< x value in meters */
    float y; /*!< y value in meters */
    float z; /*!< z value in meters */
} st_3d_xyz_point_t;

/*! Definition of a 2D box in terms of four row/column points. The order of the points is not meaningful */
typedef struct
{
    st_2d_rc_point_t corner1; /*!< first corner of box */
    st_2d_rc_point_t corner2; /*!< second corner of box */
    st_2d_rc_point_t corner3; /*!< third corner of box */
    st_2d_rc_point_t corner4; /*!< fourth corner of box */
} st_2d_box_rc_t;

/*! Definition of a 3D Box in terms of xyz coordinates */
typedef struct
{
    st_3d_xyz_point_t center; /*!< Center of box */
    float yaw;                /*!< Yaw of box */
    float width;              /*!< Width of box in meters */
    float length;             /*!< Length of box in meters */
    float height;             /*!< Height of box in meters */
} st_3d_box_xyz_t;

/* TODO we should probably move the 2D stuff out of this object type */
/*! Definition of a 3D Box, Struct contains both 3D box in real world coordinates and 2D box in terms of row/column coordinates of the input feature map */
typedef struct
{
    st_3d_box_xyz_t  xyz_box;          /*!< 3D box in real world coordinates */
    st_2d_box_rc_t   rc_box;           /*!< 2D box in row/column feature map coordinates */
    e_3d_box_class_t box_class;        /*!< Class of the box */
    float            confidence;       /*!< Confidence of the box */
    uint32_t         id;               /*!< Numeric identifier of the box */
} st_3d_box_t;

/***********************************************************************************************************************
 Exported global variables
 **********************************************************************************************************************/

extern const st_object_config_t g_object_config_3d_box;

/***********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 **********************************************************************************************************************/

extern r_result_t R_OBJECT_Add3DBox(st_object_t * const p_object, const st_3d_box_t * const p_item);
extern st_3d_box_t * R_OBJECT_Get3DBox(st_object_t * const p_object,  uint32_t item_index);
extern uint32_t R_OBJECT_Get3DBoxCount(st_object_t * const p_object);
extern r_result_t R_OBJECT_Save3DBox(st_object_t * const p_object, const char * const p_file_path);

#endif /* R_OBJECT_3D_BOX_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Objects>
 *********************************************************************************************************************/

