/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_trigger.h
 * @brief         Header file for object definition for trigger type.
 * @defgroup      PQS_Object_Trigger PQS Object Trigger
 * @section       PQS_Object_Trigger_Summary Module Summary
 *                Object definition for trigger type. Note, triggers are only used to more finely control when
 *                a module will be executed in a graph. No actual data is passed with triggers. Module A's execution
 *                time can be delayed until after Module B is finished with processing by linking the output of
 *                Module B into the trigger input of Module A. This way it is assured that Module A will always
 *                occur after Module B even if all other inputs are ready for Module A to start processing.
 * @ingroup       PQS_Objects
 * @{
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

#ifndef R_OBJECT_TRIGGER_H_
#define R_OBJECT_TRIGGER_H_

/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

#define R_OBJECT_TRIGGER_MAX_OBJECTS   (8u) /*!< Max number of trigger object to allocate on startup */

/**********************************************************************************************************************
 Exported global variables
 *********************************************************************************************************************/

extern const st_object_config_t g_object_config_trigger;

#endif /* R_OBJECT_TRIGGER_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Objects>
 *********************************************************************************************************************/
