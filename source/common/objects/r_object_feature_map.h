/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_feature_map.h
 * @brief         Header file for object definition and related API of feature maps
 * @defgroup      PQS_Object_Feature_Map PQS Object Feature Map
 * @section       PQS_Object_Feature_Map_Summary Module Summary
 *                Object definition for feature map type and API for accessing data in feature map objects. Feature
 *                maps are used to store CNN input and output features for some CNN algorithms. They are similar
 *                to images but cannot be readily viewed.
 * @ingroup       PQS_Objects
 * @{
 *********************************************************************************************************************/

/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#ifndef R_OBJECT_FEATURE_MAP_
#define R_OBJECT_FEATURE_MAP_

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/
#define R_OBJECT_FEATURE_MAP_MAX_HEIGHT       (512u)           /*!< Max height of feature map */
#define R_OBJECT_FEATURE_MAP_MAX_WIDTH        (512u)           /*!< Max width of feature map */
#define R_OBJECT_FEATURE_MAP_MAX_FEATURES     (8u)             /*!< Max number of features (data planes) */
#define R_OBJECT_FEATURE_MAP_MAX_DATA_SIZE    (sizeof(float))  /*!< Max size of data in a feature) */
#define R_OBJECT_FEATURE_MAP_MAX_FEATURES_RAW (1u)             /*!< Max number of features (data planes) for a raw feature map */

#define R_OBJECT_FEATURE_MAP_MAX_SIZE      (R_OBJECT_FEATURE_MAP_MAX_HEIGHT   * \
                                            R_OBJECT_FEATURE_MAP_MAX_WIDTH    * \
                                            R_OBJECT_FEATURE_MAP_MAX_FEATURES * \
                                            R_OBJECT_FEATURE_MAP_MAX_DATA_SIZE) /*!< Max size of a feature map in bytes */

#define R_OBJECT_FEATURE_MAP_MAX_OBJECTS   (8u) /*!< Number of feature maps to allocate on startup */

#define R_BIRDEYE_ROI_MIN_X_AXIS_METERS   (-60.0f) /*!< Birdeye Feature Map Region: Min X Axis in meters */
#define R_BIRDEYE_ROI_MAX_X_AXIS_METERS   (60.0f)  /*!< Birdeye Feature Map Region: Max X Axis in meters */
#define R_BIRDEYE_ROI_MIN_Y_AXIS_METERS   (0.0f)   /*!< Birdeye Feature Map Region: Min Y Axis in meters */
#define R_BIRDEYE_ROI_MAX_Y_AXIS_METERS   (60.0f)  /*!< Birdeye Feature Map Region: Max X Axis in meters */

#define R_BIRDEYE_INMAP_MIN_NUM_POINTS     (0.0f)    /*!< Birdeye Input Feature Map: Minimum value of number of points feature */
#define R_BIRDEYE_INMAP_MAX_NUM_POINTS     (1000.0f) /*!< Birdeye Input Feature Map: Maximum value of number of points feature */
#define R_BIRDEYE_INMAP_MIN_MAX_HEIGHT     (-5.0f)   /*!< Birdeye Input Feature Map: Minimum value of max height feature */
#define R_BIRDEYE_INMAP_MAX_MAX_HEIGHT     (5.0f)    /*!< Birdeye Input Feature Map: Maximum value of max height feature */
#define R_BIRDEYE_INMAP_MIN_MEAN_HEIGHT    (0.0f)    /*!< Birdeye Input Feature Map: Minimum value of mean height feature */
#define R_BIRDEYE_INMAP_MAX_MEAN_HEIGHT    (5.0f)    /*!< Birdeye Input Feature Map: Maximum value of mean height feature */
#define R_BIRDEYE_INMAP_MIN_MEAN_INTENSITY (0.0f)    /*!< Birdeye Input Feature Map: Minimum value of mean intensity feature */
#define R_BIRDEYE_INMAP_MAX_MEAN_INTENSITY (255.0f)  /*!< Birdeye Input Feature Map: Maximum value of mean intensity feature */
#define R_BIRDEYE_INMAP_MIN_DISTANCE       (0.0f)    /*!< Birdeye Input Feature Map: Minimum value of distance feature */
#define R_BIRDEYE_INMAP_MAX_DISTANCE       (60.0f)   /*!< Birdeye Input Feature Map: Maximum value of distance feature */

#define R_FEATURE_MAP_BYTES_8              (1u)       /*!< Feature map 8bit datasize in bytes */
#define R_FEATURE_MAP_BYTES_16             (2u)       /*!< Feature map 16bit datasize in bytes */
#define R_FEATURE_MAP_BYTES_32             (4u)       /*!< Feature map 32bit datasize in bytes */

/*! Feature map formats as a string array */
#define E_MAP_FORMAT_AS_STRING    {                                           \
                                    "R_FEATURE_MAP_FORMAT_INVALID",           \
                                    "R_FEATURE_MAP_FORMAT_BIRDEYE_INMAP",     \
                                    "R_FEATURE_MAP_FORMAT_BIRDEYE_OUTMAP",    \
                                    "R_FEATURE_MAP_FORMAT_RAW",               \
                                    "R_FEATURE_MAP_FORMAT_MAX"                \
                                    }

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Enumeration of the different feature map types @anchor e_map_format_t */
typedef enum
{
    R_FEATURE_MAP_FORMAT_INVALID = 0,    /*!< Invalid format */
    R_FEATURE_MAP_FORMAT_BIRDEYE_INMAP,  /*!< Lidar birdeye view input feature map */
    R_FEATURE_MAP_FORMAT_BIRDEYE_OUTMAP, /*!< Lidar birdeye view output feature map */
    R_FEATURE_MAP_FORMAT_RAW,            /*!< General feature map format which can be used as a data container */
    R_FEATURE_MAP_FORMAT_MAX,            /*!< Max feature map... Keep this last */
} e_map_format_t;

/*! Enumeration of the different features that are a part of the raw feature map */
typedef enum
{
    R_FEATURE_MAP_DATASIZE_INVALID = 0, /*!< Invalid datasize*/
    R_FEATURE_MAP_DATASIZE_8,           /*!< 1 byte per feature map element */
    R_FEATURE_MAP_DATASIZE_16,          /*!< 2 bytes per feature map element */
    R_FEATURE_MAP_DATASIZE_32,          /*!< 4 bytes per feature map element */
    R_FEATURE_MAP_DATASIZE_MAX,         /*!< Max datasize... Keep this last */
} e_map_datasize_t;

/*! Enumeration of the different features that are a part of the birdeye input feature map */
typedef enum
{
    R_BIRDEYE_INMAP_FEATURE_NUM_POINTS = 0,  /*!< Feature: Number of points */
    R_BIRDEYE_INMAP_FEATURE_MAX_HEIGHT,      /*!< Feature: Maximum height */
    R_BIRDEYE_INMAP_FEATURE_MEAN_HEIGHT,     /*!< Feature: Mean height */
    R_BIRDEYE_INMAP_FEATURE_MEAN_INTENSITY,  /*!< Feature: mean intensity*/
    R_BIRDEYE_INMAP_FEATURE_DISTANCE,        /*!< Feature: Distance from sensor */
    R_BIRDEYE_INMAP_FEATURE_MAX,             /*!< Max feature... keep this last */
} e_birdeye_inmap_feature_t;

/*! Enumeration of the different features that are a part of the birdeye output feature map */
typedef enum
{
    R_BIRDEYE_OUTMAP_FEATURE_VEHICLE = 0,  /*!< Feature: Vehicle*/
    R_BIRDEYE_OUTMAP_FEATURE_PEDESTRIAN,   /*!< Feature: Pedestrian*/
    R_BIRDEYE_OUTMAP_FEATURE_MAX,          /*!< Max feature... keep this last */
} e_birdeye_outmap_feature_t;

/*! Birdeye output feature formats as a string array */
#define E_BIRDEYE_OUTMAP_FEATURE_AS_STRING  {                                          \
                                                "R_BIRDEYE_OUTMAP_FEATURE_VEHICLE",    \
                                                "R_BIRDEYE_OUTMAP_FEATURE_PEDESTRIAN", \
                                                "R_BIRDEYE_OUTMAP_FEATURE_MAX"         \
                                            }


/***********************************************************************************************************************
 Exported global variables
 **********************************************************************************************************************/

extern const st_object_config_t g_object_config_feature_map;

/***********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 **********************************************************************************************************************/

extern r_result_t R_OBJECT_SetFeatureMapHeight(st_object_t * const p_object, uint32_t height);
extern r_result_t R_OBJECT_SetFeatureMapWidth(st_object_t * const p_object, uint32_t width);
extern r_result_t R_OBJECT_SetFeatureMapFormat(st_object_t * const p_object, e_map_format_t format);
extern r_result_t R_OBJECT_SetFeatureMapDatasize(st_object_t * const p_object, e_map_datasize_t datasize);

extern uint32_t R_OBJECT_GetFeatureMapHeight(st_object_t * const p_object);
extern uint32_t R_OBJECT_GetFeatureMapWidth(st_object_t * const p_object);
extern e_map_format_t R_OBJECT_GetFeatureMapFormat(st_object_t * const p_object);
extern e_map_datasize_t R_OBJECT_GetFeatureMapDatasize(st_object_t * const p_object);

extern r_result_t R_OBJECT_CheckFeatureMapHeight(st_object_t * const p_object, uint32_t height);
extern r_result_t R_OBJECT_CheckFeatureMapWidth(st_object_t * const p_object, uint32_t width);
extern r_result_t R_OBJECT_CheckFeatureMapFormat(st_object_t * const p_object, e_map_format_t format);
extern r_result_t R_OBJECT_CheckFeatureMapDatasize(st_object_t * const p_object, e_map_datasize_t datasize);

extern uint32_t R_OBJECT_GetFeatureMapIMP(st_object_t * const p_object, uint32_t feature);
extern void * R_OBJECT_GetFeatureMapCPU(st_object_t * const p_object, uint32_t feature);

#endif /* R_OBJECT_FEATURE_MAP_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Objects>
 *********************************************************************************************************************/

