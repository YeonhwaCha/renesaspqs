/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_private.h
 * @brief         Header file of data and API's that are shared between PQS Objects and PQS Framework. This file
 *                should not be included by PQS Modules.
 * @defgroup      PQS_Object_Private_Handling PQS Object Private Handling
 * @section       PQS_Object_Private_Handling_Summary Module Summary
 *                This is an API only PQS Framework should use. This contains the definition of st_object_t which
 *                should remain an opaque type for all processing modules.
 * @ingroup       PQS_Objects
 * @{
 *********************************************************************************************************************/

/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#ifndef R_OBJECT_PRIVATE_H_
#define R_OBJECT_PRIVATE_H_

/******************************************************************************/
/*                              !!! WARNING !!!                               */
/******************************************************************************/
/* This is to only be included by core object files                           */
/* If you need something from this file then re-evaluate why you need it.     */
/******************************************************************************/

#ifndef _R_OBJECT_CORE_
#error This file must only be included in core framework functions
#endif


/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

/*! State variables as a string array */
#define E_OBJECT_STATE_AS_STRING    {                                  \
                                        "R_OBJECT_STATE_UNINITIALIZED", \
                                        "R_OBJECT_STATE_ERROR",         \
                                        "R_OBJECT_STATE_FREE",          \
                                        "R_OBJECT_STATE_PROCESSING",  \
                                        "R_OBJECT_STATE_LOCKED",        \
                                    }

/**********************************************************************************************************************
 Typedef definitions
 *********************************************************************************************************************/

/*! @image html object_state_machine.png */
/*! Object state */
typedef enum
{
    R_OBJECT_STATE_UNINITIALIZED = 0, /*!< Starting state */
    R_OBJECT_STATE_ERROR,             /*!< State if there ever is an error with the object */
    R_OBJECT_STATE_FREE,              /*!< Object is not currently being used by any modules */
    R_OBJECT_STATE_PROCESSING,        /*!< Object is currently being processed by some module. */
    R_OBJECT_STATE_LOCKED,            /*!< Processing is completed on the object. Object properties cannot be changed. */
} e_object_state_t;

/*! Header information is just related to the sensor itself.  This should be tied
 * to the sensor that generated the data */
typedef struct {
    uint64_t  time_stamp; /*!< When the data was captured */
    uint32_t  sensor_id;  /*!< Id of the sensor that captured the data */
    char      description[R_OBJECT_MAX_DESCRIPTION_LENGTH]; /*!< Optional string for object description */
} st_object_header_t;

/*! Properties related to how the image object is stored in memory. */
typedef struct {
    uint32_t format;      /*!< Image format type = e_image_format_t   */
    uint32_t width;       /*!< Width           */
    uint32_t height;      /*!< Height          */
    uint32_t depth;       /*!< # data planes   */
    uint32_t byte_pp;     /*!< Bytes per pixel */
} st_image_properties_t;

/*! Properties related to how the item object is stored in memory. */
typedef struct {
    uint32_t count;           /*!< Stored item count */
    uint32_t max_items;       /*!< Max # objects    */
    uint32_t item_size;       /*!< Bytes per object */
} st_item_properties_t;

/*! Properties related to how the map object is stored in memory. */
typedef struct {
    uint32_t       format;       /*!< Format  = e_map_format_t*/
    uint32_t       width;        /*!< Width  */
    uint32_t       height;       /*!< Height */
    uint32_t       num_features; /*!< number of features in the map */
    uint32_t       data_size;    /*!< Size one element */
} st_map_properties_t;

/*! Union of the image and item properties. Either image properties, map
 * properties or item properties will be used for an object. Never both. */
typedef union { /*lint !e9018 */
    st_image_properties_t   image;   /*!< Image properties */
    st_item_properties_t    item;    /*!< Item properties */
    st_map_properties_t     map;     /*!< Map properties */
} u_object_properties_t;

/*! This is control stuff unique to each object.  It is not to be modified
 * outside of r_object.c */
typedef struct {
    e_object_state_t state;     /*!< Is the object free/in use? */
    uint32_t         ref_count; /*!< How many modules are using the data */
    pthread_mutex_t  lock;      /*!< Lock for the data */
} st_object_control_t;

/*! This is data plane stuff unique to each object, and never changes after
 * initialization.  It is not to be modified outside of r_object.c. */
typedef struct {
    e_object_type_t  type;  /*!< What kind of data is it? */
    uint32_t         size;  /*!< How big is the data plane */
    void *           p_cpu; /*!< Pointer to virtual address, use for CPU processing */
    uint32_t         imp;   /*!< Physical address for use in imp subsystem */
} st_object_data_t;

/*! Data about data */
typedef struct {
    uint32_t                 id;         /*!< Unique ID, must not be 0 */
    st_object_header_t       header;     /*!< Header info showing what sensor it came from */
    u_object_properties_t    properties; /*!< Properties of the data */
} st_object_meta_t;

/*! The definition of an object. The purpose of having so many structs
 * is that it allows easy cloning of related properties. */
typedef struct st_object_tag {
    st_object_meta_t     meta;    /*!< Object Metadata */
    st_object_control_t  control; /*!< Control of the object */
    st_object_data_t     data;    /*!< Data plane storage */
} st_object_t; /*lint !e9109 !e761 */

/**********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 *********************************************************************************************************************/

/*! @cond INTERNAL */
r_result_t R_OBJECT_PRV_LockMutex(   st_object_t * const p_object);
void       R_OBJECT_PRV_UnlockMutex( st_object_t * const p_object);
r_result_t R_OBJECT_PRV_IsInState(   st_object_t * const p_object, e_object_state_t expected_state);
r_result_t R_OBJECT_PRV_IsType(      st_object_t * const p_object, e_object_type_t  expected_type);
r_result_t R_OBJECT_PRV_CanReadInfo( st_object_t * const p_object);
r_result_t R_OBJECT_PRV_CanWriteInfo(st_object_t * const p_object);

extern uint32_t   R_OBJECT_PRV_GetMaxItemCount(st_object_t * const p_object, e_object_type_t type);
extern uint32_t   R_OBJECT_PRV_GetItemCount(   st_object_t * const p_object, e_object_type_t type);
extern r_result_t R_OBJECT_PRV_SetItemCount(   st_object_t * const p_object, e_object_type_t type, uint32_t item_count);
extern uint32_t   R_OBJECT_PRV_GetItemIMP(     st_object_t * const p_object, e_object_type_t type, uint32_t item_index);
extern void *     R_OBJECT_PRV_GetItemCPU(     st_object_t * const p_object, e_object_type_t type, uint32_t item_index);
extern r_result_t R_OBJECT_PRV_AddItem(        st_object_t * const p_object, e_object_type_t type, void const * const p_item);
extern void *     R_OBJECT_PRV_GetDataPlaneCPU(st_object_t * const p_object, e_object_type_t type);
extern uint32_t   R_OBJECT_PRV_GetDataPlaneIMP(st_object_t * const p_object, e_object_type_t type);
/*! @endcond */

#endif /* R_OBJECT_PRIVATE_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Objects>
 *********************************************************************************************************************/
