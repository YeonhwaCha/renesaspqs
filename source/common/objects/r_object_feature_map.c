/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_feature_map.c
 * @brief         Source file for object definition and related API of feature maps
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/


/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include "r_common.h"
#include "r_object_feature_map.h"

/*! Define object core, otherwise r_object_private.h will throw an error */
#define _R_OBJECT_CORE_
#include "r_object_private.h"

/**********************************************************************************************************************
 Typedefs
 *********************************************************************************************************************/

/*! Properties types contained in the feature map properties */
typedef enum
{
    MAP_PROPERTY_FORMAT = 0,
    MAP_PROPERTY_WIDTH,
    MAP_PROPERTY_HEIGHT,
    MAP_PROPERTY_NUM_FEATURES,
    MAP_PROPERTY_DATA_SIZE,
} e_map_property_t;

/**********************************************************************************************************************
 Exported global variables
 *********************************************************************************************************************/

/*! Config for feature map object */
const st_object_config_t g_object_config_feature_map = {
    .type         = R_OBJECT_TYPE_FEATURE_MAP,
    .object_count = R_OBJECT_FEATURE_MAP_MAX_OBJECTS,
    .max_items    = 1,
    .item_size    = R_OBJECT_FEATURE_MAP_MAX_SIZE,
};

/**********************************************************************************************************************
 Functions
 *********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   set_property
 * @brief           Sets the property of an object to a new value
 * @param [in,out]  p_object - pointer to object
 * @param [in]      new_val - value to set property to
 * @param [in]      property - type of property to set
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t set_property(st_object_t * const p_object, uint32_t new_val, e_map_property_t property)
{
    r_result_t result = R_OBJECT_PRV_LockMutex(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (R_OBJECT_PRV_CanWriteInfo(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Invalid object state");
            result = R_RESULT_FAILED;
        }

        if (R_OBJECT_PRV_IsType(p_object, R_OBJECT_TYPE_FEATURE_MAP) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not an a feature map");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        switch(property)
        {
            case MAP_PROPERTY_FORMAT:
                p_object->meta.properties.map.format = new_val;
                break;
            case MAP_PROPERTY_WIDTH:
                p_object->meta.properties.map.width = new_val;
                break;
            case MAP_PROPERTY_HEIGHT:
                p_object->meta.properties.map.height = new_val;
                break;
            case MAP_PROPERTY_NUM_FEATURES:
                p_object->meta.properties.map.num_features = new_val;
                break;
            case MAP_PROPERTY_DATA_SIZE:
                p_object->meta.properties.map.data_size = new_val;
                break;
            default:
                result = R_RESULT_FAILED;
                break;
        }

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Error trying to set property -- invalid property");
        }
    }

    R_OBJECT_PRV_UnlockMutex(p_object);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   get_property
 * @brief           Gets the value of the specified property of an object
 * @param [in,out]  p_object - pointer to object
 * @param [in]      property - type of property to get
 * @retval          value of property
 *********************************************************************************************************************/
static uint32_t get_property(st_object_t * const p_object, e_map_property_t property)
{
    uint32_t property_value = 0;
    r_result_t result = R_OBJECT_PRV_LockMutex(p_object);

    if (R_RESULT_SUCCESS == result)
    {
        if (R_OBJECT_PRV_CanReadInfo(p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Invalid object state");
            result = R_RESULT_FAILED;
        }

        if (R_OBJECT_PRV_IsType(p_object, R_OBJECT_TYPE_FEATURE_MAP ) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Object is not a feature map");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        switch (property)
        {
            case MAP_PROPERTY_FORMAT:
                property_value = p_object->meta.properties.map.format;
                break;
            case MAP_PROPERTY_WIDTH:
                property_value = p_object->meta.properties.map.width;
                break;
            case MAP_PROPERTY_HEIGHT:
                property_value = p_object->meta.properties.map.height;
                break;
            case MAP_PROPERTY_NUM_FEATURES:
                property_value = p_object->meta.properties.map.num_features;
                break;
            case MAP_PROPERTY_DATA_SIZE :
                property_value = p_object->meta.properties.map.data_size;
                break;
            default:
                result = R_RESULT_FAILED;
                break;
        }
    }

    R_OBJECT_PRV_UnlockMutex(p_object);

    (void)result; //Not returned
    return property_value;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_SetFeatureMapHeight
 * @brief           Sets the feature map height property to the specified value
 * @param [in,out]  p_object - pointer to object
 * @param [in]      height - value to set the feature map height to
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_SetFeatureMapHeight(st_object_t * const p_object, uint32_t height)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (R_OBJECT_FEATURE_MAP_MAX_HEIGHT < height)
    {
        R_PRINT_ERROR("Height %d is larger than R_OBJECT_FEATURE_MAP_MAX_HEIGHT %d", height, R_OBJECT_FEATURE_MAP_MAX_HEIGHT);
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = set_property(p_object, height, MAP_PROPERTY_HEIGHT);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_SetFeatureMapWidth
 * @brief           Sets the feature map width property to the specified value
 * @param [in,out]  p_object - pointer to object
 * @param [in]      width - value to set the feature map width to
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_SetFeatureMapWidth(st_object_t * const p_object, uint32_t width)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (R_OBJECT_FEATURE_MAP_MAX_WIDTH < width)
    {
        R_PRINT_ERROR("Width %d is larger than R_OBJECT_FEATURE_MAP_MAX_WIDTH %d", width, R_OBJECT_FEATURE_MAP_MAX_WIDTH);
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        result = set_property(p_object, width, MAP_PROPERTY_WIDTH);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_SetFeatureMapFormat
 * @brief           Sets the feature map format property to the specified value
 * @param [in,out]  p_object - pointer to object
 * @param [in]      format - value to set the feature map format to
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_SetFeatureMapFormat(st_object_t * const p_object, e_map_format_t format)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Check format */
    if ((R_FEATURE_MAP_FORMAT_INVALID == format) || (R_FEATURE_MAP_FORMAT_MAX <= format))
    {
        R_PRINT_ERROR("Format is not valid", format);
        result = R_RESULT_FAILED;
    }

    /* Set format and corresponding attributes */
    if (R_RESULT_SUCCESS == result)
    {
        result |= set_property(p_object, (uint32_t)format, MAP_PROPERTY_FORMAT);

        switch (format)
        {
            case R_FEATURE_MAP_FORMAT_BIRDEYE_INMAP:
            {
                result |= set_property(p_object, (uint32_t)R_BIRDEYE_INMAP_FEATURE_MAX, MAP_PROPERTY_NUM_FEATURES);
                result |= set_property(p_object, (uint32_t)sizeof(float),               MAP_PROPERTY_DATA_SIZE);
                break;
            }

            case R_FEATURE_MAP_FORMAT_BIRDEYE_OUTMAP:
            {
                result |= set_property(p_object, (uint32_t)R_BIRDEYE_OUTMAP_FEATURE_MAX, MAP_PROPERTY_NUM_FEATURES);
                result |= set_property(p_object, (uint32_t)sizeof(float),                MAP_PROPERTY_DATA_SIZE);
                break;
            }

            case R_FEATURE_MAP_FORMAT_RAW:
            {
                result |= set_property(p_object, R_OBJECT_FEATURE_MAP_MAX_FEATURES_RAW, MAP_PROPERTY_NUM_FEATURES);
                break;
            }

            case R_FEATURE_MAP_FORMAT_INVALID:
            case R_FEATURE_MAP_FORMAT_MAX:
            default:
            {
                R_PRINT_ERROR("Fell through switch. Format not supported");
                result = R_RESULT_FAILED;
                break;
            }
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_SetFeatureMapDatasize
 * @brief           Sets the feature map element datasize property to the specified value
 * @param [in,out]  p_object - pointer to object
 * @param [in]      datasize - value to set the feature map datasize to
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_SetFeatureMapDatasize(st_object_t * const p_object, e_map_datasize_t datasize)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Check datasize */
    if ((R_FEATURE_MAP_DATASIZE_INVALID == datasize) || (R_FEATURE_MAP_DATASIZE_MAX <= datasize))
    {
        R_PRINT_ERROR("Datasize is not valid", datasize);
        result = R_RESULT_FAILED;
    }

    /* Set datasize */
    if (R_RESULT_SUCCESS == result)
    {
        switch (datasize)
        {
            case R_FEATURE_MAP_DATASIZE_8:
            {
                result |= set_property(p_object, (uint32_t)sizeof(uint8_t), MAP_PROPERTY_DATA_SIZE);
                break;
            }

            case R_FEATURE_MAP_DATASIZE_16:
            {
                result |= set_property(p_object, (uint32_t)sizeof(uint16_t), MAP_PROPERTY_DATA_SIZE);
                break;
            }

            case R_FEATURE_MAP_DATASIZE_32:
            {
                result |= set_property(p_object, (uint32_t)sizeof(uint32_t), MAP_PROPERTY_DATA_SIZE);
                break;
            }

            case R_FEATURE_MAP_DATASIZE_INVALID:
            case R_FEATURE_MAP_DATASIZE_MAX:
            default:
            {
                R_PRINT_ERROR("Fell through switch. Datasize not supported");
                result = R_RESULT_FAILED;
                break;
            }
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetFeatureMapHeight
 * @brief           Returns the feature map height. Object must be locked in order to use this function.
 * @param [in]      p_object - pointer to object
 * @retval          0 - on error. Otherwise the height of the feature map
 *********************************************************************************************************************/
uint32_t R_OBJECT_GetFeatureMapHeight(st_object_t * const p_object)
{
    return get_property(p_object, MAP_PROPERTY_HEIGHT);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetFeatureMapWidth
 * @brief           Returns the feature map width. Object must be locked in order to use this function.
 * @param [in]      p_object - pointer to object
 * @retval          0 - on error. Otherwise the width of the feature map
 *********************************************************************************************************************/
uint32_t R_OBJECT_GetFeatureMapWidth(st_object_t * const p_object)
{
    return get_property(p_object, MAP_PROPERTY_WIDTH);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetFeatureMapFormat
 * @brief           Returns the feature map format. Object must be locked in order to use this function.
 * @param [in]      p_object - pointer to object
 * @retval          R_IMAGE_FORMAT_INVALID - on error. Otherwise the format of the feature map
 *********************************************************************************************************************/
e_map_format_t R_OBJECT_GetFeatureMapFormat(st_object_t * const p_object)
{
    return (e_map_format_t)get_property(p_object, MAP_PROPERTY_FORMAT); /*lint !e9030 !e9034 */
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetFeatureMapDatasize
 * @brief           Returns the feature map datasize. Object must be locked in order to use this function.
 * @param [in]      p_object - pointer to object
 * @retval          R_IMAGE_FORMAT_INVALID - on error. Otherwise the datasize of the feature map
 *********************************************************************************************************************/
e_map_datasize_t R_OBJECT_GetFeatureMapDatasize(st_object_t * const p_object)
{
    return (e_map_datasize_t)get_property(p_object, MAP_PROPERTY_DATA_SIZE); /*lint !e9030 !e9034 */
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_CheckFeatureMapHeight
 * @brief           Checks if the feature map height is equal to the specified value
 * @param [in]      p_object - pointer to object
 * @param [in]      height - value to compare feature map height to
 * @retval          R_RESULT_SUCCESS - if feature map property matches specified value
 *********************************************************************************************************************/
r_result_t R_OBJECT_CheckFeatureMapHeight(st_object_t * const p_object, uint32_t height)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (get_property(p_object, MAP_PROPERTY_HEIGHT) != height)
    {
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_CheckFeatureMapWidth
 * @brief           Checks if the feature map width is equal to the specified value
 * @param [in]      p_object - pointer to object
 * @param [in]      width - value to compare feature map width to
 * @retval          R_RESULT_SUCCESS - if feature map property matches specified value
 *********************************************************************************************************************/
r_result_t R_OBJECT_CheckFeatureMapWidth(st_object_t * const p_object, uint32_t width)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (get_property(p_object, MAP_PROPERTY_WIDTH) != width)
    {
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_CheckFeatureMapFormat
 * @brief           Checks if the feature map format is equal to the specified value
 * @param [in]      p_object - pointer to object
 * @param [in]      format - value to compare feature map format to
 * @retval          R_RESULT_SUCCESS - if feature map property matches specified value
 *********************************************************************************************************************/
r_result_t R_OBJECT_CheckFeatureMapFormat(st_object_t * const p_object, e_map_format_t format)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (get_property(p_object, MAP_PROPERTY_FORMAT) != (uint32_t)format)
    {
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_CheckFeatureMapDatasize
 * @brief           Checks if the feature map datasize is equal to the specified value
 * @param [in]      p_object - pointer to object
 * @param [in]      datasize - value to compare feature map datasize to
 * @retval          R_RESULT_SUCCESS - if feature map property matches specified value
 *********************************************************************************************************************/
r_result_t R_OBJECT_CheckFeatureMapDatasize(st_object_t * const p_object, e_map_datasize_t datasize)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (get_property(p_object, MAP_PROPERTY_DATA_SIZE) != (uint32_t)datasize)
    {
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetFeatureMapIMP
 * @brief           Returns the physical address of the specified feature map plane contained in the feature map object
 * @param [in]      p_object - pointer to object
 * @param [in]      feature - Which feature plane to get
 * @retval          0 - on error, Otherwise the address of the feature map plane
 *********************************************************************************************************************/
uint32_t R_OBJECT_GetFeatureMapIMP(st_object_t * const p_object, uint32_t feature)
{
    uint32_t address = 0;
    e_map_format_t format = (e_map_format_t)get_property(p_object, MAP_PROPERTY_FORMAT); /*lint !e9030 !e9034 */
    r_result_t result = R_OBJECT_PRV_LockMutex(p_object);

    /* Check format */
    if (R_RESULT_SUCCESS == result)
    {
        if ((R_FEATURE_MAP_FORMAT_INVALID == format) || (R_FEATURE_MAP_FORMAT_MAX <= format))
        {
            R_PRINT_ERROR("Format is invalid");
            result = R_RESULT_FAILED;
        }
    }

    /* Check feature */
    if (R_RESULT_SUCCESS == result)
    {
        if (feature >= p_object->meta.properties.map.num_features)
        {
            R_PRINT_ERROR("Feature is invalid");
            result = R_RESULT_FAILED;
        }
    }

    /* Check height, width and size */
    if (R_RESULT_SUCCESS == result)
    {
        if ((0u == p_object->meta.properties.map.width)  ||
            (0u == p_object->meta.properties.map.height) ||
            (0u == p_object->meta.properties.map.data_size))
        {
            R_PRINT_ERROR("Height, width and data size are not all set.");
            result = R_RESULT_FAILED;
        }
    }

    /* Get address for the requested feature */
    if (R_RESULT_SUCCESS == result)
    {
        /* Get address for feature 0 */
        address = p_object->data.imp;

        /* Add offset to get requested feature */
        address += (feature *  p_object->meta.properties.map.width *
                p_object->meta.properties.map.height * p_object->meta.properties.map.data_size);
    }

    R_OBJECT_PRV_UnlockMutex(p_object);

    return address;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetFeatureMapCPU
 * @brief           Returns a pointer to the virtual memory of the specified feature map plane contained in the
 *                  feature map object
 * @param [in]      p_object - pointer to object
 * @param [in]      feature - Which feature plane to get
 * @retval          NULL - on error, Otherwise a pointer to the feature map plane
 *********************************************************************************************************************/
void * R_OBJECT_GetFeatureMapCPU(st_object_t * const p_object, uint32_t feature)
{
    uint8_t * p_map = NULL;
    e_map_format_t format = (e_map_format_t)get_property(p_object, MAP_PROPERTY_FORMAT); /*lint !e9030 !e9034 */
    r_result_t result = R_OBJECT_PRV_LockMutex(p_object);

    /* Check format */
    if (R_RESULT_SUCCESS == result)
    {
        if ((R_FEATURE_MAP_FORMAT_INVALID == format) || (R_FEATURE_MAP_FORMAT_MAX <= format))
        {
            R_PRINT_ERROR("Format is invalid");
            result = R_RESULT_FAILED;
        }
    }

    /* Check feature */
    if (R_RESULT_SUCCESS == result)
    {
        if (feature >= p_object->meta.properties.map.num_features)
        {
            R_PRINT_ERROR("Feature is invalid");
            result = R_RESULT_FAILED;
        }
    }

    /* Check height, width and size */
    if (R_RESULT_SUCCESS == result)
    {
        if ((0u == p_object->meta.properties.map.width)  ||
            (0u == p_object->meta.properties.map.height) ||
            (0u == p_object->meta.properties.map.data_size))
        {
            R_PRINT_ERROR("Height, width and data size are not all set.");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* Get address for feature 0 */
        p_map = p_object->data.p_cpu;

        /* Add offset to get requested feature */
        p_map = &p_map[(feature * p_object->meta.properties.map.width *
                p_object->meta.properties.map.height * p_object->meta.properties.map.data_size)];
    }

    R_OBJECT_PRV_UnlockMutex(p_object);

    return (void *)p_map;
}
