/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object.h
 * @brief         Header file for PQS Object handling
 * @defgroup      PQS_Objects PQS Objects
 * @brief         Object handling for various object types
 * @defgroup      PQS_Object_Core PQS Object Core
 * @section       PQS_Object_Core_Summary Module Summary
 *                Core object handling for non type specific operations. Note, some API should only be used by PQS
 *                Framework and not by data processing modules. These functions are noted in the function brief.
 * @ingroup       PQS_Objects
 * @{
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

#ifndef R_OBJECT_H_
#define R_OBJECT_H_

/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

#define R_OBJECT_ID_INVALID (0u)   /*!< Object ID set when an object is in an invalid state */

/*! Maximum length for object description */
#define R_OBJECT_MAX_DESCRIPTION_LENGTH   (128u)

/*! Enumerated list of object types as a string array */
#define E_OBJECT_TYPE_AS_STRING    {                                    \
                                        "R_OBJECT_TYPE_INVALID",         \
                                        "R_OBJECT_TYPE_IMAGE",           \
                                        "R_OBJECT_TYPE_LANE",            \
                                        "R_OBJECT_TYPE_2D_BOX",          \
                                        "R_OBJECT_TYPE_3D_BOX",          \
                                        "R_OBJECT_TYPE_LIDAR_FRAME",     \
                                        "R_OBJECT_TYPE_FEATURE_MAP",     \
                                        "R_OBJECT_TYPE_FREESPACE",       \
                                        "R_OBJECT_TYPE_POSITION",        \
                                        "R_OBJECT_TYPE_TRIGGER",         \
                                        "R_OBJECT_TYPE_MAX",             \
                                    }

/**********************************************************************************************************************
 Typedef definitions
 *********************************************************************************************************************/

/*! Enum for all the object types @anchor e_object_type_t */
typedef enum {
    R_OBJECT_TYPE_INVALID = 0,       /*!< Invalid type */
    R_OBJECT_TYPE_IMAGE,             /*!< Image type */
    R_OBJECT_TYPE_LANE,              /*!< Lane type */
    R_OBJECT_TYPE_2D_BOX,            /*!< Two Dimensional Bounding Box type */
    R_OBJECT_TYPE_3D_BOX,            /*!< Three Dimensional Bounding Box type */
    R_OBJECT_TYPE_LIDAR_FRAME,       /*!< Lidar frame type */
    R_OBJECT_TYPE_FEATURE_MAP,       /*!< Feature map type */
    R_OBJECT_TYPE_FREESPACE,         /*!< Freespace type */
    R_OBJECT_TYPE_POSITION,          /*!< Vehicle position type */
    R_OBJECT_TYPE_TRIGGER,           /*!< Trigger type, special type used just to add finer control to graph building / module execution. No data is transferred with this object, */
    R_OBJECT_TYPE_MAX,               /*!< KEEP THIS LAST! End of enum.  */
} e_object_type_t;

/*! This is an opaque data type. Only the object handlers can dereference it. Modules and PQS Framework should use
 *  the dedicated API for all object interactions. */
typedef struct st_object_tag st_object_t;

/*! This struct describes an object list. Needed for each object type. */
typedef struct st_object_config_s {
    e_object_type_t type;         /*!< What kind of data is this */
    uint32_t        object_count; /*!< How many objects do we need? */
    uint32_t        max_items;    /*!< How many items do we store in each object? */
    uint32_t        item_size;    /*!< What's the max size of each item? */
} st_object_config_t;

/**********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 *********************************************************************************************************************/

extern r_result_t R_OBJECT_Init(const st_object_config_t * const * const pp_config_list, uint32_t config_list_length);
extern r_result_t R_OBJECT_Deinit(void);

/**********************************************************************************************************************
 Functions to be accessed by PQS Framework only
 *********************************************************************************************************************/

extern st_object_t * R_OBJECT_GetFreeObject(const e_object_type_t type);

extern r_result_t R_OBJECT_Lock(st_object_t * const p_object);

extern r_result_t R_OBJECT_IncrementRefCount(st_object_t * const p_object);
extern r_result_t R_OBJECT_DecrementRefCount(st_object_t * const p_object);
extern r_result_t R_OBJECT_GetRefCount(st_object_t * const p_object, uint32_t * p_ref_count);

/**********************************************************************************************************************
 Functions to be accessed by Modules
 *********************************************************************************************************************/

extern r_result_t R_OBJECT_GetObjectId(st_object_t * const p_object, uint32_t * const p_get_id);

extern r_result_t R_OBJECT_GetObjectType(st_object_t * const p_object, e_object_type_t * p_get_object_type);
extern r_result_t R_OBJECT_CheckObjectType(st_object_t * const p_object, const e_object_type_t expected_type);

extern r_result_t R_OBJECT_GetSensorId(st_object_t * const p_object, uint32_t * const p_get_sensor_id);
extern r_result_t R_OBJECT_SetSensorId(st_object_t * const p_object, uint32_t set_sensor_id);
extern r_result_t R_OBJECT_SetSensorIdFromObject(st_object_t * const p_object_dest, st_object_t * const p_object_source);

extern r_result_t R_OBJECT_GetTimeStamp(st_object_t * const p_object, uint64_t * const p_get_time);
extern r_result_t R_OBJECT_SetTimeStamp(st_object_t * const p_object, const uint64_t set_time);
extern r_result_t R_OBJECT_SetTimeStampFromObject(st_object_t * const p_object_dest, st_object_t * const p_object_source);

extern r_result_t R_OBJECT_GetDescription(st_object_t * const p_object, char * const p_get_description);
extern r_result_t R_OBJECT_SetDescription(st_object_t * const p_object, const char * const p_set_description);
extern r_result_t R_OBJECT_CopyDescription(st_object_t * const p_object_dest, st_object_t * const p_object_source);

extern r_result_t R_OBJECT_CopyHeader(st_object_t * const p_object_dest, st_object_t * const p_object_source);
extern r_result_t R_OBJECT_CopyProperties(st_object_t * const p_object_dest, st_object_t * const p_object_source);

extern r_result_t R_OBJECT_PrintDiagInfo(st_object_t * const p_object);
extern r_result_t R_OBJECT_ReadROF(st_object_t * const p_object, const char * const filename);
extern r_result_t R_OBJECT_WriteROF(st_object_t * const p_object, const char * const filename);
extern r_result_t R_OBJECT_ReadRaw(st_object_t * const p_object, const char * const filename);
extern r_result_t R_OBJECT_WriteRaw(st_object_t * const p_object, const char * const filename);

#endif /* R_OBJECT_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Objects>
 *********************************************************************************************************************/
