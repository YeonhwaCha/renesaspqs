/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_lidar_frame.h
 * @brief         Header file for object definition and related API of a lidar frame
 * @defgroup      PQS_Object_Lidar_Frame PQS Object Lidar Frame
 * @section       PQS_Object_Lidar_Frame_Summary Module Summary
 *                Object definition for lidar frame type and API for accessing data in lidar frame objects.
 * @ingroup       PQS_Objects
 * @{
 *********************************************************************************************************************/
/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

#ifndef R_OBJECT_LIDAR_FRAME_H_
#define R_OBJECT_LIDAR_FRAME_H_

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define R_OBJECT_LIDAR_FRAME_MAX_OBJECTS     (4u)      /*!< Number of lidar frame objects to allocate on startup */
#define R_OBJECT_MAX_LIDAR_POINT_COUNT       (266627u) /*!< Maximum number of points each object can hold */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Definition of a single lidar point in the frame */
typedef struct
{
    float x;            /*!< In meters */
    float y;            /*!< In meters */
    float z;            /*!< In meters */
    float intensity;    /*!< 0-255 */
} st_lidar_point_t;

/***********************************************************************************************************************
 Exported global variables
 **********************************************************************************************************************/

extern const st_object_config_t g_object_config_lidar_frame;

/***********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 **********************************************************************************************************************/

extern uint32_t    R_OBJECT_GetLidarPointCount(st_object_t * const p_object);
extern r_result_t R_OBJECT_SetLidarPointCount(st_object_t * const p_object, uint32_t count);

extern uint32_t  R_OBJECT_GetLidarFrameIMP(st_object_t * const p_object);
extern st_lidar_point_t * R_OBJECT_GetLidarFrameCPU(st_object_t * const p_object);

#endif /* R_OBJECT_LIDAR_FRAME_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Objects>
 *********************************************************************************************************************/

