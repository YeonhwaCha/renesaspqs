/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_image.h
 * @brief         Header file for object definition and related API of image type
 * @defgroup      PQS_Object_Image PQS Object Image
 * @section       PQS_Object_Image_Summary Module Summary
 *                This is the definition for image objects and an API to access data in image objects.
 * @ingroup       PQS_Objects
 * @{
 *********************************************************************************************************************/

/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#ifndef R_OBJECT_IMAGE_H_
#define R_OBJECT_IMAGE_H_

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define R_OBJECT_IMAGE_MAX_HEIGHT     (1080u)  /*!< Max height of an image: 1080        @anchor R_OBJECT_IMAGE_MAX_HEIGHT */
#define R_OBJECT_IMAGE_MAX_WIDTH      (1280u)  /*!< Max width of an image: 1280         @anchor R_OBJECT_IMAGE_MAX_WIDTH */
#define R_OBJECT_IMAGE_MAX_BPP        (4u)     /*!< Max Bytes per pixels of an image: 4 @anchor R_OBJECT_IMAGE_MAX_BPP */
#define R_OBJECT_IMAGE_MAX_DEPTH      (3u)     /*!< Max depth (planes) of an image: 3   @anchor R_OBJECT_IMAGE_MAX_DEPTH*/

#define R_OBJECT_IMAGE_MAX_IMAGE_SIZE  (R_OBJECT_IMAGE_MAX_HEIGHT * \
                                        R_OBJECT_IMAGE_MAX_HEIGHT * \
                                        R_OBJECT_IMAGE_MAX_BPP    * \
                                        R_OBJECT_IMAGE_MAX_DEPTH) /*!< Max size of an image plane in bytes */

#define R_OBJECT_IMAGE_MAX_OBJECTS    (24u) /*!< Maximum image objects to allocate on startup */

#define R_IMAGE_BYTES_PP_UYVY_INTERLEAVED          (2u) /*!< Bytes per pixel for R_IMAGE_FORMAT_UYVY_INTERLEAVED */
#define R_IMAGE_BYTES_PP_YUV422_SEMI_PLANAR        (1u) /*!< Bytes per pixel for a single plane of R_IMAGE_FORMAT_YUV422_SEMI_PLANAR */
#define R_IMAGE_BYTES_PP_YUV444_FULLY_PLANAR       (1u) /*!< Bytes per pixel for a single plane of  R_IMAGE_FORMAT_YUV444_FULLY_PLANAR */
#define R_IMAGE_BYTES_PP_YUV444_FULLY_PLANAR_16BIT (2u) /*!< Bytes per pixel for a single plane of  R_IMAGE_BYTES_PP_YUV444_FULLY_PLANAR_16BIT */
#define R_IMAGE_BYTES_PP_UYVA_INTERLEAVED          (4u) /*!< Bytes per pixel for R_IMAGE_FORMAT_UYVA_INTERLEAVED */
#define R_IMAGE_BYTES_PP_RGB24_INTERLEAVED         (3u) /*!< Bytes per pixel for R_IMAGE_FORMAT_RGB24_INTERLEAVED */
#define R_IMAGE_BYTES_PP_RGB32_INTERLEAVED         (4u) /*!< Bytes per pixel for R_IMAGE_FORMAT_RGB32_INTERLEAVED */
#define R_IMAGE_BYTES_PP_GRAYSCALE_8BIT            (1u) /*!< Bytes per pixel for R_IMAGE_FORMAT_GRAYSCALE_8BIT */
#define R_IMAGE_BYTES_PP_GRAYSCALE_16BIT           (2u) /*!< Bytes per pixel for R_IMAGE_FORMAT_GRAYSCALE_16BIT */
#define R_IMAGE_BYTES_PP_HOUGH_16BIT               (2u) /*!< Bytes per pixel for R_IMAGE_FORMAT_HOUGH_16BIT */
#define R_IMAGE_BYTES_PP_ENUM                      (1u) /*!< Bytes per pixel for R_IMAGE_FORMAT_ENUM */
#define R_IMAGE_BYTES_PP_DOF                       (4u) /*!< Bytes per pixel for R_IMAGE_FORMAT_ENUM */
#define R_IMAGE_BYTES_PP_BINARY                    (1u) /*!< Bytes per pixel for R_IMAGE_FORMAT_BINARY */

/*! Plane indexes for R_IMAGE_FORMAT_YUV444_FULLY_PLANAR image */
#define R_IMAGE_YUV444_PLANE_INDEX_Y  (0u) /*!< Plane index for Y plane */
#define R_IMAGE_YUV444_PLANE_INDEX_U  (1u) /*!< Plane index for U plane */
#define R_IMAGE_YUV444_PLANE_INDEX_V  (2u) /*!< Plane index for V plane */

/*! Image formats as a string array */
#define E_IMAGE_FORMAT_AS_STRING    {                                               \
                                        "R_IMAGE_FORMAT_INVALID",                   \
                                        "R_IMAGE_FORMAT_UYVY_INTERLEAVED",          \
                                        "R_IMAGE_FORMAT_YUV422_SEMI_PLANAR",        \
                                        "R_IMAGE_FORMAT_YUV444_FULLY_PLANAR",       \
                                        "R_IMAGE_FORMAT_YUV444_FULLY_PLANAR_16BIT", \
                                        "R_IMAGE_FORMAT_UYVA_INTERLEAVED",          \
                                        "R_IMAGE_FORMAT_RGB24_INTERLEAVED",         \
                                        "R_IMAGE_FORMAT_RGB32_INTERLEAVED",         \
                                        "R_IMAGE_FORMAT_GRAYSCALE_8BIT",            \
                                        "R_IMAGE_FORMAT_GRAYSCALE_16BIT",           \
                                        "R_IMAGE_FORMAT_HOUGH_16BIT",               \
                                        "R_IMAGE_FORMAT_ENUM",                      \
                                        "R_IMAGE_FORMAT_DOF",                       \
                                        "R_IMAGE_FORMAT_BINARY",                    \
                                        "R_IMAGE_FORMAT_MAX"                        \
                                    }

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Enumerated image formats @anchor e_image_format_t*/
typedef enum
{
    R_IMAGE_FORMAT_INVALID = 0,                /*!< Invalid type */
    R_IMAGE_FORMAT_UYVY_INTERLEAVED,           /*!< UYVY AKA Interleaved YUV 422 */
    R_IMAGE_FORMAT_YUV422_SEMI_PLANAR,         /*!< Semiplanar YUV422  */
    R_IMAGE_FORMAT_YUV444_FULLY_PLANAR,        /*!< Fully planar YUV444 */
    R_IMAGE_FORMAT_YUV444_FULLY_PLANAR_16BIT,  /*!< Fully planar YUV444 16bit version*/
    R_IMAGE_FORMAT_UYVA_INTERLEAVED,           /*!< Interleaved YUV444 AKA UYVA */
    R_IMAGE_FORMAT_RGB24_INTERLEAVED,          /*!< Interleaved RGB24 */
    R_IMAGE_FORMAT_RGB32_INTERLEAVED,          /*!< Interleaved RGB24 */
    R_IMAGE_FORMAT_GRAYSCALE_8BIT,             /*!< Grayscale 8 bit */
    R_IMAGE_FORMAT_GRAYSCALE_16BIT,            /*!< Grayscale 16 bit*/
    R_IMAGE_FORMAT_HOUGH_16BIT,                /*!< Represents the output of a Hough transformation */
    R_IMAGE_FORMAT_ENUM,                       /*!< Image whereby each pixel represents an enumerated class */ //TODO this really isn't an image per se It could be best to use feature maps instead of this type
    R_IMAGE_FORMAT_DOF,                        /*!< Image whereby each pixel represents the dense optical flow */
    R_IMAGE_FORMAT_BINARY,                     /*!< Binary */ /* TODO Currently our "binaries" have 255 for 1 This makes it easier to scale using IMR */
    R_IMAGE_FORMAT_MAX                         /*!< Maximum format value */
} e_image_format_t;

/*! Classes of Semseg enum output  */
typedef enum
{
    R_SEMSEG_CLASS_BACKGROUND = 0,
    R_SEMSEG_CLASS_ROAD,
    R_SEMSEG_CLASS_LANE,
    R_SEMSEG_CLASS_CURB,
    R_SEMSEG_CLASS_VEHICLE,
    R_SEMSEG_CLASS_OTHER_MARKINGS,
    R_SEMSEG_CLASS_SIDEWALK,
    R_SEMSEG_CLASS_PEDESTRIAN,
    R_SEMSEG_CLASS_MAX
} e_semseg_classes_t;

/***********************************************************************************************************************
 Exported global variables
 **********************************************************************************************************************/

extern const st_object_config_t g_object_config_image;

/***********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 **********************************************************************************************************************/

extern r_result_t R_OBJECT_SetImageHeight(       st_object_t * const p_object, uint32_t height);
extern r_result_t R_OBJECT_SetImageWidth(        st_object_t * const p_object, uint32_t width);
extern r_result_t R_OBJECT_SetImageDepth(        st_object_t * const p_object, uint32_t depth);
extern r_result_t R_OBJECT_SetImageBytesPerPixel(st_object_t * const p_object, uint32_t bpp);
extern r_result_t R_OBJECT_SetImageFormat(       st_object_t * const p_object, e_image_format_t format);

extern uint32_t R_OBJECT_GetImageHeight(        st_object_t * const p_object);
extern uint32_t R_OBJECT_GetImageWidth(         st_object_t * const p_object);
extern uint32_t R_OBJECT_GetImageDepth(         st_object_t * const p_object);
extern uint32_t R_OBJECT_GetImageBytesPerPixel( st_object_t * const p_object);
extern e_image_format_t R_OBJECT_GetImageFormat(st_object_t * const p_object);

extern r_result_t R_OBJECT_CheckImageHeight(       st_object_t * const p_object, uint32_t height);
extern r_result_t R_OBJECT_CheckImageWidth(        st_object_t * const p_object, uint32_t width);
extern r_result_t R_OBJECT_CheckImageDepth(        st_object_t * const p_object, uint32_t depth);
extern r_result_t R_OBJECT_CheckImageBytesPerPixel(st_object_t * const p_object, uint32_t bpp);
extern r_result_t R_OBJECT_CheckImageFormat(       st_object_t * const p_object, e_image_format_t format);

extern uint32_t  R_OBJECT_GetImagePlaneIMP(st_object_t * const p_object);
extern void *    R_OBJECT_GetImagePlaneCPU(st_object_t * const p_object);

uint32_t  R_OBJECT_GetIndexedImagePlaneIMP(st_object_t * const p_object, uint32_t plane_index);
void *    R_OBJECT_GetIndexedImagePlaneCPU(st_object_t * const p_object, uint32_t plane_index);

#endif /* R_OBJECT_IMAGE_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Objects>
 *********************************************************************************************************************/
