/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_freespace.h
 * @brief         Header file for object definition and related API of freespace type.
 * @defgroup      PQS_Object_Freespace PQS Object Freespace
 * @section       PQS_Object_Freespace_Summary Module Summary
 *                Object definition for freespace type and API for accessing data in freespace objects.
 * @ingroup       PQS_Objects
 * @{
 *********************************************************************************************************************/
/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

#ifndef R_OBJECT_FREESPACE_H_
#define R_OBJECT_FREESPACE_H_

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define R_OBJECT_MAX_FREESPACE_COUNT      (32u) /*!< How many freespace each object can hold */
#define R_OBJECT_FREESPACE_MAX_OBJECTS    (8u)  /*!< Number of freespace objects to allocate on startup */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Definition of freespace structure */
typedef struct st_freespace_s {
    float          a_left;       /**< left lane marker polynomial,  In the form x = a*y^2 + b*y + c */
    float          b_left;       /**< left lane marker polynomial,  In the form x = a*y^2 + b*y + c */
    float          c_left;       /**< left lane marker polynomial,  In the form x = a*y^2 + b*y + c */
    float          a_right;      /**< right lane marker polynomial, In the form x = a*y^2 + b*y + c */
    float          b_right;      /**< right lane marker polynomial, In the form x = a*y^2 + b*y + c */
    float          c_right;      /**< right lane marker polynomial, In the form x = a*y^2 + b*y + c */
    uint32_t       upper_y;      /**< freespace upper boundary y in pixels */
    uint32_t       lower_y;      /**< freespace lower boundary y in pixels */
} st_freespace_t;

/***********************************************************************************************************************
 Exported global variables
 **********************************************************************************************************************/

extern const st_object_config_t g_object_config_freespace;

/***********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 **********************************************************************************************************************/

extern r_result_t R_OBJECT_AddFreespace(st_object_t * const p_object, const st_freespace_t * const p_item);
extern st_freespace_t * R_OBJECT_GetFreespace(st_object_t * const p_object, uint32_t item_index);
extern uint32_t R_OBJECT_GetFreespaceCount(st_object_t * const p_object);

#endif /* R_OBJECT_FREESPACE_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Objects>
 *********************************************************************************************************************/
