/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_3d_box.c
 * @brief         Source file for object definition and related API of three dimensional box type.
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include "r_common.h"
#include "r_object_3d_box.h"

/*! Define object core, otherwise r_object_private.h will throw an error */
#define _R_OBJECT_CORE_
#include "r_object_private.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define BOX_PRV_CLASS_TO_STRING_BUFFER_LENGTH   (256u)   /*!< Size of the char array used to store the obstacle class converted to string. */
#define BOX_PRV_ELEMENT_BUFFER_SIZE             (1024u)  /*!< Size of the char array used to store the content of the JSON block for one box. */

/**********************************************************************************************************************
 Exported global variables
 *********************************************************************************************************************/

/*! Config for 3d box object */
const st_object_config_t g_object_config_3d_box = {
    .type         = R_OBJECT_TYPE_3D_BOX,
    .object_count = R_OBJECT_3D_BOX_MAX_OBJECTS,
    .max_items    = R_OBJECT_3D_BOX_MAX_BOX_COUNT,
    .item_size    = sizeof(st_3d_box_t),
};

/**********************************************************************************************************************
 Functions
 *********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   get_class_string
 * @brief           Return the string name for the obstacle class of the specified 3D box
 * @param [out]     p_string - pointer to the char array for storing the class string
 * @param [in]      length - size of the p_string array
 * @param [in]      p_bbox - pointer to the 3D box
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t get_class_string(char * p_string, uint32_t length, st_3d_box_t * p_bbox)
{
    r_result_t result = R_RESULT_SUCCESS;
    
    /* Check for null pointers */
    if (NULL == p_string)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to class string is NULL.");
    }
    if (NULL == p_bbox)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to 3D box is NULL.");
    }
    
    if (R_RESULT_SUCCESS == result)
    {   
        /* Specify the corresponding string for each class name */
        switch (p_bbox->box_class)
        {
            case R_3D_BOX_CLASS_UNKNOWN:
            {
                result |= R_COMMON_STRING_Copy(p_string, "Unknown", length);
                break;
            }
            case R_3D_BOX_CLASS_VEHICLE:
            case R_3D_BOX_CLASS_CAR:
            case R_3D_BOX_CLASS_LARGE_CAR:
            case R_3D_BOX_CLASS_LONG_VEHICLE:
            {
                result |= R_COMMON_STRING_Copy(p_string, "Car", length);
                break;
            }
            case R_3D_BOX_CLASS_GROUP_OF_VEHICLES:
            {
                result |= R_COMMON_STRING_Copy(p_string, "Vehicle group", length);
                break;
            }
            case R_3D_BOX_CLASS_PEDESTRIAN:
            {
                result |= R_COMMON_STRING_Copy(p_string, "Pedestrian", length);
                break;
            }
            case R_3D_BOX_CLASS_MAX:
            default:
            {
                result = R_RESULT_FAILED;
                R_PRINT_ERROR("Fell through switch. Format not supported.");
                break;
            }
        }
    }
    if (R_RESULT_SUCCESS != result)
    {
        R_PRINT_ERROR("Error getting the obstacle class.");
    }
    
    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_Get3DBox
 * @brief           Get the pointer to the 3D box item at a specific index
 * @param [in]      p_object - pointer to the object
 * @param [in]      item_index - index of the item contained in the object
 * @retval          NULL - on error, otherwise the pointer to the item at the specified index
 *********************************************************************************************************************/
st_3d_box_t * R_OBJECT_Get3DBox(st_object_t * const p_object, uint32_t item_index)
{
    return (st_3d_box_t *)R_OBJECT_PRV_GetItemCPU(p_object, R_OBJECT_TYPE_3D_BOX, item_index);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_Add3DBox
 * @brief           Add a 3D box to the list of box items contained in the object
 * @param [in, out] p_object - pointer to the object
 * @param [in]      p_item - pointer to the box to add
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_Add3DBox(st_object_t * const p_object, const st_3d_box_t * const p_item)
{
    return R_OBJECT_PRV_AddItem(p_object, R_OBJECT_TYPE_3D_BOX, (const void * const)p_item);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_Get3DBoxCount
 * @brief           Returns the number of 3D box items currently contained in the object
 * @param [in]      p_object - pointer to the object
 * @retval          Number of boxes currently in the object
 *********************************************************************************************************************/
uint32_t R_OBJECT_Get3DBoxCount(st_object_t * const p_object)
{
    return R_OBJECT_PRV_GetItemCount(p_object, R_OBJECT_TYPE_3D_BOX);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_Save3DBox
 * @brief           Saves the 3D items to a JSON file.
 * @param [in]      p_object - pointer to the object
 * @param [in]      p_file_path - pointer to file path
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_Save3DBox(st_object_t * const p_object, const char * const p_file_path)
{
    /* Initialized  locals */
    r_result_t result = R_RESULT_SUCCESS;
    st_json_block_t json_data;           /* JSON block for the entire file. Do not need to initialize */
    st_json_block_t json_3d_boxes_array; /* JSON block for the boxes array. Do not need to initialize */
    st_json_block_t json_box_element;    /* JSON block for one box. Do not need to initialize */
    char json_3d_boxes_array_buffer[R_JSON_DATA_BUFFER_SIZE] = {'\0'}; /* Char array for storing the boxes array content */
    char json_3d_box_buffer[BOX_PRV_ELEMENT_BUFFER_SIZE]     = {'\0'}; /* Char array for storing the box content */
    char box_class[BOX_PRV_CLASS_TO_STRING_BUFFER_LENGTH]    = {'\0'}; /* Char array for storing the string name of the box class */
    char obj_description[R_OBJECT_MAX_DESCRIPTION_LENGTH]    = {'\0'}; /* Char array for storing the object description */
    char file_buffer[R_JSON_DATA_BUFFER_SIZE]                = {'\0'}; /* Char array for storing the JSON file content */

    st_3d_box_t *p_bbox = NULL; /* Pointer to the 3D box */

    uint32_t box_index = 0;
    uint32_t num_boxes = 0;

    /* Make sure we got our private data */
    if (NULL == p_file_path)
    {
        R_PRINT_ERROR("Couldn't get file path");
        result = R_RESULT_FAILED;
    }

    /* Check object */
    if (R_OBJECT_CheckObjectType(p_object, R_OBJECT_TYPE_3D_BOX) != R_RESULT_SUCCESS)
    {
        R_PRINT_ERROR("Invalid object input");
        result = R_RESULT_FAILED;
    }
    
    /* Get the number of boxes */
    if (R_RESULT_SUCCESS == result)
    {
        num_boxes = R_OBJECT_Get3DBoxCount(p_object);
    }
    
    /* Get the object description */
    if (R_RESULT_SUCCESS == result)
    {
        result = R_OBJECT_GetDescription(p_object, obj_description);
    }
    
    /* Build 3d_box_json_file */
    if (R_RESULT_SUCCESS == result)
    {
        /* Prepare the main block */
        result |= R_COMMON_JSON_InitUnnamedBlock(&json_data, file_buffer, R_JSON_DATA_BUFFER_SIZE);
        
        /* Add the file name entry to the main block */
        result |= R_COMMON_JSON_AddStrEntry(&json_data, "pcap_name", obj_description);
        
        /* Prepare the boxes array */
        result |= R_COMMON_JSON_InitArrayBlock(&json_3d_boxes_array, "bboxes", json_3d_boxes_array_buffer, R_JSON_DATA_BUFFER_SIZE);
        
        for (box_index = 0; box_index < num_boxes; box_index++)
        {
            /* Prepare the current element of the boxes array */
            result |= R_COMMON_JSON_InitUnnamedBlock(&json_box_element, json_3d_box_buffer, BOX_PRV_ELEMENT_BUFFER_SIZE);
            
            /* Get the pointer to the current box */            
            p_bbox = R_OBJECT_Get3DBox(p_object, box_index);
            
            /* Get the string name for the class of the current box */
            result |= get_class_string(box_class, BOX_PRV_CLASS_TO_STRING_BUFFER_LENGTH, p_bbox);
        
            /* Add entries to the current element of the boxes array */
            result |= R_COMMON_JSON_AddStrEntry(  &json_box_element, "object_class",     box_class);
            result |= R_COMMON_JSON_AddFloatEntry(&json_box_element, "h",                p_bbox->xyz_box.height);
            result |= R_COMMON_JSON_AddFloatEntry(&json_box_element, "w",                p_bbox->xyz_box.width);
            result |= R_COMMON_JSON_AddFloatEntry(&json_box_element, "l",                p_bbox->xyz_box.length);
            result |= R_COMMON_JSON_AddFloatEntry(&json_box_element, "location_x",       p_bbox->xyz_box.center.x);
            result |= R_COMMON_JSON_AddFloatEntry(&json_box_element, "location_y",       p_bbox->xyz_box.center.y);
            result |= R_COMMON_JSON_AddFloatEntry(&json_box_element, "location_z",       p_bbox->xyz_box.center.z);
            result |= R_COMMON_JSON_AddFloatEntry(&json_box_element, "rz",               p_bbox->xyz_box.yaw);
            result |= R_COMMON_JSON_AddIntEntry(  &json_box_element, "id",               (int32_t)p_bbox->id);
            result |= R_COMMON_JSON_AddFloatEntry(&json_box_element, "confidence_value", p_bbox->confidence);
            
            /* Put the current element into the boxes array */
            result |= R_COMMON_JSON_AddBlockContent(&json_3d_boxes_array, &json_box_element);        
        }
        
        /* Put the boxes array into the main block */
        result |= R_COMMON_JSON_AddBlockContent(&json_data, &json_3d_boxes_array);
    }
    
    if (R_RESULT_SUCCESS == result)
    {
        R_PRINT_INFO("Saving detected obstacles to file: %s", p_file_path);
        
        /* Save to JSON file */
        result |= R_COMMON_JSON_SaveToFile(&json_data, p_file_path);
    }
    else
    {
        R_PRINT_ERROR("Error saving 3D obstacles to file.");
    }
    
    return result;
}
