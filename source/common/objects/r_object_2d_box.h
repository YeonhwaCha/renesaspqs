/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_2d_box.h
 * @brief         Header file for object definition and related API of two dimensional box type.
 * @defgroup      PQS_Object_2D_Box PQS Object 2D Box
 * @section       PQS_Object_2D_Box_Summary Module Summary
 *                Object definition for two dimensional box type and API for accessing data in 2D box objects.
 * @ingroup       PQS_Objects
 * @{
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

#ifndef R_OBJECT_2D_BOX_H_
#define R_OBJECT_2D_BOX_H_

/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

#define R_OBJECT_2D_BOX_MAX_OBJECTS    (16u)  /*!< Number of 2D box objects to allocate on startup */
#define R_OBJECT_2D_BOX_MAX_BOX_COUNT  (256u) /*!< How many 2D boxes each box object can hold */

/**********************************************************************************************************************
 Typedef definitions
 *********************************************************************************************************************/

/*! Enumerated list of the 2D box classes */
typedef enum
{
    R_2D_BOX_CLASS_UNKNOWN = 0,      /*!< Unknown Box class */
    R_2D_BOX_CLASS_VEHICLE,          /*!< Vehicle class */
    R_2D_BOX_CLASS_PEDESTRIAN,       /*!< Pedestrian class */
    R_2D_BOX_CLASS_BICYCLE,          /*!< Bicycle class */
    R_2D_BOX_CLASS_MOTORCYCLE,       /*!< Motorcycle class */
    R_2D_BOX_CLASS_TRUCK,            /*!< Truck class */
    R_2D_BOX_CLASS_MAX               /*!< Max class... Keep this element last */
} e_2d_box_class_t;

/*! Structure of a single 2D box */
typedef struct
{
    uint32_t         xmin;             /*!< Left    */
    uint32_t         ymin;             /*!< Top     */
    uint32_t         xmax;             /*!< Right   */
    uint32_t         ymax;             /*!< Bottom  */
    e_2d_box_class_t box_class;        /*!< Class of the bounding box */
    float            confidence;       /*!< Confidence from 0.0 - 1.0 */
    uint32_t         id;               /*!< ID of the detected obstacle */
} st_2d_box_t;

/**********************************************************************************************************************
 Exported global variables
 *********************************************************************************************************************/

extern const st_object_config_t g_object_config_2d_box;

/**********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 *********************************************************************************************************************/

extern r_result_t R_OBJECT_Add2DBox(st_object_t * const p_object, const st_2d_box_t * const p_item);
extern st_2d_box_t * R_OBJECT_Get2DBox(st_object_t * const p_object, uint32_t item_index);
extern uint32_t R_OBJECT_Get2DBoxCount(st_object_t * const p_object);

#endif /* R_OBJECT_2D_BOX_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Objects>
 *********************************************************************************************************************/

