/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_lane.c
 * @brief         Source file for object definition and related API of lane type.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/
#include "r_common.h"
#include "r_object_lane.h"

/*! Define object core, otherwise r_object_private.h will throw an error */
#define _R_OBJECT_CORE_
#include "r_object_private.h"

/**********************************************************************************************************************
 Exported global variables
 *********************************************************************************************************************/

/*! Object config for Lane object */
const st_object_config_t g_object_config_lane = {
    .type         = R_OBJECT_TYPE_LANE,
    .object_count = R_OBJECT_LANE_MAX_OBJECTS,
    .max_items    = R_OBJECT_LANE_MAX_LANE_COUNT,
    .item_size    = sizeof(st_lane_t),
};

/**********************************************************************************************************************
 Functions
 *********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetLane
 * @brief           Get the pointer to the lane item at a specific index
 * @param [in]      p_object - pointer to the object
 * @param [in]      item_index - index of the item contained in the object
 * @retval          NULL - on error, otherwise the pointer to the item at the specified index
 *********************************************************************************************************************/
st_lane_t * R_OBJECT_GetLane(st_object_t * const p_object, uint32_t item_index)
{
    return (st_lane_t *)R_OBJECT_PRV_GetItemCPU(p_object, R_OBJECT_TYPE_LANE, item_index);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_AddLane
 * @brief           Add a lane struct to the list of lane items contained in the object
 * @param [in, out] p_object - pointer to the object
 * @param [in]      p_item - pointer to the lane to add
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_AddLane(st_object_t * const p_object, const st_lane_t * const p_item)
{
    return R_OBJECT_PRV_AddItem(p_object, R_OBJECT_TYPE_LANE, (const void * const)p_item);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetLaneCount
 * @brief           Returns the number of lane items currently contained in the object
 * @param [in]      p_object - pointer to the object
 * @retval          Number of lanes currently in the object
 *********************************************************************************************************************/
uint32_t R_OBJECT_GetLaneCount(st_object_t * const p_object)
{
    return R_OBJECT_PRV_GetItemCount(p_object, R_OBJECT_TYPE_LANE);
}
