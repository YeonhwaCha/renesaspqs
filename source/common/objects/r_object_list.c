/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_list.c
 * @brief         Source file of object list. This contains all of the object configurations of all object types.
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include "r_common.h"
#include "r_object_list.h"
#include "r_object_image.h"
#include "r_object_lane.h"
#include "r_object_2d_box.h"
#include "r_object_3d_box.h"
#include "r_object_lidar_frame.h"
#include "r_object_feature_map.h"
#include "r_object_trigger.h"
#include "r_object_freespace.h"
#include "r_object_positioning.h"

/**********************************************************************************************************************
 Exported global variables
 *********************************************************************************************************************/

/*! List of all the configurations for all object types */
st_object_config_t const * const gp_object_list[] = {
    &g_object_config_image,
    &g_object_config_lane,
    &g_object_config_2d_box,
    &g_object_config_3d_box,
    &g_object_config_lidar_frame,
    &g_object_config_feature_map,
    &g_object_config_freespace,
    &g_object_config_positioning,
    &g_object_config_trigger,
};

/*! Number of objects types in object list */
const uint32_t g_object_list_size = (uint32_t)(sizeof(gp_object_list) / sizeof(gp_object_list[0]));
