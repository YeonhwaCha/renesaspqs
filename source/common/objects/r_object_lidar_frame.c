/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_object_lidar_frame.c
 * @brief         Source file for object definition and related API of a lidar frame
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include "r_common.h"
#include "r_object_lidar_frame.h"

/*! Define object core, otherwise r_object_private.h will throw an error */
#define _R_OBJECT_CORE_
#include "r_object_private.h"

/**********************************************************************************************************************
 Exported global variables
 *********************************************************************************************************************/

/*! Config for lidar frame object */
const st_object_config_t g_object_config_lidar_frame = {
    .type         = R_OBJECT_TYPE_LIDAR_FRAME,
    .object_count = R_OBJECT_LIDAR_FRAME_MAX_OBJECTS,
    .max_items    = R_OBJECT_MAX_LIDAR_POINT_COUNT,
    .item_size    = sizeof(st_lidar_point_t),
};

/**********************************************************************************************************************
 Functions
 *********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetLidarPointCount
 * @brief           Get the number of lidar points currently stored in the lidar frame object
 * @param [in]      p_object - pointer to the object
 * @retval          Number of lidar points in the object
 *********************************************************************************************************************/
uint32_t R_OBJECT_GetLidarPointCount(st_object_t * const p_object)
{
    return R_OBJECT_PRV_GetItemCount(p_object, R_OBJECT_TYPE_LIDAR_FRAME);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_SetLidarPointCount
 * @brief           Set the number of lidar points that is stored in the lidar frame object
 * @param [in,out]  p_object - pointer to the object
 * @param [in]      count - Number of points to set object property to
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_OBJECT_SetLidarPointCount(st_object_t * const p_object, uint32_t count)
{
    return R_OBJECT_PRV_SetItemCount(p_object, R_OBJECT_TYPE_LIDAR_FRAME, count);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetLidarFrameIMP
 * @brief           Returns the physical address of the data plane of points contained in the lidar frame object
 * @param [in]      p_object - pointer to object
 * @retval          0 - on error, Otherwise the address of the plane of lidar points
 *********************************************************************************************************************/
uint32_t R_OBJECT_GetLidarFrameIMP(st_object_t * const p_object)
{
    return R_OBJECT_PRV_GetDataPlaneIMP(p_object, R_OBJECT_TYPE_LIDAR_FRAME);
}

/******************************************************************************************************************//**
 * Function Name:   R_OBJECT_GetLidarFrameCPU
 * @brief           Returns a pointer to the virtual memory of the plane of points contained in the lidar frame object
 * @param [in]      p_object - pointer to object
 * @retval          NULL - on error, Otherwise a pointer to the plane of lidar points
 *********************************************************************************************************************/
st_lidar_point_t * R_OBJECT_GetLidarFrameCPU(st_object_t * const p_object)
{
    return (st_lidar_point_t *)R_OBJECT_PRV_GetDataPlaneCPU(p_object, R_OBJECT_TYPE_LIDAR_FRAME);
}
