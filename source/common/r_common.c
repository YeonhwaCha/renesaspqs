/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common.c
 * @brief         Source file for common API handle initializations of all R_COMMON files
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include <string.h> /* For memcpy and memset */

#include "r_common.h"

/**********************************************************************************************************************
 Private global variables and functions
 *********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_Init
 * @brief           Initializes all of the R_COMMON files
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_Init(void)
{
    r_result_t return_code = R_RESULT_SUCCESS;

    return_code |= R_COMMON_HW_Init();
    return_code |= R_COMMON_USER_Init();
    return_code |= R_COMMON_LOG_Init();
    return_code |= R_COMMON_SENSOR_Init();

    return return_code;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_Deinit
 * @brief           Deinitializes all of the R_COMMON files
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_Deinit(void)
{
    r_result_t return_code = R_RESULT_SUCCESS;

    return_code |= R_COMMON_HW_Deinit();
    return_code |= R_COMMON_USER_Deinit();
    return_code |= R_COMMON_LOG_Deinit();

    return return_code;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_Clip
 * @brief           Clips input value to a minimum and maximum value.
 * @param [in]      value - value to clip
 * @param [in]      min - Minimum value
 * @param [in]      max - Maximum value
 * @retval          returns value if value is between min and max.
 *                  returns min + 1 if value is less than or equal to min.
 *                  returns max - 1 if value is greater than or equal to max.
 *********************************************************************************************************************/
int32_t R_COMMON_Clip(int32_t value, int32_t min, int32_t max)
{
    int32_t return_value = value;

    if (min >= value)
    {
        return_value = min + 1;
    }
    else if (max <= value)
    {
        return_value = max - 1;
    }
    else
    {
        /* MISRA */
    }

    return return_value;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_Memset
 * @brief           Wrapper for memset()
 * @param [in,out]  p_memory - pointer to the memory to set
 * @param [in]      value - value to set memory to
 * @param [in]      size - size of memory area to set
 *********************************************************************************************************************/
void R_COMMON_Memset(void * p_memory, int value, size_t size)
{
    (void)memset(p_memory, value, size);
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_Memcpy
 * @brief           Wrapper for memcpy()
 * @param [in,out]  p_dest - Pointer to source memory
 * @param [in]      p_source - Pointer to source memory
 * @param [in]      size - size of memory area to copy
 *********************************************************************************************************************/
void R_COMMON_Memcpy(void * p_dest, const void * p_source, size_t size)
{
    (void)memcpy(p_dest, p_source, size);
}


