/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_sensor.c
 * @brief         Source file for common API to allow for modules to request information about a given sensor,
 *                such as position, lens correction, and various homography parameters.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include "r_common.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/


#define SENSOR_PRV_MAX_NUM_SENSORS (4u) /*!< Number of sensors supported */

/*! @cond INTERNAL */
/* Correction coeficcients for 0V10640 Leopard camera */ //TODO I'm not sure these are correct
#define SENSOR_PRV_LEOPARD_OV10640_CORRECTION_COEFF_K1         (-5.66312546e-1f)
#define SENSOR_PRV_LEOPARD_OV10640_CORRECTION_COEFF_K2         (3.079635186e-1f)
#define SENSOR_PRV_LEOPARD_OV10640_CORRECTION_COEFF_K3         (0.0f)
#define SENSOR_PRV_LEOPARD_OV10640_CORRECTION_COEFF_FX         (1.410645e+3f)
#define SENSOR_PRV_LEOPARD_OV10640_CORRECTION_COEFF_FY         (1.411872e+3f)

/* Correction coeficcients for AR0231 Leopard camera */ //TODO These are definitely wrong
#define SENSOR_PRV_LEOPARD_AR0231_CORRECTION_COEFF_K1         (-5.66312546e-1f)
#define SENSOR_PRV_LEOPARD_AR0231_CORRECTION_COEFF_K2         (3.079635186e-1f)
#define SENSOR_PRV_LEOPARD_AR0231_CORRECTION_COEFF_K3         (0.0f)
#define SENSOR_PRV_LEOPARD_AR0231_CORRECTION_COEFF_FX         (1.410645e+3f)
#define SENSOR_PRV_LEOPARD_AR0231_CORRECTION_COEFF_FY         (1.411872e+3f)

/* Correction coeficcients for Lincoln front camera */
#define SENSOR_PRV_LINCOLN_FRONT_CORRECTION_COEFF_FX        (331.1243358764958f)
#define SENSOR_PRV_LINCOLN_FRONT_CORRECTION_COEFF_FY        (331.7693929471f)
#define SENSOR_PRV_LINCOLN_FRONT_CORRECTION_COEFF_CX        (636.1420518172945f)
#define SENSOR_PRV_LINCOLN_FRONT_CORRECTION_COEFF_CY        (564.7357992998905f)
#define SENSOR_PRV_LINCOLN_FRONT_CORRECTION_COEFF_K1        (-0.0028958493637192f)
#define SENSOR_PRV_LINCOLN_FRONT_CORRECTION_COEFF_K2        (0.04842845924870365f)
#define SENSOR_PRV_LINCOLN_FRONT_CORRECTION_COEFF_K3        (-0.006915166219080396f)

/* Correction coeficcients for Lincoln rear camera */
#define SENSOR_PRV_LINCOLN_REAR_CORRECTION_COEFF_FX        (328.2699110502015f) 
#define SENSOR_PRV_LINCOLN_REAR_CORRECTION_COEFF_FY        (329.2501271860483f)
#define SENSOR_PRV_LINCOLN_REAR_CORRECTION_COEFF_CX        (638.7751822542829f)
#define SENSOR_PRV_LINCOLN_REAR_CORRECTION_COEFF_CY        (563.5016604310838f)
#define SENSOR_PRV_LINCOLN_REAR_CORRECTION_COEFF_K1        (-0.00946841945931981f)
#define SENSOR_PRV_LINCOLN_REAR_CORRECTION_COEFF_K2        (0.05467381478268808f)
#define SENSOR_PRV_LINCOLN_REAR_CORRECTION_COEFF_K3        (-0.01140389431010589f)

/* Correction coeficcients for Lincoln left camera */
#define SENSOR_PRV_LINCOLN_LEFT_CORRECTION_COEFF_FX        (329.8398261319825f) 
#define SENSOR_PRV_LINCOLN_LEFT_CORRECTION_COEFF_FY        (330.2751378501101f)
#define SENSOR_PRV_LINCOLN_LEFT_CORRECTION_COEFF_CX        (631.8527289938351f)
#define SENSOR_PRV_LINCOLN_LEFT_CORRECTION_COEFF_CY        (556.0888757680858f)
#define SENSOR_PRV_LINCOLN_LEFT_CORRECTION_COEFF_K1        (-0.003609404742060879f)
#define SENSOR_PRV_LINCOLN_LEFT_CORRECTION_COEFF_K2        (0.04982777935457842f)
#define SENSOR_PRV_LINCOLN_LEFT_CORRECTION_COEFF_K3        (-0.007536436759268973f)

/* Correction coeficcients for Lincoln right camera */
#define SENSOR_PRV_LINCOLN_RIGHT_CORRECTION_COEFF_FX        (330.9547577941478f) 
#define SENSOR_PRV_LINCOLN_RIGHT_CORRECTION_COEFF_FY        (331.5205318676297f)
#define SENSOR_PRV_LINCOLN_RIGHT_CORRECTION_COEFF_CX        (636.3862045905068f)
#define SENSOR_PRV_LINCOLN_RIGHT_CORRECTION_COEFF_CY        (561.8497713715469f)
#define SENSOR_PRV_LINCOLN_RIGHT_CORRECTION_COEFF_K1        (-0.005672854797975636f)
#define SENSOR_PRV_LINCOLN_RIGHT_CORRECTION_COEFF_K2        (0.05335916196173407f)
#define SENSOR_PRV_LINCOLN_RIGHT_CORRECTION_COEFF_K3        (-0.01059938613017412f)


/* Homography for Buick regular -> birdeye*/
#define SENSOR_PRV_BUICK_HOMOGRAPHY_H00              (-9.23592970e-01f)
#define SENSOR_PRV_BUICK_HOMOGRAPHY_H01              ( 1.26478356e+00f)
#define SENSOR_PRV_BUICK_HOMOGRAPHY_H02              (-2.93899584e+02f)
#define SENSOR_PRV_BUICK_HOMOGRAPHY_H10              ( 5.35015059e-02f)
#define SENSOR_PRV_BUICK_HOMOGRAPHY_H11              ( 2.71506588e-02f)
#define SENSOR_PRV_BUICK_HOMOGRAPHY_H12              (-5.78893119e+01f)
#define SENSOR_PRV_BUICK_HOMOGRAPHY_H20              ( 1.28919291e-04f)
#define SENSOR_PRV_BUICK_HOMOGRAPHY_H21              ( 2.41264978e-03f)
#define SENSOR_PRV_BUICK_HOMOGRAPHY_H22              (-8.90604799e-01f)

/* Homography for Buick birdeye -> regular */
#define SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H00      (-1.24521424e+00f)
#define SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H01      (-4.49997436e+00f)
#define SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H02      ( 7.03419034e+02f)
#define SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H10      (-4.33296561e-01f)
#define SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H11      (-9.27764401e+00f)
#define SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H12      ( 7.46034726e+02f)
#define SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H20      (-1.35405175e-03f)
#define SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H21      (-2.57845447e-02f)
#define SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H22      ( 1.00000000e+00f)

/* Homography for Cadillac regular -> birdeye */
#define SENSOR_PRV_CADILLAC_HOMOGRAPHY_H00           (-6.44686192e+00f)
#define SENSOR_PRV_CADILLAC_HOMOGRAPHY_H01           ( 1.28972583e+01f)
#define SENSOR_PRV_CADILLAC_HOMOGRAPHY_H02           (-3.54690509e+03f)
#define SENSOR_PRV_CADILLAC_HOMOGRAPHY_H10           ( 4.04017937e-15f)
#define SENSOR_PRV_CADILLAC_HOMOGRAPHY_H11           ( 4.76516406e+00f)
#define SENSOR_PRV_CADILLAC_HOMOGRAPHY_H12           (-1.97613283e+03f)
#define SENSOR_PRV_CADILLAC_HOMOGRAPHY_H20           (-2.02851503e-18f)
#define SENSOR_PRV_CADILLAC_HOMOGRAPHY_H21           ( 1.98661716e-02f)
#define SENSOR_PRV_CADILLAC_HOMOGRAPHY_H22           (-7.23858181e+00f)

/* Homography for Cadillac birdeye -> regular */
#define SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H00   (-1.55114226e-01f)
#define SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H01   (-7.45252930e-01f)
#define SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H02   ( 2.79460021e+02f)
#define SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H10   (-1.08246745e-15f)
#define SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H11   (-1.51906245e+00f)
#define SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H12   ( 4.14704050e+02f)
#define SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H20   (-2.92734587e-18f)
#define SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H21   (-4.16904252e-03f)
#define SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H22   ( 1.00000000e+00f)

/* Homography for Lincoln regular -> birdeye */
#define SENSOR_PRV_LINCOLN_HOMOGRAPHY_H00            (-4.27924468e+00f)
#define SENSOR_PRV_LINCOLN_HOMOGRAPHY_H01            ( 9.68353535e+00f)
#define SENSOR_PRV_LINCOLN_HOMOGRAPHY_H02            (-2.64941062e+03f)
#define SENSOR_PRV_LINCOLN_HOMOGRAPHY_H10            (-4.04811161e-15f)
#define SENSOR_PRV_LINCOLN_HOMOGRAPHY_H11            ( 3.04954325e+00f)
#define SENSOR_PRV_LINCOLN_HOMOGRAPHY_H12            (-1.21121230e+03f)
#define SENSOR_PRV_LINCOLN_HOMOGRAPHY_H20            (-3.25295453e-17f)
#define SENSOR_PRV_LINCOLN_HOMOGRAPHY_H21            ( 1.89131550e-02f)
#define SENSOR_PRV_LINCOLN_HOMOGRAPHY_H22            (-6.51189408e+00f)

/* Homography for Lincoln birdeye -> regular */
#define SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H00    (-2.33686100e-01f)
#define SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H01    (-9.92314109e-01f)
#define SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H02    ( 2.79647283e+02f)
#define SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H10    (-9.99200722e-16f)
#define SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H11    (-2.13536702e+00f)
#define SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H12    ( 3.97178265e+02f)
#define SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H20    (-1.73472348e-18f)
#define SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H21    (-6.20196318e-03f)
#define SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H22    ( 1.00000000e+00f)
/*! @endcond */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! e_vehicle_t enum as a string array */
#define E_VEHICLE_AS_STRING        {                           \
                                        "R_VEHICLE_BUICK",     \
                                        "R_VEHICLE_CADILLAC",  \
                                        "R_VEHICLE_LINCOLN",   \
                                        "R_VEHICLE_MAX",       \
                                    }

/*! Enumeration of all the various vehicles used */
typedef enum {
    R_VEHICLE_BUICK,
    R_VEHICLE_CADILLAC,
    R_VEHICLE_LINCOLN,
    R_VEHICLE_MAX,
} e_vehicle_t;

/*! e_sensor_type_t enum as a string array */
#define E_SENSOR_TYPE_AS_STRING    {                              \
                                        "R_SENSOR_TYPE_CAMERA",   \
                                        "R_SENSOR_TYPE_LIDAR",    \
                                        "R_SENSOR_TYPE_MAX",      \
                                    }

/*! Enumeration of all the various sensor types */
typedef enum {
    R_SENSOR_TYPE_CAMERA,
    R_SENSOR_TYPE_LIDAR,
    R_SENSOR_TYPE_MAX,
} e_sensor_type_t;

/*! e_sensor_model_t enum as a string array */
#define E_SENSOR_MODEL_AS_STRING    {                                     \
                                        "R_SENSOR_MODEL_LEOPARD_OV10640", \
                                        "R_SENSOR_MODEL_LEOPARD_AR0231",  \
                                        "R_SENSOR_MODEL_HDL64",           \
                                        "R_SENSOR_MODEL_LINCOLN_FRONT",   \
                                        "R_SENSOR_MODEL_LINCOLN_REAR",    \
                                        "R_SENSOR_MODEL_LINCOLN_LEFT",    \
                                        "R_SENSOR_MODEL_LINCOLN_RIGHT",   \
                                        "R_SENSOR_MODEL_MAX",             \
                                    }

/*! Enumeration of all the various sensor modles used */
typedef enum {
    R_SENSOR_MODEL_LEOPARD_OV10640,
    R_SENSOR_MODEL_LEOPARD_AR0231,
    R_SENSOR_MODEL_HDL64,
    R_SENSOR_MODEL_LINCOLN_FRONT,
    R_SENSOR_MODEL_LINCOLN_REAR,
    R_SENSOR_MODEL_LINCOLN_LEFT,
    R_SENSOR_MODEL_LINCOLN_RIGHT,
    R_SENSOR_MODEL_MAX,
} e_sensor_model_t;

/*! Information about a specific sensor */
typedef struct {
    uint32_t         id;        /*!< ID of sensor */
    e_vehicle_t      vehicle;   /*!< Vehicle sensor is in */
    e_sensor_type_t  type;      /*!< Type of sensor */
    e_sensor_model_t model;     /*!< Model of sensor*/
    st_position_t    position;  /*!< Position of sensor */
} st_sensor_info_t;

static st_sensor_info_t s_sensor_list[SENSOR_PRV_MAX_NUM_SENSORS];  /*!< List of all the sensors used */
static const char * sp_vehicle_names[] = E_VEHICLE_AS_STRING;       /*!< Vehicle names as strings */
static const char * sp_type_names[]    = E_SENSOR_TYPE_AS_STRING;   /*!< Sensor types as strings */
static const char * sp_model_names[]   = E_SENSOR_MODEL_AS_STRING;  /*!< Sensor model as strings */

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_SENSOR_Init
 * @brief           Initializes the sensor array. Various properties about each sensor can be configured in a
 *                  configuration file.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_SENSOR_Init(void)
{
    /*lint --e{586} */

    r_result_t result = R_RESULT_SUCCESS;
    char sensor_type[16]  = {'\0'};
    char sensor_model[16] = {'\0'};
    char sensor_x[16]     = {'\0'};
    char sensor_y[16]     = {'\0'};
    char sensor_z[16]     = {'\0'};
    char sensor_roll[16]  = {'\0'};
    char sensor_pitch[16] = {'\0'};
    char sensor_yaw[16]   = {'\0'};

    /* Init sensor parameters */
    for (uint32_t sensor_index = 0u; sensor_index < SENSOR_PRV_MAX_NUM_SENSORS; sensor_index++)
    {
        /* Create the names for sensor properties based on sensor index */
        (void)snprintf(sensor_type,  sizeof(sensor_type),  "sensor_%u_type",  sensor_index);
        (void)snprintf(sensor_model, sizeof(sensor_model), "sensor_%u_model", sensor_index);
        (void)snprintf(sensor_x,     sizeof(sensor_x),     "sensor_%u_x",     sensor_index);
        (void)snprintf(sensor_y,     sizeof(sensor_y),     "sensor_%u_y",     sensor_index);
        (void)snprintf(sensor_z,     sizeof(sensor_z),     "sensor_%u_z",     sensor_index);
        (void)snprintf(sensor_roll,  sizeof(sensor_roll),  "sensor_%u_roll",  sensor_index);
        (void)snprintf(sensor_pitch, sizeof(sensor_pitch), "sensor_%u_pitch", sensor_index);
        (void)snprintf(sensor_yaw,   sizeof(sensor_yaw),   "sensor_%u_yaw",   sensor_index);

        /* Init sensor struct */
        s_sensor_list[sensor_index].id             = sensor_index;
        s_sensor_list[sensor_index].vehicle        = R_COMMON_CONFIG_GET_ENUM( "sensor", "vehicle",    R_VEHICLE_LINCOLN,              sp_vehicle_names, (sizeof(sp_vehicle_names))/(sizeof(sp_vehicle_names[0])), e_vehicle_t);      /*lint !e9030 !e9034 */
        s_sensor_list[sensor_index].type           = R_COMMON_CONFIG_GET_ENUM( "sensor", sensor_type,  R_SENSOR_TYPE_CAMERA,           sp_type_names,    (sizeof(sp_type_names))/(sizeof(sp_type_names[0])),       e_sensor_type_t);  /*lint !e9030 !e9034 */
        s_sensor_list[sensor_index].model          = R_COMMON_CONFIG_GET_ENUM( "sensor", sensor_model, R_SENSOR_MODEL_LEOPARD_OV10640, sp_model_names,   (sizeof(sp_model_names))/(sizeof(sp_model_names[0])),     e_sensor_model_t); /*lint !e9030 !e9034 */
        s_sensor_list[sensor_index].position.x     = (float)R_COMMON_CONFIG_GetDouble("sensor", sensor_x,     0.0);
        s_sensor_list[sensor_index].position.y     = (float)R_COMMON_CONFIG_GetDouble("sensor", sensor_y,     0.0);
        s_sensor_list[sensor_index].position.z     = (float)R_COMMON_CONFIG_GetDouble("sensor", sensor_z,     0.0);
        s_sensor_list[sensor_index].position.roll  = (float)R_COMMON_CONFIG_GetDouble("sensor", sensor_roll,  0.0);
        s_sensor_list[sensor_index].position.pitch = (float)R_COMMON_CONFIG_GetDouble("sensor", sensor_pitch, 0.0);
        s_sensor_list[sensor_index].position.yaw   = (float)R_COMMON_CONFIG_GetDouble("sensor", sensor_yaw,   0.0);
    }

    /* TODO Get sensor parameters from config file */

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_SENSOR_GetPosition
 * @brief           Fills in p_position struct for the given sensor id.
 * @param [in ]     sensor_id - Id of the sensor
 * @param [out]     p_position - pointer to st_position_t struct that will be filled with sensor position
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_SENSOR_GetPosition(uint32_t sensor_id, st_position_t * p_position)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (sensor_id >= SENSOR_PRV_MAX_NUM_SENSORS)
    {
        R_PRINT_ERROR("Invalid sensor ID");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        R_COMMON_Memcpy(p_position, &s_sensor_list[sensor_id].position, sizeof(st_position_t));
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_SENSOR_GetCorrection
 * @brief           Fills in p_correction struct for the given sensor id.
 * @param [in ]     sensor_id - Id of the sensor
 * @param [out]     p_correction - pointer to st_correction_t struct that will be filled with sensor correction info
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_SENSOR_GetCorrection(uint32_t sensor_id, st_correction_t * p_correction)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (sensor_id >= SENSOR_PRV_MAX_NUM_SENSORS)
    {
        R_PRINT_ERROR("Invalid sensor ID");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (R_SENSOR_TYPE_CAMERA != s_sensor_list[sensor_id].type)
        {
            R_PRINT_ERROR("Only sensor type camera supported. Type %s given", sp_type_names[s_sensor_list[sensor_id].type] );
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        switch (s_sensor_list[sensor_id].model)
        {
            case R_SENSOR_MODEL_LEOPARD_OV10640:
            {
                p_correction->k1 = SENSOR_PRV_LEOPARD_OV10640_CORRECTION_COEFF_K1;
                p_correction->k2 = SENSOR_PRV_LEOPARD_OV10640_CORRECTION_COEFF_K2;
                p_correction->k3 = SENSOR_PRV_LEOPARD_OV10640_CORRECTION_COEFF_K3;
                p_correction->fx = SENSOR_PRV_LEOPARD_OV10640_CORRECTION_COEFF_FX;
                p_correction->fy = SENSOR_PRV_LEOPARD_OV10640_CORRECTION_COEFF_FY;
                break;
            }

            case R_SENSOR_MODEL_LEOPARD_AR0231:
            {
                p_correction->k1 = SENSOR_PRV_LEOPARD_AR0231_CORRECTION_COEFF_K1;
                p_correction->k2 = SENSOR_PRV_LEOPARD_AR0231_CORRECTION_COEFF_K2;
                p_correction->k3 = SENSOR_PRV_LEOPARD_AR0231_CORRECTION_COEFF_K3;
                p_correction->fx = SENSOR_PRV_LEOPARD_AR0231_CORRECTION_COEFF_FX;
                p_correction->fy = SENSOR_PRV_LEOPARD_AR0231_CORRECTION_COEFF_FY;
                break;
            }

            case R_SENSOR_MODEL_LINCOLN_FRONT:
            {
                p_correction->k1  = SENSOR_PRV_LINCOLN_FRONT_CORRECTION_COEFF_K1;
                p_correction->k2  = SENSOR_PRV_LINCOLN_FRONT_CORRECTION_COEFF_K2;
                p_correction->k3  = SENSOR_PRV_LINCOLN_FRONT_CORRECTION_COEFF_K3;
                p_correction->fx  = SENSOR_PRV_LINCOLN_FRONT_CORRECTION_COEFF_FX;
                p_correction->fy  = SENSOR_PRV_LINCOLN_FRONT_CORRECTION_COEFF_FY;
                break;
            }

            case R_SENSOR_MODEL_LINCOLN_REAR:
            {
                p_correction->k1  = SENSOR_PRV_LINCOLN_REAR_CORRECTION_COEFF_K1;
                p_correction->k2  = SENSOR_PRV_LINCOLN_REAR_CORRECTION_COEFF_K2;
                p_correction->k3  = SENSOR_PRV_LINCOLN_REAR_CORRECTION_COEFF_K3;
                p_correction->fx  = SENSOR_PRV_LINCOLN_REAR_CORRECTION_COEFF_FX;
                p_correction->fy  = SENSOR_PRV_LINCOLN_REAR_CORRECTION_COEFF_FY;
                break;
            }

            case R_SENSOR_MODEL_LINCOLN_LEFT:
            {
                p_correction->k1  = SENSOR_PRV_LINCOLN_LEFT_CORRECTION_COEFF_K1;
                p_correction->k2  = SENSOR_PRV_LINCOLN_LEFT_CORRECTION_COEFF_K2;
                p_correction->k3  = SENSOR_PRV_LINCOLN_LEFT_CORRECTION_COEFF_K3;
                p_correction->fx  = SENSOR_PRV_LINCOLN_LEFT_CORRECTION_COEFF_FX;
                p_correction->fy  = SENSOR_PRV_LINCOLN_LEFT_CORRECTION_COEFF_FY;
                break;
            }

            case R_SENSOR_MODEL_LINCOLN_RIGHT:
            {
                p_correction->k1  = SENSOR_PRV_LINCOLN_RIGHT_CORRECTION_COEFF_K1;
                p_correction->k2  = SENSOR_PRV_LINCOLN_RIGHT_CORRECTION_COEFF_K2;
                p_correction->k3  = SENSOR_PRV_LINCOLN_RIGHT_CORRECTION_COEFF_K3;
                p_correction->fx  = SENSOR_PRV_LINCOLN_RIGHT_CORRECTION_COEFF_FX;
                p_correction->fy  = SENSOR_PRV_LINCOLN_RIGHT_CORRECTION_COEFF_FY;
                break;
            }

            case R_SENSOR_MODEL_HDL64:
            case R_SENSOR_MODEL_MAX:
            default:
            {
                R_PRINT_ERROR("Sensor model not supported");
                result = R_RESULT_FAILED;
                break;
            }
        } /*lint !e788 */
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_SENSOR_GetHomography
 * @brief           Fills in p_homography struct for the given sensor id.
 * @param [in ]     sensor_id - Id of the sensor
 * @param [out]     p_homography - pointer to st_homography_t struct that will be filled with sensor homography info
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_SENSOR_GetHomography(uint32_t sensor_id, st_homography_t * p_homography)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (sensor_id >= SENSOR_PRV_MAX_NUM_SENSORS)
    {
        R_PRINT_ERROR("Invalid sensor ID");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (R_SENSOR_TYPE_CAMERA != s_sensor_list[sensor_id].type)
        {
            R_PRINT_ERROR("Only sensor type camera supported. Type %s given", sp_type_names[s_sensor_list[sensor_id].type]);
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        switch (s_sensor_list[sensor_id].vehicle)
        {
            case R_VEHICLE_CADILLAC:
            {
                p_homography->h00 = SENSOR_PRV_CADILLAC_HOMOGRAPHY_H00;
                p_homography->h01 = SENSOR_PRV_CADILLAC_HOMOGRAPHY_H01;
                p_homography->h02 = SENSOR_PRV_CADILLAC_HOMOGRAPHY_H02;
                p_homography->h10 = SENSOR_PRV_CADILLAC_HOMOGRAPHY_H10;
                p_homography->h11 = SENSOR_PRV_CADILLAC_HOMOGRAPHY_H11;
                p_homography->h12 = SENSOR_PRV_CADILLAC_HOMOGRAPHY_H12;
                p_homography->h20 = SENSOR_PRV_CADILLAC_HOMOGRAPHY_H20;
                p_homography->h21 = SENSOR_PRV_CADILLAC_HOMOGRAPHY_H21;
                p_homography->h22 = SENSOR_PRV_CADILLAC_HOMOGRAPHY_H22;
                break;
            }

            case R_VEHICLE_BUICK:
            {
                p_homography->h00 = SENSOR_PRV_BUICK_HOMOGRAPHY_H00;
                p_homography->h01 = SENSOR_PRV_BUICK_HOMOGRAPHY_H01;
                p_homography->h02 = SENSOR_PRV_BUICK_HOMOGRAPHY_H02;
                p_homography->h10 = SENSOR_PRV_BUICK_HOMOGRAPHY_H10;
                p_homography->h11 = SENSOR_PRV_BUICK_HOMOGRAPHY_H11;
                p_homography->h12 = SENSOR_PRV_BUICK_HOMOGRAPHY_H12;
                p_homography->h20 = SENSOR_PRV_BUICK_HOMOGRAPHY_H20;
                p_homography->h21 = SENSOR_PRV_BUICK_HOMOGRAPHY_H21;
                p_homography->h22 = SENSOR_PRV_BUICK_HOMOGRAPHY_H22;
                break;
            }

            case R_VEHICLE_LINCOLN:
            {
                p_homography->h00 = SENSOR_PRV_LINCOLN_HOMOGRAPHY_H00;
                p_homography->h01 = SENSOR_PRV_LINCOLN_HOMOGRAPHY_H01;
                p_homography->h02 = SENSOR_PRV_LINCOLN_HOMOGRAPHY_H02;
                p_homography->h10 = SENSOR_PRV_LINCOLN_HOMOGRAPHY_H10;
                p_homography->h11 = SENSOR_PRV_LINCOLN_HOMOGRAPHY_H11;
                p_homography->h12 = SENSOR_PRV_LINCOLN_HOMOGRAPHY_H12;
                p_homography->h20 = SENSOR_PRV_LINCOLN_HOMOGRAPHY_H20;
                p_homography->h21 = SENSOR_PRV_LINCOLN_HOMOGRAPHY_H21;
                p_homography->h22 = SENSOR_PRV_LINCOLN_HOMOGRAPHY_H22;
                break;
            }

            case R_VEHICLE_MAX:
            default:
            {
                R_PRINT_ERROR("Vehicle not supported");
                result = R_RESULT_FAILED;
                break;
            }
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_SENSOR_GetInverseHomography
 * @brief           Fills in p_inverse_homography struct for the given sensor id.
 * @param [in ]     sensor_id - Id of the sensor
 * @param [out]     p_inverse_homography - pointer to st_homography_t struct that will be filled with sensor inverse
 *                  homography info
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_SENSOR_GetInverseHomography(uint32_t sensor_id, st_homography_t * p_inverse_homography)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (sensor_id >= SENSOR_PRV_MAX_NUM_SENSORS)
    {
        R_PRINT_ERROR("Invalid sensor ID");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (R_SENSOR_TYPE_CAMERA != s_sensor_list[sensor_id].type)
        {
            R_PRINT_ERROR("Only sensor type camera supported. Type %s given", sp_type_names[s_sensor_list[sensor_id].type] );
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        switch (s_sensor_list[sensor_id].vehicle)
        {
            case R_VEHICLE_CADILLAC:
            {
                p_inverse_homography->h00 = SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H00;
                p_inverse_homography->h01 = SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H01;
                p_inverse_homography->h02 = SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H02;
                p_inverse_homography->h10 = SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H10;
                p_inverse_homography->h11 = SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H11;
                p_inverse_homography->h12 = SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H12;
                p_inverse_homography->h20 = SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H20;
                p_inverse_homography->h21 = SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H21;
                p_inverse_homography->h22 = SENSOR_PRV_CADILLAC_INVERSE_HOMOGRAPHY_H22;
                break;
            }

            case R_VEHICLE_BUICK:
            {
                p_inverse_homography->h00 = SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H00;
                p_inverse_homography->h01 = SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H01;
                p_inverse_homography->h02 = SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H02;
                p_inverse_homography->h10 = SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H10;
                p_inverse_homography->h11 = SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H11;
                p_inverse_homography->h12 = SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H12;
                p_inverse_homography->h20 = SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H20;
                p_inverse_homography->h21 = SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H21;
                p_inverse_homography->h22 = SENSOR_PRV_BUICK_INVERSE_HOMOGRAPHY_H22;
                break;
            }

            case R_VEHICLE_LINCOLN:
            {
                p_inverse_homography->h00 = SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H00;
                p_inverse_homography->h01 = SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H01;
                p_inverse_homography->h02 = SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H02;
                p_inverse_homography->h10 = SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H10;
                p_inverse_homography->h11 = SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H11;
                p_inverse_homography->h12 = SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H12;
                p_inverse_homography->h20 = SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H20;
                p_inverse_homography->h21 = SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H21;
                p_inverse_homography->h22 = SENSOR_PRV_LINCOLN_INVERSE_HOMOGRAPHY_H22;
                break;
            }

            case R_VEHICLE_MAX:
            default:
            {
                R_PRINT_ERROR("Vehicle not supported");
                result = R_RESULT_FAILED;
                break;
            }
        }
    }

    return result;
}
