/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common.h
 * @brief         Header file for common API handle initializations of all R_COMMON files
 * @defgroup      PQS_Common PQS Common
 * @brief         Common API to be used by modules
 * @defgroup      PQS_Common_Core PQS Common Core
 * @section       PQS_Common_Summary Module Summary
 *                Common code that can be used by all modules.
 * @ingroup       PQS_Common
 * @{
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

#ifndef R_COMMON_H_
#define R_COMMON_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>

#include "r_typedefs.h"
#include "r_common_sensor.h"
#include "r_object.h"
#include "r_module.h"
#include "r_broker.h"
#include "r_timer.h"
#include "r_common_time.h"
#include "r_common_log.h"
#include "r_common_config.h"
#include "r_common_user.h"
#include "r_common_string.h"
#include "r_common_hw.h"
#include "r_common_json.h"

/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

#ifndef MIN
#define MIN(a, b)         (((a) < (b)) ? (a) : (b)) /*!< returns the min between a and b */
#endif

#ifndef MAX
#define MAX(a, b)         (((a) > (b)) ? (a) : (b))  /*!< returns the max between a and b */
#endif

#define CLEAR(x)          (R_COMMON_Memset(&(x), 0, sizeof(x))) /*!< Sets the memory of x to all zeros */

/**********************************************************************************************************************
 Typedef definitions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Exported global variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 *********************************************************************************************************************/

r_result_t R_COMMON_Init(void);
r_result_t R_COMMON_Deinit(void);
int32_t    R_COMMON_Clip(int32_t value, int32_t min, int32_t max);

void R_COMMON_Memset(void * p_memory, int value, size_t size);
void R_COMMON_Memcpy(void * p_dest, const void * p_source, size_t size);

#endif /* R_COMMON_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Common>
 *********************************************************************************************************************/

