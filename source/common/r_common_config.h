/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_config.h
 * @brief         Header file for common API to configure modules and other source code.
 * @defgroup      PQS_Common_Config PQS Common Config
 * @section       PQS_Common_Config_Summary Module Summary
 *                Allows for the configuration of modules and other source code. This module uses the iniparser
 *                from ndevilla. This software is license under MIT license. See lib/third_party/ndevilla/iniparser/LICENSE
 *                for license details. See <https://github.com/ndevilla/iniparser> for more information about
 *                the software itself.
 * @ingroup       PQS_Common
 * @{
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

#ifndef R_COMMON_CONFIG_H_
    #define R_COMMON_CONFIG_H_

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

/*! Does a cast of the int return value of R_COMMON_CONFIG_GetEnum() to the desired enum type to avoid compiler warnings */
#define R_COMMON_CONFIG_GET_ENUM(section, key, value, name, size, type)  \
        (type)R_COMMON_CONFIG_GetEnum((section), (key), (int)(value), (name), (size))

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global variables
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 **********************************************************************************************************************/
r_result_t   R_COMMON_CONFIG_Init(void);
r_result_t   R_COMMON_CONFIG_Deinit(void);
r_result_t   R_COMMON_CONFIG_AddFile(char const * const file_name);
uint32_t     R_COMMON_CONFIG_GetUint32(const char * const section, const char * const key, uint32_t     default_value);
int32_t      R_COMMON_CONFIG_GetInt32( const char * const section, const char * const key, int32_t      default_value);
double       R_COMMON_CONFIG_GetDouble(const char * const section, const char * const key, double       default_value);
bool         R_COMMON_CONFIG_GetBool(  const char * const section, const char * const key, bool         default_value);
const char * R_COMMON_CONFIG_GetString(const char * const section, const char * const key, const char * const default_string);
int          R_COMMON_CONFIG_GetEnum(  const char * const section, const char * const key, int          default_value, const char * p_enum_names[], uint32_t enum_name_size);

#endif /* R_COMMON_CONFIG_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Common>
 *********************************************************************************************************************/
