/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_json.c
 * @brief         Source file for common API to populate and save JSON files.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include "r_common.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define JSON_PRV_NUM_TO_STRING_BUFFER_SIZE   (32u) /*!< Size of the char array used to store numeric values converted to string. */
#define JSON_PRV_INDENTATION_LENGTH          (2u)  /*!< Number of spaces for a single indentation step adopted when writing a JSON file. */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   add_indentation_spaces
 * @brief           Appends the number of indentation spaces specified by the indentation argument to the p_json_data string.
 * @param [in,out]  p_json_data - Destination string where to append the indentation spaces.
 * @param [in]      json_data_size - Maximum length the destination string can hold including the NULL termination.
 * @param [in]      indentation - Number of indentation spaces to be added.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t add_indentation_spaces(char * p_json_data, size_t json_data_size, uint32_t indentation)
{
    r_result_t result = R_RESULT_SUCCESS;
    
    /* Check for null pointers */
    if (NULL == p_json_data)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to output JSON data buffer is NULL.");
    }
    
    if (R_RESULT_SUCCESS == result)
    {
        /* Keep appending indentation spaces to the JSON string  until the required number is reached */
        for (uint32_t indentation_index = 0; indentation_index < indentation; indentation_index++)
        {
            result |= R_COMMON_STRING_Combine(p_json_data, " ", json_data_size);
        }
    }
    if (R_RESULT_SUCCESS != result)
    {
        R_PRINT_ERROR("Error adding indentation spaces to JSON data.");
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   decrease_indentation
 * @brief           Diminishes the number of indentation spaces by the amount specified by JSON_PRV_INDENTATION_LENGTH if the result would be positive, otherwise returns an error.
 * @param [in,out]  p_indentation - Pointer to the indentation value to be modified.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t decrease_indentation(uint32_t * p_indentation)
{
    r_result_t result = R_RESULT_SUCCESS;
    
    /* Check for null pointers */
    if (NULL == p_indentation)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to indentation value is NULL.");
    }
    
    /* Decrease the indentation value by the amount JSON_PRV_INDENTATION_LENGTH if there are enough spaces to be removed.
     * Otherwise and error must be returned. */
    if (R_RESULT_SUCCESS == result)
    {
        if ((*p_indentation) < JSON_PRV_INDENTATION_LENGTH)
        {
            R_PRINT_ERROR("Can't decrease JSON indentation anymore.");
            result = R_RESULT_FAILED;
        }
        else
        {
            (*p_indentation) -= JSON_PRV_INDENTATION_LENGTH;
        }
    }
    
    return result;
}

/******************************************************************************************************************//**
 * Function Name:   process_json_data_buffer
 * @brief           Puts the main body of the JSON block content together with the opening and closing brackets and parses it to insert the proper amount of indentation spaces.
 *                  At the conclusion, the string pointed by the output parameter p_json_data is ready to be written to file.
 * @param [in,out]  p_json_data - Pointer to the char array for storing the JSON content before writing it to file.
 * @param [in]      json_data_size - Maximum length the char array pointed by p_json_data can hold including the NULL termination.
 * @param [in]      p_json_block - Pointer to the JSON block to be parsed.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t process_json_data_buffer(char * p_json_data, size_t json_data_size, st_json_block_t * p_json_block)
{
    r_result_t result = R_RESULT_SUCCESS;
    uint32_t json_buffer_index = 0;
    uint32_t current_indentation = 0; /* Represents the indentation spaces to be added to a new line after analyzing its first character. */
    uint32_t next_indentation = 0; /* Represents the indentation spaces to be added to the next line which could depend on the content of the current line. */
    char current_element[2] = {'\0'};
    bool indentation_needed = false; /* Specifies if indentation spaces must be added after the last occurrence of a new line character. */
    bool between_quotes = false; /* Specifies if the character currently evaluated is inside a pair of quotes. */
    
    /* Check for null pointers */
    if (NULL == p_json_data)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to JSON data buffer is NULL.");
    }
    
    if (NULL == p_json_block)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to JSON data block is NULL.");
    }
    
    if (R_RESULT_SUCCESS == result)
    {
        /* Copy the JSON block into the char array, starting with the header. */
        result |= R_COMMON_STRING_Copy(p_json_data, p_json_block->header, json_data_size);
        result |= R_COMMON_STRING_Combine(p_json_data, "\n", json_data_size);
        current_indentation = JSON_PRV_INDENTATION_LENGTH; /* After the first header there is always 1 indentation step to be added. */
        next_indentation = JSON_PRV_INDENTATION_LENGTH;
        indentation_needed = true;
        
        /* Parse each character */
        for (json_buffer_index = 0; json_buffer_index < (uint32_t)p_json_block->content_buffer_size; json_buffer_index++)
        {
            /* Make sure there is still enough space in the destination string */
            if (json_buffer_index >= (uint32_t)(json_data_size - 1u))
            {
                R_PRINT_ERROR("Not enough space in JSON data buffer.");
                result = R_RESULT_FAILED;
            }
            
            /* Content between quotes must be ingored. */
            if ('"' == p_json_block->p_content_buffer[json_buffer_index])
            {
                /* Toggle between_quotes */
                if (between_quotes)
                {
                    between_quotes = false;
                }
                else
                {
                    between_quotes = true;
                }
            }

            /* Opening brackets increase the indentation of the following lines. */
            if ((('{' == p_json_block->p_content_buffer[json_buffer_index]) || ('[' == p_json_block->p_content_buffer[json_buffer_index])) && (!between_quotes))
            {
                next_indentation += JSON_PRV_INDENTATION_LENGTH;
            }

            /* Closing brackets decrease the indentation from the current line (not just the following ones). */
            if (((']' == p_json_block->p_content_buffer[json_buffer_index]) || ('}' == p_json_block->p_content_buffer[json_buffer_index])) && (!between_quotes))
            {
                result |= decrease_indentation(&current_indentation);
                result |= decrease_indentation(&next_indentation);
            }

            /* The indentation spaces must be added only at the beginning of a new line, but after checking if the first characters is a bracket. */
            if (indentation_needed)
            {
                result |= add_indentation_spaces(p_json_data, json_data_size, current_indentation);

                /* Once the indentation spaces have been added, there is no need to do it anymore until a new line begins. */
                indentation_needed = false;
            }
            
            /* After checking if the current character is a bracket and eventually adding the proper indentation spaces, the current character can be appended to the destination string.*/
            current_element[0] = p_json_block->p_content_buffer[json_buffer_index];
            result |= R_COMMON_STRING_Combine(p_json_data, current_element, json_data_size);
            
            /* If the end of the line is reached */
            if ('\n' == p_json_block->p_content_buffer[json_buffer_index])
            {
                /* A new bunch of indentation spaces must be added again at the beginning of the next one. */
                indentation_needed = true;
                current_indentation = next_indentation;
            }
            if ((R_RESULT_FAILED == result) || ('\0' == p_json_block->p_content_buffer[json_buffer_index]))
            {
                /* If something went wrong or the end of the JSON content has been reached, the loop can be exited. */
                break;
            }
        }

        /* Add the last closing bracket. */
        result |= R_COMMON_STRING_Combine(p_json_data, "\n", json_data_size);
        result |= R_COMMON_STRING_Combine(p_json_data, p_json_block->footer, json_data_size);
    }
    
    if (R_RESULT_SUCCESS != result)
    {
        R_PRINT_ERROR("Error while processing JSON data buffer.");
    }
    
    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_JSON_InitUnnamedBlock
 * @brief           Initializes the JSON block by resetting it and assign the pointer to the char array for storing its content.
 *                  No name is added to the header since this is an unnamed JSON block.
 *                  This kind of block could represent the most external container for the entire JSON structure or the element of an array.
 *                  Any content already present in the char array pointed by p_content_buffer is lost.
 * @param [in,out]  p_json_block - Pointer to the JSON block to be initialized.
 * @param [in]      p_content_buffer - Pointer to the char array for storing the JSON block content.
 * @param [in]      content_buffer_size - Maximum length the char array pointed by p_content_buffer can hold including the NULL termination.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_JSON_InitUnnamedBlock(st_json_block_t * p_json_block, char * p_content_buffer, size_t content_buffer_size)
{
    r_result_t result = R_RESULT_SUCCESS;
    
    /* Check for null pointers */
    if (NULL == p_json_block)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to JSON data block is NULL.");
    }
    
    if (NULL == p_content_buffer)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to JSON content buffer is NULL.");
    }
    
    if (R_RESULT_SUCCESS == result)
    {
        /* Reset the JSON block, no name added to the header. */
        result |= R_COMMON_STRING_Copy(p_json_block->header, "{", R_JSON_BLOCK_HEADER_LENGTH);
        result |= R_COMMON_STRING_Copy(p_json_block->footer, "}", R_JSON_BLOCK_FOOTER_LENGTH);
        p_json_block->p_content_buffer = p_content_buffer;
        p_json_block->p_content_buffer[0] = '\0'; /* Reset buffer, any previous content is lost.*/
        p_json_block->content_buffer_size = content_buffer_size;
        p_json_block->is_empty = true;
    }
    
    if (R_RESULT_SUCCESS != result)
    {
        R_PRINT_ERROR("Error setting unnamed JSON block.");
    }
    
    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_JSON_InitNamedBlock
 * @brief           Initializes the JSON block by resetting it and assign the pointer to the char array for storing its content.
 *                  The specified name is added to the block header. This kind of block represents a container for a set of other entries or blocks.
 *                  Any content already present in the char array pointed by p_content_buffer is lost.
 * @param [in,out]  p_json_block - Pointer to the JSON block to be initialized.
 * @param [in]      p_block_name - Pointer to the char array containing the name of the JSON block.
 * @param [in]      p_content_buffer - Pointer to the char array for storing the JSON block content.
 * @param [in]      content_buffer_size - Maximum length the char array pointed by p_content_buffer can hold including the NULL termination.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_JSON_InitNamedBlock(st_json_block_t * p_json_block, const char * const p_block_name, char *  p_content_buffer, size_t content_buffer_size)
{
    r_result_t result = R_RESULT_SUCCESS;
    
    /* Check for null pointers */
    if (NULL == p_json_block)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to JSON data block is NULL.");
    }
    
    if (NULL == p_block_name)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to JSON block name is NULL.");
    }
    
    if (NULL == p_content_buffer)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to JSON content buffer is NULL.");
    }
    
    if (R_RESULT_SUCCESS == result)
    {
        /* Reset the JSON block, the specified name is added to the header. */
        result |= R_COMMON_STRING_Copy(p_json_block->header , "\"", R_JSON_BLOCK_HEADER_LENGTH);
        result |= R_COMMON_STRING_Combine(p_json_block->header , p_block_name, R_JSON_BLOCK_HEADER_LENGTH);
        result |= R_COMMON_STRING_Combine(p_json_block->header , "\": {", R_JSON_BLOCK_HEADER_LENGTH);
        result |= R_COMMON_STRING_Copy(p_json_block->footer , "}", R_JSON_BLOCK_FOOTER_LENGTH);
        p_json_block->p_content_buffer = p_content_buffer;
        p_json_block->p_content_buffer[0] = '\0'; /* Reset buffer, any previous content is lost.*/
        p_json_block->content_buffer_size = content_buffer_size;
        p_json_block->is_empty = true;
    }
    
    if (R_RESULT_SUCCESS != result)
    {
        R_PRINT_ERROR("Error setting named JSON block.");
    }
    
    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_JSON_InitArrayBlock
 * @brief           Initializes the JSON block by resetting it and assign the pointer to the char array for storing its content.
 *                  The specified name is added to the block header. This kind of block represents an array of JSON blocks.
 *                  Any content already present in the char array pointed by p_content_buffer is lost.
 * @param [in,out]  p_json_block - Pointer to the JSON block to be initialized.
 * @param [in]      p_array_name - Pointer to the char array containing the name of the JSON block.
 * @param [in]      p_content_buffer - Pointer to the char array for storing the JSON block content.
 * @param [in]      content_buffer_size - Maximum length the char array pointed by p_content_buffer can hold including the NULL termination.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_JSON_InitArrayBlock(st_json_block_t * p_json_block, const char * const p_array_name, char * p_content_buffer, size_t content_buffer_size)
{
    r_result_t result = R_RESULT_SUCCESS;
    
    /* Check for null pointers */
    if (NULL == p_json_block)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to JSON data block is NULL.");
    }
    
    if (NULL == p_array_name)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to JSON array name is NULL.");
    }
    
    if (NULL == p_content_buffer)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to JSON content buffer is NULL.");
    }
    
    if (R_RESULT_SUCCESS == result)
    {
        /* Reset the JSON block, the specified name is added to the header. */
        result |= R_COMMON_STRING_Copy(p_json_block->header , "\"", R_JSON_BLOCK_HEADER_LENGTH);
        result |= R_COMMON_STRING_Combine(p_json_block->header , p_array_name, R_JSON_BLOCK_HEADER_LENGTH);
        result |= R_COMMON_STRING_Combine(p_json_block->header , "\": [", R_JSON_BLOCK_HEADER_LENGTH);
        result |= R_COMMON_STRING_Copy(p_json_block->footer , "]", R_JSON_BLOCK_FOOTER_LENGTH);
        p_json_block->p_content_buffer = p_content_buffer;
        p_json_block->p_content_buffer[0] = '\0'; /* Reset buffer, any previous content is lost.*/
        p_json_block->content_buffer_size = content_buffer_size;
        p_json_block->is_empty = true;
    }
    
    if (R_RESULT_SUCCESS != result)
    {
        R_PRINT_ERROR("Error setting JSON array block.");
    }
    
    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_JSON_AddBlockContent
 * @brief           Inserts the content of the source JSON block (including its brackets and its name, if present) into the body of the destination JSON block.
 *                  The data is appended to the content that was eventually already present inside the destination block.
 *                  This operation can't be undone without resetting the entire destination block and it is assumed that the two blocks have already been initialized.
 * @param [in,out]  p_dest_block - Pointer to the destination JSON block.
 * @param [in]      p_source_block - Pointer to the source JSON block.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_JSON_AddBlockContent(st_json_block_t * p_dest_block, st_json_block_t * p_source_block)
{
    r_result_t result = R_RESULT_SUCCESS;
    
    /* Check for null pointers */
    if (NULL == p_dest_block)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to output JSON data block is NULL.");
    }
    
    if (NULL == p_source_block)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to input JSON data block is NULL.");
    }
    
    if (R_RESULT_SUCCESS == result)
    {   
        if (p_dest_block->is_empty)
        {
            /* The JSON block is not empty anymore since content is being added. */
            p_dest_block->is_empty = false;
        }
        else
        {
            /* If the JSON block already contained other elements, a comma must be added to the last one before inserting the new one. */
            result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, ",\n", p_dest_block->content_buffer_size);
        }

        /* Copy the entire source block */
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, p_source_block->header, p_dest_block->content_buffer_size);
        if (false == p_source_block->is_empty)
        {
            /* New lines must be added between a pair of brackets only if they contain something. */
            result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, "\n", p_dest_block->content_buffer_size);
            result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, p_source_block->p_content_buffer, p_dest_block->content_buffer_size);
            result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, "\n", p_dest_block->content_buffer_size);
        }
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, p_source_block->footer, p_dest_block->content_buffer_size);
    }
    
    if (R_RESULT_SUCCESS != result)
    {
        R_PRINT_ERROR("Error adding JSON block content.");
    }
    
    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_JSON_AddStrEntry
 * @brief           Inserts an entry into the destination JSON block. The entry is represented by a key-value pair where the value is a string.
 *                  The data is appended to the content that was eventually already present inside the destination block.
 *                  This operation can't be undone without resetting the entire destination block and it is assumed that the destination block has already been initialized.
 * @param [in,out]  p_dest_block - Pointer to the destination JSON block.
 * @param [in]      p_key - Pointer to the char array storing the string key.
 * @param [in]      p_value - Pointer to the char array storing the string value.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_JSON_AddStrEntry(st_json_block_t * p_dest_block, const char * const p_key, const char * const p_value)
{
    r_result_t result = R_RESULT_SUCCESS;
    
    /* Check for null pointers */
    if (NULL == p_dest_block)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to output JSON data block is NULL.");
    }
    
    if (NULL == p_key)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to JSON key is NULL.");
    }
    
    if (NULL == p_value)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to JSON value is NULL.");
    }
    
    if (R_RESULT_SUCCESS == result)
    {  
        if (p_dest_block->is_empty)
        {
            /* The JSON block is not empty anymore since content is being added. */
            p_dest_block->is_empty = false;
        }
        else
        {
            /* If the JSON block already contained other elements, a comma must be added to the last one before inserting the new one. */
            result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, ",\n", p_dest_block->content_buffer_size);
        }

        /* Copy the key-value pair */
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, "\"",     p_dest_block->content_buffer_size);
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, p_key,    p_dest_block->content_buffer_size);
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, "\": \"", p_dest_block->content_buffer_size);
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, p_value,  p_dest_block->content_buffer_size);
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, "\"",     p_dest_block->content_buffer_size);
    }
    
    if (R_RESULT_SUCCESS != result)
    {
        R_PRINT_ERROR("Error adding JSON string entry.");
    }
    
    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_JSON_AddIntEntry
 * @brief           Inserts an entry into the destination JSON block. The entry is represented by a key-value pair where the value is a signed integer.
 *                  The data is appended to the content that was eventually already present inside the destination block.
 *                  This operation can't be undone without resetting the entire destination block and it is assumed that the destination block has already been initialized.
 * @param [in,out]  p_dest_block - Pointer to the destination JSON block.
 * @param [in]      p_key - Pointer to the char array storing the string key.
 * @param [in]      value - The integer value of the JSON entry.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_JSON_AddIntEntry(st_json_block_t * p_dest_block, const char * const p_key, int32_t value)
{
    r_result_t result = R_RESULT_SUCCESS;
    char num_value_buffer[JSON_PRV_NUM_TO_STRING_BUFFER_SIZE] = {'\0'};
    
    /* Check for null pointers */
    if (NULL == p_dest_block)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to output JSON data block is NULL.");
    }
    
    if (NULL == p_key)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to JSON key is NULL.");
    }
    
    if (R_RESULT_SUCCESS == result)
    {  
        if (p_dest_block->is_empty)
        {
            /* The JSON block is not empty anymore since content is being added. */
            p_dest_block->is_empty = false;
        }
        else
        {
            /* If the JSON block already contained other elements, a comma must be added to the last one before inserting the new one. */
            result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, ",\n", p_dest_block->content_buffer_size);
        }

        /* Copy the key */
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, "\"",   p_dest_block->content_buffer_size);
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, p_key,  p_dest_block->content_buffer_size);
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, "\": ", p_dest_block->content_buffer_size);
        
        /* Convert the numeric value to string */
        int32_t written = snprintf(num_value_buffer, sizeof(num_value_buffer), "%d", value); /*lint !e586 */

        if ((int32_t)JSON_PRV_NUM_TO_STRING_BUFFER_SIZE <= written)
        {
            R_PRINT_ERROR("Error writing data -- too large");
            result = R_RESULT_FAILED;
        }
        
        /* Copy the value */
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, num_value_buffer, p_dest_block->content_buffer_size);
    }
    
    if (R_RESULT_SUCCESS != result)
    {
        R_PRINT_ERROR("Error adding JSON int entry.");
    }
    
    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_JSON_AddFloatEntry
 * @brief           Inserts an entry into the destination JSON block. The entry is represented by a key-value pair where the value is a float.
 *                  The data is appended to the content that was eventually already present inside the destination block.
 *                  This operation can't be undone without resetting the entire destination block and it is assumed that the destination block has already been initialized.
 * @param [in,out]  p_dest_block - Pointer to the destination JSON block.
 * @param [in]      p_key - Pointer to the char array storing the string key.
 * @param [in]      value - The float value of the JSON entry.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_JSON_AddFloatEntry(st_json_block_t * p_dest_block, const char * const p_key, float value)
{
    r_result_t result = R_RESULT_SUCCESS;
    char num_value_buffer[JSON_PRV_NUM_TO_STRING_BUFFER_SIZE] = {'\0'};
    
    /* Check for null pointers */
    if (NULL == p_dest_block)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to output JSON data block is NULL.");
    }
    
    if (NULL == p_key)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to JSON key is NULL.");
    }
    
    if (R_RESULT_SUCCESS == result)
    {  
        if (p_dest_block->is_empty)
        {
            /* The JSON block is not empty anymore since content is being added. */
            p_dest_block->is_empty = false;
        }
        else
        {
            /* If the JSON block already contained other elements, a comma must be added to the last one before inserting the new one. */
            result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, ",\n", p_dest_block->content_buffer_size);
        }

        /* Copy the key */
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, "\"",   p_dest_block->content_buffer_size);
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, p_key,  p_dest_block->content_buffer_size);
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, "\": ", p_dest_block->content_buffer_size);
        
        /* Convert the numeric value to string */
        int32_t written = snprintf(num_value_buffer, sizeof(num_value_buffer), "%f", value); /*lint !e586 */

        if ((int32_t)JSON_PRV_NUM_TO_STRING_BUFFER_SIZE <= written)
        {
            R_PRINT_ERROR("Error writing data -- too large");
            result = R_RESULT_FAILED;
        }
        
        /* Copy the value */
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, num_value_buffer, p_dest_block->content_buffer_size);
    }
    
    if (R_RESULT_SUCCESS != result)
    {
        R_PRINT_ERROR("Error adding JSON float entry.");
    }
    
    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_JSON_AddBoolEntry
 * @brief           Inserts an entry into the destination JSON block. The entry is represented by a key-value pair where the value is boolean.
 *                  The data is appended to the content that was eventually already present inside the destination block.
 *                  This operation can't be undone without resetting the entire destination block and it is assumed that the destination block has already been initialized.
 * @param [in,out]  p_dest_block - Pointer to the destination JSON block.
 * @param [in]      p_key - Pointer to the char array storing the string key.
 * @param [in]      value - The boolean value of the JSON entry.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_JSON_AddBoolEntry(st_json_block_t * p_dest_block, const char * const p_key, bool value)
{
    r_result_t result = R_RESULT_SUCCESS;
    
    /* Check for null pointers */
    if (NULL == p_dest_block)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to output JSON data block is NULL.");
    }
    
    if (NULL == p_key)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to JSON key is NULL.");
    }
    
    if (R_RESULT_SUCCESS == result)
    {  
        if (p_dest_block->is_empty)
        {
            /* The JSON block is not empty anymore since content is being added. */
            p_dest_block->is_empty = false;
        }
        else
        {
            /* If the JSON block already contained other elements, a comma must be added to the last one before inserting the new one. */
            result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, ",\n", p_dest_block->content_buffer_size);
        }

        /* Copy the key-value pair */
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, "\"",   p_dest_block->content_buffer_size);
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, p_key,  p_dest_block->content_buffer_size);
        result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, "\": ", p_dest_block->content_buffer_size);

        if (value)
        {
            result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, "true", p_dest_block->content_buffer_size);
        }
        else
        {
            result |= R_COMMON_STRING_Combine(p_dest_block->p_content_buffer, "false", p_dest_block->content_buffer_size);
        }
    }
    
    if (R_RESULT_SUCCESS != result)
    {
        R_PRINT_ERROR("Error adding JSON boolean entry.");
    }
    
    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_COMMON_JSON_SaveToFile
 * @brief           Process the input JSON block into a string and saves it to file.
 *                  The maximum size of the JSON content that can be handled is limited by R_JSON_DATA_BUFFER_SIZE.
 * @param [in,out]  p_json_block - Pointer to the JSON block to be written to file.
 * @param [in]      p_file_path - Pointer to the char array specifying the path and name of the destination JSON file.
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t R_COMMON_JSON_SaveToFile(st_json_block_t * p_json_block, const char * const p_file_path)
{
    r_result_t result = R_RESULT_SUCCESS;
    char json_data[R_JSON_DATA_BUFFER_SIZE] = {'\0'};
    
    /* Check for null pointers */
    if (NULL == p_json_block)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to JSON data is NULL.");
    }

    if (NULL == p_file_path)
    {
        /* Error NULL pointer */
        result = R_RESULT_ILLEGAL_NULL_POINTER;
        R_PRINT_ERROR("Pointer to file path is NULL.");
    }
    
    if (R_RESULT_SUCCESS == result)
    {   
        /* Put the entire JSON data into a string and add indentation spaces. */
        result |= process_json_data_buffer(json_data, R_JSON_DATA_BUFFER_SIZE, p_json_block);
    }
    
    if (R_RESULT_SUCCESS == result)
    {
        /* Open the destination file. */
        FILE * p_file = fopen(p_file_path, "w"); /*lint !e586 !e668 */

        if (NULL == p_file)
        {
            R_PRINT_ERROR("Couldn't open file.");
            result = R_RESULT_FAILED;
        }
        else
        {
            /* Write the JSON content to file. */
            if (fprintf(p_file, "%s", json_data) < 0)/*lint !e586 !e668 */ //TODO json string is not formatted so we can use fwrite() here
            {
                R_PRINT_ERROR("Couldn't write file.");
                result = R_RESULT_FAILED;
            }

            /* Close file */
            if (fclose(p_file) != 0) /*lint !e586  */
            {
                R_PRINT_ERROR("Couldn't close file.");
                result = R_RESULT_FAILED;
            }
        }
    } /*lint !e593  */
    
    if (R_RESULT_SUCCESS != result)
    {
        R_PRINT_ERROR("Error while saving JSON file.");
    }
    
    return result;
}
