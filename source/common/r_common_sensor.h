/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_sensor.h
 * @brief         Header file for common API to allow for modules to request information about a given sensor,
 *                such as position, lens correction, and various homography parameters.
 * @defgroup      PQS_Common_Sensor PQS Common Sensor
 * @section       PQS_Common_Sensor_Summary Module Summary
 *                Allows for modules to request information about a given sensor, such as position, lens correction,
 *                and various homography parameters.
 * @ingroup       PQS_Common
 * @{
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

#ifndef R_COMMON_SENSOR_H_
    #define R_COMMON_SENSOR_H_

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Position of a sensor on a vehicle from a reference point (e.g. center of back bumper) */
typedef struct {
    float x;     /*!< Lateral distance in meters from reference point */
    float y;     /*!< Forward distance in meters from reference point */
    float z;     /*!< Vertical distance in meters from reference point */
    float roll;  /*!< Roll in degrees */
    float pitch; /*!< Pitch in degrees */
    float yaw;   /*!< Yaw in degrees */
} st_position_t;

/*! Correction parameters of a camera lens */
typedef struct {
    float k1;  /*!< k1 value */
    float k2;  /*!< k2 value */
    float k3;  /*!< k3 value */
    float fx;  /*!< fx value */
    float fy;  /*!< fy value */
} st_correction_t;

/*! Homography parameters of a particular camera and warping model */
typedef struct {
    float h00; /*!< h00 value */
    float h01; /*!< h01 value */
    float h02; /*!< h02 value */
    float h10; /*!< h10 value */
    float h11; /*!< h11 value */
    float h12; /*!< h12 value */
    float h20; /*!< h20 value */
    float h21; /*!< h21 value */
    float h22; /*!< h22 value */
} st_homography_t;

/***********************************************************************************************************************
 Exported global variables
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 **********************************************************************************************************************/

r_result_t R_COMMON_SENSOR_Init(void);
r_result_t R_COMMON_SENSOR_GetPosition(uint32_t sensor_id,   st_position_t *   p_position);
r_result_t R_COMMON_SENSOR_GetCorrection(uint32_t sensor_id, st_correction_t * p_correction);
r_result_t R_COMMON_SENSOR_GetHomography(uint32_t sensor_id, st_homography_t * p_homography);
r_result_t R_COMMON_SENSOR_GetInverseHomography(uint32_t sensor_id, st_homography_t * p_inverse_homography);
#endif /* R_COMMON_SENSOR_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Common>
 *********************************************************************************************************************/

