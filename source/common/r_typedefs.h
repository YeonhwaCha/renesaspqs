/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_typedefs.h
 * @brief         Header file for common typedefs
 * @defgroup      PQS_Common_Typedefs PQS Common Typedefs
 * @section       PQS_Common_Typedefs_Summary Module Summary
 *                Common Typedefs
 * @ingroup       PQS_Common
 * @{
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

#ifndef R_TYPEDEFS_H_
    #define R_TYPEDEFS_H_

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define R_PI        (3.14159f) /*!< Pi */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Enum used as return type of most functions */
typedef enum
{
    R_RESULT_SUCCESS               = 0x00, /*!< Success */
    R_RESULT_FAILED                = 0x01, /*!< General failure */
    R_RESULT_BAD_INPUT_ARGUMENTS   = 0x02, /*!< Bad arguments/attributes */
    R_RESULT_ILLEGAL_NULL_POINTER  = 0x03, /*!< NULL pointer not allowed */
    R_RESULT_INVALID_STATE         = 0x04, /*!< State not allowed */
    R_RESULT_UNKNOWN               = 0xFF  /*!< Default value to set before proper values have been set */
} r_result_t;

#endif /* R_TYPEDEFS_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Common>
 *********************************************************************************************************************/
