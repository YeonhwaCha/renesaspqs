/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_common_hw.h
 * @brief         Header file for common API for abstraction of hardware interactions.
 * @defgroup      PQS_Common_Hw PQS Common Hardware
 * @section       PQS_Common_Hw_Summary Module Summary
 *                Abstraction for hardware interactions.
 * @ingroup       PQS_Common
 * @{
 *********************************************************************************************************************/
/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

#ifndef R_COMMON_HW_H_
#define R_COMMON_HW_H_

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include "rcv_impdrv.h" /* Needed for RCvIMPDRVCOREINFO */

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define R_MAX_IMP_CORES        (5u) /*!< Number of IMP cores: 5       @anchor R_MAX_IMP_CORES */
#define R_MAX_CVE_CORES        (5u) /*!< Number of CVe (OCV) cores: 5 @anchor R_MAX_CVE_CORES */
#define R_MAX_DMA_CORES        (2u) /*!< Number of DMA cores: 2       @anchor R_MAX_DMA_CORES */
#define R_MAX_PSCOUT_CORES     (4u) /*!< Number of PSC out cores: 4   @anchor R_MAX_PSCOUT_CORES */
#define R_MAX_PSCEXE_CORES     (1u) /*!< Number of PSC exe cores: 1   @anchor R_MAX_PSCEXE_CORES */
#define R_MAX_CNN_CORES        (1u) /*!< Number of CNN cores: 1       @anchor R_MAX_CNN_CORES*/
#define R_MAX_IMR_CHANNELS     (6u) /*!< Number of IMR channels: 6    @anchor R_MAX_IMR_CHANNELS */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global functions (to be accessed by other files)
 **********************************************************************************************************************/

extern r_result_t R_COMMON_HW_Init(void);
extern r_result_t R_COMMON_HW_Deinit(void);
extern r_result_t R_COMMON_HW_ExecuteIMP(uint32_t command_list_addr, RCvIMPDRVCOREINFO * p_core_info);
extern r_result_t R_COMMON_HW_WaitIMP(RCvIMPDRVCOREINFO * p_core_info);

#endif /* R_COMMON_HW_H_ */

/******************************************************************************************************************//**
 * @} end group <PQS_Common>
 *********************************************************************************************************************/
