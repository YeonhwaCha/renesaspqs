/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_semseg_color_cve.c
 * @brief         Source file for coloring a UYVA image based upon a semseg enum. See @ref PQS_Semseg_Color_Cve for
 *                more details
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @page       PQS_Semseg_Color_Cve_Algorithm  PQS Semseg Color CVe Algorithm
 * @ingroup    PQS_Semseg_Color_Cve
 * # Algorithm Description:
 *             This module takes in a semantic segmentation enum image and colors in a UYVA image based upon the
 *             classes. The coloring is done only on every other pixel. Y values of every pixel will not be changed.
 *             This module uses CVe.
 * # Block Diagram:
 *             @image html semseg_color_cve.png
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include "r_common.h"
#include "r_object_image.h"

#include "r_phantom_color_cve.h"
#include "r_phantom_color.cv.h"

#include "generic_api_memory.h"
#include "r_atmlib_prot.h"
#include "r_extatm_ocv.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define COLOR_PRV_CHANNEL_SEMSEG_IN      (0u) /*!< Broker channel for semantic segmentation enum input */
#define COLOR_PRV_CHANNEL_UYVA_INOUT     (1u) /*!< Broker channel for UYVA image input output*/

#define COLOR_PRV_CL_SIZE                (256u * 4u) /*!< Size of command list to allocate */

#define COLOR_PRV_SEMSEG_DATA_PLANE      (0u) /*!< CVe program plane index for semseg enum */
#define COLOR_PRV_UYVA_DATA_PLANE        (1u) /*!< CVe program plane index for uyva image */
#define COLOR_PRV_NUM_DATA_PLANES        (2u) /*!< CVe program number of total planes */

#define COLOR_PRV_DEFAULT_IMAGE_HEIGHT   (512u)  /*!< Default height of the image */
#define COLOR_PRV_DEFAULT_IMAGE_WIDTH    (1024u) /*!< Default width of the image */

#define COLOR_PRV_DEFAULT_START_CORE     (0u) /*!< Default core to start with */
#define COLOR_PRV_DEFAULT_END_CORE       (1u) /*!< Default core to end with */

#define COLOR_PRV_DEFAULT_BACKGROUND_COLOR    (0x8080) /*!< Hex value for color for background class. Upper byte U. Lower byte V. */
#define COLOR_PRV_DEFAULT_ROAD_COLOR          (0x2C16) /*!< Hex value for color for road class. Upper byte U. Lower byte V. */
#define COLOR_PRV_DEFAULT_LANE_COLOR          (0x54FF) /*!< Hex value for color for lane class. Upper byte U. Lower byte V. */
#define COLOR_PRV_DEFAULT_CURB_COLOR          (0x8080) /*!< Hex value for color for curb class. Upper byte U. Lower byte V. */
#define COLOR_PRV_DEFAULT_VEHICLE_COLOR       (0xFF6B) /*!< Hex value for color for vehicle class. Upper byte U. Lower byte V. */
#define COLOR_PRV_DEFAULT_OTHER_MARKING_COLOR (0xE5B8) /*!< Hex value for color for other road markings class. Upper byte U. Lower byte V. */
#define COLOR_PRV_DEFAULT_SIDEWALK_COLOR      (0x8080) /*!< Hex value for color for sidewalk class. Upper byte U. Lower byte V. */
#define COLOR_PRV_DEFAULT_PEDESTRIAN_COLOR    (0x54FF) /*!< Hex value for color for pedestrian class. Upper byte U. Lower byte V. */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Data needed for CVe program execution */
typedef struct {
    R_ATMLIB_CLData          cl_data;                                /*!< This is command list data */
    R_ATMLIB_OCVPlaneParam   plane_param[COLOR_PRV_NUM_DATA_PLANES]; /*!< Plane param array. One param for each data plane */
    R_ATMLIB_OCVUniformParam uniform_param;                          /*!< Uniform param */
    uint32_t *               p_prog_mem;                             /*!< Pointer to allocated program memory */
} st_cve_data_t;

/*! Configuration structure for semseg_color_cve module */
typedef struct {
    uint32_t   height;           /*!< Height of image */
    uint32_t   width;            /*!< Width of image */
    uint32_t   start_core;       /*!< Which core to start with */
    uint32_t   end_core;         /*!< Which core to end with */
    uint16_t   class_color[R_SEMSEG_CLASS_MAX]; /*!< Colors for the semseg classes represented as 8 bit U value 8 bit V value */
} st_config_t;

/*! Private data structure for semseg_color_cve module */
typedef struct st_priv_tag {
    st_config_t     config;               /*!< Configuration structure */
    st_cve_data_t   cve[R_MAX_CVE_CORES]; /*!< CVe program data */
} st_priv_t; /*lint !e9109 !e761 */ /*
 * e9109 e761:
 * Description:    st_priv_t is declared in r_module.h, but each module has its own definition of st_priv_t
 * Justification:  This allows us to not have to typecast R_MODULE_GetPrivDataPtr()
 * Risk:           Possible confusion for developers using st_priv_t in two different modules
 */

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_instance);
static r_result_t R_Deinit(st_module_instance_t * const p_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_instance);

/*! Module interface definition for semseg_color_cve */
const st_module_interface_t g_module_interface_phantom_color_cve = {
    .module_name   = "phantom_color_cve",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = R_Deinit,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/***********************************************************************************************************************
 Local Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   register_plane
 * @brief           Adds the data plane to the command list data.
 * @param [in,out]  p_cl_data - pointer to the command list
 * @param [in]      p_plane - Pointer to the image plane
 * @param [in]      format - Format of the image
 * @param [in]      stride - Stride of the image
 * @param [in]      plane_index - Plane index of the image
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t register_plane(R_ATMLIB_CLData *p_cl_data, R_ATMLIB_OCVPlaneParam * p_plane, R_ATMLIB_IMAGE_FORMAT format, uint32_t stride, uint8_t plane_index)
{
    r_result_t  result = R_RESULT_SUCCESS;

    p_plane->img_type   = format;
    p_plane->img_addr   = 0; //To be filled out later
    p_plane->img_stride = stride;

    if (r_extatm_OCV_RegisterPlane(p_cl_data, p_plane, plane_index) != 0)
    {
        R_PRINT_ERROR("Failed to register plane %u", plane_index);
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   init_cl
 * @brief           Initializes the command list
 * @param [in]      p_data - pointer to the private data
 * @param [in]      core_index - Index of the CVe core to initialize
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_cl(st_priv_t * p_data, uint32_t core_index)
{
    r_result_t result   = R_RESULT_SUCCESS;
    uint32_t y_loops    = 0;
    uint32_t y_offset   = 0;
    uint32_t * p_cl_mem = NULL;

    /* CVe program memory regions */
    uint8_t program[CVE_size_of_r_phantom_color_cv_text]    = {CVE_r_phantom_color_cv_text};
    uint8_t uniform[CVE_size_of_r_phantom_color_cv_uniform] = {CVE_r_phantom_color_cv_uniform};

    /* CVe parameter structs */
    R_EXTATM_OCVMasterSlaveParam master_slave_param = {R_EXTATM_OCV_MASTER_SLAVE_ERROR, 0};
    R_ATMLIB_OCVRectParam        rect_param         = {0};
    R_EXTATM_OCVProgram          program_param      = {{0}};
    R_EXTATM_OCVFunctionParam    function_param     = {0};

    if (NULL == p_data)
    {
        R_PRINT_ERROR("NULL p_data");
        result = R_RESULT_FAILED;
    }

    /* Update some variables */
    if (R_RESULT_SUCCESS == result)
    {
        /* Calculate y loops and offset for each core */
        y_loops  = p_data->config.height / ((p_data->config.end_core - p_data->config.start_core) + 1u);
        y_offset = y_loops * (core_index - p_data->config.start_core);

        /* Update y_loops if lines don't divide evenly into cores */
        if (p_data->config.end_core == core_index)
        {
            y_loops += (p_data->config.height % ((p_data->config.end_core - p_data->config.start_core) + 1u));
        }
    }

    /* Get program and command list memory */
    if (R_RESULT_SUCCESS == result)
    {
        p_data->cve[core_index].p_prog_mem = gf_Memalign(64, CVE_size_of_r_phantom_color_cv_text);
        p_cl_mem = gf_Memalign(64, COLOR_PRV_CL_SIZE);

        /* Copy program to allocated memory for the C code: array of instructions */
        if ((NULL != p_data->cve[core_index].p_prog_mem) || (NULL != p_cl_mem))
        {
            R_COMMON_Memcpy(p_data->cve[core_index].p_prog_mem, program, sizeof(program));
            R_COMMON_Memset(p_cl_mem, 0, COLOR_PRV_CL_SIZE);
        }
        else
        {
            R_PRINT_ERROR("Failed to get memory");
            result = R_RESULT_FAILED;
        }
    }

    /* Initialize command list */
    if (R_RESULT_SUCCESS == result)
    {
        if (r_atmlib_InitializeOCVCL(&p_data->cve[core_index].cl_data, p_cl_mem, COLOR_PRV_CL_SIZE >> 2) != R_ATMLIB_E_OK)
        {
            R_PRINT_ERROR("Failed to initialize command list");
            result = R_RESULT_FAILED;
        }
    }

    /* Program physical address */
    if (R_RESULT_SUCCESS == result)
    {
        program_param.phy_addr.master = (uint32_t)gf_GetPhysAddr(p_data->cve[core_index].p_prog_mem);
        if (r_extatm_OCV_SetProgramAddress(&p_data->cve[core_index].cl_data, &program_param) != 0)
        {
            R_PRINT_ERROR("Failed to set program address");
            result = R_RESULT_FAILED;
        }
    }


    /* Set master status */
    if (R_RESULT_SUCCESS == result)
    {
        master_slave_param.mode = R_EXTATM_OCV_ALL_MASTERS;
        if (r_extatm_OCV_SetMasterSlaveMode(&p_data->cve[core_index].cl_data, &master_slave_param) != 0)
        {
            R_PRINT_ERROR("Failed to set master status");
            result = R_RESULT_FAILED;
        }
    }

    /* Uniform configuration*/
    if (R_RESULT_SUCCESS == result)
    {
        p_data->cve[core_index].uniform_param.size = (uint32_t)CVE_size_of_r_phantom_color_cv_uniform >> 2;
        p_data->cve[core_index].uniform_param.data = (uint32_t*)uniform; /*lint !e2445 */
        if (r_extatm_OCV_SetUniform(&p_data->cve[core_index].cl_data, &p_data->cve[core_index].uniform_param) != 0)
        {
            R_PRINT_ERROR("Failed to set uniform address");
            result = R_RESULT_FAILED;
        }
    }

    /* Register planes */
    if (R_RESULT_SUCCESS == result)
    {
        result |= register_plane(&p_data->cve[core_index].cl_data, &p_data->cve[core_index].plane_param[COLOR_PRV_SEMSEG_DATA_PLANE],
                R_ATMLIB_IMG_8U, p_data->config.width * R_IMAGE_BYTES_PP_ENUM,             COLOR_PRV_SEMSEG_DATA_PLANE);

        result |= register_plane(&p_data->cve[core_index].cl_data, &p_data->cve[core_index].plane_param[COLOR_PRV_UYVA_DATA_PLANE],
                R_ATMLIB_IMG_8U, p_data->config.width * R_IMAGE_BYTES_PP_UYVA_INTERLEAVED, COLOR_PRV_UYVA_DATA_PLANE);
    }

    /* Function offset set*/
    if (R_RESULT_SUCCESS == result)
    {
        function_param.pc_master = CVE_entry_master_r_phantom_color_cv__main;
        function_param.pc_slave  = 0;
        if (r_extatm_OCV_SetFunctionOffsets(&p_data->cve[core_index].cl_data, &function_param) != 0)
        {
            R_PRINT_ERROR("Failed to set function offsets");
            result = R_RESULT_FAILED;
        }
    }

    /* Add rect command */
    if (R_RESULT_SUCCESS == result)
    {
        rect_param.dx1  = 4;                                   // Increase x by 4 each loop (because we consume 4 bytes of input image each loop)
        rect_param.dy1  = 1;                                   // Increase y by 1 each loop
        rect_param.dx2  = 0;                                   // Usually you leave this at 0...
        rect_param.dy2  = 0;                                   // Usually you leave this at 0...
        rect_param.xlen = (uint16_t)p_data->config.width / 4u; // Divide width by 4 because we consume 4 pixels at a time
        rect_param.ylen = (uint16_t)y_loops;                   // This is how many y channels we do per core
        rect_param.xs   = (int16_t)0;                          // Start x at 0
        rect_param.ys   = (int16_t)y_offset;                   // Start y at offset based on core

        if (r_atmlib_OCV_RECT(&p_data->cve[core_index].cl_data, R_ATMLIB_DISABLE, 0, 0, &rect_param) != R_ATMLIB_E_OK)
        {
            R_PRINT_ERROR("Failed to add rect command");
            result = R_RESULT_FAILED;
        }
    }

    /* Add sync */
    if (R_RESULT_SUCCESS == result)
    {
        if (r_atmlib_OCV_SYNCS(&p_data->cve[core_index].cl_data, R_ATMLIB_OCVSYNCS_OCVCORE) != R_ATMLIB_E_OK)
        {
            R_PRINT_ERROR("Failed to sync command list");
            result = R_RESULT_FAILED;
        }
    }

    /* Finalize */
    if (R_RESULT_SUCCESS == result)
    {
        if (r_atmlib_FinalizeOCVCL(&p_data->cve[core_index].cl_data) != R_ATMLIB_E_OK)
        {
            R_PRINT_ERROR("Failed to finalize command list");
            result = R_RESULT_FAILED;
        }
    }

    /* Flush Cache of command list and program */
    if (R_RESULT_SUCCESS == result)
    {
        gf_DCacheFlushRange((void*)p_data->cve[core_index].cl_data.top_addr, COLOR_PRV_CL_SIZE);
        gf_DCacheFlushRange((void*)p_data->cve[core_index].p_prog_mem,       CVE_size_of_r_phantom_color_cv_text);
    }

    return result;
} /*
 * e2445:
 * Description:    Uniform memory is declared as a uint8_t array but it is added to the command list as a uint32_t array
 * Justification:  The uniform memory generated by the CVe compiler area will always be aligned to 32 bytes
 * Risk:           Future CVe compiler updates produces non-aligned memory
 */

/******************************************************************************************************************//**
 * Function Name:   deinit_cl
 * @brief           Deinitializes the command list
 * @param [in]      p_data - pointer to the private data
 * @param [in]      core_index - Index of the CVe core to initialize
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t deinit_cl(st_priv_t * p_data, uint32_t core_index)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NULL != p_data)
    {
        gf_Free((void *)p_data->cve[core_index].cl_data.top_addr);
        gf_Free((void *)p_data->cve[core_index].p_prog_mem);
    }
    else
    {
        R_PRINT_ERROR("NULL p_data");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   process_cve
 * @brief           Updates the CVe command list with the new data planes, then executes the command list and waits
 *                  for data processing to be completed.
 * @param [in]      p_instance - pointer to the module instance
 * @param [in]      p_semseg - Pointer to the semseg enum input object
 * @param [in]      p_uyva - Pointer to the uyva input output object
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t process_cve(st_module_instance_t * const p_instance, void * p_semseg, void * p_uyva)
{
    r_result_t        result     = R_RESULT_SUCCESS;
    st_priv_t *       p_data     = R_MODULE_GetPrivDataPtr(p_instance);
    uint32_t          core_index = 0;
    RCvIMPDRVCOREINFO core_info  = {0};

    /* Check private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("No private data available");
        result = R_RESULT_FAILED;
    }

    /* Start CVe program */
    if (R_RESULT_SUCCESS == result)
    {
        /* Flush cache of all data planes */
        R_TIMER_LogMessage(p_instance, "Flushing cache of data planes");
        gf_DCacheFlushRange(p_semseg, (size_t)(p_data->config.width * p_data->config.height * R_IMAGE_BYTES_PP_ENUM));
        gf_DCacheFlushRange(p_uyva,   (size_t)(p_data->config.width * p_data->config.height * R_IMAGE_BYTES_PP_UYVA_INTERLEAVED));

        /* Update image plane in command lists */
        for (core_index = p_data->config.start_core; core_index <= p_data->config.end_core; core_index++)
        {
            p_data->cve[core_index].cl_data.top_addr[p_data->cve[core_index].plane_param[COLOR_PRV_SEMSEG_DATA_PLANE].clofs_img_addr] = (uint32_t)gf_GetPhysAddr(p_semseg);
            p_data->cve[core_index].cl_data.top_addr[p_data->cve[core_index].plane_param[COLOR_PRV_UYVA_DATA_PLANE].clofs_img_addr]   = (uint32_t)gf_GetPhysAddr(p_uyva);

            /* Flush cache of command list */
            gf_DCacheFlushRange((void *)(p_data->cve[core_index].cl_data.top_addr), COLOR_PRV_CL_SIZE);
        }

        /* Execute CVe program */
        R_TIMER_LogMessage(p_instance, "Executing CVe program");
        for (core_index = p_data->config.start_core; core_index <= p_data->config.end_core; core_index++)
        {
            core_info.CoreNum  = core_index;
            core_info.CoreType = RCVDRV_CORE_TYPE_OCV;

            if (R_COMMON_HW_ExecuteIMP((uint32_t)gf_GetPhysAddr((void *)p_data->cve[core_index].cl_data.top_addr), &core_info) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Error executing command list for core %u", core_info.CoreNum);
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Wait for CVe program to finish operation */
    if (R_RESULT_SUCCESS == result)
    {
        /* Wait for all cores */
        R_TIMER_LogMessage(p_instance, "Waiting for result");
        for (core_index = p_data->config.start_core; core_index <= p_data->config.end_core; core_index++)
        {
            core_info.CoreNum  = core_index;
            core_info.CoreType = RCVDRV_CORE_TYPE_OCV;

            if (R_COMMON_HW_WaitIMP(&core_info) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Error waiting for core %u", core_info.CoreNum);
                result = R_RESULT_FAILED;
            }
        }

        /* Invalidate cache of output planes */
        R_TIMER_LogMessage(p_instance, "Invalidating cache of output planes");
        gf_DCacheInvalidateRange(p_uyva, (size_t)(R_IMAGE_BYTES_PP_UYVA_INTERLEAVED * p_data->config.width * p_data->config.height));
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   update_colors
 * @brief           Updates the CVe program uniform memory area with the color configuration
 * @param [in]      p_data - pointer to the module instance private data
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t update_colors(st_priv_t * p_data)
{
    r_result_t result = R_RESULT_SUCCESS;
    uint16_t u_value  = 0;
    uint16_t v_value  = 0;

    for (uint32_t core_index = p_data->config.start_core; core_index <= p_data->config.end_core; core_index++)
    {
        for (uint32_t class_index = 0; class_index < (uint32_t)R_SEMSEG_CLASS_MAX; class_index++)
        {
            /* Separate the u and v values */
            u_value = 0x00FFu & (p_data->config.class_color[class_index] >> 8);
            v_value = 0x00FFu & p_data->config.class_color[class_index];

            /* Update the uniform memory area */
            p_data->cve[core_index].cl_data.top_addr[p_data->cve[core_index].uniform_param.clofs_uniform + (2u * class_index) + 0u] = (uint32_t)u_value;
            p_data->cve[core_index].cl_data.top_addr[p_data->cve[core_index].uniform_param.clofs_uniform + (2u * class_index) + 1u] = (uint32_t)v_value;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * @page    PQS_Semseg_Color_Cve_Config  PQS Semseg Color CVe Config
 * @ingroup PQS_Semseg_Color_Cve
 * # Configuration Table:
 *
 *   Config Name    | Config Type      | Default | Range                              | Description
 *   -------------- | ---------------- | ------- | ---------------------------------- | ------------------
 *   height         | uint32_t         | 800     | 1 - @ref R_OBJECT_IMAGE_MAX_HEIGHT | Height of the input image
 *   width          | uint32_t         | 1280    | 2 - @ref R_OBJECT_IMAGE_MAX_WIDTH  | Width of the input image
 *   start_core     | uint32_t         | 0       | 0 - (@ref R_MAX_CVE_CORES - 1)     | Starting core to use for CVe program
 *   end_core       | uint32_t         | 4       | 0 - (@ref R_MAX_CVE_CORES - 1)     | Ending core to use for CVe program. Cannot be less than start_core.
 *   background     | uint16_t         | 0x8080  | 0x0000 - 0xFFFF                    | Color to use for background class. Upper byte U value lower byte V value
 *   road           | uint16_t         | 0x2C16  | 0x0000 - 0xFFFF                    | Color to use for road class. Upper byte U value lower byte V value
 *   lane           | uint16_t         | 0x0195  | 0x0000 - 0xFFFF                    | Color to use for lane class. Upper byte U value lower byte V value
 *   curb           | uint16_t         | 0x8080  | 0x0000 - 0xFFFF                    | Color to use for curb class. Upper byte U value lower byte V value
 *   vehicle        | uint16_t         | 0xFF6B  | 0x0000 - 0xFFFF                    | Color to use for vehicle class. Upper byte U value lower byte V value
 *   other_markings | uint16_t         | 0xE5B8  | 0x0000 - 0xFFFF                    | Color to use for other road markings class. Upper byte U value lower byte V value
 *   sidewalk       | uint16_t         | 0x8080  | 0x0000 - 0xFFFF                    | Color to use for sidewalk class. Upper byte U value lower byte V value
 *   pedestrian     | uint16_t         | 0x54FF  | 0x0000 - 0xFFFF                    | Color to use for pedestrian class. Upper byte U value lower byte V value
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_config
 * @brief           Initializes the configuration structure with either the default values or with values from a config
 *                  file if available.
 * @param [in,out]  p_config - pointer to the configuration structure
 * @param [in]      p_instance_name - String of the module instance name
 * @param [in]      p_module_name - String of the module name
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_config(st_config_t * p_config, char const * const p_instance_name, char const * const p_module_name)
{
    r_result_t result = R_RESULT_SUCCESS;

    (void)p_module_name; //Not currently used

    /* Init config */
    p_config->height     = R_COMMON_CONFIG_GetUint32(p_instance_name, "height",     COLOR_PRV_DEFAULT_IMAGE_HEIGHT);
    p_config->width      = R_COMMON_CONFIG_GetUint32(p_instance_name, "width",      COLOR_PRV_DEFAULT_IMAGE_WIDTH);
    p_config->start_core = R_COMMON_CONFIG_GetUint32(p_instance_name, "start_core", COLOR_PRV_DEFAULT_START_CORE);
    p_config->end_core   = R_COMMON_CONFIG_GetUint32(p_instance_name, "end_core",   COLOR_PRV_DEFAULT_END_CORE);

    p_config->class_color[R_SEMSEG_CLASS_BACKGROUND]     = (uint16_t)R_COMMON_CONFIG_GetUint32(p_instance_name, "background",     COLOR_PRV_DEFAULT_BACKGROUND_COLOR);
    p_config->class_color[R_SEMSEG_CLASS_ROAD]           = (uint16_t)R_COMMON_CONFIG_GetUint32(p_instance_name, "road",           COLOR_PRV_DEFAULT_ROAD_COLOR);
    p_config->class_color[R_SEMSEG_CLASS_LANE]           = (uint16_t)R_COMMON_CONFIG_GetUint32(p_instance_name, "lane",           COLOR_PRV_DEFAULT_LANE_COLOR);
    p_config->class_color[R_SEMSEG_CLASS_CURB]           = (uint16_t)R_COMMON_CONFIG_GetUint32(p_instance_name, "curb",           COLOR_PRV_DEFAULT_CURB_COLOR);
    p_config->class_color[R_SEMSEG_CLASS_VEHICLE]        = (uint16_t)R_COMMON_CONFIG_GetUint32(p_instance_name, "vehicle",        COLOR_PRV_DEFAULT_VEHICLE_COLOR);
    p_config->class_color[R_SEMSEG_CLASS_OTHER_MARKINGS] = (uint16_t)R_COMMON_CONFIG_GetUint32(p_instance_name, "other_markings", COLOR_PRV_DEFAULT_OTHER_MARKING_COLOR);
    p_config->class_color[R_SEMSEG_CLASS_SIDEWALK]       = (uint16_t)R_COMMON_CONFIG_GetUint32(p_instance_name, "sidewalk",       COLOR_PRV_DEFAULT_SIDEWALK_COLOR);
    p_config->class_color[R_SEMSEG_CLASS_PEDESTRIAN]     = (uint16_t)R_COMMON_CONFIG_GetUint32(p_instance_name, "pedestrian",     COLOR_PRV_DEFAULT_PEDESTRIAN_COLOR);

    /* Check config */
    if (p_config->start_core > p_config->end_core)
    {
        R_PRINT_ERROR("Staring CVE core cannot be greater than ending CVe core.");
        result = R_RESULT_FAILED;
    }

    if (R_MAX_CVE_CORES <= p_config->end_core)
    {
        R_PRINT_ERROR("End CVE core cannot be greater than R_MAX_CVE_CORES");
        result = R_RESULT_FAILED;
    }

    if ((0u == p_config->height) || (0u == p_config->width))
    {
        R_PRINT_ERROR("Neither height nor width can be zero");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   init_priv
 * @brief           Initializes the private data structure
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_priv(st_module_instance_t * const p_instance)
{
    r_result_t result                  = R_RESULT_SUCCESS;
    char const * const p_instance_name = R_MODULE_GetInstanceName(p_instance);
    char const * const p_module_name   = R_MODULE_GetModuleName(p_instance);
    st_priv_t * p_data                 = R_MODULE_GetPrivDataPtr(p_instance);

    /* Check private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("No private data available");
        result = R_RESULT_FAILED;
    }

    /* Init configuration */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_config(&p_data->config, p_instance_name, p_module_name) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Configuration init has failed");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/***********************************************************************************************************************
 Module Interface Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    r_result_t result  = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Init private data */
    if (R_RESULT_SUCCESS == result)
    {
        result = init_priv(p_instance);
    }

    /* Init command lists for all cores */
    if (R_RESULT_SUCCESS == result)
    {
        for (uint32_t core_index = p_data->config.start_core; core_index <= p_data->config.end_core; core_index++)
        {
            result |= init_cl(p_data, core_index);
        }
    }

    /* Update the colors in the command list based on the configs values */
    if (R_RESULT_SUCCESS == result)
    {
        result = update_colors(p_data);
    }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Semseg_Color_Cve_Channels PQS Semseg Color CVe Channels
 * @ingroup    PQS_Semseg_Color_Cve
 * # Channel Table:
 *
 *  Channel name   | Channel Type | Object Type  | Description
 *  -------------- | ------------ | ------------ | ----------------------------------
 *  semseg_in      | input        | image        | @ref PQS_Semseg_Color_Cve_Channel_semseg_in
 *  uyva_inout     | in/out       | image        | @ref PQS_Semseg_Color_Cve_Channel_uyva_inout
 *
 * @anchor PQS_Semseg_Color_Cve_Channel_semseg_in
 * ## PQS Semseg Color CVe Channel Semseg In
 *             Enumerated class image of the configured height and width. Format is R_IMAGE_FORMAT_ENUM. Bytes
 *             per pixel is 1 and image plane depth is 1. This type of image comes out of the semantic segmentation
 *             module.
 *
 * @anchor PQS_Semseg_Color_Cve_Channel_uyva_inout
 * ## PQS Semseg Color CVe Channel UYVA In/Out
 *             UYVA image of the configured height and width. Format is R_IMAGE_FORMAT_UYVA_INTERLEAVED. Bytes
 *             per pixel is 4 and image depth is 1. This image will altered during execution of this module.
 *             No other module should be processing this image during this time.
 *
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_BROKER_InitChannelInfo(p_instance, COLOR_PRV_CHANNEL_SEMSEG_IN,  "semseg_in",  R_OBJECT_TYPE_IMAGE, R_BROKER_DATA_DIRECTION_IN);
    result |= R_BROKER_InitChannelInfo(p_instance, COLOR_PRV_CHANNEL_UYVA_INOUT, "uyva_inout", R_OBJECT_TYPE_IMAGE, R_BROKER_DATA_DIRECTION_INOUT);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_Deinit
 * @brief           Deinitializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Deinit(st_module_instance_t * const p_instance)
{
    r_result_t result  = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Init command lists for all cores */
    if (R_RESULT_SUCCESS == result)
    {
        for (uint32_t core_index = p_data->config.start_core; core_index <= p_data->config.end_core; core_index++)
        {
            result |= deinit_cl(p_data, core_index);
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    r_result_t result             = R_RESULT_SUCCESS;
    st_priv_t * p_data            = R_MODULE_GetPrivDataPtr(p_instance);
    st_object_t * p_object_semseg = R_BROKER_GetObjectPtr(p_instance, COLOR_PRV_CHANNEL_SEMSEG_IN);
    st_object_t * p_object_uyva   = R_BROKER_GetObjectPtr(p_instance, COLOR_PRV_CHANNEL_UYVA_INOUT);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CheckObjectType(p_object_semseg, R_OBJECT_TYPE_IMAGE);
        result |= R_OBJECT_CheckObjectType(p_object_uyva,   R_OBJECT_TYPE_IMAGE);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Invalid object input");
        }
    }

    /* Check input */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CheckImageWidth(        p_object_semseg, p_data->config.width);
        result |= R_OBJECT_CheckImageHeight(       p_object_semseg, p_data->config.height);
        result |= R_OBJECT_CheckImageDepth(        p_object_semseg, 1);
        result |= R_OBJECT_CheckImageBytesPerPixel(p_object_semseg, R_IMAGE_BYTES_PP_ENUM);
        result |= R_OBJECT_CheckImageFormat(       p_object_semseg, R_IMAGE_FORMAT_ENUM);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Invalid image: Expected %u x %u depth=%u, bpp=%u, format=%d.  Got %d x %d depth=%d, bpp=%d, format=%d",
                    p_data->config.width,
                    p_data->config.height,
                    1,
                    R_IMAGE_BYTES_PP_ENUM,
                    R_IMAGE_FORMAT_ENUM,
                    R_OBJECT_GetImageWidth(        p_object_semseg),
                    R_OBJECT_GetImageHeight(       p_object_semseg),
                    R_OBJECT_GetImageDepth(        p_object_semseg),
                    R_OBJECT_GetImageBytesPerPixel(p_object_semseg),
                    R_OBJECT_GetImageFormat(       p_object_semseg));
        }
    }

    /* Check input object properties */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CheckImageWidth(        p_object_uyva, p_data->config.width);
        result |= R_OBJECT_CheckImageHeight(       p_object_uyva, p_data->config.height);
        result |= R_OBJECT_CheckImageDepth(        p_object_uyva, 1);
        result |= R_OBJECT_CheckImageBytesPerPixel(p_object_uyva, R_IMAGE_BYTES_PP_UYVA_INTERLEAVED);
        result |= R_OBJECT_CheckImageFormat(       p_object_uyva, R_IMAGE_FORMAT_UYVA_INTERLEAVED);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Invalid image: Expected %u x %u depth=%d, bpp=%d, format=%d. Got %d x %d depth=%d, bpp=%d, format=%d",
                    p_data->config.width,
                    p_data->config.height,
                    1,
                    R_IMAGE_BYTES_PP_UYVA_INTERLEAVED,
                    R_IMAGE_FORMAT_UYVA_INTERLEAVED,
                    R_OBJECT_GetImageWidth(        p_object_uyva),
                    R_OBJECT_GetImageHeight(       p_object_uyva),
                    R_OBJECT_GetImageDepth(        p_object_uyva),
                    R_OBJECT_GetImageBytesPerPixel(p_object_uyva),
                    R_OBJECT_GetImageFormat(       p_object_uyva));
        }
    }

    /* Do image processing */
    if (R_RESULT_SUCCESS == result)
    {
        void * p_semseg = R_OBJECT_GetImagePlaneCPU(p_object_semseg);
        void * p_uyva   = R_OBJECT_GetImagePlaneCPU(p_object_uyva);

        /* Process image planes */
        if ((NULL != p_semseg) && (NULL != p_uyva))
        {
            result = process_cve(p_instance, p_semseg, p_uyva);
        }
        else
        {
            R_PRINT_ERROR("Some error with getting CPU data");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}
