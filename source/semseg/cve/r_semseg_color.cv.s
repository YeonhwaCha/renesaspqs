#/**********************************************************************************************************************
# * DISCLAIMER
# * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
# * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
# * applicable laws, including copyright laws.
# * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
# * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
# * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
# * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
# * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
# * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
# * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
# * this software. By using this software, you agree to the additional terms and conditions found by accessing the
# * following link:
# * http://www.renesas.com/disclaimer
# *
# * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
# *********************************************************************************************************************/

#/***********************************************************************************************************************
# * History : YYYY-MM-DD Version  Description
# *         : yyyy-mm-dd 0.00     Beta Version
# **********************************************************************************************************************/

    .section uniform
## Default all colors to gray
CH0_U: 128
CH0_V: 128
CH1_U: 128
CH1_V: 128
CH2_U: 128
CH2_V: 128
CH3_U: 128
CH3_V: 128
CH4_U: 128
CH4_V: 128
CH5_U: 128
CH5_V: 128
CH6_U: 128
CH6_V: 128
CH7_U: 128
CH7_V: 128

UNIFORM_MEM:      0xf0018000 # Top of uniform mem (where CH0_U is stored)
ONE:              1

    .section .text

	.entry _main, master
# Start of program
_main:

    #
    # Load in 4 bytes of data to registers R16-R19
    #

    STCI     CR8, 0             # Set input to data plane 0
                                # Top of data plane 0 is ch 0
    LDRCI    R16, 0, 0          # R16 gets data @ x-offset of 0
    LDRCI    R17, 1, 0          # R17 gets data @ x-offset of 1
    LDRCI    R18, 2, 0          # R18 gets data @ x-offset of 2
    LDRCI    R19, 3, 0          # R19 gets data @ x-offset of 3

    MOVU     R7, UNIFORM_MEM    # Uniform mem offset

    #
    # SemSeg color output goes to data plane 1, set up output now
    #

    STCI     CR9, 1             # Set output to data plane 1
    SHLI     R0, R0, 2          # This output has 4x as many bytes as input, so adjust X value

    #
    # Coloring works by skipping pixels, depending on the row.  Figure out what row we are here
    #

    ANDU     R31, R1, ONE       # See if it's an even or odd row
    CMPU.EQ  R3, R31, ONE       # If SR is set, it's an odd row

    #
    # Load in the color for each of the 4 pixels.  Color is stored in uniform memory.
    # This is repeated 4 times for R16-R19
    #

    SHLI     R16, R16, 1        # Multiply enum by 2 to get ready for color offset in uniform memory
    LD       R20, R7, R16       # R20 gets U1
    ADDI     R16, R16, 1
    LD       R16, R7, R16       # R16 gets V1

    SHLI     R17, R17, 1        # Multiply enum by 2 to get ready for color offset in uniform memory
    LD       R21, R7, R17       # R18 gets U2
    ADDI     R17, R17, 1
    LD       R17, R7, R17       # R19 gets V2

    SHLI     R18, R18, 1        # Multiply enum by 2 to get ready for color offset in uniform memory
    LD       R22, R7, R18       # R20 gets U3
    ADDI     R18, R18, 1
    LD       R18, R7, R18       # R21 gets V3

    SHLI     R19, R19, 1        # Multiply enum by 2 to get ready for color offset in uniform memory
    LD       R23, R7, R19       # R22 gets U4
    ADDI     R19, R19, 1
    LD       R19, R7, R19       # R23 gets V4


    #
    # Write out color, only changing u/v on even or odd pixels depending on row
    # Use SR to determine if we actually write out data or if we skip it
    #

    STRCI    R20, 0,  0, !SR    # U1
    STRCI    R16, 2,  0, !SR    # V1
    STRCI    R21, 4,  0, SR     # U2
    STRCI    R17, 6,  0, SR     # V2
    STRCI    R22, 8,  0, !SR    # U3
    STRCI    R18, 10, 0, !SR    # V3
    STRCI    R23, 12, 0, SR     # U4
    STRCI    R19, 14, 0, SR     # V4

    TRAP                        # Done

