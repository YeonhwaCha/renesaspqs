/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_semseg_rvx.c
 * @brief         Source file for semantic segmentation neural network. See @ref PQS_Semseg_Rvx for more details
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @page       PQS_Semseg_Rvx_Algorithm  PQS Semseg RVX Algorithm
 * @ingroup    PQS_Semseg_Rvx
 * # Algorithm Description:
 *             This module takes in YUV444 fully planar image and executes a semantic segmentation neural network.
 *             The output of the network is an enumerated image where the value of each pixel corresponds to the
 *             class of that pixel. Classes include: background, road, lane, curb, vehicle, non-lane road markings,
 *             sidewalk, and pedestrians. This module uses the CNN-IP.
 * # Block Diagram:
 *             @image html semseg_rvx.png
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include "r_common.h"
#include "r_object_image.h"
#include "generic_api_memory.h"

#include "r_semseg_rvx.h"
#include "r_common_rvx.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define SEMSEG_PRV_CHANNEL_IMAGE_IN      (0u) /*!< Broker channel for YUV444 fully planarimage input */
#define SEMSEG_PRV_CHANNEL_SEMSEG_OUT    (1u) /*!< Broker channel for semantic segmentation enum output */

#define SEMSEG_PRV_MAX_FILENAME_LENGTH   (64u) /*!< Max length for CNN weight filenames */

#define SEMSEG_PRV_ENUM_BYTES_PP         (1u) /*!< Bytes per pixel for semseg enum image */
#define SEMSEG_PRV_PLANAR_BYTES_PP       (1u) /*!< Bytes per pixel for YUV444 fully planar */
#define SEMSEG_PRV_OUT_MAP_BYTES_PP      (2u) /*!< Bytes per pixel for output feature map */

#define SEMSEG_PRV_IMAGE_HEIGHT          (512u)  /*!< Height of the image */
#define SEMSEG_PRV_IMAGE_WIDTH           (1024u) /*!< Width of the image */

#define SEMSEG_PRV_CHANNEL_MASK          (0x0007u) /*!< Mask of the semseg enum. */

#define SEMSEG_PRV_DEFAULT_CNN_BINARY_FILENAME  "cl_cnn0_rfd.bin"     /*!< Default name for CNN file*/
#define SEMSEG_PRV_DEFAULT_DMA_BINARY_FILENAME  "cl_dma0_rfd.bin"     /*!< Default name for DMA file*/
#define SEMSEG_PRV_DEFAULT_BCL_FILENAME         "commandlist_rfd.bcl" /*!< Default name for BCL file */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Data needed for RVX program execution */
typedef struct
{
    st_rvx_mem_t  bcl;           /*!< RVX memory layout container */
    st_rvx_mem_t  cnn_cl;        /*!< RVX command lists container for CNN */
    st_rvx_mem_t  dma_cl;        /*!< RVX command lists container for DMA */
} st_algo_rvx_t;

/*! Configuration structure for semseg_rvx module */
typedef struct {
    char cnn_filename[SEMSEG_PRV_MAX_FILENAME_LENGTH];  /*!< Filename for CNN binary */
    char dma_filename[SEMSEG_PRV_MAX_FILENAME_LENGTH];  /*!< Filename for DMA binary */
    char bcl_filename[SEMSEG_PRV_MAX_FILENAME_LENGTH];  /*!< Filename for BCL binary */
} st_config_t;

/*! Private data structure for semseg_rvx module */
typedef struct st_priv_tag {
    st_config_t       config;    /*!< Configuration structure */
    st_algo_rvx_t     algo_rvx;  /*!< RVX data */
} st_priv_t; /*lint !e9109 !e761 */ /*
 * e9109 e761:
 * Description:    st_priv_t is declared in r_module.h, but each module has its own definition of st_priv_t
 * Justification:  This allows us to not have to typecast R_MODULE_GetPrivDataPtr()
 * Risk:           Possible confusion for developers using st_priv_t in two different modules
 */

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_instance);
static r_result_t R_Deinit(st_module_instance_t * const p_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_instance);

/*! Module interface definition for semseg_rvx */
const st_module_interface_t g_module_interface_semseg_rvx = {
    .module_name   = "semseg_rvx",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = R_Deinit,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * @page    PQS_Semseg_Rvx_Config  PQS Semseg RVX Config
 * @ingroup PQS_Semseg_Rvx
 * # Configuration Table:
 *
 *   Config Name    | Config Type  | Default              | Range  | Description
 *   -------------- | -------------| -------------------- | ------ | ------------------
 *   cnn_filename   | string       | cl_cnn0_rfd.bin      | Any    | Filename of the CNN binary
 *   dma_filename   | string       | cl_dma0_rfd.bin      | Any    | Filename of the DMA binary
 *   bcl_filename   | string       | commandlist_rfd.bcl  | Any    | Filename of the BCL binary
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_config
 * @brief           Initializes the configuration structure with either the default values or with values from a config
 *                  file if available.
 * @param [in,out]  p_config - pointer to the configuration structure
 * @param [in]      p_instance_name - String of the module instance name
 * @param [in]      p_module_name - String of the module name
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_config(st_config_t * p_config, char const * const p_instance_name, char const * const p_module_name)
{
    r_result_t result = R_RESULT_SUCCESS;

    (void)p_module_name; // Not used

    /* Init config */
    result |= R_COMMON_STRING_Copy(p_config->cnn_filename, R_COMMON_CONFIG_GetString(p_instance_name, "cnn_filename", SEMSEG_PRV_DEFAULT_CNN_BINARY_FILENAME), SEMSEG_PRV_MAX_FILENAME_LENGTH);
    result |= R_COMMON_STRING_Copy(p_config->dma_filename, R_COMMON_CONFIG_GetString(p_instance_name, "dma_filename", SEMSEG_PRV_DEFAULT_DMA_BINARY_FILENAME), SEMSEG_PRV_MAX_FILENAME_LENGTH);
    result |= R_COMMON_STRING_Copy(p_config->bcl_filename, R_COMMON_CONFIG_GetString(p_instance_name, "bcl_filename", SEMSEG_PRV_DEFAULT_BCL_FILENAME),        SEMSEG_PRV_MAX_FILENAME_LENGTH);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   process_data
 * @brief           Executes the neural network
 * @param [in]      p_instance - pointer to the module instance
 * @param [in]      p_plane_y - Pointer to the input Y data plane
 * @param [in]      p_plane_u - Pointer to the input U data plane
 * @param [in]      p_plane_v - Pointer to the input V data plane
 * @param [out]     p_plane_semseg - Pointer to the output semseg enum data plane
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t process_data(st_module_instance_t * const p_instance, uint8_t * p_plane_y, uint8_t * p_plane_u, uint8_t * p_plane_v,  uint8_t * p_plane_semseg)
{
    /* Initialized  locals */
    r_result_t result          = R_RESULT_SUCCESS;
    uint32_t input_plane_size  = SEMSEG_PRV_IMAGE_HEIGHT * SEMSEG_PRV_IMAGE_WIDTH * SEMSEG_PRV_PLANAR_BYTES_PP;
    uint32_t output_plane_size = SEMSEG_PRV_IMAGE_HEIGHT * SEMSEG_PRV_IMAGE_WIDTH * SEMSEG_PRV_OUT_MAP_BYTES_PP;
    st_priv_t * p_data         = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Process data */
    if (R_RESULT_SUCCESS == result)
    {
        /* Requesting input buffers from RVX generated command list */
        bcl_mem_entry_t* p_entry_y = rvxt_bcl_getEntryMemory(p_data->algo_rvx.bcl.virt, 0, 0);
        bcl_mem_entry_t* p_entry_u = rvxt_bcl_getEntryMemory(p_data->algo_rvx.bcl.virt, 0, 1);
        bcl_mem_entry_t* p_entry_v = rvxt_bcl_getEntryMemory(p_data->algo_rvx.bcl.virt, 0, 2);
        bcl_mem_exit_t*  p_exit    = rvxt_bcl_getExitMemory( p_data->algo_rvx.bcl.virt, 0, 0);

        /* Getting virtual addresses of memory planes requested by RVX */
        uint8_t *  p_y_data   =  (uint8_t *)gf_GetVirtAddr((uintptr_t)p_entry_y->destination);
        uint8_t *  p_u_data   =  (uint8_t *)gf_GetVirtAddr((uintptr_t)p_entry_u->destination);
        uint8_t *  p_v_data   =  (uint8_t *)gf_GetVirtAddr((uintptr_t)p_entry_v->destination);
        uint16_t * p_out_data = (uint16_t *)gf_GetVirtAddr((uintptr_t)p_exit->base);

        /* Copying input data to RVX reserved data planes */
        R_TIMER_LogMessage(p_instance, "Copy input");
        R_COMMON_Memcpy(p_y_data, p_plane_y, input_plane_size);
        R_COMMON_Memcpy(p_u_data, p_plane_u, input_plane_size);
        R_COMMON_Memcpy(p_v_data, p_plane_v, input_plane_size);

        /* Flush cache of all data planes */
        R_TIMER_LogMessage(p_instance, "Flush cache of all data planes");
        gf_DCacheFlushRange(p_y_data,   input_plane_size);
        gf_DCacheFlushRange(p_u_data,   input_plane_size);
        gf_DCacheFlushRange(p_v_data,   input_plane_size);
        gf_DCacheFlushRange(p_out_data, output_plane_size);

        /* Run CNN */
        R_TIMER_LogMessage(p_instance, "Execute RVX");
        result = R_COMMON_RVX_Execute(&p_data->algo_rvx.bcl, &p_data->algo_rvx.cnn_cl, &p_data->algo_rvx.dma_cl);

        /* Invalidate cache of output planes */
        R_TIMER_LogMessage(p_instance, "Invalidate cache of output planes");
        gf_DCacheInvalidateRange(p_out_data, output_plane_size);

        /* Copying output of CNN into designated output plane */
        R_TIMER_LogMessage(p_instance, "Copy output");
        for (uint32_t k = 0; k < (SEMSEG_PRV_IMAGE_HEIGHT * SEMSEG_PRV_IMAGE_WIDTH); k++)
        {
            p_plane_semseg[k] = (uint8_t)(p_out_data[k] & SEMSEG_PRV_CHANNEL_MASK);
        }
    }

    return result;
}

/***********************************************************************************************************************
 Module Interface Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    r_result_t result                  = R_RESULT_SUCCESS;
    char const * const p_instance_name = R_MODULE_GetInstanceName(p_instance);
    char const * const p_module_name   = R_MODULE_GetModuleName(p_instance);
    st_priv_t * p_data                 = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Init configuration */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_config(&p_data->config, p_instance_name, p_module_name) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Configuration init has failed");
            result = R_RESULT_FAILED;
        }
    }

    /* Init rvx */
    if (R_RESULT_SUCCESS == result)
    {
        /* R_COMMON_RVX_Init() will init the bcl, cnn_cl, and dma_cl structs. They do not need to be initialized before */
        if (R_COMMON_RVX_Init(&p_data->algo_rvx.bcl, &p_data->algo_rvx.cnn_cl, &p_data->algo_rvx.dma_cl,
                p_data->config.bcl_filename, p_data->config.cnn_filename, p_data->config.dma_filename) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("RVX Init failed");
            result = R_RESULT_FAILED;
        }

    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_Deinit
 * @brief           Deinitializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Deinit (st_module_instance_t * const p_instance)
{
    r_result_t result  = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Clean up rvx */
    if (R_RESULT_SUCCESS == result)
    {
        if (R_COMMON_RVX_Deinit(&p_data->algo_rvx.bcl, &p_data->algo_rvx.cnn_cl, &p_data->algo_rvx.dma_cl) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("RVX Deinit failed");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Semseg_Rvx_Channels PQS Semseg RVX Channels
 * @ingroup    PQS_Semseg_Rvx
 * # Channel Table:
 *
 *  Channel name   | Channel Type | Object Type  | Description
 *  -------------- | ------------ | ------------ | ----------------------------------
 *  image_in       | input        | image        | @ref PQS_Semseg_Rvx_Channel_image_in
 *  semseg_out     | output       | image        | @ref PQS_Semseg_Rvx_Channel_semseg_out
 *
 * @anchor PQS_Semseg_Rvx_Channel_image_in
 * ## PQS Semseg Rvx Channel Image Input
 *             Fully planar YUV444 image of size 1024 width by 512 height. Format is R_IMAGE_FORMAT_YUV444_FULLY_PLANAR.
 *             Bytes per pixel is 1 per plane and image plane depth is 3.
 *
 * @anchor PQS_Semseg_Rvx_Channel_semseg_out
 * ## PQS Semseg RVX Channel Semseg Output
 *             Enumerated class image of size 1024 width by 512 height. Format is R_IMAGE_FORMAT_ENUM. Bytes
 *             per pixel is 1 and image plane depth is 1.
 *
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_BROKER_InitChannelInfo(p_instance, SEMSEG_PRV_CHANNEL_IMAGE_IN,   "image_in",   R_OBJECT_TYPE_IMAGE, R_BROKER_DATA_DIRECTION_IN);
    result |= R_BROKER_InitChannelInfo(p_instance, SEMSEG_PRV_CHANNEL_SEMSEG_OUT, "semseg_out", R_OBJECT_TYPE_IMAGE, R_BROKER_DATA_DIRECTION_OUT);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    r_result_t result             = R_RESULT_SUCCESS;
    st_object_t * p_object_image  = R_BROKER_GetObjectPtr(p_instance, SEMSEG_PRV_CHANNEL_IMAGE_IN);
    st_object_t * p_object_semseg = R_BROKER_GetObjectPtr(p_instance, SEMSEG_PRV_CHANNEL_SEMSEG_OUT);

    /* Check objects */
    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */ /*
 * e948 e774:
 * Description:    This if statement always evaluates to true
 * Justification:  Encapsulation of code with a result check adds to code uniformity and allows for better portability
 * Risk:           None
 */
    {
        result |= R_OBJECT_CheckObjectType(p_object_image,  R_OBJECT_TYPE_IMAGE);
        result |= R_OBJECT_CheckObjectType(p_object_semseg, R_OBJECT_TYPE_IMAGE);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Invalid object passed");
        }
    }

    /* Check input image */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CheckImageDepth(        p_object_image, 3);
        result |= R_OBJECT_CheckImageBytesPerPixel(p_object_image, SEMSEG_PRV_PLANAR_BYTES_PP);
        result |= R_OBJECT_CheckImageFormat(       p_object_image, R_IMAGE_FORMAT_YUV444_FULLY_PLANAR);
        result |= R_OBJECT_CheckImageHeight(       p_object_image, SEMSEG_PRV_IMAGE_HEIGHT);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Invalid object passed");
        }
    }

    /* Fill enum object */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CopyHeader(           p_object_semseg, p_object_image);
        result |= R_OBJECT_SetImageHeight(       p_object_semseg, SEMSEG_PRV_IMAGE_HEIGHT);
        result |= R_OBJECT_SetImageWidth(        p_object_semseg, SEMSEG_PRV_IMAGE_WIDTH);
        result |= R_OBJECT_SetImageDepth(        p_object_semseg, 1);
        result |= R_OBJECT_SetImageBytesPerPixel(p_object_semseg, SEMSEG_PRV_ENUM_BYTES_PP);
        result |= R_OBJECT_SetImageFormat(       p_object_semseg, R_IMAGE_FORMAT_ENUM);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Couldn't set output params");
        }
    }

    /* Run DNN */
    if (R_RESULT_SUCCESS == result)
    {
        void * p_semseg  = R_OBJECT_GetImagePlaneCPU(p_object_semseg);
        void * p_y_plane = R_OBJECT_GetIndexedImagePlaneCPU(p_object_image, R_IMAGE_YUV444_PLANE_INDEX_Y);
        void * p_u_plane = R_OBJECT_GetIndexedImagePlaneCPU(p_object_image, R_IMAGE_YUV444_PLANE_INDEX_U);
        void * p_v_plane = R_OBJECT_GetIndexedImagePlaneCPU(p_object_image, R_IMAGE_YUV444_PLANE_INDEX_V);

        /* Process image planes */
        if ((NULL != p_semseg) && (NULL != p_y_plane) && (NULL != p_u_plane) && (NULL != p_v_plane))
        {
            result = process_data(p_instance, p_y_plane, p_u_plane, p_v_plane, p_semseg);
        }
        else
        {
            R_PRINT_ERROR("Error getting data planes");
            result = R_RESULT_FAILED;
        }

    }

    return result;
}

