/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_birdeye_imr.c
 * @brief         Source file for birdeye view image transformation using IMR. See @ref PQS_Birdeye_IMR for more details
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @page       PQS_Birdeye_IMR_Algorithm  PQS Birdeye IMR Algorithm
 * @ingroup    PQS_Birdeye_IMR
 * # Algorithm Description:
 *             This module uses the IMR to perform a birdeye view transformation on either an interleaved UYVY
 *             image, or on a binary image. The homography info used dictates the transformation. Homography data
 *             is stored and configured in the R_COMMON_SENSOR module on a per sensor basis and is retrieved via
 *             the sensor_id.
 * # Block Diagram:
 *             @image html birdeye_imr.png
 *********************************************************************************************************************/
/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include "r_common.h"
#include "r_object_image.h"
#include "generic_api_memory.h"

#include "r_imr_api.h"
#include "r_imr_cmn.h"
#include "opencv_lens.h"
#include "imr_dlgen.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define BIRDEYE_PRV_CHANNEL_IMAGE_IN          (0) /*!< Broker Channel for input image */
#define BIRDEYE_PRV_CHANNEL_IMAGE_OUT         (1) /*!< Broker Channel for output image */

#define BIRDEYE_PRV_DL_ALIGN                  (64)   /*!< Display list memory alignment */
#define BIRDEYE_PRV_DL_ALLOC_SIZE             (2 * 1024 * 1024UL)  /*!< Size of Display list */
#define BIRDEYE_PRV_MESH_SIZE                 (2)    /*!< Mesh Size */
#define BIRDEYE_PRV_OPT_NOT_USED              (0)    /*!< Execution option not used */
#define BIRDEYE_PRV_EXEC_TIMEOUT              (500U) /*!< Execution timeout in miliseconds */

/* Note Homography parameters are based on the input and output image sizes. Currently these are not configurable */
#define BIRDEYE_PRV_INPUT_HEIGHT              (512)   /*!< Fixed value for input image height. */
#define BIRDEYE_PRV_INPUT_WIDTH               (1024)  /*!< Fixed value for input image width. */
#define BIRDEYE_PRV_OUTPUT_HEIGHT             (320)   /*!< Fixed value for output image height. */
#define BIRDEYE_PRV_OUTPUT_WIDTH              (320)   /*!< Fixed value for output image width. */

#define BIRDEYE_PRV_DEFAULT_IMR_CHANNEL_START (0) /*!< Default value for IMR staring channel. Can be overridden by config */
#define BIRDEYE_PRV_DEFAULT_IMR_CHANNEL_END   (5) /*!< Default value for IMR ending channel. Can be overridden by config */

#define BIRDEYE_PRV_DEFAULT_IMAGE_FORMAT      (R_IMAGE_FORMAT_UYVY_INTERLEAVED) /*!< Default value for input image format  */

#define BIRDEYE_PRV_DEFAULT_SENSOR_ID         (0) /*!< Default value for sensor ID. Needed to fetch lens homography parameters.
                                                         Can be overridden by config */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Configuration structure for birdeye_imr module */
typedef struct {
    e_image_format_t  format;        /*!< Format of input and output image */
    uint32_t          sensor_id;     /*!< Id of the sensor */
    uint32_t          start_channel; /*!< Starting IMR channel */
    uint32_t          end_channel;   /*!< Ending IMR channel */
} st_config_t;

/*! Private data structure for birdeye_imr module */
typedef struct st_priv_tag {
    st_config_t config;                              /*!< Configuration parameters */
    r_imr_dl_t    display_lists[R_MAX_IMR_CHANNELS]; /*!< Display list for each IMR channels */
    r_imr_data_t imr_input;                          /*!< Input image info */
    r_imr_data_t imr_output;                         /*!< Output image info */
} st_priv_t;

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_instance);
static r_result_t R_Deinit(st_module_instance_t * const p_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_instance);

/*! Module interface definition for birdeye_imr */
const st_module_interface_t g_module_interface_birdeye_imr = {
    .module_name   = "birdeye_imr",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = R_Deinit,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Local Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   get_attr
 * @brief           Returns the IMR data attribute for a given image format
 * @param [in]      format - Format of the image
 * @retval          The image attribute
 *********************************************************************************************************************/
static uint32_t get_attr(e_image_format_t format)
{
    uint32_t attr = R_IMR_8BIT;

    switch (format)
    {
        case R_IMAGE_FORMAT_UYVY_INTERLEAVED:
        {
            attr = R_IMR_YUV;
            break;
        }

        case R_IMAGE_FORMAT_BINARY:
        {
            attr = R_IMR_8BIT;
            break;
        }

        default:
        {
            R_PRINT_ERROR("Fell through switch. Format not supported.");
            break;
        }
    }

    return attr;
}


/******************************************************************************************************************//**
 * Function Name:   get_bytes_per_pixel
 * @brief           Returns the bytes per pixel of the given format
 * @param [in]      format - Format of the image
 * @retval          0 - on failure, otherwise the bytes per pixel of the given image format
 *********************************************************************************************************************/
static uint32_t get_bytes_per_pixel(e_image_format_t format)
{
    uint8_t bytes_per_pixel = 0;

    switch (format)
    {
        case R_IMAGE_FORMAT_UYVY_INTERLEAVED:
        {
            bytes_per_pixel = R_IMAGE_BYTES_PP_UYVY_INTERLEAVED;
            break;
        }

        case R_IMAGE_FORMAT_BINARY:
        {
            bytes_per_pixel = R_IMAGE_BYTES_PP_BINARY;
            break;
        }

        default:
        {
            R_PRINT_ERROR("Fell through switch. Format not supported.");
        }
    }

    return bytes_per_pixel;
}

/******************************************************************************************************************//**
 * @page    PQS_Birdeye_IMR_Config  PQS Birdeye IMR Config
 * @ingroup PQS_Birdeye_IMR
 * # Configuration Table:
 *
 *  Config Name   | Config Type      | Default                         | Range                                                    | Description
 *  ------------- | ---------------- | ------------------------------- | --------                                                 |------------------------------------------
 *  format        | e_image_format_t | R_IMAGE_FORMAT_UYVY_INTERLEAVED | R_IMAGE_FORMAT_UYVY_INTERLEAVED or R_IMAGE_FORMAT_BINARY | Format of the input image.
 *  start_channel | uint32_t         | 0                               | 0 - (@ref R_MAX_IMR_CHANNELS - 1)                        | Beginning IMR channel to use for program
 *  end_channel   | uint32_t         | 5                               | 0 - (@ref R_MAX_IMR_CHANNELS - 1)                        | Ending IMR channel to use for program. Cannot be less than start_channel
 *  sensor_id     | uint32_t         | 0                               | None                                                     | ID of the sensor to use for lens correction transformation
 *
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_config
 * @brief           Initializes the configuration structure with either the default values or with values from a config
 *                  file if available.
 * @param [in,out]  p_config - pointer to the configuration structure
 * @param [in]      p_instance_name - String of the module instance name
 * @param [in]      p_module_name - String of the module name
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_config(st_config_t * p_config, char const * const p_instance_name, char const * const p_module_name)
{
    r_result_t result = R_RESULT_SUCCESS;
    const char * format_names[] = E_IMAGE_FORMAT_AS_STRING;

    /* Get config */
    p_config->format        = R_COMMON_CONFIG_GET_ENUM( p_instance_name, "format",        BIRDEYE_PRV_DEFAULT_IMAGE_FORMAT, format_names, (sizeof(format_names))/(sizeof(format_names[0])), e_image_format_t);
    p_config->sensor_id     = R_COMMON_CONFIG_GetUint32(p_instance_name, "sensor_id",     BIRDEYE_PRV_DEFAULT_SENSOR_ID);
    p_config->start_channel = R_COMMON_CONFIG_GetUint32(p_instance_name, "start_channel", BIRDEYE_PRV_DEFAULT_IMR_CHANNEL_START);
    p_config->end_channel   = R_COMMON_CONFIG_GetUint32(p_instance_name, "end_channel",   BIRDEYE_PRV_DEFAULT_IMR_CHANNEL_END);

    /* Check config */
    if (p_config->start_channel > p_config->end_channel)
    {
        R_PRINT_ERROR("Staring IMR channel cannot be greater than ending channel.");
        result = R_RESULT_FAILED;
    }

    if (R_MAX_IMR_CHANNELS <= p_config->end_channel)
    {
        R_PRINT_ERROR("End channel cannot be greater than R_MAX_IMR_CHANNELS");
        result = R_RESULT_FAILED;
    }

    if ((R_IMAGE_FORMAT_UYVY_INTERLEAVED != p_config->format) && (R_IMAGE_FORMAT_BINARY != p_config->format))
    {
        R_PRINT_ERROR("Input format not supported.");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   init_priv
 * @brief           Initializes the private data structure
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_priv(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    char const * const p_instance_name = R_MODULE_GetInstanceName(p_instance);
    char const * const p_module_name   = R_MODULE_GetModuleName(p_instance);
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    /* Check private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("No private data available");
        result = R_RESULT_FAILED;
    }

    /* Init configuration */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_config(&p_data->config, p_instance_name, p_module_name) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Configuration init has failed");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   process_image_planes
 * @brief           Processes the raw data planes
 * @param [in]      p_instance - Pointer to the module instance
 * @param [in]      p_input_plane - Pointer to input data plane
 * @param [out]     p_output_plane - Pointer to output data plane
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t process_image_planes(st_module_instance_t * const p_instance, uint8_t * p_input_plane, uint8_t * p_output_plane)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);
    r_imr_source_t src_info = {0};
    size_t input_size       = 0;
    size_t output_size      = 0;
    int32_t imr_result      = 0;
    uint8_t channel_index   = 0;

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Update data */
    if ((R_RESULT_SUCCESS == result))
    {
        input_size  = p_data->imr_input.Stride  * p_data->imr_input.Height;
        output_size = p_data->imr_output.Stride * p_data->imr_output.Height;

        /* Update input struct */
        p_data->imr_input.VmrStartAddr = (addr_t)p_input_plane;
        p_data->imr_input.PhysAddr     = gf_GetPhysAddr(p_input_plane);

        /* Update output struct */
        p_data->imr_output.VmrStartAddr = (addr_t)p_output_plane;
        p_data->imr_output.PhysAddr     = gf_GetPhysAddr(p_output_plane);

        /* Flush Cache */
        gf_DCacheFlushRange((void *)p_input_plane,  input_size);
        gf_DCacheFlushRange((void *)p_output_plane, output_size);
    }

    /* Set Source data */
    if ((R_RESULT_SUCCESS == result))
    {
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            /* Note: start_line, end_line, and mesh_size are unused */
            src_info.source_width  = p_data->imr_input.Width;
            src_info.source_height = p_data->imr_input.Height;

            imr_result = R_IMR_SetSource(channel_index, &src_info);
            if (E_OK != imr_result)
            {
                R_PRINT_ERROR("Error in R_IMR_SetSource() for channel %d ret = %d", channel_index, imr_result);
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Start IMR */
    if ((R_RESULT_SUCCESS == result))
    {
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            imr_result = R_IMR_ExecuteExt(channel_index,
                                    &p_data->display_lists[channel_index],
                                    &p_data->imr_input,
                                    &p_data->imr_output,
                                    R_IMR_EXE_MODE_BILINEAR_ENABLE | R_IMR_EXE_MODE_TEXTUREMAPPING,
                                    BIRDEYE_PRV_OPT_NOT_USED);

            if (E_OK != imr_result)
            {
                R_PRINT_ERROR("Error in R_IMR_ExecuteExt() for channel %d ret = %d", channel_index, imr_result);
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Wait and read status */
    if (R_RESULT_SUCCESS == result)
    {
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            imr_result = R_IMR_WaitEventTimeout(channel_index, BIRDEYE_PRV_EXEC_TIMEOUT);

            if (R_IMR_SR_TRA  != imr_result)
            {
                R_PRINT_ERROR("Error in R_IMR_WaitEventTimeout() for channel %d ret = %d", channel_index, result);
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Invalidate data cache */
    gf_DCacheInvalidateRange((void *)p_output_plane, output_size);

    return result;
}

/***********************************************************************************************************************
 Module Interface Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    r_result_t result = init_priv(p_instance);
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);
    uint8_t channel_index = 0;
    int imr_result = 0;
    imr_roi_t imr_regions[R_MAX_IMR_CHANNELS] = {0};
    uint8_t num_channels_used = 0;
    st_homography_t homography = {0};

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Get parameters for camera */
    if (R_RESULT_SUCCESS == result)
    {
        if (R_COMMON_SENSOR_GetHomography(p_data->config.sensor_id, &homography) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error getting sensor info");
            result = R_RESULT_FAILED;
        }
    }

    /* Init local variables */
    if (R_RESULT_SUCCESS == result)
    {
        num_channels_used = (p_data->config.end_channel - p_data->config.start_channel) + 1;

        /* Init IMR input image struct */
        p_data->imr_input.Height       = BIRDEYE_PRV_INPUT_HEIGHT;
        p_data->imr_input.Width        = BIRDEYE_PRV_INPUT_WIDTH;
        p_data->imr_input.bpp          = 8;
        p_data->imr_input.Stride       = BIRDEYE_PRV_INPUT_WIDTH * get_bytes_per_pixel(p_data->config.format);
        p_data->imr_input.Attr         = get_attr(p_data->config.format);
        p_data->imr_input.VmrStartAddr = 0;
        p_data->imr_input.PhysAddr     = 0;

        /* Init IMR output image struct */
        p_data->imr_output.Height       = BIRDEYE_PRV_OUTPUT_HEIGHT;
        p_data->imr_output.Width        = BIRDEYE_PRV_OUTPUT_WIDTH;
        p_data->imr_output.bpp          = 8;
        p_data->imr_output.Stride       = BIRDEYE_PRV_OUTPUT_WIDTH * get_bytes_per_pixel(p_data->config.format);
        p_data->imr_output.Attr         = get_attr(p_data->config.format);
        p_data->imr_output.VmrStartAddr = 0;
        p_data->imr_output.PhysAddr     = 0;

        /* Set IMR regions structs */
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            imr_regions[channel_index].x = 0;
            imr_regions[channel_index].y = (channel_index - p_data->config.start_channel) * (BIRDEYE_PRV_OUTPUT_HEIGHT / num_channels_used);
            imr_regions[channel_index].Width  = BIRDEYE_PRV_OUTPUT_WIDTH;
            imr_regions[channel_index].Height = (BIRDEYE_PRV_OUTPUT_HEIGHT / num_channels_used);

            /* last time though loop account for remainder */
            if (p_data->config.end_channel == channel_index)
            {
                imr_regions[channel_index].Height += (BIRDEYE_PRV_OUTPUT_HEIGHT % num_channels_used);
            }

            R_PRINT_INFO("IMR Region for channel %u\n"
                         "\t x: %u\n"
                         "\t y: %u\n"
                         "\t Width: %u\n"
                         "\t Height: %u",
                         channel_index,
                         imr_regions[channel_index].x,
                         imr_regions[channel_index].y,
                         imr_regions[channel_index].Width,
                         imr_regions[channel_index].Height);
        }
    }

    /* Create display lists */
    if (R_RESULT_SUCCESS == result)
    {
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            p_data->display_lists[channel_index].VmrStartAddr = (addr_t)gf_Memalign(BIRDEYE_PRV_DL_ALIGN, BIRDEYE_PRV_DL_ALLOC_SIZE);

            if (0 != p_data->display_lists[channel_index].VmrStartAddr)
            {
                p_data->display_lists[channel_index].PhysAddr = gf_GetPhysAddr((void*)p_data->display_lists[channel_index].VmrStartAddr);
                p_data->display_lists[channel_index].Size     = BIRDEYE_PRV_DL_ALLOC_SIZE;
                p_data->display_lists[channel_index].Pos      = 0;
            }
            else
            {
                R_PRINT_ERROR("Failed to allocate memory for display list");
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Build display lists */
    if (R_RESULT_SUCCESS == result)
    {
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            imr_result = imr_build_display_list_roi(&p_data->display_lists[channel_index],
                                                    &p_data->imr_input,
                                                    &p_data->imr_output,
                                                    BIRDEYE_PRV_MESH_SIZE,
                                                    map_opencv_homography,
                                                    &homography,
                                                    imr_regions[channel_index]);

            if (0 == imr_result)
            {
                /* Flush DL memory to DRAM */
                gf_DCacheFlushRange((void *)p_data->display_lists[channel_index].VmrStartAddr, (size_t)p_data->display_lists[channel_index].Size);
            }
            else
            {
                R_PRINT_ERROR("ERROR: DL Creation failed with error %d", imr_result);
                result = R_RESULT_FAILED;
            }
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Birdeye_IMR_Channels PQS Birdeye IMR Channels
 * @ingroup    PQS_Birdeye_IMR
 * # Channel Table:
 *
 *  Channel name | Channel Type | Object Type | Description
 *  ------------ | ------------ | ----------- | ----------------------------------
 *  image_in     | input        | image       | @ref PQS_Birdeye_IMR_Channel_Image_In
 *  image_out    | output       | image       | @ref PQS_Birdeye_IMR_Channel_Image_Out
 *
 * @anchor PQS_Birdeye_IMR_Channel_Image_In
 * ## PQS Birdeye IMR Channel Image In
 *             Input is either an YUV422 interleaved UYVY image of type R_IMAGE_FORMAT_UYVY_INTERLEAVED or an image
 *             of type R_IMAGE_FORMAT_BINARY, note the R_IMAGE_FORMAT_BINARY image format has 255 value representing
 *             a logical one. The height is fixed at 512 and the width is fixed at 1024.
 *
 * @anchor PQS_Birdeye_IMR_Channel_Image_Out
 * ## PQS Birdeye IMR Channel Image Out
 *             Input is either an YUV422 interleaved UYVY image of type R_IMAGE_FORMAT_UYVY_INTERLEAVED or an image
 *             of type R_IMAGE_FORMAT_BINARY, note the R_IMAGE_FORMAT_BINARY image format has 255 value representing
 *             a logical one. The height is fixed at 320 and the width is fixed at 320.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_BROKER_InitChannelInfo(p_instance, BIRDEYE_PRV_CHANNEL_IMAGE_IN,  "image_in",  R_OBJECT_TYPE_IMAGE, R_BROKER_DATA_DIRECTION_IN);
    result |= R_BROKER_InitChannelInfo(p_instance, BIRDEYE_PRV_CHANNEL_IMAGE_OUT, "image_out", R_OBJECT_TYPE_IMAGE, R_BROKER_DATA_DIRECTION_OUT);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_Deinit
 * @brief           Deinitializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Deinit(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);
    uint8_t channel_index = 0;

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Free display lists */
    if (R_RESULT_SUCCESS == result)
    {
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            gf_Free((void*)p_data->display_lists[channel_index].VmrStartAddr);
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    r_result_t result         = R_RESULT_SUCCESS;
    st_object_t * p_image_in  = R_BROKER_GetObjectPtr(p_instance, BIRDEYE_PRV_CHANNEL_IMAGE_IN);
    st_object_t * p_image_out = R_BROKER_GetObjectPtr(p_instance, BIRDEYE_PRV_CHANNEL_IMAGE_OUT);
    st_priv_t * p_data        = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    if ((R_OBJECT_CheckObjectType(p_image_in,  R_OBJECT_TYPE_IMAGE) != R_RESULT_SUCCESS) ||
        (R_OBJECT_CheckObjectType(p_image_out, R_OBJECT_TYPE_IMAGE) != R_RESULT_SUCCESS))
    {
        R_PRINT_ERROR("Invalid object input");
        result = R_RESULT_FAILED;
    }

    /* Check input */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CheckImageWidth(        p_image_in, BIRDEYE_PRV_INPUT_WIDTH);
        result |= R_OBJECT_CheckImageHeight(       p_image_in, BIRDEYE_PRV_INPUT_HEIGHT);
        result |= R_OBJECT_CheckImageDepth(        p_image_in, 1);
        result |= R_OBJECT_CheckImageBytesPerPixel(p_image_in, get_bytes_per_pixel(p_data->config.format));
        result |= R_OBJECT_CheckImageFormat(       p_image_in, p_data->config.format);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Invalid image: Expected %u x %u depth=%d, bpp=%d, format=%d.  Got %d x %d depth=%d, bpp=%d, format=%d",
                    BIRDEYE_PRV_INPUT_WIDTH,
                    BIRDEYE_PRV_INPUT_HEIGHT,
                    1,
                    get_bytes_per_pixel(p_data->config.format),
                    p_data->config.format,
                    R_OBJECT_GetImageWidth(        p_image_in),
                    R_OBJECT_GetImageHeight(       p_image_in),
                    R_OBJECT_GetImageDepth(        p_image_in),
                    R_OBJECT_GetImageBytesPerPixel(p_image_in),
                    R_OBJECT_GetImageFormat(       p_image_in));
        }
    }

    /* Fill output object */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CopyHeader(            p_image_out, p_image_in);
        result |= R_OBJECT_SetImageWidth(         p_image_out, BIRDEYE_PRV_OUTPUT_WIDTH);
        result |= R_OBJECT_SetImageHeight(        p_image_out, BIRDEYE_PRV_OUTPUT_HEIGHT);
        result |= R_OBJECT_SetImageDepth(         p_image_out, 1);
        result |= R_OBJECT_SetImageBytesPerPixel( p_image_out, get_bytes_per_pixel(p_data->config.format));
        result |= R_OBJECT_SetImageFormat(        p_image_out, p_data->config.format);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Couldn't set output params");
        }
    }

    /* Start image processing */
    if (R_RESULT_SUCCESS == result)
    {
        uint8_t * p_plane_in  = R_OBJECT_GetImagePlaneCPU(p_image_in);
        uint8_t * p_plane_out = R_OBJECT_GetImagePlaneCPU(p_image_out);

        if ((NULL != p_plane_in) && (NULL != p_plane_out))
        {
            if (process_image_planes(p_instance, p_plane_in, p_plane_out) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Error in imr function");
                result = R_RESULT_FAILED;
            }
        }
        else
        {
            R_PRINT_ERROR("Some error with getting CPU data");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}
