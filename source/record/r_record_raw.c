/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_record_raw.c
 * @brief         Source file for recording of raw objects to disk. See @ref PQS_Record_Raw for more details
 *********************************************************************************************************************/
 /******************************************************************************************************************//**
 * @page       PQS_Record_Raw_Algorithm  PQS Record Raw Algorithm
 * @ingroup    PQS_Record_Raw
 * # Algorithm Description:
 *             This module records objects data planes to disk in a raw format i.e. no header information.
 *
 * # Block Diagram:
 *             @image html record_raw.png
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include "r_common.h"
#include "r_common_os.h"

#include "r_record_raw.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define RECORD_PRV_CHANNEL_IN  (0) /*!< Broker channel for input image */

#define RECORD_PRV_MAX_FOLDER_NAME_LENGTH       (256) /*!< Maximum length for a folder name */
#define RECORD_PRV_MAX_FILE_NAME_LENGTH         (256) /*!< Maximum length for the file name */
#define RECORD_PRV_MAX_OBJECT_TYPE_CFG_LENGTH   (256) /*!< Maximum length for the object type name in the configuration variable */

#define RECORD_PRV_DEFAULT_FOLDER_NAME     "./record"               /*!< Folder name that contains the record data. Can be relative or absolute. */
#define RECORD_PRV_FILE_EXTENSION          "raw"                    /*!< Extension of the files to be written to disk */
#define RECORD_PRV_DEFAULT_OBJECT_TYPE     (R_OBJECT_TYPE_IMAGE)    /*!< Default object type name to be recorded. */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Configuration struct for record_raw module */
typedef struct {
    char folder_name[RECORD_PRV_MAX_FOLDER_NAME_LENGTH];    /*!< Name of the folder for record data */
    e_object_type_t object_type;                            /*!< Type of the object to be recorded */
} st_record_cfg_t;

/*! Private data structure for record_raw module */
typedef struct st_priv_tag {
    st_record_cfg_t config;             /*!< Configuration struct */
    uint32_t        capture_id;         /*!< Index of current capture */
} st_priv_t;

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_module_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_module_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_module_instance);

/*! Module interface definition for record_raw */
const st_module_interface_t g_module_interface_record_raw = {
    .module_name   = "record_raw",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = NULL,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/***********************************************************************************************************************
 Local Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * @page    PQS_Record_Raw_Config  PQS Record Raw Config
 * @ingroup PQS_Record_Raw
 * # Configuration Table:
 *
 *   Config Name  | Config Type  | Default             | Range  | Description
 *   ------------ | -------------| --------------------| -------| ------------------
 *   folder_name  | string       | "./record"          | Any    | Name of the folder to write files to
 *   object_type  | string       | R_OBJECT_TYPE_IMAGE | Any    | Type of the object to be recorded
 *
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_config
 * @brief           Initializes the configuration structure with either the default values or with values from a config
 *                  file if available.
 * @param [in,out]  p_config - pointer to the configuration structure
 * @param [in]      p_instance_name - String of the module instance name
 * @param [in]      p_module_name - String of the module name
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_config(st_record_cfg_t * p_config, char const * const p_instance_name, char const * const p_module_name)
{
    r_result_t result = R_RESULT_SUCCESS;
    const char * object_types[] = E_OBJECT_TYPE_AS_STRING;

    /* Update config */
    result |= R_COMMON_STRING_Copy(p_config->folder_name, R_COMMON_CONFIG_GetString(p_instance_name, "folder_name", RECORD_PRV_DEFAULT_FOLDER_NAME), RECORD_PRV_MAX_FOLDER_NAME_LENGTH);
    p_config->object_type = R_COMMON_CONFIG_GET_ENUM( p_instance_name, "object_type", RECORD_PRV_DEFAULT_OBJECT_TYPE, object_types, (sizeof(object_types))/(sizeof(object_types[0])), e_object_type_t);

    /* Check config */
    if ((p_config->object_type <= R_OBJECT_TYPE_INVALID) || (p_config->object_type >= R_OBJECT_TYPE_MAX))
    {
        R_PRINT_ERROR("Config object type not supported.");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   get_fname
 * @brief           Fills in the supplied filename string with the combined folder and filename to of the object to
 *                  record to next
 * @param [in]      p_priv - Module private data
 * @param [in]      fname_size - Size of the supplied fname string
 * @param [out]     fname - Name of the file to record
 * @retval          R_RESULT_SUCCESS on success
 *********************************************************************************************************************/
static r_result_t get_fname(st_priv_t * const p_priv, const uint32_t fname_size, char * const fname)
{
    r_result_t result = R_RESULT_SUCCESS;
    int32_t written = snprintf(fname, fname_size, "%s/%06u.%s", p_priv->config.folder_name, p_priv->capture_id, RECORD_PRV_FILE_EXTENSION);

    if ((written < 0) || (written == fname_size))
    {
        R_PRINT_ERROR("Error occurred getting filename");
        result = R_RESULT_FAILED;
    }

    return result;
}

/***********************************************************************************************************************
 Module Interface Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    r_result_t result  = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);
    char const * const p_instance_name = R_MODULE_GetInstanceName(p_instance);
    char const * const p_module_name   = R_MODULE_GetModuleName(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Init configuration */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_config(&p_data->config, p_instance_name, p_module_name) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Configuration init has failed");
            result = R_RESULT_FAILED;
        }
    }

    /* Init private data struct */
    if (R_RESULT_SUCCESS == result)
    {
        p_data->capture_id = 0;
    }

    /* Create directory in case it doesn't exist */
    if (R_RESULT_SUCCESS == result)
    {
        result = R_COMMON_OS_MakeDir(p_data->config.folder_name);
    }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Record_Raw_Channels PQS Record Raw Channels
 * @ingroup    PQS_Record_Raw
 * # Channel Table:
 *
 *  Channel name | Channel Type | Object Type                  | Description
 *  ------------ | ------------ | ---------------------------- | ----------------------------------
 *  data         | input        | depends on the configuration | @ref PQS_Record_Raw_Channel_data
 *
 * @anchor PQS_Record_Raw_Channel_data
 * ## PQS Record Raw Channel data
 *             This is the input object containing the data to write to memory, its type must be specified in the configuration file.
 *
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    /* Check for NULL pointer */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Specify the actual input object type */
    if (R_RESULT_SUCCESS == result)
    {
        result = R_BROKER_InitChannelInfo(p_instance, RECORD_PRV_CHANNEL_IN, "data", p_data->config.object_type, R_BROKER_DATA_DIRECTION_IN);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t *   p_data   = R_MODULE_GetPrivDataPtr(p_instance);
    st_object_t * p_object = R_BROKER_GetObjectPtr(p_instance, RECORD_PRV_CHANNEL_IN);
    char fname[RECORD_PRV_MAX_FILE_NAME_LENGTH] = {0};

    /* Make sure we got our private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Check object */
    if (R_OBJECT_CheckObjectType(p_object, p_data->config.object_type) != R_RESULT_SUCCESS)
    {
        R_PRINT_ERROR("Invalid object input");
        result = R_RESULT_FAILED;
    }

    /* Get name for file */
    if (R_RESULT_SUCCESS == result)
    {
        if (get_fname(p_data, sizeof(fname), fname) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Couldn't create file name");
            result = R_RESULT_FAILED;
        }
    }

    /* Write to memory */
    if (R_RESULT_SUCCESS == result)
    {
        if (R_OBJECT_WriteRaw(p_object, fname) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Couldn't write file '%s' for recording", fname);
            result = R_RESULT_FAILED;
        }
    }

    /* Always update capture id so that we notice if a frame goes missing */
    if (NULL != p_data)
    {
        p_data->capture_id++;
    }

    return result;
}
