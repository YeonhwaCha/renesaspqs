/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_playback_vraw.c
 * @brief         Source file for playback of raw videos from disk. See @ref PQS_Playback_Vraw for more details
 *********************************************************************************************************************/
 /******************************************************************************************************************//**
 * @page       PQS_Playback_Vraw_Algorithm  PQS Playback Video Raw Algorithm
 * @ingroup    PQS_Playback_Vraw
 * # Algorithm Description:
 *             This module reads in a raw video file and begins the image processing pipeline. The raw video consists
 *             of raw (meaning image planes without header information) images concatenated together into a single file.
 *             Once the module reaches the end of the playback file it will loop to the beginning of the file and playback
 *             again. The user can interact with this module via different command line keystrokes.
 *
 * # Block Diagram:
 *             @image html playback_vraw.png
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/*lint --e{586} */

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include <unistd.h> /* For usleep */

#include "r_common.h"
#include "r_object_image.h"
#include "r_playback_vraw.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define PLAYBACK_PRV_CHANNEL_OUT             (0u)    /*!< Broker Channel for output image */
#define PLAYBACK_PRV_CHANNEL_TRIGGER         (1u)    /*!< Broker Channel for input trigger */

#define PLAYBACK_PRV_MAX_FILE_NAME_LENGTH    (256u)  /*!< Maximum length for the file name */

#define PLAYBACK_PRV_DEFAULT_FORMAT          (R_IMAGE_FORMAT_UYVY_INTERLEAVED) /*!< Default output format */

#define PLAYBACK_PRV_DEFAULT_HEIGHT          (800u)  /*!< Default output image height.*/
#define PLAYBACK_PRV_DEFAULT_WIDTH           (1280u) /*!< Default output image width. */

#define PLAYBACK_PRV_DEFAULT_SENSOR_ID       (0u)    /*!< Default Sensor ID for the camera */

#define PLAYBACK_PRV_DEFAULT_FOLDER_NAME     "./playback/playback.vraw" /*!< File name that contains the playback data. Can be relative or absolute. */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Enumeration of the different playback states the module can be in */
typedef enum {
    PLAYBACK_STREAM = 0,
    PLAYBACK_FRAME_WAITING_FOR_USER,
    PLAYBACK_FRAME_USER_PRESSED_KEY,
} e_playback_state_t;

/*! Configuration Struct for playback_vraw module */
typedef struct {
    e_image_format_t  image_format;      /*!< Format of image */
    uint32_t          image_height;      /*!< Height of image */
    uint32_t          image_width;       /*!< Width of image */
    uint32_t          sensor_id;         /*!< Id of the sensor */
    char filename[PLAYBACK_PRV_MAX_FILE_NAME_LENGTH]; /*!< Name of the file to playback */
} st_playback_cfg_t;

/*! Private data structure for playback_vraw module */
typedef struct st_priv_tag {
    st_playback_cfg_t  config;             /*!< Module configuration structure*/
    FILE *             p_file;             /*!< File pointer to playback file */
    uint32_t           loop_idx;           /*!< Index of how many time data has been looped through */
    e_playback_state_t state;              /*!< State of the module */
} st_priv_t; /*lint !e9109 !e761 */

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_instance);
static r_result_t R_Deinit(st_module_instance_t * const p_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_instance);

/*! Module interface definition for playback_vraw */
const st_module_interface_t g_module_interface_playback_vraw = {
    .module_name   = "playback_vraw",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = R_Deinit,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/***********************************************************************************************************************
 Local Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * @page    PQS_Playback_Vraw_Config  PQS Playback Video Raw Config
 * @ingroup PQS_Playback_Vraw
 * # Configuration Table:
 *
 *   Config Name  | Config Type      | Default                            | Range                                                    | Description
 *   ------------ | ---------------- | ---------------------------------- | ---------------------------------------------------------| ------------------
 *   image_format | e_image_format_t | R_IMAGE_FORMAT_UYVY_INTERLEAVED    | R_IMAGE_FORMAT_UYVY_INTERLEAVED                          | Image format
 *   image_height | uint32_t         | 800                                | 1 - @ref R_OBJECT_IMAGE_MAX_HEIGHT                       | Height of output image.
 *   image_width  | uint32_t         | 1280                               | 1 - @ref R_OBJECT_IMAGE_MAX_WIDTH                        | Width of output image.
 *   sensor_id    | uint32_t         | 0                                  | Any                                                      | ID of the sensor to set in object
 *   filename     | string           | "./playback/playback.vraw"         | Any                                                      | Name of the folder to read files from
 *
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_config
 * @brief           Initializes the configuration structure with either the default values or with values from a config
 *                  file if available.
 * @param [in,out]  p_config - pointer to the configuration structure
 * @param [in]      p_instance_name - String of the module instance name
 * @param [in]      p_module_name - String of the module name
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_config(st_playback_cfg_t * p_config, char const * const p_instance_name, char const * const p_module_name)
{
    r_result_t result = R_RESULT_SUCCESS;
    const char * format_names[] = E_IMAGE_FORMAT_AS_STRING;

    (void)p_module_name; //Not currently used;

    /* Update config */
    p_config->image_format = R_COMMON_CONFIG_GET_ENUM( p_instance_name, "image_format", PLAYBACK_PRV_DEFAULT_FORMAT, format_names, (sizeof(format_names))/(sizeof(format_names[0])), e_image_format_t); /*lint !e9030 !e9034 */
    p_config->image_height = R_COMMON_CONFIG_GetUint32(p_instance_name, "image_height", PLAYBACK_PRV_DEFAULT_HEIGHT);
    p_config->image_width  = R_COMMON_CONFIG_GetUint32(p_instance_name, "image_width",  PLAYBACK_PRV_DEFAULT_WIDTH);
    p_config->sensor_id    = R_COMMON_CONFIG_GetUint32(p_instance_name, "sensor_id",    PLAYBACK_PRV_DEFAULT_SENSOR_ID);
    result |= R_COMMON_STRING_Copy(p_config->filename, R_COMMON_CONFIG_GetString(p_instance_name, "filename", PLAYBACK_PRV_DEFAULT_FOLDER_NAME), PLAYBACK_PRV_MAX_FILE_NAME_LENGTH);

    /* Check config */
    if (p_config->image_height > R_OBJECT_IMAGE_MAX_HEIGHT)
    {
        R_PRINT_ERROR("Height must be less than R_OBJECT_IMAGE_MAX_HEIGHT");
        result = R_RESULT_FAILED;
    }

    if (p_config->image_width > R_OBJECT_IMAGE_MAX_WIDTH)
    {
        R_PRINT_ERROR("Width must be less than R_OBJECT_IMAGE_MAX_WIDTH");
        result = R_RESULT_FAILED;
    }

    if (0u == p_config->image_height )
    {
        R_PRINT_ERROR("Height cannot be zero!");
        result = R_RESULT_FAILED;
    }

    if (0u == p_config->image_width )
    {
        R_PRINT_ERROR("Width cannot be zero!");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * @page    PQS_Playback_Vraw_User  PQS Playback Video Raw User Input
 * @ingroup PQS_Playback_Vraw
 * # User Input Table:
 *
 *   Input Character  | Description                                       |
 *   ---------------- | ------------------------------------------------- |
 *   SPACE_BAR        | Pause/Continue playback                           |
 *   UP_ARROW         | Repeat the current frame                          |
 *   RIGHT_ARROW      | Play next frame in forward direction              |
 *   LEFT_ARROW       | Play next frame in reverse direction              |
 *
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_user_input
 * @brief           Registers the inputs users can use to interact with this module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_user_input(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_COMMON_USER_RegisterInput(p_instance, SPACE_BAR,   "Pause/Continue playback");
    result |= R_COMMON_USER_RegisterInput(p_instance, UP_ARROW,    "Repeat the current frame");
    result |= R_COMMON_USER_RegisterInput(p_instance, RIGHT_ARROW, "Play next frame in forward direction");
    result |= R_COMMON_USER_RegisterInput(p_instance, LEFT_ARROW,  "Play next frame in reverse direction");

    return result;
}


/******************************************************************************************************************//**
 * Function Name:   get_bytes_per_pixel
 * @brief           Returns the number of bytes per pixel for the given e_image_format_t format
 * @param [in]      format - Format of the image
 * @retval          bytes per pixel of format
 *********************************************************************************************************************/
static uint32_t get_bytes_per_pixel(e_image_format_t format)
{
    uint32_t bytes_per_pixel = 0;

    switch (format) /*lint !e788 */
    {
        case R_IMAGE_FORMAT_UYVY_INTERLEAVED:
        {
            bytes_per_pixel = 2;
            break;
        }

        case R_IMAGE_FORMAT_RGB32_INTERLEAVED:
        case R_IMAGE_FORMAT_UYVA_INTERLEAVED:
        {
            bytes_per_pixel = 4;
            break;
        }

        default:
        {
            R_PRINT_ERROR("Fell through switch. Format not supported.");
            break;
        }
    }

    return bytes_per_pixel;
}

/******************************************************************************************************************//**
 * Function Name:   set_header
 * @brief           Sets the object header (timestamp and sensor id)
 * @param [in]      p_priv - Module private data
 * @param [out]     p_object - object to set properties in
 * @param [in]      fname - Name of the playabck file
 * @retval          R_RESULT_SUCCESS on success
 *********************************************************************************************************************/
static r_result_t set_header(st_priv_t * const p_priv,  st_object_t * const p_object, const char * const fname)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Set time stamp and sensor of image object */
    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        result |= R_OBJECT_SetSensorId( p_object, p_priv->config.sensor_id);
        result |= R_OBJECT_SetTimeStamp(p_object, R_COMMON_TIME_GetRealUsec());
        result |= R_OBJECT_SetDescription(p_object, fname);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Error setting header info");
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   set_image_properties
 * @brief           Sets the  image properties in the supplied object
 * @param [in]      p_priv - Module private data
 * @param [out]     p_object - object to set properties in
 * @retval          R_RESULT_SUCCESS on success
 *********************************************************************************************************************/
static r_result_t set_image_properties(st_priv_t * const p_priv, st_object_t * const p_object)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Image object properties */
    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        result |= R_OBJECT_SetImageHeight(       p_object, p_priv->config.image_height);
        result |= R_OBJECT_SetImageWidth(        p_object, p_priv->config.image_width);
        result |= R_OBJECT_SetImageDepth(        p_object, 1);
        result |= R_OBJECT_SetImageBytesPerPixel(p_object, get_bytes_per_pixel(p_priv->config.image_format));
        result |= R_OBJECT_SetImageFormat(       p_object, p_priv->config.image_format);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Error setting header info");
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   process_user_input
 * @brief           processes the given user input, changing the playback state and playback index appropriately
 * @param [in]      p_priv - Module private data
 * @param [in]      user_input - user input to process
 * @retval          R_RESULT_SUCCESS on success
 *********************************************************************************************************************/
static r_result_t process_user_input(st_priv_t * p_priv, const char user_input)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NULL == p_priv)
    {
        R_PRINT_ERROR("No private data");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* Toggle playback mode */
        if (SPACE_BAR == user_input)
        {
            if (PLAYBACK_STREAM == p_priv->state)
            {
                p_priv->state = PLAYBACK_FRAME_WAITING_FOR_USER;
            }
            else
            {
                p_priv->state = PLAYBACK_STREAM;
            }
        }
        else
        {
            if (PLAYBACK_STREAM != p_priv->state)
            {
                int64_t image_size = (int32_t)((int32_t)p_priv->config.image_height *
                                               (int32_t)p_priv->config.image_width *
                                               (int32_t)get_bytes_per_pixel(p_priv->config.image_format));

                /* Note:
                 *
                 * The fread() function used to read files automatically updates the position indicator. This means to read
                 * the next file we do not need to update the indicator. To replay the same image we need to reverse the
                 * position by one image size, and to play the previous image we need to reverse the position by two
                 * time the image size.
                 */

                switch (user_input)
                {
                    /* Play next file */
                    case RIGHT_ARROW: /*lint !e9127 */
                    {
                        p_priv->state = PLAYBACK_FRAME_USER_PRESSED_KEY;
                        break;
                    }

                    /* Play previous file */
                    case LEFT_ARROW: /*lint !e9127 */
                    {
                        /* Check if we are near the beginning of the file */
                        if (ftell(p_priv->p_file) < (2 * image_size))
                        {
                            /* Move to the last image in the file */
                            if (fseek(p_priv->p_file, (-1) * image_size, SEEK_END) != 0)
                            {
                                R_PRINT_ERROR("Error finding file");
                                result = R_RESULT_FAILED;
                            }

                        }
                        else
                        {
                            /* Move back two images from the current location */
                            if (fseek(p_priv->p_file, (-2) * image_size, SEEK_CUR) != 0)
                            {
                                R_PRINT_ERROR("Error finding file");
                                result = R_RESULT_FAILED;
                            }
                        }

                        p_priv->state = PLAYBACK_FRAME_USER_PRESSED_KEY;
                        break;
                    }

                    /* Play same file again */
                    case UP_ARROW: /*lint !e9127 */
                    {
                        /* Reset to the last image again */
                        if (fseek(p_priv->p_file, (-1) * image_size, SEEK_CUR) != 0)
                        {
                            R_PRINT_ERROR("Error finding file");
                            result = R_RESULT_FAILED;
                        }

                        p_priv->state = PLAYBACK_FRAME_USER_PRESSED_KEY;
                        break;
                    }

                    default:
                    {
                        R_PRINT_ERROR("User input %c not supported", user_input);
                        result = R_RESULT_FAILED;
                        break;
                    }
                }
            }
            else
            {
                R_PRINT_WARNING("Playback is currently in stream mode. Press space bar to pause");
            }
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   read_file
 * @brief           Reads the file from memory, will try to gracefully handle if the end of file is reached, by
 *                  wrapping around to the beginning of the file
 * @param [in]      p_file - Pointer to the file to read from
 * @param [out]     p_image - Pointer to the image plane to read data into
 * @param [in]      image_size - Size of the image to read
 * @retval          R_RESULT_SUCCESS on success
 *********************************************************************************************************************/
static r_result_t read_file(FILE * p_file, uint8_t * p_image, uint32_t image_size)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Read file */
    if (fread(p_image, image_size, 1, p_file) != 1u) /*lint !e668 */
    {
        /* Check if we have reached the end of the file */
        if (feof(p_file) != 0)
        {
            /* Reset to the beginning of the file */
            if (fseek(p_file, 0, SEEK_SET) == 0)
            {
                /* Try the read again */
                if (fread(p_image, image_size, 1, p_file) != 1u) /*lint !e668 */
                {
                    R_PRINT_ERROR("Error reading file");
                    result = R_RESULT_FAILED;
                }
            }
            else
            {
                R_PRINT_ERROR("Error updating file position");
                result = R_RESULT_FAILED;
            }
        }
        else
        {
            /* If fread() returned an error and we are not at the end of the file then this
             * is an error we should print to screen
             */
            R_PRINT_ERROR("Error reading file");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/***********************************************************************************************************************
 Module Interface Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    r_result_t result  = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);
    char const * const p_instance_name = R_MODULE_GetInstanceName(p_instance);
    char const * const p_module_name   = R_MODULE_GetModuleName(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("No private data");
        result = R_RESULT_FAILED;
    }

    /* Init configuration */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_config(&p_data->config, p_instance_name, p_module_name) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Configuration init has failed");
            result = R_RESULT_FAILED;
        }
    }

    /* Register user input */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_user_input(p_instance) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("user input init has failed");
            result = R_RESULT_FAILED;
        }
    }

    /* Init private data struct */
    if (R_RESULT_SUCCESS == result)
    {
        /* Fill in the rest of private data struct */
        p_data->loop_idx = 1;
        p_data->state    = PLAYBACK_FRAME_WAITING_FOR_USER;

        p_data->p_file = fopen(p_data->config.filename, "rb");

        if (NULL == p_data->p_file)
        {
            R_PRINT_ERROR("Error opening file %s", p_data->config.filename);
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Playback_Vraw_Channels PQS Playback Video Raw Channels
 * @ingroup    PQS_Playback_Vraw
 * # Channel Table:
 *
 *  Channel name | Channel Type | Object Type  | Description
 *  ------------ | ------------ | ------------ | ----------------------------------
 *  data         | output       | image        | @ref PQS_Playback_Vraw_Channel_data
 *  trigger      | trigger      | trigger      | @ref PQS_Playback_Vraw_Channel_trigger
 *
 * @anchor PQS_Playback_Vraw_Channel_data
 * ## PQS Playback Video Raw Channel data
 *             This is the output image containing the image frame that is read from memory. The height, width,
 *             and format of the image is configurable, but must match the properties of the raw frame in memory.
 *
 * @anchor PQS_Playback_Vraw_Channel_trigger
 * ## PQS Playback Video Raw Channel trigger
 *             This is the trigger for the playback module. Any object can be routed to this channel to trigger
 *             the playback module to read and output a single frame. The output data of the playback module can be routed
 *             to this channel to keep a continuous stream of images going. Another option is to route the
 *             final output object of a given processing pipeline into this channel to assure the pipeline is only
 *             processing one image at a time. This channel must be kicked off by an external trigger in the graph
 *             file.
 *
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_BROKER_InitChannelInfo(p_instance, PLAYBACK_PRV_CHANNEL_OUT,     "data",    R_OBJECT_TYPE_IMAGE,   R_BROKER_DATA_DIRECTION_OUT);
    result |= R_BROKER_InitChannelInfo(p_instance, PLAYBACK_PRV_CHANNEL_TRIGGER, "trigger", R_OBJECT_TYPE_TRIGGER, R_BROKER_DATA_DIRECTION_IN);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_Deinit
 * @brief           Deinitializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Deinit(st_module_instance_t *const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t *p_data = R_MODULE_GetPrivDataPtr(p_instance);

    /* Check private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    if (0 != fclose(p_data->p_file)) /*lint !e482 */
    {
        R_PRINT_ERROR("Error closing file %s", p_data->config.filename);
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    st_priv_t *   p_data   = R_MODULE_GetPrivDataPtr(p_instance);
    st_object_t * p_object = R_BROKER_GetObjectPtr(p_instance, PLAYBACK_PRV_CHANNEL_OUT);
    r_result_t result = R_RESULT_SUCCESS;
    char user_input = USER_INPUT_NOT_AVAILABLE;
    uint8_t * p_image = NULL;
    uint32_t image_size = 0;

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Check object */
    if (R_OBJECT_CheckObjectType(p_object, R_OBJECT_TYPE_IMAGE) != R_RESULT_SUCCESS)
    {
        R_PRINT_ERROR("Invalid object input");
        result = R_RESULT_FAILED;
    }

    /* Get data plane */
    if (R_RESULT_SUCCESS == result)
    {
        p_image = R_OBJECT_GetImagePlaneCPU(p_object);

        if (NULL == p_image)
        {
            R_PRINT_ERROR("Error getting data plane");
            result = R_RESULT_FAILED;
        }
    }

    /* Get user input */
    R_TIMER_LogMessage(p_instance, "Get user input");
    if (R_RESULT_SUCCESS == result)
    {
        user_input = R_COMMON_USER_GetInput(p_instance);

        /* Process all pending inputs */
        while (USER_INPUT_NOT_AVAILABLE != user_input)
        {
            (void)process_user_input(p_data, user_input);
            user_input = R_COMMON_USER_GetInput(p_instance);
        }

        /* Check if we are still waiting for a user input */
        while (PLAYBACK_FRAME_WAITING_FOR_USER == p_data->state)
        {
            (void)usleep(10000);

            user_input = R_COMMON_USER_GetInput(p_instance);

            if (USER_INPUT_NOT_AVAILABLE != user_input)
            {
                (void)process_user_input(p_data, user_input);
            }
        }

        /* If the user pressed a key set the waiting state again */
        if (PLAYBACK_FRAME_USER_PRESSED_KEY == p_data->state)
        {
            p_data->state = PLAYBACK_FRAME_WAITING_FOR_USER;
        }
    }

    /* Fill image object info */
    if (R_RESULT_SUCCESS == result)
    {
        if (set_image_properties(p_data, p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Couldn't fill header data");
            result = R_RESULT_FAILED;
        }
    }

    /* Read file */
    R_TIMER_LogMessage(p_instance, "Read file");
    if (R_RESULT_SUCCESS == result)
    {
        image_size = p_data->config.image_height * p_data->config.image_width * get_bytes_per_pixel(p_data->config.image_format);

        /* Read file */
        if (read_file(p_data->p_file, p_image, image_size) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error reading file");
            result = R_RESULT_FAILED;
        }
    }

    /* Fill object header info */
    if (R_RESULT_SUCCESS == result)
    {
        if (set_header(p_data, p_object, p_data->config.filename) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Couldn't fill header data");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}
