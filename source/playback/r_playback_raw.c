/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_playback_raw.c
 * @brief         Source file for playback of raw objects. See @ref PQS_Playback_Raw for more details
 *********************************************************************************************************************/
 /******************************************************************************************************************//**
 * @page       PQS_Playback_Raw_Algorithm  PQS Playback Raw Algorithm
 * @ingroup    PQS_Playback_Raw
 * # Algorithm Description:
 *             This module reads raw objects from a folder to begin a processing pipeline. All of the files in the
 *             configured playback folder are analyzed during module initialization. The files with the @ref
 *             PLAYBACK_PRV_RAW_FILE_EXTENSION will be played back in alphabetical order. The objects must be in a "raw"
 *             format, meaning that the files only contain the object data itself, without any header information. Once
 *             the module reaches the end of the playback dataset it will loop to the beginning and start playback
 *             again. The user can interact with this module via different command line keystrokes.
 *             The module handles different types of objects, it is possible to select which type is the desired one
 *             by specifying it in the module configuration file, more details available here: @ref
 *             PQS_Playback_Raw_Config .
 *
 * # Block Diagram:
 *             @image html playback_raw.png
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include <unistd.h> /* For usleep */
#include <dirent.h> /* For scandir */

#include "r_common.h"
#include "r_object_image.h"
#include "r_object_feature_map.h"
#include "r_playback_raw.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define PLAYBACK_PRV_CHANNEL_OUT            (0u)    /*!< Broker Channel for output image */
#define PLAYBACK_PRV_CHANNEL_TRIGGER        (1u)    /*!< Broker Channel for input trigger */

#define PLAYBACK_PRV_MAX_FOLDER_NAME_LENGTH (256u)  /*!< Maximum length for a folder name */
#define PLAYBACK_PRV_MAX_FILE_NAME_LENGTH   (256u)  /*!< Maximum length for the file name */

#define PLAYBACK_PRV_FILE_EXTENSION         "raw"   /*!< Extension of the files to be read from disk: "raw" @anchor PLAYBACK_PRV_RAW_FILE_EXTENSION */

#define PLAYBACK_PRV_DEFAULT_IMG_FORMAT     (R_IMAGE_FORMAT_UYVY_INTERLEAVED)  /*!< Default output format for image types */
#define PLAYBACK_PRV_DEFAULT_FTR_MAP_FORMAT (R_FEATURE_MAP_FORMAT_RAW)         /*!< Default output format for feature_map types */

#define PLAYBACK_PRV_DEFAULT_IMG_HEIGHT     (800u)  /*!< Default output height for image types */
#define PLAYBACK_PRV_DEFAULT_IMG_WIDTH      (1280u) /*!< Default output width for image types */
#define PLAYBACK_PRV_DEFAULT_FTR_MAP_HEIGHT (256u)  /*!< Default output height for feature_map types */
#define PLAYBACK_PRV_DEFAULT_FTR_MAP_WIDTH  (512u)  /*!< Default output width for feature_map types */

#define PLAYBACK_PRV_DEFAULT_SKIP           (0u)    /*!< Default skip value. */

#define PLAYBACK_PRV_DEFAULT_SENSOR_ID      (0u)    /*!< Default Sensor ID */

#define PLAYBACK_PRV_DEFAULT_FOLDER_NAME    "./playback" /*!< Folder name that contains the playback data. Can be relative or absolute. */

#define PLAYBACK_PRV_DEFAULT_START_INDEX    (0u)    /*!< Default start index for the playback data. */
#define PLAYBACK_PRV_DEFAULT_END_INDEX      (0u)    /*!< Default end index for the playback data (0 means play all the files). */

#define PLAYBACK_PRV_DEFAULT_OBJECT_TYPE    (R_OBJECT_TYPE_IMAGE) /*!< Default object type name to be played back. */


/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Enumeration of the different playback states the module can be in */
typedef enum {
    PLAYBACK_STREAM = 0,
    PLAYBACK_FRAME_WAITING_FOR_USER,
    PLAYBACK_FRAME_USER_PRESSED_KEY,
} e_playback_state_t;

/*! Configuration Struct for image type settings */
typedef struct {
    e_image_format_t  format;      /*!< Format of image */
    uint32_t          height;      /*!< Height of image */
    uint32_t          width;       /*!< Width of image */
} st_playback_image_cfg_t;

/*! Configuration Struct for feature_map type settings */
typedef struct {
    e_map_format_t    format;      /*!< Format of feature_map */
    uint32_t          height;      /*!< Height of feature_map */
    uint32_t          width;       /*!< Width of ifeature_mapmage */
} st_playback_feature_map_cfg_t;

/*! Configuration Struct for playback_raw module */
typedef struct {
    e_object_type_t                 object_type;            /*!< Type of the object to be played back */
    st_playback_image_cfg_t         image_type_cfg;         /*!< Configuration for image type */
    st_playback_feature_map_cfg_t   feature_map_type_cfg;   /*!< Configuration for feature_map type */
    uint32_t                        skip;                   /*!< How many frames to skip in the data set, 0 means play all, 1 means playback one skip one. 2 means playback one skip two, etc. */
    uint32_t                        sensor_id;              /*!< Id of the sensor */
    uint32_t                        start_index;            /*!< Starting index of the playback loop */
    uint32_t                        end_index;              /*!< Ending index of the playback loops */
    char folder_name[PLAYBACK_PRV_MAX_FOLDER_NAME_LENGTH];  /*!< Name of the folder containing playback data */
} st_playback_cfg_t;

/*! Private data structure for playback_raw module */
typedef struct st_priv_tag {
    st_playback_cfg_t  config;             /*!< Module configuration structure*/
    struct dirent **   name_list;          /*!< List of all the files in the playback folder */
    uint32_t           num_files;          /*!< Number of files in the playback folder */
    uint32_t           playback_idx;       /*!< Index of current capture */
    uint32_t           loop_idx;           /*!< Index of how many time data has been looped through */
    e_playback_state_t state;              /*!< State of the module */
} st_priv_t; /*lint !e9109 !e761 */

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_instance);
static r_result_t R_Deinit(st_module_instance_t * const p_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_instance);

/*! Module interface definition for playback_raw */
const st_module_interface_t g_module_interface_playback_raw = {
    .module_name   = "playback_raw",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = R_Deinit,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/***********************************************************************************************************************
 Local Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * @page    PQS_Playback_Raw_Config  PQS Playback Raw Config
 * @ingroup PQS_Playback_Raw
 * # Configuration Table:
 *
 *   Config Name                     || Config Type      | Default                         | Range                                                                                                                                                | Description
 *   --------------------------------|| ---------------- | ------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------
 *   object_type                     || e_object_type_t  | R_OBJECT_TYPE_IMAGE             | @ref e_object_type_t (currently only R_OBJECT_TYPE_IMAGE and R_OBJECT_TYPE_FEATURE_MAP are supported)                                                | Specifies the type of the output object to be played back
 *   Image       |       image_format | e_image_format_t | R_IMAGE_FORMAT_UYVY_INTERLEAVED | @ref e_image_format_t                                                                                                                                | Image format for the image object type
 *   ^           |       image_height | uint32_t         | 800                             | 1 - @ref R_OBJECT_IMAGE_MAX_HEIGHT                                                                                                                   | Height of output image for the image object type
 *   ^           |        image_width | uint32_t         | 1280                            | 1 - @ref R_OBJECT_IMAGE_MAX_WIDTH                                                                                                                    | Width of output image for the image object type
 *   Feature Map | feature_map_format | e_map_format_t   | R_FEATURE_MAP_FORMAT_RAW        | @ref e_map_format_t                                                                                                                                  | Feature map format for the feature_map object type
 *   ^           | feature_map_height | uint32_t         | 256                             | 1 - @ref R_OBJECT_FEATURE_MAP_MAX_HEIGHT                                                                                                             | Height of output feature map for the feature_map object type
 *   ^           |  feature_map_width | uint32_t         | 512                             | 1 - @ref R_OBJECT_FEATURE_MAP_MAX_WIDTH                                                                                                              | Width of output feature map for the feature_map object type
 *   skip                            || uint32_t         | 0                               | Any                                                                                                                                                  | How many objects in the dataset to skip between playing back objects
 *   sensor_id                       || uint32_t         | 0                               | Any                                                                                                                                                  | ID of the sensor to set in object
 *   start_index                     || uint32_t         | 1                               | Must be less than or equal to the number of files in playback folder                                                                                 | Index of the file to start playback data with
 *   end_index                       || uint32_t         | 0                               | Must be greater or equal to start index and smaller or equal to the number of files, or it can be set to 0 (all available files will be played back) | Index of the file to stop playack with
 *   folder_name                     || string           | "./playback"                    | Any                                                                                                                                                  | Name of the folder to read files from
 *
 * # Playback Skip:
 *            This module can be set up to skip files in the playback data set. This is useful if the data set is recorded
 *            at a higher frame per second than what the desired pipeline operates at. skip = 0 means do not skip any image,
 *            skip = 1 means skip every other image, skip = 2 means read one image then skip two, etc.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_config
 * @brief           Initializes the configuration structure with either the default values or with values from a config
 *                  file if available.
 * @param [in,out]  p_config - pointer to the configuration structure
 * @param [in]      p_instance_name - String of the module instance name
 * @param [in]      p_module_name - String of the module name
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_config(st_playback_cfg_t * p_config, char const * const p_instance_name, char const * const p_module_name)
{
    r_result_t result = R_RESULT_SUCCESS;
    const char * object_types[] = E_OBJECT_TYPE_AS_STRING;
    const char * image_format_names[] = E_IMAGE_FORMAT_AS_STRING;
    const char * feature_map_format_names[] = E_MAP_FORMAT_AS_STRING;

    (void)p_module_name; //Not currently used

    /* Update config */
    p_config->object_type                   = R_COMMON_CONFIG_GET_ENUM( p_instance_name, "object_type",         PLAYBACK_PRV_DEFAULT_OBJECT_TYPE, object_types, (sizeof(object_types))/(sizeof(object_types[0])), e_object_type_t); /*lint !e9030 !e9034 */
    p_config->image_type_cfg.format         = R_COMMON_CONFIG_GET_ENUM( p_instance_name, "image_format",        PLAYBACK_PRV_DEFAULT_IMG_FORMAT, image_format_names, (sizeof(image_format_names))/(sizeof(image_format_names[0])), e_image_format_t); /*lint !e9030 !e9034 */
    p_config->image_type_cfg.height         = R_COMMON_CONFIG_GetUint32(p_instance_name, "image_height",        PLAYBACK_PRV_DEFAULT_IMG_HEIGHT);
    p_config->image_type_cfg.width          = R_COMMON_CONFIG_GetUint32(p_instance_name, "image_width",         PLAYBACK_PRV_DEFAULT_IMG_WIDTH);
    p_config->feature_map_type_cfg.format   = R_COMMON_CONFIG_GET_ENUM( p_instance_name, "feature_map_format",  PLAYBACK_PRV_DEFAULT_FTR_MAP_FORMAT, feature_map_format_names, (sizeof(feature_map_format_names))/(sizeof(feature_map_format_names[0])), e_map_format_t); /*lint !e9030 !e9034 */
    p_config->feature_map_type_cfg.height   = R_COMMON_CONFIG_GetUint32(p_instance_name, "feature_map_height",  PLAYBACK_PRV_DEFAULT_FTR_MAP_HEIGHT);
    p_config->feature_map_type_cfg.width    = R_COMMON_CONFIG_GetUint32(p_instance_name, "feature_map_width",   PLAYBACK_PRV_DEFAULT_FTR_MAP_WIDTH);
    p_config->skip                          = R_COMMON_CONFIG_GetUint32(p_instance_name, "skip",                PLAYBACK_PRV_DEFAULT_SKIP);
    p_config->sensor_id                     = R_COMMON_CONFIG_GetUint32(p_instance_name, "sensor_id",           PLAYBACK_PRV_DEFAULT_SENSOR_ID);
    p_config->start_index                   = R_COMMON_CONFIG_GetUint32(p_instance_name, "start_index",         PLAYBACK_PRV_DEFAULT_START_INDEX);
    p_config->end_index                     = R_COMMON_CONFIG_GetUint32(p_instance_name, "end_index",           PLAYBACK_PRV_DEFAULT_END_INDEX);
    result |= R_COMMON_STRING_Copy(p_config->folder_name, R_COMMON_CONFIG_GetString(p_instance_name, "folder_name", PLAYBACK_PRV_DEFAULT_FOLDER_NAME), PLAYBACK_PRV_MAX_FOLDER_NAME_LENGTH);

    /* The first file names listed by scandir are:
     * filename[0] = "."
     * filename[1] = ".."
     * filename[2] = "first_actual_file.raw"
     * ...
     * but it easier for the user to consider 1 as the index for the first proper file rather than 2. For such reason
     * the starting value specified by the user in the config file is here increased by 1 and from now onward it will
     * represent the index in the list of file names returned by scandir.
     */
    p_config->start_index += 1u;

    /* Same for end_index. If end_index is 0 it means that all the files must be played back, therefore such value
     * won't be changed. */
    if (p_config->end_index > 0u)
    {
        p_config->end_index += 1u;
    }

    /* Only R_OBJECT_TYPE_IMAGE and R_OBJECT_TYPE_FEATURE_MAP are supported for now */
    switch (p_config->object_type) /*lint !e788 */
    {
        case R_OBJECT_TYPE_IMAGE:
        case R_OBJECT_TYPE_FEATURE_MAP:
        {
            result = R_RESULT_SUCCESS;
            break;
        }

        default:
        {
            R_PRINT_ERROR("Config object type not supported.");
            result = R_RESULT_FAILED;
            break;
        }
    }

    /* Check image configuration validity */
    if (p_config->image_type_cfg.height > R_OBJECT_IMAGE_MAX_HEIGHT)
    {
        R_PRINT_ERROR("Height must be less than R_OBJECT_IMAGE_MAX_HEIGHT");
        result = R_RESULT_FAILED;
    }

    if (p_config->image_type_cfg.width > R_OBJECT_IMAGE_MAX_WIDTH)
    {
        R_PRINT_ERROR("Width must be less than R_OBJECT_IMAGE_MAX_WIDTH");
        result = R_RESULT_FAILED;
    }

    if (0u == p_config->image_type_cfg.height )
    {
        R_PRINT_ERROR("Height cannot be zero!");
        result = R_RESULT_FAILED;
    }

    if (0u == p_config->image_type_cfg.width )
    {
        R_PRINT_ERROR("Width cannot be zero!");
        result = R_RESULT_FAILED;
    }

    /* Check feature map configuration validity */
    if (p_config->feature_map_type_cfg.height > R_OBJECT_FEATURE_MAP_MAX_HEIGHT)
    {
        R_PRINT_ERROR("Height must be less than R_OBJECT_FEATURE_MAP_MAX_HEIGHT");
        result = R_RESULT_FAILED;
    }

    if (p_config->feature_map_type_cfg.width > R_OBJECT_FEATURE_MAP_MAX_WIDTH)
    {
        R_PRINT_ERROR("Width must be less than R_OBJECT_FEATURE_MAP_MAX_WIDTH");
        result = R_RESULT_FAILED;
    }

    if (0u == p_config->feature_map_type_cfg.height )
    {
        R_PRINT_ERROR("Height cannot be zero!");
        result = R_RESULT_FAILED;
    }

    if (0u == p_config->feature_map_type_cfg.width )
    {
        R_PRINT_ERROR("Width cannot be zero!");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * @page    PQS_Playback_Raw_User  PQS Playback Raw User Input
 * @ingroup PQS_Playback_Raw
 * # User Input Table:
 *
 *   Input Character  | Description                                       |
 *   ---------------- | ------------------------------------------------- |
 *   SPACE_BAR        | Toggle between stream and frame by frame playback |
 *   UP_ARROW         | Repeat the current frame                          |
 *   RIGHT_ARROW      | Play next frame in forward direction              |
 *   LEFT_ARROW       | Play next frame in reverse direction              |
 *
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_user_input
 * @brief           Registers the inputs users can use to interact with this module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_user_input(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_COMMON_USER_RegisterInput(p_instance, SPACE_BAR,   "Toggle between stream and frame by frame playback");
    result |= R_COMMON_USER_RegisterInput(p_instance, UP_ARROW,    "Repeat the current frame");
    result |= R_COMMON_USER_RegisterInput(p_instance, RIGHT_ARROW, "Play next frame in forward direction");
    result |= R_COMMON_USER_RegisterInput(p_instance, LEFT_ARROW,  "Play next frame in reverse direction");

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   check_filetype
 * @brief           Analyzes the filename of the next file to be playback and checks if it has the correct extension
 * @param [in]      p_priv - Module private data
 * @retval          R_RESULT_SUCCESS if the file has the correct extension
 *********************************************************************************************************************/
static r_result_t check_filetype(st_priv_t * const p_priv)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_word_list_t split_filename = {{{'\0'}}, 0};

    /* Note: during normal operation this function will return R_RESULT_FAILED. Two of the file names
     * return by scandir() are the .  and .. directories. We need to ignore these.
     */

    /* Split the file name to get the extension */
    result |= R_COMMON_STRING_Split(p_priv->name_list[p_priv->playback_idx]->d_name, &split_filename, '.');

    /* Check number of words in file name */
    if (R_RESULT_SUCCESS == result)
    {
        if (2u != split_filename.num_words)
        {
            R_PRINT_DEBUG("Ignoring file. Expected file name to be *.%s actual file name = %s",
                    PLAYBACK_PRV_FILE_EXTENSION, p_priv->name_list[p_priv->playback_idx]->d_name);
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (R_COMMON_STRING_Compare(split_filename.word_list[1], PLAYBACK_PRV_FILE_EXTENSION) != R_RESULT_SUCCESS)
        {
            R_PRINT_DEBUG("Ignoring file. Expected file name to be *.%s actual file name = %s",
                    PLAYBACK_PRV_FILE_EXTENSION, p_priv->name_list[p_priv->playback_idx]->d_name);
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   get_filename
 * @brief           Fills in the supplied filename string with the combined folder and filename of the object to
 *                  playback next
 * @param [in]      p_priv - Module private data
 * @param [in]      fname_size - Size of the supplied fname string
 * @param [out]     fname - Name of the file to playback
 * @retval          R_RESULT_SUCCESS on success
 *********************************************************************************************************************/
static r_result_t get_filename(st_priv_t * const p_priv, const uint32_t fname_size, char * const fname)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Assemble full file name including directory */
    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        R_COMMON_Memset(fname, 0, fname_size);

        result |= R_COMMON_STRING_Combine(fname, p_priv->config.folder_name, fname_size);
        result |= R_COMMON_STRING_AppendIfNeeded(fname, '/', fname_size);
        result |= R_COMMON_STRING_Combine(fname, p_priv->name_list[p_priv->playback_idx]->d_name, fname_size);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Error building complete file name");
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   get_bytes_per_pixel
 * @brief           Returns the number of bytes per pixel for the given e_image_format_t format
 * @param [in]      format - Format of the image
 * @retval          bytes per pixel of format
 *********************************************************************************************************************/
static uint32_t get_bytes_per_pixel(e_image_format_t format)
{
    uint32_t bytes_per_pixel = 0;

    switch (format) /*lint !e788 */
    {
        case R_IMAGE_FORMAT_UYVY_INTERLEAVED:
        {
            bytes_per_pixel = 2;
            break;
        }

        case R_IMAGE_FORMAT_RGB32_INTERLEAVED:
        case R_IMAGE_FORMAT_UYVA_INTERLEAVED:
        {
            bytes_per_pixel = 4;
            break;
        }

        default:
        {
            R_PRINT_ERROR("Fell through switch. Format not supported.");
            break;
        }
    }

    return bytes_per_pixel;
}

/******************************************************************************************************************//**
 * Function Name:   set_header
 * @brief           Sets the object header (timestamp and sensor id)
 * @param [in]      p_priv - Module private data
 * @param [out]     p_object - object to set properties in
 * @param [in]      fname - Name of the playabck file
 * @retval          R_RESULT_SUCCESS on success
 *********************************************************************************************************************/
static r_result_t set_header(st_priv_t * const p_priv,  st_object_t * const p_object, const char * const fname)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Set time stamp and sensor of image object */
    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        result |= R_OBJECT_SetSensorId( p_object, p_priv->config.sensor_id);
        result |= R_OBJECT_SetTimeStamp(p_object, R_COMMON_TIME_GetRealUsec());
        result |= R_OBJECT_SetDescription(p_object, fname);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Error setting header info");
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   set_image_properties
 * @brief           Sets the  image properties in the supplied object
 * @param [in]      p_priv - Module private data
 * @param [out]     p_object - object to set properties in
 * @retval          R_RESULT_SUCCESS on success
 *********************************************************************************************************************/
static r_result_t set_image_properties(st_priv_t * const p_priv, st_object_t * const p_object)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Image object properties */
    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        result |= R_OBJECT_SetImageHeight(       p_object, p_priv->config.image_type_cfg.height);
        result |= R_OBJECT_SetImageWidth(        p_object, p_priv->config.image_type_cfg.width);
        result |= R_OBJECT_SetImageDepth(        p_object, 1);
        result |= R_OBJECT_SetImageBytesPerPixel(p_object, get_bytes_per_pixel(p_priv->config.image_type_cfg.format));
        result |= R_OBJECT_SetImageFormat(       p_object, p_priv->config.image_type_cfg.format);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Error setting header info");
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   set_feature_map_properties
 * @brief           Sets the feature map properties in the supplied object
 * @param [in]      p_priv - Module private data
 * @param [out]     p_object - object to set properties in
 * @retval          R_RESULT_SUCCESS on success
 *********************************************************************************************************************/
static r_result_t set_feature_map_properties(st_priv_t * const p_priv, st_object_t * const p_object)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Set feature_map output object properties */
    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        result |= R_OBJECT_SetFeatureMapHeight(p_object, p_priv->config.feature_map_type_cfg.height);
        result |= R_OBJECT_SetFeatureMapWidth( p_object, p_priv->config.feature_map_type_cfg.width);
        result |= R_OBJECT_SetFeatureMapFormat(p_object, p_priv->config.feature_map_type_cfg.format);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Error setting header info");
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   set_object_properties
 * @brief           Sets the properties in the supplied object
 * @param [in]      p_priv - Module private data
 * @param [out]     p_object - object to set properties in
 * @retval          R_RESULT_SUCCESS on success
 *********************************************************************************************************************/
static r_result_t set_object_properties(st_priv_t * const p_priv, st_object_t * const p_object)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Set the output object properties depending on the object type */
    switch (p_priv->config.object_type) /*lint !e788 */
    {
        case R_OBJECT_TYPE_IMAGE:
        {
            result = set_image_properties(p_priv, p_object);
            break;
        }

        case R_OBJECT_TYPE_FEATURE_MAP:
        {
            result = set_feature_map_properties(p_priv, p_object);
            break;
        }

        default:
        {
            R_PRINT_ERROR("The configuration value for the object type name is not supported.");
            result = R_RESULT_FAILED;
            break;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   process_user_input
 * @brief           processes the given user input, changing the playback state and playback index appropriately
 * @param [in]      p_priv - Module private data
 * @param [in]      user_input - user input to process
 * @retval          R_RESULT_SUCCESS on success
 *********************************************************************************************************************/
static r_result_t process_user_input(st_priv_t * p_priv, const char user_input)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NULL == p_priv)
    {
        R_PRINT_ERROR("No private data");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* Toggle playback mode */
        if (SPACE_BAR == user_input)
        {
            if (PLAYBACK_STREAM == p_priv->state)
            {
                p_priv->state = PLAYBACK_FRAME_WAITING_FOR_USER;
            }
            else
            {
                p_priv->state = PLAYBACK_STREAM;
            }
        }
        else
        {
            if (PLAYBACK_STREAM != p_priv->state)
            {
                switch (user_input)
                {
                    /* Play next file */
                    case RIGHT_ARROW: /*lint !e9127 */
                    {
                        if (p_priv->playback_idx < p_priv->config.end_index)
                        {
                            (p_priv->playback_idx)++;
                        }
                        else
                        {
                            R_PRINT_INFO("We are at the last frame of the sequence. Looping to the start.");
                            p_priv->playback_idx = p_priv->config.start_index;
                        }

                        p_priv->state = PLAYBACK_FRAME_USER_PRESSED_KEY;
                        break;
                    }

                    /* Play previous file */
                    case LEFT_ARROW: /*lint !e9127 */
                    {
                        if (p_priv->playback_idx > p_priv->config.start_index)
                        {
                            (p_priv->playback_idx)--;
                        }
                        else
                        {
                            R_PRINT_INFO("We are at the first frame of the sequence. Looping to the end.");
                            p_priv->playback_idx = p_priv->config.end_index;
                        }

                        p_priv->state = PLAYBACK_FRAME_USER_PRESSED_KEY;
                        break;
                    }

                    /* Play same file again */
                    case UP_ARROW: /*lint !e9127 */
                    {
                        /* Don't change the capture id just change the state*/
                        p_priv->state = PLAYBACK_FRAME_USER_PRESSED_KEY;
                        break;
                    }

                    default:
                    {
                        R_PRINT_ERROR("User input %c not supported", user_input);
                        result = R_RESULT_FAILED;
                        break;
                    }
                }
            }
            else
            {
                R_PRINT_WARNING("Playback is currently in stream mode. Press space bar to pause");
            }
        }
    }

    return result;
}

/***********************************************************************************************************************
 Module Interface Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    r_result_t result  = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    /* Names */
    char const * const p_instance_name = R_MODULE_GetInstanceName(p_instance);
    char const * const p_module_name   = R_MODULE_GetModuleName(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("No private data");
        result = R_RESULT_FAILED;
    }

    /* Init configuration */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_config(&p_data->config, p_instance_name, p_module_name) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Configuration init has failed");
            result = R_RESULT_FAILED;
        }
    }

    /* Register user input */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_user_input(p_instance) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("user input init has failed");
            result = R_RESULT_FAILED;
        }
    }

    /* Init private data struct */
    if (R_RESULT_SUCCESS == result)
    {
        /* Scan the directory and build file name array */
        int32_t scandir_result = scandir(p_data->config.folder_name, &p_data->name_list, NULL, alphasort); /*lint !e9025 */

        /* Check number of files in directory */
        if (0 >= scandir_result)
        {
            R_PRINT_ERROR("Error parsing directory %s for playback files", p_data->config.folder_name);
            result = R_RESULT_FAILED;
        }
        else
        {
            /* Fill in the rest of private data struct */
            p_data->playback_idx  = p_data->config.start_index;
            p_data->loop_idx      = 1;
            p_data->state         = PLAYBACK_FRAME_WAITING_FOR_USER;
            p_data->num_files     = (uint32_t)scandir_result;
        }
    }

    /* Adapt configuration to number of files */
    if (R_RESULT_SUCCESS == result)
    {
        /* If the configuration value for end_index is 0, it means that all the files must be played back */
        if (0u == p_data->config.end_index)
        {
            p_data->config.end_index = p_data->num_files - 1u;
        }
        else
        {
            /* If a certain value has been specified for end_index, check if it's greater than the number of available files */
            if (p_data->config.end_index >= p_data->num_files)
            {
                R_PRINT_ERROR("Number of frames specified in configuration file (%u) is larger than the number of files found (%u).",
                        p_data->config.end_index, p_data->num_files);
            }

            /* In any case play the (available) data back up to the specified frame */
            p_data->config.end_index = MIN(p_data->config.end_index, p_data->num_files - 1u);
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Playback_Raw_Channels PQS Playback Raw Channels
 * @ingroup    PQS_Playback_Raw
 * # Channel Table:
 *
 *  Channel name | Channel Type | Object Type  | Description
 *  ------------ | ------------ | ------------ | ----------------------------------
 *  data         | output       | image        | @ref PQS_Playback_Raw_Channel_data
 *  trigger      | trigger      | trigger      | @ref PQS_Playback_Raw_Channel_trigger
 *
 * @anchor PQS_Playback_Raw_Channel_data
 * ## PQS Playback Raw Channel data
 *             This is the output image containing the image frame that is read from memory. The height, width,
 *             and format of the image is configurable, but must match the properties of the raw frame in memory.
 *
 * @anchor PQS_Playback_Raw_Channel_trigger
 * ## PQS Playback Raw Channel trigger
 *             This is the trigger for the playback module. Any object can be routed to this channel to trigger
 *             the playback module to read and output a single frame. The output data of the playback module can be routed
 *             to this channel to keep a continuous stream of images going. Another option is to route the
 *             final output object of a given processing pipeline into this channel to assure the pipeline is only
 *             processing one image at a time. This channel must be kicked off by an external trigger in the graph
 *             file.
 *
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    /* Check for NULL pointer */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    result |= R_BROKER_InitChannelInfo(p_instance, PLAYBACK_PRV_CHANNEL_TRIGGER, "trigger", R_OBJECT_TYPE_TRIGGER, R_BROKER_DATA_DIRECTION_IN);

    /* Specify the output object */
    if (R_RESULT_SUCCESS == result)
    {
        result = R_BROKER_InitChannelInfo(p_instance, PLAYBACK_PRV_CHANNEL_OUT, "data", p_data->config.object_type, R_BROKER_DATA_DIRECTION_OUT);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_Deinit
 * @brief           Deinitializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Deinit(st_module_instance_t *const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t *p_data = R_MODULE_GetPrivDataPtr(p_instance);

    /* Check private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    for (uint32_t i = 0; i < p_data->num_files; i++)
    {
        free(p_data->name_list[i]); /*lint !e586 */
    }

    free(p_data->name_list); /*lint !e586 */
    p_data->name_list = NULL;

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t *   p_data = R_MODULE_GetPrivDataPtr(p_instance);
    st_object_t * p_object = R_BROKER_GetObjectPtr(p_instance, PLAYBACK_PRV_CHANNEL_OUT);
    char fname[PLAYBACK_PRV_MAX_FILE_NAME_LENGTH] = {'\0'};
    char user_input = USER_INPUT_NOT_AVAILABLE;

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Check object */
    if (R_OBJECT_CheckObjectType(p_object, p_data->config.object_type) != R_RESULT_SUCCESS)
    {
        R_PRINT_ERROR("Invalid object input");
        result = R_RESULT_FAILED;
    }

    /* Get user input */
    R_TIMER_LogMessage(p_instance, "Get user input");
    if (R_RESULT_SUCCESS == result)
    {
        user_input = R_COMMON_USER_GetInput(p_instance);

        /* Process all pending inputs */
        while (USER_INPUT_NOT_AVAILABLE != user_input)
        {
            (void)process_user_input(p_data, user_input);
            user_input = R_COMMON_USER_GetInput(p_instance);
        }

        /* Check if we are still waiting for a user input */
        while (PLAYBACK_FRAME_WAITING_FOR_USER == p_data->state)
        {
            (void)usleep(10000);

            user_input = R_COMMON_USER_GetInput(p_instance);

            if (USER_INPUT_NOT_AVAILABLE != user_input)
            {
                (void)process_user_input(p_data, user_input);
            }
        }

        /* If the user pressed a key set the waiting state again */
        if (PLAYBACK_FRAME_USER_PRESSED_KEY == p_data->state)
        {
            p_data->state = PLAYBACK_FRAME_WAITING_FOR_USER;
        }
    }

    /* Fill object info */
    if (R_RESULT_SUCCESS == result)
    {
        if (set_object_properties(p_data, p_object) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Couldn't fill header data");
            result = R_RESULT_FAILED;
        }
    }

    /* Read file */
    R_TIMER_LogMessage(p_instance, "Read file");
    if (R_RESULT_SUCCESS == result)
    {
        /* Loop until we get the correct file type */
        while (check_filetype(p_data) != R_RESULT_SUCCESS)
        {
            p_data->playback_idx++;
        }

        if (get_filename(p_data, PLAYBACK_PRV_MAX_FILE_NAME_LENGTH, fname) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Couldn't create playback file name");
            result = R_RESULT_FAILED;
        }

        if (R_RESULT_SUCCESS == result)
        {
            R_PRINT_INFO("Playing back file %s", fname);
            if (R_OBJECT_ReadRaw(p_object, fname) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Couldn't process file '%s' for playback", fname);
                result = R_RESULT_FAILED;
            }
        }
    }

    /* If in stream mode, Increment capture ID */
    if (R_RESULT_SUCCESS == result)
    {
        if (PLAYBACK_STREAM == p_data->state)
        {
            p_data->playback_idx += (p_data->config.skip + 1u);

            /* Check if we are at the end of the data set */
            if (p_data->playback_idx > p_data->config.end_index)
            {
                R_PRINT_NOTICE("Looped though data %u times", p_data->loop_idx);
                p_data->playback_idx = p_data->config.start_index;
                p_data->loop_idx++;
            }
        }
    }

    /* Fill object header info */
    if (R_RESULT_SUCCESS == result)
    {
        if (set_header(p_data, p_object, fname) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Couldn't fill header data");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}
