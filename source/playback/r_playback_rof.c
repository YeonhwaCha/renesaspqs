/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_playback_rof.c
 * @brief         Source file for playback of ROF objects from disk. See @ref PQS_Playback_ROF for more details
 *********************************************************************************************************************/
 /******************************************************************************************************************//**
 * @page       PQS_Playback_ROF_Algorithm  PQS Playback ROF Algorithm
 * @ingroup    PQS_Playback_ROF
 * # Algorithm Description:
 *             This module reads ROF objects from a folder and inputs them into a processing pipeline. The objects
 *             must be in ROF (Renesas Object Format).  All of the files in the configured playback folder are analyzed
 *             during module initialization. The files with the @ref PLAYBACK_PRV_ROF_FILE_EXTENSION will be
 *             played back in alphabetical order. Once the module reaches the end of the playback data set it
 *             will loop to the beginning and start playback again.
 *
 * # Block Diagram:
 *             @image html playback_rof.png
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include <dirent.h> /* For scandir */

#include "r_common.h"
#include "r_playback_rof.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define PLAYBACK_PRV_CHANNEL_OUT             (0u)    /*!< Broker Channel for output image */
#define PLAYBACK_PRV_CHANNEL_TRIGGER         (1u)    /*!< Broker Channel for input trigger */

#define PLAYBACK_PRV_MAX_FOLDER_NAME_LENGTH  (256u)  /*!< Maximum length for a file name */
#define PLAYBACK_PRV_MAX_FILE_NAME_LENGTH    (256u)  /*!< Maximum length for the folder name */

#define PLAYBACK_PRV_FILE_EXTENSION          "rof"  /*!< Extension of the files to be read from disk: "rof" @anchor PLAYBACK_PRV_ROF_FILE_EXTENSION */

#define PLAYBACK_PRV_DEFAULT_START_INDEX     (2u)    /*!< Default start index for the playback data.
                                                     *   Set to 2 so we ignore the "." and ".." file names returned by scandir */

#define PLAYBACK_PRV_DEFAULT_FOLDER_NAME     "./playback" /*!< Folder name that contains the playback data. Can be relative or absolute. */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Configuration Struct for playback_rof module */
typedef struct {
    uint32_t    start_index; /*!< Starting index of the playback loop */
    uint32_t    end_index;   /*!< Ending index of the playback loops */
    char        folder_name[PLAYBACK_PRV_MAX_FOLDER_NAME_LENGTH]; /*!< Name of the folder containing playback data */
} st_playback_cfg_t;

/*! Private data structure for playback_rof module */
typedef struct st_priv_tag {
        st_playback_cfg_t  config;             /*!< Module configuration structure */
        struct dirent **   name_list;          /*!< List of all the files in the playback folder */
        int32_t            num_files;          /*!< Number of files in the playback folder */
        uint32_t           playback_idx;       /*!< Index of current capture */
        uint32_t           loop_idx;           /*!< Index of how many time data has been looped through */
} st_priv_t; /*lint !e9109 !e761 */

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_instance);
static r_result_t R_Deinit(st_module_instance_t * const p_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_instance);

/*! Module interface definition for playback_rof */
const st_module_interface_t g_module_interface_playback_rof = {
    .module_name   = "playback_rof",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = R_Deinit,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/***********************************************************************************************************************
 Local Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * @page    PQS_Playback_ROF_Config  PQS Playback ROF Config
 * @ingroup PQS_Playback_ROF
 * # Configuration Table:
 *
 *   Config Name  | Config Type      | Default                            | Range                                                    | Description
 *   ------------ | ---------------- | ---------------------------------- | ---------------------------------------------------------| ------------------
 *   start_index  | uint32_t         | 2                                  | Must be less than the number of files in playback folder | Index of the file to start playback data with
 *   end_index    | uint32_t         | Number of files in playback folder | Must be greater than start index                         | Index of the file to stop playack with
 *   folder_name  | string           | "./playback"                       | Any                                                      | Name of the folder to read files from
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_config
 * @brief           Initializes the configuration structure with either the default values or with values from a config
 *                  file if available.
 * @param [in,out]  p_config - pointer to the configuration structure
 * @param [in]      p_instance_name - String of the module instance name
 * @param [in]      p_module_name - String of the module name
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_config(st_playback_cfg_t * p_config, char const * const p_instance_name, char const * const p_module_name)
{
    r_result_t result = R_RESULT_SUCCESS;

    (void)p_module_name; //Not currently used

    /* Update config */
    p_config->start_index = R_COMMON_CONFIG_GetUint32(p_instance_name, "start_index", PLAYBACK_PRV_DEFAULT_START_INDEX);
    result |= R_COMMON_STRING_Copy(p_config->folder_name, R_COMMON_CONFIG_GetString(p_instance_name, "folder_name", PLAYBACK_PRV_DEFAULT_FOLDER_NAME), PLAYBACK_PRV_MAX_FOLDER_NAME_LENGTH);

    /* Note: end index is not set here, it it set after we have scanned the directory */

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   init_config_again
 * @brief           Initializes the configuration structure after the playback directory has been scanned. .
 * @param [in,out]  p_priv - pointer to the private data structure
 * @param [in]      p_instance_name - String of the module instance name
 * @param [in]      p_module_name - String of the module name
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_config_again(st_priv_t * p_priv, char const * const p_instance_name, char const * const p_module_name)
{
    r_result_t result = R_RESULT_SUCCESS;

    (void)p_module_name; //Not currently used

    /*
     * Note we need to break up the config init into two parts because scanning the directory requires the
     * p_config->folder_name be configured and initializing the p_config->end_index value requires the directory
     * to be scanned.
     */

    /* Configure end index based on number of files in directory */
    p_priv->config.end_index = R_COMMON_CONFIG_GetUint32(p_instance_name, "end_index", (uint32_t)p_priv->num_files);

    if (p_priv->config.start_index > p_priv->config.end_index)
    {
        R_PRINT_ERROR("Start index cannot be larger than end index");
        result = R_RESULT_FAILED;
    }

    return result;
}


/******************************************************************************************************************//**
 * Function Name:   get_filename
 * @brief           Fills in the supplied filename string with the combined folder and filename to of the image to
 *                  playback next
 * @param [in]      p_priv - Module private data
 * @param [in]      fname_size - Size of the supplied fname string
 * @param [out]     fname - Name of the file to playback
 * @retval          R_RESULT_SUCCESS on success
 *********************************************************************************************************************/
static r_result_t get_filename(st_priv_t * const p_priv, const uint32_t fname_size, char * const fname)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Assemble full file name including directory */
    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        R_COMMON_Memset(fname, 0, fname_size);
        result |= R_COMMON_STRING_Combine(fname, p_priv->config.folder_name, fname_size);
        result |= R_COMMON_STRING_Combine(fname, "/", fname_size);
        result |= R_COMMON_STRING_Combine(fname, p_priv->name_list[p_priv->playback_idx]->d_name, fname_size);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Error building complete file name");
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   check_filetype
 * @brief           Analyzes the filename of the next file to be playback and checks if it has the correct extension
 * @param [in]      p_priv - Module private data
 * @retval          R_RESULT_SUCCESS if the file has the correct extension
 *********************************************************************************************************************/
static r_result_t check_filetype(st_priv_t * const p_priv)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_word_list_t split_filename = {{{'\0'}}, 0};

    /* Note: during normal operation this function will return R_RESULT_FAILED. Two of the file names
     * return by scandir() are the .  and .. directories. We need to ignore these.
     */

    if (R_RESULT_SUCCESS == result) /*lint !e948 !e774 */
    {
        /* Split the file name to get the extension */
        if (R_COMMON_STRING_Split(p_priv->name_list[p_priv->playback_idx]->d_name, &split_filename, '.') != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error with manipulating file name");
            result = R_RESULT_FAILED;
        }
    }

    /* Check number of words in file name */
    if (R_RESULT_SUCCESS == result)
    {
        if (2u != split_filename.num_words)
        {
            R_PRINT_DEBUG("Ignoring file. Expected file name to be *.%s actual file name = %s", PLAYBACK_PRV_FILE_EXTENSION, p_priv->name_list[p_priv->playback_idx]->d_name);
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        if (R_COMMON_STRING_Compare(split_filename.word_list[1], PLAYBACK_PRV_FILE_EXTENSION) != R_RESULT_SUCCESS)
        {
            R_PRINT_DEBUG("Ignoring file. Expected file name to be *.%s actual file name = %s", PLAYBACK_PRV_FILE_EXTENSION, p_priv->name_list[p_priv->playback_idx]->d_name);
            result = R_RESULT_FAILED;
        }
    }

    return result;
}


/***********************************************************************************************************************
 Module Interface Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    r_result_t result  = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    /* MOdule names */
    char const * const p_instance_name = R_MODULE_GetInstanceName(p_instance);
    char const * const p_module_name   = R_MODULE_GetModuleName(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("No private data");
        result = R_RESULT_FAILED;
    }

    /* Init configuration */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_config(&p_data->config, p_instance_name, p_module_name) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Configuration init has failed");
            result = R_RESULT_FAILED;
        }
    }

    /* Init private data struct */
    if (R_RESULT_SUCCESS == result)
    {
        /* Scan the directory and build file name array */
        p_data->num_files = scandir(p_data->config.folder_name, &p_data->name_list, NULL, alphasort); /*lint !e9025 */

        /* Fill in the rest of private data struct */
        p_data->playback_idx = p_data->config.start_index;
        p_data->loop_idx = 1;

        /* Check number of files in directory */
        if (0 == p_data->num_files)
        {
            R_PRINT_ERROR("Error parsing directory %s for playback files", p_data->config.folder_name);
            result = R_RESULT_FAILED;
        }
    }

    /* Do second round of configuration */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_config_again(p_data, p_instance_name, p_module_name) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Configuration init has failed");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Playback_ROF_Channels PQS Playback ROF Channels
 * @ingroup    PQS_Playback_ROF
 * # Channel Table:
 *
 *  Channel name | Channel Type | Object Type  | Description
 *  ------------ | ------------ | ------------ | ----------------------------------
 *  data         | output       | image        | @ref PQS_Playback_ROF_Channel_data
 *  trigger      | trigger      | trigger      | @ref PQS_Playback_ROF_Channel_trigger
 *
 * @anchor PQS_Playback_ROF_Channel_data
 * ## PQS Playback ROF Channel data
 *             This is the output object containing the object data and properties as read from memory.
 *
 * @anchor PQS_Playback_ROF_Channel_trigger
 * ## PQS Playback ROF Channel trigger
 *             This is the trigger for the playback module. Any object can be routed to this channel to trigger
 *             the playback module to read and output a single frame. The output data of the playback module can be routed
 *             to this channel to keep a continuous stream of images going. Another option is to route the
 *             final output object of a given processing pipeline into this channel to assure the pipeline is only
 *             processing one image at a time. This channel must be kicked off by an external trigger in the graph
 *             file.
 *
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_BROKER_InitChannelInfo(p_instance, PLAYBACK_PRV_CHANNEL_OUT,     "data",    R_OBJECT_TYPE_IMAGE,   R_BROKER_DATA_DIRECTION_OUT);
    result |= R_BROKER_InitChannelInfo(p_instance, PLAYBACK_PRV_CHANNEL_TRIGGER, "trigger", R_OBJECT_TYPE_TRIGGER, R_BROKER_DATA_DIRECTION_IN);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_Deinit
 * @brief           Deinitializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Deinit(st_module_instance_t *const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t *p_data = R_MODULE_GetPrivDataPtr(p_instance);

    /* Check private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    for (int32_t i = 0; i < p_data->num_files; i++)
    {
        free(p_data->name_list[i]); /*lint !e586 */
    }

    free(p_data->name_list); /*lint !e586 */
    p_data->name_list = NULL;

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    st_priv_t *   p_data   = R_MODULE_GetPrivDataPtr(p_instance);
    st_object_t * p_object = R_BROKER_GetObjectPtr(p_instance, PLAYBACK_PRV_CHANNEL_OUT);
    r_result_t result = R_RESULT_SUCCESS;
    char fname[PLAYBACK_PRV_MAX_FILE_NAME_LENGTH] = {'\0'};

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Check object */
    if (R_OBJECT_CheckObjectType(p_object, R_OBJECT_TYPE_IMAGE) != R_RESULT_SUCCESS)
    {
        R_PRINT_ERROR("Invalid object input");
        result = R_RESULT_FAILED;
    }

    /* Read file */
    R_TIMER_LogMessage(p_instance, "Read file");
    if (R_RESULT_SUCCESS == result)
    {
        /* Loop until we get the correct file type */
        while (check_filetype(p_data) != R_RESULT_SUCCESS)
        {
            p_data->playback_idx++;
        }

        if (get_filename(p_data, PLAYBACK_PRV_MAX_FILE_NAME_LENGTH, fname) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Couldn't create playback file name");
            result = R_RESULT_FAILED;
        }

        if (R_RESULT_SUCCESS == result)
        {
            R_PRINT_INFO("Playing back file %s", fname);

            if (R_OBJECT_ReadROF(p_object, fname) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Couldn't process file '%s' for playback", fname);
                result = R_RESULT_FAILED;
            }
        }

        /* Increment file index */
        p_data->playback_idx++;

        /* Check if we are at the end of the data set */
        if (p_data->playback_idx >= p_data->config.end_index)
        {
            R_PRINT_NOTICE("Looped though data %u times", p_data->loop_idx);
            p_data->playback_idx = p_data->config.start_index;
            p_data->loop_idx++;
        }
    }

    return result;
}
