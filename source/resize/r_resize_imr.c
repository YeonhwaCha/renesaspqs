/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_resize_imr.c
 * @brief         Source file for resizing an image using IMR. See @ref PQS_Resize_IMR for more details
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @page       PQS_Resize_IMR_Algorithm  PQS Resize IMR Algorithm
 * @ingroup    PQS_Resize_IMR
 * # Algorithm Description:
 *             This module uses the IMR to resize an image. The output image can only be shrunk by half in both height
 *              and width. Aliasing can occur if IMR is used to resize smaller that that.
 * # Block Diagram:
 *             @image html resize_imr.png
 *********************************************************************************************************************/
/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/
#include "r_common.h"
#include "r_object_image.h"
#include "generic_api_memory.h"

#include "r_resize_imr.h"
#include "r_imr_api.h"
#include "opencv_lens.h"
#include "imr_dlgen.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define RESIZE_PRV_CHANNEL_IMAGE_IN          (0u) /*!< Broker Channel for input image */
#define RESIZE_PRV_CHANNEL_IMAGE_OUT         (1u) /*!< Broker Channel for output image */

#define RESIZE_PRV_DL_ALIGN                  (64u)                /*!< Display list memory alignment */
#define RESIZE_PRV_DL_ALLOC_SIZE             (2u * 1024u * 1024u) /*!< Size of Display list */
#define RESIZE_PRV_MESH_SIZE                 (4u)                 /*!< Mesh Size */
#define RESIZE_PRV_OPT_NOT_USED              (R_IMR_EXEC_Y)       /*!< Execution option not used */
#define RESIZE_PRV_EXEC_TIMEOUT              (500u)               /*!< Execution timeout in miliseconds */

#define RESIZE_PRV_DEFAULT_INPUT_HEIGHT      (800u)  /*!< Default value for input image height. Can be overridden by config  */
#define RESIZE_PRV_DEFAULT_INPUT_WIDTH       (1280u) /*!< Default value for input image width. Can be overridden by config  */
#define RESIZE_PRV_DEFAULT_INPUT_STRIDE      (2560u) /*!< Default value for input image stride. Can be overridden by config  */

#define RESIZE_PRV_DEFAULT_OUTPUT_HEIGHT     (640u)  /*!< Default value for output image height. Can be overridden by config  */
#define RESIZE_PRV_DEFAULT_OUTPUT_WIDTH      (1024u) /*!< Default value for output image width. Can be overridden by config */
#define RESIZE_PRV_DEFAULT_OUTPUT_STRIDE     (2048u) /*!< Default value for output image stride. Can be overridden by config */

#define RESIZE_PRV_DEFAULT_IMR_CHANNEL_START (0) /*!< Default value for IMR staring channel. Can be overridden by config */
#define RESIZE_PRV_DEFAULT_IMR_CHANNEL_END   (5) /*!< Default value for IMR ending channel. Can be overridden by config */

#define RESIZE_PRV_DEFAULT_FORMAT            (R_IMAGE_FORMAT_UYVY_INTERLEAVED) /*!< Default value for image format. Can be overridden by config  */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Configuration structure for resize_imr module */
typedef struct {
    uint32_t         input_height;    /*!< Height of image */
    uint32_t         input_width;     /*!< Width of image */
    uint32_t         input_stride;    /*!< Stride of image */
    uint32_t         output_height;   /*!< Height of image */
    uint32_t         output_width;    /*!< Width of image */
    uint32_t         output_stride;   /*!< Stride of image */
    int32_t          start_channel;   /*!< Starting IMR channel */
    int32_t          end_channel;     /*!< Ending IMR channel */
    e_image_format_t format;          /*!< Format of image */
} st_config_t;

/*! Private data structure for resize_imr module */
typedef struct st_priv_tag {
    st_config_t  config;                            /*!< Configuration parameters */
    r_imr_dl_t   display_lists[R_MAX_IMR_CHANNELS]; /*!< Display list for each IMR channels */
    r_imr_data_t imr_input;                         /*!< Input image info */
    r_imr_data_t imr_output;                        /*!< Output image info */
} st_priv_t; /*lint !e9109 !e761 */

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_instance);
static r_result_t R_Deinit(st_module_instance_t * const p_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_instance);

/*! Module interface definition for resize_imr */
const st_module_interface_t g_module_interface_resize_imr = {
    .module_name   = "resize_imr",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = R_Deinit,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Local Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   get_attr
 * @brief           Returns the IMR data attribute for a given image format
 * @param [in]      format - Format of the image
 * @retval          The image attribute
 *********************************************************************************************************************/
static uint32_t get_attr(e_image_format_t format)
{
    uint32_t attr = R_IMR_8BIT;

    switch (format) /*lint !e788 */
    {
        case R_IMAGE_FORMAT_UYVY_INTERLEAVED:
        {
            attr = R_IMR_YUV;
            break;
        }

        case R_IMAGE_FORMAT_BINARY:
        {
            attr = R_IMR_8BIT;
            break;
        }

        default:
        {
            R_PRINT_ERROR("Fell through switch. Format not supported.");
            break;
        }
    }

    return attr;
}

/******************************************************************************************************************//**
 * Function Name:   get_bytes_per_pixel
 * @brief           Returns the bytes per pixel of the given format
 * @param [in]      format - Format of the image
 * @retval          0 - on failure, otherwise the bytes per pixel of the given image format
 *********************************************************************************************************************/
static uint32_t get_bytes_per_pixel(e_image_format_t format)
{
    uint8_t bytes_per_pixel = 0;

    switch (format) /*lint !e788 */
    {
        case R_IMAGE_FORMAT_UYVY_INTERLEAVED:
        {
            bytes_per_pixel = R_IMAGE_BYTES_PP_UYVY_INTERLEAVED;
            break;
        }

        case R_IMAGE_FORMAT_BINARY:
        {
            bytes_per_pixel = R_IMAGE_BYTES_PP_BINARY;
            break;
        }

        default:
        {
            R_PRINT_ERROR("Fell through switch. Format not supported.");
            break;
        }
    }

    return bytes_per_pixel;
}

/******************************************************************************************************************//**
 * @page    PQS_Resize_IMR_Config  PQS Resize IMR Config
 * @ingroup PQS_Resize_IMR
 * # Configuration Table:
 *
 *  Config Name   | Config Type      | Default                         | Range                                                    | Description
 *  ------------- | ---------------- | ------------------------------- | -------------------------------------------------------- |------------------------------------------
 *  input_height  | uint32_t         | 800                             | 1 - @ref R_OBJECT_IMAGE_MAX_HEIGHT                       | Height of the input image.
 *  input_width   | uint32_t         | 1280                            | 1 - @ref R_OBJECT_IMAGE_MAX_WIDTH                        | Width of the input image.
 *  input_stride  | uint32_t         | 2560                            | 256 - @ref R_OBJECT_IMAGE_MAX_WIDTH                      | Stride of the input image. Must be a multiple of 256. Cannot be less than input width * image bytes per pixel
 *  output_height | uint32_t         | 640                             | 1 - @ref R_OBJECT_IMAGE_MAX_HEIGHT                       | Height of the output image. Cannot be less than half of the input height
 *  output_width  | uint32_t         | 1024                            | 32 - @ref R_OBJECT_IMAGE_MAX_WIDTH                       | Width of the output image. Must be a multiple of 32. Cannot be less than half of the input width
 *  output_stride | uint32_t         | 2048                            | 32 - @ref R_OBJECT_IMAGE_MAX_WIDTH                       | Stride of the  output image. Cannot be less than output width * image bytes per pixel
 *  start_channel | uint32_t         | 0                               | 0 - (@ref R_MAX_IMR_CHANNELS - 1)                        | Beginning IMR channel to use for program
 *  end_channel   | uint32_t         | 5                               | 0 - (@ref R_MAX_IMR_CHANNELS - 1)                        | Ending IMR channel to use for program. Cannot be less than start_channel
 *  format        | e_image_format_t | R_IMAGE_FORMAT_UYVY_INTERLEAVED | R_IMAGE_FORMAT_UYVY_INTERLEAVED or R_IMAGE_FORMAT_BINARY | Format of the input image.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_config
 * @brief           Initializes the configuration structure with either the default values or with values from a config
 *                  file if available.
 * @param [in,out]  p_config - pointer to the configuration structure
 * @param [in]      p_instance_name - String of the module instance name
 * @param [in]      p_module_name - String of the module name
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_config(st_config_t * p_config, char const * const p_instance_name, char const * const p_module_name)
{
    r_result_t result = R_RESULT_SUCCESS;
    const char * format_names[] = E_IMAGE_FORMAT_AS_STRING;

    (void)p_module_name; //Not currently used

    /* Init config */
    p_config->input_height  = R_COMMON_CONFIG_GetUint32(p_instance_name, "input_height",  RESIZE_PRV_DEFAULT_INPUT_HEIGHT);
    p_config->input_width   = R_COMMON_CONFIG_GetUint32(p_instance_name, "input_width",   RESIZE_PRV_DEFAULT_INPUT_WIDTH);
    p_config->input_stride  = R_COMMON_CONFIG_GetUint32(p_instance_name, "input_stride",  RESIZE_PRV_DEFAULT_INPUT_STRIDE);
    p_config->output_height = R_COMMON_CONFIG_GetUint32(p_instance_name, "output_height", RESIZE_PRV_DEFAULT_OUTPUT_HEIGHT);
    p_config->output_width  = R_COMMON_CONFIG_GetUint32(p_instance_name, "output_width",  RESIZE_PRV_DEFAULT_OUTPUT_WIDTH);
    p_config->output_stride = R_COMMON_CONFIG_GetUint32(p_instance_name, "output_stride", RESIZE_PRV_DEFAULT_OUTPUT_STRIDE);
    p_config->start_channel = R_COMMON_CONFIG_GetInt32( p_instance_name, "start_channel", RESIZE_PRV_DEFAULT_IMR_CHANNEL_START);
    p_config->end_channel   = R_COMMON_CONFIG_GetInt32( p_instance_name, "end_channel",   RESIZE_PRV_DEFAULT_IMR_CHANNEL_END);
    p_config->format        = R_COMMON_CONFIG_GET_ENUM( p_instance_name, "format",        RESIZE_PRV_DEFAULT_FORMAT, format_names, (sizeof(format_names))/(sizeof(format_names[0])), e_image_format_t); /*lint !e9030 !e9034 */

    /* Check config */
    if ((p_config->output_width < (p_config->input_width / 2u)) || (p_config->output_height < (p_config->input_height / 2u)))
    {
        R_PRINT_ERROR("Scaling cannot be less than 0.5. Aliasing can occur.");
        result = R_RESULT_FAILED;
    }

    if ((p_config->input_stride  < (get_bytes_per_pixel(p_config->format) * p_config->input_width)) || /*lint !e9007 !e931 */
        (p_config->output_stride < (get_bytes_per_pixel(p_config->format) * p_config->output_width)))
    {
        R_PRINT_ERROR("Stride not large enough for image width");
        result = R_RESULT_FAILED;
    }

    if ((p_config->input_stride % 256u) != 0u)
    {
        R_PRINT_ERROR("Input stride must be a multiple of 256");
        result = R_RESULT_FAILED;
    }

    if ((p_config->output_width % 32u) != 0u)
    {
        R_PRINT_ERROR("Output width must be a multiple of 32");
        result = R_RESULT_FAILED;
    }

    if ((p_config->input_height > R_OBJECT_IMAGE_MAX_HEIGHT) || (p_config->output_height > R_OBJECT_IMAGE_MAX_HEIGHT) )
    {
        R_PRINT_ERROR("Height must be less than R_OBJECT_IMAGE_MAX_HEIGHT");
        result = R_RESULT_FAILED;
    }

    if ((p_config->input_width > R_OBJECT_IMAGE_MAX_WIDTH) || (p_config->output_width > R_OBJECT_IMAGE_MAX_WIDTH) )
    {
        R_PRINT_ERROR("Width must be less than R_OBJECT_IMAGE_MAX_WIDTH");
        result = R_RESULT_FAILED;
    }

    if ((0u == p_config->input_height) || (0u == p_config->output_height))
    {
        R_PRINT_ERROR("Height cannot be zero!");
        result = R_RESULT_FAILED;
    }

    if ((0u == p_config->input_width) || (0u == p_config->output_width))
    {
        R_PRINT_ERROR("Width cannot be zero!");
        result = R_RESULT_FAILED;
    }

    if (0 > p_config->start_channel)
    {
        R_PRINT_ERROR("Staring IMR channel cannot be less than zero.");
        result = R_RESULT_FAILED;
    }

    if (p_config->start_channel > p_config->end_channel)
    {
        R_PRINT_ERROR("Staring IMR channel cannot be greater than ending channel.");
        result = R_RESULT_FAILED;
    }

    if ((int32_t)R_MAX_IMR_CHANNELS <= p_config->end_channel)
    {
        R_PRINT_ERROR("End channel cannot be greater than R_MAX_IMR_CHANNELS");
        result = R_RESULT_FAILED;
    }

    if ((R_IMAGE_FORMAT_UYVY_INTERLEAVED != p_config->format) && (R_IMAGE_FORMAT_BINARY != p_config->format))
    {
        R_PRINT_ERROR("Input format not supported.");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   init_priv
 * @brief           Initializes the private data structure
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_priv(st_module_instance_t * const p_instance)
{
    r_result_t result  = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    /* Module names */
    char const * const p_instance_name = R_MODULE_GetInstanceName(p_instance);
    char const * const p_module_name   = R_MODULE_GetModuleName(p_instance);

    /* Check private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("No private data available");
        result = R_RESULT_FAILED;
    }

    /* Init configuration */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_config(&p_data->config, p_instance_name, p_module_name) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Configuration init has failed");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   process_image_planes
 * @brief           Processes the raw data planes
 * @param [in]      p_instance - Pointer to the module instance
 * @param [in]      p_input_plane - Pointer to input data plane
 * @param [out]     p_output_plane - Pointer to output data plane
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t process_image_planes(st_module_instance_t * const p_instance, void * p_input_plane, void * p_output_plane)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);
    r_imr_source_t src_info = {0};
    size_t input_size       = 0;
    size_t output_size      = 0;
    int32_t imr_result      = 0;
    int32_t channel_index   = 0;

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Update data */
    if ((R_RESULT_SUCCESS == result))
    {
        input_size  = (size_t)(p_data->imr_input.Stride  * p_data->imr_input.Height);
        output_size = (size_t)(p_data->imr_output.Stride * p_data->imr_output.Height);

        /* Update input struct */
        p_data->imr_input.VmrStartAddr = (addr_t)p_input_plane; /*lint !e9078 !e923 !e9091 */
        p_data->imr_input.PhysAddr     = (uint32_t)gf_GetPhysAddr(p_input_plane);

        /* Update output struct */
        p_data->imr_output.VmrStartAddr = (addr_t)p_output_plane; /*lint !e9078 !e923 !e9091 */
        p_data->imr_output.PhysAddr     = (uint32_t)gf_GetPhysAddr(p_output_plane);

        /* Flush Cache */
        gf_DCacheFlushRange((void *)p_input_plane,  input_size);
        gf_DCacheFlushRange((void *)p_output_plane, output_size);
    }

    /* Set Source data */
    if ((R_RESULT_SUCCESS == result))
    {
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            /* Note: start_line, end_line, and mesh_size are unused */
            src_info.source_width  = p_data->imr_input.Width;
            src_info.source_height = p_data->imr_input.Height;

            imr_result = R_IMR_SetSource(channel_index, &src_info);
            if (E_OK != imr_result)
            {
                R_PRINT_ERROR("Error in R_IMR_SetSource() for channel %d ret = %d", channel_index, imr_result);
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Start IMR */
    if ((R_RESULT_SUCCESS == result))
    {
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            imr_result = R_IMR_ExecuteExt(channel_index,
                                    &p_data->display_lists[channel_index],
                                    &p_data->imr_input,
                                    &p_data->imr_output,
                                    R_IMR_EXE_MODE_BILINEAR_ENABLE | R_IMR_EXE_MODE_TEXTUREMAPPING,
                                    RESIZE_PRV_OPT_NOT_USED);

            if (E_OK != imr_result)
            {
                R_PRINT_ERROR("Error in R_IMR_ExecuteExt() for channel %d ret = %d", channel_index, imr_result);
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Wait and read status */
    if (R_RESULT_SUCCESS == result)
    {
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            imr_result = R_IMR_WaitEventTimeout(channel_index, RESIZE_PRV_EXEC_TIMEOUT);

            if (R_IMR_SR_TRA != (uint32_t)imr_result)
            {
                R_PRINT_ERROR("Error in R_IMR_WaitEventTimeout() for channel %d ret = %d", channel_index, result);
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Invalidate data cache */
    gf_DCacheInvalidateRange((void *)p_output_plane, output_size);

    return result;
}

/***********************************************************************************************************************
 Module Interface Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    r_result_t result     = init_priv(p_instance);
    st_priv_t * p_data    = R_MODULE_GetPrivDataPtr(p_instance);
    int32_t channel_index = 0;
    int32_t imr_result    = 0;

    /* IMR config structs */
    undistort_params_t opencv_coeff           = {0.0f};
    imr_roi_t imr_regions[R_MAX_IMR_CHANNELS] = {{0}};

    /* Casting values to floats */
    float input_width_float   = (float)p_data->config.input_width;
    float input_height_float  = (float)p_data->config.input_height;
    float output_width_float  = (float)p_data->config.output_width;
    float output_height_float = (float)p_data->config.output_height;

    /* Check private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Init local variables */
    if (R_RESULT_SUCCESS == result)
    {
        uint32_t num_channels_used = ((uint32_t)p_data->config.end_channel - (uint32_t)p_data->config.start_channel) + 1u;

        /* Set coefficients correction/scaling */
        opencv_coeff.k1 = 0.0f;
        opencv_coeff.k2 = 0.0f;
        opencv_coeff.k3 = 0.0f;
        opencv_coeff.fx = 0.0f;
        opencv_coeff.fy = 0.0f;
        opencv_coeff.cx = 0.0f;
        opencv_coeff.cy = 0.0f;
        opencv_coeff.ox = 0.0f;
        opencv_coeff.oy = 0.0f;
        opencv_coeff.sx = input_width_float/output_width_float;
        opencv_coeff.sy = input_height_float/output_height_float;

        /* Init IMR input image struct */

        p_data->imr_input.Height       = p_data->config.input_height;
        p_data->imr_input.Width        = p_data->config.input_width;
        p_data->imr_input.bpp          = 8;
        p_data->imr_input.Stride       = p_data->config.input_stride;
        p_data->imr_input.Attr         = get_attr(p_data->config.format);
        p_data->imr_input.VmrStartAddr = 0;
        p_data->imr_input.PhysAddr     = 0;

        /* Init IMR output image struct */
        p_data->imr_output.Height       = p_data->config.output_height;
        p_data->imr_output.Width        = p_data->config.output_width;
        p_data->imr_output.bpp          = 8;
        p_data->imr_output.Stride       = p_data->config.output_stride;
        p_data->imr_output.Attr         = get_attr(p_data->config.format);
        p_data->imr_output.VmrStartAddr = 0;
        p_data->imr_output.PhysAddr     = 0;

        /* Set IMR regions structs */
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            imr_regions[channel_index].x      = (uint16_t)0;
            imr_regions[channel_index].y      = (uint16_t)(((uint16_t)channel_index - (uint16_t)p_data->config.start_channel) *
                                                    (uint16_t)(p_data->config.output_height / num_channels_used));
            imr_regions[channel_index].Width  = (uint16_t)p_data->config.output_width;
            imr_regions[channel_index].Height = (uint16_t)(p_data->config.output_height / num_channels_used);

            /* last time though loop account for remainder */
            if (p_data->config.end_channel == channel_index)
            {
                imr_regions[channel_index].Height += (uint16_t)(p_data->config.output_height % num_channels_used);
            }

            R_PRINT_INFO("IMR Region for channel %u\n"
                         "\t x: %u\n"
                         "\t y: %u\n"
                         "\t Width: %u\n"
                         "\t Height: %u",
                         channel_index,
                         imr_regions[channel_index].x,
                         imr_regions[channel_index].y,
                         imr_regions[channel_index].Width,
                         imr_regions[channel_index].Height);
        }
    }

    /* Create display lists */
    if (R_RESULT_SUCCESS == result)
    {
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            p_data->display_lists[channel_index].VmrStartAddr = (addr_t)gf_Memalign(RESIZE_PRV_DL_ALIGN, RESIZE_PRV_DL_ALLOC_SIZE); /*lint !e923 !e9091 */

            if (0u != p_data->display_lists[channel_index].VmrStartAddr)
            {
                p_data->display_lists[channel_index].PhysAddr = (uint32_t)gf_GetPhysAddr((void*)p_data->display_lists[channel_index].VmrStartAddr); /*lint !e923 !e511 */
                p_data->display_lists[channel_index].Size     = RESIZE_PRV_DL_ALLOC_SIZE;
                p_data->display_lists[channel_index].Pos      = 0;
            }
            else
            {
                R_PRINT_ERROR("Failed to allocate memory for display list");
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Build display lists */
    if (R_RESULT_SUCCESS == result)
    {
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            imr_result = imr_build_display_list_roi(&p_data->display_lists[channel_index],
                                                    &p_data->imr_input,
                                                    &p_data->imr_output,
                                                    RESIZE_PRV_MESH_SIZE,
                                                    map_opencv_scale,
                                                    &opencv_coeff,
                                                    imr_regions[channel_index]);

            if (0 == imr_result)
            {
                /* Flush DL memory to DRAM */
                gf_DCacheFlushRange((void *)p_data->display_lists[channel_index].VmrStartAddr, (size_t)p_data->display_lists[channel_index].Size); /*lint !e923 !e511 */
            }
            else
            {
                R_PRINT_ERROR("ERROR: DL Creation failed with error %d", imr_result);
                result = R_RESULT_FAILED;
            }
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Resize_IMR_Channels PQS Resize IMR Channels
 * @ingroup    PQS_Resize_IMR
 * # Channel Table:
 *
 *  Channel name | Channel Type | Object Type | Description
 *  ------------ | ------------ | ----------- | ----------------------------------
 *  image_in     | input        | image       | @ref PQS_Resize_IMR_Channel_Image_In
 *  image_out    | output       | image       | @ref PQS_Resize_IMR_Channel_Image_Out
 *
 * @anchor PQS_Resize_IMR_Channel_Image_In
 * ## PQS Resize IMR Channel Image In
 *             Input is either an YUV422 interleaved UYVY image of type R_IMAGE_FORMAT_UYVY_INTERLEAVED or an image
 *             of type R_IMAGE_FORMAT_BINARY, note the R_IMAGE_FORMAT_BINARY image format has 255 value representing
 *             a logical one. The height and width are both configurable.
 *
 * @anchor PQS_Resize_IMR_Channel_Image_Out
 * ## PQS Resize IMR Channel Image Out
 *             Output is either an YUV422 interleaved UYVY image of type R_IMAGE_FORMAT_UYVY_INTERLEAVED or an image
 *             of type R_IMAGE_FORMAT_BINARY, note the R_IMAGE_FORMAT_BINARY image format has 255 value representing
 *             a logical one. The height and width are both configurable.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_BROKER_InitChannelInfo(p_instance, RESIZE_PRV_CHANNEL_IMAGE_IN,  "image_in",  R_OBJECT_TYPE_IMAGE, R_BROKER_DATA_DIRECTION_IN);
    result |= R_BROKER_InitChannelInfo(p_instance, RESIZE_PRV_CHANNEL_IMAGE_OUT, "image_out", R_OBJECT_TYPE_IMAGE, R_BROKER_DATA_DIRECTION_OUT);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_Deinit
 * @brief           Deinitializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Deinit(st_module_instance_t * const p_instance)
{
    r_result_t result     = R_RESULT_SUCCESS;
    st_priv_t * p_data    = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Free display lists */
    if (R_RESULT_SUCCESS == result)
    {
        for (int32_t channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            gf_Free((void*)p_data->display_lists[channel_index].VmrStartAddr); /*lint !e923 !e511 */
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    st_priv_t * p_data        = R_MODULE_GetPrivDataPtr(p_instance);
    r_result_t result         = R_RESULT_SUCCESS;
    st_object_t * p_image_in  = R_BROKER_GetObjectPtr(p_instance, RESIZE_PRV_CHANNEL_IMAGE_IN);
    st_object_t * p_image_out = R_BROKER_GetObjectPtr(p_instance, RESIZE_PRV_CHANNEL_IMAGE_OUT);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Check objects */
    if (R_RESULT_SUCCESS != result)
    {
        result |= R_OBJECT_CheckObjectType(p_image_in,  R_OBJECT_TYPE_IMAGE);
        result |= R_OBJECT_CheckObjectType(p_image_out, R_OBJECT_TYPE_IMAGE);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Invalid object input");
        }
    }

    /* Check input */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CheckImageWidth(        p_image_in, p_data->config.input_width);
        result |= R_OBJECT_CheckImageHeight(       p_image_in, p_data->config.input_height);
        result |= R_OBJECT_CheckImageDepth(        p_image_in, 1);
        result |= R_OBJECT_CheckImageBytesPerPixel(p_image_in, get_bytes_per_pixel(p_data->config.format));
        result |= R_OBJECT_CheckImageFormat(       p_image_in, p_data->config.format);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Invalid image: Expected %u x %u depth=%d, bpp=%d, format=%d.  Got %d x %d depth=%d, bpp=%d, format=%d",
                    p_data->config.input_width,
                    p_data->config.input_height,
                    1,
                    get_bytes_per_pixel(p_data->config.format),
                    p_data->config.format,
                    R_OBJECT_GetImageWidth(        p_image_in),
                    R_OBJECT_GetImageHeight(       p_image_in),
                    R_OBJECT_GetImageDepth(        p_image_in),
                    R_OBJECT_GetImageBytesPerPixel(p_image_in),
                    R_OBJECT_GetImageFormat(       p_image_in));
        }
    }

    /* Fill output object */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CopyHeader(           p_image_out, p_image_in);
        result |= R_OBJECT_SetImageWidth(        p_image_out, p_data->config.output_width);
        result |= R_OBJECT_SetImageHeight(       p_image_out, p_data->config.output_height);
        result |= R_OBJECT_SetImageDepth(        p_image_out, 1);
        result |= R_OBJECT_SetImageBytesPerPixel(p_image_out, get_bytes_per_pixel(p_data->config.format));
        result |= R_OBJECT_SetImageFormat(       p_image_out, p_data->config.format);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Couldn't set uyva output params");
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        void * p_plane_in  = R_OBJECT_GetImagePlaneCPU(p_image_in);
        void * p_plane_out = R_OBJECT_GetImagePlaneCPU(p_image_out);

        /* Start image processing */
        if ((NULL != p_plane_in) && (NULL != p_plane_out))
        {
            if (process_image_planes(p_instance, p_plane_in, p_plane_out) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Error in imr function");
                result = R_RESULT_FAILED;
            }
        }
        else
        {
            R_PRINT_ERROR("Some error with getting CPU data");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}
