/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_resize_psc.c
 * @brief         Source file for resizing an image using PSC. See @ref PQS_Resize_PSC for more details
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @page       PQS_Resize_PSC_Algorithm  PQS Resize PSC Algorithm
 * @ingroup    PQS_Resize_PSC
 * # Algorithm Description:
 *             This module uses the the Pyrimidal Scaler to resize an image. The output image can only be shrunk by
 *             half in both height and width.
 * # Block Diagram:
 *             @image html resize_psc.png
 *********************************************************************************************************************/
/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/
#include "r_common.h"
#include "r_object_image.h"

#include "r_resize_psc.h"
#include "generic_api_memory.h"
#include "rcv_impdrv.h"
#include "r_atmlib_types.h"
#include "r_atmlib_def.h"
#include "r_atmlib_prot.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define RESIZE_PRV_CHANNEL_IMAGE_IN          (0u) /*!< Broker Channel for input image */
#define RESIZE_PRV_CHANNEL_IMAGE_OUT         (1u) /*!< Broker Channel for output image */

#define RESIZE_PRV_DEFAULT_INPUT_HEIGHT      (408u)  /*!< Default value for input image height. Can be overridden by config  */
#define RESIZE_PRV_DEFAULT_INPUT_WIDTH       (960u)  /*!< Default value for input image width. Can be overridden by config  */

#define RESIZE_PRV_DEFAULT_OUTPUT_HEIGHT     (204u)  /*!< Default value for output image height. Can be overridden by config  */
#define RESIZE_PRV_DEFAULT_OUTPUT_WIDTH      (480u)  /*!< Default value for output image width. Can be overridden by config */

#define RESIZE_PRV_DEFAULT_FORMAT            (R_IMAGE_FORMAT_GRAYSCALE_16BIT) /*!< Default value for image format. Can be overridden by config  */

#define RESIZE_PRV_MAX_CL_LENGTH             (4096u) /*!< CL maximum length (used for allocation) */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Configuration structure for resize_psc module */
typedef struct {
    uint32_t         input_height;    /*!< Height of image */
    uint32_t         input_width;     /*!< Width of image */
    uint32_t         output_height;   /*!< Height of image */
    uint32_t         output_width;    /*!< Width of image */
    e_image_format_t format;          /*!< Format of image */
} st_config_t;

/*! Data needed for IMP program execution */
typedef struct {
    R_ATMLIB_CLData       cl_data;      /*!< This is command list data */
    R_ATMLIB_PSCSrcParam  src_param;    /*!< Source image parameter */
    R_ATMLIB_PSCDstParam  dst_param;    /*!< Destination image parameter */
} st_psc_data_t;

/*! Private data structure for resize_psc module */
typedef struct st_priv_tag {
        st_config_t          config;       /*!< Configuration parameters */
        st_psc_data_t        psc_data;     /*!< Command list data */
} st_priv_t; /*lint !e9109 !e761 */

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_instance);
static r_result_t R_Deinit(st_module_instance_t * const p_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_instance);

/*! Module interface definition for resize_psc */
const st_module_interface_t g_module_interface_resize_psc = {
    .module_name   = "resize_psc",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = R_Deinit,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Local Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   get_bytes_per_pixel
 * @brief           Returns the bytes per pixel of the given format
 * @param [in]      format - Format of the image
 * @retval          0 - on failure, otherwise the bytes per pixel of the given image format
 *********************************************************************************************************************/
static uint32_t get_bytes_per_pixel(e_image_format_t format)
{
    uint8_t bytes_per_pixel = 0;

    switch (format) /*lint !e788 */
    {
        case R_IMAGE_FORMAT_GRAYSCALE_8BIT:
        {
            bytes_per_pixel = R_IMAGE_BYTES_PP_GRAYSCALE_8BIT;
            break;
        }

        case R_IMAGE_FORMAT_GRAYSCALE_16BIT:
        {
            bytes_per_pixel = R_IMAGE_BYTES_PP_GRAYSCALE_16BIT;
            break;
        }

        default:
        {
            R_PRINT_ERROR("Fell through switch. Format not supported.");
            break;
        }
    }

    return bytes_per_pixel;
}

/******************************************************************************************************************//**
 * Function Name:   get_type
 * @brief           Returns the atomic lib type of the image for a given format
 * @param [in]      format - Format of the image
 * @retval          The type of the image
 *********************************************************************************************************************/
static R_ATMLIB_IMAGE_FORMAT get_type(e_image_format_t format)
{
    R_ATMLIB_IMAGE_FORMAT type = R_ATMLIB_IMG_8U;

    switch (format) /*lint !e788 */
    {
        case R_IMAGE_FORMAT_GRAYSCALE_8BIT:
        {
            type = R_ATMLIB_IMG_8U;
            break;
        }

        case R_IMAGE_FORMAT_GRAYSCALE_16BIT:
        {
            type = R_ATMLIB_IMG_16S;
            break;
        }

        default:
        {
            R_PRINT_ERROR("Fell through switch. Format not supported.");
            break;
        }
    }

    return type;
}


/******************************************************************************************************************//**
 * Function Name:   mag_calc
 * @brief           Calculates the magnification parameter based on the input and output sizes
 * @param [in]      size_input - Input size parameter
 * @param [in]      size_output - Output size parameter
 * @param [in]      p_factor - Pointer to the magnification factor
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t mag_calc(uint32_t size_input, uint32_t  size_output, uint32_t * const p_factor)
{
    r_result_t result;
    uint32_t   mag;

    /* Calculate according to formula
     * mag = (Size of before reduction / Size of after reduction) * 2^20 */
    mag = (uint32_t)(1 << 20) * (uint32_t)((double)size_input / (double)size_output);

    /* Safety and validation checks */
    if (NULL == p_factor)
    {
        result = R_RESULT_ILLEGAL_NULL_POINTER;
    }
    else if ((mag > 0x00200000u) || (mag < 0x00100000u))
    {
        /* Invalid factor due to invalid input data */
        *p_factor = 0;
        result    = R_RESULT_BAD_INPUT_ARGUMENTS;
    }
    else
    {
        /* Store factor and update return value */
        *p_factor = mag;
        result    = R_RESULT_SUCCESS;
    }

    return result;
}

/******************************************************************************************************************//**
 * @page    PQS_Resize_PSC_Config  PQS Resize PSC Config
 * @ingroup PQS_Resize_PSC
 * # Configuration Table:
 *
 *  Config Name   | Config Type      | Default                        | Range                                                           | Description
 *  ------------- | ---------------- | ------------------------------ | --------------------------------------------------------------- |------------------------------------------
 *  input_height  | uint32_t         | 800                            | 1 - @ref R_OBJECT_IMAGE_MAX_HEIGHT                              | Height of the input image.
 *  input_width   | uint32_t         | 1280                           | 1 - @ref R_OBJECT_IMAGE_MAX_WIDTH                               | Width of the input image.
 *  output_height | uint32_t         | 640                            | 1 - @ref R_OBJECT_IMAGE_MAX_HEIGHT                              | Height of the output image. Cannot be less than half of the input height
 *  output_width  | uint32_t         | 1024                           | 1 - @ref R_OBJECT_IMAGE_MAX_WIDTH                               | Width of the output image. Cannot be less than half of the input width
 *  format        | e_image_format_t | R_IMAGE_FORMAT_GRAYSCALE_16BIT | R_IMAGE_FORMAT_GRAYSCALE_8BIT or R_IMAGE_FORMAT_GRAYSCALE_16BIT | Format of input and output image
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_config
 * @brief           Initializes the configuration structure with either the default values or with values from a config
 *                  file if available.
 * @param [in,out]  p_config - pointer to the configuration structure
 * @param [in]      p_instance_name - String of the module instance name
 * @param [in]      p_module_name - String of the module name
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_config(st_config_t * p_config, char const * const p_instance_name, char const * const p_module_name)
{
    r_result_t result = R_RESULT_SUCCESS;
    const char * format_names[] = E_IMAGE_FORMAT_AS_STRING;

    (void)p_module_name; //Not currently used

    /* Init config */
    p_config->input_height  = R_COMMON_CONFIG_GetUint32(p_instance_name, "input_height",  RESIZE_PRV_DEFAULT_INPUT_HEIGHT);
    p_config->input_width   = R_COMMON_CONFIG_GetUint32(p_instance_name, "input_width",   RESIZE_PRV_DEFAULT_INPUT_WIDTH);
    p_config->output_height = R_COMMON_CONFIG_GetUint32(p_instance_name, "output_height", RESIZE_PRV_DEFAULT_OUTPUT_HEIGHT);
    p_config->output_width  = R_COMMON_CONFIG_GetUint32(p_instance_name, "output_width",  RESIZE_PRV_DEFAULT_OUTPUT_WIDTH);
    p_config->format        = R_COMMON_CONFIG_GET_ENUM( p_instance_name, "format",        RESIZE_PRV_DEFAULT_FORMAT, format_names, (sizeof(format_names))/(sizeof(format_names[0])), e_image_format_t); /*lint !e9030 !e9034 */


    /* Check config */
    if ((p_config->output_width < (p_config->input_width / 2u)) || (p_config->output_height < (p_config->input_height / 2u)))
    {
        R_PRINT_ERROR("Scaling cannot be less than 0.5.");
        result = R_RESULT_FAILED;
    }

    if ((p_config->input_height > R_OBJECT_IMAGE_MAX_HEIGHT) || (p_config->output_height > R_OBJECT_IMAGE_MAX_HEIGHT) )
    {
        R_PRINT_ERROR("Height must be less than R_OBJECT_IMAGE_MAX_HEIGHT");
        result = R_RESULT_FAILED;
    }

    if ((p_config->input_width > R_OBJECT_IMAGE_MAX_WIDTH) || (p_config->output_width > R_OBJECT_IMAGE_MAX_WIDTH) )
    {
        R_PRINT_ERROR("Width must be less than R_OBJECT_IMAGE_MAX_WIDTH");
        result = R_RESULT_FAILED;
    }

    if ((0u == p_config->input_height) || (0u == p_config->output_height))
    {
        R_PRINT_ERROR("Height cannot be zero!");
        result = R_RESULT_FAILED;
    }

    if ((0u == p_config->input_width) || (0u == p_config->output_width))
    {
        R_PRINT_ERROR("Width cannot be zero!");
        result = R_RESULT_FAILED;
    }

    if ((R_IMAGE_FORMAT_GRAYSCALE_8BIT != p_config->format) && (R_IMAGE_FORMAT_GRAYSCALE_16BIT != p_config->format))
    {
        R_PRINT_ERROR("Input format not supported.");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   init_priv
 * @brief           Initializes the private data structure
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_priv(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    char const * const p_instance_name = R_MODULE_GetInstanceName(p_instance);
    char const * const p_module_name   = R_MODULE_GetModuleName(p_instance);
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    /* Check private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("No private data available");
        result = R_RESULT_FAILED;
    }

    /* Init configuration */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_config(&p_data->config, p_instance_name, p_module_name) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Configuration init has failed");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   process_image_planes
 * @brief           Processes the raw data planes
 * @param [in]      p_instance - Pointer to the module instance
 * @param [in]      p_input - Pointer to input data plane
 * @param [out]     p_output - Pointer to output data plane
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t process_image_planes(st_module_instance_t * const p_instance, void * p_input, void * p_output)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);
    RCvIMPDRVCOREINFO core_info  = {0};

    /* Check private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("No private data available");
        result = R_RESULT_FAILED;
    }

    /* Start IMP program */
    if (R_RESULT_SUCCESS == result)
    {
        /* Flush Cache of input and output data planes */
        gf_DCacheFlushRange(p_input,  p_data->config.input_height  * p_data->config.input_width  * sizeof(uint16_t)); //TODO configurable
        gf_DCacheFlushRange(p_output, p_data->config.output_height * p_data->config.output_width * sizeof(uint16_t)); //TODO configurable

        /* Update image plane address in command lists */
        p_data->psc_data.cl_data.top_addr[p_data->psc_data.src_param.clofs_src_addr] = (uint32_t)gf_GetPhysAddr(p_input);
        p_data->psc_data.cl_data.top_addr[p_data->psc_data.dst_param.clofs_dst_addr] = (uint32_t)gf_GetPhysAddr(p_output);

        /* Flush cache of command list */
        gf_DCacheFlushRange((void *)(p_data->psc_data.cl_data.top_addr), RESIZE_PRV_MAX_CL_LENGTH);

        core_info.CoreNum  = 0;
        core_info.CoreType = RCVDRV_CORE_TYPE_PSCEXE;

        /* execute IMP func */
        if (R_COMMON_HW_ExecuteIMP((uint32_t)gf_GetPhysAddr((void *)p_data->psc_data.cl_data.top_addr), &core_info) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error executing command list for core %u", core_info.CoreNum);
            result = R_RESULT_FAILED;
        }
    }

    /* Wait for result */
    if (R_RESULT_SUCCESS == result)
    {
        if(R_COMMON_HW_WaitIMP(&core_info) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Error waiting for PSC core %u to finish", core_info.CoreNum);
            result = R_RESULT_FAILED;
        }

        /* Invalidate Cache */
    }

    return result;
}

/***********************************************************************************************************************
 Module Interface Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    r_result_t result = init_priv(p_instance);
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);
    uint32_t * p_cl_mem = NULL;

    /* Check private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Initialize command list */
    if (R_RESULT_SUCCESS == result)
    {
        /* Get memory */
        p_cl_mem = gf_Memalign(64, RESIZE_PRV_MAX_CL_LENGTH);

        if (NULL != p_cl_mem )
        {
            /* Clear memory */
            R_COMMON_Memset(p_cl_mem, 0, RESIZE_PRV_MAX_CL_LENGTH);

            /* Init command list */
            if (r_atmlib_InitializePSCCL(&p_data->psc_data.cl_data, p_cl_mem, RESIZE_PRV_MAX_CL_LENGTH >> 2) != R_ATMLIB_E_OK)
            {
                R_PRINT_ERROR("Failed to initialize command list");
                result = R_RESULT_FAILED;
            }
        }
        else
        {
            R_PRINT_ERROR("Failed to get memory");
            result = R_RESULT_FAILED;
        }
    }

    /* Set image parameters */
    if (R_RESULT_SUCCESS == result)
    {
        /* Prepare source parameters */
        p_data->psc_data.src_param.addr   = 0; // Will be set later
        p_data->psc_data.src_param.leng   = (p_data->config.input_height << 16) | p_data->config.input_width;
        p_data->psc_data.src_param.stride = p_data->config.input_width * get_bytes_per_pixel(p_data->config.format);
        p_data->psc_data.src_param.type   = get_type(p_data->config.format);

        /* Prepare destination parameters */
        p_data->psc_data.dst_param.addr       = 0; /// Will be set later
        p_data->psc_data.dst_param.leng       = (p_data->config.output_height << 16) | p_data->config.output_width;
        p_data->psc_data.dst_param.stride     = p_data->config.output_width * get_bytes_per_pixel(p_data->config.format);
        p_data->psc_data.dst_param.type       = get_type(p_data->config.format);
        p_data->psc_data.dst_param.xmag       = 0; // Will be set below in function mag_calc()
        p_data->psc_data.dst_param.ymag       = 0; // Will be set below in function mag_calc()
        p_data->psc_data.dst_param.ofs_x      = 0;
        p_data->psc_data.dst_param.ofs_y      = 0;
        p_data->psc_data.dst_param.blend_mode = R_ATMLIB_PSC_BLENDMODE_BL;
        p_data->psc_data.dst_param.round_mode = R_ATMLIB_ROUNDMODE_DOWN;

        result |= mag_calc(p_data->config.input_width,  p_data->config.output_width,  &p_data->psc_data.dst_param.xmag);
        result |= mag_calc(p_data->config.input_height, p_data->config.output_height, &p_data->psc_data.dst_param.ymag);
    }

    /* Set commands */
    if (R_RESULT_SUCCESS == result)
    {
        if (r_atmlib_SetDataPSCCL(&p_data->psc_data.cl_data, R_ATMLIB_PSC_BASIC,
                &p_data->psc_data.src_param, &p_data->psc_data.dst_param, NULL,  NULL, NULL) != R_ATMLIB_E_OK)
        {
            R_PRINT_ERROR("failed - r_atmlib_SetDataPSCCL()");
            result = R_RESULT_FAILED;
        }
    }

    /* Finalize */
    if (R_RESULT_SUCCESS == result)
    {
        if (r_atmlib_FinalizePSCCL(&p_data->psc_data.cl_data) != R_ATMLIB_E_OK)
        {
            R_PRINT_ERROR("failed - r_atmlib_FinalizePSCCL()");
            result = R_RESULT_FAILED;
        }

        gf_DCacheFlushRange((void*)p_data->psc_data.cl_data.top_addr, RESIZE_PRV_MAX_CL_LENGTH);
    }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Resize_PSC_Channels PQS Resize PSC Channels
 * @ingroup    PQS_Resize_PSC
 * # Channel Table:
 *
 *  Channel name | Channel Type | Object Type | Description
 *  ------------ | ------------ | ----------- | ----------------------------------
 *  image_in     | input        | image       | @ref PQS_Resize_PSC_Channel_Image_In
 *  image_out    | output       | image       | @ref PQS_Resize_PSC_Channel_Image_Out
 *
 * @anchor PQS_Resize_PSC_Channel_Image_In
 * ## PQS Resize PSC Channel Image In
 *             Input is a R_IMAGE_FORMAT_GRAYSCALE_16BIT image. The height and width are both configurable.
 *
 * @anchor PQS_Resize_PSC_Channel_Image_Out
 * ## PQS Resize PSC Channel Image Out
 *             Output is a R_IMAGE_FORMAT_GRAYSCALE_16BIT image. The height and width are both configurable.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_BROKER_InitChannelInfo(p_instance, RESIZE_PRV_CHANNEL_IMAGE_IN,   "image_in",    R_OBJECT_TYPE_IMAGE,   R_BROKER_DATA_DIRECTION_IN);
    result |= R_BROKER_InitChannelInfo(p_instance, RESIZE_PRV_CHANNEL_IMAGE_OUT,  "image_out",   R_OBJECT_TYPE_IMAGE,   R_BROKER_DATA_DIRECTION_OUT);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_Deinit
 * @brief           Deinitializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Deinit(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Free command lists */
    if (R_RESULT_SUCCESS == result)
    {
        gf_Free((void *)p_data->psc_data.cl_data.top_addr);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    st_priv_t * p_data        = R_MODULE_GetPrivDataPtr(p_instance);
    r_result_t result         = R_RESULT_SUCCESS;
    st_object_t * p_image_in  = R_BROKER_GetObjectPtr(p_instance, RESIZE_PRV_CHANNEL_IMAGE_IN);
    st_object_t * p_image_out = R_BROKER_GetObjectPtr(p_instance, RESIZE_PRV_CHANNEL_IMAGE_OUT);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Check objects */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CheckObjectType(p_image_in,  R_OBJECT_TYPE_IMAGE);
        result |= R_OBJECT_CheckObjectType(p_image_out, R_OBJECT_TYPE_IMAGE);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Invalid object input");
        }
    }

    /* Check input */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CheckImageWidth(         p_image_in, p_data->config.input_width);
        result |= R_OBJECT_CheckImageHeight(        p_image_in, p_data->config.input_height);
        result |= R_OBJECT_CheckImageBytesPerPixel( p_image_in, get_bytes_per_pixel(p_data->config.format));
        result |= R_OBJECT_CheckImageDepth(         p_image_in, 1);
        result |= R_OBJECT_CheckImageFormat(        p_image_in, p_data->config.format);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Invalid image: Expected %u x %u depth=%d, bpp=%d, format=%d.  Got %d x %d depth=%d, bpp=%d, format=%d",
                    p_data->config.input_width,
                    p_data->config.input_height,
                    get_bytes_per_pixel(p_data->config.format),
                    1,
                    p_data->config.format,
                    R_OBJECT_GetImageWidth(p_image_in),
                    R_OBJECT_GetImageHeight(p_image_in),
                    R_OBJECT_GetImageDepth(p_image_in),
                    R_OBJECT_GetImageBytesPerPixel(p_image_in),
                    R_OBJECT_GetImageFormat(p_image_in));
        }
    }

    /* Fill output object */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CopyHeader(           p_image_out, p_image_in);
        result |= R_OBJECT_SetImageWidth(        p_image_out, p_data->config.output_width);
        result |= R_OBJECT_SetImageHeight(       p_image_out, p_data->config.output_height);
        result |= R_OBJECT_SetImageBytesPerPixel(p_image_out, get_bytes_per_pixel(p_data->config.format));
        result |= R_OBJECT_SetImageDepth(        p_image_out, 1);
        result |= R_OBJECT_SetImageFormat(       p_image_out, p_data->config.format);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Couldn't set output params");
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        void * p_plane_in  = (void *)R_OBJECT_GetImagePlaneCPU(p_image_in);
        void * p_plane_out = (void *)R_OBJECT_GetImagePlaneCPU(p_image_out);

        /* Start image processing */
        if ((NULL != p_plane_in) && (NULL != p_plane_out))
        {
            if (process_image_planes(p_instance, p_plane_in, p_plane_out) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Error in process function");
                result = R_RESULT_FAILED;
            }
        }
        else
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Some error with getting image planes");
        }
    }

    return result;
}
