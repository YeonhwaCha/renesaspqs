/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_display_fb.c
 * @brief         Source file for displaying images using Linux frame buffer. See @ref PQS_Display_FB for more details
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @page       PQS_Display_FB_Algorithm  PQS Display FB Algorithm
 * @ingroup    PQS_Display_FB
 * # Algorithm Description:
 *             This module connects to a Linux frame buffer and will display an input image to an HDMI display. The
 *             Linux frame buffer will use the Display Unit (DU) hardware IP. The input image height and width must be
 *             less than the display height and with.
 * # Block Diagram:
 *             @image html display_fb.png
 *********************************************************************************************************************/
/***********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/


#include <fcntl.h>     /* For open */
#include <unistd.h>    /* For close */
#include <sys/mman.h>  /* For mmap */
#include <sys/ioctl.h> /* For ioctl */
#include <linux/fb.h>  /* For framebuffer */

#include "r_common.h"
#include "r_object_image.h"

#include "r_display_fb.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define DISPLAY_PRV_CHANNEL_UYVA_IN        (0u) /*!< Broker channel index for image in */

#define DISPLAY_PRV_MAX_FILE_NAME          (128u) /*!< Max size of frame buffer device name */

#define DISPLAY_PRV_DEFAULT_NAME           "/dev/fb0" /*!< Name for the display device. Can be overridden by config */

#define DISPLAY_PRV_DEFAULT_FORMAT         (R_IMAGE_FORMAT_RGB32_INTERLEAVED) /*!< Format of the connected display. Can be overridden by config */

#define DISPLAY_PRV_DEFAULT_HEIGHT         (288u)  /*!< Height of the connect display. Can be overridden by config */
#define DISPLAY_PRV_DEFAULT_WIDTH          (512u) /*!< Width of the connect display. Can be overridden by config */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Info about mmap buffer */
typedef struct
{
    void*   start;  /*!< Pointer to the start of mapped memory */
    size_t  length; /*!< Length of the mapped memory */
} st_mmap_buffer_t;

/*! Struct for a 32bit pixel */
typedef struct
{
    uint8_t first;  /*!< First byte of a pixel */
    uint8_t second; /*!< Second byte of a pixel */
    uint8_t third;  /*!< Third byte of a pixel */
    uint8_t fourth; /*!< Fourth byte of a pixel */
} st_pixel_32bit_t;

/*! Configuration structure for display_fb module */
typedef struct {
    char              fb_name[DISPLAY_PRV_MAX_FILE_NAME]; /*!< Name of fb device */
    e_image_format_t  format;                             /*!< Output format */
    uint32_t          height;                             /*!< Output height of image */
    uint32_t          width;                              /*!< Output width of image */
} st_display_cfg_t;

/*! Private data structure for display_fb module */
typedef struct st_priv_tag {
    st_display_cfg_t         config;        /*!< Configuration parameters */
    int                      fd;            /*!< File descriptor */
    struct fb_var_screeninfo variable_info; /*!< Variable info of connected display */
    struct fb_fix_screeninfo fixed_info;    /*!< Fixed info of connected display */
    st_mmap_buffer_t         mmap_buffer;   /*!< Mem map buffer Info */
} st_priv_t; /*lint !e9109 !e761 */

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_instance);
static r_result_t R_Deinit(st_module_instance_t * const p_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_instance);

/*! Module interface definition for display_fb */
const st_module_interface_t g_module_interface_display_fb = {
    .module_name   = "display_fb",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = R_Deinit,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/***********************************************************************************************************************
 Private global variables
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Local Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   get_bytes_per_pixel
 * @brief           Returns the bytes per pixel of the given format
 * @param [in]      format - Format of the image
 * @retval          0 - on failure, otherwise the bytes per pixel of the given image format
 *********************************************************************************************************************/
static uint32_t get_bytes_per_pixel(e_image_format_t format)
{
    uint32_t bpp = 0;

    switch (format) /*lint !e788 */
    {
        case R_IMAGE_FORMAT_RGB32_INTERLEAVED:
        case R_IMAGE_FORMAT_UYVA_INTERLEAVED:
        {
            bpp = 4;
            break;
        }

        /* Invalid format*/
        default:
        {
            R_PRINT_ERROR("Fell through switch. Format not supported.");
            break;
        }
    }

    return bpp;
}


/******************************************************************************************************************//**
 * Function Name:   set_background_color
 * @brief           Sets the display to black
 * @param [in]      p_data - Pointer to the private data structure
 *********************************************************************************************************************/
static void set_background_color(st_priv_t * const p_data)
{
    uint8_t * p_frame_buffer = (uint8_t *)(p_data->mmap_buffer.start);
    st_pixel_32bit_t pixel = {0};

    switch (p_data->config.format) /*lint !e788 */
    {
        case R_IMAGE_FORMAT_RGB32_INTERLEAVED:
        {
            /* Set pixel to black */
            pixel.first  = 0x00; // blue
            pixel.second = 0x00; // green
            pixel.third  = 0x00; // red
            pixel.fourth = 0x00; // alpha
            break;
        }

        case R_IMAGE_FORMAT_UYVA_INTERLEAVED:
        {
            /* Set pixel to black */
            pixel.first  = 0x80; // u
            pixel.second = 0x00; // y
            pixel.third  = 0x80; // v
            pixel.fourth = 0x00; // alpha
            break;
        }

        /* Invalid format*/
        default:
        {
            R_PRINT_ERROR("Invalid disp. format given (%d)", p_data->config.format);
            break;
        }
    }

    /* Set background color */
    for (uint32_t byte_index = 0u; byte_index < p_data->mmap_buffer.length; byte_index += 4u)
    {
        p_frame_buffer[byte_index + 0u] = pixel.first;
        p_frame_buffer[byte_index + 1u] = pixel.second;
        p_frame_buffer[byte_index + 2u] = pixel.third;
        p_frame_buffer[byte_index + 3u] = pixel.fourth;
    }
}

/******************************************************************************************************************//**
 * Function Name:   add_offset
 * @brief           Adds an offset? TODO: The variable names of this function are poor and confusing.
 * @param [in]      display - TODO
 * @param [in]      image - TODO
 * @param [in]      step - TODO
 * @retval          offset
 *********************************************************************************************************************/
static uint32_t add_offset(const uint32_t display, const uint32_t image, const uint32_t step)
{
    uint32_t offset = 0;
    if (display > image)
    {
        offset = (display - image) / 2u;
        offset *= step;
    }

    return offset;
}

/******************************************************************************************************************//**
 * @page    PQS_Display_FB_Config  PQS Display Framebuffer Config
 * @ingroup PQS_Display_FB
 * # Configuration Table:
 *
 *   Config Name  | Config Type      | Default                          | Range                                                               | Description
 *   ------------ | ------------     | -------------------------------- | ------------------------------------------------------------------- | ------------------
 *   fb_name      | String           | "/dev/fb0"                       | Less than DISPLAY_PRV_MAX_FILE_NAME characters                      | Name of the display buffer device
 *   format       | e_image_format_t | R_IMAGE_FORMAT_RGB32_INTERLEAVED | R_IMAGE_FORMAT_UYVY_INTERLEAVED or R_IMAGE_FORMAT_RGB32_INTERLEAVED | Format of the input image.
 *   height       | uint32_t         | 800                              | Any                                                                 | Height of the connected display
 *   width        | uint32_t         | 1280                             | Any                                                                 | Width of the connected display
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_config
 * @brief           Initializes the configuration structure with either the default values or with values from a config
 *                  file if available.
 * @param [in,out]  p_config - pointer to the configuration structure
 * @param [in]      p_instance_name - String of the module instance name
 * @param [in]      p_module_name - String of the module name
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_config(st_display_cfg_t * p_config, char const * const p_instance_name, char const * const p_module_name)
{
    r_result_t result = R_RESULT_SUCCESS;
    const char * format_names[] = E_IMAGE_FORMAT_AS_STRING;

    (void)p_module_name; //Not currently used

    /* Init config */
    result |= R_COMMON_STRING_Copy(p_config->fb_name, R_COMMON_CONFIG_GetString(p_instance_name, "fb_name", DISPLAY_PRV_DEFAULT_NAME), DISPLAY_PRV_MAX_FILE_NAME);
    p_config->format = R_COMMON_CONFIG_GET_ENUM( p_instance_name, "format",  DISPLAY_PRV_DEFAULT_FORMAT, format_names, (sizeof(format_names))/(sizeof(format_names[0])), e_image_format_t); /*lint !e9030 !e9034 */
    p_config->height = R_COMMON_CONFIG_GetUint32(p_instance_name, "height",  DISPLAY_PRV_DEFAULT_HEIGHT);
    p_config->width  = R_COMMON_CONFIG_GetUint32(p_instance_name, "width",   DISPLAY_PRV_DEFAULT_WIDTH);


    /* Check config */
    if ((R_IMAGE_FORMAT_UYVA_INTERLEAVED != p_config->format) && (R_IMAGE_FORMAT_RGB32_INTERLEAVED != p_config->format))
    {
        R_PRINT_ERROR("Input format not supported.");
        result = R_RESULT_FAILED;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   init_priv
 * @brief           Initializes the private data structure
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_priv(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    char const * const p_instance_name = R_MODULE_GetInstanceName(p_instance);
    char const * const p_module_name   = R_MODULE_GetModuleName(p_instance);
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    /* Check private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("No private data available");
        result = R_RESULT_FAILED;
    }

    /* Init configuration */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_config(&p_data->config, p_instance_name, p_module_name) != R_RESULT_SUCCESS)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Configuration init has failed");
        }
    }

    /* Init the rest of the private structure */
    if (R_RESULT_SUCCESS == result)
    {
        /* Zero out fixed info */
        R_COMMON_Memset(&p_data->fixed_info, 0, sizeof(p_data->fixed_info));

        /* Zero out variable info */
        R_COMMON_Memset(&p_data->variable_info, 0, sizeof(p_data->variable_info));

        /* Set fd */
        p_data->fd = -1;

        /* Set mmap */
        p_data->mmap_buffer.start  = NULL;
        p_data->mmap_buffer.length = 0;
    }

    return result;
}

/***********************************************************************************************************************
 Module Interface Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    r_result_t result = init_priv(p_instance);
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Open frame buffer */
    if (R_RESULT_SUCCESS == result)
    {
        p_data->fd = open(p_data->config.fb_name, O_RDWR, 0); /*lint !e9001 */

        if ((-1) == p_data->fd)
        {
            R_PRINT_ERROR("Cannot open");
            result = R_RESULT_FAILED;
        }
     }

    /* Get fixed screen information */
    if (R_RESULT_SUCCESS == result)
    {
        if ((-1) == ioctl(p_data->fd, FBIOGET_FSCREENINFO, &(p_data->fixed_info)))
        {
            R_PRINT_ERROR("Error reading fixed information");
            result = R_RESULT_FAILED;
        }
        else
        {
            R_PRINT_INFO("fb0 Fixed Info: ID= %s  Address= 0x%lx, length=%d, FB_TYPE=%d, Interleave=%d, FB_VISUAL=%d, line length=%d, acceleration= %d, capabilities=%d",
                    p_data->fixed_info.id,
                    p_data->fixed_info.smem_start,
                    p_data->fixed_info.smem_len,
                    p_data->fixed_info.type,
                    p_data->fixed_info.type_aux,
                    p_data->fixed_info.visual,
                    p_data->fixed_info.line_length,
                    p_data->fixed_info.accel,
                    p_data->fixed_info.capabilities);
        }
    }

    /* Check configuration is acceptable for fixed info */
    if (R_RESULT_SUCCESS == result)
    {
        if ((p_data->config.width * get_bytes_per_pixel(p_data->config.format)) > p_data->fixed_info.line_length)
        {
            R_PRINT_ERROR("Configured width is too large for connected display");
            result = R_RESULT_FAILED;
        }

        if ((p_data->config.height) > (p_data->fixed_info.smem_len / p_data->fixed_info.line_length))
        {
            R_PRINT_ERROR("Configured height is too large for connected display");
            result = R_RESULT_FAILED;
        }
    }

    /* Get variable screen information */
    if (R_RESULT_SUCCESS == result)
    {
        if ((-1) == ioctl(p_data->fd, FBIOGET_VSCREENINFO, &(p_data->variable_info)))
        {
            R_PRINT_ERROR("Error reading variable information");
            result = R_RESULT_FAILED;
        }
    }

    /* Update variable screen information */
    if (R_RESULT_SUCCESS == result)
    {
        /* TODO probably more info should be updated here */
        p_data->variable_info.xres           = p_data->config.width;
        p_data->variable_info.yres           = p_data->config.height;
        p_data->variable_info.bits_per_pixel = (get_bytes_per_pixel(p_data->config.format) * 8u);

        if ((-1) == ioctl(p_data->fd, FBIOPUT_VSCREENINFO, &(p_data->variable_info)))
        {
            R_PRINT_ERROR("Error setting variable information");
            result = R_RESULT_FAILED;
        }
        else
        {
            R_PRINT_INFO("FB variable Info: Width=%u, Height=%u, Bits per pixel=%u, Grayscale=%u, Non-standard =%u",
            p_data->variable_info.xres,
            p_data->variable_info.yres,
            p_data->variable_info.bits_per_pixel,
            p_data->variable_info.grayscale,
            p_data->variable_info.nonstd);

            R_PRINT_INFO("\t Red Info:    offset=%d, length=%d, msb=%d", p_data->variable_info.red.offset,    p_data->variable_info.red.length,    p_data->variable_info.red.msb_right);
            R_PRINT_INFO("\t Blue Info:   offset=%d, length=%d, msb=%d", p_data->variable_info.blue.offset,   p_data->variable_info.blue.length,   p_data->variable_info.blue.msb_right);
            R_PRINT_INFO("\t Green Info:  offset=%d, length=%d, msb=%d", p_data->variable_info.green.offset,  p_data->variable_info.green.length,  p_data->variable_info.green.msb_right);
            R_PRINT_INFO("\t Transp Info: offset=%d, length=%d, msb=%d", p_data->variable_info.transp.offset, p_data->variable_info.transp.length, p_data->variable_info.transp.msb_right);
        }
    }

    /* Map frame buffer to memory */
    if (R_RESULT_SUCCESS == result)
    {
        /* Figure out the size of the screen in bytes */
        p_data->mmap_buffer.length = p_data->fixed_info.smem_len;

        /* Map */
        p_data->mmap_buffer.start = mmap(NULL, p_data->mmap_buffer.length, PROT_READ | PROT_WRITE, MAP_SHARED, p_data->fd, 0); /*lint !e9130 */

        if (MAP_FAILED == p_data->mmap_buffer.start) /*lint !e923 */
        {
            R_PRINT_ERROR("Failed to map framebuffer device to memory");
            result = R_RESULT_FAILED;
        }
    }

    /* Set screen background */
    if (R_RESULT_SUCCESS == result)
    {
        set_background_color(p_data);
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_Deinit
 * @brief           Deinitializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Deinit(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Unmap frame buffer from memory */
    if (R_RESULT_SUCCESS == result)
    {
        if ((-1) == munmap(p_data->mmap_buffer.start, p_data->mmap_buffer.length))
        {
            R_PRINT_ERROR("Failed to unmap framebuffer device from memory");
            result = R_RESULT_FAILED;
        }
    }

    /* Close frame buffer */
    if (R_RESULT_SUCCESS == result)
    {
        if (0 <= p_data->fd)
        {
            if ((-1) == close(p_data->fd))
            {
                R_PRINT_ERROR("Cannot close framebuffer");
                result = R_RESULT_FAILED;
            }
            else
            {
                p_data->fd = -1;
            }
        }
     }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Display_FB_Channels PQS Display FB Channels
 * @ingroup    PQS_Display_FB
 * # Channel Table:
 *
 *  Channel name | Channel Type | Object Type | Description
 *  ------------ | ------------ | ----------- | ----------------------------------
 *  uyva_in      | input        | image       | @ref PQS_Display_FB_Channel_Image_In
 *
 * @anchor PQS_Display_FB_Channel_Image_In
 * ## PQS Display FB Channel Image In
 *             Input is either an YUV444 interleaved UYVA image of type R_IMAGE_FORMAT_UYVA_INTERLEAVED or a BGRA image
 *             of type R_IMAGE_FORMAT_RGB32_INTERLEAVED. The height and width of this image must be less than the
 *             height and width of the connected display.
 *
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_BROKER_InitChannelInfo(p_instance, DISPLAY_PRV_CHANNEL_UYVA_IN, "uyva_in", R_OBJECT_TYPE_IMAGE, R_BROKER_DATA_DIRECTION_IN);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    r_result_t result        = R_RESULT_SUCCESS;
    uint8_t * p_image_line   = NULL;
    uint32_t image_width     = 0;
    uint32_t image_height    = 0;
    uint32_t image_step      = 0;
    uint32_t image_stride    = 0;
    st_priv_t * p_data       = R_MODULE_GetPrivDataPtr(p_instance);
    st_object_t * p_object   = R_BROKER_GetObjectPtr(p_instance, DISPLAY_PRV_CHANNEL_UYVA_IN);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    if (R_OBJECT_CheckObjectType(p_object, R_OBJECT_TYPE_IMAGE) != R_RESULT_SUCCESS)
    {
        R_PRINT_ERROR("Invalid object input");
        result = R_RESULT_FAILED;
    }

    /* Get the actual memory address */
    if (R_RESULT_SUCCESS == result)
    {
        p_image_line = R_OBJECT_GetImagePlaneCPU(p_object);

        if (NULL == p_image_line)
        {
            result = R_RESULT_FAILED;
        }
    }

    /* Make sure depth/type are correct */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CheckImageDepth(         p_object, 1);
        result |= R_OBJECT_CheckImageBytesPerPixel( p_object, get_bytes_per_pixel(p_data->config.format));
        result |= R_OBJECT_CheckImageFormat(        p_object, p_data->config.format);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Invalid depth | bytes per pixel | type");
        }
    }

    /* Get image height/width/step */
    if (R_RESULT_SUCCESS == result)
    {
        image_width  = R_OBJECT_GetImageWidth(p_object);
        image_height = R_OBJECT_GetImageHeight(p_object);
        image_step   = R_OBJECT_GetImageBytesPerPixel(p_object);
        image_stride = image_width * image_step;

        if ((0u == image_width) || (0u == image_height) || (0u == image_step))
        {
            /* Too small */
            R_PRINT_ERROR("Image has width || height || step == 0");
            result = R_RESULT_FAILED;
        }

        if ((p_data->variable_info.xres < image_width) || (p_data->variable_info.yres < image_height))
        {
            /* Too big */
            R_PRINT_ERROR("Image is too big to view on display");
            R_PRINT_INFO ("Image width: %d, Display width: %d, Image height: %d, Display height: %d \n", image_width, p_data->variable_info.xres, image_height, p_data->variable_info.yres );
            result = R_RESULT_FAILED;
        }
    }

    /* Display image */
    if (R_RESULT_SUCCESS == result)
    {
        /* Get the start of the first centered line */
        uint32_t offset = 0;

        /* Add x offset */
        offset += add_offset(p_data->variable_info.xres, image_width, image_step);

        /* Add y offset */
        offset += add_offset(p_data->variable_info.yres, image_height, p_data->variable_info.xres * image_step);

        /* Get fb line */
        uint8_t * p_frame_buffer_line = &(((uint8_t *)(p_data->mmap_buffer.start))[offset]);

        /* Loop though height */
        for (uint32_t height_index = 0; height_index < image_height; height_index++)
        {
            /* Copy line from image to framebuffer */
            R_COMMON_Memcpy(p_frame_buffer_line, p_image_line, image_stride);

            /* Update line pointers */
            p_frame_buffer_line = &(p_frame_buffer_line[p_data->fixed_info.line_length]);
            p_image_line = &(p_image_line[image_stride]);
        }
    }

    /* Print object description for image */
    if (R_RESULT_SUCCESS == result)
    {
        char image_description[R_OBJECT_MAX_DESCRIPTION_LENGTH] = {'\0'};

        if (R_OBJECT_GetDescription(p_object, image_description) == R_RESULT_SUCCESS)
        {
            /* Only print description if the string contains more than just NULL termination */
            if (R_COMMON_STRING_Length(image_description) > 1u)
            {
                R_PRINT_INFO("Image description: %s", image_description);
            }
        }
    }

    /* Print total processing time */
    if (R_RESULT_SUCCESS == result)
    {
        uint64_t image_timestamp = 0;

        if (R_OBJECT_GetTimeStamp(p_object, &image_timestamp) == R_RESULT_SUCCESS)
        {
            R_TIMER_LogTimestamp(p_instance, R_COMMON_TIME_GetRealUsec() - image_timestamp, "Total processing time of pipeline is");
        }
    }

    return result;
}

