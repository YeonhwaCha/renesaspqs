/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_draw_2d_box.c
 * @brief         Source file for drawing of 2d boxes over image. See @ref PQS_Draw_2dBox for more details
 *********************************************************************************************************************/
/*****************************************************************************************************************//**
 * @page       PQS_Draw_2dBox_Algorithm  PQS Draw 2D Box Algorithm
 * @ingroup    PQS_Draw_2dBox
 * # Algorithm Description:
 *             This module takes a 2D Box object and draws the detected boxes over an input/output image. Vehicles
 *             boxes are drawn in blue and pedestrian boxes are drawn in red. The input image and the box must both
 *             have the exact same timestamp to assure the boxes are drawn on the correct image. Other box information
 *             such as box class, detection confidence, and obstacle id can be printed to screen as well. They will
 *             appear at thetop right corner of the 2D box.
 * # Block Diagram:
 *             @image html draw_2dbox.png
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include "r_common.h"
#include "r_common_text.h"
#include "r_object_2d_box.h"
#include "r_object_image.h"

/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

#define DRAW_PRV_CHANNEL_IMAGE_INOUT  (0)          /*!< Broker Channel for in/out image */
#define DRAW_PRV_CHANNEL_BBOX_IN      (1)          /*!< Broker Channel for input 2d box */

#define DRAW_PRV_MESSAGE_BOARDER      (2)          /*!< How many pixels to have around message text */
#define DRAW_PRV_MAX_MESSAGE_LENGTH   (32)         /*!< Maximum length of messages drawn on image */

#define DRAW_PRV_DEFAULT_DRAW_CLASS       (true)   /*!< Default to draw box class */
#define DRAW_PRV_DEFAULT_DRAW_CONFIDENCE  (true)   /*!< Default to draw box confidence */
#define DRAW_PRV_DEFAULT_DRAW_ID          (true)   /*!< Default to draw box id */

/**********************************************************************************************************************
 Typedef definitions
 *********************************************************************************************************************/

/*! Struct for uyva pixel */
typedef struct
{
    uint8_t u; /*!< u byte */
    uint8_t y; /*!< y byte */
    uint8_t v; /*!< v byte */
    uint8_t a; /*!< alpha byte */
} st_uyva_t;

/*! Union for uyva pixel, allows to access a uyva pixel per individual 8bit element or the whole 32bit pixel at once*/
typedef union
{
    st_uyva_t subpixel; /*!< 8bit subpixel struct */
    uint32_t  pixel;    /*!< Full 32 bit pixel */
} u_yuva_t;

/*! Configuration Struct for draw_2d_box module */
typedef struct
{
    bool draw_class;       /*!< Bool to draw the box class message */
    bool draw_confidence;  /*!< Bool to draw the box confidence message */
    bool draw_id;          /*!< Bool to draw the box id message */
} st_draw_cfg_t;

/*! Private data structure for draw_2d_box module */
typedef struct st_priv_tag {
    st_draw_cfg_t config;      /*!< Configuration structure */
    u_yuva_t pedestrian_color; /*!< Color for pedestrian class */
    u_yuva_t vehicle_color;    /*!< Color for vehicle class */
    u_yuva_t unknown_color;    /*!< Color for unknown class */
    u_yuva_t text_color;       /*!< Color for text */
    int32_t  thickness;        /*!< Thickness of box lines */
} st_priv_t;

/**********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 *********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_module_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_module_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_module_instance);

/*! Module interface definition for draw_2d_box */
const st_module_interface_t g_module_interface_draw_2d_box = {
    .module_name   = "draw_2d_box",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = NULL,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/**********************************************************************************************************************
 Local Functions
 *********************************************************************************************************************/

/******************************************************************************************************************//**
 * @page    PQS_Draw_2dBox_Config  PQS Draw 2D Box Config
 * @ingroup PQS_Draw_2dBox
 * # Configuration Table:
 *
 *   Config Name     | Config Type | Default | Range       | Description
 *   --------------- | ----------- | --------| ------------| ------------------
 *   draw_class      | bool        | true    | true/false  | True if class message should be drawn for each box
 *   draw_confidence | bool        | true    | true/false  | True if confidence message should be drawn for each box
 *   draw_id         | bool        | true    | true/false  | True if id message should be drawn for each box
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_config
 * @brief           Initializes the configuration structure with either the default values or with values from a config
 *                  file if available.
 * @param [in,out]  p_config - pointer to the configuration structure
 * @param [in]      p_instance_name - String of the module instance name
 * @param [in]      p_module_name - String of the module name
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_config(st_draw_cfg_t * p_config, char const * const p_instance_name, char const * const p_module_name)
{
    r_result_t result = R_RESULT_SUCCESS;

    /* Init config */
    p_config->draw_class      = R_COMMON_CONFIG_GetBool(p_instance_name, "draw_class",      DRAW_PRV_DEFAULT_DRAW_CLASS);
    p_config->draw_confidence = R_COMMON_CONFIG_GetBool(p_instance_name, "draw_confidence", DRAW_PRV_DEFAULT_DRAW_CONFIDENCE);
    p_config->draw_id         = R_COMMON_CONFIG_GetBool(p_instance_name, "draw_id",         DRAW_PRV_DEFAULT_DRAW_ID);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   get_color
 * @brief           Returns a pointer for the color corresponding to the given box_class
 * @param [in]      p_data - pointer to the private data structure
 * @param [in]      box_class - Class of the box to get the color for
 * @retval          Pointer to the color to use
 *********************************************************************************************************************/
static const u_yuva_t * get_color(const st_priv_t * p_data, e_2d_box_class_t box_class)
{
    const u_yuva_t * p_color;

    switch (box_class)
    {
        case R_2D_BOX_CLASS_PEDESTRIAN:
        {
            p_color = &p_data->pedestrian_color;
            break;
        }

        case R_2D_BOX_CLASS_VEHICLE:
        {
            p_color = &p_data->vehicle_color;
            break;
        }

        default:
        {
            p_color = &p_data->unknown_color;
            break;
        }
    }

    return p_color;
}

/******************************************************************************************************************//**
 * Function Name:   fill_message
 * @brief           Fills in the p_message string with the message to draw over the image for a given 2d box.
 * @param [in]      p_data - pointer to the private data structure
 * @param [in]      p_bbox - Pointer to the box data to use to generate message
 * @param [out]     p_message - String to fill in with generated message
 * @param [in]      message_length - Length of the p_messagse character array
 *********************************************************************************************************************/
static void fill_message(const st_priv_t * p_data, const st_2d_box_t * p_bbox, char * const p_message, uint32_t message_length)
{
    char class_string[DRAW_PRV_MAX_MESSAGE_LENGTH]      = {0};
    char confidence_string[DRAW_PRV_MAX_MESSAGE_LENGTH] = {0};
    char id_string[DRAW_PRV_MAX_MESSAGE_LENGTH]         = {0};
    bool space_needed = false;

    /* Get the proper message based off of the box class */
    switch (p_bbox->box_class)
    {
        case R_2D_BOX_CLASS_PEDESTRIAN:
        {
            snprintf(class_string, DRAW_PRV_MAX_MESSAGE_LENGTH, "PED");
            break;
        }

        case R_2D_BOX_CLASS_VEHICLE:
        {
            snprintf(class_string, DRAW_PRV_MAX_MESSAGE_LENGTH, "VEH");
            break;
        }

        default:
        {
            snprintf(class_string, DRAW_PRV_MAX_MESSAGE_LENGTH, "UNKNOWN");
            break;
        }
    }

    snprintf(confidence_string, DRAW_PRV_MAX_MESSAGE_LENGTH, "%u%%", (uint32_t)(p_bbox->confidence * 100));
    snprintf(id_string,         DRAW_PRV_MAX_MESSAGE_LENGTH, "ID=%u", p_bbox->id);

    /* Clear message */
    R_COMMON_Memset(p_message, 0, message_length);

    /* Write box class */
    if (true == p_data->config.draw_class)
    {
        if (true == space_needed)
        {
            (void)R_COMMON_STRING_Combine(p_message, " ", message_length);
        }

        (void)R_COMMON_STRING_Combine(p_message, class_string, message_length);
        space_needed = true;
    }

    /* Write box confidence */
    if (true == p_data->config.draw_confidence)
    {
        if (true == space_needed)
        {
            (void)R_COMMON_STRING_Combine(p_message, " ", message_length);
        }

        (void)R_COMMON_STRING_Combine(p_message, confidence_string, message_length);
        space_needed = false; // There already is a lot of space after the % so we do not need extra space
    }

    /* Write box ID */
    if (true == p_data->config.draw_id)
    {
        if (true == space_needed)
        {
            (void)R_COMMON_STRING_Combine(p_message, " ", message_length);
        }

        (void)R_COMMON_STRING_Combine(p_message, id_string, message_length);
        space_needed = true;
    }
}

/******************************************************************************************************************//**
 * Function Name:   draw_letter
 * @brief           Draws a letter to the supplied image.
 * @param [in]      letter - character to draw
 * @param [in]      color - color to use to draw character
 * @param [out]     p_image - pointer to the part of the image plane to start drawing on. This can be any part of
 *                  the image.
 * @param [in]      image_width - width of the image in pixels
 *********************************************************************************************************************/
static void draw_letter(const char letter, u_yuva_t color, uint32_t * p_image, uint32_t image_width)
{
    const a_letter_t * p_letter_array = R_COMMON_TEXT_GetLetterArray(letter);
    uint32_t i = 0;
    uint32_t j = 0;

    for (i = 0; i < R_COMMON_LETTER_HEIGHT; i++)
    {
        for (j = 0; j < R_COMMON_LETTER_WIDTH; j++)
        {
            if (R_COMMON_TEXT_GetLetterValue(p_letter_array, i, j) == true)
            {
                p_image[j + (i * image_width)] = color.pixel;
            }
        }
    }
}

/******************************************************************************************************************//**
 * Function Name:   draw_bbox
 * @brief           Draws a bounding box on the image
 * @param [in]      p_data - pointer to the private data structure
 * @param [in]      p_bbox - Pointer to the box data to draw
 * @param [out]     p_image - pointer to image plane to draw on.
 * @param [in]      width - width of the image
 * @param [in]      height - height of the image
 *********************************************************************************************************************/
static void draw_bbox(const st_priv_t * p_data, const st_2d_box_t * p_bbox, uint32_t * p_image, uint32_t width, uint32_t height)
{
    int32_t xmin_in  = R_COMMON_Clip(p_bbox->xmin               , 0, width);
    int32_t xmin_out = R_COMMON_Clip(xmin_in - p_data->thickness, 0, width);
    int32_t xmax_in  = R_COMMON_Clip(p_bbox->xmax               , 0, width);
    int32_t xmax_out = R_COMMON_Clip(xmax_in + p_data->thickness, 0, width);

    int32_t ymin_in  = R_COMMON_Clip(p_bbox->ymin               , 0, height);
    int32_t ymin_out = R_COMMON_Clip(ymin_in - p_data->thickness, 0, height);
    int32_t ymax_in  = R_COMMON_Clip(p_bbox->ymax               , 0, height);
    int32_t ymax_out = R_COMMON_Clip(ymax_in + p_data->thickness, 0, height);

    const u_yuva_t * p_color = get_color(p_data, p_bbox->box_class);
    char message[DRAW_PRV_MAX_MESSAGE_LENGTH] = {0};

    int32_t message_length = 0;
    int32_t message_ymin   = 0;
    int32_t message_ymax   = 0;
    int32_t message_xmin   = 0;
    int32_t message_xmax   = 0;
    int32_t x              = 0;
    int32_t y              = 0;
    int32_t y_off          = 0;
    int32_t letter_index   = 0;

    /* Get the message to write to screen */
    fill_message(p_data, p_bbox, message, DRAW_PRV_MAX_MESSAGE_LENGTH);

    /* Update parameters based on message length */
    message_length = (int32_t)R_COMMON_STRING_Length(message) - 1;  // Subtract 1 because we do not care about the null termination
    message_ymin   = R_COMMON_Clip(ymin_in - (R_COMMON_LETTER_HEIGHT + (2 * DRAW_PRV_MESSAGE_BOARDER)), 0, height);
    message_ymax   = ymin_in;
    message_xmin   = xmin_out;
    message_xmax   = R_COMMON_Clip(xmin_out + (message_length * R_COMMON_LETTER_WIDTH) + (2 * DRAW_PRV_MESSAGE_BOARDER), 0, width);

    /* Draw top line */
    for (y = ymin_out; ymin_in > y; y++)
    {
        y_off = y * width;
        for (x = xmin_out; xmax_out > x; x++)
        {
            p_image[y_off + x] = p_color->pixel;
        }
    }

    /* Draw left/right */
    for (y = ymin_in; ymax_in > y; y++)
    {
        y_off = y * width;

        /* Left */
        for (x = xmin_out; xmin_in > x; x++)
        {
            p_image[y_off + x] = p_color->pixel;
        }

        /* Right */
        for (x = xmax_in; xmax_out > x; x++)
        {
            p_image[y_off + x] = p_color->pixel;
        }
    }

    /* Draw bottom line */
    for (y = ymax_in; ymax_out > y; y++)
    {
        y_off = y * width;
        for (x = xmin_out; xmax_out > x; x++)
        {
            p_image[y_off + x] = p_color->pixel;
        }
    }

    /* Draw box for message */
    for (y = message_ymin; y < message_ymax; y++)
    {
        y_off = y * width;
        for (x = message_xmin; x < message_xmax; x++)
        {
            p_image[y_off + x] = p_color->pixel;
        }
    }

    /* Write message */
    for (letter_index = 0; letter_index < message_length; letter_index++)
    {
        x = message_xmin + DRAW_PRV_MESSAGE_BOARDER + (letter_index * R_COMMON_LETTER_WIDTH);
        y = message_ymin + DRAW_PRV_MESSAGE_BOARDER;

        /* Check if letter overruns image */
        if (((x + R_COMMON_LETTER_WIDTH) <= message_xmax) && ((y + R_COMMON_LETTER_HEIGHT) <= message_ymax))
        {
            draw_letter(message[letter_index], p_data->text_color, &p_image[x + (y * width)], width);
        }
    }
}

/**********************************************************************************************************************
 Module Interface
 *********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    r_result_t result  = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);
    char const * const p_instance_name = R_MODULE_GetInstanceName(p_instance);
    char const * const p_module_name   = R_MODULE_GetModuleName(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("No private data");
        result = R_RESULT_FAILED;
    }

    /* Init configuration */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_config(&p_data->config, p_instance_name, p_module_name) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Configuration init has failed");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* Blue */
        p_data->vehicle_color.subpixel.y = 0x1D;
        p_data->vehicle_color.subpixel.u = 0xFF;
        p_data->vehicle_color.subpixel.v = 0x68;
        p_data->vehicle_color.subpixel.a = 0x00;

        /* Red */
        p_data->pedestrian_color.subpixel.y = 0x4c;
        p_data->pedestrian_color.subpixel.u = 0x54;
        p_data->pedestrian_color.subpixel.v = 0xFF;
        p_data->pedestrian_color.subpixel.a = 0x00;

        /* Black */
        p_data->unknown_color.subpixel.y = 0;
        p_data->unknown_color.subpixel.u = 0x80;
        p_data->unknown_color.subpixel.v = 0x80;
        p_data->unknown_color.subpixel.a = 0;

        /* Light grey */
        p_data->text_color.subpixel.y = 0xA0;
        p_data->text_color.subpixel.u = 0x80;
        p_data->text_color.subpixel.v = 0x80;
        p_data->text_color.subpixel.a = 0;

        p_data->thickness = 4;
    }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Draw_2dBox_Channels PQS Draw 2D Box Channels
 * @ingroup    PQS_Draw_2dBox
 * # Channel Table:
 *
 *  Channel name | Channel Type | Object Type  | Description
 *  ------------ | ------------ | ------------ | ----------------------------------
 *  image_inout  | input/output | image        | @ref PQS_Draw_2dBox_Channel_image
 *  bbox_in      | input        | 2D Box       | @ref PQS_Draw_2dBox_Channel_bbox
 *
 * @anchor PQS_Draw_2dBox_Channel_image
 * ## PQS Draw 2D Box Channel image
 *             This is the input and output image to draw on. Not this module will alter the contents of the image.
 *             No other module should be using the image while this module is executing. The format of the
 *             image must be in R_IMAGE_FORMAT_UYVA_INTERLEAVED.
 *
 * @anchor PQS_Draw_2dBox_Channel_bbox
 * ## PQS Draw 2D Box Channel bbox
 *             This is the 2D bounding box object containing the list of all the bounding boxes to draw on the
 *             image.
 *
 * # Time Sync:
 *        To assure that the bounding box and the image are synchronized, the timestamp of both objects will be
 *        compared. If the timestamps are different this module will return a failure.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_BROKER_InitChannelInfo(p_instance, DRAW_PRV_CHANNEL_IMAGE_INOUT, "image_inout", R_OBJECT_TYPE_IMAGE,  R_BROKER_DATA_DIRECTION_INOUT);
    result |= R_BROKER_InitChannelInfo(p_instance, DRAW_PRV_CHANNEL_BBOX_IN,     "bbox_in",     R_OBJECT_TYPE_2D_BOX, R_BROKER_DATA_DIRECTION_IN);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    r_result_t result                  = R_RESULT_SUCCESS;
    st_priv_t * p_data                 = R_MODULE_GetPrivDataPtr(p_instance);

    st_object_t * p_object_image_inout = R_BROKER_GetObjectPtr(p_instance, DRAW_PRV_CHANNEL_IMAGE_INOUT);
    uint32_t * p_image                 = (uint32_t *)R_OBJECT_GetImagePlaneCPU(p_object_image_inout);
    uint32_t width                     = R_OBJECT_GetImageWidth(p_object_image_inout);
    uint32_t height                = R_OBJECT_GetImageHeight(p_object_image_inout);

    st_object_t * p_object_bbox_in     = R_BROKER_GetObjectPtr(p_instance, DRAW_PRV_CHANNEL_BBOX_IN);
    int32_t max_objects                = R_OBJECT_Get2DBoxCount(p_object_bbox_in);
    uint64_t image_timestamp           = 0;
    uint64_t bbox_timestamp            = 0;

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        if ((R_OBJECT_CheckObjectType(p_object_image_inout, R_OBJECT_TYPE_IMAGE)  != R_RESULT_SUCCESS) ||
            (R_OBJECT_CheckObjectType(p_object_bbox_in,     R_OBJECT_TYPE_2D_BOX) != R_RESULT_SUCCESS))
        {
            R_PRINT_ERROR("Invalid object passed");
            result = R_RESULT_FAILED;
        }
    }

    /* Check timestamps */
    if (R_RESULT_SUCCESS == result)
    {
        /*if ((R_OBJECT_GetTimeStamp(p_object_image_inout, &image_timestamp) == R_RESULT_SUCCESS) &&
            (R_OBJECT_GetTimeStamp(p_object_bbox_in,     &bbox_timestamp)  == R_RESULT_SUCCESS))
        {
            if (bbox_timestamp != image_timestamp)
            {
                R_PRINT_ERROR("Timestamps are not synced. Image = %lu BBox = %lu", image_timestamp, bbox_timestamp);
                result = R_RESULT_FAILED;
            }
        }
        else
        {
            R_PRINT_ERROR("Cannot get timestamps");
            result = R_RESULT_FAILED;
        }*/
    }

    /* Check image */
    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == p_image)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image data pointer");
        }

        if (R_OBJECT_GetImageFormat(p_object_image_inout) != R_IMAGE_FORMAT_UYVA_INTERLEAVED)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image format.  Expected format %d, got %d", R_IMAGE_FORMAT_UYVA_INTERLEAVED, R_OBJECT_GetImageFormat(p_object_image_inout));
        }

        if (0 == width)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image width");
        }

        if (0 == height)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image height");
        }
    }

    /* Draw boxes */
    if (R_RESULT_SUCCESS == result)
    {
        int32_t i;

        for(i = 0; max_objects > i; i++)
        {
            st_2d_box_t * p_bbox = R_OBJECT_Get2DBox(p_object_bbox_in, i);

            if (NULL != p_bbox)
            {
                draw_bbox(p_data, p_bbox, p_image, width, height);
            }
        }
    }

    return result;
}


