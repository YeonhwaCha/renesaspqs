/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_draw_lane.c
 * @brief         Source file for drawing of lane data over image. See @ref PQS_Draw_Lane for more details
 *********************************************************************************************************************/
/*****************************************************************************************************************//**
 * @page       PQS_Draw_Lane_Algorithm  PQS Draw Lane Algorithm
 * @ingroup    PQS_Draw_Lane
 * # Algorithm Description:
 *             This module takes a Lane object and draws the lane data over an input/output image. Lanes are drawn in
 *             colors determined by the type of lane. The distance from the center of the car to the lane in meters
 *             is also drawn on the screen. Positive values represent lanes to the right and negative values
 *             represent lanes to the left.
 *
 * # Block Diagram:
 *             @image html draw_lane.png
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include "r_common.h"
#include "r_common_text.h"
#include "r_object_lane.h"
#include "r_object_image.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define DRAW_PRV_CHANNEL_IMAGE_INOUT  (0)       /*!< Broker Channel for in/out image */
#define DRAW_PRV_CHANNEL_LANE_IN      (1)       /*!< Broker Channel for input lane */

#define DRAW_PRV_MESSAGE_BOARDER      (2)       /*!< How many pixels to have around message text */
#define DRAW_PRV_MAX_MESSAGE_LENGTH   (32)      /*!< Maximum length of messages drawn on image */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Struct for uyva pixel */
typedef struct
{
    uint8_t u; /*!< u byte */
    uint8_t y; /*!< y byte */
    uint8_t v; /*!< v byte */
    uint8_t a; /*!< alpha byte */
} st_uyva_t;

/*! Union for uyva pixel, allows to access a uyva pixel per individual 8bit element or the whole 32bit pixel at once*/
typedef union
{
    st_uyva_t subpixel; /*!< 8bit subpixel struct */
    uint32_t  pixel;    /*!< Full 32 bit pixel */
} u_yuva_t;

/*! Private data structure for draw_lane module */
typedef struct st_priv_tag
{
    u_yuva_t white_lane_color;         /*!< Color for white lane */
    u_yuva_t yellow_lane_color;        /*!< Color for yellow lane */
    u_yuva_t extrapolated_lane_color;  /*!< Color for extrapolated lane */
    u_yuva_t text_color;               /*!< Color for message text */
    int32_t  thickness;                /*!< Thickness of lane */
    int32_t  dashed_lane_segment_size; /*!< Dash size */
} st_priv_t;

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_module_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_module_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_module_instance);

/*! Module interface definition for draw_lane */
const st_module_interface_t g_module_interface_draw_lane = {
    .module_name   = "draw_lane",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = NULL,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/***********************************************************************************************************************
 Local Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   get_color
 * @brief           Returns a pointer for the color corresponding to the given lane
 * @param [in]      p_data - pointer to the private data structure
 * @param [in]      p_lane - pointer to the lane
 * @retval          Pointer to the color to use
 *********************************************************************************************************************/
static const u_yuva_t * get_color(st_priv_t * p_data, st_lane_t * p_lane)
{
    const u_yuva_t * p_color;

    if (true == p_lane->extrapolated)
    {
        p_color = &p_data->extrapolated_lane_color;
    }
    else
    {
        switch (p_lane->color)
        {
            case R_LANE_COLOR_YELLOW:
            {
                p_color = &p_data->yellow_lane_color;
                break;
            }

            case R_LANE_COLOR_WHITE:
            {
                p_color = &p_data->white_lane_color;
                break;
            }

            default:
            {
                R_PRINT_ERROR("Color not supported. Defaulting to white");
                p_color = &p_data->white_lane_color;
                break;
            }
        }
    }

    return p_color;
}

/******************************************************************************************************************//**
 * Function Name:   draw_letter
 * @brief           Draws a letter to the supplied image.
 * @param [in]      letter - character to draw
 * @param [in]      color - color to use to draw character
 * @param [out]     p_image - pointer to the part of the image plane to start drawing on. This can be any part of
 *                  the image.
 * @param [in]      image_width - width of the image in pixels
 *********************************************************************************************************************/
static void draw_letter(const char letter, u_yuva_t color, uint32_t * p_image, uint32_t image_width)
{
    const a_letter_t * p_letter_array = R_COMMON_TEXT_GetLetterArray(letter);
    uint32_t i = 0;
    uint32_t j = 0;

    for (i = 0; i < R_COMMON_LETTER_HEIGHT; i++)
    {
        for (j = 0; j < R_COMMON_LETTER_WIDTH; j++)
        {
            if (R_COMMON_TEXT_GetLetterValue(p_letter_array, i, j) == true)
            {
                p_image[j + (i * image_width)] = color.pixel;
            }
        }
    }
}

/* TODO this function could be cleaned up to make it more readible / maintainable */
/******************************************************************************************************************//**
 * Function Name:   draw_lane
 * @brief           Draws a bounding box on the image
 * @param [in]      p_data - pointer to the private data structure
 * @param [in]      p_lane - Pointer to the lane data to draw
 * @param [out]     p_image - pointer to image plane to draw on.
 * @param [in]      width - width of the image
 * @param [in]      height - height of the image
 *********************************************************************************************************************/
static void draw_lane(st_priv_t * p_data, st_lane_t * p_lane, uint32_t * p_image, int32_t width, int32_t height)
{
    int32_t  x     = 0;
    int32_t  x_end = 0;
    int32_t  y     = 0;
    int32_t  y_off = 0;
    int32_t  dash_size = 0;
    int32_t  ymin = R_COMMON_Clip((int32_t)(p_lane->ymin), 0, height);
    int32_t  ymax = R_COMMON_Clip((int32_t)(p_lane->ymax), 0, height);
    int32_t  x_bottom = 0;
    const u_yuva_t * p_color = get_color(p_data, p_lane);

    float x_f = 0.0;
    float y_f = 0.0;

    char message[DRAW_PRV_MAX_MESSAGE_LENGTH] = {0};
    int32_t message_length = 0;
    int32_t message_ymin   = R_COMMON_Clip(ymax - (R_COMMON_LETTER_HEIGHT + (2 * DRAW_PRV_MESSAGE_BOARDER)), 0, height);
    int32_t message_ymax   = ymax;
    int32_t message_xmin   = 0;
    int32_t message_xmax   = 0;
    int32_t letter_index   = 0;

    /* Create the message from the offset value  */
    if (p_lane->offset > 0)
    {
        /* Add in a + sign for positive values */
        (void)snprintf(message, DRAW_PRV_MAX_MESSAGE_LENGTH, "+%.2fm", p_lane->offset);
    }
    else
    {
        /* Negative values will already have - sign */
        (void)snprintf(message, DRAW_PRV_MAX_MESSAGE_LENGTH, "%.2fm", p_lane->offset);
    }

    message_length = (int32_t)R_COMMON_STRING_Length(message) - 1; //Subtract 1 because we do not care about the null termination

    /* Calculate the bottom of the lane */
    y_f = (float)(ymax);
    x_f = (y_f * ((p_lane->a * y_f) + p_lane->b)) + p_lane->c;
    x_bottom = R_COMMON_Clip((int32_t)(x_f), 0, width);

    /* TODO this whole logic could really be improved so that is not piecewise */
    /* Get x values for min and max based of the message size */
    if (x_bottom < (width / 2))
    {
        /* Left side of screen */
        message_xmin = x_bottom;
        message_xmax = R_COMMON_Clip(message_xmin + (message_length * R_COMMON_LETTER_WIDTH) + (2 * DRAW_PRV_MESSAGE_BOARDER), 0, width);
    }
    else
    {
        /* Right side of screen */
        message_xmin = R_COMMON_Clip(x_bottom - ((message_length * R_COMMON_LETTER_WIDTH) + (2 * DRAW_PRV_MESSAGE_BOARDER)), 0, width);
        message_xmax = x_bottom;
    }

    /* Get the dash length */
    if (R_LANE_TYPE_DASHED == p_lane->type)
    {
        dash_size = p_data->dashed_lane_segment_size;
    }
    else
    {
        dash_size = ymax - ymin;
    }

    /* Draw line */
    for (y = ymin; ymax > y; y += dash_size)
    {
        int32_t y_local_max = R_COMMON_Clip(y + dash_size, 0, ymax);

        /* TODO do not reuse loop variable y here */
        /* Note: Reusing loop variable y here */
        for(; y_local_max > y; y++)
        {
            /* Calculate y offset */
            y_off = y * width;

            /* Calculate x */
            y_f   = (float)(y);
            x_f   = (y_f * ((p_lane->a * y_f) + p_lane->b)) + p_lane->c;

            /* Subtract thickness offset and round to nearest integer */
            x_f   = (x_f - ((float)(p_data->thickness) / 2.0)) + 0.5;
            x     = (int32_t)(x_f);

            /* Clip values so they fit on screen */
            x     = R_COMMON_Clip(x, 0, width);
            x_end = R_COMMON_Clip(x + p_data->thickness, 0, width);

            /* Draw the segment */
            for(; x_end>x; x++)
            {
                p_image[y_off + x] = p_color->pixel;
            }
        }
    }

    /* Draw box for message */
    for (y = message_ymin; y < message_ymax; y++)
    {
        y_off = y * width;
        for (x = message_xmin; x < message_xmax; x++)
        {
            p_image[y_off + x] = p_color->pixel;
        }
    }

    /* Write message */
    for(letter_index = 0; letter_index <= message_length; letter_index++)
    {
        x = message_xmin + DRAW_PRV_MESSAGE_BOARDER + (letter_index * R_COMMON_LETTER_WIDTH);
        y = message_ymin + DRAW_PRV_MESSAGE_BOARDER;

        /* Check if letter overruns image */
        if (((x + R_COMMON_LETTER_WIDTH) <= message_xmax) && ((y + R_COMMON_LETTER_HEIGHT) <= message_ymax))
        {
            draw_letter(message[letter_index], p_data->text_color, &p_image[x + (y * width)], width);
        }
    }
}

/***********************************************************************************************************************
 Module Interface Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* Blue */
        p_data->white_lane_color.subpixel.y  = 29;
        p_data->white_lane_color.subpixel.u  = 255;
        p_data->white_lane_color.subpixel.v  = 107;
        p_data->white_lane_color.subpixel.a  = 0;

        /* Red */
        p_data->yellow_lane_color.subpixel.y = 76;
        p_data->yellow_lane_color.subpixel.u = 84;
        p_data->yellow_lane_color.subpixel.v = 255;
        p_data->yellow_lane_color.subpixel.a = 0;

        /* Purple */
        p_data->extrapolated_lane_color.subpixel.y = 76;
        p_data->extrapolated_lane_color.subpixel.u = 212;
        p_data->extrapolated_lane_color.subpixel.v = 167;
        p_data->extrapolated_lane_color.subpixel.a = 0;

        /* White */
        p_data->text_color.subpixel.y = 0xFF;
        p_data->text_color.subpixel.u = 0x80;
        p_data->text_color.subpixel.v = 0x80;
        p_data->text_color.subpixel.a = 0;


        p_data->dashed_lane_segment_size = 20;
        p_data->thickness                = 5;
    }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Draw_Lane_Channels PQS Draw Lane Channels
 * @ingroup    PQS_Draw_Lane
 * # Channel Table:
 *
 *  Channel name | Channel Type | Object Type  | Description
 *  ------------ | ------------ | ------------ | ----------------------------------
 *  image_inout  | input/output | image        | @ref PQS_Draw_Lane_Channel_image
 *  lane_in      | input        | lane         | @ref PQS_Draw_Lane_Channel_lane
 *
 * @anchor PQS_Draw_Lane_Channel_image
 * ## PQS Draw Lane Channel image
 *             This is the input and output image to draw on. Note: this module WILL alter the contents of the image.
 *             No other module should be using the image while this module is executing. The format of the
 *             image must be in R_IMAGE_FORMAT_UYVA_INTERLEAVED.
 *
 * @anchor PQS_Draw_Lane_Channel_lane
 * ## PQS Draw Lane Channel lane
 *             This is the lane object containing the list of all the lanes detected to draw on the
 *             image.
 *
 * # Time Sync:
 *        To assure that the lanes and the image are synchronized, the timestamp of both objects will be
 *        compared. If the timestamps are different this module will return a failure.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_BROKER_InitChannelInfo(p_instance, DRAW_PRV_CHANNEL_IMAGE_INOUT, "image_inout", R_OBJECT_TYPE_IMAGE, R_BROKER_DATA_DIRECTION_INOUT);
    result |= R_BROKER_InitChannelInfo(p_instance, DRAW_PRV_CHANNEL_LANE_IN,     "lane_in",     R_OBJECT_TYPE_LANE,  R_BROKER_DATA_DIRECTION_IN);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    r_result_t result                  = R_RESULT_SUCCESS;
    st_priv_t * p_data                 = R_MODULE_GetPrivDataPtr(p_instance);

    st_object_t * p_object_image_inout = R_BROKER_GetObjectPtr(p_instance, DRAW_PRV_CHANNEL_IMAGE_INOUT);
    uint32_t * p_image                 = (uint32_t *)R_OBJECT_GetImagePlaneCPU(p_object_image_inout);
    uint32_t width                     = R_OBJECT_GetImageWidth(p_object_image_inout);
    uint32_t height                    = R_OBJECT_GetImageHeight(p_object_image_inout);

    st_object_t * p_object_lane_in     = R_BROKER_GetObjectPtr(p_instance, DRAW_PRV_CHANNEL_LANE_IN);
    int32_t max_objects                = R_OBJECT_GetLaneCount(p_object_lane_in);
    uint64_t image_timestamp           = 0;
    uint64_t lane_timestamp            = 0;

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    if ((R_OBJECT_CheckObjectType(p_object_image_inout, R_OBJECT_TYPE_IMAGE) != R_RESULT_SUCCESS) ||
        (R_OBJECT_CheckObjectType(p_object_lane_in, R_OBJECT_TYPE_LANE)      != R_RESULT_SUCCESS))
    {
        R_PRINT_ERROR("Invalid object passed");
        result = R_RESULT_FAILED;
    }

    /* Check timestamps */
    if (R_RESULT_SUCCESS == result)
    {
        if ((R_OBJECT_GetTimeStamp(p_object_image_inout, &image_timestamp) == R_RESULT_SUCCESS) &&
            (R_OBJECT_GetTimeStamp(p_object_lane_in,     &lane_timestamp)  == R_RESULT_SUCCESS))
        {
            if (lane_timestamp != image_timestamp)
            {
                R_PRINT_ERROR("Timestamps are not synced");
                result = R_RESULT_FAILED;
            }
        }
        else
        {
            R_PRINT_ERROR("Cannot get timestamps");
            result = R_RESULT_FAILED;
        }
    }


    /* Check image */
    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == p_image)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image data pointer");
        }

        if (R_OBJECT_GetImageFormat(p_object_image_inout) != R_IMAGE_FORMAT_UYVA_INTERLEAVED)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image format.  Expected format %d, got %d", R_IMAGE_FORMAT_UYVA_INTERLEAVED, R_OBJECT_GetImageFormat(p_object_image_inout));
        }

        if (0 == width)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image width");
        }

        if (0 == height)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image height");
        }
    }

    /* Draw lanes */
    if (R_RESULT_SUCCESS == result)
    {
        int32_t i;

        for(i = 0; max_objects > i; i++)
        {
            st_lane_t * p_lane = R_OBJECT_GetLane(p_object_lane_in, i);

            if (NULL != p_lane)
            {
                draw_lane(p_data, p_lane, p_image, width, height);
            }
        }
    }

    return result;
}
