/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_draw_freespace.c
 * @brief         Source file for drawing of freespace over image. See @ref PQS_Draw_Freespace for more details
 *********************************************************************************************************************/
/*****************************************************************************************************************//**
 * @page       PQS_Draw_Freespace_Algorithm  PQS Draw Freespace Algorithm
 * @ingroup    PQS_Draw_Freespace
 * # Algorithm Description:
 *             This module takes a Freespace object and draws the freespace over an input/output image. Freespace is
 *             drawn in green by shading every other pixel's U and V value. This freespace is then outlined by a solid
 *             green border.
 * # Block Diagram:
 *             @image html draw_freespace.png
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include "r_common.h"
#include "r_common_text.h"
#include "r_object_freespace.h"
#include "r_object_image.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define DRAW_PRV_CHANNEL_IMAGE_INOUT     (0) /*!< Broker Channel for in/out image */
#define DRAW_PRV_CHANNEL_FREESPACE_IN    (1) /*!< Broker Channel for freespace input */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Struct for uyva pixel */
typedef struct
{
    uint8_t u; /*!< u byte */
    uint8_t y; /*!< y byte */
    uint8_t v; /*!< v byte */
    uint8_t a; /*!< alpha byte */
} st_uyva_t;

/*! Union for uyva pixel, allows to access a uyva pixel per individual 8bit element or the whole 32bit pixel at once*/
typedef union
{
    st_uyva_t subpixel; /*!< 8bit subpixel struct */
    uint32_t  pixel;    /*!< Full 32 bit pixel */
} u_yuva_t;

/*! Private data structure for draw_freespace module */
typedef struct st_priv_tag
{
    u_yuva_t free_color; /*!< Color to use draw freespace */
    int32_t  border;     /*!< Border thickness */
} st_priv_t;

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_module_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_module_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_module_instance);

/*! Module interface definition for draw_freespace */
const st_module_interface_t g_module_interface_draw_freespace = {
    .module_name   = "draw_freespace",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = NULL,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/***********************************************************************************************************************
 Local Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   draw_freespace
 * @brief           Draws freespace on an image
 * @param [in]      p_data - pointer to the private data structure
 * @param [in]      p_free - Pointer to the freespace data to draw
 * @param [out]     p_image - pointer to image plane to draw on.
 * @param [in]      width - width of the image
 * @param [in]      height - height of the image
 *********************************************************************************************************************/
static void draw_freespace(st_priv_t * p_data, st_freespace_t * p_free, uint32_t * p_image, int32_t width, int32_t height)
{
    int32_t y_up     = R_COMMON_Clip((int32_t)(p_free->upper_y), 0, height);
    int32_t y_low    = R_COMMON_Clip((int32_t)(p_free->lower_y), 0, height);
    int32_t y_offset = 0;
    int32_t x        = 0;
    int32_t y        = 0;
    int32_t x_left   = 0;
    int32_t x_right  = 0;
    float y_f        = 0.0;
    float x_left_f   = 0.0;
    float x_right_f  = 0.0;
    st_uyva_t * p_pix = NULL;

    /* Draw upper solid line  */
    for (y = y_up; y < (y_up + p_data->border); y++)
    {
        /* Calculate y offset */
        y_offset = y * width;

        y_f = (float)(y);
        x_left_f  = (y_f * ((p_free->a_left * y_f) + p_free->b_left)) + p_free->c_left;
        x_right_f = (y_f * ((p_free->a_right * y_f) + p_free->b_right)) + p_free->c_right;

        x_left  = R_COMMON_Clip((int32_t)x_left_f,  0, width);
        x_right = R_COMMON_Clip((int32_t)x_right_f, 0, width);

        /* Draw solid line */
        for (x = x_left; x < x_right; x++)
        {
            p_image[y_offset + x] = p_data->free_color.pixel;
        }
    }

    /* Draw freespace  */
    for (y = (y_up + p_data->border); y < y_low; y++)
    {
        /* Calculate y offset */
        y_offset = y * width;

        y_f = (float)(y);
        x_left_f  = (y_f * ((p_free->a_left * y_f) + p_free->b_left)) + p_free->c_left;
        x_right_f = (y_f * ((p_free->a_right * y_f) + p_free->b_right)) + p_free->c_right;

        x_left  = R_COMMON_Clip((int32_t)x_left_f,  0, width);
        x_right = R_COMMON_Clip((int32_t)x_right_f, 0, width);

        /* Draw left border, solid line */
        for (x = x_left; x < (x_left + p_data->border); x++)
        {
            p_image[y_offset + x] = p_data->free_color.pixel;
        }

        /* Draw freespace between border, only shade in U and V for every other pixel */
        for (x = (x_left + p_data->border); x < (x_right - p_data->border); x += 2)

        {
            p_pix = (st_uyva_t *)&p_image[y_offset + x];
            p_pix->u = p_data->free_color.subpixel.u;
            p_pix->v = p_data->free_color.subpixel.v;
        }

        /* Draw right border */
        for (x = (x_right - p_data->border); x < x_right; x++)
        {
            p_image[y_offset + x] = p_data->free_color.pixel;
        }
    }
}

/***********************************************************************************************************************
 Module Interface Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* Green */
        p_data->free_color.subpixel.y = 0x80;
        p_data->free_color.subpixel.u = 0x2C;
        p_data->free_color.subpixel.v = 0x16;
        p_data->free_color.subpixel.a = 0;

        /* Freespace border in pixels*/
        p_data->border = 8;
    }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Draw_Freespace_Channels PQS Draw Freespace Channels
 * @ingroup    PQS_Draw_Freespace
 * # Channel Table:
 *
 *  Channel name | Channel Type | Object Type  | Description
 *  ------------ | ------------ | ------------ | ----------------------------------
 *  image_inout  | input/output | image        | @ref PQS_Draw_Freespace_Channel_image
 *  freespace    | input        | Freespace    | @ref PQS_Draw_Freespace_Channel_freespace
 *
 * @anchor PQS_Draw_Freespace_Channel_image
 * ## PQS Draw Freespace Channel image
 *             This is the input and output image to draw on. Note: this module WILL alter the contents of the image.
 *             No other module should be using the image while this module is executing. The format of the
 *             image must be in R_IMAGE_FORMAT_UYVA_INTERLEAVED.
 *
 * @anchor PQS_Draw_Freespace_Channel_freespace
 * ## PQS Draw Freespace Channel Freespace
 *             This is the freespace object containing the list of all the freespaces detected to draw on the
 *             image.
 *
 * # Time Sync:
 *        To assure that the freespace and the image are synchronized, the timestamp of both objects will be
 *        compared. If the timestamps are different this module will return a failure.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_BROKER_InitChannelInfo(p_instance, DRAW_PRV_CHANNEL_IMAGE_INOUT,  "image_inout", R_OBJECT_TYPE_IMAGE,     R_BROKER_DATA_DIRECTION_INOUT);
    result |= R_BROKER_InitChannelInfo(p_instance, DRAW_PRV_CHANNEL_FREESPACE_IN, "freespace",   R_OBJECT_TYPE_FREESPACE, R_BROKER_DATA_DIRECTION_IN);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    r_result_t result                  = R_RESULT_SUCCESS;
    st_priv_t * p_data                 = R_MODULE_GetPrivDataPtr(p_instance);

    st_object_t * p_object_image_inout = R_BROKER_GetObjectPtr(p_instance, DRAW_PRV_CHANNEL_IMAGE_INOUT);
    uint32_t * p_image                 = (uint32_t *)R_OBJECT_GetImagePlaneCPU(p_object_image_inout);
    uint32_t width                     = R_OBJECT_GetImageWidth(p_object_image_inout);
    uint32_t height                    = R_OBJECT_GetImageHeight(p_object_image_inout);

    st_object_t * p_object_free_in     = R_BROKER_GetObjectPtr(p_instance, DRAW_PRV_CHANNEL_FREESPACE_IN);
    int32_t max_objects                = R_OBJECT_GetFreespaceCount(p_object_free_in);
    uint64_t image_timestamp           = 0;
    uint64_t free_timestamp            = 0;

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    if ((R_OBJECT_CheckObjectType(p_object_image_inout, R_OBJECT_TYPE_IMAGE)     != R_RESULT_SUCCESS) ||
        (R_OBJECT_CheckObjectType(p_object_free_in,     R_OBJECT_TYPE_FREESPACE) != R_RESULT_SUCCESS))
    {
        R_PRINT_ERROR("Invalid object passed");
        result = R_RESULT_FAILED;
    }

    /* Check timestamps */
    if (R_RESULT_SUCCESS == result)
    {
        if ((R_OBJECT_GetTimeStamp(p_object_image_inout, &image_timestamp) == R_RESULT_SUCCESS) &&
            (R_OBJECT_GetTimeStamp(p_object_free_in,     &free_timestamp)  == R_RESULT_SUCCESS))
        {
            if (free_timestamp != image_timestamp)
            {
                R_PRINT_ERROR("Timestamps are not synced");
                result = R_RESULT_FAILED;
            }
        }
        else
        {
            R_PRINT_ERROR("Cannot get timestamps");
            result = R_RESULT_FAILED;
        }
    }

    /* Check the image */
    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == p_image)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image data pointer");
        }

        if (R_OBJECT_GetImageFormat(p_object_image_inout) != R_IMAGE_FORMAT_UYVA_INTERLEAVED)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image format.  Expected format %d, got %d", R_IMAGE_FORMAT_UYVA_INTERLEAVED, R_OBJECT_GetImageFormat(p_object_image_inout));
        }

        if (0 == width)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image width");
        }

        if (0 == height)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image height");
        }
    }

    /* Draw freespace */
    if (R_RESULT_SUCCESS == result)
    {
        int32_t i;

        for (i = 0; max_objects > i; i++)
        {
            st_freespace_t * p_free = R_OBJECT_GetFreespace(p_object_free_in, i);

            if (NULL != p_free)
            {
                draw_freespace(p_data, p_free, p_image, width, height);
            }
        }
    }

    return result;
}
