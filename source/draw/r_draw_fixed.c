/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_draw_fixed.c
 * @brief         Source file for drawing of fixed data over images. See @ref PQS_Draw_Fixed for more details
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * @page       PQS_Draw_Fixed_Algorithm  PQS Draw Fixed Algorithm
 * @ingroup    PQS_Draw_Fixed
 * # Algorithm Description:
 *             This module draws fixed data over an image. Fixed messages can be written as well as the Renesas logo.
 *             Color space supported is UYVA and BGRA.
 * # Block Diagram:
 *             @image html draw_fixed.png
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/**********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 *********************************************************************************************************************/

#include "r_common.h"
#include "r_common_text.h"
#include "r_object_image.h"

/**********************************************************************************************************************
 Macro definitions
 *********************************************************************************************************************/

#define DRAW_PRV_CHANNEL_IMAGE             (0)             /*!< Channel for input */

#define DRAW_PRV_MCOD_TOP_LEFT_MESSAGE              "LEFT CAMERA"   /*!< Top Left message for MCOD */
#define DRAW_PRV_MCOD_TOP_RIGHT_MESSAGE             "RIGHT CAMERA"  /*!< Top Right message for MCOD */
#define DRAW_PRV_MCOD_BOTTOM_LEFT_MESSAGE           "REAR CAMERA"   /*!< Bottom Left message for MCOD */
#define DRAW_PRV_MCOD_BOTTOM_RIGHT_MESSAGE          "FRONT CAMERA"  /*!< Bottom Right message for MCOD */

#define DRAW_PRV_MESSAGE_BOARDER                    (2)            /*!< Border around the messages in pixels */

#define DRAW_PRV_LOGO_HEIGHT                        (34)           /*!< Height of the Renesas logo */
#define DRAW_PRV_LOGO_WIDTH                         (208)          /*!< Width of the Renesas logo */
#define DRAW_PRV_LOGO_BORDER                        (8)            /*!< Number of pixels to offset the logo from the bottom right corner */

#define DRAW_PRV_DEFAULT_DRAW_MCOD_4CAM_MESSAGE     (false)        /*!< Bool to draw the MCOD 4 camera messages */
#define DRAW_PRV_DEFAULT_DRAW_MCOD_3CAM_MESSAGE     (false)        /*!< Bool to draw the MCOD 3 camera messages */
#define DRAW_PRV_DEFAULT_DRAW_LOGO                  (true)         /*!< Bool to draw Renesas logo */

#define DRAW_PRV_DEFAULT_FORMAT                     (R_IMAGE_FORMAT_UYVA_INTERLEAVED) /*!< Format of the connected display. Can be overridden by config */

/**********************************************************************************************************************
 Local Typedef definitions
 *********************************************************************************************************************/

/*! Struct for uyva pixel */
typedef struct
{
    uint8_t first;  /*!< u byte */
    uint8_t second; /*!< y byte */
    uint8_t third;  /*!< v byte */
    uint8_t fourth; /*!< alpha byte */
} st_pixel_32bit_t;

/*! Union for uyva pixel, allows to access a uyva pixel per individual 8bit element or the whole 32bit pixel at once*/
typedef union
{
    st_pixel_32bit_t subpixel; /*!< 8bit subpixel struct */
    uint32_t  pixel;           /*!< Full 32 bit pixel */
} u_pixel_32bit_t;

/*! Pixel positions of a boxrelative to an image */
typedef struct
{
    int32_t top;    /*!< top pixel of box */
    int32_t left;   /*!< left pixel of box */
    int32_t bottom; /*!< bottom pixel of box */
    int32_t right;  /*!< right pixel of box */
} st_pix_position_t;

/*! Configuration structure for draw_fixed module */
typedef struct {
    bool              draw_mcod_4cam_msg;     /*!< True if MCOD 4 camera messages should be drawn */
    bool              draw_mcod_3cam_msg;     /*!< True if MCOD 3 camera messages should be drawn */
    bool              draw_logo;              /*!< True if logo should be drawn */
    e_image_format_t  format;                 /*!< Output format */
} st_draw_cfg_t;

/*! Private data structure for draw_fixed module */
typedef struct st_priv_tag {
    u_pixel_32bit_t      text_color; /*!< Color to draw text with */
    u_pixel_32bit_t      box_color;  /*!< Color to draw box with */
    u_pixel_32bit_t      logo_color; /*!< Color to draw Renesas logo with */
    st_draw_cfg_t config;     /*!< Configuration structure */
} st_priv_t;

/**********************************************************************************************************************
 Exported global variables
 *********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_module_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_instance);
static r_result_t R_ProcessData(       st_module_instance_t * const p_instance);

/*! Module interface definition for draw_fixed */
const st_module_interface_t g_module_interface_draw_fixed = {
    .module_name   = "draw_fixed",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = NULL,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/**********************************************************************************************************************
 Private global variables
 *********************************************************************************************************************/

/*! Byte array of the Renesas logo created with <https://manytools.org/hacker-tools/image-to-byte-array>
 * Edited by hand to get nice columns :) */
static uint8_t s_renesas_logo[] = {
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xff, 0x80,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xff, 0xf8, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x07, 0xf8, 0x3f, 0xfe, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1e,
0x00, 0x03, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x01, 0xff, 0x80,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0xff, 0x80, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0x80, 0x7f, 0xff, 0xf8, 0x7f, 0x80, 0x01, 0xe0, 0x0f,
0xff, 0xff, 0x01, 0xff, 0xff, 0xf0, 0x00, 0x3f, 0xc0, 0x00, 0x7f, 0xff, 0xf8, 0x00, 0x00,
0x00, 0xff, 0x81, 0xff, 0xff, 0xf0, 0x7f, 0xe0, 0x01, 0xe0, 0x3f, 0xff, 0xfe, 0x07, 0xff,
0xff, 0xe0, 0x00, 0x7f, 0xe0, 0x03, 0xff, 0xff, 0xf0, 0x00, 0x00, 0x00, 0xff, 0x83, 0xff,
0xff, 0xe0, 0x7f, 0xf0, 0x01, 0xe0, 0x7f, 0xff, 0xfc, 0x0f, 0xff, 0xff, 0xc0, 0x00, 0xff,
0xf0, 0x07, 0xff, 0xff, 0xe0, 0x00, 0x00, 0x00, 0xff, 0x07, 0xf0, 0x00, 0x00, 0x7f, 0xf8,
0x01, 0xe0, 0xff, 0x00, 0x00, 0x1f, 0xe0, 0x00, 0x00, 0x01, 0xff, 0xf0, 0x07, 0xf8, 0x00,
0x00, 0x00, 0x00, 0x01, 0xff, 0x07, 0xf0, 0x00, 0x00, 0x7f, 0xf8, 0x01, 0xe0, 0xfe, 0x00,
0x00, 0x1f, 0xe0, 0x00, 0x00, 0x01, 0xff, 0xf8, 0x07, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x07,
0xfc, 0x0f, 0xe0, 0x00, 0x00, 0x7b, 0xfc, 0x01, 0xe0, 0xfe, 0x00, 0x00, 0x1f, 0xf0, 0x00,
0x00, 0x03, 0xf3, 0xf8, 0x07, 0xfc, 0x00, 0x00, 0x00, 0x00, 0x0f, 0xf8, 0x0f, 0xe0, 0x00,
0x00, 0x79, 0xfe, 0x01, 0xe0, 0xfe, 0x00, 0x00, 0x0f, 0xfc, 0x00, 0x00, 0x07, 0xf1, 0xfc,
0x07, 0xff, 0x00, 0x00, 0x00, 0x00, 0x3f, 0xe0, 0x0f, 0xe0, 0x00, 0x00, 0x78, 0xff, 0x01,
0xe0, 0xfe, 0x00, 0x00, 0x0f, 0xff, 0xc0, 0x00, 0x07, 0xe1, 0xfe, 0x03, 0xff, 0xe0, 0x00,
0x00, 0x00, 0x7f, 0xc0, 0x0f, 0xff, 0xff, 0xe0, 0x78, 0x7f, 0x81, 0xe0, 0xff, 0xff, 0xfe,
0x03, 0xff, 0xf8, 0x00, 0x0f, 0xe0, 0xfe, 0x01, 0xff, 0xfc, 0x00, 0x00, 0x00, 0x7f, 0x80,
0x0f, 0xff, 0xff, 0xe0, 0x78, 0x3f, 0xc1, 0xe0, 0xff, 0xff, 0xfc, 0x01, 0xff, 0xff, 0x00,
0x0f, 0xc0, 0xff, 0x00, 0x7f, 0xff, 0x80, 0x1f, 0xf0, 0x3f, 0x80, 0x0f, 0xff, 0xff, 0xc0,
0x78, 0x3f, 0xe1, 0xe0, 0xff, 0xff, 0xf8, 0x00, 0x3f, 0xff, 0xc0, 0x1f, 0x80, 0x7f, 0x80,
0x1f, 0xff, 0xe0, 0x1f, 0xf0, 0x3f, 0xc0, 0x0f, 0xe0, 0x00, 0x00, 0x78, 0x1f, 0xf1, 0xe0,
0xfe, 0x00, 0x00, 0x00, 0x07, 0xff, 0xe0, 0x3f, 0x80, 0x3f, 0x80, 0x03, 0xff, 0xf8, 0x1f,
0xf0, 0x1f, 0xe0, 0x0f, 0xe0, 0x00, 0x00, 0x78, 0x0f, 0xf1, 0xe0, 0xfe, 0x00, 0x00, 0x00,
0x00, 0xff, 0xf0, 0x3f, 0x00, 0x3f, 0xc0, 0x00, 0x3f, 0xfc, 0x1f, 0xf0, 0x0f, 0xf0, 0x0f,
0xe0, 0x00, 0x00, 0x78, 0x07, 0xf9, 0xe0, 0xfe, 0x00, 0x00, 0x00, 0x00, 0x1f, 0xf8, 0x7f,
0x00, 0x1f, 0xc0, 0x00, 0x07, 0xfc, 0x1f, 0xf0, 0x07, 0xf8, 0x0f, 0xe0, 0x00, 0x00, 0x78,
0x03, 0xfd, 0xe0, 0xfe, 0x00, 0x00, 0x00, 0x00, 0x07, 0xf8, 0xfe, 0x00, 0x1f, 0xe0, 0x00,
0x03, 0xfe, 0x1f, 0xf0, 0x03, 0xfc, 0x07, 0xf0, 0x00, 0x00, 0x78, 0x01, 0xff, 0xe0, 0xfe,
0x00, 0x00, 0x00, 0x00, 0x07, 0xf8, 0xfe, 0x00, 0x0f, 0xf0, 0x00, 0x01, 0xfe, 0x1f, 0xf0,
0x00, 0xfe, 0x07, 0xf8, 0x00, 0x00, 0x78, 0x00, 0xff, 0xe0, 0x7f, 0x00, 0x00, 0x00, 0x00,
0x07, 0xf9, 0xfc, 0x00, 0x07, 0xf0, 0x00, 0x03, 0xfc, 0x1f, 0xf0, 0x00, 0x7f, 0x03, 0xff,
0xff, 0xf8, 0x78, 0x00, 0x7f, 0xe0, 0x7f, 0xff, 0xff, 0x0f, 0xff, 0xff, 0xf1, 0xf8, 0x0f,
0xff, 0xff, 0xff, 0xff, 0xfc, 0x1f, 0xf0, 0x00, 0x3f, 0x81, 0xff, 0xff, 0xf0, 0x78, 0x00,
0x3f, 0xe0, 0x1f, 0xff, 0xff, 0x1f, 0xff, 0xff, 0xe3, 0xf8, 0x1f, 0xff, 0xff, 0xff, 0xff,
0xf0, 0x00, 0x00, 0x00, 0x0f, 0xc0, 0x3f, 0xff, 0xf0, 0x78, 0x00, 0x1f, 0xe0, 0x03, 0xff,
0xfe, 0x1f, 0xff, 0xfe, 0x07, 0xf0, 0x1f, 0xff, 0xff, 0xff, 0xff, 0x80, 0x00, 0x00, 0x00,
0x03, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xf0, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x03, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

/**********************************************************************************************************************
 Local Functions
 *********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   calc_msg_position_top_left
 * @brief           Populates the position structure based off of the top left corner and the supplied message
 * @param [out]     p_position - pointer to position structure to fill out
 * @param [in]      top - top of the message position
 * @param [in]      left - left hand side of the message position
 * @param [in]      p_message - String message
 *********************************************************************************************************************/
static void calc_msg_position_top_left(st_pix_position_t * p_position, uint32_t top, uint32_t left, const char * p_message)
{
    size_t message_length = R_COMMON_STRING_Length(p_message) - 1;  //Subtract 1 because we do not care about the null termination
    p_position->bottom = top + (R_COMMON_LETTER_HEIGHT + (2 * DRAW_PRV_MESSAGE_BOARDER));
    p_position->right  = left + (message_length * R_COMMON_LETTER_WIDTH) + (2 * DRAW_PRV_MESSAGE_BOARDER);
    p_position->top    = top;
    p_position->left   = left;
}

/******************************************************************************************************************//**
 * Function Name:   calc_msg_position_top_right
 * @brief           Populates the position structure based off of the top left corner and the supplied message
 * @param [out]     p_position - pointer to position structure to fill out
 * @param [in]      top - top of the message position
 * @param [in]      right - right hand side of the message position
 * @param [in]      p_message - String message
 *********************************************************************************************************************/
static void calc_msg_position_top_right(st_pix_position_t * p_position, uint32_t top, uint32_t right, const char * p_message)
{
    size_t message_length = R_COMMON_STRING_Length(p_message) - 1;  //Subtract 1 because we do not care about the null termination
    p_position->bottom = top + (R_COMMON_LETTER_HEIGHT + (2 * DRAW_PRV_MESSAGE_BOARDER));
    p_position->left   = right - ((message_length * R_COMMON_LETTER_WIDTH) + (2 * DRAW_PRV_MESSAGE_BOARDER));
    p_position->top    = top;
    p_position->right  = right;
}

/******************************************************************************************************************//**
 * Function Name:   draw_letter
 * @brief           Draws a letter to the supplied image.
 * @param [in]      letter - character to draw
 * @param [in]      color - color to use to draw character
 * @param [out]     p_image - pointer to the part of the image plane to start drawing on. This can be any part of
 *                  the image.
 * @param [in]      image_width - width of the image in pixels
 *********************************************************************************************************************/
static void draw_letter(const char letter, u_pixel_32bit_t color, uint32_t * p_image, uint32_t image_width)
{
    const a_letter_t * p_letter_array = R_COMMON_TEXT_GetLetterArray(letter);
    uint32_t i = 0;
    uint32_t j = 0;

    for (i = 0; i < R_COMMON_LETTER_HEIGHT; i++)
    {
        for (j = 0; j < R_COMMON_LETTER_WIDTH; j++)
        {
            if (R_COMMON_TEXT_GetLetterValue(p_letter_array, i, j) == true)
            {
                p_image[j + (i * image_width)] = color.pixel;
            }
        }
    }
}

/******************************************************************************************************************//**
 * Function Name:   draw_message
 * @brief           Draws a message over the supplied image
 * @param [in]      p_data - pointer to the private data structure
 * @param [in]      p_message - String message to draw
 * @param [in]      p_position - Pointer to position struct to draw the message to
 * @param [out]     p_image - pointer to image plane to draw on.
 * @param [in]      image_width - width of the image
 * @param [in]      image_height - height of the image
 *********************************************************************************************************************/
static void draw_message(const st_priv_t * p_data, const char * p_message, st_pix_position_t * p_position, uint32_t * p_image, uint32_t image_width, uint32_t image_height)
{
    size_t message_length = R_COMMON_STRING_Length(p_message) - 1;  //Subtract 1 because we do not care about the null termination
    uint32_t y = 0;
    uint32_t x = 0;
    uint32_t y_off = 0;
    uint32_t letter_index = 0;

    /* Draw box for message */
    for (y = p_position->top; y < p_position->bottom; y++)
    {
        y_off = y * image_width;

        for (x = p_position->left; x < p_position->right; x++)
        {
            p_image[y_off + x] = p_data->box_color.pixel;
        }
    }

    /* Write message */
    for (letter_index = 0; letter_index < message_length; letter_index++)
    {
        x = p_position->left + DRAW_PRV_MESSAGE_BOARDER + (letter_index * R_COMMON_LETTER_WIDTH);
        y = p_position->top + DRAW_PRV_MESSAGE_BOARDER;

        draw_letter(p_message[letter_index], p_data->text_color, &p_image[x + (y * image_width)], image_width);
    }
}

/******************************************************************************************************************//**
 * Function Name:   get_logo_value
 * @brief           Determines if a pixel at the given x and y position should be shaded in order to draw the logo
 * @param [in]      x_position - x position to check
 * @param [in]      y_position - y position to check
 * @retval          true if the pixel should be shaded, false if the pixel should be left alone
 *********************************************************************************************************************/
static bool get_logo_value(uint32_t x_position, uint32_t y_position)
{
    bool result = false;
    uint32_t byte_index = (y_position * (DRAW_PRV_LOGO_WIDTH/8)) + (x_position/8);
    uint32_t bit_index = x_position % 8;

    if (1 == ((s_renesas_logo[byte_index] >> (7 - bit_index)) & 1))
    {
        result = true;
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   draw_logo
 * @brief           Draws the Renesas logo at the given position
 * @param [in]      p_data - pointer to the private data structure
 * @param [in]      p_position - Pointer to position struct to draw the message to
 * @param [out]     p_image - pointer to image plane to draw on.
 * @param [in]      image_width - width of the image
 * @param [in]      image_height - height of the image
 *********************************************************************************************************************/
static void draw_logo(const st_priv_t * p_data, st_pix_position_t * p_position, uint32_t * p_image, uint32_t image_width, uint32_t image_height)
{
    uint32_t i = 0;
    uint32_t j = 0;

    uint32_t * p_image_logo = &p_image[p_position->left + (p_position->top * image_width)];

    for (i = 0; i < DRAW_PRV_LOGO_HEIGHT; i++)
    {
        for (j = 0; j < DRAW_PRV_LOGO_WIDTH; j++)
        {
            if (get_logo_value(j, i) == true)
            {
                p_image_logo[j + (i * image_width)] = p_data->logo_color.pixel;;
            }
        }
    }
}

/******************************************************************************************************************//**
 * @page    PQS_Draw_Fixed_Config  PQS Draw Fixed Config
 * @ingroup PQS_Draw_Fixed
 * # Configuration Table:
 *
 *   Config Name            | Config Type      | Default                         | Range                                                               | Description
 *   ---------------------- | -----------------| ------------------------------- | ------------------------------------------------------------------- | ------------------
 *   draw_mcod_4cam_msg     | bool             | false                           | true or false                                                       | True if the messages for MCOD 4 camera should be draw
 *   draw_mcod_3cam_msg     | bool             | false                           | true or false                                                       | True if the messages for MCOD 3 camera should be draw
 *   draw_logo              | bool             | true                            | true or false                                                       | True if the Renesas logo should be drawn
 *   format                 | e_image_format_t | R_IMAGE_FORMAT_UYVA_INTERLEAVED | R_IMAGE_FORMAT_UYVY_INTERLEAVED or R_IMAGE_FORMAT_RGB32_INTERLEAVED | Format of the input image.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_config
 * @brief           Initializes the configuration structure with either the default values or with values from a config
 *                  file if available.
 * @param [in,out]  p_config - pointer to the configuration structure
 * @param [in]      p_instance_name - String of the module instance name
 * @param [in]      p_module_name - String of the module name
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_config(st_draw_cfg_t * p_config, char const * const p_instance_name, char const * const p_module_name)
{
    r_result_t result = R_RESULT_SUCCESS;
    const char * format_names[] = E_IMAGE_FORMAT_AS_STRING;

    /* Init config */
    p_config->draw_mcod_4cam_msg = R_COMMON_CONFIG_GetBool(p_instance_name,  "draw_mcod_4cam_msg", DRAW_PRV_DEFAULT_DRAW_MCOD_4CAM_MESSAGE);
    p_config->draw_mcod_3cam_msg = R_COMMON_CONFIG_GetBool(p_instance_name,  "draw_mcod_3cam_msg", DRAW_PRV_DEFAULT_DRAW_MCOD_3CAM_MESSAGE);
    p_config->draw_logo          = R_COMMON_CONFIG_GetBool(p_instance_name,  "draw_logo",          DRAW_PRV_DEFAULT_DRAW_LOGO);
    p_config->format             = R_COMMON_CONFIG_GET_ENUM(p_instance_name, "format",             DRAW_PRV_DEFAULT_FORMAT, format_names, (sizeof(format_names))/(sizeof(format_names[0])), e_image_format_t);
    return result;
}

/***********************************************************************************************************************
 Module Interface Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    char const * const p_instance_name = R_MODULE_GetInstanceName(p_instance);
    char const * const p_module_name   = R_MODULE_GetModuleName(p_instance);
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Init configuration */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_config(&p_data->config, p_instance_name, p_module_name) != R_RESULT_SUCCESS)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Configuration init has failed");
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        switch (p_data->config.format)
        {
            case R_IMAGE_FORMAT_RGB32_INTERLEAVED:
            {
                /* Black */
                p_data->box_color.subpixel.first  = 0;    // blue
                p_data->box_color.subpixel.second = 0x80; // green
                p_data->box_color.subpixel.third  = 0x80; // red
                p_data->box_color.subpixel.fourth = 0;    // alpha

                /* Light grey */
                p_data->text_color.subpixel.first  = 0xA0; // blue
                p_data->text_color.subpixel.second = 0x80; // green
                p_data->text_color.subpixel.third  = 0x80; // red
                p_data->text_color.subpixel.fourth = 0;    // alpha

                /* Renesas Blue */
                p_data->logo_color.subpixel.first  = 152; // blue
                p_data->logo_color.subpixel.second = 64;  // green
                p_data->logo_color.subpixel.third  = 46; // red
                p_data->logo_color.subpixel.fourth = 0;   // alpha
                break;
            }

            case R_IMAGE_FORMAT_UYVA_INTERLEAVED:
            {
                /* Black */
                p_data->box_color.subpixel.first  = 0x80; // u
                p_data->box_color.subpixel.second = 0x00; // y
                p_data->box_color.subpixel.third  = 0x80; // v
                p_data->box_color.subpixel.fourth = 0;    // alpha

                /* Light grey */
                p_data->text_color.subpixel.first  = 0x80; // u
                p_data->text_color.subpixel.second = 0xA0; // y
                p_data->text_color.subpixel.third  = 0x80; // v
                p_data->text_color.subpixel.fourth = 0;    // alpha

                /* Renesas Blue */
                p_data->logo_color.subpixel.first  = 175; // u
                p_data->logo_color.subpixel.second = 69;  // y
                p_data->logo_color.subpixel.third  = 112; // v
                p_data->logo_color.subpixel.fourth = 0;   // alpha
                break;
            }

            /* Invalid format*/
            default:
            {
                R_PRINT_ERROR("Fell through switch. Format not supported.");
                break;
            }
        }

    }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Draw_Fixed_Channels PQS Draw Fixed Channels
 * @ingroup    PQS_Draw_Fixed
 * # Channel Table:
 *
 *  Channel name | Channel Type | Object Type  | Description
 *  ------------ | ------------ | ------------ | ----------------------------------
 *  image_inout  | input        | image        | @ref PQS_Draw_Fixed_Channel_image
 *
 * @anchor PQS_Draw_Fixed_Channel_image
 * ## PQS Draw Fixed Channel Image
 *             Input output image to draw fixed drawing on
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_BROKER_InitChannelInfo(p_instance, DRAW_PRV_CHANNEL_IMAGE, "image_inout", R_OBJECT_TYPE_IMAGE, R_BROKER_DATA_DIRECTION_INOUT);

    return result;
}


/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    r_result_t result      = R_RESULT_SUCCESS;
    st_priv_t * p_data     = R_MODULE_GetPrivDataPtr(p_instance);
    st_object_t * p_object = R_BROKER_GetObjectPtr(p_instance, DRAW_PRV_CHANNEL_IMAGE);
    uint32_t * p_plane     = (uint32_t *)R_OBJECT_GetImagePlaneCPU(p_object);
    uint32_t image_width   = R_OBJECT_GetImageWidth(p_object);
    uint32_t image_height  = R_OBJECT_GetImageHeight(p_object);
    st_pix_position_t  mcod_top_left_position     = {0};
    st_pix_position_t  mcod_top_right_position    = {0};
    st_pix_position_t  mcod_bottom_left_position  = {0};
    st_pix_position_t  mcod_bottom_right_position = {0};
    st_pix_position_t  logo_position              = {0};

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Check objects */
    if (R_OBJECT_CheckObjectType(p_object,  R_OBJECT_TYPE_IMAGE) != R_RESULT_SUCCESS)
    {
        R_PRINT_ERROR("Invalid objects");
        result = R_RESULT_FAILED;
    }

    /* Check image */
    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == p_plane)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image data pointer");
        }

        if (R_OBJECT_GetImageFormat(p_object) != p_data->config.format)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image format. Expected format %d, got %d", p_data->config.format, R_OBJECT_GetImageFormat(p_object));
        }
    }

    /* Draw */
    if (R_RESULT_SUCCESS == result)
    {
        /* Draw messages */
        if (true == p_data->config.draw_mcod_4cam_msg)
        {
            calc_msg_position_top_left( &mcod_top_left_position,     0,              0,           DRAW_PRV_MCOD_TOP_LEFT_MESSAGE);
            calc_msg_position_top_right(&mcod_top_right_position,    0,              image_width, DRAW_PRV_MCOD_TOP_RIGHT_MESSAGE);
            calc_msg_position_top_left( &mcod_bottom_left_position,  image_height/2, 0,           DRAW_PRV_MCOD_BOTTOM_RIGHT_MESSAGE);
            calc_msg_position_top_right(&mcod_bottom_right_position, image_height/2, image_width, DRAW_PRV_MCOD_BOTTOM_LEFT_MESSAGE);

            /* Do processing here */
            draw_message(p_data, DRAW_PRV_MCOD_TOP_LEFT_MESSAGE,     &mcod_top_left_position,     p_plane, image_width, image_height);
            draw_message(p_data, DRAW_PRV_MCOD_TOP_RIGHT_MESSAGE,    &mcod_top_right_position,    p_plane, image_width, image_height);
            draw_message(p_data, DRAW_PRV_MCOD_BOTTOM_RIGHT_MESSAGE, &mcod_bottom_left_position,  p_plane, image_width, image_height);
            draw_message(p_data, DRAW_PRV_MCOD_BOTTOM_LEFT_MESSAGE,  &mcod_bottom_right_position, p_plane, image_width, image_height);
        }
        
        if (true == p_data->config.draw_mcod_3cam_msg)
        {
            calc_msg_position_top_left( &mcod_top_left_position,     0,              0,           DRAW_PRV_MCOD_BOTTOM_RIGHT_MESSAGE);
            calc_msg_position_top_left( &mcod_bottom_left_position,  image_height/2, 0,           DRAW_PRV_MCOD_TOP_LEFT_MESSAGE);
            calc_msg_position_top_right(&mcod_bottom_right_position, image_height/2, image_width, DRAW_PRV_MCOD_TOP_RIGHT_MESSAGE);

            /* Do processing here */
            draw_message(p_data, DRAW_PRV_MCOD_BOTTOM_RIGHT_MESSAGE, &mcod_top_left_position,     p_plane, image_width, image_height);
            draw_message(p_data, DRAW_PRV_MCOD_TOP_LEFT_MESSAGE,     &mcod_bottom_left_position,  p_plane, image_width, image_height);
            draw_message(p_data, DRAW_PRV_MCOD_TOP_RIGHT_MESSAGE,    &mcod_bottom_right_position, p_plane, image_width, image_height);
        }

        /* Draw logo */
        if (true == p_data->config.draw_logo)
        {
            logo_position.bottom = image_height - DRAW_PRV_LOGO_BORDER;
            logo_position.right  = image_width - DRAW_PRV_LOGO_BORDER;
            logo_position.top    = logo_position.bottom  - DRAW_PRV_LOGO_HEIGHT;
            logo_position.left   = logo_position.right - DRAW_PRV_LOGO_WIDTH;

            draw_logo(p_data, &logo_position, p_plane, image_width, image_height);
        }
    }

    return result;
}

