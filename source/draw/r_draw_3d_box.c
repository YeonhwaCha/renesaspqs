/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_draw_3d_box.c
 * @brief         Source file for drawing of 3d boxes over image. See @ref PQS_Draw_3dBox for more details
 *********************************************************************************************************************/
 /******************************************************************************************************************//**
 * @page       PQS_Draw_3dBox_Algorithm  PQS Draw 3d Box Algorithm
 * @ingroup    PQS_Draw_3dBox
 * # Algorithm Description:
 *             This module takes a 3d Box object and draws the 2d top down box esover an input/output image. Vehicles
 *             boxes are drawn in magentia  The input image and the box must both have the exact same timestamp
 *             to assure the boxes are drawn on the correct image. A scaling factor can be applied in order to draw
 *             boxes on a larger image than the feature map where they were detected.
 * # Block Diagram:
 *             @image html draw_3dbox.png
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#include "r_common.h"
#include "r_common_text.h"
#include "r_object_3d_box.h"
#include "r_object_image.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define DRAW_PRV_CHANNEL_IMAGE_INOUT  (0)  /*!< Data broker channel for input output image object */
#define DRAW_PRV_CHANNEL_BBOX_IN      (1)  /*!< Data broker channel for in bounding box object */

#define DRAW_PRV_WIDTH_SCALE_FACTOR   (2)  /*!< Scaling factor for showing bounding boxes on image.
                                                Scale factor 2 means images is twice as large as bounding box dimensions*/
#define DRAW_PRV_HEIGHT_SCALE_FACTOR  (2)  /*!< Scaling factor for showing bounding boxes on image.
                                                Scale factor 2 means images is twice as large as bounding box dimensions */

#define DRAW_PRV_LINE_THICKNESS       (2)  /*!< How thick to draw bounding box lines in pixels */

#define DRAW_PRV_CIRCLE_RADIUS        (7)  /*!< Circle radius in pixels */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Struct for image meta data */
typedef struct
{
    uint32_t * p_plane; /*!< Pointer to the image plane */
    uint32_t height;    /*!< Height of the image */
    uint32_t width;     /*!< Width of the image */
} st_image_data_t;

/*! Struct for bgra pixel */
typedef struct
{
    uint8_t b; /*!< blue byte */
    uint8_t g; /*!< green byte */
    uint8_t r; /*!< red byte */
    uint8_t a; /*!< alpha byte */
} st_bgra_t;

/*! Union for bgra pixel, allows to access a bgra pixel per individual 8bit element or the whole 32bit pixel at once*/
typedef union
{
    st_bgra_t subpixel; /*!< 8bit subpixel struct */
    uint32_t  pixel;    /*!< Full 32 bit pixel */
} u_bgra_t;

/*! Private data structure for draw_3d_box module */
typedef struct st_priv_tag {
    u_bgra_t vehicle_color;             /*!< Color for vehicle class */
    u_bgra_t car_color;                 /*!< Color for car class */
    u_bgra_t large_car_color;           /*!< Color for large car class */
    u_bgra_t long_vehicle_color;        /*!< Color for long vehicle class */
    u_bgra_t group_of_vehicles_color;   /*!< Color for group of vehicles class */
    u_bgra_t pedestrian_color;          /*!< Color for pedestrian class */
    u_bgra_t unknown_color;             /*!< Color for unknown class */
} st_priv_t;


/*! Boolean for how to draw a circle. */ /* TODO this could be a bit map to reduce space in memory */
static const uint8_t s_circle[2 * DRAW_PRV_CIRCLE_RADIUS][2 * DRAW_PRV_CIRCLE_RADIUS] = {
     {0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0},
     {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
     {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
     {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
     {0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0},
     {1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1},
     {1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1},
     {1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1},
     {1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1},
     {0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0},
     {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
     {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
     {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
     {0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0},
};

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_module_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_module_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_module_instance);

/*! Module interface definition for draw_3d_box */
const st_module_interface_t g_module_interface_draw_3d_box = {
    .module_name   = "draw_3d_box",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = NULL,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/***********************************************************************************************************************
 Local Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   get_color
 * @brief           Returns a pointer for the color corresponding to the given box_class
 * @param [in]      p_data - pointer to the private data structure
 * @param [in]      box_class - Class of the box to get the color for
 * @retval          Pointer to the color to use
 *********************************************************************************************************************/
static const u_bgra_t * get_color(const st_priv_t * p_data, e_3d_box_class_t box_class)
{
    const u_bgra_t * p_color;

    switch (box_class)
    {
        case R_3D_BOX_CLASS_VEHICLE:
        {
            p_color = &p_data->vehicle_color;
            break;
        }
        case R_3D_BOX_CLASS_CAR:
        {
            p_color = &p_data->car_color;
            break;
        }
        case R_3D_BOX_CLASS_LARGE_CAR:
        {
            p_color = &p_data->large_car_color;
            break;
        }
        case R_3D_BOX_CLASS_LONG_VEHICLE:
        {
            p_color = &p_data->long_vehicle_color;
            break;
        }
        case R_3D_BOX_CLASS_GROUP_OF_VEHICLES:
        {
            p_color = &p_data->group_of_vehicles_color;
            break;
        }
        case R_3D_BOX_CLASS_PEDESTRIAN:
        {
            p_color = &p_data->pedestrian_color;
            break;
        }

        default:
        {
            p_color = &p_data->unknown_color;
            break;
        }
    }

    return p_color;
}

/******************************************************************************************************************//**
 * Function Name:   check_obstacle
 * @brief           Checks if a given 3d box is valid
 * @param [in]      p_obstacle - pointer to the 3d box to check
 * @retval          R_RESULT_SUCCESS if box is valid, R_RESULT_FAILED otherwise
 *********************************************************************************************************************/
static r_result_t check_obstacle(const st_3d_box_t * p_obstacle)
{
    r_result_t return_code = R_RESULT_SUCCESS;

    if (R_3D_BOX_CLASS_PEDESTRIAN == p_obstacle->box_class)
    {
        /* Pedestrians are a single pixel all row and column values should be the same */
        if ((p_obstacle->rc_box.corner1.c != p_obstacle->rc_box.corner2.c) ||
            (p_obstacle->rc_box.corner1.c != p_obstacle->rc_box.corner3.c) ||
            (p_obstacle->rc_box.corner1.c != p_obstacle->rc_box.corner4.c) ||
            (p_obstacle->rc_box.corner1.r != p_obstacle->rc_box.corner2.r) ||
            (p_obstacle->rc_box.corner1.r != p_obstacle->rc_box.corner3.r) ||
            (p_obstacle->rc_box.corner1.r != p_obstacle->rc_box.corner4.r))
        {
            R_PRINT_ERROR("Bounding box for pedestrian is not correctly formed");
            R_PRINT("\t Corner 1: c = %u r = %u \n", p_obstacle->rc_box.corner1.c, p_obstacle->rc_box.corner1.r);
            R_PRINT("\t Corner 2: c = %u r = %u \n", p_obstacle->rc_box.corner2.c, p_obstacle->rc_box.corner2.r);
            R_PRINT("\t Corner 3: c = %u r = %u \n", p_obstacle->rc_box.corner3.c, p_obstacle->rc_box.corner3.r);
            R_PRINT("\t Corner 4: c = %u r = %u \n", p_obstacle->rc_box.corner4.c, p_obstacle->rc_box.corner4.r);
            return_code = R_RESULT_FAILED;
        }
    }
    else
    {
        float point1_c_float = (float)p_obstacle->rc_box.corner1.c; // cast uint32_t to float
        float point1_r_float = (float)p_obstacle->rc_box.corner1.r; // cast uint32_t to float
        float point2_c_float = (float)p_obstacle->rc_box.corner2.c; // cast uint32_t to float
        float point2_r_float = (float)p_obstacle->rc_box.corner2.r; // cast uint32_t to float
        float point3_c_float = (float)p_obstacle->rc_box.corner3.c; // cast uint32_t to float
        float point3_r_float = (float)p_obstacle->rc_box.corner3.r; // cast uint32_t to float
        float point4_c_float = (float)p_obstacle->rc_box.corner4.c; // cast uint32_t to float
        float point4_r_float = (float)p_obstacle->rc_box.corner4.r; // cast uint32_t to float
        float length_1_to_2 = sqrt(pow(point2_c_float - point1_c_float, 2) + pow(point2_r_float - point1_r_float, 2));
        float length_2_to_3 = sqrt(pow(point3_c_float - point2_c_float, 2) + pow(point3_r_float - point2_r_float, 2));
        float length_3_to_4 = sqrt(pow(point4_c_float - point3_c_float, 2) + pow(point4_r_float - point3_r_float, 2));
        float length_4_to_1 = sqrt(pow(point1_c_float - point4_c_float, 2) + pow(point1_r_float - point4_r_float, 2));
        float length_1_to_3 = sqrt(pow(point3_c_float - point1_c_float, 2) + pow(point3_r_float - point1_r_float, 2));
        float length_2_to_4 = sqrt(pow(point4_c_float - point2_c_float, 2) + pow(point4_r_float - point2_r_float, 2));

        float max_length = length_1_to_2;
        max_length = MAX(max_length, length_2_to_3);
        max_length = MAX(max_length, length_3_to_4);
        max_length = MAX(max_length, length_4_to_1);
        max_length = MAX(max_length, length_1_to_3);
        max_length = MAX(max_length, length_2_to_4);

        /* Note we are checking that connecting point 1 -> point 2 -> point 3 -> point 4 -> point 1 forms a box */
        if ((max_length != length_1_to_3) && (max_length != length_2_to_4))
        {
            R_PRINT_ERROR("Bounding box for vehicle is not correctly formed");
            R_PRINT("\t Corner 1: c = %u r = %u \n", p_obstacle->rc_box.corner1.c, p_obstacle->rc_box.corner1.r);
            R_PRINT("\t Corner 2: c = %u r = %u \n", p_obstacle->rc_box.corner2.c, p_obstacle->rc_box.corner2.r);
            R_PRINT("\t Corner 3: c = %u r = %u \n", p_obstacle->rc_box.corner3.c, p_obstacle->rc_box.corner3.r);
            R_PRINT("\t Corner 4: c = %u r = %u \n", p_obstacle->rc_box.corner4.c, p_obstacle->rc_box.corner4.r);
            R_PRINT("\t line 1 -> 2 %f \n", length_1_to_2);
            R_PRINT("\t line 2 -> 3 %f \n", length_2_to_3);
            R_PRINT("\t line 3 -> 4 %f \n", length_3_to_4);
            R_PRINT("\t line 4 -> 1 %f \n", length_4_to_1);
            R_PRINT("\t line 1 -> 3 %f \n", length_1_to_3);
            R_PRINT("\t line 2 -> 4 %f \n", length_2_to_4);
            return_code = R_RESULT_FAILED;
        }
    }

    return return_code;
}


/******************************************************************************************************************//**
 * Function Name:   connect_points
 * @brief           Draws a line to connect point1 to point2
 * @param [in]      p_color - Color to draw the box
 * @param [in]      point1 - First point to connect
 * @param [in]      point2 - Second point to connect
 * @param [in]      box_class - Class of the box
 * @param [in]      image - Struct containing the image data
 *********************************************************************************************************************/
static void connect_points(const u_bgra_t * p_color, st_2d_rc_point_t point1, st_2d_rc_point_t point2, e_3d_box_class_t box_class, st_image_data_t image)
{
    uint32_t c_value_uint = 0;
    uint32_t r_value_uint = 0;
    uint32_t thickness    = 0;
    float c_slope_float = 0.0;
    float r_slope_float = 0.0;
    float c_value_float = 0.0;
    float r_value_float = 0.0;

    float point1_c_float = (float)point1.c; // cast uint32_t to float
    float point1_r_float = (float)point1.r; // cast uint32_t to float
    float point2_c_float = (float)point2.c; // cast uint32_t to float
    float point2_r_float = (float)point2.r; // cast uint32_t to float

    /* Note: we are currently drawing every line both horizontally and vertically so that we draw as many pixels as possible */

    /* Draw lines horizontally */
    if (point2.c != point1.c)
    {
        c_slope_float = (point2_r_float - point1_r_float)/(point2_c_float - point1_c_float);
        c_value_uint = point1.c;

        while (point2.c != c_value_uint)
        {
            c_value_float = (float)c_value_uint;
            r_value_uint = (uint32_t)round((c_slope_float * (c_value_float - point1_c_float)) + point1_r_float);

            for (thickness = 0; thickness < DRAW_PRV_LINE_THICKNESS; thickness++)
            {
                if (((r_value_uint + thickness) < image.height) && (c_value_uint < image.width))
                {
                    image.p_plane[((r_value_uint + thickness) * image.width) + (c_value_uint)] = p_color->pixel;
                }
            }

            if (c_value_uint < point2.c)
            {
                c_value_uint++;
            }
            else
            {
                c_value_uint--;
            }
        }
    }

    /* Draw lines vertically */
    if (point2.r != point1.r)
    {
        r_slope_float = (point2_c_float - point1_c_float)/(point2_r_float - point1_r_float);
        r_value_uint = point1.r;

        while (point2.r != r_value_uint)
        {
            r_value_float = (float)r_value_uint;
            c_value_uint = (uint32_t)round((r_slope_float * (r_value_float - point1_r_float)) + point1_c_float);

            for (thickness = 0; thickness < DRAW_PRV_LINE_THICKNESS; thickness++)
            {
                if ((r_value_uint < image.height) && ((c_value_uint + thickness) < image.width))
                {
                    image.p_plane[(r_value_uint * image.width) + (c_value_uint + thickness)] = p_color->pixel;
                }
            }

            if (r_value_uint < point2.r)
            {
                r_value_uint++;
            }
            else
            {
                r_value_uint--;
            }
        }
    }
}

/******************************************************************************************************************//**
 * Function Name:   scale_bbox
 * @brief           Scales up a bounding box to based on defined scaling factors. This is useful when bounding boxes are
 *                  generated using a smaller feature map than the image we want to display them on is.
 * @param [in,out]  p_bbox - Pointer to the box data to use scale
 *********************************************************************************************************************/
static void scale_bbox(st_3d_box_t * p_bbox)
{
    p_bbox->rc_box.corner1.c *= DRAW_PRV_WIDTH_SCALE_FACTOR;
    p_bbox->rc_box.corner1.r *= DRAW_PRV_HEIGHT_SCALE_FACTOR;
    p_bbox->rc_box.corner2.c *= DRAW_PRV_WIDTH_SCALE_FACTOR;
    p_bbox->rc_box.corner2.r *= DRAW_PRV_HEIGHT_SCALE_FACTOR;
    p_bbox->rc_box.corner3.c *= DRAW_PRV_WIDTH_SCALE_FACTOR;
    p_bbox->rc_box.corner3.r *= DRAW_PRV_HEIGHT_SCALE_FACTOR;
    p_bbox->rc_box.corner4.c *= DRAW_PRV_WIDTH_SCALE_FACTOR;
    p_bbox->rc_box.corner4.r *= DRAW_PRV_HEIGHT_SCALE_FACTOR;
}

/******************************************************************************************************************//**
 * Function Name:   draw_box
 * @brief           Draws a bounding box on the image
 * @param [in]      p_color - Color to draw the box
 * @param [in]      p_bbox - Pointer to the box data to draw
 * @param [in]      image - Struct containing the image data
 *********************************************************************************************************************/
void draw_box(const u_bgra_t * p_color, const st_3d_box_t * p_bbox, st_image_data_t image)
{
    connect_points(p_color, p_bbox->rc_box.corner1, p_bbox->rc_box.corner2, p_bbox->box_class, image);
    connect_points(p_color, p_bbox->rc_box.corner2, p_bbox->rc_box.corner3, p_bbox->box_class, image);
    connect_points(p_color, p_bbox->rc_box.corner3, p_bbox->rc_box.corner4, p_bbox->box_class, image);
    connect_points(p_color, p_bbox->rc_box.corner4, p_bbox->rc_box.corner1, p_bbox->box_class, image);
}

/******************************************************************************************************************//**
 * Function Name:   draw_circle
 * @brief           Draws a circle on the image
 * @param [in]      p_color - Color to draw the circle
 * @param [in]      p_bbox - Pointer to the box data to draw
 * @param [in]      image - Struct containing the image data
 *********************************************************************************************************************/
void draw_circle(const u_bgra_t * p_color, const st_3d_box_t * p_bbox, st_image_data_t image)
{
    int32_t x_start = (int32_t)p_bbox->rc_box.corner1.c - DRAW_PRV_CIRCLE_RADIUS;
    int32_t y_start = (int32_t)p_bbox->rc_box.corner1.r - DRAW_PRV_CIRCLE_RADIUS;
    int32_t x = 0;
    int32_t y = 0;

    for (int32_t y_index = 0; y_index < (2 * DRAW_PRV_CIRCLE_RADIUS); y_index++)
    {
        for (int32_t x_index = 0; x_index < (2 * DRAW_PRV_CIRCLE_RADIUS); x_index++)
        {
            x = x_start + x_index;
            y = y_start + y_index;

            /* Check both x and y are within image */
            if ((x >= 0) && (x < image.width) && (y >= 0) && (y < image.height))
            {
                /* Check if the pixel should be shaded */
                if (1 == s_circle[x_index][y_index])
                {
                    image.p_plane[(y * image.width) + x] = p_color->pixel;
                }
            }
        }
    }
}

/******************************************************************************************************************//**
 * Function Name:   draw
 * @brief           Draws a bounding box on the image
 * @param [in]      p_data - pointer to the private data structure
 * @param [in]      p_bbox - Pointer to the box data draw
 * @param [in]      image - Struct containing the image data
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
r_result_t draw(const st_priv_t * p_data, const st_3d_box_t * p_bbox, st_image_data_t image)
{
    r_result_t result = R_RESULT_SUCCESS;

    if (NULL == p_bbox)
    {
        R_PRINT_ERROR("Error getting bounding box");
        result = R_RESULT_FAILED;
    }

    /* Check for validity */
    if (R_RESULT_SUCCESS == result)
    {
        if (check_obstacle(p_bbox) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Bounding box is not valid");
            result = R_RESULT_FAILED;
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        const u_bgra_t * p_color = get_color(p_data, p_bbox->box_class);
        st_3d_box_t local_bbox = {0};

        /* Copy bounding box to local box */
        R_COMMON_Memcpy(&local_bbox, p_bbox, sizeof(st_3d_box_t));

        /* Apply scaling factor */
        scale_bbox(&local_bbox);

        /* Draw circles for pedestrians otherwise draw boxes */
        if (R_3D_BOX_CLASS_PEDESTRIAN == p_bbox->box_class)
        {
            draw_circle(p_color, &local_bbox, image);
        }
        else
        {
            draw_box(p_color, &local_bbox, image);
        }
    }

    return result;
}

/***********************************************************************************************************************
 Module Interface
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        /* Hot Pink */
        p_data->vehicle_color.subpixel.b = 255;
        p_data->vehicle_color.subpixel.r = 255;
        p_data->vehicle_color.subpixel.g = 0;
        p_data->vehicle_color.subpixel.a = 0;

        /* Dark Pink */
        p_data->car_color.subpixel.b = 128;
        p_data->car_color.subpixel.r = 255;
        p_data->car_color.subpixel.g = 0;
        p_data->car_color.subpixel.a = 0;

        /* Aqua green */
        p_data->large_car_color.subpixel.b = 128;
        p_data->large_car_color.subpixel.r = 0;
        p_data->large_car_color.subpixel.g = 128;
        p_data->large_car_color.subpixel.a = 0;

        /* Lime Green */
        p_data->long_vehicle_color.subpixel.b = 0;
        p_data->long_vehicle_color.subpixel.r = 0;
        p_data->long_vehicle_color.subpixel.g = 255;
        p_data->long_vehicle_color.subpixel.a = 0;

        /* Grey */
        p_data->group_of_vehicles_color.subpixel.b = 128;
        p_data->group_of_vehicles_color.subpixel.r = 128;
        p_data->group_of_vehicles_color.subpixel.g = 128;
        p_data->group_of_vehicles_color.subpixel.a = 0;

        /* Red */
        p_data->pedestrian_color.subpixel.b = 0;
        p_data->pedestrian_color.subpixel.r = 255;
        p_data->pedestrian_color.subpixel.g = 0;
        p_data->pedestrian_color.subpixel.a = 0;

        /* Black */
        p_data->unknown_color.subpixel.b = 0;
        p_data->unknown_color.subpixel.r = 0;
        p_data->unknown_color.subpixel.g = 0;
        p_data->unknown_color.subpixel.a = 0;
    }

    return result;
}

/******************************************************************************************************************//**
 * @page       PQS_Draw_3dBox_Channels PQS Draw 3D Box  Channels
 * @ingroup    PQS_Draw_3dBox
 * # Channel Table:
 *
 *  Channel name | Channel Type | Object Type  | Description
 *  ------------ | ------------ | ------------ | ----------------------------------
 *  image_inout  | input/output | image        | @ref PQS_Draw_3dBox_Channel_image
 *  bbox_in      | input        | 3D Box       | @ref PQS_Draw_3dBox_Channel_bbox
 *
 * @anchor PQS_Draw_3dBox_Channel_image
 * ## PQS Draw 3D Box Channel image
 *             This is the input and output image to draw on. Not this module will alter the contents of the image.
 *             No other module should be using the image while this module is executing. The format of the
 *             image must be in R_IMAGE_FORMAT_UYVA_INTERLEAVED.
 *
 * @anchor PQS_Draw_3dBox_Channel_bbox
 * ## PQS Draw 3D Box Channel bbox
 *             This is the 3D bounding box object containing the list of all the bounding boxes to draw on the
 *             image.
 *
 * # Time Sync:
 *        To assure that the bounding box and the image are synchronized, the timestamp of both objects will be
 *        compared. If the timestamps are different this module will return a failure.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_BROKER_InitChannelInfo(p_instance, DRAW_PRV_CHANNEL_IMAGE_INOUT, "image_inout", R_OBJECT_TYPE_IMAGE,  R_BROKER_DATA_DIRECTION_INOUT);
    result |= R_BROKER_InitChannelInfo(p_instance, DRAW_PRV_CHANNEL_BBOX_IN,     "bbox_in",     R_OBJECT_TYPE_3D_BOX, R_BROKER_DATA_DIRECTION_IN);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    r_result_t result   = R_RESULT_SUCCESS;
    st_priv_t * p_data  = R_MODULE_GetPrivDataPtr(p_instance);

    st_object_t * p_object_bbox_in     = R_BROKER_GetObjectPtr(p_instance, DRAW_PRV_CHANNEL_BBOX_IN);
    st_object_t * p_object_image_inout = R_BROKER_GetObjectPtr(p_instance, DRAW_PRV_CHANNEL_IMAGE_INOUT);
    st_image_data_t image              = {.p_plane = (uint32_t *)R_OBJECT_GetImagePlaneCPU(p_object_image_inout),
                                          .width   = R_OBJECT_GetImageWidth(p_object_image_inout),
                                          .height  = R_OBJECT_GetImageHeight(p_object_image_inout)};

    /*
    ** Validate inputs
    */

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    if (R_RESULT_SUCCESS == result)
    {
        if ((R_OBJECT_CheckObjectType(p_object_image_inout, R_OBJECT_TYPE_IMAGE)  != R_RESULT_SUCCESS) ||
            (R_OBJECT_CheckObjectType(p_object_bbox_in,     R_OBJECT_TYPE_3D_BOX) != R_RESULT_SUCCESS))
        {
            R_PRINT_ERROR("Invalid object passed");
            result = R_RESULT_FAILED;
        }
    }

    /* Check timestamps */
    if (R_RESULT_SUCCESS == result)
    {
        uint64_t image_timestamp = 0;
        uint64_t bbox_timestamp  = 0;

        if ((R_OBJECT_GetTimeStamp(p_object_image_inout, &image_timestamp) == R_RESULT_SUCCESS) &&
            (R_OBJECT_GetTimeStamp(p_object_bbox_in,     &bbox_timestamp)  == R_RESULT_SUCCESS))
        {
            // Debug
            R_PRINT_DEBUG("Draw - image_timestamp: %lu\n", image_timestamp);
            R_PRINT_DEBUG("Draw - bbox_timestamp: %lu\n", bbox_timestamp);

            if (bbox_timestamp != image_timestamp)
            {
                R_PRINT_ERROR("Timestamps are not synced. Image = %lu BBox = %lu", image_timestamp, bbox_timestamp);
                result = R_RESULT_FAILED;
            }
        }
        else
        {
            R_PRINT_ERROR("Cannot get timestamps");
            result = R_RESULT_FAILED;
        }
    }

    /* Check image */
    if (R_RESULT_SUCCESS == result)
    {
        if (NULL == image.p_plane)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image data pointer");
        }

        if (R_OBJECT_GetImageFormat(p_object_image_inout) != R_IMAGE_FORMAT_RGB32_INTERLEAVED)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image format.  Expected format %d, got %d", R_IMAGE_FORMAT_RGB32_INTERLEAVED, R_OBJECT_GetImageFormat(p_object_image_inout));
        }

        if (0 == image.width)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image width");
        }

        if (0 == image.height)
        {
            result = R_RESULT_FAILED;
            R_PRINT_ERROR("Invalid image height");
        }
    }

    /* Draw boxes */
    if (R_RESULT_SUCCESS == result)
    {
        uint32_t max_objects = R_OBJECT_Get3DBoxCount(p_object_bbox_in);

        for (uint32_t i = 0; i < max_objects; i++)
        {
            st_3d_box_t * p_bbox = R_OBJECT_Get3DBox(p_object_bbox_in, i);

            result |= draw(p_data, p_bbox, image);
        }
    }

    return result;
}


