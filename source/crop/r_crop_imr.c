/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
 * SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/******************************************************************************************************************//**
 * @file          r_crop_imr.c
 * @brief         Source file for image cropping using IMR. See @ref PQS_Crop_IMR for more details
 *********************************************************************************************************************/
 /******************************************************************************************************************//**
 * @page       PQS_Crop_IMR_Algorithm  PQS Crop IMR Algorithm
 * @ingroup    PQS_Crop_IMR
 * # Algorithm Description:
 *             This modules will crop an image. The height and width of the output image can be configured, along
 *             with the location of the cropping rectangle on the input image.
 * # Block Diagram:
 *             @image html crop_imr.png
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * History : YYYY-MM-DD Version  Description
 *         : yyyy-mm-dd 0.00     Beta Version
 *********************************************************************************************************************/

/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/
#include "r_common.h"
#include "r_object_image.h"
#include "generic_api_memory.h"

#include "r_crop_imr.h"
#include "r_imr_api.h"
#include "opencv_lens.h"
#include "imr_dlgen.h"

/***********************************************************************************************************************
 Macro definitions
 **********************************************************************************************************************/

#define CROP_PRV_CHANNEL_IMAGE_IN          (0u) /*!< Broker Channel for input image */
#define CROP_PRV_CHANNEL_IMAGE_OUT         (1u) /*!< Broker Channel for output image */
#define CROP_PRV_CHANNEL_TRIGGER           (2u) /*!< Broker Channel for trigger */

#define CROP_PRV_DL_ALIGN                  (64u)                /*!< Display list memory alignment */
#define CROP_PRV_DL_ALLOC_SIZE             (2u * 1024u * 1024u) /*!< Size of Display list */
#define CROP_PRV_MESH_SIZE                 (4u)                 /*!< Mesh Size */
#define CROP_PRV_OPT_NOT_USED              (R_IMR_EXEC_Y)       /*!< Execution option not used */
#define CROP_PRV_EXEC_TIMEOUT              (500u)               /*!< Execution timeout in miliseconds */

#define CROP_PRV_DEFAULT_INPUT_HEIGHT      (800u)  /*!< Default value for input image height. Can be overridden by config */
#define CROP_PRV_DEFAULT_INPUT_WIDTH       (1280u) /*!< Default value for input image width. Can be overridden by config */

#define CROP_PRV_DEFAULT_OUTPUT_HEIGHT     (400u) /*!< Default value for output image height. Can be overridden by config */
#define CROP_PRV_DEFAULT_OUTPUT_WIDTH      (640u) /*!< Default value for input image width. Can be overridden by config */

#define CROP_PRV_DEFAULT_OFFSET_TOP        (200u) /*!< Default crop parameter. Number of pixels from the top of image to crop.*/
#define CROP_PRV_DEFAULT_OFFSET_LEFT       (320u) /*!< Default crop parameter. Number of pixels from the left of image to crop. */

#define CROP_PRV_DEFAULT_IMR_CHANNEL_START (0) /*!< Default value for IMR staring channel. Can be overridden by config */
#define CROP_PRV_DEFAULT_IMR_CHANNEL_END   (5) /*!< Default value for IMR ending channel. Can be overridden by config */

/***********************************************************************************************************************
 Typedef definitions
 **********************************************************************************************************************/

/*! Configuration structure for crop_imr module */
typedef struct {
    uint32_t     input_height;    /*!< Height of image */
    uint32_t     input_width;     /*!< Width of image */
    uint32_t     output_height;   /*!< Height of image */
    uint32_t     output_width;    /*!< Width of image */
    uint32_t     offset_top;      /*!< Top left corner of crop box */
    uint32_t     offset_left;     /*!< Top left corner of crop box */
    int32_t      start_channel;   /*!< Starting IMR channel */
    int32_t      end_channel;     /*!< Ending IMR channel */
} st_config_t;

/*! Private data structure for crop_imr module */
typedef struct st_priv_tag {
    st_config_t  config;                            /*!< Configuration parameters */
    r_imr_dl_t   display_lists[R_MAX_IMR_CHANNELS]; /*!< Display list for each IMR channels */
    r_imr_data_t imr_input;                         /*!< Input image info */
    r_imr_data_t imr_output;                        /*!< Output image info */
} st_priv_t; /*lint !e9109 !e761 */

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

static r_result_t R_Init(st_module_instance_t * const p_instance);
static r_result_t R_InitChannels(st_module_instance_t * const p_instance);
static r_result_t R_Deinit(st_module_instance_t * const p_instance);
static r_result_t R_ProcessData(st_module_instance_t * const p_instance);

/*! Module interface definition for crop_imr */
const st_module_interface_t g_module_interface_crop_imr = {
    .module_name   = "crop_imr",
    .priv_size     = sizeof(st_priv_t),
    .init          = R_Init,
    .deinit        = R_Deinit,
    .init_channels = R_InitChannels,
    .process_data  = R_ProcessData,
};

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Local Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * @page    PQS_Crop_IMR_Config  PQS Crop IMR Config
 * @ingroup PQS_Crop_IMR
 * # Configuration Table:
 *
 *   Config Name  | Config Type | Default | Range                                | Description
 *   ------------ | ----------- | ------- | -----------------------------------  | ------------------
 *  input_height  | uint32_t    | 800     | 1 - @ref R_OBJECT_IMAGE_MAX_HEIGHT   | Height of the input image
 *  input_width   | uint32_t    | 1280    | 128 - @ref R_OBJECT_IMAGE_MAX_WIDTH  | Width of the input image. Must be a multiple of 128
 *  output_height | uint32_t    | 400     | 1 - @ref R_OBJECT_IMAGE_MAX_HEIGHT   | Height of the input image
 *  output_width  | uint32_t    | 640     | 32 - @ref R_OBJECT_IMAGE_MAX_WIDTH   | Width of the output image. Must be a multiple of 32
 *  offset_top    | uint32_t    | 200     | 0 - (Input width minus output width) | Offset to at top of image
 *  offset_left   | uint32_t    | 320     | 0 - (Input width minus output width) | Offset at left of image
 *  start_channel | int32_t     | 0       | 0 - (@ref R_MAX_IMR_CHANNELS - 1)    | Beginning IMR channel to use for program
 *  end_channel   | int32_t     | 5       | 0 - (@ref R_MAX_IMR_CHANNELS - 1)    | Ending IMR channel to use for program. Cannot be less than start_channel
 *
 * # Cropping Diagram
 *       @image html crop_imr_config.png
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   init_config
 * @brief           Initializes the configuration structure with either the default values or with values from a config
 *                  file if available.
 * @param [in,out]  p_config - pointer to the configuration structure
 * @param [in]      p_instance_name - String of the module instance name
 * @param [in]      p_module_name - String of the module name
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_config(st_config_t * p_config, char const * const p_instance_name, char const * const p_module_name)
{
    r_result_t result = R_RESULT_SUCCESS;

    (void)p_module_name; //not used

    /* Init config */
    p_config->input_height  = R_COMMON_CONFIG_GetUint32(p_instance_name, "input_height",  CROP_PRV_DEFAULT_INPUT_HEIGHT);
    p_config->input_width   = R_COMMON_CONFIG_GetUint32(p_instance_name, "input_width",   CROP_PRV_DEFAULT_INPUT_WIDTH);
    p_config->output_height = R_COMMON_CONFIG_GetUint32(p_instance_name, "output_height", CROP_PRV_DEFAULT_OUTPUT_HEIGHT);
    p_config->output_width  = R_COMMON_CONFIG_GetUint32(p_instance_name, "output_width",  CROP_PRV_DEFAULT_OUTPUT_WIDTH);
    p_config->offset_top    = R_COMMON_CONFIG_GetUint32(p_instance_name, "offset_top",    CROP_PRV_DEFAULT_OFFSET_TOP);
    p_config->offset_left   = R_COMMON_CONFIG_GetUint32(p_instance_name, "offset_left",   CROP_PRV_DEFAULT_OFFSET_LEFT);
    p_config->start_channel = R_COMMON_CONFIG_GetInt32( p_instance_name, "start_channel", CROP_PRV_DEFAULT_IMR_CHANNEL_START);
    p_config->end_channel   = R_COMMON_CONFIG_GetInt32( p_instance_name, "end_channel",   CROP_PRV_DEFAULT_IMR_CHANNEL_END);

    /* Check config */
    if (p_config->input_height > R_OBJECT_IMAGE_MAX_HEIGHT)
    {
        R_PRINT_ERROR("Height must be less than R_OBJECT_IMAGE_MAX_HEIGHT");
        result = R_RESULT_FAILED;
    }

    if ((0u == p_config->input_height) || (0u == p_config->output_height))
    {
        R_PRINT_ERROR("Height cannot be zero!");
        result = R_RESULT_FAILED;
    }

    if (p_config->input_width > R_OBJECT_IMAGE_MAX_WIDTH)
    {
        R_PRINT_ERROR("Width must be less than R_OBJECT_IMAGE_MAX_WIDTH");
        result = R_RESULT_FAILED;
    }

    if ((p_config->offset_top + p_config->output_height) > p_config->input_height)
    {
        R_PRINT_ERROR("Cropping range outside of input image");
        result = R_RESULT_FAILED;
    }

    if ((p_config->offset_left + p_config->output_width) > p_config->input_width)
    {
        R_PRINT_ERROR("Cropping range outside of input image");
        result = R_RESULT_FAILED;
    }

    if (((p_config->input_width * R_IMAGE_BYTES_PP_UYVY_INTERLEAVED) % 256u) != 0u)
    {
        R_PRINT_ERROR("Input stride must be a multiple of 256");
        result = R_RESULT_FAILED;
    }

    if ((p_config->output_width % 32u) != 0u)
    {
        R_PRINT_ERROR("Output width must be a multiple of 32");
        result = R_RESULT_FAILED;
    }

    if (0 > p_config->start_channel)
    {
        R_PRINT_ERROR("Staring IMR channel cannot be less than zero.");
        result = R_RESULT_FAILED;
    }

    if (p_config->start_channel > p_config->end_channel)
    {
        R_PRINT_ERROR("Staring IMR channel cannot be greater than ending channel.");
        result = R_RESULT_FAILED;
    }

    if ((int32_t)R_MAX_IMR_CHANNELS <= p_config->end_channel)
    {
        R_PRINT_ERROR("End channel cannot be greater than R_MAX_IMR_CHANNELS");
        result = R_RESULT_FAILED;
    }


    return result;
}

/******************************************************************************************************************//**
 * Function Name:   init_priv
 * @brief           Initializes the private data structure
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t init_priv(st_module_instance_t * const p_instance)
{
    r_result_t result                  = R_RESULT_SUCCESS;
    st_priv_t * p_data                 = R_MODULE_GetPrivDataPtr(p_instance);
    char const * const p_instance_name = R_MODULE_GetInstanceName(p_instance);
    char const * const p_module_name   = R_MODULE_GetModuleName(p_instance);

    /* Check private data */
    if (NULL == p_data)
    {
        R_PRINT_ERROR("No private data available");
        result = R_RESULT_FAILED;
    }

    /* Init configuration */
    if (R_RESULT_SUCCESS == result)
    {
        if (init_config(&p_data->config, p_instance_name, p_module_name) != R_RESULT_SUCCESS)
        {
            R_PRINT_ERROR("Configuration init has failed");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   process_image_planes
 * @brief           Does the data processing on the raw data.
 * @param [in]      p_instance - Pointer to the module instance
 * @param [in]      p_input_plane - Pointer to input data plane
 * @param [out]     p_output_plane - Pointer to output data plane
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t process_image_planes(st_module_instance_t * const p_instance, void * p_input_plane, void * p_output_plane)
{
    r_result_t result       = R_RESULT_SUCCESS;
    st_priv_t * p_data      = R_MODULE_GetPrivDataPtr(p_instance);
    r_imr_source_t src_info = {0};
    size_t input_size       = 0;
    size_t output_size      = 0;
    int32_t imr_result      = 0;
    int32_t channel_index   = 0;

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Update data */
    if ((R_RESULT_SUCCESS == result))
    {
        input_size  = (size_t)(p_data->imr_input.Stride  * p_data->imr_input.Height);
        output_size = (size_t)(p_data->imr_output.Stride * p_data->imr_output.Height);

        /* Update input struct */
        p_data->imr_input.VmrStartAddr = (addr_t)p_input_plane; /*lint !e9078 !e923 !e9091 */
        p_data->imr_input.PhysAddr     = (uint32_t)gf_GetPhysAddr(p_input_plane);

        /* Update output struct */
        p_data->imr_output.VmrStartAddr = (addr_t)p_output_plane; /*lint !e9078 !e923 !e9091 */
        p_data->imr_output.PhysAddr     = (uint32_t)gf_GetPhysAddr(p_output_plane);

        /* Flush Cache */
        gf_DCacheFlushRange((void *)p_input_plane,  input_size);
        gf_DCacheFlushRange((void *)p_output_plane, output_size);
    }

    /* Set Source data */
    if ((R_RESULT_SUCCESS == result))
    {
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            /* Note: start_line, end_line, and mesh_size are unused */
            src_info.source_width  = p_data->imr_input.Width;
            src_info.source_height = p_data->imr_input.Height;

            imr_result = R_IMR_SetSource(channel_index, &src_info);
            if (E_OK != imr_result)
            {
                R_PRINT_ERROR("Error in R_IMR_SetSource() for channel %d ret = %d", channel_index, imr_result);
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Start IMR */
    if ((R_RESULT_SUCCESS == result))
    {
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            imr_result = R_IMR_ExecuteExt(channel_index,
                                    &p_data->display_lists[channel_index],
                                    &p_data->imr_input,
                                    &p_data->imr_output,
                                    R_IMR_EXE_MODE_BILINEAR_ENABLE | R_IMR_EXE_MODE_TEXTUREMAPPING,
                                    CROP_PRV_OPT_NOT_USED);

            if (E_OK != imr_result)
            {
                R_PRINT_ERROR("Error in R_IMR_ExecuteExt() for channel %d ret = %d", channel_index, imr_result);
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Wait and read status */
    if (R_RESULT_SUCCESS == result)
    {
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            imr_result = R_IMR_WaitEventTimeout(channel_index, CROP_PRV_EXEC_TIMEOUT);

            if (R_IMR_SR_TRA != (uint32_t)imr_result)
            {
                R_PRINT_ERROR("Error in R_IMR_WaitEventTimeout() for channel %d ret = %d", channel_index, result);
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Invalidate data cache */
    gf_DCacheInvalidateRange((void *)p_output_plane, output_size);

    return result;
}

/***********************************************************************************************************************
 Module Interface Functions
 **********************************************************************************************************************/

/******************************************************************************************************************//**
 * Function Name:   R_Init
 * @brief           Initializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Init(st_module_instance_t * const p_instance)
{
    r_result_t result     = init_priv(p_instance);
    st_priv_t * p_data    = R_MODULE_GetPrivDataPtr(p_instance);
    int32_t channel_index = 0;
    int32_t imr_result    = 0;

    /* IMR config structs */
    undistort_params_t opencv_coeff           = {0.0f};
    imr_roi_t imr_regions[R_MAX_IMR_CHANNELS] = {{0}};

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Init local variables */
    if (R_RESULT_SUCCESS == result)
    {
        uint32_t num_channels_used = ((uint32_t)p_data->config.end_channel - (uint32_t)p_data->config.start_channel) + 1u;

        /* Set coefficients correction/scaling */
        opencv_coeff.k1 = 0.0f;
        opencv_coeff.k2 = 0.0f;
        opencv_coeff.k3 = 0.0f;
        opencv_coeff.fx = 0.0f;
        opencv_coeff.fy = 0.0f;
        opencv_coeff.cx = 0.0f;
        opencv_coeff.cy = 0.0f;
        opencv_coeff.ox = (float)(p_data->config.offset_left);
        opencv_coeff.oy = (float)(p_data->config.offset_top);
        opencv_coeff.sx = 1.0f;
        opencv_coeff.sy = 1.0f;

        /* Init IMR input image struct */
        p_data->imr_input.Height       = p_data->config.input_height;
        p_data->imr_input.Width        = p_data->config.input_width;
        p_data->imr_input.bpp          = 8;
        p_data->imr_input.Stride       = p_data->config.input_width * R_IMAGE_BYTES_PP_UYVY_INTERLEAVED;
        p_data->imr_input.Attr         = R_IMR_YUV;
        p_data->imr_input.VmrStartAddr = 0;
        p_data->imr_input.PhysAddr     = 0;

        /* Init IMR output image struct */
        p_data->imr_output.Height       = p_data->config.output_height;
        p_data->imr_output.Width        = p_data->config.output_width;
        p_data->imr_output.bpp          = 8;
        p_data->imr_output.Stride       = p_data->config.output_width * R_IMAGE_BYTES_PP_UYVY_INTERLEAVED;
        p_data->imr_output.Attr         = R_IMR_YUV;
        p_data->imr_output.VmrStartAddr = 0;
        p_data->imr_output.PhysAddr     = 0;

        /* Set IMR regions structs */
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            imr_regions[channel_index].x      = (uint16_t)0;
            imr_regions[channel_index].y      = (uint16_t)(((uint16_t)channel_index - (uint16_t)p_data->config.start_channel) *
                                                    (uint16_t)(p_data->config.output_height / num_channels_used));
            imr_regions[channel_index].Width  = (uint16_t)p_data->config.output_width;
            imr_regions[channel_index].Height = (uint16_t)(p_data->config.output_height / num_channels_used);

            /* last time though loop account for remainder */
            if (p_data->config.end_channel == channel_index)
            {
                imr_regions[channel_index].Height += (uint16_t)(p_data->config.output_height % num_channels_used);
            }

            R_PRINT_INFO("IMR Region for channel %u\n"
                         "\t x: %u\n"
                         "\t y: %u\n"
                         "\t Width: %u\n"
                         "\t Height: %u",
                         channel_index,
                         imr_regions[channel_index].x,
                         imr_regions[channel_index].y,
                         imr_regions[channel_index].Width,
                         imr_regions[channel_index].Height);
        }
    }

    /* Create display lists */
    if (R_RESULT_SUCCESS == result)
    {
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            p_data->display_lists[channel_index].VmrStartAddr = (addr_t)gf_Memalign(CROP_PRV_DL_ALIGN, CROP_PRV_DL_ALLOC_SIZE); /*lint !e923 !e9091 */

            if (0u != p_data->display_lists[channel_index].VmrStartAddr)
            {
                p_data->display_lists[channel_index].PhysAddr = (uint32_t)gf_GetPhysAddr((void*)p_data->display_lists[channel_index].VmrStartAddr); /*lint !e923 !e511 */
                p_data->display_lists[channel_index].Size     = CROP_PRV_DL_ALLOC_SIZE;
                p_data->display_lists[channel_index].Pos      = 0;
            }
            else
            {
                R_PRINT_ERROR("Failed to allocate memory for display list");
                result = R_RESULT_FAILED;
            }
        }
    }

    /* Build display lists */
    if (R_RESULT_SUCCESS == result)
    {
        for (channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            imr_result = imr_build_display_list_roi(&p_data->display_lists[channel_index],
                                                    &p_data->imr_input,
                                                    &p_data->imr_output,
                                                    CROP_PRV_MESH_SIZE,
                                                    map_opencv_scale_shift,
                                                    &opencv_coeff,
                                                    imr_regions[channel_index]);

            if (0 == imr_result)
            {
                /* Flush DL memory to DRAM */
                gf_DCacheFlushRange((void *)p_data->display_lists[channel_index].VmrStartAddr, (size_t)p_data->display_lists[channel_index].Size); /*lint !e923 !e511 */
            }
            else
            {
                R_PRINT_ERROR("ERROR: DL Creation failed with error %d", imr_result);
                result = R_RESULT_FAILED;
            }
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * @page        PQS_Crop_IMR_Channels PQS Crop IMR Channels
 * @ingroup     PQS_Crop_IMR
 * # Channel Table:
 *
 *   Channel name | Channel Type | Object Type | Description
 *   ------------ | ------------ | ----------- | ----------------------------------
 *   image_in     | input        | image       | @ref Crop_IMR_Channel_Image_In
 *   image_out    | output       | image       | @ref Crop_IMR_Channel_Image_Out
 *   trigger      | trigger      | trigger     | @ref Crop_IMR_Channel_Trigger
 *
 * @anchor  Crop_IMR_Channel_Image_In
 * ## PQS Crop IMR Channel Image In:
 *              Input is an YUV422 interleaved UYVY image of type R_IMAGE_FORMAT_UYVY_INTERLEAVED and of the
 *              configured height and the configured size. There are two bytes per pixel and a plane depth of one.
 *
 * @anchor  Crop_IMR_Channel_Image_Out
 * ## PQS Crop IMR Channel Image Out:
 *              Output is an YUV422 interleaved UYVY image of type R_IMAGE_FORMAT_UYVY_INTERLEAVED and of the
 *              configured height and the configured size. There are two bytes per pixel and a plane depth of one.
 *
 * @anchor  Crop_IMR_Channel_Trigger
 * ## PQS Crop IMR Channel Trigger:
 *              This is a trigger channel used to control when the modules will begin processing. Any object
 *              can be routed to this channel, from any module. The data contained in the object will not
 *              be processed.
 *********************************************************************************************************************/
/******************************************************************************************************************//**
 * Function Name:   R_InitChannels
 * @brief           Initializes the data broker channels.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_InitChannels(st_module_instance_t * const p_instance)
{
    r_result_t result = R_RESULT_SUCCESS;

    result |= R_BROKER_InitChannelInfo(p_instance, CROP_PRV_CHANNEL_IMAGE_IN,  "image_in",  R_OBJECT_TYPE_IMAGE,   R_BROKER_DATA_DIRECTION_IN);
    result |= R_BROKER_InitChannelInfo(p_instance, CROP_PRV_CHANNEL_IMAGE_OUT, "image_out", R_OBJECT_TYPE_IMAGE,   R_BROKER_DATA_DIRECTION_OUT);
    result |= R_BROKER_InitChannelInfo(p_instance, CROP_PRV_CHANNEL_TRIGGER,   "trigger",   R_OBJECT_TYPE_TRIGGER, R_BROKER_DATA_DIRECTION_IN);

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_Deinit
 * @brief           Deinitializes the module
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_Deinit(st_module_instance_t * const p_instance)
{
    r_result_t result  = R_RESULT_SUCCESS;
    st_priv_t * p_data = R_MODULE_GetPrivDataPtr(p_instance);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Free display lists */
    if (R_RESULT_SUCCESS == result)
    {
        for (int32_t channel_index = p_data->config.start_channel; channel_index <= p_data->config.end_channel; channel_index++)
        {
            gf_Free((void*)p_data->display_lists[channel_index].VmrStartAddr); /*lint !e923 !e511 */
        }
    }

    return result;
}

/******************************************************************************************************************//**
 * Function Name:   R_ProcessData
 * @brief           Does the data processing.
 * @param [in]      p_instance - Pointer to the module instance
 * @retval          R_RESULT_SUCCESS - on success
 *********************************************************************************************************************/
static r_result_t R_ProcessData(st_module_instance_t * const p_instance)
{
    /* Initialized  locals */
    st_priv_t * p_data        = R_MODULE_GetPrivDataPtr(p_instance);
    r_result_t result         = R_RESULT_SUCCESS;
    st_object_t * p_image_in  = R_BROKER_GetObjectPtr(p_instance, CROP_PRV_CHANNEL_IMAGE_IN);
    st_object_t * p_image_out = R_BROKER_GetObjectPtr(p_instance, CROP_PRV_CHANNEL_IMAGE_OUT);

    if (NULL == p_data)
    {
        R_PRINT_ERROR("Couldn't get private data");
        result = R_RESULT_FAILED;
    }

    /* Check objects */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CheckObjectType(p_image_in,  R_OBJECT_TYPE_IMAGE);
        result |= R_OBJECT_CheckObjectType(p_image_out, R_OBJECT_TYPE_IMAGE);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Invalid object input");
        }
    }

    /* Check input */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CheckImageWidth(        p_image_in, p_data->config.input_width);
        result |= R_OBJECT_CheckImageHeight(       p_image_in, p_data->config.input_height);
        result |= R_OBJECT_CheckImageDepth(        p_image_in, 1);
        result |= R_OBJECT_CheckImageBytesPerPixel(p_image_in, R_IMAGE_BYTES_PP_UYVY_INTERLEAVED);
        result |= R_OBJECT_CheckImageFormat(       p_image_in, R_IMAGE_FORMAT_UYVY_INTERLEAVED);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Invalid image: Expected %u x %u depth=%d, bpp=%d, format=%d.  Got %d x %d depth=%d, bpp=%d, format=%d",
                    p_data->config.input_width,
                    p_data->config.input_height,
                    1,
                    R_IMAGE_BYTES_PP_UYVY_INTERLEAVED,
                    R_IMAGE_FORMAT_UYVY_INTERLEAVED,
                    R_OBJECT_GetImageWidth(        p_image_in),
                    R_OBJECT_GetImageHeight(       p_image_in),
                    R_OBJECT_GetImageDepth(        p_image_in),
                    R_OBJECT_GetImageBytesPerPixel(p_image_in),
                    R_OBJECT_GetImageFormat(       p_image_in));
        }
    }

    /* Fill output object */
    if (R_RESULT_SUCCESS == result)
    {
        result |= R_OBJECT_CopyHeader(           p_image_out, p_image_in);
        result |= R_OBJECT_SetImageWidth(        p_image_out, p_data->config.output_width);
        result |= R_OBJECT_SetImageHeight(       p_image_out, p_data->config.output_height);
        result |= R_OBJECT_SetImageDepth(        p_image_out, 1);
        result |= R_OBJECT_SetImageBytesPerPixel(p_image_out, R_IMAGE_BYTES_PP_UYVY_INTERLEAVED);
        result |= R_OBJECT_SetImageFormat(       p_image_out, R_IMAGE_FORMAT_UYVY_INTERLEAVED);

        if (R_RESULT_SUCCESS != result)
        {
            R_PRINT_ERROR("Couldn't set output params");
        }
    }

    if (R_RESULT_SUCCESS == result)
    {
        void * p_plane_in  = R_OBJECT_GetImagePlaneCPU(p_image_in);
        void * p_plane_out = R_OBJECT_GetImagePlaneCPU(p_image_out);

        /* Start image processing */
        if ((NULL != p_plane_in) && (NULL != p_plane_out))
        {
            if (process_image_planes(p_instance, p_plane_in, p_plane_out) != R_RESULT_SUCCESS)
            {
                R_PRINT_ERROR("Error in imr function");
                result = R_RESULT_FAILED;
            }
        }
        else
        {
            R_PRINT_ERROR("Some error with getting CPU data");
            result = R_RESULT_FAILED;
        }
    }

    return result;
}
