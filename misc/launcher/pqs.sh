#!/bin/sh

EXPECTED_YOCTO_VERSION="2.4.2"
EXPECTED_KERNEL_VERSION="4.14.35"
EXPECTED_SOC="renesas,r8a77980"
EXPECTED_DISPLAY_COLOR_FORMAT="rgb"
EXPECTED_DISPLAY_RESOLUTION=(1920 1080)
EXPECTED_CMA_SIZE="600M"
EXPECTED_IMP_COUNT=5
EXPECTED_IMPC_COUNT=1
EXPECTED_CNN_COUNT=1
EXPECTED_PSC_COUNT=2
EXPECTED_CVE_COUNT=5
EXPECTED_IMR_COUNT=6

# FastRTPS configuration
FASTRTPS_ADDR=224.0.0.0
FASTRTPS_NETMASK=240.0.0.0
FASTRTPS_INTERFACE=eth0

# How much memory should cmem driver use?
CMEMDRV_BSIZE="0x20000000"

# Device tree path -- should not need to ever change
DT_PATH="/proc/device-tree"

PQS_EXE="./RenesasPQS"

# Display stuff -- it is later populated from the active dtb
DISPLAY_COUNT=""
DISPLAY_COLOR_FORMAT=""

## Auto-set stuff -- do not configure!

declare -a PQS_PIPELINE=()
PQS_PROJECT_FILE=""
PQS_PROJECT_SINK=""
PQS_PROJECT_SOURCE=""
PROJECT_FILES=( $(find . -name "*.proj") )
#declare -a PQS_PARAMS=()
PQS_PARAMS=""
PQS_RGF_LIST=""
PQS_INI_LIST=""
GDB_DEBUG="gdbserver :2345"

declare -A pipeline=()
declare -A default=()
inc_path="./"

# To get started, make sure we are in the directory we think we are in!
cd $(dirname $(realpath $0))
if [ "${PC_MODE}" != "y" ]; then
    LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:./lib"
fi

tests_attempted=0
tests_passed=0
tests_failed=0
trying()
{
    echo ""
    echo "==> Checking $1..."
    tests_attempted=$(expr ${tests_attempted} + 1)
}

passed()
{
    echo "...passed"
    tests_passed=$(expr ${tests_passed} + 1)
    true
}

failed()
{
    echo "$1"
    if [ $# -gt 1 ]; then
        echo "    Expected: $2"
        echo "    Actual:   $3"
    fi
    tests_failed=$(expr ${tests_failed} + 1)
    false
}

pqs_driver_swap_imr()
{
    trying "swap of imr driver"
    rmmod rcar_imr > /dev/null 2> /dev/null
    rmmod uio_imr > /dev/null 2> /dev/null
    if modprobe uio_imr; then
        passed
    else
        failed "Couldn't switch to uio_imr driver"
    fi
}

pqs_driver_swap_cmemdrv()
{
    trying "bsize update to ${CMEMDRV_BSIZE} in cmemdrv"
    rmmod cmemdrv > /dev/null 2> /dev/null
    if modprobe cmemdrv bsize=${CMEMDRV_BSIZE}; then
        passed
    else
        failed "Couldn't load cmemdrv with a bsize of ${CMEMDRV_BSIZE}"
    fi
}

pqs_fix_display()
{
    # Disable screen power off
    setterm --term linux --powerdown 0 --blank 0 --cursor off --clear all > /dev/tty1

    #Remove blinky cursor
    echo -e -n '\033[?25l' > /dev/tty0
}

pqs_fix_page_swap_kernel_panic()
{
    echo 5000 > /proc/sys/vm/min_free_kbytes
    echo 1 > /proc/sys/vm/zone_reclaim_mode
}

set_pqs_environment()
{
    pqs_driver_swap_imr
    pqs_driver_swap_cmemdrv
    pqs_fix_display
    pqs_fix_page_swap_kernel_panic
}

setup_network()
{
    if grep -q $'^[ \t]*instance[ \t]*[^ \t]*[ \t]*is[ \t]*publish' ${PQS_RGF_LIST}; then
        trying "routing for FastRTPS"
        if route | grep -q "${FASTRTPS_ADDR}.*${FASTRTPS_NETMASK}.*${FASTRTPS_INTERFACE}"; then
            passed
        elif route add -net "${FASTRTPS_ADDR}" netmask "${FASTRTPS_NETMASK}" "${FASTRTPS_INTERFACE}"; then
            passed
        else
            failed "Couldn't add route"
        fi
    fi
}

run_pqs()
{
    echo "Running PQS..."
    echo "   Pipeline: ${PQS_PIPELINE[@]}"
    echo "   Params:   ${PQS_PARAMS}"
    chmod +x "${PQS_EXE}"

    if [ "${debug}" == "n" ]; then
        eval ${PQS_EXE} ${PQS_PARAMS}
    else
        PQS_EXE="RenesasPQS"	
        eval ${GDB_DEBUG} ${PQS_EXE} ${PQS_PARAMS}
    fi


}

get_path()
{
    file_name=$1
    parent=$2
    prefix_path="$(realpath $(dirname ${parent}))"
    [ "${file_name:0:1}" != "/" ] && file_name="${prefix_path}/${file_name}"
    echo "${file_name}"
}

patch_list()
{
    flag=""
    parent=$1
    list=""
    shift

    for c in $@; do
        if [[ "$c" == -* ]]; then
            flag=$c
        else
            req_flag="${flag}"
            patch="n"
            case "$c" in
                *.ini) req_flag="-c" && patch="y";;
                *.rgf) req_flag="-g" && patch="y";;
            esac
            # If it is an ini or rgf, then correct the path
            [ "${patch}" == "y" ] && c="$(get_path $c $parent)"

            # If the user did not include a flag then add the flag
            [ "${flag}" != "${req_flag}" ] && c="${req_flag} ${c}"
            flag=""
        fi
        list="${list} $c"
    done
    echo "${list}"
}

find_project_file()
{
    pipeline_name=$1
    if [ "${pipeline_name}" != "" ] && file=$(grep -l "^[ ]*pipeline\[\"${pipeline_name}\"\][ ]*=" ${PROJECT_FILES[@]}); then
        echo "${file}"
    else
        false
    fi
}

load_project_file()
{
    file=$1
    # Reset
    pipeline=()
    default=()
    source=()
    sink=()
    inc_path=$(dirname "${file}")
    
    # Source file
    source ${file}
}

get_cmdline_params()
{
    project_file=$1
    shift
    cmd=""
    for p in $@; do
        [[ "$p" != *:- ]] && cmd="$(patch_list ${project_file} ${pipeline[$p]}) ${cmd}"
    done

    echo "${cmd}"
}

extract_flag_values()
{
    flag=$1
    shift
    echo "$@" \
        | sed -re 's/[ \t]*((-[a-zA-Z0-9])?([ \t]*[^- \t][^ \t]*)?)[ \t]?/\1\n/g' \
        | sed -rne "s/^[ \t]*-${flag}[ \t]*(.*)[\n]?/\1/p" \
        | tr '\n' ' '
}

extract_include_file_lists()
{
    PQS_RGF_LIST="$(extract_flag_values g ${PQS_PARAMS})"
    PQS_INI_LIST="$(extract_flag_values c ${PQS_PARAMS})"
}

find_and_load_pipeline()
{
    if ! file=$(find_project_file ${PQS_PIPELINE}); then
        echo "Invalid pipeline name: ${PQS_PIPELINE}"
        exit 1
    fi
    if ! load_project_file ${file}; then
        echo "Invalid pipeline (${PQS_PIPELINE}) loaded from ${file}"
        exit 1
    fi
    PQS_PROJECT_FILE="${file}"

    # Remove defaults if they were listed in pipeline
    for p in ${PQS_PIPELINE[@]}; do
        [[ "$p" == *:* ]] && default[${p%%:*}]=""
    done

    # Load defaults
    PQS_PIPELINE+=( ${default[@]} )
}

print_all_source_sink()
{
    for p in ${@}; do
        [[ "${p}" == *:* ]] && printf '   %s\n' "${p}"
    done
}


list_pqs_source_sink()
{
    find_and_load_pipeline

    echo ""
    echo "Sources/Sinks for ${PQS_PIPELINE} (from ${file})"
    echo "================="
    print_all_source_sink ${!pipeline[@]} | sort
    echo "================="
    echo ""
}

list_pqs_pipeline()
{
    echo ""
    echo "PQS Pipelines"
    echo "================="
    for f in ${PROJECT_FILES[@]}; do
        echo " From $f:"
        load_project_file $f
        for p in ${!pipeline[@]}; do
            [[ "${p}" != *:* ]] && printf '   %s\n' "${p}"
        done
    done
    echo "================="
    echo ""
}

list_pqs()
{
    if [ "${PQS_PIPELINE}" == "" ]; then
        list_pqs_pipeline
    else
        list_pqs_source_sink
    fi
}

find_pipeline_from_file_list()
{
    if [ "$1" != "" ]; then
        for f in ${@:2}; do
            grep -l "^[ ]*${1}[ ]*=" ${@:2} 2> /dev/null && return
        done
    fi
    false
}

path_patch()
{
    echo tbd
}

select_pqs_pipeline()
{
    if [ "${PQS_PIPELINE}" == "" ]; then
        list_pqs_pipeline
        echo ""
        read -p "Enter pipeline name: " PQS_PIPELINE
    fi
    echo "PQS Pipeline Selected: ${PQS_PIPELINE}"
}

hex_add()
{
    dec1=$((0x$1))
    dec2=$((0x$2))
    printf '%08X' $(expr ${dec1} + ${dec2})
}

hex_subtract()
{
    dec1=$((0x$1))
    dec2=$((0x$2))
    printf '%08X' $(expr ${dec1} - ${dec2})
}

test_version()
{
    name="$1"
    trying "version of ${name}"
    expected=(${2//./ })
    actual=(${3//./ })
    difference="same"
    for i in "${!expected[@]}"; do
        if [ "${actual[$i]}" == "" ]; then
            actual[$i]="-1"
        fi
        if [ "${expected[$i]}" != "${actual[$i]}" ]; then
            if [ "${expected[$i]}" -gt "${actual[$i]}" ]; then
                difference="older"
                break
            else
                difference="newer"
                break
            fi
        fi
    done
    if [ "${difference}" == "same" ]; then
        passed
    else
        failed "Invalid ${name} version.  Your version is ${difference} than expected" "$2" "$3"
    fi
}

test_dtb_compatible_string()
{
    dt_name="$1"
    expected="$2"
    trying "DTB compatibility for \"${dt_name}\""
    actual=($(sed 's/\o0/ /g' "${DT_PATH}/${dt_name}"))
    difference="different"
    for str in "${actual[@]}"; do
        if [ "${str}" == "${expected}" ]; then
            passed
            return
        fi
    done
    actual="${actual[@]}"
    failed "Invalid DTB compatibility for value \"${dt_name}\"" "${expected}" "${actual}"
}

get_dtb_value()
{
    sed 's/\o0//g' ${DT_PATH}/${1}
}

get_display_colorspace()
{
    display_name='soc/i2c*/hdmi*/adi,input-colorspace'
    display_list=($(ls ${DT_PATH}/${display_name}))
    DISPLAY_COUNT="${#display_list[@]}"
    DISPLAY_COLOR_FORMAT="$(get_dtb_value ${display_name})"
}

test_dtb_monitor_colorspace()
{
    trying "display color format"
    if [ "1" != "${DISPLAY_COUNT}" ]; then
        failed "Unknown display setting in DTB" "monitor count = 1" "monitor count = ${DISPLAY_COUNT}"
    else
        if [ "${EXPECTED_DISPLAY_COLOR_FORMAT}" == "${DISPLAY_COLOR_FORMAT}" ]; then
            passed
        else
            failed "Format mismatch" "${EXPECTED_DISPLAY_COLOR_FORMAT}" "${DISPLAY_COLOR_FORMAT}"
        fi
    fi
}

get_cmdline_value()
{
    echo " $(cat /proc/cmdline) " | sed -n "s/^.*[ \t]${1}=\([^ \t]*\).*\$/\1/p"
}

check_cmdline_flag()
{
    echo " $(cat /proc/cmdline) " | sed -n "s/^.*[ \t]${1}[ \t].*\$/set/p"
}

test_bootargs_display()
{
    trying "bootargs display settings"
    raw=$(get_cmdline_value video)
    cmdline_res=($(echo ${raw} | sed -n 's/^[^0-9]*\([0-9]*\)[xX]\([0-9]*\).*$/\1 \2/p'))
    expected="${EXPECTED_DISPLAY_RESOLUTION[0]}x${EXPECTED_DISPLAY_RESOLUTION[1]}"
    actual="${cmdline_res[0]}x${cmdline_res[1]}"
    if [ "${expected}" == "${actual}" ]; then
        passed
    else
        failed "bootarg \"video\" does not match expected value" "${expected}" "${raw}"
    fi
}

test_bootargs_value()
{
    trying "bootarg \"$1\""
    expected="$2"
    actual=`get_cmdline_value "$1"`
    if [ "${expected}" == "${actual}" ]; then
        passed
    else
        failed "bootarg \"$1\" does not match expected value" "${expected}" "${actual}"
    fi
}

test_fb_size()
{
    trying "framebuffer size"
    fb_tmp=/tmp/fb.tmp
    cp /dev/fb0 "${fb_tmp}"
    actual=$(( $(stat -c%s "${fb_tmp}") ))
    rm "${fb_tmp}"
    expected="$(( ${EXPECTED_DISPLAY_RESOLUTION[0]} * ${EXPECTED_DISPLAY_RESOLUTION[1]} * 4 ))"
    if [ "${expected}" == "${actual}" ]; then
        passed
    else
        if [ "${actual}" == "$(( 1024 * 768 * 4 ))" ]; then
            actual="${actual} (Has your monitor been attached since power was applied?)"
        fi
        failed "Framebuffer size mismatch" "${expected}" "${actual}"
    fi
}

test_dev_count()
{
    trying "device count for \"$1\""
    actual=$(ls /dev | grep -E "${1}[0-9]+" | wc -l)
    expected="$2"
    if [ "${expected}" == "${actual}" ]; then
        passed
    else
        failed "device count for \"$1\" did not match expected value" "${expected}" "${actual}"
    fi
}

imr_patch()
{
    str=''
    for i in 0 1 2; do
        addr=$(hex_add FE870000 "${i}0000" | tr '[:upper:]' '[:lower:]')
        str="${str}s/imr${i}@${addr}/imr$(expr ${i} + 1)@${addr}/gI; "
    done
    sed -i "${str}" "${1}"
}

dtb_patch_all()
{
    patch_fcn="$1"
    had_failure=no
    BACKUP_DIR="/boot/backup_dtb_$(find /boot -type d -path "/boot/backup_dtb_*" | wc -l)"
    echo "Backing up DTBs to \"${BACKUP_DIR}\"..."
    mkdir "${BACKUP_DIR}"
    for f in $(find /boot -maxdepth 1 -type f -path "*.dtb"); do
        if cp "${f}" "${BACKUP_DIR}/" && eval "${patch_fcn}" "${f}"; then
            echo "Successfully patched file \"$f\""
        else
            echo "Error attempting to patch \"$f\"..."
            had_failure=yes
        fi
    done
    if [ "${had_failure}" == "no" ]; then
        echo "Successfully patched all DTBs!"
        echo "Rebooting now..."
        sync
        reboot
        exit
    else
        echo "Error patching files.  Please consider copying the backup from \"${BACKUP_DIR}\""
        echo "Example: "
        echo "\$ cp -r \"${BACKUP_DIR}\" /boot"
        echo ""
        echo "Exiting now to avoid unrecoverable system..."
        exit 1
    fi
}

evaluate_imr_patch()
{
    ls ${DT_PATH}/soc/imr3* > /dev/null 2> /dev/null && echo "Note: Device tree appears to be correct" && return
    echo ""
    echo "================================================================================"
    echo "== It seems that you have an error in your dtb regarding imr.  This script    =="
    echo "== can attempt to patch your dtb in place.  Before patching, it will create a =="
    echo "== backup of your dtb files under /boot/dtb_backup_\# (where \# starts at 0). =="
    echo "==                                                                            =="
    echo "== Note: A patch will automatically reset your board                          =="
    echo "================================================================================"
    echo ""
    read -p "Would you like to patch? [y/N]" -t 120 do_patch
    if [ "$(echo $do_patch | tr Y y)" == "y" ]; then
        dtb_patch_all imr_patch
    else
        echo "Skipping patch..."
    fi

}

self_test()
{
    imr_patch=n
    . /etc/os-release
    echo ""
    test_version "Yocto" "${EXPECTED_YOCTO_VERSION}" "${VERSION_ID}"
    test_version "Linux Kernel" "${EXPECTED_KERNEL_VERSION}" "$(uname -r | grep -om1 '^[0-9\.]*')"
    test_dtb_compatible_string "compatible" "${EXPECTED_SOC}"
    test_dtb_monitor_colorspace "${EXPECTED_DISPLAY_COLOR_FORMAT}"
    test_bootargs_display
    test_bootargs_value cma "${EXPECTED_CMA_SIZE}"
    test_fb_size
    test_dev_count imp "${EXPECTED_IMP_COUNT}"
    test_dev_count impc "${EXPECTED_IMPC_COUNT}"
    test_dev_count impsc "${EXPECTED_CVE_COUNT}"
    test_dev_count imppsc "${EXPECTED_PSC_COUNT}"
    test_dev_count impcnn "${EXPECTED_CNN_COUNT}"
    test_dev_count imr "${EXPECTED_IMR_COUNT}" || evaluate_imr_patch

}

summary()
{
    echo ""
    echo "==> Environment Check summary"
    echo "    Tests:  ${tests_attempted}"
    echo "    Passed: ${tests_passed}"
    echo "    Failed: ${tests_failed}"
    if [ "${tests_attempted}" == "${tests_passed}" ]; then
        echo "==> Environment check PASSED"
    else
        echo "==> FAILED environment check"
        echo "==> Continuing to attempt to run PQS..."
    fi
    echo ""
}

echo ""
echo "    ********"
echo "  ***     ***"
echo "           ***  ********  ****   **   ********   ********     *****    ********"
echo "          ***  ***        *****  **  ***        ***          *** ***  ***"
echo "  **** ****    *********  ** *** **  *********   ********   ***   ***  ********"
echo "  ****   ***   ***        **  *****  ***               *** ***     ***       ***"
echo "  ****    ***   ********  **   ****   ********  ********* ***  ****************"
echo "            **"
echo "              ***                   Perception Quick Start"
echo ""

print_help()
{
    echo "===================================="
    echo "Perception Quick Start Launcher Help"
    echo "===================================="
    echo " PQS Launcher Usage:"
    echo " ./pqs.sh [Option] [PQS_Pipeline] [source:Data_Source] [sink:Data_Sink] [-- PQS_Flags]"
    echo ""
    echo " Option"
    echo "   Note: At most 1 option can be used"
    echo "   -h|--help:               Prints this help message"
    echo "   -l|--list|ls:            Lists pipelines"
    echo "   -l|--list|ls <pipeline>: Lists sources/sinks for a pipeline"
    echo "   -debug                   Runs gdbserver for the purpose of debugging"	
    echo "   notest:                  Executes without doing pre-tests"
    echo "   noexe:                   Does all checks but does not execute PQS"
    #echo "   rgb_patch:              !!Experimental!! Patches DTB for rgb displays"
    #echo "   yuv_patch:              !!Experimental!! Patches DTB for yuv displays"
    echo ""
    echo " PQS_Pipeline"
    echo "    Allows auto run of existing PQS pipeline."
    echo "      * If no pipeline is provided, it will prompt you for which pipeline to load"
    echo ""
    echo " source:Data_Source"
    echo "    Allows you to specify the data source"
    echo "      * You must specify a PQS_Pipeline to use this option."
    echo "      * If this parameter is omitted then the defaults for the pipeline"
    echo "        will be taken."
    echo "      * E.g. source:camera"
    echo ""
    echo " sink:Data_Sink"
    echo "    Allows you to specify the data sink."
    echo "      * You must specify a PQS_Pipeline to use this option."
    echo "      * If this parameter is omitted then the defaults for the pipeline"
    echo "        will be taken."
    echo "      * E.g. sink:display_yuv444"
    echo ""
    echo " PQS_Flags"
    echo "    Anything after '--' will be passed directly to ${PQS_EXE} as command line"
    echo "    flags, after the pipeline specific flags."
    echo "      * See ${PQS_EXE} help below for a list of available flags"
    echo "      * E.g. ./pqs.sh -- -v0"
    echo ""
    echo "===================================="
    echo "End of PQS Launcher Help"
    echo "===================================="
    ${PQS_EXE} -h
    echo ""
}

do_test=y
do_list=n
just_echo=""
shift_count=1
debug=n

case "$1" in
    --)                           shift_count=0;;
    -l|-list|--list|list|-ls|ls)  do_list=y;;
    -debug)                       debug=y;;
    -*|help)                      print_help && exit;;
    rgb_patch|yuv_patch)          eval $1 && exit;;
    notest)                       do_test=n;;
    noexe)                        just_echo="echo";;
    *)                            shift_count=0;;
esac

shift "${shift_count}"
# Load environment
while [ "$1" != "--" ] && [ "$1" != "" ]; do
    p="$p $1"
    shift
done
# Eat the --
[ "$1" == "--" ] && shift
PQS_PIPELINE=( $p )
PQS_PARAMS=""
# Add passed params to the PQS_PARAMS variable
# Need to look @ each one individually so we don't miss space characters
for p in "${@}"; do
    PQS_PARAMS="${PQS_PARAMS} $(echo "$p" | sed -r "s/([ \t]+)/'\1'/g")"
done

# Run command
if [ "${do_list}" == "y" ]; then
    list_pqs
else
    # Check colorspace first as projects may need DISPLAY_COLOR_FORMAT 
    get_display_colorspace
    select_pqs_pipeline
    find_and_load_pipeline
    PQS_PARAMS="$(get_cmdline_params ${PQS_PROJECT_FILE} ${PQS_PIPELINE[@]}) ${PQS_PARAMS}"
    extract_include_file_lists
    if [ "${PC_MODE}" != "y" ]; then
        set_pqs_environment
        setup_network
        if [ "${do_test}" == "y" ]; then
            self_test
            summary
        fi
    fi
    run_pqs ${just_echo}
fi
