include make/common.mk

#Everything from /source/ folder
SRC_CVE_C   := $(call rwildcard, source/, *.cv.c)
SRC_C       := $(call rwildcard, source/, *.c)
SRC_C       := $(filter-out      $(SRC_CVE_C),$(SRC_C))
SRC_C       := $(filter-out      $(DO_NOT_COMPILE),$(SRC_C))
SRC_CPP     := $(call rwildcard, source/, *.cpp)
SRC_CXX     := $(call rwildcard, source/, *.cxx)
OBJ_C       := $(patsubst %.c,   $(BUILD)/%.o, $(SRC_C))
OBJ_CPP     := $(patsubst %.cpp, $(BUILD)/%.o, $(SRC_CPP))
OBJ_CXX     := $(patsubst %.cxx, $(BUILD)/%.o, $(SRC_CXX))
INC         := $(call rwildcard, source/, *.h *.hpp)
INC_GEN     := $(call rwildcard, $(AUTOGEN)/, *.h *.hpp)

#Everything from /lib/rcar_environment/
SRC_LIB_RCAR_MAIN_C  := $(call rwildcard, $(RCAR_ENV_PATH)/, *main.c)
SRC_LIB_RCAR_C       := $(call rwildcard, $(RCAR_ENV_PATH)/, *.c)
SRC_LIB_RCAR_C       := $(filter-out      $(SRC_LIB_RCAR_MAIN_C),$(SRC_LIB_RCAR_C))
SRC_LIB_RCAR_C       := $(subst $(RCAR_ENV_PATH)/,,  $(SRC_LIB_RCAR_C))
OBJ_LIB_RCAR_C       := $(patsubst %.c,   $(BUILD)/lib/rcar_environment/%.o, $(SRC_LIB_RCAR_C))
INC_LIB_RCAR         := $(call rwildcard, $(RCAR_ENV_PATH)/, *.h *.hpp)
LIBS_STATIC_RCAR     := $(call rwildcard, $(RCAR_ENV_PATH)/, *.a)
	
#Everything from /lib/renesas/
SRC_LIB_RENESAS_C    := $(call rwildcard, lib/renesas/, *.c)
OBJ_LIB_RENESAS_C    := $(patsubst %.c,   $(BUILD)/%.o, $(SRC_LIB_RENESAS_C))
INC_LIB_RENESAS      := $(call rwildcard, lib/renesas/, *.h *.hpp)
LIBS_STATIC_RENESAS  := $(call rwildcard, lib/renesas/, *.a)

#Everything from /lib/third_party/
SRC_LIB_TP_C         := $(call rwildcard, lib/third_party/, *.c)
SRC_LIB_TP_CPP       := $(call rwildcard, lib/third_party/, *.cpp)
OBJ_LIB_TP_C         := $(patsubst %.c,   $(BUILD)/%.o, $(SRC_LIB_TP_C))
OBJ_LIB_TP_CPP       := $(patsubst %.cpp, $(BUILD)/%.o, $(SRC_LIB_TP_CPP))
LIBS_STATIC_TP       := $(call rwildcard, lib/third_party/, *.a)
LIBS_DYNAMIC_TP      := $(call rwildcard, lib/third_party/, *.so)

#Put it all together 
LDLIBS           := $(LIBS_STATIC_RCAR) $(LIBS_STATIC_RENESAS) $(LIBS_STATIC_TP) $(addprefix -l,$(EXT_LIBS)) $(addprefix -L,$(sort $(dir $(LIBS_DYNAMIC_TP))))
INC_DIR          := $(sort $(dir $(INC) $(INC_GEN) $(INC_LIB_RCAR) $(INC_LIB_RENESAS)))
OBJ              := $(OBJ_C) $(OBJ_CPP) $(OBJ_CXX) $(OBJ_LIB_RCAR_C) $(OBJ_LIB_RENESAS_C) $(OBJ_LIB_TP_C) $(OBJ_LIB_TP_CPP)

# Add defines to flags
INCFLAGS        += $(addprefix -I, $(INC_DIR))
INCFLAGS        += $(addprefix -isystem, $(COMPILER_INC) $(THIRD_PARTY_LIB))
CFLAGS          += $(COMMON_FLAGS)
CXXFLAGS        += $(COMMON_FLAGS)

DEP             := $(patsubst %.o,%.d,$(OBJ))
DEPNAME         = $(patsubst %.o,%.d,$@)
DEPFLAGS        = -MT $@ -MMD -MP -MF $(DEPNAME).tmp
DEPMV           = $(MV) $(DEPNAME).tmp $(DEPNAME)

.PHONY: default clean

default: version $(TARGET)

clean: 
	$(RM) $(BUILD) $(BIN)

version: 
	$(RM) $(BUILD)/source/common/r_common_version.o

# Generic compile for C files
$(OBJ_C): $(BUILD)/%.o: %.c
	$(info CC $<)
	$(MKDIR)
	$(MUTE)$(CC) $(CFLAGS) $(INCFLAGS) $(DEPFLAGS) $< -o $@
	$(MUTE)$(DEPMV)

# Generic compile for CPP files
$(OBJ_CPP): $(BUILD)/%.o: %.cpp
	$(info CXX $<)
	$(MKDIR)
	$(MUTE)$(CXX) $(CXXFLAGS) $(INCFLAGS) $(DEPFLAGS) $< -o $@
	$(MUTE)$(DEPMV)
	
# Generic compile for CXX files
$(OBJ_CXX): $(BUILD)/%.o: %.cxx
	$(info CXX $<)
	$(MKDIR)
	$(MUTE)$(CXX) $(CXXFLAGS) $(INCFLAGS) $(DEPFLAGS) $< -o $@
	$(MUTE)$(DEPMV)

# Generic compile for RCAR_ENV C files ignore warnings
$(OBJ_LIB_RCAR_C): $(BUILD)/lib/rcar_environment/%.o: $(RCAR_ENV_PATH)/%.c
	$(info CC $<)
	$(MKDIR)
	$(MUTE)$(CC) $(CFLAGS) -w $(INCFLAGS) $(DEPFLAGS) $< -o $@
	$(MUTE)$(DEPMV)

# Generic compile for C files ignore warnings
$(OBJ_LIB_RENESAS_C) $(OBJ_LIB_TP_C): $(BUILD)/%.o: %.c
	$(info CC $<)
	$(MKDIR)
	$(MUTE)$(CC) $(CFLAGS) -w $(INCFLAGS) $(DEPFLAGS) $< -o $@
	$(MUTE)$(DEPMV)

# Generic compile for CPP files ignore warnings
$(OBJ_LIB_TP_CPP): $(BUILD)/%.o: %.cpp 
	$(info CXX $<)
	$(MKDIR)
	$(MUTE)$(CXX) $(CXXFLAGS) -w $(INCFLAGS) $(DEPFLAGS) $< -o $@
	$(MUTE)$(DEPMV)

# Overall goal - make the program
$(TARGET): $(OBJ) 
	$(info Linking...)
	$(MKDIR)
	$(MUTE)$(CXX) $(LDFLAGS) -o $@ $(OBJ) $(LDLIBS)

# All dependencies included here (if they exist)
-include $(DEP)
