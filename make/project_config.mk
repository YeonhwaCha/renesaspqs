#Name for project binary
PRJ_NAME        := RenesasPQS

#Current build date
BUILD_DATE      := $(shell date --iso-8601)

#Define compiler used
COMPILER_TYPE   := GNU

#Define versions
PQS_MAJOR_VERSION  := "3"
PQS_MINOR_VERSION  := "1"
PQS_BUGFIX_VERSION := "0"

#Git version tag and number of uncommitted files
ifneq ("$(wildcard ./.git/HEAD)","")
   GIT_COMMIT     := $(shell git rev-parse --verify --short HEAD)
   GIT_UNCOMITTED := $(shell git diff HEAD --numstat | wc -l)
else
   GIT_COMMIT     := "NOT VERSIONED"
   GIT_UNCOMITTED := "NOT_VERSIONED"
endif

# Flags for CC
CFLAGS          +=

# Flags for CXX
CXXFLAGS        += -std=c++0x -std=c++11

#Optimization level based on debug flag
ifeq ($(DEBUG),true)
  COMMON_FLAGS += -O0 -g3
else
  COMMON_FLAGS += -O2 -g0
endif

# Any flags common to CC and CXX
COMMON_FLAGS    += -c -Wall -march=armv8-a -mtune=cortex-a53 \
		   -DDRV_ARCH_RCAR_V3H                            \
		   -DPQS_MAJOR_VERSION=\"$(PQS_MAJOR_VERSION)\"   \
		   -DPQS_MINOR_VERSION=\"$(PQS_MINOR_VERSION)\"   \
		   -DPQS_BUGFIX_VERSION=\"$(PQS_BUGFIX_VERSION)\" \
		   -DGIT_COMMIT=\"$(GIT_COMMIT)\"                 \
		   -DGIT_UNCOMITTED=\"$(GIT_UNCOMITTED)\"         \
		   -DBUILD_DATE=\"$(BUILD_DATE)\"

# Flags for LD
LDFLAGS         +=

# List any external libraries the code depends on here
EXT_LIBS        += pthread m

# List any third party library paths
THIRD_PARTY_LIB += ./lib/third_party/ndevilla/iniparser

#RCAR settings
RCAR_TYPE       := V3H
RCAR_BUILD      := rcar_linux_HIL_aarch64_GNU
