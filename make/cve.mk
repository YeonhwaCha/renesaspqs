include make/common.mk

# All Cve C files
CVE_SRC_C       := $(call rwildcard, source/, *.cv.c)
# All Cve S files not already in build folder
CVE_SRC_ASM     := $(call rwildcard, source/, *.cv.s)
# Where to put S files from C build
CVE_C_ASM_BUILD := $(patsubst %.c,$(BUILD)/%.s,$(CVE_SRC_C))
# Where to copy S files 
CVE_ASM_BUILD   := $(patsubst %.s,$(BUILD)/%.s,$(CVE_SRC_ASM))
# All ASM files
CVE_ASM         := $(CVE_C_ASM_BUILD) $(CVE_ASM_BUILD)
# All Cve obj files
CVE_OBJ         := $(patsubst %.s,%.o,$(CVE_ASM))
# All Cve bin files
CVE_BIN         := $(patsubst %.o,%.bin,$(CVE_OBJ))
# All Cve h files
CVE_H           := $(patsubst $(BUILD)/%.bin,$(AUTOGEN)/%.h, $(CVE_BIN))

.PHONY: default
.NOTPARALLEL:

default: $(CVE_H)

clean:
	$(RM) $(AUTOGEN)
	
# Create s from CVE C code
$(CVE_C_ASM_BUILD): $(BUILD)/%.s: %.c
	$(info ccimp $<)
	$(MKDIR)
	@ccimp.exe -Ospeed -S -stack-section=impc $< -o $@

# Just copy asm files over to build folder to make life easier
$(CVE_ASM_BUILD): $(BUILD)/%.s: %.s
	$(MKDIR)
	$(CP)  $< $@

# Create o from CVE ASM code
$(CVE_OBJ): %.o: %.s
	$(info sasm $<)
	$(MKDIR)
	@sasm.exe -c $(RCAR_TYPE) -O ELF -T REL $< -o $@

# Create bin from CVE o
$(CVE_BIN): %.bin: %.o
	$(info slink $<)
	$(MKDIR)
	@slink.exe -z stack-section=impc $< -o $@ -d

# Create headers
$(CVE_H): $(AUTOGEN)/%.h: $(BUILD)/%.bin
	$(info scdmp $<)
	$(MKDIR)
	@scdmp.exe $< -o $@
