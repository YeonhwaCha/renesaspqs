#Use this file to change configurations for different users of the same project
#All paths must use forward slash (i.e '/') not backslash

#Define path to the rcar_environment folder
RCAR_ENV_PATH := C:/Renesas/rcar_environment/RCAR_V3H1/rcar_linux_HIL_aarch64_GNU

#Define compiler path, only necessary for Windows compilation
COMPILER_PATH := C:/Renesas/toolchains/gcc-linaro-7.3.1-2018.05-i686-mingw32_aarch64-linux-gnu

#Define absolute path to the RenesasPQS root folder (only necesary for running PC-Lint)
PROJECT_PATH := C:/Renesas/projects/RenesasPQS

#Optional user pre build step invoked with target 'user'
user_pre_build:

#Optional user post build step invoked with target 'user'
user_post_build:

