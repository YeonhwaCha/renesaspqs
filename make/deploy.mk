include make/common.mk

# All files in project folder
PROJECT_CNN_FILES       := $(call rwildcard, misc/projects/, *.bin) $(call rwildcard, misc/projects/, *.bcl)
PROJECT_CNN_FILE_DEPLOY := $(patsubst misc/projects/%,$(DEPLOY)/projects/%, $(PROJECT_CNN_FILES))

PROJECT_TEXT_FILES       := $(call rwildcard, misc/projects/, *.proj) $(call rwildcard, misc/projects/, *.rgf) $(call rwildcard, misc/projects/, *.ini)
PROJECT_TEXT_FILE_DEPLOY := $(patsubst misc/projects/%, $(DEPLOY)/projects/%, $(PROJECT_TEXT_FILES))

LAUNCHER_FILES       := $(call rwildcard, misc/launcher/, *.sh) 
LAUNCHER_FILE_DEPLOY := $(patsubst misc/launcher/%, $(DEPLOY)/%, $(LAUNCHER_FILES))

#Fast RTPS libs
RTPS_FILES       := $(call rwildcard, lib/third_party/eProsima/lib/, *)
RTPS_FILE_DEPLOY := $(patsubst lib/third_party/eProsima/lib/%,$(DEPLOY)/lib/%, $(RTPS_FILES))

#Data
DATA_FILES       := $(call rwildcard, data/, *)
DATA_FILE_DEPLOY := $(patsubst data/%, $(DEPLOY)/data/%, $(DATA_FILES))

#Binary 
BIN_DEPLOY := $(patsubst $(BIN)/%,$(DEPLOY)/%, $(TARGET))

.PHONY: default clean

default: $(PROJECT_CNN_FILE_DEPLOY) $(PROJECT_TEXT_FILE_DEPLOY) $(LAUNCHER_FILE_DEPLOY) $(RTPS_FILE_DEPLOY) $(DATA_FILE_DEPLOY) $(BIN_DEPLOY)

clean: 
	$(RM) $(DEPLOY)

$(PROJECT_CNN_FILE_DEPLOY): $(DEPLOY)/projects/%: misc/projects/%
	$(MKDIR)
	$(CP) $< $@

$(PROJECT_TEXT_FILE_DEPLOY): $(DEPLOY)/projects/%: misc/projects/%
	$(MKDIR)
	$(CP) $< $@
	@dos2unix -q $@

$(LAUNCHER_FILE_DEPLOY): $(DEPLOY)/%: misc/launcher/%
	$(MKDIR)
	$(CP) $< $@
	@dos2unix -q $@

$(RTPS_FILE_DEPLOY): $(DEPLOY)/lib/%: lib/third_party/eProsima/lib/%
	$(MKDIR)
	$(CP) $< $@

$(DATA_FILE_DEPLOY): $(DEPLOY)/data/%: data/%
	$(MKDIR)
	$(CP) $< $@

$(BIN_DEPLOY): $(DEPLOY)/%: $(BIN)/%
	$(MKDIR)
	$(CP) $< $@

