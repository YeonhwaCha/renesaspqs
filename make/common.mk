include make/project_config.mk
include make/user_config.mk

AUTOGEN  := ./autogen
BUILD    := ./build
LINT     := ./lint
BIN      := ./bin
DEPLOY   := ./deploy
TARGET   := $(BIN)/$(PRJ_NAME)

#UNIX command definitions
MKDIR = @mkdir -p $(@D)
ECHO  = @echo
MV    = @mv
CP    = @cp
RM    = @rm -rf

rwildcard = $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2) $(filter $(subst *,%,$2),$d))

#Allow for full verbose output if VEBOSE is defined via commandline
ifndef VERBOSE
  MUTE := @
else
  MUTE := 
endif

#Get the OS
ifeq ($(OS), Windows_NT)
  OS_USED := WINDOWS
else
  UNAME := $(shell uname)
  ifeq ($(UNAME), Linux)
    OS_USED := LINUX
  else
    $(info )
    $(info Problem detecting your computers operating system only Windows and Linux are supported)
    $(error Invalid environment)
  endif
endif

#Check if rcar_environment path is defined
ifeq ($(RCAR_ENV_PATH), )
  $(info )
  $(info RCAR_ENV_PATH is not set. Please set the rcar_environment directory in the make\user_config.mk file )
  $(error Invalid environment)
endif

#Special handling for Windows compilers
ifeq ($(OS_USED), WINDOWS)
  #Check if compiler path is defined
  ifeq ($(COMPILER_PATH), )
    $(info )
    $(info COMPILER_PATH is not set.)
    $(info Please set the root compiler directory in the make\user_config.mk file)
    $(error Invalid environment)
  endif

  #Get path to GCC compiler
  ifeq ($(COMPILER_TYPE), GNU)
    CC  := $(COMPILER_PATH)/bin/aarch64-linux-gnu-gcc.exe 
    CXX := $(COMPILER_PATH)/bin/aarch64-linux-gnu-g++.exe 
    COMPILER_INC += $(COMPILER_PATH)/aarch64-linux-gnu/include/c++/7.3.1
    COMPILER_INC += $(COMPILER_PATH)/aarch64-linux-gnu/include/c++/7.3.1/aarch64-linux-gnu
    COMPILER_INC += $(COMPILER_PATH)/aarch64-linux-gnu/include/c++/7.3.1/backward
    COMPILER_INC += $(COMPILER_PATH)/lib/gcc/aarch64-linux-gnu/7.3.1/include
    COMPILER_INC += $(COMPILER_PATH)/lib/gcc/aarch64-linux-gnu/7.3.1/include-fixed
    COMPILER_INC += $(COMPILER_PATH)/aarch64-linux-gnu/include
    COMPILER_INC += $(COMPILER_PATH)/aarch64-linux-gnu/libc/usr/include/
  endif
endif

#Special handling for Linux compilers
ifeq ($(OS_USED), LINUX)
  #Verify environment is set up correctly
  ifndef SDKTARGETSYSROOT 
    $(info )
    $(info SDKTARGETSYSROOT was not set.)
    $(info Source environment-setup-aarch64-poky-linux from SDK and try again)
    $(info Searching /opt for environment-setup-aarch64-poky-linux...)
    $(info Results:$(addprefix ${NEWLINE}==> ,$(shell find /opt -maxdepth 3 -name environment-setup-aarch64-poky-linux -type f 2> /dev/null)))
    $(error Invalid environment)
  endif
endif
