include make/common.mk

PCLINT_WARNING_LEVEL   := -w3
PCLINT_POLICY          := -e655 -e613 -e641 -e9132 -e9141 -e9027 -e9147 -e9079 -e925 -e927 -e930 -e9058 -e9087 -e793 -e9045 -e952 -e953 -e954 -e818 -e9059 -e9259 -e9051 -e9117 -e9119 -e726 -e9106 -e970 -e835 -e829 -e9026 -e9057 -e9046 -e731 -e621 -e9071 -e9033 -e9123 -e9133 -e943 -e785 -e9068
PCLINT_OPTIONS         := -u -idlen\(64, p\) -width\(120,4\) $(PCLINT_WARNING_LEVEL) $(PCLINT_POLICY)

#Find all sources
SRC_CVE_C   := $(call rwildcard, source/, *.cv.c)
SRC_C       := $(call rwildcard, source/, *.c)
SRC_C       := $(filter-out $(SRC_CVE_C),$(SRC_C))

#Find all includes
INC         := $(call rwildcard, source/, *.h *.hpp)
INC         += $(call rwildcard, lib/renesas/, *.h *.hpp)
INC         += $(call rwildcard, autogen/, *.h *.hpp)
INC_DIR     := $(sort $(dir $(INC)))
INC_DIR     += $(THIRD_PARTY_LIB)

LINT_OPTION := $(patsubst %.c, $(LINT)/%.lnt, $(SRC_C))
LINT_BAT    := $(patsubst %.c, $(LINT)/%.bat, $(SRC_C))

LINT_OPTION_PROJECT  := $(LINT)/lint_project.lnt
LINT_BAT_PROJECT     := $(LINT)/lint_project.bat

#Check if project path is defined
ifeq ($(PROJECT_PATH), )
    $(info )
    $(info PROJECT_PATH is not set.)
    $(info Please set the directory of the project in the make\user_config.mk file)
    $(error Invalid environment)
endif

.PHONY: default clean

default: $(LINT_OPTION) $(LINT_BAT) $(LINT)/lint_project.lnt $(LINT)/lint_project.bat

clean: 
	$(RM) $(LINT_OPTION) $(LINT_BAT) $(LINT)/lint_project.lnt $(LINT)/lint_project.bat

#Create lint options for C files
$(LINT_OPTION): $(LINT)/%.lnt: %.c
	$(info Lint options: $<)
	$(MKDIR)
	$(ECHO) $(PCLINT_OPTIONS)                           >> $@
	$(ECHO) ""                                          >> $@
	$(ECHO) $(addprefix -i$(PROJECT_PATH)/, $(INC_DIR)) >> $@
	$(ECHO) ""                                          >> $@
	$(ECHO) $(addprefix $(PROJECT_PATH)/, $<)           >> $@

$(LINT_BAT): $(LINT)/%.bat: %.c
	$(info Lint bat: $<)
	$(MKDIR)
	$(ECHO) "call lin > "$(notdir $(patsubst %.bat, %_results.txt, $@)) $(notdir $(patsubst %.bat, %.lnt, $@))  >> $@
	$(ECHO) ""                                                                                             >> $@
	$(ECHO) "rm _LINT.TMP"                                                                                 >> $@  

#Create lint options for whole project
$(LINT)/lint_project.lnt:
	$(info Lint options for whole project:)
	$(MKDIR)
	$(ECHO) $(PCLINT_OPTIONS)                                >> $@
	$(ECHO) ""                                               >> $@
	$(ECHO) $(addprefix -i$(PROJECT_PATH)/, $(INC_DIR))      >> $@
	$(ECHO) ""                                               >> $@
	$(ECHO) $(addprefix $(PROJECT_PATH)/, $(SRC_C))          >> $@

$(LINT)/lint_project.bat:
	$(info Lint bat for whole project:)
	$(MKDIR)
	$(ECHO) "call lin > "$(notdir $(patsubst %.bat, %_results.txt, $@)) $(notdir $(patsubst %.bat, %.lnt, $@))  >>$@
	$(ECHO) ""                                                                                              >> $@
	$(ECHO) "rm _LINT.TMP"                                                                                  >> $@  
