/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
* other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
* EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
* SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
* SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
* this software. By using this software, you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_extatm_ocv.h
* Description  : CV Engine Extended Atomic Library
***********************************************************************************************************************/

/***********************************************************************************************************************
* History : DD.MM.YYYY Version  Description
*         : 15.01.2007 1.00     First Release
***********************************************************************************************************************/

#ifndef R_EXTATM_OCV_H_
#define R_EXTATM_OCV_H_

/***********************************************************************************************************************
   Macro definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
   Typedef definitions
***********************************************************************************************************************/
typedef struct
{
    uint32_t pc_master;
    uint32_t pc_master_sini;
    uint32_t pc_slave;
    uint32_t pc_slave_sini;
    uint32_t clofs_uniform;

} R_EXTATM_OCVFunctionParam;

typedef struct
{
    uint32_t master;
    uint32_t slave;

} R_EXTATM_OCVPhyaddr;

typedef struct
{
    R_EXTATM_OCVPhyaddr phy_addr;        //!< physical address
    uint32_t            clofs_uniform;   //!< cl offset

} R_EXTATM_OCVProgram;

typedef enum
{
    R_EXTATM_OCV_ALL_MASTERS    = 0x000000FF,
    R_EXTATM_OCV_THREAD0_MASTER = 0x00000001,
    R_EXTATM_OCV_THREAD1_MASTER = 0x00000002,
    R_EXTATM_OCV_THREAD2_MASTER = 0x00000004,
    R_EXTATM_OCV_THREAD3_MASTER = 0x00000008,
    R_EXTATM_OCV_THREAD4_MASTER = 0x00000010,
    R_EXTATM_OCV_THREAD5_MASTER = 0x00000020,
    R_EXTATM_OCV_THREAD6_MASTER = 0x00000040,
    R_EXTATM_OCV_THREAD7_MASTER = 0x00000080,
    R_EXTATM_OCV_THREAD0_SLAVE  = 0x00010000,
    R_EXTATM_OCV_THREAD1_SLAVE  = 0x00020000,
    R_EXTATM_OCV_THREAD2_SLAVE  = 0x00040000,
    R_EXTATM_OCV_THREAD3_SLAVE  = 0x00080000,
    R_EXTATM_OCV_THREAD4_SLAVE  = 0x00100000,
    R_EXTATM_OCV_THREAD5_SLAVE  = 0x00200000,
    R_EXTATM_OCV_THREAD6_SLAVE  = 0x00400000,
    R_EXTATM_OCV_THREAD7_SLAVE  = 0x00800000,

    /* Check for wrong values */
    R_EXTATM_OCV_MASTER_SLAVE_ERROR = 0xFF00FF00

} R_EXTATM_OCVMasterSlaveMode;

typedef struct
{
    R_EXTATM_OCVMasterSlaveMode mode;
    uint32_t                    clofs_uniform;

} R_EXTATM_OCVMasterSlaveParam;

typedef struct
{
    union
    {
        struct
        {
            uint32_t y : 16; /* pixels */
            uint32_t x : 16; /* pixels */

        } xy_ofs;
        uint32_t byte_ofs; /* bytes */

    } src_ofs;

    uint32_t dst_offset; /* bytes  */
    uint32_t gwm_stride; /* bytes  */

    union
    {
        struct
        {
            uint32_t y_length : 16; /* height */
            uint32_t x_length : 16; /* Width */

        } xy;
        uint32_t xy_length; /* bytes */

    } len;

    union
    {
        struct
        {
            uint32_t dmac_exe : 1;   /* DMA Exe */
            uint32_t dma_status : 1; /* Reserved */
            uint32_t : 1;            /* Reserved */
            uint32_t wait : 1;       /* Sync */
            uint32_t plane_id : 3;   /* plane sel */
            uint32_t : 1;            /* Reserved */
            uint32_t direction : 1;  /* transfer direction */
            uint32_t : 2;            /* Reserved */
            uint32_t mode : 1;       /* Selection Mode */
            uint32_t RB : 2;         /* always 1 */
            uint32_t DMR : 18;       /* Reserved */

        } par;
        uint32_t value;

    } dmaconf;

    uint32_t clofs_addr; /* cl offset address*/

} R_EXTATM_OCVDmacGwmConfParam;   //Dmac Global working Memory configuration parameter

/***********************************************************************************************************************
   Exported global variables
***********************************************************************************************************************/

/***********************************************************************************************************************
   Exported global functions (to be accessed by other files)
***********************************************************************************************************************/
int32_t r_extatm_OCV_RegisterPlane(R_ATMLIB_CLData*        cldata,
                                   R_ATMLIB_OCVPlaneParam* plane,
                                   uint8_t                 index);
int32_t r_extatm_OCV_SetUniform(R_ATMLIB_CLData*          cldata,
                                R_ATMLIB_OCVUniformParam* uniform);
int32_t r_extatm_OCV_SetProgramAddress(R_ATMLIB_CLData*     cldata,
                                       R_EXTATM_OCVProgram* program);
int32_t r_extatm_OCV_SetFunctionOffsets(R_ATMLIB_CLData*           cldata,
                                        R_EXTATM_OCVFunctionParam* param);
int32_t r_extatm_OCV_SetMasterSlaveMode(R_ATMLIB_CLData*              cldata,
                                        R_EXTATM_OCVMasterSlaveParam* param);
int32_t r_extatm_OCV_SetDmacGwmConfReg(R_ATMLIB_CLData*              cldata,
                                       R_EXTATM_OCVDmacGwmConfParam* param);
#endif /* R_EXTATM_OCV_H_ */
