/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
* other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
* EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
* SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
* SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
* this software. By using this software, you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_extatm_ocv.c
* Description  : CV Engine Extended Atomic Library
***********************************************************************************************************************/

/**********************************************************************************************************************
 * History : DD.MM.YYYY Version  Description
 *         : 24.08.2017 1.00     First Release
 ***********************************************************************************************************************/

/***********************************************************************************************************************
   Includes   <System Includes> , "Project Includes"
***********************************************************************************************************************/
#include <stdint.h>
#include "r_atmlib_def.h"
#include "r_atmlib_prot.h"
#include "r_atmlib_types.h"
#include "r_impx5p_reg_ocv.h"
#include "r_extatm_ocv.h"

/***********************************************************************************************************************
   Macro definitions
***********************************************************************************************************************/
#define R_EXTATM_NB_PLANE (8)
#define EXTATM_NULL       ((void*)0)

#define R_ATMLIB_IMG_16U  (5U)

/***********************************************************************************************************************
   Typedef definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
   Exported global variables (to be accessed by other files)
***********************************************************************************************************************/

/***********************************************************************************************************************
   Private global variables and functions
***********************************************************************************************************************/

/* Array of addresses for the Plane configuration registers */
const uint32_t plane_reg[R_EXTATM_NB_PLANE] = {
    R_IMPX5P_REG_OCV_IMGBAR0,
    R_IMPX5P_REG_OCV_IMGBAR1,
    R_IMPX5P_REG_OCV_IMGBAR2,
    R_IMPX5P_REG_OCV_IMGBAR3,
    R_IMPX5P_REG_OCV_IMGBAR4,
    R_IMPX5P_REG_OCV_IMGBAR5,
    R_IMPX5P_REG_OCV_IMGBAR6,
    R_IMPX5P_REG_OCV_IMGBAR7
};

/***********************************************************************************************************************
* Function Name: r_extatm_OCV_RegisterPlane
* Description  : Adds a plane configuration into a Command List for CVe.
* Arguments    : cldata -
*                    Pointer to the Command List structure
*                offset -
*                    Where to write the commands. -1 to add at the end of the CL
*                plane -
*                        Pointer to a plane parameters structure
*                index -
*                        Number of the plane, should be between 0 and 7
* Return Value : ret -
*                    0 if the CL has been correctly updated. -1 if something went wrong.
***********************************************************************************************************************/
int32_t r_extatm_OCV_RegisterPlane(R_ATMLIB_CLData*cldata, R_ATMLIB_OCVPlaneParam* plane, uint8_t index)
{
    uint32_t data[3];
    int32_t ret;

    /* Check for NULL pointers */
    if ((EXTATM_NULL == cldata) || (EXTATM_NULL == plane))
    {
        return -1;
    }

    /* Only set correct planes (between 0 and 7) */
    if (index >= R_EXTATM_NB_PLANE)
    {
        return -1;
    }

    /* Configure the plane */
    data[0] = plane->img_addr;   //!< physical address
    data[1] = plane->img_stride; //!< stride

    /* Float to Integer Format Conversion Round Mode*/
    data[2] = 0x00000100; //!< 1: IEEE round to zero

    /* Pixel Format for unified thread(master and slave) */
    switch (plane->img_type)
    {
        case R_ATMLIB_IMG_8S:
            data[2] |= 0x00010001U;
            break;

        case R_ATMLIB_IMG_8U:
            data[2] |= 0x00000000U;
            break;

        case R_ATMLIB_IMG_16S:
            data[2] |= 0x00030003U;
            break;

        case R_ATMLIB_IMG_16U:
            data[2] |= 0x00020002U;
            break;

        case R_ATMLIB_IMG_32S:
            data[2] |= 0x00050005U;
            break;

        case R_ATMLIB_IMG_BNR: /* BNR is Float */
            data[2] |= 0x00070007U;
            break;

        default:
            break;

    }

    /* Update the relative address of the plane in the Atomic Lib (for future patching) */
    /* TODO This had to be updated to work */
    plane->clofs_img_addr = (uint32_t)(cldata->cur_addr - cldata->top_addr + 1);

    /* Write the configuration at the correct registers */
    ret = r_atmlib_OCV_WPR(cldata, plane_reg[index], 3, data);

    return ret;
}                  /* End of function r_extatm_OCV_RegisterPlane() */

/***********************************************************************************************************************
* Function Name: r_extatm_OCV_SetUniform
* Description  : Adds the uniform memory configuration to a Command List
* Arguments    : cldata -
*                    Pointer to the Command List structure
*                offset -
*                    Where to write the commands. -1 to add at the end of the CL
*                uniform -
*                        Pointer to a uniform parameters structure
* Return Value : ret -
*                    0 if the CL has been correctly updated. -1 if something went wrong.
***********************************************************************************************************************/
int32_t r_extatm_OCV_SetUniform(R_ATMLIB_CLData* cldata, R_ATMLIB_OCVUniformParam* uniform)
{
    int32_t ret;

    /* Check for NULL pointers */
    if ((EXTATM_NULL == cldata) || (EXTATM_NULL == uniform))
    {
        return -1;
    }

    /* Check for uniform size and null data in uniform parameter */
    if ((uniform->size < 1) || (uniform->size > 64) || (EXTATM_NULL == uniform->data))
    {
        return -1;
    }

    /* Update the relative address of uniform in the Atomic Lib (for future patching)*/
    /* TODO This had to be updated to work */
    uniform->clofs_uniform = (uint32_t)(cldata->cur_addr - cldata->top_addr + 1);

    ret = r_atmlib_OCV_WPR(cldata, R_IMPX5P_REG_OCV_UNR0, uniform->size, uniform->data);

    return ret;
}                  /* End of function r_extatm_OCV_SetUniform() */

/***********************************************************************************************************************
* Function Name: r_extatm_OCV_SetProgramAddress
* Description  : Configures the Program Address used for future RECT, POINT, POINTS or SINI commands
* Arguments    : cldata -
*                    Pointer to the Command List structure
*                offset -
*                    Where to write the commands. -1 to add at the end of the CL
*                program_addr -
*                        Physical address where to find the program
* Return Value : ret -
*                    0 if the CL has been correctly updated. -1 if something went wrong.
***********************************************************************************************************************/
int32_t r_extatm_OCV_SetProgramAddress(R_ATMLIB_CLData* cldata, R_EXTATM_OCVProgram* program)
{
    int32_t ret;
    uint32_t data[2];

    /* Check for NULL pointers */
    if ((EXTATM_NULL == cldata) || (EXTATM_NULL == program))
    {
        return -1;
    }

    /* Vs I$ Base Address Register (VIBAR) set for master thread */
    data[0] = program->phy_addr.master;

    /* Ps I$ Base Address Register (PIBAR) set for slave thread */
    data[1] = program->phy_addr.slave;

    /* Update the relative address of uniform in the Atomic lib (for future patching)*/
    /* TODO This had to be updated to work */
    program->clofs_uniform = (uint32_t)(cldata->cur_addr - cldata->top_addr + 1);

    if (data[1] == 0x0000)
    {
        ret = r_atmlib_OCV_WPR(cldata, R_IMPX5P_REG_OCV_VIBAR, 1, data);
    }
    else
    {
        ret = r_atmlib_OCV_WPR(cldata, R_IMPX5P_REG_OCV_VIBAR, 2, data);
    }

    return ret;
}                  /* End of function r_extatm_OCV_SetProgramAddress() */

/***********************************************************************************************************************
* Function Name: r_extatm_OCV_SetFunctionOffsets
* Description  : Configures the function offset used for other commands
* Arguments    : cldata -
*                    Pointer to the Command List structure
*                offset -
*                    Where to write the commands. -1 to add at the end of the CL
*                param -
*                      OCV function parameter
* Return Value : ret -
*                    0 if the CL has been correctly updated. -1 if something went wrong.
***********************************************************************************************************************/
int32_t r_extatm_OCV_SetFunctionOffsets(R_ATMLIB_CLData*cldata, R_EXTATM_OCVFunctionParam* param)
{
    int32_t ret;
    uint32_t data[4];

    /* Check for NULL pointers */
    if ((EXTATM_NULL == cldata) || (EXTATM_NULL == param))
    {
        return -1;
    }

    /* Master PC start address register */
    data[0] = param->pc_master;     //!< VPCSAR
    data[1] = param->pc_master_sini; //!< VSINISAR

    /* Slave PC start address register */
    data[2] = param->pc_slave;    //!< PPCSAR
    data[3] = param->pc_slave_sini; //!< PSINISAR
    
    /* TODO This had to be updated to work */
    param->clofs_uniform = (uint32_t)(cldata->cur_addr - cldata->top_addr + 1);

    /* Add the register configuration in Vs PC Start Address Register */
    ret = r_atmlib_OCV_WPR(cldata, R_IMPX5P_REG_OCV_VPCSAR, 4, data);

    return ret;
}                  /* End of function r_extatm_OCV_SetProgramAddress() */

/***********************************************************************************************************************
* Function Name: r_extatm_OCV_SetMasterSlaveMode
* Description  : Configures the Program Address used for future RECT, POINT, POINTS or SINI commands
* Arguments    : cldata -
*                    Pointer to the Command List structure
*                offset -
*                    Where to write the commands. -1 to add at the end of the CL
*                param -
*                        Flags for either Master or Slave mode for each thread. Can be piped.
* Return Value : ret -
*                    0 if the CL has been correctly updated. -1 if something went wrong.
***********************************************************************************************************************/
int32_t r_extatm_OCV_SetMasterSlaveMode(R_ATMLIB_CLData*cldata, R_EXTATM_OCVMasterSlaveParam* param)
{
    int32_t ret;
    uint32_t data[1];

    /* Check for NULL pointers */ ///* Check for illegal bit value */
    if ((EXTATM_NULL == cldata) || (EXTATM_NULL == param))
    {
        return -1;
    }

    if (param->mode & R_EXTATM_OCV_MASTER_SLAVE_ERROR)
    {
        return -1;
    }

    /* Check for master + slave in same thread */
    if (
        ((param->mode & R_EXTATM_OCV_THREAD0_MASTER) && (param->mode & R_EXTATM_OCV_THREAD0_SLAVE)) ||
        ((param->mode & R_EXTATM_OCV_THREAD1_MASTER) && (param->mode & R_EXTATM_OCV_THREAD1_SLAVE)) ||
        ((param->mode & R_EXTATM_OCV_THREAD2_MASTER) && (param->mode & R_EXTATM_OCV_THREAD2_SLAVE)) ||
        ((param->mode & R_EXTATM_OCV_THREAD3_MASTER) && (param->mode & R_EXTATM_OCV_THREAD3_SLAVE)) ||
        ((param->mode & R_EXTATM_OCV_THREAD4_MASTER) && (param->mode & R_EXTATM_OCV_THREAD4_SLAVE)) ||
        ((param->mode & R_EXTATM_OCV_THREAD5_MASTER) && (param->mode & R_EXTATM_OCV_THREAD5_SLAVE)) ||
        ((param->mode & R_EXTATM_OCV_THREAD6_MASTER) && (param->mode & R_EXTATM_OCV_THREAD6_SLAVE)) ||
        ((param->mode & R_EXTATM_OCV_THREAD7_MASTER) && (param->mode & R_EXTATM_OCV_THREAD7_SLAVE))
        )
    {
        return -1;
    }

    /* TODO This had to be updated to work */
    param->clofs_uniform = (uint32_t)(cldata->cur_addr - cldata->top_addr + 1);

    /* Add the register configuration in TG Shader Enable Register1 */
    data[0] = param->mode;
    ret     = r_atmlib_OCV_WPR(cldata, R_IMPX5P_REG_OCV_TGSEN1, 1, data);

    return ret;
}                  /* End of function r_extatm_OCV_SetMasterSlaveMode() */

/***********************************************************************************************************************
* Function Name: r_extatm_OCV_SetDmacGwmConfReg
* Description  : Configures the Program Address used for future RECT, POINT, POINTS or SINI commands
* Arguments    : cldata -
*                    Pointer to the Command List structure
*                param -
*                        DMAC GWM configuration parameter.
* Return Value : ret -
*                    0 if the CL has been correctly updated. -1 if something went wrong.
***********************************************************************************************************************/
int32_t r_extatm_OCV_SetDmacGwmConfReg(R_ATMLIB_CLData*cldata, R_EXTATM_OCVDmacGwmConfParam* param)
{
    int32_t ret;
    uint32_t data[4];

    /* Check for NULL pointers */
    if ((EXTATM_NULL == cldata) || (EXTATM_NULL == param))
    {
        return -1;
    }

    /* Prepare configuration in DMAC-GWM Register */

    /* DMAC external memory offset address set (DMEXTMEMOFSADDR)*/
    data[0] = param->src_ofs.byte_ofs;

    /* DMAC GWM destination offset set (DMGWMBADDR) */
    data[1] = param->dst_offset;

    /* DMAC GWM stride set (DMGWMSTRR) */
    data[2] = param->gwm_stride;

    /* DMAC processing length set (DMLNGR) */
    if ((0 < param->len.xy.x_length) && (param->len.xy.x_length <= 0x1000) && (0 < param->len.xy.y_length) && (param->len.xy.y_length <= 0x1000))
    {
        data[3] = param->len.xy_length;
    }
    else
    {
        return -1;
    }

    ret = r_atmlib_OCV_WPR(cldata, R_IMPX5P_REG_OCV_DMEXTMEMOFSADDR, 4, data);

    /* Add configuration in DMac Control Register 0 (DMCR0)*/
    param->dmaconf.par.RB         = 3;   //!< Reserved bits
    param->dmaconf.par.dma_status = 0;   //!< Reserved status bit
    param->dmaconf.par.dmac_exe   = 1;   //!< DMAC conf + execution activation
    param->dmaconf.par.DMR        = 0x0000; //!< Reserved bits assign 0

    /* DMAC register complete value */
    data[0] = param->dmaconf.value;

    /* Update the relative address of the plane in the Atomic Lib (for future patching) */
    /* TODO This had to be updated to work */
    param->clofs_addr = (uint32_t)(cldata->cur_addr - cldata->top_addr + 1);;

    ret     = r_atmlib_OCV_WPR(cldata, R_IMPX5P_REG_OCV_DMCR0, 1, data);

    return ret;
}                  /* End of function r_extatm_OCV_SetDmacGwmConfigSet() */

//EOF
