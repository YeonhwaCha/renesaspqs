/******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized. This
 * software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
 * LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software
 * and to discontinue the availability of this software. By using this software,
 * you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 ******************************************************************************/

#ifndef HELPER_LUT_MAPPING_H_
#define HELPER_LUT_MAPPING_H_




int __map_LUT_delta(void* mapFuncData, float x, float y, float *u, float *v);

int __map_LUT(void* mapFuncData, float x, float y, float *u, float *v);

int __map_LUT_420(void* mapFuncData, float x, float y, float *u, float *v);

int __map_LUT_bicubic(void* mapFuncData, float x, float y, float *u, float *v);

int __map_LUT_bicubic_delta(void* mapFuncData, float x, float y, float *u, float *v);

int __map_LUT_bicubic_420(void* mapFuncData, float x, float y, float *u, float *v);

int __map_LUT_bicubic_420422(void* mapFuncData, float x, float y, float *u, float *v);

int __map_LUT_bicubic_420transp(void* mapFuncData, float x, float y, float *u, float *v);


int readBinaryLUT(const char* fname);

int readASCIILUT(const char* fname, int width, int height, int inverse);

int setupLUT(const float* lutdata, int width, int height, int lutstep);
int setupLUTNoCopy(float* lutdata, int width, int height, int lutstep);

void saveLUTAsHeader(const char* fname);

void setLUTStep(int lutstep);
int getLUTWidth();
int getLUTHeight();

void setupMappingOffsets(int ofs_x, int ofs_y, int ofs_u, int ofs_v);

#endif /* HELPER_LUT_MAPPING_H_ */
