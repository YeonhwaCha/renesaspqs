/******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized. This
 * software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
 * LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software
 * and to discontinue the availability of this software. By using this software,
 * you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 ******************************************************************************/

#ifndef HELPER_OPENCV_LENS_H_
#define HELPER_OPENCV_LENS_H_


/**
 * Parameters for undistortion
 */
typedef struct undistort_params {
    float k1,k2,k3; // radial distortion parameters
    float fx,fy;    // focal length ratio
    float cx,cy;    // optical center
    float sx,sy;    // prescaling factors (source.width/dest.width and height)
    float ox,oy;    // offset
    float fs;       // focal lenght scale factor for fisheye lenses
} undistort_params_t;

typedef struct homography_params {
    float h00,h01,h02,h10,h11,h12,h20,h21,h22; // mohography matrix row-column order
} homography_params_t;




void setup_IMI_RDACM20_lens_params(undistort_params_t* mapFuncData, int srcWidth, int srcHeight, int dstWidth, int dstHeight);

void setup_opencv_lens_params(undistort_params_t* mapFuncData,
        float scaleX, float scaleY, float centerX, float centerY,
        float focalX, float focalY, float k1, float k2, float k3);

int map_opencv_undist(void* mapFuncData, float x, float y, float *u, float *v);
int map_opencv_undist_left_half(void* mapFuncData, float x, float y, float *u, float *v);
int map_opencv_undist_right_half(void* mapFuncData, float x, float y, float *u, float *v);
int map_opencv_undist_generic_model(void *usrData, float x, float y, float *X, float *Y);
int map_opencv_undist_fisheye_model(void *usrData, float x, float y, float *X, float *Y);
int map_opencv_scale(void *usrData, float x, float y, float *X, float *Y);

int map_opencv_scale_shift(void *usrData, float x, float y, float *X, float *Y);
int map_opencv_homography(void *usrData, float x, float y, float *X, float *Y);

#endif /* HELPER_OPENCV_LENS_H_ */
