/******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized. This
 * software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
 * LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software
 * and to discontinue the availability of this software. By using this software,
 * you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 ******************************************************************************/


#include "opencv_lens.h"

#include <math.h>
#include <stdio.h>



/**
 * Sets up Lens Parameters as measured for IMI RDACM20
 * and sets the scaling parameters (source/dest).
 */

void setup_IMI_RDACM20_lens_params(undistort_params_t* lensParam, int srcWidth, int srcHeight, int dstWidth, int dstHeight)
{
    if (!lensParam)
    {
        return;
    }
    /* distortion matrix calculated with OCV */
    lensParam->k1=-5.59601058e-02;
    lensParam->k2=9.61266173e-02;
    lensParam->k3=-1.51595983e-02;

    /* camera matrix calculated with OCV */
    /* Values valid for 1280 x 800 px Camera image => Normalize accordingly */

    float sx = ((float)srcWidth)/1280.0f;
    float sy = ((float)srcHeight)/800.0f;

    //printf(" sx %g sy %g\n",sx,sy);
    lensParam->fx=335.58559971*sx; // / 640.0;
    lensParam->cx=639.93001485*sx; // / 1280.0; // cx should be near to 0.5
    lensParam->fy=339.81981557*sy; // / 400.0;
    lensParam->cy=390.19524606*sy; // / 800.0;  // cy should be near to 0.5

    /* Scaling */
    lensParam->sx = ((float)srcWidth)/((float)dstWidth);
    lensParam->sy = ((float)srcHeight)/((float)dstHeight);
}





/**
 *
 */
void setup_opencv_lens_params(undistort_params_t* lensParam,
        float scaleX, float scaleY,
        float centerX, float centerY,
        float focalX, float focalY, float k1, float k2, float k3)
{
    if (!lensParam)
    {
        return;
    }
    lensParam->sx = scaleX;
    lensParam->sy = scaleY;
    lensParam->cx = centerX;
    lensParam->cy = centerY;
    lensParam->fx = focalX;
    lensParam->fy = focalY;
    lensParam->k1 = k1;
    lensParam->k2 = k2;
    lensParam->k3 = k3;
}



/**
 * Applies lens undistortion as set up using parameters calculated with OpenCV Calibration toolbox.
 */

int map_opencv_undist(void *usrData, float x, float y, float *X, float *Y)
{
    undistort_params_t* lensParam = (undistort_params_t*)usrData;
    if (!lensParam) return 1;
    float a = (x*lensParam->sx-lensParam->cx)/lensParam->fx;
    float b = (y*lensParam->sy-lensParam->cy)/lensParam->fy;
    float theta = atan(sqrt(a * a + b * b));
    float theta2 = theta * theta;
    float theta4 = theta2 * theta2;
    float theta6 = theta4 * theta2;
    float divider = sqrt(a*a+b*b);
    if (divider==0.0) divider = 1.0; // avoid NaN
    float temp0 = theta * (1 + lensParam->k1 * theta2 + lensParam->k2 * theta4 + lensParam->k3 * theta6);
    float corr_x = (a * temp0) /divider * lensParam->fx + lensParam->cx; // k1/5 , or not ???
    float corr_y = (b * temp0) /divider* lensParam->fy + lensParam->cy;
    *X = corr_x;
    *Y = corr_y;
    return 0;
}

#define X_SCALE 2
#define X_OFFSET 400

int map_opencv_undist_left_half(void *usrData, float x, float y, float *X, float *Y)
{
    undistort_params_t* lensParam = (undistort_params_t*)usrData;
    if (!lensParam) return 1;
    float a = (x*lensParam->sx-lensParam->cx)/lensParam->fx;
    float b = (y*lensParam->sy-lensParam->cy)/lensParam->fy;
    float theta = atan(sqrt(a * a + b * b));
    float theta2 = theta * theta;
    float theta4 = theta2 * theta2;
    float theta6 = theta4 * theta2;
    float divider = sqrt(a*a+b*b);
    if (divider==0.0) divider = 1.0; // avoid NaN
    float temp0 = theta * (1 + lensParam->k1 * theta2 + lensParam->k2 * theta4 + lensParam->k3 * theta6);
    float corr_x = (a * temp0) /divider * lensParam->fx + lensParam->cx; // k1/5 , or not ???
    float corr_y = (b * temp0) /divider* lensParam->fy + lensParam->cy;
    *X = corr_x*X_SCALE;
    *Y = corr_y;
    return 0;
}

int map_opencv_undist_right_half(void *usrData, float x, float y, float *X, float *Y)
{
    undistort_params_t* lensParam = (undistort_params_t*)usrData;
    if (!lensParam) return 1;
    float a = (x*lensParam->sx-lensParam->cx)/lensParam->fx;
    float b = (y*lensParam->sy-lensParam->cy)/lensParam->fy;
    float theta = atan(sqrt(a * a + b * b));
    float theta2 = theta * theta;
    float theta4 = theta2 * theta2;
    float theta6 = theta4 * theta2;
    float divider = sqrt(a*a+b*b);
    if (divider==0.0) divider = 1.0; // avoid NaN
    float temp0 = theta * (1 + lensParam->k1 * theta2 + lensParam->k2 * theta4 + lensParam->k3 * theta6);
    float corr_x = (a * temp0) /divider * lensParam->fx + lensParam->cx; // k1/5 , or not ???
    float corr_y = (b * temp0) /divider* lensParam->fy + lensParam->cy;
    *X = corr_x*X_SCALE-X_OFFSET;
    *Y = corr_y;
    return 0;
}

int map_opencv_scale(void *usrData, float x, float y, float *X, float *Y)
{
    undistort_params_t* lensParam = (undistort_params_t*)usrData;
    if (!lensParam) return 1;
    float corr_x = (int)x*lensParam->sx;
    float corr_y = (int)y*lensParam->sy;
    *X = corr_x;
    *Y = corr_y;
    return 0;
}

int map_opencv_scale_shift(void *usrData, float x, float y, float *X, float *Y)
{
    undistort_params_t* lensParam = (undistort_params_t*)usrData;
    if (!lensParam) return 1;
    float corr_x = (int)x*lensParam->sx + lensParam->ox;
    float corr_y = (int)y*lensParam->sy + lensParam->oy;
    *X = corr_x;
    *Y = corr_y;
    return 0;
}

int map_opencv_undist_generic_model(void *usrData, float x, float y, float *X, float *Y)
{
    undistort_params_t* lensParam = (undistort_params_t*)usrData;
    if (!lensParam) return 1;
    float x_ = (x*lensParam->sx-lensParam->cx)/lensParam->fx;
    float y_ = (y*lensParam->sy-lensParam->cy)/lensParam->fy;
    float r2 = x_*x_ + y_*y_;
    float r4 = r2*r2;
    float r6 = r4*r2;
    float x__ = x_*(1 + lensParam->k1*r2 + lensParam->k2*r4 + lensParam->k3*r6);
    float y__ = y_*(1 + lensParam->k1*r2 + lensParam->k2*r4 + lensParam->k3*r6);
    float corr_x = lensParam->fx*x__ + lensParam->cx; // k1/5 , or not ???
    float corr_y = lensParam->fy*y__ + lensParam->cy;
    *X = corr_x;
    *Y = corr_y;
    return 0;
}

int map_opencv_undist_fisheye_model(void *usrData, float x, float y, float *X, float *Y)
{

    undistort_params_t* lensParam = (undistort_params_t*)usrData;
    float scale_ratio = lensParam->fs;
    if (!lensParam) return 1;
    
    float x_ = -((lensParam->cx/lensParam->sx)/(lensParam->fx*scale_ratio)) + x*(1/(lensParam->fx*scale_ratio));
    float y_ = y*(1/(lensParam->fy*scale_ratio)) -((lensParam->cy/lensParam->sy)/(lensParam->fy*scale_ratio));
    float w_ = 1.0;
    float scale = 0.0;

    float theta = atan(sqrt(x_ * x_ + y_ * y_));
    float theta2 = theta*theta;
    float theta4 = theta2*theta2;
    float theta6 = theta4*theta2;
    float theta8 = theta4*theta4;
    float temp0 = theta * (1 + lensParam->k1 * theta2 + lensParam->k2 * theta4 + lensParam->k3 * theta6);
    float divider = sqrt(x_ * x_ + y_ * y_);
    if (divider==0.0) divider = 1.0; // avoid NaN
        scale = temp0 / divider;

    *X = lensParam->fx*x_*scale + lensParam->cx;
    *Y = lensParam->fy*y_*scale + lensParam->cy;


    /*float x_ = (x*lensParam->sx-lensParam->cx)/lensParam->fx;
    float y_ = (y*lensParam->sy-lensParam->cy)/lensParam->fy;
    float r2 = x_*x_ + y_*y_;
    float r4 = r2*r2;
    float r6 = r4*r2;
    float x__ = x_*(1 + lensParam->k1*r2 + lensParam->k2*r4 + lensParam->k3*r6);
    float y__ = y_*(1 + lensParam->k1*r2 + lensParam->k2*r4 + lensParam->k3*r6);
    float corr_x = lensParam->fx*x__ + lensParam->cx; // k1/5 , or not ???
    float corr_y = lensParam->fy*y__ + lensParam->cy;
    *X = corr_x;
    *Y = corr_y;*/
    return 0;
}

int map_opencv_homography(void *usrData, float x, float y, float *X, float *Y)
{
    homography_params_t* homographyParam = (homography_params_t*)usrData;
    if (!homographyParam) return 1;

    float temp_x, temp_y, temp_z;

    temp_x = homographyParam->h00 * x + homographyParam->h01 * y + homographyParam->h02 * 1.0;
    temp_y = homographyParam->h10 * x + homographyParam->h11 * y + homographyParam->h12 * 1.0;
    temp_z = homographyParam->h20 * x + homographyParam->h21 * y + homographyParam->h22 * 1.0;

    if(temp_z)
    {
        temp_x = temp_x/temp_z;
        temp_y = temp_y/temp_z;
    }
    else
        return 1;
    *X = temp_x;
    *Y = temp_y;
    return 0;
}
