/******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized. This
 * software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
 * LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software
 * and to discontinue the availability of this software. By using this software,
 * you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
 ******************************************************************************/



#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

static float * LUT=NULL;
static int LUTWidth=0,LUTHeight=0;
static int LUTStep=1;


static int    Offset_u = 0;
static int    Offset_v = 0;
static int    Offset_X = 0;
static int    Offset_Y = 0;


/**
 * Maps mesh using LUT containing delta values
 * @param x Destination coordinate
 * @param y Destination vertical component
 * @param u Source (Texture) coordinate
 * @param v Source (Texture) coordinate
 * @return 0 on success, other on error
 *
 * This code \e requires the LUT to have the same
 * width and height as the output image.
 * Anyway, fractional part is already used for bilinear
 * interpolation, so extension to general case is straight forward.
 */
int __map_LUT_delta(void* mapFuncData, float x, float y, float *u, float *v)
{
	int x0,y0,x1,y1;
	float lx,ly;
	if (LUTStep>1) {
		lx = x/LUTStep+Offset_X;
		ly = y/LUTStep+Offset_Y;
	} else {
		lx = x+Offset_X;
		ly = y+Offset_Y;
	}
	x0 = floor(lx);
	y0 = floor(ly);
	x1 = floor(lx+1.0f);
	y1 = floor(ly+1.0f);
	if (x0<0) x0=0;
	if (x0>LUTWidth-1) x0=LUTWidth-1;
	if (x1<0) x1=0;
	if (x1>LUTWidth-1) x1=LUTWidth-1;
	if (y0<0) y0=0;
	if (y0>LUTHeight-1) y0=LUTHeight-1;
	if (y1<0) y1=0;
	if (y1>LUTHeight-1) y1=LUTHeight-1;
	float ax = lx-(float)x0;
	float ay = ly-(float)y0;
	float u0 = ax*LUT[(y0*LUTWidth+x0)*2+0]+(1.0-ax)*LUT[(y0*LUTWidth+x1)*2+0];
	float v0 = ax*LUT[(y0*LUTWidth+x0)*2+1]+(1.0-ax)*LUT[(y0*LUTWidth+x1)*2+1];
	float u1 = ax*LUT[(y1*LUTWidth+x0)*2+0]+(1.0-ax)*LUT[(y1*LUTWidth+x1)*2+0];
	float v1 = ax*LUT[(y1*LUTWidth+x0)*2+1]+(1.0-ax)*LUT[(y1*LUTWidth+x1)*2+1];
	*u = (x + ay*u0 + (1.0f-ay)*u1)-Offset_u;
	*v = (y + ay*v0 + (1.0f-ay)*v1)-Offset_v;
    return 0;
}


/**
 * Maps mesh using LUT containing u/v values
 * @param x Destination coordinate
 * @param y Destination vertical component
 * @param u Source (Texture) coordinate
 * @param v Source (Texture) coordinate
 * @return 0 on success, other on error
 *
 * This code \e requires the LUT to have the same
 * width and height as the output image.
 * Anyway, fractional part is already used for bilinear
 * interpolation, so extension to general case is straight forward.
 */
int __map_LUT(void* mapFuncData, float x, float y, float *u, float *v)
{
	int x0,y0,x1,y1;
	float lx,ly;
	if (LUTStep>1) {
		lx = x/LUTStep+Offset_X;
		ly = y/LUTStep+Offset_Y;
	} else {
		lx = x+Offset_X;
		ly = y+Offset_Y;
	}
	x0 = floor(lx);
	y0 = floor(ly);
	x1 = floor(lx+1.0f);
	y1 = floor(ly+1.0f);
    if (x0<0) x0=0;
    if (x0>LUTWidth-1) x0=LUTWidth-1;
    if (x1<0) x1=0;
    if (x1>LUTWidth-1) x1=LUTWidth-1;
    if (y0<0) y0=0;
    if (y0>LUTHeight-1) y0=LUTHeight-1;
    if (y1<0) y1=0;
    if (y1>LUTHeight-1) y1=LUTHeight-1;
    float ax = lx-x0;
    float ay = ly-y0;
    float u0 = ax*LUT[(y0*LUTWidth+x0)*2+0]+(1.0-ax)*LUT[(y0*LUTWidth+x1)*2+0];
    float v0 = ax*LUT[(y0*LUTWidth+x0)*2+1]+(1.0-ax)*LUT[(y0*LUTWidth+x1)*2+1];
    float u1 = ax*LUT[(y1*LUTWidth+x0)*2+0]+(1.0-ax)*LUT[(y1*LUTWidth+x1)*2+0];
    float v1 = ax*LUT[(y1*LUTWidth+x0)*2+1]+(1.0-ax)*LUT[(y1*LUTWidth+x1)*2+1];
    *u = (ay*u0 + (1.0-ay)*u1)-Offset_u;
    *v = (ay*v0 + (1.0-ay)*v1)-Offset_v;
    return 0;
}







/**
 * Maps mesh using LUT containing u/v values for 420 downsampled UV data
 * @param x Destination coordinate
 * @param y Destination vertical component
 * @param u Source (Texture) coordinate
 * @param v Source (Texture) coordinate
 * @return 0 on success, other on error
 *
 * This code \e requires the LUT to have double
 * height as the output image, V coordinates should be for double height, too.
 * This enables to use the same LUT for UV8 and Y when using 420 subsampling.
 */

int __map_LUT_420(void* mapFuncData, float ox, float oy, float *u, float *v)
{
    float x = ox;
    float y = oy*2.0; // Scale up
	int x0,y0,x1,y1;
	float lx,ly;
	if (LUTStep>1) {
		lx = x/LUTStep+Offset_X;
		ly = y/LUTStep+Offset_Y;
	} else {
		lx = x+Offset_X;
		ly = y+Offset_Y;
	}
	x0 = floor(lx);
	y0 = floor(ly);
	x1 = floor(lx+1.0f);
	y1 = floor(ly+1.0f);
    if (x0<0) x0=0;
    if (x0>LUTWidth-1) x0=LUTWidth-1;
    if (x1<0) x1=0;
    if (x1>LUTWidth-1) x1=LUTWidth-1;
    if (y0<0) y0=0;
    if (y0>LUTHeight-1) y0=LUTHeight-1;
    if (y1<0) y1=0;
    if (y1>LUTHeight-1) y1=LUTHeight-1;
    float ax = lx-x0;
    float ay = ly-y0;
    float u0 = ax*LUT[(y0*LUTWidth+x0)*2+0]+(1.0-ax)*LUT[(y0*LUTWidth+x1)*2+0];
    float v0 = ax*LUT[(y0*LUTWidth+x0)*2+1]+(1.0-ax)*LUT[(y0*LUTWidth+x1)*2+1];
    float u1 = ax*LUT[(y1*LUTWidth+x0)*2+0]+(1.0-ax)*LUT[(y1*LUTWidth+x1)*2+0];
    float v1 = ax*LUT[(y1*LUTWidth+x0)*2+1]+(1.0-ax)*LUT[(y1*LUTWidth+x1)*2+1];
    *u = (ay*u0 + (1.0-ay)*u1)-Offset_u;
    *v = (ay*v0 + (1.0-ay)*v1)*0.5-Offset_v; // Scale down
    return 0;
}



/**
 * Cubic convolution function (from OpenCV)
 */

static inline void interpolateCubic( float x, float* coeffs )
{
    const float A = -0.5f; // -0.75f;
    coeffs[0] = ((A*(x + 1) - 5*A)*(x + 1) + 8*A)*(x + 1) - 4*A;
    coeffs[1] = ((A + 2)*x - (A + 3))*x*x + 1;
    coeffs[2] = ((A + 2)*(1 - x) - (A + 3))*(1 - x)*(1 - x) + 1;
    coeffs[3] = 1.f - coeffs[0] - coeffs[1] - coeffs[2];
}


/**
 * Bicubic convolution interpolation
 *
 * \see http://verona.fi-p.unam.mx/boris/practicas/CubConvInterp.pdf
 *
 */

/* Internal version */
int map_LUT_bicubicInternal(void* mapFuncData, float x, float y, float *u, float *v)
{
	float lx,ly;
	if (LUTStep>1) {
		lx = x/LUTStep+Offset_X;
		ly = y/LUTStep+Offset_Y;
	} else {
		lx = x+Offset_X;
		ly = y+Offset_Y;
	}
    float coeff[4];
    int x0 = floor(lx);
    int y0 = floor(ly);
    if ((lx<0)||(ly<0)||(lx>LUTWidth)||(ly>LUTHeight)) return 1;
    interpolateCubic( (lx-x0), coeff );
    float fu1=0, fu2=0, fu3=0, fu4=0;
    float fv1=0, fv2=0, fv3=0, fv4=0;
    if ((y0 > 0)&&(y0 <LUTHeight+1)) {
        float u1=0,u2=0,u3=0,u4=0;
        float v1=0,v2=0,v3=0,v4=0;
        if ((x0 > 0)&&(x0<LUTWidth+1))  { u1=LUT[((y0-1)*LUTWidth+(x0-1))*2+0]; v1=LUT[((y0-1)*LUTWidth+(x0-1))*2+1]; }
        if ((x0 > -1)&&(x0<LUTWidth))   { u2=LUT[((y0-1)*LUTWidth+(x0))*2+0];   v2=LUT[((y0-1)*LUTWidth+(x0))*2+1];   }
        if ((x0 > -2)&&(x0<LUTWidth-1)) { u3=LUT[((y0-1)*LUTWidth+(x0+1))*2+0]; v3=LUT[((y0-1)*LUTWidth+(x0+1))*2+1]; }
        if ((x0 > -3)&&(x0<LUTWidth-2)) { u4=LUT[((y0-1)*LUTWidth+(x0+2))*2+0]; v4=LUT[((y0-1)*LUTWidth+(x0+2))*2+1]; }
        // Repeat values at borders
        if (x0 == -2) {
            u1=u2=u3=u4; v1=v2=v3=v4;
        } else if (x0 == -1) {
            u1=u2=u3; v1=v2=v3;
        } else if (x0 == 0) {
            u1=u2; v1=v2;
        }
        if (x0 == LUTWidth) {
            u4=u3=u2=u1; v4=v3=v2=v1;
        } else if (x0 == LUTWidth-1) {
            u4=u3=u2; v4=v3=v2;
        } else if (x0 == LUTWidth-2) {
            u4=u3; v4=v3;
        }
        fu1 =  coeff[0]*u1 + coeff[1]*u2 + coeff[2]*u3 + coeff[3]*u4;
        fv1 =  coeff[0]*v1 + coeff[1]*v2 + coeff[2]*v3 + coeff[3]*v4;
    }
    if ((y0 > -1)&&(y0 <LUTHeight)) {
        float u1=0,u2=0,u3=0,u4=0;
        float v1=0,v2=0,v3=0,v4=0;
        if ((x0 > 0)&&(x0<LUTWidth+1))  { u1=LUT[((y0)*LUTWidth+(x0-1))*2+0]; v1=LUT[((y0)*LUTWidth+(x0-1))*2+1]; }
        if ((x0 > -1)&&(x0<LUTWidth))   { u2=LUT[((y0)*LUTWidth+(x0))*2+0];   v2=LUT[((y0)*LUTWidth+(x0))*2+1];   }
        if ((x0 > -2)&&(x0<LUTWidth-1)) { u3=LUT[((y0)*LUTWidth+(x0+1))*2+0]; v3=LUT[((y0)*LUTWidth+(x0+1))*2+1]; }
        if ((x0 > -3)&&(x0<LUTWidth-2)) { u4=LUT[((y0)*LUTWidth+(x0+2))*2+0]; v4=LUT[((y0)*LUTWidth+(x0+2))*2+1]; }
        // Repeat values at borders
        if (x0 == -2) {
            u1=u2=u3=u4; v1=v2=v3=v4;
        } else if (x0 == -1) {
            u1=u2=u3; v1=v2=v3;
        } else if (x0 == 0) {
            u1=u2; v1=v2;
        }
        if (x0 == LUTWidth) {
            u4=u3=u2=u1; v4=v3=v2=v1;
        } else if (x0 == LUTWidth-1) {
            u4=u3=u2; v4=v3=v2;
        } else if (x0 == LUTWidth-2) {
            u4=u3; v4=v3;
        }
        fu2 =  coeff[0]*u1 + coeff[1]*u2 + coeff[2]*u3 + coeff[3]*u4;
        fv2 =  coeff[0]*v1 + coeff[1]*v2 + coeff[2]*v3 + coeff[3]*v4;
    }
    if ((y0 > -2)&&(y0 <LUTHeight-1)) {
        float u1=0,u2=0,u3=0,u4=0;
        float v1=0,v2=0,v3=0,v4=0;
        if ((x0 > 0)&&(x0<LUTWidth+1))  { u1=LUT[((y0+1)*LUTWidth+(x0-1))*2+0]; v1=LUT[((y0+1)*LUTWidth+(x0-1))*2+1]; }
        if ((x0 > -1)&&(x0<LUTWidth))   { u2=LUT[((y0+1)*LUTWidth+(x0))*2+0];   v2=LUT[((y0+1)*LUTWidth+(x0))*2+1];   }
        if ((x0 > -2)&&(x0<LUTWidth-1)) { u3=LUT[((y0+1)*LUTWidth+(x0+1))*2+0]; v3=LUT[((y0+1)*LUTWidth+(x0+1))*2+1]; }
        if ((x0 > -3)&&(x0<LUTWidth-2)) { u4=LUT[((y0+1)*LUTWidth+(x0+2))*2+0]; v4=LUT[((y0+1)*LUTWidth+(x0+2))*2+1]; }
        // Repeat values at borders
        if (x0 == -2) {
            u1=u2=u3=u4; v1=v2=v3=v4;
        } else if (x0 == -1) {
            u1=u2=u3; v1=v2=v3;
        } else if (x0 == 0) {
            u1=u2; v1=v2;
        }
        if (x0 == LUTWidth) {
            u4=u3=u2=u1; v4=v3=v2=v1;
        } else if (x0 == LUTWidth-1) {
            u4=u3=u2; v4=v3=v2;
        } else if (x0 == LUTWidth-2) {
            u4=u3; v4=v3;
        }
        fu3 =  coeff[0]*u1 + coeff[1]*u2 + coeff[2]*u3 + coeff[3]*u4;
        fv3 =  coeff[0]*v1 + coeff[1]*v2 + coeff[2]*v3 + coeff[3]*v4;
    }
    if ((y0 > -3)&&(y0 <LUTHeight-2)) {
        float u1=0,u2=0,u3=0,u4=0;
        float v1=0,v2=0,v3=0,v4=0;
        if ((x0 > 0)&&(x0<LUTWidth+1))  { u1=LUT[((y0+2)*LUTWidth+(x0-1))*2+0]; v1=LUT[((y0+2)*LUTWidth+(x0-1))*2+1]; }
        if ((x0 > -1)&&(x0<LUTWidth))   { u2=LUT[((y0+2)*LUTWidth+(x0))*2+0];   v2=LUT[((y0+2)*LUTWidth+(x0))*2+1];   }
        if ((x0 > -2)&&(x0<LUTWidth-1)) { u3=LUT[((y0+2)*LUTWidth+(x0+1))*2+0]; v3=LUT[((y0+2)*LUTWidth+(x0+1))*2+1]; }
        if ((x0 > -3)&&(x0<LUTWidth-2)) { u4=LUT[((y0+2)*LUTWidth+(x0+2))*2+0]; v4=LUT[((y0+2)*LUTWidth+(x0+2))*2+1]; }
        // Repeat values at borders
        if (x0 == -2) {
            u1=u2=u3=u4; v1=v2=v3=v4;
        } else if (x0 == -1) {
            u1=u2=u3; v1=v2=v3;
        } else if (x0 == 0) {
            u1=u2; v1=v2;
        }
        if (x0 == LUTWidth) {
            u4=u3=u2=u1; v4=v3=v2=v1;
        } else if (x0 == LUTWidth-1) {
            u4=u3=u2; v4=v3=v2;
        } else if (x0 == LUTWidth-2) {
            u4=u3; v4=v3;
        }
        fu4 =  coeff[0]*u1 + coeff[1]*u2 + coeff[2]*u3 + coeff[3]*u4;
        fv4 =  coeff[0]*v1 + coeff[1]*v2 + coeff[2]*v3 + coeff[3]*v4;
    }
    // Repeat values at borders
    if (y0 == -2) {
        fu1=fu2=fu3=fu4; fv1=fv2=fv3=fv4;
    } else if (y0 == -1) {
        fu1=fu2=fu3; fv1=fv2=fv3;
    } else if (y0 == 0) {
        fu1=fu2; fv1=fv2;
    }
    if (y0 == LUTHeight) {
        fu4=fu3=fu2=fu1; fv4=fv3=fv2=fv1;
    } else if (y0 == LUTHeight-1) {
        fu4=fu3=fu2; fv4=fv3=fv2;
    } else if (y0 == LUTHeight-2) {
        fu4=fu3; fv4=fv3;
    }
    // Finally interpolate
    interpolateCubic( (ly-y0), coeff );
    *u = coeff[0]*fu1+coeff[1]*fu2+coeff[2]*fu3+coeff[3]*fu4-Offset_u;
    *v = coeff[0]*fv1+coeff[1]*fv2+coeff[2]*fv3+coeff[3]*fv4-Offset_v;
    return 0;
}


int __map_LUT_bicubic(void* mapFuncData, float x, float y, float *u, float *v)
{
	int ret = map_LUT_bicubicInternal(mapFuncData, x, y, u, v);
	if (ret) {
		return ret;
	}
	return 0;
}



int __map_LUT_bicubic_delta(void* mapFuncData, float x, float y, float *u, float *v)
{
	// Use the absolute value variant to interpolate the delta
	float delta_u,delta_v;
	int ret = map_LUT_bicubicInternal(mapFuncData, x,y,&delta_u,&delta_v);
	if (ret) {
		return ret;
	}
	// Just deltas to original x/y value
	*u = x+delta_u;
	*v = y+delta_v;
	return 0;
}


int __map_LUT_bicubic_420(void* mapFuncData, float x, float y, float *u, float *v)
{
	// Scale y-coordinates so we can re-use Y LUT
	float local_u,local_v;
	int ret = map_LUT_bicubicInternal(mapFuncData, x,y*2.0f,&local_u,&local_v);
	if (ret) {
		return ret;
	}
	*u = local_u;
	*v = (local_v-Offset_v)/2.0;
	return 0;
}


int __map_LUT_bicubic_420422(void* mapFuncData, float x, float y, float *u, float *v)
{
	// Scales coordinates so we can re-use Y LUT for 420 input and 422 output
	float local_u,local_v;
	int ret = map_LUT_bicubicInternal(mapFuncData, x,y,&local_u,&local_v); // output coords 422
	if (ret) {
		return ret;
	}
	*u = local_u;
	*v = (local_v-Offset_v)/2.0; // tex coords 420
	return 0;
}


int __map_LUT_bicubic_420transp(void* mapFuncData, float x, float y, float *u, float *v)
{
	// Scale y-coordinates so we can re-use Y LUT
	float local_u,local_v;
	int ret = map_LUT_bicubicInternal(mapFuncData, y*2.0f,x,&local_u,&local_v); // Transposed
	if (ret) {
		return ret;
	}
	*u = local_u;
	*v = (local_v-Offset_v)/2.0;
	return 0;
}




#if 1

/**
 * Reads a binary lookup table from file.
 * Simple converter code to create this format can be found in 'data' directory.
 *
 * @param fname The full file name to open
 *
 * The file consists of a simple header followed by
 * interleaved 32bit float values which represent
 * the dx/dy offset to lookup a u/v texel from x/y coordinate.
 *
 * This sample program requires the LUT to have the same
 * dimension as the output image, extending this to
 * general case is straightforward.
 * (again, check the converter code)
 */
int readBinaryLUT(const char* fname)
{
    uint32_t magic,width,height;
    size_t c, fsize;
    FILE * fp = fopen(fname,"rb");
    if (!fp) {
        return 1;
    }
    fseek(fp,0,SEEK_END);
    fsize=ftell(fp);
    fseek(fp,0,SEEK_SET);
    fread(&magic,sizeof(uint32_t),1,fp);
    fread(&width,sizeof(uint32_t),1,fp);
    fread(&height,sizeof(uint32_t),1,fp);
    if (magic != 0x4c524d49) { // 'IMRL'
        printf("Error, LUT file has wrong MAGIC, 'IMRL' expected\n");
        fclose(fp);
        return 1;
    }
    uint32_t nfloats = (2*width*height);
    uint32_t expected = nfloats*sizeof(float)+12;
    if (fsize != expected) {


    	printf("Error, LUT file has wrong size (%lld, should be %d)\n",(long long)fsize,expected);
        fclose(fp);
        return 1;
    }
    LUT = malloc(nfloats*sizeof(float));
    float *dst = LUT;
    int toread = nfloats;
    while (toread>0) {
        c = fread(dst,sizeof(float),2*width,fp);
        dst+=c;
        toread-=c;
    }
    fclose(fp);
    LUTWidth=width;
    LUTHeight=height;
    return 0;
}


/**
 * Reads a ASCII lookup table from file.
 *
 * @param fname The full file name to open
 *
 * File format is simple ASCII file,
 * '#' as first item on line starts comment,
 * '(nnn.nnnn nnn.nnn)' represents a u/v coordinate pair
 * which gives texture coordinate to use at this pixel.
 * We have width times height coordinates in LUT.
 *
 * This sample program requires the LUT to have the same
 * dimension as the output image.
 */
int readASCIILUT(const char* fname, int width, int height)
{
    char * curline=NULL;
    size_t llen;
    int lineno=0; // start counting with _one_
    FILE * fp = fopen(fname,"rt");
    if (!fp) {
        return 1;
    }

    int nfloats =2*width*height;
    float *dst = malloc(nfloats*sizeof(float));;
    memset(dst,0,nfloats*sizeof(float));

    LUTWidth=width;
    LUTHeight=height;
    LUT=dst;

    int toread = nfloats;
    while (toread) {
        int read = getline(&curline, &llen, fp);
    	//curline = readAllocLine(fp);
        if (!curline) {
            // EOF
            break;
        }
        lineno++;
        if (curline[0] == '#') continue; // skip comments
        /* parse line */
        char * cpos = curline;
        int coordsperline=0;
        do {
            cpos = strchr(cpos, '(');
            if (!cpos) break; // no more coords in line
            char * endpos = strchr(cpos+1,')');
            if (!endpos) {
                printf("WARN: Format: Missing closing bracket in line %d:\n%s\n",lineno,curline);
                break; // stop parsing this line
            }
            char * seppos = strchr(cpos+1,' ');
            if (!seppos) {
                printf("WARN: Format: Missing seperating space in line %d:\n%s\n",lineno,curline);
                break; // stop parsing this line
            }
            if (seppos>=endpos) {
                printf("ERROR: Format: next space %ld after ending bracket %ld in line %d:\n%s\n",
                        (seppos-curline),(endpos-curline),lineno,curline);
                break; // stop parsing this line
            }
            char * dend;
            float u,v;
            u=strtod(cpos+1,&dend);
            if (dend!=seppos) {
                printf("WARN: parsing double: dend %ld != seppos %ld\n",(dend-curline),(seppos-curline));
            }
            v=strtod(seppos+1,&dend);
            if (dend!=endpos) {
                printf("WARN: parsing double: dend %ld != endpos %ld\n",(dend-curline),(endpos-curline));
            }
            dst[0]=u; dst[1]=v;
            dst+=2;
            cpos=endpos;
            toread-=2;
            coordsperline++;
        } while (toread);
        if (coordsperline != width) {
            printf("WARN: line %d: parsed %d coords, expected %d\n",lineno,coordsperline,width);
        }
    }
    fclose(fp);
    if (toread) {
        printf("ERROR: Failed to parse file (%d coords missing)\n",toread);
        return 1;
    }

    return 0;
}

#endif


int setupLUT(const float* lutdata, int width, int height, int lutstep)
{
	if ((!lutdata)||(width<0)||(height<0)) {
		return 1;
	}
    LUTWidth=width;
    LUTHeight=height;
    LUTStep=lutstep;
    int nfloats =2*width*height;
    LUT = (float*)malloc(nfloats*sizeof(float));
    memcpy(LUT,lutdata,nfloats*sizeof(float));
    return 0;
}

int setupLUTNoCopy(float* lutdata, int width, int height, int lutstep)
{
    if ((!lutdata)||(width<0)||(height<0)) {
        return 1;
    }
    LUTWidth=width;
    LUTHeight=height;
    LUTStep=lutstep;
    int nfloats =2*width*height;
    if (LUT) free(LUT);
    LUT = lutdata;
    return 0;
}


void setLUTStep(int lutstep)
{
	LUTStep=lutstep;
}


int getLUTWidth()
{
	return LUT?LUTWidth:0;
}

int getLUTHeight()
{
	return LUT?LUTHeight:0;
}


void saveLUTAsHeader(const char* fname)
{
	if (!LUT || (LUTWidth<1) || (LUTHeight<1)) {
		printf("LUT is empty, nothing saved!\n");
		return;
	}
	FILE *fp=fopen(fname,"wt");
	if (!fp) {
		printf("Failed to open file %s for writing\n",fname);
		return;
	}
	fprintf(fp,"#define IMR_LUT_width  (%d)\n",LUTWidth);
	fprintf(fp,"#define IMR_LUT_height (%d)\n",LUTHeight);
	fprintf(fp,"#define IMR_LUT_step   (%d)\n",LUTStep);
	fprintf(fp,"/**\n * x,y value-pairs describing deviation of U,V coordinate from X,Y\n *\n * The width and height has to be greater or equal to 2\n */\n");
	fprintf(fp,"float IMR_LUT_data[IMR_LUT_width*IMR_LUT_height*2] = {\n");
	for (int y=0; y<LUTHeight; y++) {
		fprintf(fp,"        ");
		for (int x=0; x<LUTWidth; x++) {
			fprintf(fp,"%.3f, %.3f,  ",LUT[(y*LUTWidth+x)*2],LUT[(y*LUTWidth+x)*2+1]);
		}
		fprintf(fp,"\n");
	}
	fprintf(fp,"};\n\n");
	fclose(fp);
}

