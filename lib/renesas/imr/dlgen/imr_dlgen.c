/******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized. This
 * software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
 * LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software
 * and to discontinue the availability of this software. By using this software,
 * you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 ******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h> // floor


#include "imr_dlgen.h"
#include "imr_reg.h"

// Number of fractional bits for u/v coords in DL
#define SRC_DECIMALS (5)

// Number of fractional bits for x/y coords in DL
#define DST_DECIMALS (2)

/**
 * Structure to output texture coordinates into DL
 * (using 16bit fixpoint format for u/v)
 */
typedef struct IMR_Coord
{
    struct
    {
        uint16_t v, // !< source coordinate v
                 u; // !< source coordinate u

    };

} __attribute__((packed)) imr_coord_t;

/**
 * Convenience macro to output one coordinate
 */
#define WRITE_COORD(coord, c0, c1, decimals) \
    coord.u = (uint16_t)floor((c0) * (1 << (decimals))); coord.v = (uint16_t)floor((c1) * (1 << (decimals)));

/**
 * Builds DL for IMR using given mapping for U/V creation
 *
 * @param dl       Structure describing display list (memory pointers, size, write-position)
 *                 Make sure its allocated with enough free space (use calcDLSize if unsure)
 * @param src      Structure describing input image
 * @param dest     Structure describing output image
 * @param meshSize Spacing of triangle mesh in pixels (output coordinates).
 *                 Values 2,4 and 8 make usually sense, small values will result in larger DL,
 *                 while large values can be problematic in regards of distortion error (due to linear approximation).
 *                 Output image size shall be fully dividable by this, code may need changes if not.
 *
 * @param mapFunc  Mapping function giving texture coordinate for each output pixel
 *                 (not accessed with sub-pixel accuracy in this simple implementation)
 *
 * This method creates a Display list for IMR which is build using a regular mesh,
 * consisting of Triangle strips spanning the whole image, where vertices are evenly spaced.
 * This spacing is controlled by \ref meshSize parameter, and
 *
 * @return 0 on success, 1 else
 */

int imr_build_display_list(r_imr_dl_t* dl, r_imr_data_t* src, r_imr_data_t* dest, uint8_t meshSize, mesh_map_func mapFunc, void* mapFuncData)
{
    const int uxStep    = meshSize;
    const int vyStep    = meshSize;
    const int coordSize = 2;
    uint32_t* dst       = (uint32_t*)dl->VmrStartAddr;

    // Setup u,v Dimensions (Texture space)

    int swidth = (src->Width > 2048) ? 2048 : src->Width;
    int dwidth = (dest->Width > 2048) ? 2048 : dest->Width;

    // Input image size, always writing for UV8 mode (Y uses only lower 16 bits)
    *(dst + dl->Pos++) = IMR_OP_WTL(IMR_REG_OFFSET_SUSR, 1);
    *(dst + dl->Pos++) = ((swidth - 1) & 0x7ff) | (((swidth - 2) & 0x7ff) << 16);

    // height dimension is always stored in the LSBs
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_SVSR, src->Height - 1);

    // Setup clip rectangle for output
    // IMR_DL_XYClippingSet(&dl,0,0,imr->outFrameWidth-1,imr->outFrameHeight-1,DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_XMINR, 0 << DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_YMINR, 0 << DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_XMAXR, (dwidth - 1) << DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_YMAXR, (dest->Height - 1) << DST_DECIMALS);

    // Setup fix point format for U/V and X/Y coords
    // IMR_DL_UVXYDecimalPointSet(&dl,SRC_DECIMALS,DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_UVDPOR, (SRC_DECIMALS & 0x7) | (DST_DECIMALS ? 1 << 8 : 0 << 8));

    // NO AUTODG

    // Start to write commands at next free dword of DL memory
    dst += dl->Pos;

    // Start Tri-strips, one per line
    for (int y = 0 ; y < dest->Height ; y += vyStep)
    {
        // Check if DL can hold this line ...
        int nverts = (((src->Width - 1) / uxStep) + 2) * 2;

        int x_offset_from_zero = 0;
        int x_offset_from_width = src->Width;


        for (int x = 0 ; x <= src->Width ; x += uxStep)
        {
            float temp_u0, temp_v0;
            float temp_u1, temp_v1;
            int temp_res;

            temp_res = (*mapFunc)(mapFuncData, x, y, &temp_u0, &temp_v0); 
            temp_res |= (*mapFunc)(mapFuncData, x, y + vyStep, &temp_u1, &temp_v1); 
            
            if(     ((temp_u0>=0) && (temp_u0<src->Width)   &&
                    (temp_v0>=0)  && (temp_v0<src->Height)  &&
                    (temp_u1>=0)  && (temp_u1<src->Width)   &&
                    (temp_v1>=0)  && (temp_v1<src->Height)) 
                 && (temp_res==0)      )
            {
                x_offset_from_zero = x;
                break;
            }
        }

        for (int x = x_offset_from_zero + uxStep ; x <= src->Width ; x += uxStep)
        {
            float temp_u0, temp_v0;
            float temp_u1, temp_v1;
            int temp_res;

            temp_res = (*mapFunc)(mapFuncData, x, y, &temp_u0, &temp_v0); 
            temp_res |= (*mapFunc)(mapFuncData, x, y + vyStep, &temp_u1, &temp_v1); 
            
            if(     ((temp_u0<0) || (temp_u0>src->Width)   ||
                    (temp_v0<0)  || (temp_v0>src->Height)  ||
                    (temp_u1<0)  || (temp_u1>src->Width)   ||
                    (temp_v1<0)  || (temp_v1>src->Height)) 
                 && (temp_res==0)      )
            {
                x_offset_from_width = x - uxStep;
                break;
            }
        }
        if(x_offset_from_zero>= x_offset_from_width)
        {
            continue;
        }
        else
        {
            nverts = ((((src->Width - x_offset_from_zero - (src->Width - x_offset_from_width)) - 1) / uxStep) + 2) * 2;
        }


        if (dl->Pos + 1 + nverts * coordSize > dl->Size / 4)
        {
            printf("ERROR: DL full, y=%d: %d verts need %d bytes, but only %d bytes left\n",
                   y, nverts, (2 + nverts * coordSize) * 4, dl->Size - (dl->Pos + 1 + nverts * coordSize) * 4);
            return 1;
        }

        int vcount = 0;

        // Start a triangle strip
        *(dst) = IMR_OP_TRI(nverts);
        dst++;
        dl->Pos++;

        // Output first two triangles
        // NOTE: TCM = 0 (counter-clockwise) (default on V3M BSP driver)
        imr_coord_t* coord = (imr_coord_t*)dst;
        float        u, v;
        int          res;

        // NOTE: mapFunc needs addedd xofs
        //       X coordinates start with zero (-> subtracted xofs)
        //       u values from mapping need to subtract uofs
        res = (*mapFunc)(mapFuncData, x_offset_from_zero, y, &u, &v); // topleft
        WRITE_COORD(coord[0 * 2], u, v, SRC_DECIMALS);
        WRITE_COORD(coord[0 * 2 + 1], x_offset_from_zero, y, DST_DECIMALS);
        res |= (*mapFunc)(mapFuncData, x_offset_from_zero, y + vyStep, &u, &v); // bottomleft
        WRITE_COORD(coord[1 * 2], u, v, SRC_DECIMALS);
        WRITE_COORD(coord[1 * 2 + 1], x_offset_from_zero, y + vyStep, DST_DECIMALS);
        res |= (*mapFunc)(mapFuncData, x_offset_from_zero + uxStep, y, &u, &v); // topright
        WRITE_COORD(coord[2 * 2], u, v, SRC_DECIMALS);
        WRITE_COORD(coord[2 * 2 + 1], x_offset_from_zero + uxStep, y, DST_DECIMALS);
        res |= (*mapFunc)(mapFuncData, x_offset_from_zero + uxStep, y + vyStep, &u, &v); // bottomright
        WRITE_COORD(coord[3 * 2], u, v, SRC_DECIMALS);
        WRITE_COORD(coord[3 * 2 + 1], x_offset_from_zero + uxStep, (y + vyStep), DST_DECIMALS);

        vcount  += 4;
        dst     += 4 * coordSize;
        coord   += 4 * coordSize;
        dl->Pos += 4 * coordSize;

        // Output remaining vertices
        for (int x =  x_offset_from_zero + 2 * uxStep ; x <= x_offset_from_width ; x += uxStep)
        {
            // Add two points at top and bottom line for next two tris ...
            res |= (*mapFunc)(mapFuncData, x, y, &u, &v); // top
            WRITE_COORD(coord[0 * 2], u, v, SRC_DECIMALS);
            WRITE_COORD(coord[0 * 2 + 1], x, y, DST_DECIMALS);
            res |= (*mapFunc)(mapFuncData, x, y + vyStep, &u, &v); // bottom
            WRITE_COORD(coord[1 * 2], u, v, SRC_DECIMALS);
            WRITE_COORD(coord[1 * 2 + 1], x, y + vyStep, DST_DECIMALS);

            vcount  += 2;
            dst     += 2 * coordSize;
            coord   += 2 * coordSize;
            dl->Pos += 2 * coordSize;
        }

        // Ensure mapFunc did not return error
        if (res)
        {
            printf("ERROR from mapfunc\n");
            return 1;
        }

        // Ensure we have written exactly the number of vertices
        // as encoded in TRI command at beginning
        if (vcount != nverts)
        {
            printf("ERROR: vcount %d != nverts %d\n", vcount, nverts);
            return 1;
        }
    }

    // Update the number of used dwords in our DL struct

    int ptrdiff = (dst - (uint32_t*)dl->VmrStartAddr);

    if (ptrdiff != dl->Pos)
    {
        printf("ERROR: DL filling mismatch: ptr diff is %d, filled is %d\n", ptrdiff, dl->Pos);

        // dl->Pos=ptrdiff;
        return 1;
    }

    // Add sync and trap
    *(((uint32_t*)(dl->VmrStartAddr)) + dl->Pos++) = IMR_OP_SYNCM;
    *(((uint32_t*)(dl->VmrStartAddr)) + dl->Pos++) = IMR_OP_TRAP;
    return 0;
}


/**
 * Builds DL for IMR using given mapping for U/V creation
 *
 * currently, only support ROI for vertical direction
 *
 * @param dl       Structure describing display list (memory pointers, size, write-position)
 *                 Make sure its allocated with enough free space (use calcDLSize if unsure)
 * @param src      Structure describing input image
 * @param dest     Structure describing output image
 * @param meshSize Spacing of triangle mesh in pixels (output coordinates).
 *                 Values 2,4 and 8 make usually sense, small values will result in larger DL,
 *                 while large values can be problematic in regards of distortion error (due to linear approximation).
 *                 Output image size shall be fully dividable by this, code may need changes if not.
 *
 * @param mapFunc  Mapping function giving texture coordinate for each output pixel
 *                 (not accessed with sub-pixel accuracy in this simple implementation)
 * @param roi      destination ROI(processing ROI for subarea)
 *
 * This method creates a Display list for IMR which is build using a regular mesh,
 * consisting of Triangle strips spanning the whole image, where vertices are evenly spaced.
 * This spacing is controlled by \ref meshSize parameter, and
 *
 * @return 0 on success, 1 else
 */

int imr_build_display_list_roi(r_imr_dl_t* dl, r_imr_data_t* src, r_imr_data_t* dest, uint8_t meshSize, mesh_map_func mapFunc, void* mapFuncData, imr_roi_t roi)
{
    const int uxStep    = meshSize;
    const int vyStep    = meshSize;
    const int coordSize = 2;
    uint32_t* dst       = (uint32_t*)dl->VmrStartAddr;

    // Setup u,v Dimensions (Texture space)

    int swidth = (src->Width > 2048) ? 2048 : src->Width;
    int dwidth = (dest->Width > 2048) ? 2048 : dest->Width;

    // Input image size, always writing for UV8 mode (Y uses only lower 16 bits)
    *(dst + dl->Pos++) = IMR_OP_WTL(IMR_REG_OFFSET_SUSR, 1);
    *(dst + dl->Pos++) = ((swidth - 1) & 0x7ff) | (((swidth - 2) & 0x7ff) << 16);

    // height dimension is always stored in the LSBs
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_SVSR, src->Height - 1);

    // Setup clip rectangle for output
    // IMR_DL_XYClippingSet(&dl,0,0,imr->outFrameWidth-1,imr->outFrameHeight-1,DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_XMINR, 0 << DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_YMINR, 0 << DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_XMAXR, (dwidth - 1) << DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_YMAXR, (dest->Height - 1) << DST_DECIMALS);

    // Setup fix point format for U/V and X/Y coords
    // IMR_DL_UVXYDecimalPointSet(&dl,SRC_DECIMALS,DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_UVDPOR, (SRC_DECIMALS & 0x7) | (DST_DECIMALS ? 1 << 8 : 0 << 8));

    // NO AUTODG

    // Start to write commands at next free dword of DL memory
    dst += dl->Pos;

    // Start Tri-strips, one per line
    for (int y = roi.y ; y < roi.y + roi.Height ; y += vyStep)
    {
        // Check if DL can hold this line ...
        int nverts = (((src->Width - 1) / uxStep) + 2) * 2;

        if (dl->Pos + 1 + nverts * coordSize > dl->Size / 4)
        {
            printf("ERROR: DL full, y=%d: %d verts need %d bytes, but only %d bytes left\n",
                   y, nverts, (2 + nverts * coordSize) * 4, dl->Size - (dl->Pos + 1 + nverts * coordSize) * 4);
            return 1;
        }

        int vcount = 0;

        // Start a triangle strip
        *(dst) = IMR_OP_TRI(nverts);
        dst++;
        dl->Pos++;

        // Output first two triangles
        // NOTE: TCM = 0 (counter-clockwise) (default on V3M BSP driver)
        imr_coord_t* coord = (imr_coord_t*)dst;
        float        u, v;
        int          res;

        // NOTE: mapFunc needs addedd xofs
        //       X coordinates start with zero (-> subtracted xofs)
        //       u values from mapping need to subtract uofs
        res = (*mapFunc)(mapFuncData, 0, y, &u, &v); // topleft
        WRITE_COORD(coord[0 * 2], u, v, SRC_DECIMALS);
        WRITE_COORD(coord[0 * 2 + 1], 0, y, DST_DECIMALS);
        res |= (*mapFunc)(mapFuncData, 0, y + vyStep, &u, &v); // bottomleft
        WRITE_COORD(coord[1 * 2], u, v, SRC_DECIMALS);
        WRITE_COORD(coord[1 * 2 + 1], 0, y + vyStep, DST_DECIMALS);
        res |= (*mapFunc)(mapFuncData, 0 + uxStep, y, &u, &v); // topright
        WRITE_COORD(coord[2 * 2], u, v, SRC_DECIMALS);
        WRITE_COORD(coord[2 * 2 + 1], uxStep, y, DST_DECIMALS);
        res |= (*mapFunc)(mapFuncData, 0 + uxStep, y + vyStep, &u, &v); // bottomright
        WRITE_COORD(coord[3 * 2], u, v, SRC_DECIMALS);
        WRITE_COORD(coord[3 * 2 + 1], uxStep, (y + vyStep), DST_DECIMALS);

        vcount  += 4;
        dst     += 4 * coordSize;
        coord   += 4 * coordSize;
        dl->Pos += 4 * coordSize;

        // Output remaining vertices
        for (int x = 2 * uxStep ; x <= src->Width ; x += uxStep)
        {
            // Add two points at top and bottom line for next two tris ...
            res |= (*mapFunc)(mapFuncData, x, y, &u, &v); // top
            WRITE_COORD(coord[0 * 2], u, v, SRC_DECIMALS);
            WRITE_COORD(coord[0 * 2 + 1], x, y, DST_DECIMALS);
            res |= (*mapFunc)(mapFuncData, x, y + vyStep, &u, &v); // bottom
            WRITE_COORD(coord[1 * 2], u, v, SRC_DECIMALS);
            WRITE_COORD(coord[1 * 2 + 1], x, y + vyStep, DST_DECIMALS);

            vcount  += 2;
            dst     += 2 * coordSize;
            coord   += 2 * coordSize;
            dl->Pos += 2 * coordSize;
        }

        // Ensure mapFunc did not return error
        if (res)
        {
            printf("ERROR from mapfunc\n");
            return 1;
        }

        // Ensure we have written exactly the number of vertices
        // as encoded in TRI command at beginning
        if (vcount != nverts)
        {
            printf("ERROR: vcount %d != nverts %d\n", vcount, nverts);
            return 1;
        }
    }

    // Update the number of used dwords in our DL struct

    int ptrdiff = (dst - (uint32_t*)dl->VmrStartAddr);

    if (ptrdiff != dl->Pos)
    {
        printf("ERROR: DL filling mismatch: ptr diff is %d, filled is %d\n", ptrdiff, dl->Pos);

        // dl->Pos=ptrdiff;
        return 1;
    }

    // Add sync and trap
    *(((uint32_t*)(dl->VmrStartAddr)) + dl->Pos++) = IMR_OP_SYNCM;
    *(((uint32_t*)(dl->VmrStartAddr)) + dl->Pos++) = IMR_OP_TRAP;
    return 0;
}


/**
 * Builds DL for IMR using given mapping for U/V creation
 *
 * currently only support cropping and roi for vertical direction
 *
 * @param dl       Structure describing display list (memory pointers, size, write-position)
 *                 Make sure its allocated with enough free space (use calcDLSize if unsure)
 * @param src      Structure describing input image
 * @param dest     Structure describing output image
 * @param meshSize Spacing of triangle mesh in pixels (output coordinates).
 *                 Values 2,4 and 8 make usually sense, small values will result in larger DL,
 *                 while large values can be problematic in regards of distortion error (due to linear approximation).
 *                 Output image size shall be fully dividable by this, code may need changes if not.
 *
 * @param mapFunc  Mapping function giving texture coordinate for each output pixel
 *                 (not accessed with sub-pixel accuracy in this simple implementation)
 * @param roi      destination ROI(processing ROI for subarea)
 * @param roi_crop destination crop area(has origin in non-cropped destination image)
 *
 * This method creates a Display list for IMR which is build using a regular mesh,
 * consisting of Triangle strips spanning the whole image, where vertices are evenly spaced.
 * This spacing is controlled by \ref meshSize parameter, and
 *
 * @return 0 on success, 1 else
 */

int imr_build_display_list_roi_with_crop(r_imr_dl_t* dl, r_imr_data_t* src, r_imr_data_t* dest, uint8_t meshSize, mesh_map_func mapFunc, void* mapFuncData, imr_roi_t roi, imr_roi_t roi_crop)
{
    const int uxStep    = meshSize;
    const int vyStep    = meshSize;
    const int coordSize = 2;
    uint32_t* dst       = (uint32_t*)dl->VmrStartAddr;

    // Setup u,v Dimensions (Texture space)

    int swidth = (src->Width > 2048) ? 2048 : src->Width;
    int dwidth = (dest->Width > 2048) ? 2048 : dest->Width;

    // Input image size, always writing for UV8 mode (Y uses only lower 16 bits)
    *(dst + dl->Pos++) = IMR_OP_WTL(IMR_REG_OFFSET_SUSR, 1);
    *(dst + dl->Pos++) = ((swidth - 1) & 0x7ff) | (((swidth - 2) & 0x7ff) << 16);

    // height dimension is always stored in the LSBs
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_SVSR, src->Height - 1);

    // Setup clip rectangle for output
    // IMR_DL_XYClippingSet(&dl,0,0,imr->outFrameWidth-1,imr->outFrameHeight-1,DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_XMINR, 0 << DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_YMINR, 0 << DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_XMAXR, (dwidth - 1) << DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_YMAXR, (dest->Height - 1) << DST_DECIMALS);

    // Setup fix point format for U/V and X/Y coords
    // IMR_DL_UVXYDecimalPointSet(&dl,SRC_DECIMALS,DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_UVDPOR, (SRC_DECIMALS & 0x7) | (DST_DECIMALS ? 1 << 8 : 0 << 8));

    // NO AUTODG

    // Start to write commands at next free dword of DL memory
    dst += dl->Pos;

    // Start Tri-strips, one per line
    for (int y = roi.y ; y < roi.y + roi.Height ; y += vyStep)
    {
        if((y>=roi_crop.y)&&(y<roi_crop.y+roi_crop.Height))
        {
 
            // Check if DL can hold this line ...
            int nverts = (((src->Width - 1) / uxStep) + 2) * 2;

            if (dl->Pos + 1 + nverts * coordSize > dl->Size / 4)
            {
                printf("ERROR: DL full, y=%d: %d verts need %d bytes, but only %d bytes left\n",
                       y, nverts, (2 + nverts * coordSize) * 4, dl->Size - (dl->Pos + 1 + nverts * coordSize) * 4);
                return 1;
            }
    
            int vcount = 0;
    
            // Start a triangle strip
            *(dst) = IMR_OP_TRI(nverts);
            dst++;
            dl->Pos++;
    
            // Output first two triangles
            // NOTE: TCM = 0 (counter-clockwise) (default on V3M BSP driver)
            imr_coord_t* coord = (imr_coord_t*)dst;
            float        u, v;
            int          res;
    
            // NOTE: mapFunc needs addedd xofs
            //       X coordinates start with zero (-> subtracted xofs)
            //       u values from mapping need to subtract uofs
            res = (*mapFunc)(mapFuncData, 0, y, &u, &v); // topleft
            WRITE_COORD(coord[0 * 2], u, v, SRC_DECIMALS);
            WRITE_COORD(coord[0 * 2 + 1], 0, y-roi_crop.y, DST_DECIMALS);
            res |= (*mapFunc)(mapFuncData, 0, y + vyStep, &u, &v); // bottomleft
            WRITE_COORD(coord[1 * 2], u, v, SRC_DECIMALS);
            WRITE_COORD(coord[1 * 2 + 1], 0, y + vyStep-roi_crop.y, DST_DECIMALS);
            res |= (*mapFunc)(mapFuncData, 0 + uxStep, y, &u, &v); // topright
            WRITE_COORD(coord[2 * 2], u, v, SRC_DECIMALS);
            WRITE_COORD(coord[2 * 2 + 1], uxStep, y-roi_crop.y, DST_DECIMALS);
            res |= (*mapFunc)(mapFuncData, 0 + uxStep, y + vyStep, &u, &v); // bottomright
            WRITE_COORD(coord[3 * 2], u, v, SRC_DECIMALS);
            WRITE_COORD(coord[3 * 2 + 1], uxStep, (y + vyStep-roi_crop.y), DST_DECIMALS);
    
            vcount  += 4;
            dst     += 4 * coordSize;
            coord   += 4 * coordSize;
            dl->Pos += 4 * coordSize;
    
            // Output remaining vertices
            for (int x = 2 * uxStep ; x <= src->Width ; x += uxStep)
            {
                // Add two points at top and bottom line for next two tris ...
                res |= (*mapFunc)(mapFuncData, x, y, &u, &v); // top
                WRITE_COORD(coord[0 * 2], u, v, SRC_DECIMALS);
                WRITE_COORD(coord[0 * 2 + 1], x, y-roi_crop.y, DST_DECIMALS);
                res |= (*mapFunc)(mapFuncData, x, y + vyStep, &u, &v); // bottom
                WRITE_COORD(coord[1 * 2], u, v, SRC_DECIMALS);
                WRITE_COORD(coord[1 * 2 + 1], x, y + vyStep-roi_crop.y, DST_DECIMALS);
    
                vcount  += 2;
                dst     += 2 * coordSize;
                coord   += 2 * coordSize;
                dl->Pos += 2 * coordSize;
            }
    
            // Ensure mapFunc did not return error
            if (res)
            {
                printf("ERROR from mapfunc\n");
                return 1;
            }
    
            // Ensure we have written exactly the number of vertices
            // as encoded in TRI command at beginning
            if (vcount != nverts)
            {
                printf("ERROR: vcount %d != nverts %d\n", vcount, nverts);
                return 1;
            }

        }
   }

    // Update the number of used dwords in our DL struct

    int ptrdiff = (dst - (uint32_t*)dl->VmrStartAddr);

    if (ptrdiff != dl->Pos)
    {
        printf("ERROR: DL filling mismatch: ptr diff is %d, filled is %d\n", ptrdiff, dl->Pos);

        // dl->Pos=ptrdiff;
        return 1;
    }

    // Add sync and trap
    *(((uint32_t*)(dl->VmrStartAddr)) + dl->Pos++) = IMR_OP_SYNCM;
    *(((uint32_t*)(dl->VmrStartAddr)) + dl->Pos++) = IMR_OP_TRAP;
    return 0;
}

/**
 * Builds DL for IMR using given mapping for U/V creation
 *
 * This version supports translation and mirroring of the image
 *
 * @param dl       Structure describing display list (memory pointers, size, write-position)
 *                 Make sure its allocated with enough free space (use calcDLSize if unsure)
 * @param src      Structure describing input image
 * @param dest     Structure describing output image
 * @param meshSize Spacing of triangle mesh in pixels (output coordinates).
 *                 Values 2,4 and 8 make usually sense, small values will result in larger DL,
 *                 while large values can be problematic in regards of distortion error (due to linear approximation).
 *                 Output image size shall be fully dividable by this, code may need changes if not.
 *
 * @param mapFunc  Mapping function giving texture coordinate for each output pixel
 *                 (not accessed with sub-pixel accuracy in this simple implementation)
 * @param roi      destination ROI(processing ROI for subarea)
 * @param mirror   flag to set mirroring of the result image
 *
 * This method creates a Display list for IMR which is build using a regular mesh,
 * consisting of Triangle strips spanning the whole image, where vertices are evenly spaced.
 * This spacing is controlled by \ref meshSize parameter, and
 *
 * @return 0 on success, 1 else
 */

int imr_build_display_list_roi_with_shift(r_imr_dl_t* dl, r_imr_data_t* src, r_imr_data_t* dest, uint8_t meshSize, mesh_map_func mapFunc, void* mapFuncData, imr_roi_t roi, int8_t mirror)
{
    const int uxStep    = meshSize;
    const int vyStep    = meshSize;
    const int coordSize = 2;
    uint32_t* dst       = (uint32_t*)dl->VmrStartAddr;
    
    // Flag for mirroring the image
    int src_x_rev = 1;
    if( mirror )
    {
        src_x_rev = -1;
    }

    // Setup u,v Dimensions (Texture space)
    int swidth = (src->Width > 2048) ? 2048 : src->Width;
    int dwidth = (dest->Width > 2048) ? 2048 : dest->Width;

    // Input image size, always writing for UV8 mode (Y uses only lower 16 bits)
    *(dst + dl->Pos++) = IMR_OP_WTL(IMR_REG_OFFSET_SUSR, 1);
    *(dst + dl->Pos++) = ((swidth - 1) & 0x7ff) | (((swidth - 2) & 0x7ff) << 16);

    // height dimension is always stored in the LSBs
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_SVSR, src->Height - 1);

    // Setup clip rectangle for output
    // IMR_DL_XYClippingSet(&dl,0,0,imr->outFrameWidth-1,imr->outFrameHeight-1,DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_XMINR, 0 << DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_YMINR, 0 << DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_XMAXR, (dwidth - 1) << DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_YMAXR, (dest->Height - 1) << DST_DECIMALS);

    // Setup fix point format for U/V and X/Y coords
    // IMR_DL_UVXYDecimalPointSet(&dl,SRC_DECIMALS,DST_DECIMALS);
    *(dst + dl->Pos++) = IMR_OP_WTS(IMR_REG_OFFSET_UVDPOR, (SRC_DECIMALS & 0x7) | (DST_DECIMALS ? 1 << 8 : 0 << 8));

    // NO AUTODG

    // Start to write commands at next free dword of DL memory
    dst += dl->Pos;

    // Start Tri-strips, one per line
    for (int y = roi.y ; y < roi.y + roi.Height ; y += vyStep)
    {
 
        // Check if DL can hold this line ...
        int nverts = (((src->Width - 1) / uxStep) + 2) * 2;

        if (dl->Pos + 1 + nverts * coordSize > dl->Size / 4)
        {
            printf("ERROR: DL full, y=%d: %d verts need %d bytes, but only %d bytes left\n",
                   y, nverts, (2 + nverts * coordSize) * 4, dl->Size - (dl->Pos + 1 + nverts * coordSize) * 4);
            return 1;
        }
    
        int vcount = 0;
    
        // Start a triangle strip
        *(dst) = IMR_OP_TRI(nverts);
        dst++;
        dl->Pos++;
    
        // Output first two triangles
        // NOTE: TCM = 0 (counter-clockwise) (default on V3M BSP driver)
        imr_coord_t* coord = (imr_coord_t*)dst;
        float        u, v;
        int          res;
    
        // NOTE: mapFunc needs addedd xofs
        //       X coordinates start with zero (-> subtracted xofs)
        //       u values from mapping need to subtract uofs
        res = (*mapFunc)(mapFuncData, 0, y, &u, &v); // topleft
        WRITE_COORD(coord[0 * 2], src->Width*mirror + (src_x_rev)*(u + roi.x_offset), v + roi.y_offset, SRC_DECIMALS);
        WRITE_COORD(coord[0 * 2 + 1], roi.x_shift, y+roi.y_shift, DST_DECIMALS);
        res |= (*mapFunc)(mapFuncData, 0, y + vyStep , &u, &v); // bottomleft
        WRITE_COORD(coord[1 * 2], src->Width*mirror + (src_x_rev)*(u + roi.x_offset), v + roi.y_offset, SRC_DECIMALS);
        WRITE_COORD(coord[1 * 2 + 1], roi.x_shift, y + vyStep+roi.y_shift, DST_DECIMALS);
        res |= (*mapFunc)(mapFuncData, 0 + uxStep, y, &u, &v); // topright
        WRITE_COORD(coord[2 * 2], src->Width*mirror + (src_x_rev)*(u + roi.x_offset), v + roi.y_offset, SRC_DECIMALS);
        WRITE_COORD(coord[2 * 2 + 1], uxStep+roi.x_shift, y+roi.y_shift, DST_DECIMALS);
        res |= (*mapFunc)(mapFuncData, 0 + uxStep, y + vyStep, &u, &v); // bottomright
        WRITE_COORD(coord[3 * 2], src->Width*mirror + (src_x_rev)*(u + roi.x_offset), v + roi.y_offset, SRC_DECIMALS);
        WRITE_COORD(coord[3 * 2 + 1], uxStep+roi.x_shift, (y + vyStep+roi.y_shift), DST_DECIMALS);
    
        vcount  += 4;
        dst     += 4 * coordSize;
        coord   += 4 * coordSize;
        dl->Pos += 4 * coordSize;
    
        // Output remaining vertices
        for (int x = 2 * uxStep ; x <= src->Width ; x += uxStep)
        {
            // Add two points at top and bottom line for next two tris ...
            res |= (*mapFunc)(mapFuncData, x, y, &u, &v); // top
            WRITE_COORD(coord[0 * 2], src->Width*mirror + (src_x_rev)*(u + roi.x_offset), v + roi.y_offset, SRC_DECIMALS);
            WRITE_COORD(coord[0 * 2 + 1], x + roi.x_shift, y + roi.y_shift, DST_DECIMALS);
            res |= (*mapFunc)(mapFuncData, x, y + vyStep, &u, &v); // bottom
            WRITE_COORD(coord[1 * 2], src->Width*mirror + (src_x_rev)*(u + roi.x_offset), v + roi.y_offset, SRC_DECIMALS);
            WRITE_COORD(coord[1 * 2 + 1], x + roi.x_shift, y + vyStep+roi.y_shift, DST_DECIMALS);
    
            vcount  += 2;
            dst     += 2 * coordSize;
            coord   += 2 * coordSize;
            dl->Pos += 2 * coordSize;
        }
    
        // Ensure mapFunc did not return error
        if (res)
        {
            printf("ERROR from mapfunc\n");
            return 1;
        }
    
        // Ensure we have written exactly the number of vertices
        // as encoded in TRI command at beginning
        if (vcount != nverts)
        {
            printf("ERROR: vcount %d != nverts %d\n", vcount, nverts);
            return 1;
        }
    }

    // Update the number of used dwords in our DL struct
    int ptrdiff = (dst - (uint32_t*)dl->VmrStartAddr);

    if (ptrdiff != dl->Pos)
    {
        printf("ERROR: DL filling mismatch: ptr diff is %d, filled is %d\n", ptrdiff, dl->Pos);
        return 1;
    }

    // Add sync and trap
    *(((uint32_t*)(dl->VmrStartAddr)) + dl->Pos++) = IMR_OP_SYNCM;
    *(((uint32_t*)(dl->VmrStartAddr)) + dl->Pos++) = IMR_OP_TRAP;
    return 0;
}
