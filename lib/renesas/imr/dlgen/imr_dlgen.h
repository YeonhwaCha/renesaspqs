/******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized. This
 * software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
 * LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software
 * and to discontinue the availability of this software. By using this software,
 * you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
 ******************************************************************************/

#ifndef IMRDL_H_
#define IMRDL_H_


#include "r_imr_type.h" // for r_imr_dl_t and r_imr_data_t

/**
 * Prototype for mapping function passed to display list creation
 *
 * @param mapFuncData Pointer to struct with additional parameters used by map_func implementation (set to zero if not used)
 * @param X Destination coordinate
 * @param Y Destination coordinate
 * @param u Texture coordinate (output)
 * @param v Texture coordinate (output)
 * @return status, 1 on success, 0 on error
 */
typedef int (*mesh_map_func)(void* mapFuncData, float x, float y, float *U, float *V);

typedef struct imr_roi {
    uint16_t x;         //!< ROI start x
    uint16_t y;         //!< ROI start y
    uint16_t Width;     //!< ROI width
    uint16_t Height;    //!< ROI height
    int32_t  x_shift;   //!< ROI screen shift x 
    int32_t  y_shift;   //!< ROI screen shift y
    int32_t  x_offset;  //!< ROI camera offset x 
    int32_t  y_offset;  //!< ROI camera offset y
} imr_roi_t;


/**
 * @brief Build a display list for IMR-LX4
 *
 * @param dl    Display list structure including previously allocated memory (virtual address is accessed).
 *              The actual display list is written to this memory region.
 * @param src   Structure describing input image. Only attributes (Width, Height, Stride and Format) are read.
 *              Actual memory (virtual/physical) is not accessed by this function, and no address is written to DL.
 * @param dest  Structure describing output image. Only attributes (Width, Height, Stride and Format) are read.
 *                  Actual memory (virtual/physical) is not accessed by this function, and no address is written to DL.
 * @param meshSize  Horizontal and vertical size of triangles in output coordinate system (in pixel)
 * @param mapFunc   Pointer to function providing source image coordinates for each valid output image coordinate.
 *                  In general this function has to be smooth with sub-pixel accuracy (piecewise linear is not enough),
 *                  but this (simple) sample implementation does _not_ use it.
 *                  Be warned that our upcoming library _does_ require sub-pixel accuracy to work.
 * @param mapFuncData  Optional data to pass to mapFunc (set to zero if not used)
 *
 * Builds a display list which applies a image transformation according to passed mapping function.
 * Image parameters (bitdepth, color format and size, including stride) have to match the
 * parameters set up in IMR registers (done by driver) when executing the display list.
 *
 */
int imr_build_display_list(r_imr_dl_t *dl, r_imr_data_t *src, r_imr_data_t *dest, uint8_t meshSize, mesh_map_func mapFunc, void* mapFuncData);

/**
 * @brief Build a display list for IMR-LX4
 *
 * currently, only support ROI for vertical direction
 *
 * @param dl    Display list structure including previously allocated memory (virtual address is accessed).
 *              The actual display list is written to this memory region.
 * @param src   Structure describing input image. Only attributes (Width, Height, Stride and Format) are read.
 *              Actual memory (virtual/physical) is not accessed by this function, and no address is written to DL.
 * @param dest  Structure describing output image. Only attributes (Width, Height, Stride and Format) are read.
 *                  Actual memory (virtual/physical) is not accessed by this function, and no address is written to DL.
 * @param meshSize  Horizontal and vertical size of triangles in output coordinate system (in pixel)
 * @param mapFunc   Pointer to function providing source image coordinates for each valid output image coordinate.
 *                  In general this function has to be smooth with sub-pixel accuracy (piecewise linear is not enough),
 *                  but this (simple) sample implementation does _not_ use it.
 *                  Be warned that our upcoming library _does_ require sub-pixel accuracy to work.
 * @param mapFuncData  Optional data to pass to mapFunc (set to zero if not used)
 * @param roi  destination ROI
 *
 * Builds a display list which applies a image transformation according to passed mapping function.
 * Image parameters (bitdepth, color format and size, including stride) have to match the
 * parameters set up in IMR registers (done by driver) when executing the display list.
 *
 */
int imr_build_display_list_roi(r_imr_dl_t *dl, r_imr_data_t *src, r_imr_data_t *dest, uint8_t meshSize, mesh_map_func mapFunc, void* mapFuncData, imr_roi_t roi);

/**
 * @brief Build a display list for IMR-LX4
 *
 * @param dl    Display list structure including previously allocated memory (virtual address is accessed).
 *              The actual display list is written to this memory region.
 * @param src   Structure describing input image. Only attributes (Width, Height, Stride and Format) are read.
 *              Actual memory (virtual/physical) is not accessed by this function, and no address is written to DL.
 * @param dest  Structure describing output image. Only attributes (Width, Height, Stride and Format) are read.
 *                  Actual memory (virtual/physical) is not accessed by this function, and no address is written to DL.
 * @param meshSize  Horizontal and vertical size of triangles in output coordinate system (in pixel)
 * @param mapFunc   Pointer to function providing source image coordinates for each valid output image coordinate.
 *                  In general this function has to be smooth with sub-pixel accuracy (piecewise linear is not enough),
 *                  but this (simple) sample implementation does _not_ use it.
 *                  Be warned that our upcoming library _does_ require sub-pixel accuracy to work.
 * @param mapFuncData  Optional data to pass to mapFunc (set to zero if not used)
 * @param roi destination ROI(processing area for subdivision)
 * @param roi_crop ( crop area, based on destination coordinate of non-cropped image)
 * 
 *
 * Builds a display list which applies a image transformation according to passed mapping function.
 * Image parameters (bitdepth, color format and size, including stride) have to match the
 * parameters set up in IMR registers (done by driver) when executing the display list.
 *
 */
int imr_build_display_list_roi_with_crop(r_imr_dl_t *dl, r_imr_data_t *src, r_imr_data_t *dest, uint8_t meshSize, mesh_map_func mapFunc, void* mapFuncData, imr_roi_t roi, imr_roi_t roi_crop);

/**
 * Builds DL for IMR using given mapping for U/V creation
 *
 * This version supports translation and mirroring of the image
 *
 * @param dl       Structure describing display list (memory pointers, size, write-position)
 *                 Make sure its allocated with enough free space (use calcDLSize if unsure)
 * @param src      Structure describing input image
 * @param dest     Structure describing output image
 * @param meshSize Spacing of triangle mesh in pixels (output coordinates).
 *                 Values 2,4 and 8 make usually sense, small values will result in larger DL,
 *                 while large values can be problematic in regards of distortion error (due to linear approximation).
 *                 Output image size shall be fully dividable by this, code may need changes if not.
 *
 * @param mapFunc  Mapping function giving texture coordinate for each output pixel
 *                 (not accessed with sub-pixel accuracy in this simple implementation)
 * @param roi      destination ROI(processing ROI for subarea)
 * @param mirror   flag to set mirroring of the result image
 *
 * This method creates a Display list for IMR which is build using a regular mesh,
 * consisting of Triangle strips spanning the whole image, where vertices are evenly spaced.
 * This spacing is controlled by \ref meshSize parameter, and
 *
 * @return 0 on success, 1 else
 */
int imr_build_display_list_roi_with_shift(r_imr_dl_t *dl, r_imr_data_t *src, r_imr_data_t *dest, uint8_t meshSize, mesh_map_func mapFunc, void* mapFuncData, imr_roi_t roi, int8_t mirror);
#endif /* IMRDL_H_ */
