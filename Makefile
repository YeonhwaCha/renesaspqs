.PHONY: default clean rebuild lint doxygen deploy debug user

default: make/user_config.mk
	@echo "Making CVe sources..."
	@$(MAKE) -f make/cve.mk default --no-print-directory
	@echo "Making ARM sources..."
	@$(MAKE) -f make/src.mk default --no-print-directory
	@echo "Build completed successfully"

clean:
	@echo "Cleaning CVe build artifacts..."
	@$(MAKE) -f make/cve.mk clean --no-print-directory
	@echo "Cleaning ARM build artifacts..."
	@$(MAKE) -f make/src.mk clean --no-print-directory
	@echo "Project cleaned"

rebuild: clean default

lint:
	@echo "Cleaning PC-lint project..."
	@$(MAKE) -f make/lint.mk clean --no-print-directory
	@echo "Making PC-lint project..."
	@$(MAKE) -f make/lint.mk default --no-print-directory

doxygen:
	@echo "Making doxygen documentation..."
	@doxygen misc/doxygen/doxygen_config
	@start doc/doxygen/index.html

deploy: default
	@echo "Cleaning RenesasPQS deployment..."
	@$(MAKE) -f make/deploy.mk clean --no-print-directory
	@echo "Making RenesasPQS deployment..."
	@$(MAKE) -f make/deploy.mk default --no-print-directory

debug: export DEBUG:=true
debug: default
	@echo "This build is a debug version"

user_pre:
	@$(MAKE) -f make/user_config.mk user_pre_build --no-print-directory

user_post: 
	@$(MAKE) -f make/user_config.mk user_post_build --no-print-directory

user: user_pre default user_post

make/user_config.mk:
	@echo "Generating empty config file at $@"
	@echo "#Use this file to change configurations for different users of the same project"                 >> $@
	@echo "#All paths must use forward slash (i.e '/') not backslash"                                       >> $@
	@echo ""                                                                                                >> $@
	@echo "#Define path to the rcar_environment folder"                                                     >> $@
	@echo "RCAR_ENV_PATH := C:/Renesas/rcar_environment/RCAR_V3H1/rcar_linux_HIL_aarch64_GNU"               >> $@
	@echo ""                                                                                                >> $@
	@echo "#Define compiler path, only necessary for Windows compilation"                                   >> $@
	@echo "COMPILER_PATH := C:/Renesas/toolchains/gcc-linaro-7.3.1-2018.05-i686-mingw32_aarch64-linux-gnu"  >> $@
	@echo ""                                                                                                >> $@
	@echo "#Define absolute path to the RenesasPQS root folder (only necesary for running PC-Lint)"         >> $@
	@echo "PROJECT_PATH := C:/Renesas/projects/RenesasPQS"                                                  >> $@
	@echo ""                                                                                                >> $@
	@echo "#Optional user pre build step invoked with target 'user'"                                        >> $@
	@echo "user_pre_build:"                                                                                 >> $@
	@echo ""                                                                                                >> $@
	@echo "#Optional user post build step invoked with target 'user'"                                       >> $@
	@echo "user_post_build:"                                                                                >> $@
	@echo ""                                                                                                >> $@

